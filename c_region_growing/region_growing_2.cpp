/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "region_growing.h"
#include "region_growing_2.h"
#include "colors.h"
#include <queue>
#include <stdlib.h>
#include <math.h>


const int H_MAX = 180;
const int H_MAX_D_2 = H_MAX/2;

#define STRAT_DIFF
//#define STRAT_RANGES


inline double h_diff (double h1, double h2)
{
    double diff = fabs(h1-h2);
    if (diff < H_MAX_D_2)
    {
        return diff;
    }
    else
    {
        return H_MAX - diff;
    }
}

inline bool similar(double h1, double s1, double v1, 
                    double h2, double s2, double v2,
                    std::vector<double> *diff_data)
{
    
    double hd = h_diff(h1, h2);
    double sd = fabs(s1-s2);
    double vd = fabs(v1-v2);
    
#ifdef STRAT_DIFF
//    double diff = (*diff_data)[0]*hd + (*diff_data)[1]*hd*hd + (*diff_data)[2]*hd*hd*hd +
//                  (*diff_data)[3]*sd + (*diff_data)[4]*sd*sd + (*diff_data)[5]*sd*sd*sd +
//                  (*diff_data)[6]*vd + (*diff_data)[7]*vd*vd + (*diff_data)[8]*vd*vd*vd +
//                  (*diff_data)[9]*vd*sd + (*diff_data)[10]*vd*hd + (*diff_data)[11]*sd*hd +
//                  (*diff_data)[12];
    
    double diff = (*diff_data)[0]*hd + (*diff_data)[1]*sd + (*diff_data)[2]*vd +
                  (*diff_data)[3]*vd*sd + (*diff_data)[4]*vd*hd + (*diff_data)[5]*sd*hd +
                  (*diff_data)[6];
    
//    printf("diff: %f\n", diff);
                  
//    return (-2.5 < diff && diff < -2) || (-0.5 < diff && diff < 0.5) || (2 < diff && diff < 2.5);
    return diff < (*diff_data)[7];

#endif
    
#ifdef STRAT_RANGES
    
    const int ROWS = 3;
    const int COLUMNS = 6;
    const int DIFFS = ROWS+1;
    const int CHANNELS = 3;
    
    double ranges[ROWS][COLUMNS];
    
    for (int i = 0; i < ROWS; i++)
    {
        for (int j = 0; j < COLUMNS; j++)
        {
            ranges[i][j] = (*diff_data)[COLUMNS*i+j];
        }
    }
    
    double diffs[DIFFS][CHANNELS];
    
    for (int i = 0; i < DIFFS; i++)
    {
        for (int j = 0; j < CHANNELS; j++)
        {
            diffs[i][j] = (*diff_data)[COLUMNS*ROWS + CHANNELS*i + j];
        }
    }
    
    for (int i = 0; i < ROWS; i++)
    {
        double hl = ranges[i][0];
        double hh = ranges[i][1];
        double sl = ranges[i][2];
        double sh = ranges[i][3];
        double vl = ranges[i][4];
        double vh = ranges[i][5];
        
        if (hl <= h1 && h1 <= hh && hl <= h2 && h2 <= hh &&
            sl <= s1 && s1 <= sh && sl <= s2 && s2 <= sh &&
            vl <= v1 && v1 <= vh && vl <= v2 && v2 <= vh)
        {
            if (hd < diffs[i][0] && sd < diffs[i][1] && vd < diffs[i][2])
            {
                return true;
            }
        }
            
    }
    
//    printf("%f\n", diffs[DIFFS-1][2]);
    
    return hd < diffs[DIFFS-1][0] && sd < diffs[DIFFS-1][1] && vd < diffs[DIFFS-1][2];
    
    
    
#endif
}

void grow_region_2(Image *img, RegionContainer *cont, Point seed, int label, std::vector<double> *diff_data)
{
    const int height = (int)img->dimensions[0];
    const int width = (int)img->dimensions[1];
    
    double avg_diff = 0;
    int avg_diff_pts = 0;
    
    std::queue<Point> q = std::queue<Point>();
    
    q.push(seed);
    
    Region *r_ptr = new Region(label);
    
    Region &r = *r_ptr;
    
    cont->regions_map[LINEAR_INDEX(seed.row,seed.col,width)] = label;
    
    // Steps da adottare per ciclare nei dintorni di un punto (r-->righe, c-->colonne)
    int r_steps[] = {0, 1, 0, -1, 1, -1, 1, -1};
    int c_steps[] =  {1, 0, -1, 0, 1, -1, -1, 1};
    
    // Dice se un certo step si può utilizzare o meno per i contorni (di solito
    // i punti +1 +1, -1 -1, +1 -1, -1 +1 non si usano come contorno per evitare di avere una
    // linea di contorno troppo spessa)
    int boundary_step[] = {1, 1, 1, 1, 0, 0, 0, 0};
    
    const int steps = sizeof(r_steps)/sizeof(int);
    
    while (q.size() > 0)
    {
        Point center = q.front();
        q.pop();
        
        int row = center.row;
        int col = center.col;
        
        // Prendo i colori del punto in esame
        int center_R = (int)GET_RCC(img,row,col,Color::R);
        int center_G = (int)GET_RCC(img,row,col,Color::G);
        int center_B = (int)GET_RCC(img,row,col,Color::B);
        
        // Aggiorno la regione
        r.points->push_back(center); // Appendo il punto corrente ai punti della regione
        r.average[Color::R] += center_R; // Aggiorno la media (fino alla fine del calcolo contiene solo la somma dei colori)
        r.average[Color::G] += center_G;
        r.average[Color::B] += center_B;
        r.centroid[PtArray::ROW] += center.row; // Aggiorno il centroide (fino alla fine del calcolo contiene solo la somma delle posizioni)
        r.centroid[PtArray::COL] += center.col;
        
        // Aggiorno il bounding box
        if (row<r.bounding_box.top_left.row || r.bounding_box.top_left.row==-1)
        {
            r.bounding_box.top_left.row = row;
        }
        if (col<r.bounding_box.top_left.col || r.bounding_box.top_left.col==-1)
        {
            r.bounding_box.top_left.col = col;
        }
        if (row>r.bounding_box.bottom_right.row || r.bounding_box.bottom_right.row==-1)
        {
            r.bounding_box.bottom_right.row = row;
        }
        if (col>r.bounding_box.bottom_right.col || r.bounding_box.bottom_right.col==-1)
        {
            r.bounding_box.bottom_right.col = col;
        }
        
        // Calcolo la media effettiva dato che mi serve per verificare la distanza del punto corrente da essa
        double avg_R = r.average[Color::R]/r.points->size();
        double avg_G = r.average[Color::G]/r.points->size();
        double avg_B = r.average[Color::B]/r.points->size();
        
        bool is_bound = false;
        
        //printf("B %d %d %d %d %f %f %f ", r.points->size(), center_R, center_G, center_B, r.average[Color::R], r.average[Color::G], r.average[Color::B]);
        
        for (int s = 0; s < steps; s++)
        {
            int near_row = center.row + r_steps[s];
            int near_col = center.col + c_steps[s];
            
            if (near_row >= 0 and near_row < height and near_col >= 0 and near_col < width 
                and cont->regions_map[LINEAR_INDEX(near_row, near_col, width)] == NO_REGION)
            {
                int near_R = (int)GET_RCC(img,near_row,near_col,Color::R);
                int near_G = (int)GET_RCC(img,near_row,near_col,Color::G);
                int near_B = (int)GET_RCC(img,near_row,near_col,Color::B);
                
                if (similar(avg_B, avg_G, avg_R, near_B, near_G, near_R, diff_data)){
                    cont->regions_map[LINEAR_INDEX(near_row, near_col, width)] = label;
                    q.push(Point(near_row, near_col));
                } else {
                    // Se il punto ha un colore troppo diverso da uno dei suoi vicini, allora è di contorno
                    is_bound = true;

                }
                
                
            } else if (near_row >= 0 and near_row < height and near_col >= 0 and near_col < width
                      and cont->regions_map[LINEAR_INDEX(near_row, near_col, width)] != label)
            {
                // regione vicina trovata
                
                int near_region = cont->regions_map[LINEAR_INDEX(near_row, near_col, width)];
                
                
                
                // Se il punto confina con un'altra regione allora è un punto di contorno
                if (boundary_step[s] == 1)
                {
                    is_bound = true;
                    
                    // Aggiungo la regione confinante solo se la tocca con un punto di contorno!
                    if (r.near_regions->count(near_region) == 0)
                    {
                        // Regione vicina non ancora aggiunta

                        r.near_regions->insert(near_region);

                        //printf("%d %d\n", label, near_region);

                        Region *near = (*(cont->regions))[near_region];

                        near->near_regions->insert(label);
                    }
                }
                
            } else if (near_row < 0 or near_row >= height or near_col < 0 or near_col >= width) {
                // vicina alla regione 0 (fuori dall'immagine)
                
                if (r.near_regions->count(OUTMOST_REGION) == 0)
                {
                    // Regione vicina non ancora aggiunta
                    
                    r.near_regions->insert(OUTMOST_REGION);
                    
                    //printf("%d %d\n", label, OUTMOST_REGION);
                    
                    Region *near = (*(cont->regions))[OUTMOST_REGION];
                    
                    near->near_regions->insert(label);
                }
                
                // Se il punto confina con la regione 0, allora è di contorno
                if (boundary_step[s] == 1)
                {
                    is_bound = true;
                }
            }
        }
        
        if (is_bound)
        {
            r.boundary_points->push_back(center);
        }
        
        
    }
    
    // Divisione finale per salvare nelle variabili la vera media e il vero centroide
    r.average[Color::R] /= r.points->size();
    r.average[Color::G] /= r.points->size();
    r.average[Color::B] /= r.points->size();
    r.centroid[PtArray::ROW] /= r.points->size();
    r.centroid[PtArray::COL] /= r.points->size();
    
//    printf("AVG_DIFF: %f\n", avg_diff/avg_diff_pts);
    
    cont->regions->push_back(r_ptr);
}

RegionContainer *region_growing_2(Image *img, std::vector<double> *diff_data){
    
    const int height = img->dimensions[0];
    
    const int width = img->dimensions[1];
    
//    const int channels = (int)img->dimensions[3];
    const int channels = img->dimensions[2];
    
    RegionContainer *container = new RegionContainer(height, width);
    
    // Aggiungo la regione esterna
    container->regions->push_back(new Region(OUTMOST_REGION));
    
    int label = 1;
    
    for (int row = 0; row < height; row++)
    {
        for (int col = 0; col < width; col++)
        {
            unsigned char R = GET_RCC(img,row,col,Color::R);
            unsigned char G = GET_RCC(img,row,col,Color::G);
            unsigned char B = GET_RCC(img,row,col,Color::B);
            
            if (container->regions_map[LINEAR_INDEX(row,col,width)] == NO_REGION)
            {
                grow_region_2(img, container, Point(row, col), label, diff_data);
                label++;
            }
        }
        
    }
    
    //printf("%d\n", container->regions->size());
    
    return container;
    
}

