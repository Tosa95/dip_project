/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   atan_table.h
 * Author: davide
 *
 * Created on 20 luglio 2018, 12.44
 */

#ifndef ATAN_TABLE_H
#define ATAN_TABLE_H

namespace atan_table
{
    extern const double K;
    extern const double STEP;
    extern const int LENGTH;
    extern const double TABLE[];
}

#endif /* ATAN_TABLE_H */

