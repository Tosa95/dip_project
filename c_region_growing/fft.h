/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   fft.h
 * Author: davide
 *
 * Created on 4 giugno 2018, 18.32
 */

#ifndef FFT_H
#define FFT_H

#include <complex>
#include <valarray>
#include <vector>

typedef std::complex<double> Complex;
typedef std::valarray<Complex> CArray;

CArray *rfft (std::vector<double> *x);
std::vector<double> *correlation (std::vector<double> *x1, std::vector<double> *x2);
std::vector<double> *correlation (CArray *X1, CArray *X2);

#endif /* FFT_H */

