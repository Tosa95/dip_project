#include "subregions.h"
#include "region_growing.h"
#include <vector>
#include <math.h>
#include <numeric>
#include <algorithm>
#include <limits>
#include "fft.h"
#include "shape_recognition.h"
#include "tabulized_atan.h"
#include <cmath>
#include <Python.h>
#include <numpy/arrayobject.h>

double get_contour_mean(std::vector<double> *contour)
{
    double sum = 0;
    
    for (double s : *(contour))
    {
        sum += s;
    }
    
    return sum / contour->size();
}

double get_distance (double centroid[2], Point p)
{
    double c_row = centroid[0];
    double c_col = centroid[1];
    double p_row = p.row;
    double p_col = p.col;
    
    return sqrtf((c_row-p_row)*(c_row-p_row) + (c_col-p_col)*(c_col-p_col));
}

std::vector<double>* compute_contour_plot_2 (std::vector<Point> *boundary_points, double centroid[2], int num_points, double max_interpolation_angle)
{
    std::vector<double> *result = new std::vector<double>(num_points, -1); 
    double step = (2*M_PI)/(num_points-1);
    // Computes all the angles
    for (int i = 0; i < boundary_points->size(); i++)
    {
        Point pt = boundary_points->at(i);
        double y = pt.row - centroid[0];
        double x = pt.col - centroid[1];
        double angle = tabulized_atan2((double)y,(double)x);
        int index = int(round((angle + M_PI)/step));
        if (result->at(index)==-1)
        {
            result->at(index) = get_distance(centroid, pt);
        }
    } 
    
    return result;
}

std::vector<double>* compute_contour_plot (std::vector<Point> *boundary_points, double centroid[2], int num_points, double max_interpolation_angle)
{
    
    
    std::vector<double> *result = new std::vector<double>(num_points, 0); 
    double *angles = new double[boundary_points->size()];
    
    // Computes all the angles
    for (int i = 0; i < boundary_points->size(); i++)
    {
        Point pt = boundary_points->at(i);
        double y = pt.row - centroid[0];
        double x = pt.col - centroid[1];
        angles[i] = tabulized_atan2((double)y,(double)x);
    }
    
    int *ordered = new int[boundary_points->size()];
    
    std::iota(ordered, ordered + boundary_points->size(), 0);
    
    std::sort(ordered, ordered + boundary_points->size(), [&](int pt1, int pt2){ return angles[pt1]<angles[pt2];});
    
    double step = (2*M_PI)/num_points;
    
    // Currently considered angle from centroid to perimeter
    double curr_angle = -M_PI;
    
    // Current position into the result vector
    int curr_pos_result = 0;
    
    // Current posision into the points vector
    int curr_pos_points = 0;
    
    // Says if the next point is considered an exact match (doesn't need to be interpolated)
    bool matching = true;
    
    // Says if we have a previous matching point
    bool have_prev_matching = false;
    
    // Distance from the centroid of the previous matching point
    double prev_matching_dist = 0;
    
    // Position in the result array of the previous matching point
    int prev_matching_pos_result = 0;
    
    // Angle of the previous matching point
    double prev_matching_angle = -M_PI;
    
    
    // Says if the shape is wrong. If that's the case, the output will be all zeros
    bool wrong = false;
    
    // Do until I haven't examined all points
    while (curr_pos_points < boundary_points-> size())
    {
        //printf("curr_pos_points %d\n", curr_pos_points);
        double current_point_angle = angles[ordered[curr_pos_points]];
        
        // If the current point has an angle higher than the current,
        // it means that I must increment the position inside the result
        if (current_point_angle >= curr_angle) 
        {
            if (matching)
            {
                // If the point (in the result) matches (is the first that comes here after, 
                // which means it will be placed in the result with no interpolation)
                
                matching = false;
                
                double current_dist = get_distance(centroid, boundary_points->at(ordered[curr_pos_points]));
                
                result->at(curr_pos_result) = current_dist;
                
                if (have_prev_matching)
                {
                    if ((curr_angle - prev_matching_angle)<max_interpolation_angle)
                    {
                        // Linear interpolation
                        for (int pos = prev_matching_pos_result + 1; pos<curr_pos_result; pos++)
                        {
                            double interpolated_dist = ((current_dist - prev_matching_dist)/(curr_pos_result - prev_matching_pos_result)) * (pos - prev_matching_pos_result) + prev_matching_dist;
                            result->at(pos) = interpolated_dist;
                        }
                    }
                    else
                    {
                        wrong = true;
                    }
                }
                
                
                prev_matching_dist = current_dist;
                prev_matching_pos_result = curr_pos_result;
                prev_matching_angle = curr_angle;
                have_prev_matching = true;
            }
            
            curr_angle += step;
            curr_pos_result++;
            
            // If I went over the array bounds, i break the cycle
            if (curr_pos_result >= num_points)
            {
                break;
            }
        }
        else
        {
            curr_pos_points++;
            matching = true;
        }
    }
    
    // Interpolating the last points using the first one (the function is circular)
    
    curr_pos_result = num_points-1;
    double current_dist = result->at(0);
    result->at(curr_pos_result) = current_dist;
    
    for (int pos = prev_matching_pos_result + 1; pos<curr_pos_result; pos++)
    {
        double interpolated_dist = ((current_dist - prev_matching_dist)/(curr_pos_result - prev_matching_pos_result)) * (pos - prev_matching_pos_result) + prev_matching_dist;
        result->at(pos) = interpolated_dist;
    }
    
    // Normalization using maximum
    
    double max = *std::max_element(result->begin(), result->end());
    
    for (int i = 0; i < result->size(); i++)
    {
        result->at(i) /= max;
    }
    
    delete[] ordered;
    delete[] angles;
    
    if (!wrong)
    {
        return result;
    }
    else
    {
        delete result;
        return new std::vector<double>(num_points,0);
    }
}

double shape_distance (std::vector<double> *contour1, std::vector<double> *contour2, int offset)
{
    // Computing the distance of the two aligned signals by applying an offset to one of the indices
    int i1 = offset;
    int i2 = 0;
    
    double sum = 0;
    
    while (i2 < contour1->size())
    {
        // Compute square error
        double error = contour1->at(i1) - contour2->at(i2); 
        sum += error * error;
        
        // Increment indexes
        i1++;
        i2++;
        
        // If i'm going after the end, return to the beginning
        if (i1 >= contour1->size())
        {
            i1 = 0;
        }
    }
    
    // No sqrt so it's not an euclidean distance, however, it's ok, we save time!
    return sum;
}

double shape_distance (std::vector<double> *contour1, std::vector<double> *contour2)
{
    // Finds max index in correlation
    std::vector<double> *corr = correlation(contour1, contour2);
    std::vector<double>::iterator max_it = std::max_element(corr->begin(), corr->end());
    int correlation_argmax = std::distance(corr->begin(), max_it);
    delete corr;
    
    // Now we have to align the two signal and compute the difference
    
    // But it's better to simply iterate with an offset
    
    int offset = correlation_argmax;
    clock_t start = clock();
    double res = shape_distance(contour1, contour2, offset);
    clock_t end = clock();
    //printf("Timing shape distance no correlation: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);
    
    return res;
}



double shape_distance (std::vector<double> *contour1, std::vector<double> *contour2, CArray *contour1_fft, CArray *contour2_fft)
{
    // Finds max index in correlation
    // Calls the version of the correaltion that accepts two ffts
    std::vector<double> *corr = correlation(contour1_fft, contour2_fft);
    std::vector<double>::iterator max_it = std::max_element(corr->begin(), corr->end());
    int correlation_argmax = std::distance(corr->begin(), max_it);
    delete corr;
    
    // Now we have to align the two signal and compute the difference
    
    // But it's better to simply iterate with an offset
    
    int offset = correlation_argmax;
    clock_t start = clock();
    double res = shape_distance(contour1, contour2, offset);
    clock_t end = clock();
    //printf("Timing shape distance no correlation: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);
    
    return res;
}

ShapeContoursFFTMap *compute_ffts (ShapeContoursMap * contours)
{
    ShapeContoursFFTMap* result = new ShapeContoursFFTMap();
    for(auto& shape: *contours)
    {
        std::string shape_name = shape.first;
        ShapeContoursFFT *container_of_contours_ffts = new ShapeContoursFFT();
        for(ShapeContour *contour: (*(shape.second)))
        {
            //printf("I'm here\n");
            ShapeContourFFT *single_fft = rfft(contour);
            container_of_contours_ffts->push_back(single_fft);
        }
        result->insert(std::pair<std::string,ShapeContoursFFT*>(shape_name,container_of_contours_ffts));
    }
    return result;
}

ShapeContoursMeansMap *compute_means (ShapeContoursMap * contours)
{
    ShapeContoursMeansMap *result = new ShapeContoursMeansMap();
    
    for(auto& shape: *contours)
    {
        std::string shape_name = shape.first;
        std::vector<double> *container_of_means = new std::vector<double>;
        for(ShapeContour *contour: (*(shape.second)))
        {
            container_of_means->push_back(get_contour_mean(contour));
        }
        result->insert(std::pair<std::string,std::vector<double> *>(shape_name,container_of_means));
    }
    return result;
}

BaseShapesContainer::BaseShapesContainer(ShapeContoursMap* contours)
{
    this->contours = contours;
    this->contours_ffts = compute_ffts(contours);
    this->contours_means = compute_means(contours);
    
    #ifdef MEMORY_BENCHMARK
        membench.incref(BASE_SHAPES_CONTAINER_MEMBENCH_NAME);
    #endif
}

BaseShapesContainer::~BaseShapesContainer()
{
    //Cycles on the fft map
    for (auto& ffts_array : (*this->contours_ffts))
    {
        // Cycles on a group (relative to the same shape) of ffts
        for(auto& one_fft : (*(ffts_array.second)))
        {
            delete one_fft;
        }
        delete ffts_array.second;
    }
    delete this->contours_ffts;
    
    //Cycles on the contours map
    for (auto& contours_array : (*this->contours))
    {
        // Cycles on a group (relative to the same shape) of ffts
        for(auto& one_contour : (*(contours_array.second)))
        {
            delete one_contour;
        }
        delete contours_array.second;
    }
    delete this->contours;
    
    //Cycles on the means map
    for (auto& means_array : (*this->contours_means))
    {
        delete means_array.second;
    }
    delete this->contours_means;
    
    #ifdef MEMORY_BENCHMARK
        membench.decref(BASE_SHAPES_CONTAINER_MEMBENCH_NAME);
    #endif
}

std::pair<std::string,double> get_best_matching_shape(std::vector<double> *contour, BaseShapesContainer *base_shapes, double threshold, double max_diff_threshold = 3, double max_diff_on_mean = 0.1)
{
    CArray *contour_fft = rfft(contour);
    double mean = get_contour_mean(contour);
    
    std::string best = NO_MATCHING_SHAPE;
    
    double best_dist = threshold;
    
    for (auto&shape_pair : *(base_shapes->get_ffts()))
    {
        std::string shape_name = shape_pair.first;
        ShapeContoursFFT *shape_contours_ffts = shape_pair.second;
        ShapeContours *shape_contours = base_shapes->get_contours()->at(shape_name);
        std::vector<double> *shape_contours_means = base_shapes->get_means()->at(shape_name);
        
        for ( int i = 0; i< shape_contours->size(); i++)
        {
            CArray *shape_fft = shape_contours_ffts->at(i);
            ShapeContour *shape_contour = shape_contours->at(i);
            double shape_mean = shape_contours_means->at(i);
            
            if (fabs(shape_mean - mean) < max_diff_on_mean)
            {
            
                double dist = shape_distance(shape_contour, contour, shape_fft, contour_fft);

                if (dist > max_diff_threshold)
                {
                    break;
                }

                if (dist<=best_dist)
                {
                    best_dist = dist;
                    best = shape_name;
    //                printf("Dist: %f, Name: %s\n", dist, shape_name.c_str());
                }
            }
        }
    }
    
    delete contour_fft;
    
    return std::pair<std::string,double>(best,best_dist);
}

ShapeRecognitionResult *recognize_relevant_shapes(RegionContainer *regions, int num_points, 
        BaseShapesContainer *base_shapes, double threshold, double max_diff_threshold, 
        double max_diff_on_mean, int min_bound_points, bool collapse_inner_points)
{
    ShapeRecognitionResult *srr = new ShapeRecognitionResult(threshold);
    
    std::vector<int> *visited = new std::vector<int>(regions->regions->size(), 0);
    
    for (int i : *(regions->ordered_regions))
    {
        Region *r = regions->regions->at(i);
        if (r->boundary_points->size() >= min_bound_points && r->label != 0 && visited->at(r->label) == 0)
        {
            
            visited->at(r->label) = 1;
            if (r->inner_regions->size() > 0)
            {
                Region *collapsed  = collapse_inner_regions(r->label, regions, collapse_inner_points);
                ShapeContour *sc = compute_contour_plot(collapsed->boundary_points, collapsed->centroid, num_points);
                std::pair<std::string,double> match_result = get_best_matching_shape(sc, base_shapes, threshold, max_diff_threshold, max_diff_on_mean);
                delete sc;

                if (match_result.first != NO_MATCHING_SHAPE)
                {
    //                printf("Found: %d\n", r->label);
                    srr->add_shape(collapsed,match_result.first,match_result.second);
                    std::unordered_set<int> *inner = get_all_inner_regions(r->label, regions);
                    for (int inner_index : (*inner))
                    {
                        visited->at(inner_index) = 1;
                    }
                    delete inner;
                } else {
                    delete collapsed;
                }
            }
        }  
    }
    
    delete visited;
    
    return srr;
}

inline static int wrap_index(int index, int length)
{
    if (index < 0)
    {
        return length + index;
    }
    
    if (index >= length)
    {
        return index - length;
    }
    
    return index;
}

ShapeDescriptor::ShapeDescriptor(Region* r, int num_contour_points) {
    
    this->region = r;
    this->num_points = num_contour_points;
    this->contour = 0;
    
//    this->contour = new std::vector<double> ();
}

void ShapeDescriptor::compute_simple_params()
{
    Region *r = this->region;
    
    double bb_perim = (r->bounding_box.bottom_right.col - r->bounding_box.top_left.col)*2 +
                      (r->bounding_box.bottom_right.row - r->bounding_box.top_left.row)*2;
    
    double bb_area = (r->bounding_box.bottom_right.col - r->bounding_box.top_left.col)*
                     (r->bounding_box.bottom_right.row - r->bounding_box.top_left.row);
    
    // Rapporto tra il perimetro del bounding box e quello della forma
    this->perimeter_ratio = bb_perim / r->boundary_points->size();
    
    this->area_perimeter_ratio = r->points_num / r->boundary_points->size();
    
    this->area_ratio = bb_area / r->points_num;
}

void ShapeDescriptor::compute_complex_params()
{
   Region *r = this->region; 
   int num_contour_points = this->num_points;
   
   std::vector<Point> *decimated_points = decimate_points(r->boundary_points, num_contour_points*2);
    
//    printf("dec_point_size: %d\n", decimated_points->size());
    
    this->contour = compute_contour_plot(decimated_points, r->centroid, num_contour_points);
    
    delete decimated_points;
    
//    CArray *contour_fft = rfft(this->contour);
//    std::vector<double> *corr = correlation(contour_fft, contour_fft);
    
    double sum = 0;
    double sum_d1 = 0;
    
    for (int i = 0; i < this->contour->size(); i++)
    {
        sum += (*this->contour)[i];
        sum_d1 += fabs((*this->contour)[i] - (*this->contour)[wrap_index(i-1, this->contour->size())]);
    }
    
    double mean = sum/this->contour->size();
    double d1_mean = sum_d1/this->contour->size();
    
    double err_sum = 0;
    double err_sum_d1 = 0;
    
    for (int i = 0; i < this->contour->size(); i++)
    {
        double d1 = fabs((*this->contour)[i] - (*this->contour)[wrap_index(i-1, this->contour->size())]);
        
        err_sum += pow((*this->contour)[i] - mean, 2);
        err_sum_d1 += pow(d1 - d1_mean, 2);
    }
    
    double std = sqrt(err_sum/this->contour->size());
    double d1_std = sqrt(err_sum_d1/this->contour->size());
    
    this->mean = mean;
    this->std = std;
    this->d1_mean = d1_mean;
    this->d1_std = d1_std;
}

ShapeDescriptorsContainer *get_all_shapes_descriptors(RegionContainer *regions, int num_points, int min_bound_pts)
{
    ShapeDescriptorsContainer *res = new ShapeDescriptorsContainer();
    
    for (int i : *(regions->ordered_regions))
    {
        Region *r = regions->regions->at(i);
        
        if (r->boundary_points->size() >= min_bound_pts)
        {
            Region *collapsed  = collapse_inner_regions(r->label, regions, false);

    //        printf("Qui ci arrivo 2\n");

            ShapeDescriptor *desc = new ShapeDescriptor(collapsed, num_points);
            desc->compute_simple_params();
            desc->compute_complex_params();

    //        printf("Qui ci arrivo 4\n");

            res->get_descriptors()->push_back(desc);
        }
    }
    
    return res;
}

std::vector <Point> *decimate_points(std::vector <Point> *orig_points, int keep_n)
{
    if (orig_points->size() > keep_n)
    {
        double step = ((double)orig_points->size())/keep_n;

        double cnt = step;
        int index = 0;

        std::vector<Point> *res = new std::vector<Point>();

        while (index < orig_points->size())
        {
            res->push_back(orig_points->at(index));

            index += (int)cnt;
            cnt += step - (int)cnt;
        }

        return res;
    }
    else
    {
        return new std::vector<Point>(*orig_points);
    }
}

bool is_relevant (ShapeDescriptor *sd, std::vector<double> *params)
{
    double shape_params[] = {sd->get_perimeter_ratio(), 
                       sd->get_area_perimeter_ratio(), 
                       sd->get_area_ratio()};
    
    int n_pars = sizeof(shape_params)/sizeof(double);
    int genes_per_shape = n_pars*2;
    int n_shapes = params->size()/(genes_per_shape);
    
    for (int j = 0; j < n_shapes; j++)
    {
        bool relevant = true;
        for (int i = 0; i < n_pars; i++)
        {
            double min = params->at(j*genes_per_shape + 2*i);
            double max = params->at(j*genes_per_shape + 2*i + 1);
            
            if (!(min<=shape_params[i] && shape_params[i] <= max))
            {
                relevant = false;
                break;
            }
        }
        if (relevant)
        {
            return true;
        }
    }
    
    return false;
}

ShapeRecognitionResult *recognize_relevant_shapes_2(RegionContainer *regions, int num_points, int min_bound_points, std::vector<double> *params)
{
    ShapeRecognitionResult *srr = new ShapeRecognitionResult(1);
    
    std::vector<int> *visited = new std::vector<int>(regions->regions->size(), 0);
    
    for (int i : *(regions->ordered_regions))
    {
        Region *r = regions->regions->at(i);
        if (r->boundary_points->size() >= min_bound_points && r->label != 0 && visited->at(r->label) == 0)
        {
            
            visited->at(r->label) = 1;
            
            Region *collapsed  = collapse_inner_regions(r->label, regions, false);
            
            ShapeDescriptor *sd = new ShapeDescriptor(collapsed, num_points);
            sd->compute_simple_params();
            
            bool relevant = is_relevant(sd, params);
            
            delete sd;
            
            if (relevant)
            {
                srr->add_shape(r,"relevant",1);
                std::unordered_set<int> *inner = get_all_inner_regions(r->label, regions);
                for (int inner_index : (*inner))
                {
//                    visited->at(inner_index) = 1;
                }
                delete inner;
            }
            
        } 
    }
    
    delete visited;
    
    return srr;
}

void get_principal_region_area(CImage *img, ShapeRecognitionResult *srr, PyObject *out, double accepted_size_ratio){
    
    Region *best_region = NULL;
    double best_dist = -1;
    double height = img->dimensions[0];
    double width = img->dimensions[1];
    
    double center_h = height/2.0;
    double center_w = width/2.0;
    
    for (Region *r : *(srr->get_recognized_regions()))
    {
        double centroid_h = r->centroid[PtArray::ROW];
        double centroid_w = r->centroid[PtArray::COL];
        
        double h_diff = center_h - centroid_h;
        double w_diff = center_w - centroid_w;
        
        double l2_diff = sqrt(h_diff*h_diff + w_diff*w_diff);
        
        if (best_dist == -1 || l2_diff < best_dist)
        {
            best_dist = l2_diff;
            best_region = r;
        }
    }

    PyObject *zero = PyFloat_FromDouble(0.0);
    PyObject *one = PyFloat_FromDouble(1.0);
    PyObject *mone = PyFloat_FromDouble(-1.0);
    
    if (best_region != NULL)
    {
        
        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                PyArray_SETITEM(out, PyArray_GETPTR2(out, row, col), zero);
            }
        }
        for (Point p : *(best_region->points))
        {
            PyArray_SETITEM(out, PyArray_GETPTR2(out, p.row, p.col), one);
        }
    } else {
        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                PyArray_SETITEM(out, PyArray_GETPTR2(out, row, col), mone);
            }
        }
    }
    
    Py_DECREF(zero);
    Py_DECREF(one);
    Py_DECREF(mone); 
}