# -*- coding: utf-8 -*-
#
#	Autore: Tosatto Davide
#

from distutils.core import setup, Extension

module1 = Extension('advanced_region_growing',
                    sources = ["atan_table.cpp", "tabulized_atan.cpp", 'region_growing.cpp', 'region_growing_2.cpp', 'subregions.cpp', 'shape_recognition.cpp', 'python_interface.cpp',
                               'fft.cpp', "python_c_data_converters.cpp", "memory_bench.cpp"],
                    extra_compile_args=["-std=c++11"])

setup (name = 'advanced_region_growing',
       version = '0.1',
       description = 'Implementazione di una versione avanzata dell algoritmo region growing',
       ext_modules = [module1])
