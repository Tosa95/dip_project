/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   tabulized_atan.h
 * Author: davide
 *
 * Created on 20 luglio 2018, 12.53
 */

#ifndef TABULIZED_ATAN_H
#define TABULIZED_ATAN_H

#include "atan_table.h"
#include<cmath>

    inline double tabulized_atan(double x)
    {
        if (x < -atan_table::K)
        {
            return -M_PI/2;
        }

        if (x > atan_table::K)
        {
            return M_PI/2;
        }

        int index = int((x+atan_table::K)/atan_table::STEP);

        return (atan_table::TABLE[index] + atan_table::TABLE[index+1])/2;
    }
    
    inline double tabulized_atan2(double y, double x)
    {
        if (x > 0)
        {
            return tabulized_atan(y/x);
        }
        else if (x<0)
        {
            double u = tabulized_atan(y/x);
            
            if (u>0)
            {
                return  u - M_PI;
            }
            else
            {
                return u + M_PI;
            }
            
        }
        else
        {
            return 0;
        }
    }

#endif /* TABULIZED_ATAN_H */

