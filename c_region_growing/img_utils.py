from math import sqrt
from random import randint

import cv2

def resize(img, megapixels):

    megapixels = megapixels*1000000

    height, width, _ = img.shape

    original_aspect_ratio = float(height)/width

    new_width = sqrt(megapixels/original_aspect_ratio)

    new_height = original_aspect_ratio*new_width

    res = cv2.resize(img, (int(new_width), int(new_height)))

    return res, new_width/width, new_height/height

def print_relevant_shapes (image, relevant):

    for shape, label in relevant:

        bb = shape.get_bounding_box()

        color = (randint(50, 255), randint(50, 255), randint(50, 255))

        cv2.rectangle(image, (bb[1], bb[0]), (bb[3], bb[2]), color, 1)

        font = cv2.FONT_HERSHEY_COMPLEX_SMALL

        cv2.putText(image, label, (bb[1], bb[2]), font, 0.7, color, 1, cv2.LINE_AA)