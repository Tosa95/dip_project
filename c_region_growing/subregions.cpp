/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <deque>  
#include <queue>

#include "region_growing.h"
#include "subregions.h"

const int WHITE = 0;
const int GREY = 1;
const int BLACK = 2;

bool is_bfs_ancestor (int ancestor_region_index, int region_index, RegionContainer *regions)
{
    // We assume a node to be an ancestor of itself
    if (region_index == ancestor_region_index)
    {
        return true;
    }
    
    // If we reach region zero and our supposed ancestor region is not zero, 
    // we are sure ancestor_region_index is not an ancestor of region_index
    if (region_index == OUTMOST_REGION)
    {
        return false;
    }
    
    // Get the region in order to obtain the father
    Region *region = regions->regions->at(region_index);
    
    return is_bfs_ancestor(ancestor_region_index, region->bfs_father, regions);
}

void bfs (RegionContainer *regions)
{
    int regions_num = regions->regions->size(); 
    std::vector<int> *colors = new std::vector<int>(regions_num, WHITE);
    std::queue<int> *to_visit = new std::queue<int>();
    
    // Visits regions starting from the first one
    to_visit->push(0);
    
    // It mustn't be deallocated at end: has to be returned!
    std::vector<int> *ordered = regions->ordered_regions;
    
    Region *current = regions->regions->at(0);
    
    current->distance = 0;
    
    colors->at(0) = GREY;
    
    while (!to_visit -> empty())
    {
        int current_index = to_visit->front();
        to_visit->pop();
        current = regions->regions->at(current_index);
        
        ordered->push_back(current_index);
        
        int curr_dist = current->distance;
        
        // We have to dereference near regions before being able to iterate on it!
        for (int neigh_index : *(current->near_regions))
        {
            if (colors->at(neigh_index) == WHITE)
            {
                colors->at(neigh_index) = GREY;
                
                current->bfs_children->insert(neigh_index);
                
                Region *neigh = regions->regions->at(neigh_index);
                neigh->bfs_father = current_index;
                neigh->distance = curr_dist + 1;
                
                to_visit->push(neigh_index);
            }
        }
        
        colors->at(current_index) = BLACK;
    }
    
    delete to_visit;
    delete colors;
}

void find_super_region(int first_region_index, RegionContainer *regions, std::vector<int> *colors)
{
    std::queue<int> *to_visit = new std::queue<int>();
    
    // Visits regions starting from the first one
    to_visit->push(first_region_index);
    
    Region *current = regions->regions->at(first_region_index);
    int dist = current->distance;
    
    // Contains (the indices of) all regions that are candidate to have the same super region
    std::vector<int> *neighbours = new std::vector<int>();
    
    // Contains all regions that are nearer to the outmost region than the current considered distance 
    // that are adjacent to one or more of the region in neighbours
    std::unordered_set<int> *lower_distance_regions = new std::unordered_set<int>;
    
    std::vector<int> *colors_for_visiting = new std::vector<int>(regions->regions->size(), WHITE);
    
    while (true)
    {
        
        while (to_visit->size() > 0)
        {
//            printf("internal: %d\n", dist);
            
            int current_index = to_visit->front();
            to_visit->pop();
            
            if (colors->at(current_index) == WHITE)
            {
                neighbours->push_back(current_index);
            }
            
            colors->at(current_index) = BLACK;
            
            Region *current = regions->regions->at(current_index);
            
            
            
            for (int neigh_index : (*current->near_regions))
            {
                
                Region *neigh = regions->regions->at(neigh_index);
                if (colors_for_visiting->at(neigh_index) == WHITE && neigh->distance == dist)
                {
                    to_visit->push(neigh_index);
                    colors_for_visiting->at(neigh_index) = BLACK;
                }
                // if neigh is nearer to the outmost region than current distance and is not in the lower distance regions 
                else if (neigh->distance < dist && lower_distance_regions->count(neigh_index) == 0) 
                {
                    lower_distance_regions->insert(neigh_index);
                }
            }
            
        }
        
//        printf("%d\n", dist);
        
        // If I have only one region of lower distance that is adjacent to all neighbours, than it is the super region
        if (lower_distance_regions->size() == 1)
        {
            // the only element of the set is the super region (index)
            int super_region_index = *(lower_distance_regions->begin());
            Region *super_region = regions-> regions->at(super_region_index);
            
            // Adds all neighbours as inner areas of super area and set the super
            // area of nighbours to super area
            for (int neigh_index : (*neighbours))
            {
                super_region->inner_regions->insert(neigh_index);
                
                Region *neigh = regions->regions->at(neigh_index);
                
                neigh->super_region = super_region_index;
            }
            
            delete lower_distance_regions;
            break;
        }
        else // We haven't found the super region, so we need to repeat the algorithm starting from lower distance areas
        {
            //Adds all lower distance areas to the queue in order to restart from them
            for(int lower_dist_index : (*lower_distance_regions))
            {
                to_visit->push(lower_dist_index);
            }
            
            delete lower_distance_regions;
            lower_distance_regions = new std::unordered_set<int>;
            
            // I must continue with decreased distance because the current is not 
            // sufficient to find a super region
            dist--;
            
            // I must be able to pass again on the regions
            delete colors_for_visiting;
            colors_for_visiting = new std::vector<int>(regions->regions->size(), WHITE);
        }
        
        
        
    }
    
    
    delete colors_for_visiting;
    delete neighbours;
    delete to_visit;
}

void subregions(RegionContainer *regions)
{
    int regions_num = regions->regions->size();
    std::vector<int> *ordered = regions->ordered_regions;
    std::vector<int> *colors = new std::vector<int>(regions_num, WHITE);
    
    // Must iterate in reverse order in order to start from further regions
    for (std::vector<int>::reverse_iterator i = ordered->rbegin(); i != ordered->rend(); i++ )
    {
        int curr_index = *(i);
        
        // The outmost hasn't got a super area, the algorithm wouldn't terminate!
        if (curr_index != OUTMOST_REGION)
        {
            //printf("%d\n", curr_index);
            if (colors->at(curr_index) == WHITE)
            {
                find_super_region(curr_index, regions, colors);
            }
        }
    }
    
    delete colors;
}

std::unordered_set<int> *get_all_inner_regions (int region, RegionContainer *regions)
{
    std::queue<int> *to_visit = new std::queue<int>();
    to_visit->push(region);
    
    std::unordered_set<int> *result = new std::unordered_set<int>();
    
    while (to_visit->size()>0)
    {
        int current_index = to_visit->front();
        to_visit->pop();
        result->insert(current_index);
        Region *current = regions->regions->at(current_index);
        
        for (int inner : (*current->inner_regions))
        {
            to_visit->push(inner);
        }
    }
    
    delete to_visit;
    
    return result;
}

bool is_bound (Point p, RegionContainer *regions, std::unordered_set<int> *inner_regions)
{
    int r_steps[] = {0, 1, 0, -1};
    int c_steps[] =  {1, 0, -1, 0};
    int width = regions->width;
    int height = regions->height;
    
    const int steps = sizeof(r_steps)/sizeof(int);
    
    // Cycle that goes through the neighbours points of the given point in
    // order to get the regions touched by the point
    for (int s = 0; s < steps; s++)
    {
        int r_step = r_steps[s];
        int c_step = c_steps[s];
        
        int current_row = p.row + r_step;
        int current_col = p.col + c_step;
        
        if (current_row>=0 && current_row<height && current_col>=0 && current_col<width)
        {
            int touched = regions->regions_map[LINEAR_INDEX(current_row, current_col, width)];
            
            // If it touches a region that is not inner, it's a bound point!
            if (inner_regions->count(touched)==0)
            {
                return true;
            }
        }
        else // If the indices go outside, we are touching the outmost region and it can't be inner
        {
            return true;
        }
    }
    
    return false;
}

Region *collapse_inner_regions(int region_index, RegionContainer *regions, bool collapse_internal_points)
{
    
    //printf("Region index: %d\n", region_index);
    
    Region *original_region = regions->regions->at(region_index);
    Region *result = new Region(original_region->label);
    
    result->bounding_box = original_region->bounding_box;
    result->points_num = original_region->points->size();
    
    result->average[0] = original_region->average[0] * original_region->points->size();
    result->average[1] = original_region->average[1] * original_region->points->size();
    result->average[2] = original_region->average[2] * original_region->points->size();
    result->centroid[0] = original_region->centroid[0] * original_region->points->size();
    result->centroid[1] = original_region->centroid[1] * original_region->points->size();
    result->super_region = original_region->super_region;
    result->distance = original_region->distance;
    result->points = new std::vector<Point>((*original_region->points));     
    
    std::unordered_set<int> *inner_regions = get_all_inner_regions(region_index, regions);
    
    for (int inner_index : (*inner_regions))
    {
        Region *inner = regions->regions->at(inner_index);
        
        if (collapse_internal_points)
        {

            // Insert all points of the inner region inside the super one
            result->points->insert(result->points->begin(), inner->points->begin(), inner->points->end());

            // Adds in order to compute real average and centroid at the end
            result->average[0] += inner->average[0] * inner->points->size();
            result->average[1] += inner->average[1] * inner->points->size();
            result->average[2] += inner->average[2] * inner->points->size();
            result->centroid[0] += inner->centroid[0] * inner->points->size();
            result->centroid[1] += inner->centroid[1] * inner->points->size();
        }
        
        result->points_num += inner->points->size();

    }
    
    // Cycle to add only real contour points
    for (Point p : (*original_region->boundary_points))
    {
        if (is_bound(p,regions,inner_regions))
        {
            result->boundary_points->push_back(p);
        }
    }
    
    // Computes the average
    result->average[0] /= result->points->size();
    result->average[1] /= result->points->size();
    result->average[2] /= result->points->size();
    result->centroid[0] /= result->points->size();
    result->centroid[1] /= result->points->size();
    
    delete inner_regions;
    
    return result;
}