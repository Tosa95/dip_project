/*
*   Autori: Tosatto Davide, Grespan Riccardo
*
*   Wrapper del modulo di controllo del sensore di prossimita attraverso le
*   primitive di interoperabilità Python-C
*
*/

#include <Python.h>
#include <numpy/arrayobject.h>

static PyObject * advanced_region_growing_test (PyObject *self, PyObject *args);

// Tabella delle funzioni esportate
static PyMethodDef module_methods[] = {
    {"test", advanced_region_growing_test, METH_VARARGS, "Ritorna la distanza"},
    // Terminatore
    {NULL, NULL, 0, NULL}
};

//Funzione che inizializza il modulo. Obbligatoria per permettere il funzionamento dello stesso
PyMODINIT_FUNC
initadvanced_region_growing(void)
{
    PyObject *m = Py_InitModule3("advanced_region_growing", module_methods, "Modulo che implementa una versione avanzata dell'algoritmo region growing");
    if (m == NULL)
        return;
}


//----------FUNZIONI ESPORTATE-------------------

static PyObject * advanced_region_growing_test (PyObject *self, PyObject *args)
{
    PyArrayObject *arg1=0;
    unsigned char *img=0;
    
    if(!PyArg_ParseTuple(args, "O", &arg1))
    {
        printf("ERROR PARSE!");
    }
    
    printf("%d\n", arg1->descr->alignment);
    
    const int height = (int)arg1->dimensions[0];
    const int width = (int)arg1->dimensions[1];
    const int channels = (int)arg1->dimensions[3];
    const int row_length = width*channels;
    
    img = (unsigned char *)arg1->data;
    
    for (int row = 0; row < height; row++)
    {
        for (int col = 0; col < width; col++)
        {
            unsigned char R = img[row*row_length + col*channels + 0];
            unsigned char G = img[row*row_length + col*channels + 1];
            unsigned char B = img[row*row_length + col*channels + 2];
            
            printf("Color at [%d][%d]: (%d,%d,%d)\n", row, col, R, G, B);
        }
    }
    
    return PyInt_FromLong(0);
}



