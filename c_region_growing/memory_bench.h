/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   memory_bench.h
 * Author: root
 *
 * Created on May 4, 2019, 12:29 PM
 */

#ifndef MEMORY_BENCH_H
    #define MEMORY_BENCH_H

//    #define MEMORY_BENCHMARK

    #include <map>
    #include <string>

    class MemoryBenchmark{
        std::map<std::string,long> refcount;

    public:

        void incref(const std::string &name)
        {
            if (refcount.count(name) == 0)
            {
                refcount[name] = 1;
            } else {
                refcount[name]++;
            }
        }

        void decref(const std::string &name)
        {
            if (refcount.count(name) == 0)
            {
                refcount[name] = -1;
            } else {
                refcount[name]--;
            }
        }

        long getref(const std::string &name)
        {
            return refcount[name];
        }
        
        void print_deficits()
        {
            printf("Deficits: \n");
            for (std::pair<const std::string,long>& x : refcount)
            {
                printf("\t-%s deficit: %ld\n", x.first.c_str(), x.second);
            }
        }
    };

    extern MemoryBenchmark membench;

    const std::string REGION_MEMBENCH_NAME = "Region";
    const std::string POINT_MEMBENCH_NAME = "Point";
    const std::string BOUNDING_BOX_MEMBENCH_NAME = "BoundingBox";
    const std::string REGION_CONTAINER_MEMBENCH_NAME = "RegionContainer";
    const std::string C_IMAGE_MEMBENCH_NAME = "CImage";
    const std::string BASE_SHAPES_CONTAINER_MEMBENCH_NAME = "BaseShapesContainer";
    const std::string SHAPE_RECOGNITION_RESULT_MEMBENCH_NAME = "ShapeRecognitionResult";

#endif /* MEMORY_BENCH_H */

