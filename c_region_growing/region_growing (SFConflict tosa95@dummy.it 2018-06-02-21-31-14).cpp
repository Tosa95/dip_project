/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "region_growing.h"
#include "colors.h"
#include <queue>
#include <stdlib.h>
#include <math.h>

Region::Region(int label) {
    
    this->label = label;
    points = new std::vector<Point>();
    boundary_points = new std::vector<Point>();
    near_regions = new std::unordered_set<int>();
    average[0] = average[1] = average[2] = 0;
    centroid[0] = centroid[1] = 0;
    bounding_box = BoundingBox(Point(-1,-1), Point(-1,-1));
    
    bfs_father = NO_FATHER;
    bfs_children = new std::unordered_set<int>();
    
    super_region = NO_REGION;
    inner_regions = new std::unordered_set<int>();
}

Region::~Region() {
    
    delete points;
    
    delete boundary_points;
    
    delete near_regions;
    
    delete bfs_children;
    
    delete inner_regions;
}

RegionContainer::RegionContainer(int height, int width) {
    regions = new std::vector<Region*>();
    ordered_regions = new std::vector<int>();
    regions_map = new int[height*width];
    
    for(int row = 0; row < height; row++)
    {
        for (int col = 0; col < width; col++)
        {
            regions_map[LINEAR_INDEX(row,col,width)] = NO_REGION;
        }
    }
}


RegionContainer::~RegionContainer()
{
    for (const auto* region: *regions)
    {
        delete region;
    } 
    delete regions;
    delete ordered_regions;
    delete regions_map;
}



void grow_region(Image *img, RegionContainer *cont, Point seed, int label, double threshold)
{
    const int height = (int)img->dimensions[0];
    const int width = (int)img->dimensions[1];
    
    std::queue<Point> q = std::queue<Point>();
    
    q.push(seed);
    
    Region *r_ptr = new Region(label);
    
    Region &r = *r_ptr;
    
    cont->regions_map[LINEAR_INDEX(seed.row,seed.col,width)] = label;
    
    // Steps da adottare per ciclare nei dintorni di un punto (r-->righe, c-->colonne)
    int r_steps[] = {0, 1, 0, -1, 1, -1, 1, -1};
    int c_steps[] =  {1, 0, -1, 0, 1, -1, -1, 1};
    
    // Dice se un certo step si può utilizzare o meno per i contorni (di solito
    // i punti +1 +1, -1 -1, +1 -1, -1 +1 non si usano come contorno per evitare di avere una
    // linea di contorno troppo spessa)
    int boundary_step[] = {1, 1, 1, 1, 0, 0, 0, 0};
    
    const int steps = sizeof(r_steps)/sizeof(int);
    
    while (q.size() > 0)
    {
        Point center = q.front();
        q.pop();
        
        int row = center.row;
        int col = center.col;
        
        // Prendo i colori del punto in esame
        int center_R = (int)GET_RCC(img,row,col,Color::R);
        int center_G = (int)GET_RCC(img,row,col,Color::G);
        int center_B = (int)GET_RCC(img,row,col,Color::B);
        
        // Aggiorno la regione
        r.points->push_back(center); // Appendo il punto corrente ai punti della regione
        r.average[Color::R] += center_R; // Aggiorno la media (fino alla fine del calcolo contiene solo la somma dei colori)
        r.average[Color::G] += center_G;
        r.average[Color::B] += center_B;
        r.centroid[PtArray::ROW] += center.row; // Aggiorno il centroide (fino alla fine del calcolo contiene solo la somma delle posizioni)
        r.centroid[PtArray::COL] += center.col;
        
        // Aggiorno il bounding box
        if (row<r.bounding_box.top_left.row || r.bounding_box.top_left.row==-1)
        {
            r.bounding_box.top_left.row = row;
        }
        if (col<r.bounding_box.top_left.col || r.bounding_box.top_left.col==-1)
        {
            r.bounding_box.top_left.col = col;
        }
        if (row>r.bounding_box.bottom_right.row || r.bounding_box.bottom_right.row==-1)
        {
            r.bounding_box.bottom_right.row = row;
        }
        if (col>r.bounding_box.bottom_right.col || r.bounding_box.bottom_right.col==-1)
        {
            r.bounding_box.bottom_right.col = col;
        }
        
        // Calcolo la media effettiva dato che mi serve per verificare la distanza del punto corrente da essa
        double avg_R = r.average[Color::R]/r.points->size();
        double avg_G = r.average[Color::G]/r.points->size();
        double avg_B = r.average[Color::B]/r.points->size();
        
        bool is_bound = false;
        
        //printf("B %d %d %d %d %f %f %f ", r.points->size(), center_R, center_G, center_B, r.average[Color::R], r.average[Color::G], r.average[Color::B]);
        
        for (int s = 0; s < steps; s++)
        {
            int near_row = center.row + r_steps[s];
            int near_col = center.col + c_steps[s];
            
            if (near_row >= 0 and near_row < height and near_col >= 0 and near_col < width 
                and cont->regions_map[LINEAR_INDEX(near_row, near_col, width)] == NO_REGION)
            {
                int near_R = (int)GET_RCC(img,near_row,near_col,Color::R);
                int near_G = (int)GET_RCC(img,near_row,near_col,Color::G);
                int near_B = (int)GET_RCC(img,near_row,near_col,Color::B);
                
                double dist = fabs(avg_R-near_R) + fabs(avg_G-near_G) + fabs(avg_B-near_B);
                
                //printf("%ld %f %f %f %f %d %d %d x:%d y:%d\n", r.points->size(), avg_R, avg_G, avg_B, dist, near_R, near_G, near_B, near_col, near_row);
                
                if (dist < threshold){
                    cont->regions_map[LINEAR_INDEX(near_row, near_col, width)] = label;
                    q.push(Point(near_row, near_col));
                } else {
                    // Se il punto ha un colore troppo diverso da uno dei suoi vicini, allora è di contorno
                    is_bound = true;

                }
                
                
            } else if (near_row >= 0 and near_row < height and near_col >= 0 and near_col < width
                      and cont->regions_map[LINEAR_INDEX(near_row, near_col, width)] != label)
            {
                // regione vicina trovata
                
                int near_region = cont->regions_map[LINEAR_INDEX(near_row, near_col, width)];
                
                if (r.near_regions->count(near_region) == 0)
                {
                    // Regione vicina non ancora aggiunta
                    
                    r.near_regions->insert(near_region);
                    
                    //printf("%d %d\n", label, near_region);
                    
                    Region *near = (*(cont->regions))[near_region];
                    
                    near->near_regions->insert(label);
                }
                
                // Se il punto confina con un'altra regione allora è un punto di contorno
                if (boundary_step[s] == 1)
                {
                    is_bound = true;
                }
                
            } else if (near_row < 0 or near_row >= height or near_col < 0 or near_col >= width) {
                // vicina alla regione 0 (fuori dall'immagine)
                
                if (r.near_regions->count(OUTMOST_REGION) == 0)
                {
                    // Regione vicina non ancora aggiunta
                    
                    r.near_regions->insert(OUTMOST_REGION);
                    
                    //printf("%d %d\n", label, OUTMOST_REGION);
                    
                    Region *near = (*(cont->regions))[OUTMOST_REGION];
                    
                    near->near_regions->insert(label);
                }
                
                // Se il punto confina con la regione 0, allora è di contorno
                if (boundary_step[s] == 1)
                {
                    is_bound = true;
                }
            }
        }
        
        if (is_bound)
        {
            r.boundary_points->push_back(center);
        }
        
        
    }
    
    // Divisione finale per salvare nelle variabili la vera media e il vero centroide
    r.average[Color::R] /= r.points->size();
    r.average[Color::G] /= r.points->size();
    r.average[Color::B] /= r.points->size();
    r.centroid[PtArray::ROW] /= r.points->size();
    r.centroid[PtArray::COL] /= r.points->size();
    
    cont->regions->push_back(r_ptr);
}

RegionContainer *region_growing(Image *img, double threshold){
    
    const int height = (int)img->dimensions[0];
    const int width = (int)img->dimensions[1];
    const int channels = (int)img->dimensions[3];
    
    RegionContainer *container = new RegionContainer(height, width);
    
    // Aggiungo la regione esterna
    container->regions->push_back(new Region(OUTMOST_REGION));
    
    int label = 1;
    
    for (int row = 0; row < height; row++)
    {
        for (int col = 0; col < width; col++)
        {
            unsigned char R = GET_RCC(img,row,col,Color::R);
            unsigned char G = GET_RCC(img,row,col,Color::G);
            unsigned char B = GET_RCC(img,row,col,Color::B);
            
            if (container->regions_map[LINEAR_INDEX(row,col,width)] == NO_REGION)
            {
                grow_region(img, container, Point(row, col), label, threshold);
                label++;
            }
        }
        
    }
    
    //printf("%d\n", container->regions->size());
    
    return container;
    
}