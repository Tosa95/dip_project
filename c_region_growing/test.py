import advanced_region_growing
import cv2
import datetime
import numpy as np
import img_utils
import json

test_img, _, _ = img_utils.resize(cv2.imread("pericolo1.jpg").astype(np.double), 0.05)
#test_img = cv2.imread("test_image_3_areas.png").astype(np.uint8)

test_img = cv2.GaussianBlur(test_img.astype(np.double), (5, 5), 0)


begin = datetime.datetime.now()
res = advanced_region_growing.region_growing(test_img.astype(np.uint8), 70)
after_cpp = datetime.datetime.now()
print "\n\nPYTHON!!!\n\n"

height, width, _ = test_img.shape

j = 0.0

for row in xrange(height):
    for col in xrange(width):
        j += int(test_img[row][col][0]) + int(test_img[row][col][1]) + int(test_img[row][col][2])
        #print "Color at [%d][%d]: (%d,%d,%d)" % (row, col,test_img[row][col][0] , test_img[row][col][1], test_img[row][col][2])

after_python = datetime.datetime.now()

#print res, float(j)/(height*width*3)
#print res["map"]
print "CPP: %s, PYTH; %s" % (after_cpp - begin, after_python - after_cpp)