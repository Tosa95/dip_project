/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   shape_recognition.h
 * Author: davide
 *
 * Created on 3 giugno 2018, 15.30
 */

#ifndef SHAPE_RECOGNITION_H
#define SHAPE_RECOGNITION_H

#include <string>
#include <vector>
#include <unordered_map>
#include "fft.h"
#include "region_growing.h"

#include <Python.h>
#include <numpy/arrayobject.h>

#include "memory_bench.h"

std::vector<double>* compute_contour_plot (std::vector<Point> *boundary_points, double centroid[2], int num_points, double max_interpolation_angle = M_PI/6);
std::vector<double>* compute_contour_plot_2 (std::vector<Point> *boundary_points, double centroid[2], int num_points, double max_interpolation_angle = M_PI/6);
double shape_distance (std::vector<double> *contour1, std::vector<double> *contour2);

const std::string NO_MATCHING_SHAPE ("no_shape");

typedef std::vector<double> ShapeContour;
typedef std::vector<ShapeContour*> ShapeContours;
typedef std::unordered_map<std::string,ShapeContours*> ShapeContoursMap;

typedef CArray ShapeContourFFT;
typedef std::vector<ShapeContourFFT*> ShapeContoursFFT;
typedef std::unordered_map<std::string,ShapeContoursFFT*> ShapeContoursFFTMap;
typedef std::unordered_map<std::string,std::vector<double>*> ShapeContoursMeansMap;

class BaseShapesContainer
{
    ShapeContoursMap *contours;
    ShapeContoursFFTMap *contours_ffts;
    ShapeContoursMeansMap *contours_means;
    
public:
    
    BaseShapesContainer(ShapeContoursMap *contours);
    ~BaseShapesContainer();
    ShapeContoursFFTMap *get_ffts()
    {
        return this->contours_ffts;
    }
    ShapeContoursMap *get_contours()
    {
        return this->contours;
    }
    ShapeContoursMeansMap *get_means()
    {
        return this->contours_means;
    }
};


struct ShapeRecognized{
    BoundingBox bb;
    std::string shape_name;
    double distance_normalized;
    
    ShapeRecognized(BoundingBox bb, std::string shape_name, double distance_normalized)
    {
        this->bb = bb;
        this->shape_name = shape_name;
        this->distance_normalized = distance_normalized;
    }
};

class ShapeRecognitionResult
{
    std::vector<ShapeRecognized> *data;
    std::vector<Region*> *regions;
    double threshold;
    
public:
    
    ShapeRecognitionResult(double threshold)
    {
        this->data = new std::vector<ShapeRecognized>();
        this->regions = new std::vector<Region *>();
        this->threshold = threshold;
        #ifdef MEMORY_BENCHMARK
            membench.incref(SHAPE_RECOGNITION_RESULT_MEMBENCH_NAME);
        #endif
    }
    
    ~ShapeRecognitionResult()
    {
        delete this->data;
        
        for (Region * r : (*this->regions))
        {
            delete r;
        }
        
        delete this->regions;
        
        #ifdef MEMORY_BENCHMARK
            membench.decref(SHAPE_RECOGNITION_RESULT_MEMBENCH_NAME);
        #endif
    }
    
    void add_shape (Region *r, std::string shape_name, double dist)
    {
        double normalized_dist = dist/this->threshold;
        
        this->data->push_back(ShapeRecognized(r->bounding_box, shape_name, normalized_dist));
        this->regions->push_back(r);
    }
    
    std::vector<ShapeRecognized> *get_recognized_shapes()
    {
        return this->data;
    }
    
    std::vector<Region *> *get_recognized_regions(){
        return this->regions;
    }
    
    int get_num_shapes()
    {
        return this->data->size();
    }
    
};

ShapeRecognitionResult *recognize_relevant_shapes(RegionContainer *regions, 
        int num_points, BaseShapesContainer *base_shapes, double threshold, 
        double max_diff_threshold, double max_diff_on_mean, int min_bound_points,
        bool collapse_inner_points = false);

class ShapeDescriptor{
    ShapeContour *contour;
    Region* region;
    double mean;
    double std;
    double d1_mean;
    double d1_std;
    double perimeter_ratio;
    double area_ratio;
    double area_perimeter_ratio;
    int num_points;
    
public:
    
    ShapeDescriptor(Region *r, int num_contour_points);
    
    ~ShapeDescriptor()
    {
        if (this->contour != 0)
        {
            delete this->contour;
        }
        delete this->region;
    }
    
    void compute_simple_params();
    
    void compute_complex_params();
    
    ShapeContour *get_contour()
    {
        return this->contour;
    }
    
    double get_mean()
    {
        return this->mean;
    }
    
    double get_std()
    {
        return this->std;
    }
    
    double get_d1_mean()
    {
        return this->d1_mean;
    }
    
    double get_d1_std()
    {
        return this->d1_std;
    }
    
    double get_perimeter_ratio()
    {
        return this->perimeter_ratio;
    }
    
    double get_area_perimeter_ratio()
    {
        return this->area_perimeter_ratio;
    }
    
    double get_area_ratio()
    {
        return this->area_ratio;
    }
    
    Region* get_region()
    {
        return this->region;
    }
};

class ShapeDescriptorsContainer
{
    std::vector<ShapeDescriptor*> *descriptors;
    
public:
    
    ShapeDescriptorsContainer()
    {
        this->descriptors = new std::vector<ShapeDescriptor*>();
    }
    
    ~ShapeDescriptorsContainer()
    {
        for (ShapeDescriptor *d : (*this->descriptors))
        {
            delete d;
        }
        
        delete this->descriptors;
    }
    
    std::vector<ShapeDescriptor *> *get_descriptors()
    {
        return this->descriptors;
    }
};

ShapeDescriptorsContainer *get_all_shapes_descriptors(RegionContainer *regions, int num_points, int min_bound_pts);

std::vector <Point> *decimate_points(std::vector <Point> *orig_points, int keep_n);

bool is_relevant (ShapeDescriptor *sd, std::vector<double> *params);

ShapeRecognitionResult *recognize_relevant_shapes_2(RegionContainer *regions, int num_points, int min_bound_points, std::vector<double> *params);

/*
 * Returns a numpy array that contains 1.0 in the pixels of the principal region and 0.0 in the other ones.
 * 
 * If no principal region has been found, it will return null.
 * 
 * For principal region selection will be first chosen the one whose centroid is nearest to the center of the image
 * and then it will be checked if it is sufficiently large using the accepted_size_ratio parameter: if the average of the ratio 
 * btw area height and image height and area width and image width falls below that parameter, the shape won't be accepted.
 * 
 * IMPORTANT: not thread safe, must have GIL enabled.
 */
void get_principal_region_area(CImage *img, ShapeRecognitionResult *srr, PyObject *out, double accepted_size_ratio = 0.6);

#endif /* SHAPE_RECOGNITION_H */

