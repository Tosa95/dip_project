/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   subregions.h
 * Author: davide
 *
 * Created on 2 giugno 2018, 16.03
 */

#ifndef SUBREGIONS_H
#define SUBREGIONS_H

#include "region_growing.h"

void bfs (RegionContainer *regions);
void subregions(RegionContainer *regions);
std::unordered_set<int> *get_all_inner_regions (int region, RegionContainer *regions);
bool is_bound (Point p, RegionContainer *regions, std::unordered_set<int> *inner_regions);
Region *collapse_inner_regions(int region_index, RegionContainer *regions, bool collapse_internal_points);
std::unordered_set<int> *get_all_inner_regions (int region, RegionContainer *regions);

#endif /* SUBREGIONS_H */

