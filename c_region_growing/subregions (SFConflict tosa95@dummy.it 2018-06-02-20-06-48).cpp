/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <deque>
#include <queue>

#include "region_growing.h"
#include "subregions.h"

const int WHITE = 0;
const int GREY = 1;
const int BLACK = 2;

bool is_bfs_ancestor (int ancestor_region_index, int region_index, RegionContainer *regions)
{
    // We assume a node to be an ancestor of itself
    if (region_index == ancestor_region_index)
    {
        return true;
    }
    
    // If we reach region zero and our supposed ancestor region is not zero, 
    // we are sure ancestor_region_index is not an ancestor of region_index
    if (region_index == OUTMOST_REGION)
    {
        return false;
    }
    
    // Get the region in order to obtain the father
    Region *region = regions->regions->at(region_index);
    
    return is_bfs_ancestor(ancestor_region_index, region->bfs_father, regions);
}

void bfs (RegionContainer *regions)
{
    int regions_num = regions->regions->size(); 
    std::vector<int> *colors = new std::vector<int>(regions_num, WHITE);
    std::queue<int> *to_visit = new std::queue<int>();
    
    // Visits regions starting from the first one
    to_visit->push(0);
    
    // It mustn't be deallocated at end: has to be returned!
    std::vector<int> *ordered = regions->ordered_regions;
    
    Region *current = regions->regions->at(0);
    
    current->distance = 0;
    
    colors->at(0) = GREY;
    
    while (!to_visit -> empty())
    {
        int current_index = to_visit->front();
        to_visit->pop();
        current = regions->regions->at(current_index);
        
        ordered->push_back(current_index);
        
        int curr_dist = current->distance;
        
        // We have to dereference near regions before being able to iterate on it!
        for (int neigh_index : *(current->near_regions))
        {
            if (colors->at(neigh_index) == WHITE)
            {
                colors->at(neigh_index) = GREY;
                
                current->bfs_children->insert(neigh_index);
                
                Region *neigh = regions->regions->at(neigh_index);
                neigh->bfs_father = current_index;
                neigh->distance = curr_dist + 1;
                
                to_visit->push(neigh_index);
            }
        }
        
        colors->at(current_index) = BLACK;
    }
    
    delete to_visit;
    delete colors;
}

void find_super_region(int first_region_index, RegionContainer *regions, std::vector<int> *colors)
{
    std::queue<int> *to_visit = new std::queue<int>();
    
    // Visits regions starting from the first one
    to_visit->push(first_region_index);
    
    Region *current = regions->regions->at(first_region_index);
    int dist = current->distance;
    
    // Contains (the indices of) all regions that are candidate to have the same super region
    std::vector<int> *neighbours = new std::vector<int>();
    
    // Contains all regions that are nearer to the outmost region than the current considered distance 
    // that are adjacent to one or more of the region in neighbours
    std::unordered_set<int> *lower_distance_regions = new std::unordered_set<int>;
    
    while (true)
    {
        while (to_visit->size() > 0)
        {
            int current_index = to_visit->front();
            to_visit->pop();
            
            if (colors->at(current_index) == WHITE)
            {
                neighbours->push_back(current_index);
            }
            
            colors->at(current_index) = BLACK;
            
            Region *current = regions->regions->at(current_index);
            
            for (int neigh_index : (*current->near_regions))
            {
                Region *neigh = regions->regions->at(neigh_index);
                if (colors->at(neigh_index) == WHITE && neigh->distance == dist)
                {
                    to_visit->push(neigh_index);
                }
                // if neigh is nearer to the outmost region than current distance and is not in the lower distance regions 
                else if (neigh->distance < dist && lower_distance_regions->count(neigh_index) == 0) 
                {
                    lower_distance_regions->insert(neigh_index);
                }
            }
            
        }
        
        // If I have only one region of lower distance that is adjacent to all neighbours, than it is the super region
        if (lower_distance_regions->size() == 1)
        {
            // the only element of the set is the super region (index)
            int super_region_index = *(lower_distance_regions->begin());
            Region *super_region = regions-> regions->at(super_region_index);
            
            // Adds all neighbours as inner areas of super area and set the super
            // area of nighbours to super area
            for (int neigh_index : (*neighbours))
            {
                super_region->inner_regions->insert(neigh_index);
                
                Region *neigh = regions->regions->at(neigh_index);
                
                neigh->super_region = super_region_index;
            }
            
            delete lower_distance_regions;
            break;
        }
        else // We haven't found the super region, so we need to repeat the algorithm starting from lower distance areas
        {
            //Adds all lower distance areas to the queue in order to restart from them
            for(int lower_dist_index : (*lower_distance_regions))
            {
                to_visit->push(lower_dist_index);
            }
            
            delete lower_distance_regions;
            lower_distance_regions = new std::unordered_set<int>;
            
            // I must continue with decreased distance because the current is not 
            // sufficient to find a super region
            dist--;
            
        }
        
        
        
    }
    
    
    
    delete neighbours;
    delete to_visit;
}

void subregions(RegionContainer *regions)
{
    int regions_num = regions->regions->size();
    std::vector<int> *ordered = regions->ordered_regions;
    std::vector<int> *colors = new std::vector<int>(regions_num, WHITE);
    
    // Must iterate in reverse order in order to start from further regions
    for (std::vector<int>::reverse_iterator i = ordered->rbegin(); i != ordered->rend(); i++ )
    {
        int curr_index = *(i);
        
        // The outmost hasn't got a super area, the algorithm wouldn't terminate!
        if (curr_index != OUTMOST_REGION)
        {
            //printf("%d\n", curr_index);
            if (colors->at(curr_index) == WHITE)
            {
                find_super_region(curr_index, regions, colors);
            }
        }
    }
    
    delete colors;
}