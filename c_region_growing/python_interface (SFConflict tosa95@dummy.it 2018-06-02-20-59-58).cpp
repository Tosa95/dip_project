/*
*   Autori: Tosatto Davide, Grespan Riccardo
*
*   Wrapper del modulo di controllo del sensore di prossimita attraverso le
*   primitive di interoperabilità Python-C
*
*/

#include <Python.h>
#include <numpy/arrayobject.h>
#include <iostream>
#include <unordered_set>
#include "image_utils.h"
#include "colors.h"
#include "region_growing.h"
#include "subregions.h"

using namespace std;

static PyObject * advanced_region_growing_test (PyObject *self, PyObject *args);
static PyObject * advanced_region_growing_region_growing (PyObject *self, PyObject *args);
static PyObject * advanced_region_growing_region_growing_and_bfs (PyObject *self, PyObject *args);
static PyObject * advanced_region_growing_hierarchic_region_growing (PyObject *self, PyObject *args);

// Tabella delle funzioni esportate
static PyMethodDef module_methods[] = {
    {"test", advanced_region_growing_test, METH_VARARGS, "Metodo di test"},
    {"region_growing", advanced_region_growing_region_growing, METH_VARARGS, "Advanced region growing"},
    {"region_growing_and_bfs", advanced_region_growing_region_growing_and_bfs, METH_VARARGS, "Advanced region growing with bfs"},
    {"hierarchic_region_growing", advanced_region_growing_hierarchic_region_growing, METH_VARARGS, "Hierarchic region growing"},
    // Terminatore
    {NULL, NULL, 0, NULL}
};

//Funzione che inizializza il modulo. Obbligatoria per permettere il funzionamento dello stesso
PyMODINIT_FUNC
initadvanced_region_growing(void)
{
    PyObject *m = Py_InitModule3("advanced_region_growing", module_methods, "Modulo che implementa una versione avanzata dell'algoritmo region growing");
    if (m == NULL)
        return;
    
    import_array();
}


//----------FUNZIONI ESPORTATE-------------------

//static PyObject * advanced_region_growing_test (PyObject *self, PyObject *args)
//{
//    PyArrayObject *arg1=0;
//    unsigned char *img=0;
//    
//    PyArrayObject *X;
//    int ndX;
//    npy_intp *shapeX;
//    PyArray_Descr *dtype;
//    NpyIter *iter;
//    NpyIter_IterNextFunc *iternext;
//    PyArray_Type;
//    
//    PyArg_ParseTuple(args, "O", &X);
//    
//    char *img = PyArray_DATA(X);
//    
//    
//    
////    ndX = PyArray_NDIM(X);
////    shapeX = PyArray_SHAPE(X);
//
////    dtype = PyArray_DescrFromType(NPY_UINT8);
////
////    iter = NpyIter_New(X, NPY_ITER_READONLY, NPY_KEEPORDER, NPY_NO_CASTING, dtype);
////    
////    if (iter==NULL) {
////        return NULL;
////    }
////    iternext = NpyIter_GetIterNext(iter, NULL);
////    char **dataptr = (char **) NpyIter_GetDataPtrArray(iter);
//
////    do {
////        cout << **dataptr << endl;
////    } while (iternext(iter));
//
////    NpyIter_Deallocate(iter);
//    return PyInt_FromLong(0);
//}
//
//
//


static PyObject * advanced_region_growing_test (PyObject *self, PyObject *args)
{
    PyArrayObject *img=0;
    
    if(!PyArg_ParseTuple(args, "O", &img))
    {
        printf("ERROR PARSE!");
    }
    
    //printf("%d\n", arg1->descr->alignment);
    
    const int height = (int)img->dimensions[0];
    const int width = (int)img->dimensions[1];
    const int channels = (int)img->dimensions[3];
    const int row_length = width*channels;
    
    int i = 0;
    int avg = 0;
    
    Region *r = new Region(1);
    
    r -> points->push_back(Point(3,4));
    
    for (int row = 0; row < height; row++)
    {
        for (int col = 0; col < width; col++)
        {
            unsigned char R = GET_RCC(img,row,col,Color::R);
            unsigned char G = GET_RCC(img,row,col,Color::G);
            unsigned char B = GET_RCC(img,row,col,Color::B);
            
            i++;
            
            avg += R + G + B;
            
            //printf("Color at [%d][%d]: (%d,%d,%d)\n", row, col, R, G, B);
        }
    }
    
    RegionContainer *res = region_growing(img, 10);
    
    delete res;
    
    delete r;

    return PyFloat_FromDouble(double(avg)/(i*3));
}

static PyObject * from_c_regions_to_python (RegionContainer *region_cont, int height, int width)
{
    PyObject *out_regions = PyList_New(0);
    PyObject *out_dict = PyDict_New();
    
    npy_intp dims[2];
    dims[0] = height;
    dims[1] = width;
    PyObject *out_map = PyArray_SimpleNew(2, dims, PyArray_INT32);

    
    
    for (int row = 0; row < height; row++)
    {
        for (int col = 0; col < width; col++)
        {
            //printf("%d\n", region_cont->regions_map[LINEAR_INDEX(row,col,width)]);
            npy_intp pos[2] = {row, col};
            PyArray_SETITEM(out_map, PyArray_GETPTR2(out_map, row, col), PyInt_FromLong(region_cont->regions_map[LINEAR_INDEX(row,col,width)]));
        }
    }
    
    PyDict_SetItemString(out_dict, "map", out_map);
    
    PyObject *ordered_regions = PyList_New(0);
    
    for (int reg: *(region_cont->ordered_regions))
    {
        PyList_Append(ordered_regions, PyInt_FromLong(reg));
    }
    
    PyDict_SetItemString(out_dict, "ordered_regions", ordered_regions);
    
    for (Region *r:*(region_cont->regions))
    {
        PyObject *region_dict = PyDict_New();
        
        PyDict_SetItemString(region_dict, "label", PyInt_FromLong(r->label));
        
        PyObject *average_py = PyList_New(0);
        PyList_Append(average_py, PyFloat_FromDouble(r->average[Color::B]));
        PyList_Append(average_py, PyFloat_FromDouble(r->average[Color::G]));
        PyList_Append(average_py, PyFloat_FromDouble(r->average[Color::R]));
        
        PyDict_SetItemString(region_dict, "average", average_py);
        
        PyObject *centroid_py = PyList_New(0);
        PyList_Append(centroid_py, PyFloat_FromDouble(r->centroid[PtArray::ROW]));
        PyList_Append(centroid_py, PyFloat_FromDouble(r->centroid[PtArray::COL]));

        PyDict_SetItemString(region_dict, "centroid", centroid_py);
        
        PyObject *points_py = PyList_New(0);
        
        for (Point pt : *(r->points))
        {
            PyObject *point_py = PyList_New(0);
            PyList_Append(point_py, PyInt_FromLong(pt.row));
            PyList_Append(point_py, PyInt_FromLong(pt.col));
            
            
            PyList_Append(points_py, point_py);
        }

        PyDict_SetItemString(region_dict, "points", points_py);
        
        
        PyObject *bound_points_py = PyList_New(0);
        
        for (Point pt : *(r->boundary_points))
        {
            PyObject *point_py = PyList_New(0);
            PyList_Append(point_py, PyInt_FromLong(pt.row));
            PyList_Append(point_py, PyInt_FromLong(pt.col));
            
            
            PyList_Append(bound_points_py, point_py);
        }

        PyDict_SetItemString(region_dict, "boundary_points", bound_points_py);
        
        
        PyObject *near_areas = PyList_New(0);
        
        for (int reg: *(r->near_regions))
        {
            PyList_Append(near_areas, PyInt_FromLong(reg));
        }
        
        PyDict_SetItemString(region_dict, "near_areas", near_areas);
        
        PyObject *bounding_box = PyList_New(0);
        
        PyList_Append(bounding_box, PyInt_FromLong(r->bounding_box.top_left.row));
        PyList_Append(bounding_box, PyInt_FromLong(r->bounding_box.top_left.col));
        PyList_Append(bounding_box, PyInt_FromLong(r->bounding_box.bottom_right.row));
        PyList_Append(bounding_box, PyInt_FromLong(r->bounding_box.bottom_right.col));
        
        PyDict_SetItemString(region_dict, "bounding_box", bounding_box);
        
        PyObject *bfs_children = PyList_New(0);
        
        for (int child: *(r->bfs_children))
        {
            PyList_Append(bfs_children, PyInt_FromLong(child));
        }
        
        PyDict_SetItemString(region_dict, "bfs_children", bfs_children);
        
        PyDict_SetItemString(region_dict, "bfs_father", PyInt_FromLong(r->bfs_father));
        
        PyDict_SetItemString(region_dict, "distance", PyInt_FromLong(r->distance));
        
        PyDict_SetItemString(region_dict, "super_region", PyInt_FromLong(r->super_region));
        
        PyObject *inner_regions = PyList_New(0);
        
        for (int child: *(r->inner_regions))
        {
            PyList_Append(inner_regions, PyInt_FromLong(child));
        }
        
        PyDict_SetItemString(region_dict, "inner_regions", inner_regions);
        
        PyList_Append(out_regions, region_dict);
    }
    
    
    PyDict_SetItemString(out_dict, "regions", out_regions);
    
    return out_dict;
}

static PyObject * advanced_region_growing_region_growing (PyObject *self, PyObject *args)
{
    
    PyArrayObject *img=0;
    double threshold=0;
    
    
    
    if(!PyArg_ParseTuple(args, "Od", &img, &threshold))
    {
        printf("ERROR PARSE!");
    }

    const int height = (int)(img->dimensions[0]);
    const int width = (int)(img->dimensions[1]);
    
    RegionContainer *region_cont = region_growing(img, threshold);
    
    PyObject * out_dict = from_c_regions_to_python(region_cont, height, width);
    
    int ris = region_cont->regions->size();
    
    delete region_cont;

    return out_dict;
}

static PyObject * advanced_region_growing_region_growing_and_bfs (PyObject *self, PyObject *args)
{
    
    PyArrayObject *img=0;
    double threshold=0;
    
    
    
    if(!PyArg_ParseTuple(args, "Od", &img, &threshold))
    {
        printf("ERROR PARSE!");
    }

    const int height = (int)(img->dimensions[0]);
    const int width = (int)(img->dimensions[1]);
    
    RegionContainer *region_cont = region_growing(img, threshold);
    
    bfs(region_cont);
    
    PyObject * out_dict = from_c_regions_to_python(region_cont, height, width);
    
    int ris = region_cont->regions->size();
    
    delete region_cont;

    return out_dict;
}

static PyObject * advanced_region_growing_hierarchic_region_growing (PyObject *self, PyObject *args)
{
    
    PyArrayObject *img=0;
    double threshold=0;
    
    
    
    if(!PyArg_ParseTuple(args, "Od", &img, &threshold))
    {
        printf("ERROR PARSE!");
    }

    const int height = (int)(img->dimensions[0]);
    const int width = (int)(img->dimensions[1]);
    
    printf("THRESHOLD: %f\n", threshold); 
    
    RegionContainer *region_cont = region_growing(img, threshold);
    
    bfs(region_cont);
    subregions(region_cont);
    
    PyObject * out_dict = from_c_regions_to_python(region_cont, height, width);
    
    int ris = region_cont->regions->size();
    
    delete region_cont;

    return out_dict;
}