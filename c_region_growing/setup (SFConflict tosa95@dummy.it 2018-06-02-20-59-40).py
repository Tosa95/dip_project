# -*- coding: utf-8 -*-
#
#	Autori: Tosatto Davide, Riccardo Grespan
#
#	Script per compilare ed installare il modulo proxsensor, scritto in C
#   Comando da eseguire: sudo python setup.py install
#

from distutils.core import setup, Extension

module1 = Extension('advanced_region_growing',
                    sources = ['region_growing.cpp', 'subregions.cpp', 'python_interface.cpp'],
                    extra_compile_args=["-std=c++11"])

setup (name = 'advanced_region_growing',
       version = '0.1',
       description = 'Implementazione di una versione avanzata dell algoritmo region growing',
       ext_modules = [module1])
