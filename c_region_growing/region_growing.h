/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   region_growing.h
 * Author: davide
 *
 * Created on 19 maggio 2018, 12.25
 */

#ifndef REGION_GROWING_H
#define REGION_GROWING_H

#include <stdlib.h>
#include <vector>
#include <unordered_set>
#include <list>
#include <Python.h>
#include <numpy/arrayobject.h>
#include "image_utils.h"
#include "memory_bench.h"

#define LINEAR_INDEX(row, col, width) (row*width + col)

const int NO_REGION = -1;
const int OUTMOST_REGION = 0;
const int NO_FATHER = -1;
const int DISTANCE_NOT_CALCULATED = -1;

enum PtArray{ROW=0, COL};

struct Point{
    int row;
    int col;
    bool destroyed = false;
    Point(const Point &other)
    {
        row = other.row;
        col = other.col;
        destroyed = other.destroyed;
        #ifdef MEMORY_BENCHMARK
            membench.incref(POINT_MEMBENCH_NAME);
        #endif
    }
    Point(int row, int col){
        this->row = row;
        this->col = col;
        #ifdef MEMORY_BENCHMARK
            membench.incref(POINT_MEMBENCH_NAME);
        #endif
    }
    Point(){
        this->row = 0;
        this->col = 0;
        #ifdef MEMORY_BENCHMARK
            membench.incref(POINT_MEMBENCH_NAME);
        #endif
    }
    ~Point(){
        #ifdef MEMORY_BENCHMARK
            if (!destroyed)
                membench.decref(POINT_MEMBENCH_NAME);
        #endif
        destroyed = true;
    }
};

struct BoundingBox{
    Point top_left;
    Point bottom_right;
    BoundingBox(Point top_left, Point bottom_right)
    {
        this->top_left = top_left;
        this->bottom_right = bottom_right;
        #ifdef MEMORY_BENCHMARK
            membench.incref(BOUNDING_BOX_MEMBENCH_NAME);
        #endif
    }
    BoundingBox()
    {
        this->top_left = Point();
        this->bottom_right = Point();
        #ifdef MEMORY_BENCHMARK
            membench.incref(BOUNDING_BOX_MEMBENCH_NAME);
        #endif
    }
    BoundingBox(const BoundingBox &other)
    {
        top_left = other.top_left;
        bottom_right = other.bottom_right;
        #ifdef MEMORY_BENCHMARK
            membench.incref(BOUNDING_BOX_MEMBENCH_NAME);
        #endif
    }
    ~BoundingBox()
    {
        #ifdef MEMORY_BENCHMARK
            membench.decref(BOUNDING_BOX_MEMBENCH_NAME);
        #endif
    }
};

struct Region{
    int label;
    double average[3];
    double centroid[2];
    BoundingBox bounding_box;
    
    std::vector <Point> *points;
    std::vector <Point> *boundary_points;
    std::unordered_set <int> *near_regions;
    
    int distance;
    int bfs_father;
    std::unordered_set <int> *bfs_children;
    
    int super_region;
    std::unordered_set <int> *inner_regions;
    
    // Numero di punti comprese le sotto regioni
    int points_num;
    
    Region(int label);
    ~Region();
};

struct RegionContainer{
    std::vector <Region*> *regions;
    std::vector <int> *ordered_regions;
    int *regions_map;
    int height;
    int width;
    
    RegionContainer(int height, int width);
    ~RegionContainer();
};

RegionContainer *region_growing(Image *img, double threshold, bool avg_based = true);

#endif /* REGION_GROWING_H */

