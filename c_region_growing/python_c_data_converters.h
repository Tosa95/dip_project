/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   python_c_data_converters.h
 * Author: Davide
 *
 * Created on 30 novembre 2018, 8.48
 */

#ifndef PYTHON_C_DATA_CONVERTERS_H

#include <Python.h>
#include <vector>
#include "shape_recognition.h"

PyObject * shape_descriptor_to_python (ShapeDescriptor *descriptor);

PyObject * shape_descriptors_to_python (ShapeDescriptorsContainer *descriptors);

PyObject * c_double_vector_to_python (std::vector<double> *v);

PyObject * c_color_to_python (double *c);

#define PYTHON_C_DATA_CONVERTERS_H



#endif /* PYTHON_C_DATA_CONVERTERS_H */

