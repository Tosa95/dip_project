/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   image_utils.h
 * Author: davide
 *
 * Created on 19 maggio 2018, 13.32
 */

#ifndef IMAGE_UTILS_H
#define IMAGE_UTILS_H

#include <Python.h>
#include <numpy/arrayobject.h>
#include "memory_bench.h"

//#define GET_RCC(img,row,col,ch) (*(unsigned char *)PyArray_GETPTR3(img, row, col, ch))

#define GET_RCC_NP(img,row,col,ch) (*(unsigned char *)PyArray_GETPTR3(img, row, col, ch))

#define LINEAR_INDEX_3(row, col, ch, width, height) (row*width + col + ch*width*height)
#define GET_RCC(img,row,col,ch) (img->data[LINEAR_INDEX_3(row,col,ch,img->dimensions[1],img->dimensions[0])])

//typedef PyArrayObject Image;



struct CImage{
    int dimensions[3];
    unsigned char *data;
    
    CImage(int height, int width, int channels)
    {
        this->dimensions[0] = height;
        this->dimensions[1] = width;
        this->dimensions[2] = channels;
        
        this->data = new unsigned char [height * width * channels];
        
        #ifdef MEMORY_BENCHMARK
            membench.incref(C_IMAGE_MEMBENCH_NAME);
        #endif
    }
    
    ~CImage()
    {
        delete[] this->data;
        #ifdef MEMORY_BENCHMARK
            membench.decref(C_IMAGE_MEMBENCH_NAME);
        #endif
    }
};

typedef CImage Image;

#endif /* IMAGE_UTILS_H */

