/*
*   Autore: Tosatto Davide
*
*   
*
*/

#include <Python.h>
#include <numpy/arrayobject.h>
#include <iostream>
#include <unordered_set>
#include "image_utils.h"
#include "colors.h"
#include "region_growing.h"
#include "region_growing_2.h"
#include "subregions.h"
#include "shape_recognition.h"
#include <time.h>
#include "fft.h"
#include "tabulized_atan.h"
#include <math.h>
#include <unistd.h>
#include "python_c_data_converters.h"
#include "memory_bench.h"

using namespace std;

static PyObject * advanced_region_growing_test (PyObject *self, PyObject *args);
static PyObject * advanced_region_growing_region_growing (PyObject *self, PyObject *args);
static PyObject * advanced_region_growing_region_growing_and_bfs (PyObject *self, PyObject *args);
static PyObject * advanced_region_growing_hierarchic_region_growing (PyObject *self, PyObject *args);
static PyObject * advanced_region_growing_compute_contour_plot (PyObject *self, PyObject *args);
static PyObject * advanced_region_growing_test_region_collapse (PyObject *self, PyObject *args);
static PyObject * advanced_region_growing_test_correlation (PyObject *self, PyObject *args);
static PyObject * advanced_region_growing_test_shape_distance (PyObject *self, PyObject *args);
static PyObject * advanced_region_growing_find_relevant_shapes (PyObject *self, PyObject *args);
static PyObject * advanced_region_growing_find_relevant_shapes_2 (PyObject *self, PyObject *args);
static PyObject * advanced_region_growing_get_shapes_descriptors (PyObject *self, PyObject *args);
static PyObject * advanced_region_growing_find_relevant_shapes_fast (PyObject *self, PyObject *args);
static PyObject * advanced_region_growing_get_principal_shape_area (PyObject *self, PyObject *args);

// Tabella delle funzioni esportate
static PyMethodDef module_methods[] = {
    {"test", advanced_region_growing_test, METH_VARARGS, "Metodo di test"},
//    {"region_growing", advanced_region_growing_region_growing, METH_VARARGS, "Advanced region growing"},
//    {"region_growing_and_bfs", advanced_region_growing_region_growing_and_bfs, METH_VARARGS, "Advanced region growing with bfs"},
//    {"hierarchic_region_growing", advanced_region_growing_hierarchic_region_growing, METH_VARARGS, "Hierarchic region growing"},
//    {"compute_contour_plot", advanced_region_growing_compute_contour_plot, METH_VARARGS, "Computes contour plots"},
//    {"test_region_collapse", advanced_region_growing_test_region_collapse, METH_VARARGS, "Tests region collapse"},
//    {"test_shape_distance", advanced_region_growing_test_shape_distance, METH_VARARGS, "Tests shape distance"},
//    {"test_correlation", advanced_region_growing_test_correlation, METH_VARARGS, "Tests correlation"},
    {"find_relevant_shapes", advanced_region_growing_find_relevant_shapes, METH_VARARGS, "Finds relevant shapes in the image"},
    {"find_relevant_shapes_2", advanced_region_growing_find_relevant_shapes_2, METH_VARARGS, "Finds relevant shapes in the image, version 2"},
    {"get_shapes_descriptors", advanced_region_growing_get_shapes_descriptors, METH_VARARGS, "Get the descriptors of the sahpes in the image"},
    {"find_relevant_shapes_fast", advanced_region_growing_find_relevant_shapes_fast, METH_VARARGS, "Finds relevant shapes in the image, faster version"},
    {"get_principal_shape_area", advanced_region_growing_get_principal_shape_area, METH_VARARGS, "Gets the area of the principal shape"},
    // Terminatore
    {NULL, NULL, 0, NULL}
};

//Funzione che inizializza il modulo. Obbligatoria per permettere il funzionamento dello stesso
PyMODINIT_FUNC
initadvanced_region_growing(void)
{
    PyObject *m = Py_InitModule3("advanced_region_growing", module_methods, "Modulo che implementa una versione avanzata dell'algoritmo region growing");
    if (m == NULL)
        return;
    
    import_array();
    
    printf("Init done\n");
}


//----------FUNZIONI ESPORTATE-------------------

//static PyObject * advanced_region_growing_test (PyObject *self, PyObject *args)
//{
//    PyArrayObject *arg1=0;
//    unsigned char *img=0;
//    
//    PyArrayObject *X;
//    int ndX;
//    npy_intp *shapeX;
//    PyArray_Descr *dtype;
//    NpyIter *iter;
//    NpyIter_IterNextFunc *iternext;
//    PyArray_Type;
//    
//    PyArg_ParseTuple(args, "O", &X);
//    
//    char *img = PyArray_DATA(X);
//    
//    
//    
////    ndX = PyArray_NDIM(X);
////    shapeX = PyArray_SHAPE(X);
//
////    dtype = PyArray_DescrFromType(NPY_UINT8);
////
////    iter = NpyIter_New(X, NPY_ITER_READONLY, NPY_KEEPORDER, NPY_NO_CASTING, dtype);
////    
////    if (iter==NULL) {
////        return NULL;
////    }
////    iternext = NpyIter_GetIterNext(iter, NULL);
////    char **dataptr = (char **) NpyIter_GetDataPtrArray(iter);
//
////    do {
////        cout << **dataptr << endl;
////    } while (iternext(iter));
//
////    NpyIter_Deallocate(iter);
//    return PyInt_FromLong(0);
//}
//
//
//


static PyObject * advanced_region_growing_test (PyObject *self, PyObject *args)
{
    clock_t start = clock();
    
    double sum = 0;
    int measurements = 0;
    
    for (double a = -M_PI; a<M_PI; a+=0.001)
    {
        double x = cos(a);
        double y = sin(a);
        sum += fabs(tabulized_atan2(y,x) - atan2(y,x));
        printf("cfrnt: %f  %f  x=%f  y=%f\n", tabulized_atan2(y,x), atan2(y,x), x, y);
        measurements++;
    }
    
    clock_t end = clock();
    
    printf("Timing: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);

    return PyFloat_FromDouble(sum/measurements);
}

static PyObject * from_c_region_to_python (Region *r)
{
    PyObject *region_dict = PyDict_New();
        
    PyDict_SetItemString(region_dict, "label", PyInt_FromLong(r->label));

    PyObject *average_py = PyList_New(0);
    PyList_Append(average_py, PyFloat_FromDouble(r->average[Color::B]));
    PyList_Append(average_py, PyFloat_FromDouble(r->average[Color::G]));
    PyList_Append(average_py, PyFloat_FromDouble(r->average[Color::R]));

    PyDict_SetItemString(region_dict, "average", average_py);

    PyObject *centroid_py = PyList_New(0);
    PyList_Append(centroid_py, PyFloat_FromDouble(r->centroid[PtArray::ROW]));
    PyList_Append(centroid_py, PyFloat_FromDouble(r->centroid[PtArray::COL]));

    PyDict_SetItemString(region_dict, "centroid", centroid_py);

    PyObject *points_py = PyList_New(0);

    for (Point pt : *(r->points))
    {
        PyObject *point_py = PyList_New(0);
        PyList_Append(point_py, PyInt_FromLong(pt.row));
        PyList_Append(point_py, PyInt_FromLong(pt.col));


        PyList_Append(points_py, point_py);
    }

    PyDict_SetItemString(region_dict, "points", points_py);


    PyObject *bound_points_py = PyList_New(0);

    for (Point pt : *(r->boundary_points))
    {
        PyObject *point_py = PyList_New(0);
        PyList_Append(point_py, PyInt_FromLong(pt.row));
        PyList_Append(point_py, PyInt_FromLong(pt.col));


        PyList_Append(bound_points_py, point_py);
    }

    PyDict_SetItemString(region_dict, "boundary_points", bound_points_py);


    PyObject *near_areas = PyList_New(0);

    for (int reg: *(r->near_regions))
    {
        PyList_Append(near_areas, PyInt_FromLong(reg));
    }

    PyDict_SetItemString(region_dict, "near_areas", near_areas);

    PyObject *bounding_box = PyList_New(0);

    PyList_Append(bounding_box, PyInt_FromLong(r->bounding_box.top_left.row));
    PyList_Append(bounding_box, PyInt_FromLong(r->bounding_box.top_left.col));
    PyList_Append(bounding_box, PyInt_FromLong(r->bounding_box.bottom_right.row));
    PyList_Append(bounding_box, PyInt_FromLong(r->bounding_box.bottom_right.col));

    PyDict_SetItemString(region_dict, "bounding_box", bounding_box);

    PyObject *bfs_children = PyList_New(0);

    for (int child: *(r->bfs_children))
    {
        PyList_Append(bfs_children, PyInt_FromLong(child));
    }

    PyDict_SetItemString(region_dict, "bfs_children", bfs_children);

    PyDict_SetItemString(region_dict, "bfs_father", PyInt_FromLong(r->bfs_father));

    PyDict_SetItemString(region_dict, "distance", PyInt_FromLong(r->distance));

    PyDict_SetItemString(region_dict, "super_region", PyInt_FromLong(r->super_region));

    PyObject *inner_regions = PyList_New(0);

    for (int child: *(r->inner_regions))
    {
        PyList_Append(inner_regions, PyInt_FromLong(child));
    }

    PyDict_SetItemString(region_dict, "inner_regions", inner_regions);

    return region_dict;
}

static PyObject * from_c_regions_to_python (RegionContainer *region_cont, int height, int width)
{
    PyObject *out_regions = PyList_New(0);
    PyObject *out_dict = PyDict_New();
    
    npy_intp dims[2];
    dims[0] = height;
    dims[1] = width;
    PyObject *out_map = PyArray_SimpleNew(2, dims, PyArray_INT32);
    
    printf("Array built successfully\n");
    
    for (int row = 0; row < height; row++)
    {
        for (int col = 0; col < width; col++)
        {
            //printf("%d\n", region_cont->regions_map[LINEAR_INDEX(row,col,width)]);
            npy_intp pos[2] = {row, col};
            PyArray_SETITEM(out_map, PyArray_GETPTR2(out_map, row, col), PyInt_FromLong(region_cont->regions_map[LINEAR_INDEX(row,col,width)]));
        }
    }
    
    PyDict_SetItemString(out_dict, "map", out_map);
    
    PyObject *ordered_regions = PyList_New(0);
    
    for (int reg: *(region_cont->ordered_regions))
    {
        PyList_Append(ordered_regions, PyInt_FromLong(reg));
    }
    
    PyDict_SetItemString(out_dict, "ordered_regions", ordered_regions);
    
    for (Region *r:*(region_cont->regions))
    {
        PyObject *region_dict = from_c_region_to_python(r);
        
        PyList_Append(out_regions, region_dict);
    }
    
    
    PyDict_SetItemString(out_dict, "regions", out_regions);
    
    return out_dict;
}

//static PyObject * advanced_region_growing_region_growing (PyObject *self, PyObject *args)
//{
//    
//    PyArrayObject *img=0;
//    double threshold=0;
//    
//    
//    
//    if(!PyArg_ParseTuple(args, "Od", &img, &threshold))
//    {
//        printf("ERROR PARSE!");
//    }
//
//    const int height = (int)(img->dimensions[0]);
//    const int width = (int)(img->dimensions[1]);
//    
//    RegionContainer *region_cont = region_growing(img, threshold);
//    
//    PyObject * out_dict = from_c_regions_to_python(region_cont, height, width);
//    
//    int ris = region_cont->regions->size();
//    
//    delete region_cont;
//
//    return out_dict;
//}
//
//static PyObject * advanced_region_growing_region_growing_and_bfs (PyObject *self, PyObject *args)
//{
//    
//    PyArrayObject *img=0;
//    double threshold=0;
//    
//    
//    
//    if(!PyArg_ParseTuple(args, "Od", &img, &threshold))
//    {
//        printf("ERROR PARSE!");
//    }
//
//    const int height = (int)(img->dimensions[0]);
//    const int width = (int)(img->dimensions[1]);
//    
//    RegionContainer *region_cont = region_growing(img, threshold);
//    
//    bfs(region_cont);
//    
//    PyObject * out_dict = from_c_regions_to_python(region_cont, height, width);
//    
//    int ris = region_cont->regions->size();
//    
//    delete region_cont;
//
//    return out_dict;
//}
//
//static PyObject * advanced_region_growing_hierarchic_region_growing (PyObject *self, PyObject *args)
//{
//    
//    PyArrayObject *img=0;
//    double threshold=0;
//    
//    
//    
//    if(!PyArg_ParseTuple(args, "Od", &img, &threshold))
//    {
//        printf("ERROR PARSE!");
//    }
//
//    const int height = (int)(img->dimensions[0]);
//    const int width = (int)(img->dimensions[1]);
//    
//    printf("THRESHOLD: %f\n", threshold); 
//    
//    clock_t start = clock();
//    RegionContainer *region_cont = region_growing(img, threshold);
//    bfs(region_cont);
//    subregions(region_cont);
//    clock_t end = clock();
//    printf("Timing: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);
//    
//    
//    
//    
//    
//    PyObject * out_dict = from_c_regions_to_python(region_cont, height, width);
//    
//    int ris = region_cont->regions->size();
//    
//    delete region_cont;
//
//    return out_dict;
//}
//
//static PyObject * advanced_region_growing_compute_contour_plot (PyObject *self, PyObject *args)
//{
//    PyObject *contour_points=0;
//    PyObject *centroid=0;
//    int num_points=0;
//    
//    
//    
//    if(!PyArg_ParseTuple(args, "OOi", &contour_points, &centroid, &num_points))
//    {
//        printf("ERROR PARSE!");
//    }
//    
//    Py_ssize_t cp_size = PyList_Size(contour_points);
//    std::vector<Point> *boundary_points_c = new std::vector<Point>(cp_size);
//    
//    for (int i = 0; i < cp_size; i++)
//    {
//        PyObject *point =  PyList_GetItem(contour_points, i);
//        Point pt = Point((int)PyInt_AsLong(PyList_GetItem(point, 0)), (int)PyInt_AsLong(PyList_GetItem(point, 1)));
//        boundary_points_c->at(i) = pt;
//    }
//    
//    double centroid_row = PyFloat_AsDouble(PyList_GetItem(centroid, 0));
//    double centroid_col = PyFloat_AsDouble(PyList_GetItem(centroid, 1));
//    
//    double centroid_c[2] = {centroid_row, centroid_col};
//    
//    clock_t start = clock();
//    std::vector<double> *contour = compute_contour_plot(boundary_points_c, centroid_c, num_points);
//    clock_t end = clock();
//    printf("Timing: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);
//    
//    PyObject *result = PyList_New(0);
//    
//    for (double dist : (*contour))
//    {
//        PyList_Append(result, PyFloat_FromDouble(dist));
//    }
//    delete boundary_points_c;
//    delete contour;
//    
//    return result;
//}
//
//static PyObject * advanced_region_growing_test_region_collapse (PyObject *self, PyObject *args)
//{
//    
//    PyArrayObject *img=0;
//    double threshold=0;
//    int region_lbl=0;
//    
//    
//    
//    if(!PyArg_ParseTuple(args, "Odi", &img, &threshold, &region_lbl))
//    {
//        printf("ERROR PARSE!  aa");
//    }
//
//    const int height = (int)(img->dimensions[0]);
//    const int width = (int)(img->dimensions[1]);
//    
//    printf("THRESHOLD: %f\n", threshold); 
//    
//    
//    RegionContainer *region_cont = region_growing(img, threshold);
//    bfs(region_cont);
//    subregions(region_cont);
//    
//    clock_t start = clock();
//    Region *collapsed = collapse_inner_regions(region_lbl, region_cont, false);
//    clock_t end = clock();
//    printf("Timing collapse: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);
//    
//    PyObject *result = from_c_region_to_python(collapsed);
//    
//    
////    Py_DECREF(img);
//    delete region_cont;
//
//    return result;
//}
//
//static PyObject * advanced_region_growing_test_correlation (PyObject *self, PyObject *args)
//{
//    printf("HEREEEEE\n");
//    
//    PyArrayObject *img=0;
//    double threshold=0;
//    int region_lbl_1=0;
//    int region_lbl_2=0;
//    
//    
//    
//    if(!PyArg_ParseTuple(args, "Odii", &img, &threshold, &region_lbl_1, &region_lbl_2))
//    {
//        printf("ERROR PARSE! bb");
//    }
//
//    const int height = (int)(img->dimensions[0]);
//    const int width = (int)(img->dimensions[1]);
//    
//    printf("THRESHOLD: %f\n", threshold); 
//    
//    
//    RegionContainer *region_cont = region_growing(img, threshold);
//    bfs(region_cont);
//    subregions(region_cont);
//    
//    clock_t start = clock();
//    Region *collapsed_1 = collapse_inner_regions(region_lbl_1, region_cont, false);
//    Region *collapsed_2 = collapse_inner_regions(region_lbl_2, region_cont, false);
//    
//    std::vector<double> *contour_1 = compute_contour_plot(collapsed_1->boundary_points, collapsed_1->centroid, 128);
//    std::vector<double> *contour_2 = compute_contour_plot(collapsed_2->boundary_points, collapsed_2->centroid, 128);
//    
//    printf("CONTOUR: %f\n", contour_2->at(20));
//    
//    std::vector<double> *res = correlation(contour_1, contour_2);
//    
//    delete contour_1;
//    delete contour_2;
//    
//    clock_t end = clock();
//    printf("Timing correlation: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);
//    
//    PyObject *result = PyList_New(0);
//    
//    for (double dist : (*res))
//    {
//        PyList_Append(result, PyFloat_FromDouble(dist));
//    }
//    
//    delete region_cont;
//
//    return result;
//}
//
//static PyObject * advanced_region_growing_test_shape_distance (PyObject *self, PyObject *args)
//{
//    printf("HEREEEEE\n");
//    
//    PyArrayObject *img=0;
//    double threshold=0;
//    int region_lbl_1=0;
//    int region_lbl_2=0;
//    
//    
//    
//    if(!PyArg_ParseTuple(args, "Odii", &img, &threshold, &region_lbl_1, &region_lbl_2))
//    {
//        printf("ERROR PARSE! bb");
//    }
//
//    const int height = (int)(img->dimensions[0]);
//    const int width = (int)(img->dimensions[1]);
//    
//    printf("THRESHOLD: %f\n", threshold); 
//    
//    
//    RegionContainer *region_cont = region_growing(img, threshold);
//    bfs(region_cont);
//    subregions(region_cont);
//    
//    
//    Region *collapsed_1 = collapse_inner_regions(region_lbl_1, region_cont, false);
//    Region *collapsed_2 = collapse_inner_regions(region_lbl_2, region_cont, false);
//    
//    std::vector<double> *contour_1 = compute_contour_plot(collapsed_1->boundary_points, collapsed_1->centroid, 128);
//    std::vector<double> *contour_2 = compute_contour_plot(collapsed_2->boundary_points, collapsed_2->centroid, 128);
//    
//    printf("CONTOUR: %f\n", contour_2->at(20));
//    
//    clock_t start = clock();
//    double res = shape_distance(contour_1, contour_2);
//    clock_t end = clock();
//    printf("Timing shape distance: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);
//    
//    delete contour_1;
//    delete contour_2;
//    
//    
//    
//    delete region_cont;
//
//    return PyFloat_FromDouble(res);
//}

static BaseShapesContainer *to_c_shapes_contours(PyObject *py_contours)
{
    PyObject *keys = PyDict_Keys(py_contours);
    
    ShapeContoursMap *scm = new ShapeContoursMap();
    
    for(int i = 0; i<PyList_Size(keys); i++)
    {
        std::string shape_name(PyString_AsString(PyList_GetItem(keys,i)));
        PyObject *shape_contours = PyDict_GetItemString(py_contours, shape_name.c_str());
        
        ShapeContours *contours_container = new ShapeContours();
        
        for(int j = 0; j < PyList_Size(shape_contours); j++)
        {
            
            PyObject *py_contour = PyList_GetItem(shape_contours, j);
            
            ShapeContour *single_contour = new ShapeContour();
            
            for (int k = 0; k<PyList_Size(py_contour); k++)
            {
                // Inserts a single entry of the contour into the array
                single_contour->push_back(PyFloat_AsDouble(PyList_GetItem(py_contour, k)));
            }
            
            contours_container->push_back(single_contour);
        }
        
        scm->insert(std::pair<std::string,ShapeContours*>(shape_name, contours_container));
    }
    
    return new BaseShapesContainer(scm);
}

static PyObject * shape_recognition_to_python (ShapeRecognitionResult *rresult)
{
    PyObject *result = PyList_New(0);
    
    for (ShapeRecognized sr : *(rresult->get_recognized_shapes()))
    {
        PyObject *shape_descriptor = PyDict_New();
        
        PyObject *bounding_box = PyList_New(0);

        PyList_Append(bounding_box, PyInt_FromLong(sr.bb.top_left.row));
        PyList_Append(bounding_box, PyInt_FromLong(sr.bb.top_left.col));
        PyList_Append(bounding_box, PyInt_FromLong(sr.bb.bottom_right.row));
        PyList_Append(bounding_box, PyInt_FromLong(sr.bb.bottom_right.col));
        
        PyDict_SetItemString(shape_descriptor, "bounding_box", bounding_box);
        Py_DECREF(bounding_box);
        PyDict_SetItemString(shape_descriptor, "norm_dist", PyFloat_FromDouble(sr.distance_normalized));
        PyDict_SetItemString(shape_descriptor, "name", PyString_FromString(sr.shape_name.c_str()));
        
        PyList_Append(result, shape_descriptor);
        
    }
    
    return result;
}

CImage *numpy_to_c(PyArrayObject *img)
{
    const int height = (int)(img->dimensions[0]);
    const int width = (int)(img->dimensions[1]);
    const int channels = (int)(img->dimensions[2]);
    
    CImage *res = new CImage(height, width, channels);
    
    for (int h = 0; h < height; h++)
    {
        for (int w = 0; w < width; w++)
        {
            for (int c = 0; c < channels; c++)
            {
//                printf("%d, %d, %d, %d, %d, %d\n", h,w,c,width,height, LINEAR_INDEX_3(h,w,c,width,height));
                res->data[LINEAR_INDEX_3(h,w,c,width,height)] = GET_RCC_NP(img,h,w,c);
            }
        }
    }
    
    return res;
}

std::vector<double> *python_list_to_c_vector(PyObject *list)
{
    size_t sz = PyList_Size(list);
    std::vector<double> *res = new std::vector<double>();
    
    for (int i = 0; i < sz; i++)
    {
//        double num = PyFloat_AsDouble(PyList_GetItem(list, i));
//        printf ("%.2f, ", num);
        res->push_back(PyFloat_AsDouble(PyList_GetItem(list, i)));
    }
    
//    printf ("\n");
    
    return res;
}

/*
 * This is the true function used to find relevant shapes
 */
static PyObject * advanced_region_growing_find_relevant_shapes (PyObject *self, PyObject *args)
{
    // The image to be analyzed
    PyArrayObject *img=0;
    // Threshold for the region growing part
    double threshold_rg=0;
    // Object describing the shapes to match with
    PyObject *base_shapes=0;
    // Threshold for the shape matching part
    double threshold_sm=0;
    // Threshold beyond that the matching stops with negative results
    double max_diff_threshold=0;
    // Threshold on the difference between contour plots means
    double max_diff_on_mean=0;
    // Number of points to use for contours
    int num_points=0;
    // Minimum number of boundary points for matching (to avoid false matching in too small shapes)
    int min_boundary_points=0;
    
    if(!PyArg_ParseTuple(args, "OdOdddii", &img, &threshold_rg, &base_shapes, &threshold_sm, &max_diff_threshold, &max_diff_on_mean, &num_points, &min_boundary_points))
    {
        printf("ERROR PARSE!");
    }
    
    BaseShapesContainer *bsc;
    RegionContainer *region_cont;
    ShapeRecognitionResult *res;
    clock_t start = clock();
    
    CImage *c_img = numpy_to_c(img);
    bsc = to_c_shapes_contours(base_shapes);
    
    Py_BEGIN_ALLOW_THREADS // Releases the GIL

    region_cont = region_growing(c_img, threshold_rg);
    bfs(region_cont);
    subregions(region_cont);
    res = recognize_relevant_shapes(region_cont, num_points, bsc, threshold_sm, max_diff_threshold, max_diff_on_mean, min_boundary_points);

//    usleep(1000000);
    Py_END_ALLOW_THREADS // Re-acquires the GIL
            
    clock_t end = clock();
            
//    printf("Timing: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);
    
    PyObject *ris = shape_recognition_to_python(res);
    
    delete region_cont;
    delete bsc;
    delete res;
    delete c_img;
    
//    Py_DECREF(img);
//    Py_DECREF(base_shapes);

    return ris; 
}

/*
 * This uses region growing 2 that is the one involved in genetic optimization. For now this is only experimental.
 */
static PyObject * advanced_region_growing_find_relevant_shapes_2 (PyObject *self, PyObject *args)
{
    // The image to be analyzed
    PyArrayObject *img=0;
    // Data used to compute difference btw colors
    PyObject *diff_data_py=0;
    // Object describing the shapes to match with
    PyObject *base_shapes=0;
    // Threshold for the shape matching part
    double threshold_sm=0;
    // Threshold beyond that the matching stops with negative results
    double max_diff_threshold=0;
    // Threshold on the difference between contour plots means
    double max_diff_on_mean=0;
    // Number of points to use for contours
    int num_points=0;
    // Minimum number of boundary points for matching (to avoid false matching in too small shapes)
    int min_boundary_points=0;
    
    if(!PyArg_ParseTuple(args, "OOOdddii", &img, &diff_data_py, &base_shapes, &threshold_sm, &max_diff_threshold, &max_diff_on_mean, &num_points, &min_boundary_points))
    {
        printf("ERROR PARSE!");
    }

    const int height = (int)(img->dimensions[0]);
    const int width = (int)(img->dimensions[1]);
    
    BaseShapesContainer *bsc;
    RegionContainer *region_cont;
    ShapeRecognitionResult *res;
    clock_t start = clock();
    
    std::vector<double> *diff_data = python_list_to_c_vector(diff_data_py);    
    CImage *c_img = numpy_to_c(img);
    bsc = to_c_shapes_contours(base_shapes);
    
    Py_BEGIN_ALLOW_THREADS // Releases the GIL

    region_cont = region_growing_2(c_img, diff_data); 
    
    bfs(region_cont);
    subregions(region_cont);
    res = recognize_relevant_shapes(region_cont, num_points, bsc, threshold_sm, max_diff_threshold, max_diff_on_mean, min_boundary_points);
    
//    usleep(1000000);
    Py_END_ALLOW_THREADS // Re-acquires the GIL
            
    clock_t end = clock();
            
//    printf("Timing: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);
    
    PyObject *ris = shape_recognition_to_python(res);
    
    delete region_cont;
    delete bsc;
    delete res;
    delete c_img;
    delete diff_data;
    
//    Py_DECREF(img);
//    Py_DECREF(base_shapes);

    return ris; 
}

static PyObject * advanced_region_growing_get_shapes_descriptors (PyObject *self, PyObject *args)
{
   // The image to be analyzed
    PyArrayObject *img=0;
    // Threshold for the region growing part
    double threshold_rg=0;
    // Number of points to use for contours
    int num_points=0;
    // Min boundary points to be considered a shape
    int num_bound_points=0;
    
    if(!PyArg_ParseTuple(args, "Odii", &img, &threshold_rg, &num_points, &num_bound_points))
    {
        printf("ERROR PARSE!");
    }

    const int height = (int)(img->dimensions[0]);
    const int width = (int)(img->dimensions[1]);
    
    RegionContainer *region_cont;
    ShapeDescriptorsContainer *res;
    
    
    CImage *c_img = numpy_to_c(img);
    
    Py_BEGIN_ALLOW_THREADS // Releases the GIL

    clock_t start = clock();            
    region_cont = region_growing(c_img, threshold_rg); 
    clock_t end = clock();
    
    printf("Timing rg: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);
    
    bfs(region_cont);
    subregions(region_cont);
    
    start = clock();
    res = get_all_shapes_descriptors(region_cont, num_points, num_bound_points);
    end = clock();
            
    printf("Timing descriptors: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);
    
//    usleep(1000000);
    Py_END_ALLOW_THREADS // Re-acquires the GIL
    
    clock_t start = clock();
    PyObject *ris = shape_descriptors_to_python(res);
    clock_t end = clock();
    
    printf("Timing conversion: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);
    
    delete region_cont;
    delete res;
    delete c_img;
    
//    Py_DECREF(img);
//    Py_DECREF(base_shapes);

    return ris; 
}

static PyObject * advanced_region_growing_find_relevant_shapes_fast (PyObject *self, PyObject *args)
{
    // The image to be analyzed
    PyArrayObject *img=0;
    // Threshold for the region growing part
    double threshold_rg=0;
    // Parameters for shape recognition
    PyObject *params=0;
    // Number of points to use for contours
    int num_points=0;
    // Minimum number of boundary points for matching (to avoid false matching in too small shapes)
    int min_boundary_points=0;
    
    if(!PyArg_ParseTuple(args, "OdiiO", &img, &threshold_rg, &num_points, &min_boundary_points, &params))
    {
        printf("ERROR PARSE!");
    }
    
//    printf("QUI QUO QUA!\n");
    
    BaseShapesContainer *bsc;
    RegionContainer *region_cont;
    ShapeRecognitionResult *res;
    
    CImage *c_img = numpy_to_c(img);
    std::vector<double> *sh_rec_params = python_list_to_c_vector(params);
    
    Py_BEGIN_ALLOW_THREADS // Releases the GIL

    clock_t start = clock();
    region_cont = region_growing(c_img, threshold_rg); 
    clock_t end = clock();
    
    printf("Timing rg: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);
    
    start = clock();
    bfs(region_cont);
    end = clock();
    
    printf("Timing bfs: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);
    
    start = clock();
    subregions(region_cont);
    end = clock();
    
    printf("Timing subr: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);
    
    start = clock();
    res = recognize_relevant_shapes_2(region_cont, num_points, min_boundary_points, sh_rec_params);
    end = clock();
            
    printf("Timing rec: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);
    
//    usleep(1000000);
    Py_END_ALLOW_THREADS // Re-acquires the GIL
            
    
    
    PyObject *ris = shape_recognition_to_python(res);
    
    delete region_cont;
    delete sh_rec_params;
    delete res;
    delete c_img;
    
//    Py_DECREF(img);
//    Py_DECREF(base_shapes);

    return ris; 
}

//static PyObject * advanced_region_growing_get_principal_shape_area (PyObject *self, PyObject *args)
//{
//    printf("HEREEEEEEE0!!!");
//    // The image to be analyzed
//    PyArrayObject *img=0;
//    // Threshold for the region growing part
//    double threshold_rg=0;
//    // Object describing the shapes to match with
//    PyObject *base_shapes=0;
//    // Threshold for the shape matching part
//    double threshold_sm=0;
//    // Threshold beyond that the matching stops with negative results
//    double max_diff_threshold=0;
//    // Threshold on the difference between contour plots means
//    double max_diff_on_mean=0;
//    // Number of points to use for contours
//    int num_points=0;
//    // Minimum number of boundary points for matching (to avoid false matching in too small shapes)
//    int min_boundary_points=0;
//    
//    if(!PyArg_ParseTuple(args, "OdOdddii", &img, &threshold_rg, &base_shapes, &threshold_sm, &max_diff_threshold, &max_diff_on_mean, &num_points, &min_boundary_points))
//    {
//        printf("ERROR PARSE!");
//    }
//    
//    BaseShapesContainer *bsc;
//    RegionContainer *region_cont;
//    ShapeRecognitionResult *res;
//    clock_t start = clock();
//    
//    CImage *c_img = numpy_to_c(img);
//    bsc = to_c_shapes_contours(base_shapes);
//    
//    Py_BEGIN_ALLOW_THREADS // Releases the GIL
//
//    region_cont = region_growing(c_img, threshold_rg);
//    bfs(region_cont);
//    subregions(region_cont);
//    res = recognize_relevant_shapes(region_cont, num_points, bsc, threshold_sm, 
//            max_diff_threshold, max_diff_on_mean, min_boundary_points,true);
//
////    usleep(1000000);
//    Py_END_ALLOW_THREADS // Re-acquires the GIL
//            
//    printf("HEREEEEEEE1!!!");
//            
//    PyObject *out = get_principal_region_area(c_img, res);
//            
//    clock_t end = clock();
//            
////    printf("Timing: %f\n", (double) (end-start) / CLOCKS_PER_SEC * 1000.0);
//    
//    delete region_cont;
//    delete bsc;
//    delete res;
//    delete c_img;
//    
////    Py_DECREF(img);
////    Py_DECREF(base_shapes);
//
//    return out; 
//}

/*
 * This is the true function used to find relevant shapes
 */
static PyObject * advanced_region_growing_get_principal_shape_area (PyObject *self, PyObject *args)
{
    // The image to be analyzed
    PyArrayObject *img=0;
    // Threshold for the region growing part
    double threshold_rg=0;
    // Object describing the shapes to match with
    PyObject *base_shapes=0;
    // Threshold for the shape matching part
    double threshold_sm=0;
    // Threshold beyond that the matching stops with negative results
    double max_diff_threshold=0;
    // Threshold on the difference between contour plots means
    double max_diff_on_mean=0;
    // Number of points to use for contours
    int num_points=0;
    // Minimum number of boundary points for matching (to avoid false matching in too small shapes)
    int min_boundary_points=0;
    // Do region growing with or without average on color
    int avg_based_rg = 0;
    
    if(!PyArg_ParseTuple(args, "OdOdddiii", &img, &threshold_rg, &base_shapes, &threshold_sm, &max_diff_threshold, &max_diff_on_mean, &num_points, &min_boundary_points,
            &avg_based_rg))
    {
        printf("ERROR PARSE!");
    }
    
    BaseShapesContainer *bsc;
    RegionContainer *region_cont;
    ShapeRecognitionResult *res;
    clock_t start = clock();
    
    CImage *c_img = numpy_to_c(img);
    bsc = to_c_shapes_contours(base_shapes);
    
    Py_BEGIN_ALLOW_THREADS // Releases the GIL

    region_cont = region_growing(c_img, threshold_rg, avg_based_rg==0?false:true);
    bfs(region_cont);
    subregions(region_cont);
    res = recognize_relevant_shapes(region_cont, num_points, bsc, threshold_sm, 
            max_diff_threshold, max_diff_on_mean, min_boundary_points, true);

//    usleep(1000000);
    Py_END_ALLOW_THREADS // Re-acquires the GIL
            
    npy_intp dims[2];
    dims[0] = c_img->dimensions[0];
    dims[1] = c_img->dimensions[1];
    PyObject *out= PyArray_SimpleNew(2, dims, NPY_FLOAT64);
    
    get_principal_region_area(c_img, res, out);
            
    clock_t end = clock();
      
    
    delete region_cont;
    delete bsc;
    delete res;
    delete c_img;
    
//    Py_DECREF(out);
//    Py_DECREF(base_shapes);

#ifdef MEMORY_BENCHMARK
    membench.print_deficits();
#endif
    
    return out; 
}