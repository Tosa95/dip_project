#include "python_c_data_converters.h"

PyObject * shape_descriptor_to_python (ShapeDescriptor *descriptor)
{
    PyObject *res = PyDict_New();
    
    PyDict_SetItemString(res, "num_pts", 
            PyInt_FromLong(descriptor->get_region()->points->size()));
    
    PyDict_SetItemString(res, "label", 
            PyInt_FromLong(descriptor->get_region()->label));
    
    PyDict_SetItemString(res, "perimeter_ratio", 
            PyFloat_FromDouble(descriptor->get_perimeter_ratio()));
    
        PyDict_SetItemString(res, "area_ratio", 
            PyFloat_FromDouble(descriptor->get_area_ratio()));
    
    PyDict_SetItemString(res, "area_perimeter_ratio", 
            PyFloat_FromDouble(descriptor->get_area_perimeter_ratio()));
    
    PyDict_SetItemString(res, "mean", 
            PyFloat_FromDouble(descriptor->get_mean()));
    
    PyDict_SetItemString(res, "std", 
            PyFloat_FromDouble(descriptor->get_std()));
    
    PyDict_SetItemString(res, "d1_mean", 
            PyFloat_FromDouble(descriptor->get_d1_mean()));
    
    PyDict_SetItemString(res, "d1_std", 
            PyFloat_FromDouble(descriptor->get_d1_std()));
    
    PyDict_SetItemString(res, "mean_color", 
            c_color_to_python(descriptor->get_region()->average));
    
    PyDict_SetItemString(res, "contour", 
            c_double_vector_to_python(descriptor->get_contour()));
    
    return res;
}

PyObject * shape_descriptors_to_python (ShapeDescriptorsContainer *descriptors)
{
    PyObject *res = PyList_New(0);
    
    for (ShapeDescriptor *d : (*(descriptors->get_descriptors())))
    {
        PyList_Append(res, shape_descriptor_to_python(d));
    }
    
    return res;
}

PyObject * c_double_vector_to_python (std::vector<double> *v)
{
    PyObject *res = PyList_New(0);
    
    for (double value : (*v))
    {
        PyList_Append(res, PyFloat_FromDouble(value));
    }
    
    return res;
}

PyObject * c_color_to_python (double *c)
{
    PyObject *res = PyList_New(0);
    

    PyList_Append(res, PyFloat_FromDouble(c[0]));
    PyList_Append(res, PyFloat_FromDouble(c[1]));
    PyList_Append(res, PyFloat_FromDouble(c[2]));
    
    return res;
}
  