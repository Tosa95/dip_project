#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/atan_table.o \
	${OBJECTDIR}/fft.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/memory_bench.o \
	${OBJECTDIR}/python_c_data_converters.o \
	${OBJECTDIR}/python_interface.o \
	${OBJECTDIR}/region_growing.o \
	${OBJECTDIR}/region_growing_2.o \
	${OBJECTDIR}/shape_recognition.o \
	${OBJECTDIR}/subregions.o \
	${OBJECTDIR}/tabulized_atan.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-L/C/Python27/Lib

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/c_region_growing

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/c_region_growing: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/c_region_growing ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/atan_table.o: atan_table.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/include/python2.7 -I/usr/include/python2.7/numpy -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/atan_table.o atan_table.cpp

${OBJECTDIR}/fft.o: fft.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/include/python2.7 -I/usr/include/python2.7/numpy -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/fft.o fft.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/include/python2.7 -I/usr/include/python2.7/numpy -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/memory_bench.o: memory_bench.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/include/python2.7 -I/usr/include/python2.7/numpy -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/memory_bench.o memory_bench.cpp

${OBJECTDIR}/python_c_data_converters.o: python_c_data_converters.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/include/python2.7 -I/usr/include/python2.7/numpy -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/python_c_data_converters.o python_c_data_converters.cpp

${OBJECTDIR}/python_interface.o: python_interface.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/include/python2.7 -I/usr/include/python2.7/numpy -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/python_interface.o python_interface.cpp

${OBJECTDIR}/region_growing.o: region_growing.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/include/python2.7 -I/usr/include/python2.7/numpy -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/region_growing.o region_growing.cpp

${OBJECTDIR}/region_growing_2.o: region_growing_2.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/include/python2.7 -I/usr/include/python2.7/numpy -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/region_growing_2.o region_growing_2.cpp

${OBJECTDIR}/shape_recognition.o: shape_recognition.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/include/python2.7 -I/usr/include/python2.7/numpy -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/shape_recognition.o shape_recognition.cpp

${OBJECTDIR}/subregions.o: subregions.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/include/python2.7 -I/usr/include/python2.7/numpy -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/subregions.o subregions.cpp

${OBJECTDIR}/tabulized_atan.o: tabulized_atan.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/include/python2.7 -I/usr/include/python2.7/numpy -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tabulized_atan.o tabulized_atan.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
