/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   region_growing.h
 * Author: davide
 *
 * Created on 19 maggio 2018, 12.25
 */

#ifndef REGION_GROWING_H_2
#define REGION_GROWING_H_2

#include <stdlib.h>
#include <vector>
#include <unordered_set>
#include <list>
#include <Python.h>
#include <vector>
#include <numpy/arrayobject.h>
#include "image_utils.h"

RegionContainer *region_growing_2(Image *img, std::vector<double> *diff_data);

#endif /* REGION_GROWING_H_2 */

