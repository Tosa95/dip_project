import json
import traceback

import numpy as np
import urllib
import cv2
import requests
import time

MAIN_ENVIRONMENT = True

SERVICE_ADDRESS = "http://192.168.1.133:5000" if MAIN_ENVIRONMENT else "http://192.168.1.32:5000"

print SERVICE_ADDRESS

IMAGES_URL = SERVICE_ADDRESS + "/images/"
LABEL_TYPE_TREE = SERVICE_ADDRESS + "/get_label_type_tree_w_counts/"
RANDOM_IMAGES_URL = SERVICE_ADDRESS + "/get_random_image"
RANDOM_UNBURSTED_IMAGES_URL = SERVICE_ADDRESS + "/get_random_unbursted_image"
NEIGHBORS_URL = SERVICE_ADDRESS + "/get_neighbors/"
NEIGHBORS_REV_URL = SERVICE_ADDRESS + "/get_neighbors_rev/"
ADD_BURST_URL = SERVICE_ADDRESS + "/add_burst"
LABELED_IMAGES = SERVICE_ADDRESS + "/get_random_labeled_images/"
UNSCORED_IMAGES = SERVICE_ADDRESS + "/get_unscored_images/"
ALL_IMAGES = SERVICE_ADDRESS + "/get_all_images"
DELETE_IMAGE = SERVICE_ADDRESS + "/delete_image/"
ADD_SCORE = SERVICE_ADDRESS + "/add_image_score/"
SET_SCORE = SERVICE_ADDRESS + "/set_image_score/"
SET_SCORES = SERVICE_ADDRESS + "/set_images_scores/"
DATASET = SERVICE_ADDRESS + "/get_dataset/"
LABEL_INFO = SERVICE_ADDRESS + "/get_label_info/"
ADD_IMAGE_PREDICTED_LABEL = SERVICE_ADDRESS + "/add_image_predicted_label/"
ADD_IMAGE_PREDICTED_LABELS = SERVICE_ADDRESS + "/add_image_predicted_labels/"

# METHOD #1: OpenCV, NumPy, and urllib
def url_to_image(url):
    # download the image, convert it to a NumPy array, and then read
    # it into OpenCV format
    # print url
    resp = urllib.urlopen(url)
    # print resp
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)

    # return the image
    return image

def get_random_image_id():
    r = requests.get(RANDOM_IMAGES_URL)
    return r.json()["ID_image"]

def get_label_type_tree(base_type_id=1):
    full_path = LABEL_TYPE_TREE + "%d" % base_type_id
    r = requests.get(full_path)
    return r.json()["tree"]

def get_random_unbursted_image_id():
    r = requests.get(RANDOM_UNBURSTED_IMAGES_URL)
    return r.json()["ID_image"]

def get_img_url(id):
    return IMAGES_URL + str(id)

def get_img_by_id(id):
    return url_to_image(get_img_url(id))

def get_labeled_images(lbl_type, lbl_value, img_num):

    full_path = LABELED_IMAGES + "%d/%d/%d" % (lbl_type, lbl_value, img_num)

    # r = requests.get(full_path)
    # print r.text

    r = requests.get(full_path)
    return r.json()["images"]

def get_all_unscored_images(ID_score):

    full_path = UNSCORED_IMAGES + str(ID_score)

    r = requests.get(full_path)
    return r.json()["images"]

def get_all_images():

    full_path = ALL_IMAGES

    r = requests.get(full_path)
    return r.json()["images"]

def add_image_score (ID_image, ID_score, value, ID_predicted_label_type = None, ID_predicted_label_value = None):

    if ID_predicted_label_type is None:
        full_path = ADD_SCORE + "%d/%d/%f" % (ID_image, ID_score, value)
    else:
        full_path = ADD_SCORE + "%d/%d/%f/%d/%d" % (ID_image, ID_score, value,
                                                    ID_predicted_label_type, ID_predicted_label_value)

    # r = requests.get(full_path)
    # print full_path, r.text

    r = requests.get(full_path)
    return r.json()

def set_image_score (ID_image, ID_score, value, ID_predicted_label_type = None, ID_predicted_label_value = None):

    if ID_predicted_label_type is None:
        full_path = SET_SCORE + "%d/%d/%f" % (ID_image, ID_score, value)
    else:
        full_path = SET_SCORE + "%d/%d/%f/%d/%d" % (ID_image, ID_score, value,
                                                    ID_predicted_label_type, ID_predicted_label_value)

    # r = requests.get(full_path)
    # print full_path, r.text

    r = requests.get(full_path)
    return r.json()

def set_images_scores (data):

    full_path = SET_SCORES + "%s" % json.dumps(data)
    r = requests.get(full_path)
    return r.json()

def delete_image(img_id):

    full_path = DELETE_IMAGE + str(img_id)

    r = requests.get(full_path)
    print r.json()

def get_dataset (dataset_description, balanced=True, group_count_limit=0):
    full_path = DATASET + "%s/%d/%d" % (json.dumps(dataset_description), (1 if balanced else 0), group_count_limit)

    r = requests.get(full_path)
    return r.json()["dataset"]

def get_label_info(label_type_id, label_value_id):
    full_path = LABEL_INFO + "%d/%d" % (label_type_id, label_value_id)

    r = requests.get(full_path)
    return r.json()["data"]

def add_image_predicted_label(ID_image, value, ID_predicted_label_type = None, ID_predicted_label_value = None):
    full_path = ADD_IMAGE_PREDICTED_LABEL + "%d/%f/%d/%d" % (ID_image, value,
                                                ID_predicted_label_type, ID_predicted_label_value)
    r = requests.get(full_path)
    return r.json()

def add_image_predicted_labels(labels):

    full_path = ADD_IMAGE_PREDICTED_LABELS + "%s" % json.dumps(labels)
    r = requests.get(full_path)
    return r.json()