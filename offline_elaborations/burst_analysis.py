# import the necessary packages
import traceback

import numpy as np
import urllib
import cv2
import requests
import time

from global_settings import get_settings
from service_access import get_img_by_id, get_random_image_id, NEIGHBORS_URL, NEIGHBORS_REV_URL, \
    get_random_unbursted_image_id, ADD_BURST_URL
from utils.image_caching import ImageCache

RESHAPE_X = 300
RESHAPE_Y = 300

MAX_SIZE_DIFF_COEFF = 1.2

MAX_DIFF = 10000

def img_diff(img1, img2):

    (h1, w1, _) = img1.shape
    (h2, w2, _) = img2.shape

    if h1 > h2*MAX_SIZE_DIFF_COEFF or h2 > h1*MAX_SIZE_DIFF_COEFF:
        return MAX_DIFF

    if w1 > w2*MAX_SIZE_DIFF_COEFF or w2 > w1*MAX_SIZE_DIFF_COEFF:
        return MAX_DIFF

    img1 = cv2.resize(img1, (RESHAPE_X,RESHAPE_Y))
    img2 = cv2.resize(img2, (RESHAPE_X, RESHAPE_Y))

    return np.sqrt(np.sum(np.power(np.subtract(img1, img2),2)))/(RESHAPE_X*RESHAPE_Y)

def analyze_mean_case(num_samples):
    results = []

    for i in xrange(num_samples):

        print i
        img1 = get_img_by_id(get_random_image_id())
        img2 = get_img_by_id(get_random_image_id())

        results.append(img_diff(img1, img2))

    return np.mean(np.array(results)), np.std(np.array(results))
#0.0586283872924 0.00261015841417

def forward_pass(orig_img, neigh, eq_th, img_cache=ImageCache(get_settings().get_img_cache_path())):

    res = set()

    curr_img = orig_img

    for img_id in neigh:
        img = img_cache.get_image_by_id(img_id)

        diff = img_diff(curr_img, img)

        if diff <= eq_th:
            res.add(img_id)
            curr_img = img


    return res


def get_burst_ids(inital_ID, seconds, eq_th, img_cache=ImageCache(get_settings().get_img_cache_path())):

    orig_img = img_cache.get_image_by_id(inital_ID)

    # r = requests.get(NEIGHBORS_URL + "%d/%d" % (inital_ID, seconds))
    # print r.text

    neighbors = requests.get(NEIGHBORS_URL + "%d/%d" % (inital_ID, seconds)).json()["neighbors"]
    neighbors_rev = requests.get(NEIGHBORS_REV_URL + "%d/%d" % (inital_ID, seconds)).json()["neighbors"]

    ids = set()
    ids.add(inital_ID)

    for n in [neighbors, neighbors_rev]:
        ids.update(forward_pass(orig_img, n, eq_th))

    return ids

def compute_average_burst():
    results = []
    for i in xrange(100):
        id = get_random_image_id()

        burst = get_burst_ids(id, 20, 0.048)
        results.append(len(burst))

        print "%d => %d" % (i, len(burst))

    print np.mean(np.array(results)), np.std(np.array(results))
#12.45 25.7803704395
#17.54 33.9135430175

def burst_analysis (num_bursts, seconds = 20, eq_th = 0.050):

    for i in xrange(num_bursts):

        try:
            id = get_random_unbursted_image_id()

            burst = get_burst_ids(id, seconds, eq_th)

            print "%d => %d" % (i, len(burst))

            if len(burst) > 1:

                    r = requests.post(ADD_BURST_URL, json=list(burst))
                    print r.text


        except Exception as e:
            print e
            traceback.print_exc()
            time.sleep(10)

burst_analysis(10000)

# for id in burst:
#
#     cv2.imshow('result-' + str(id), get_img_by_id(id))

# while cv2.waitKey(1) != 27:
#     pass


# analyze_mean_case(10)

# img1 = get_img_by_id(16568)
# img2 = get_img_by_id(8642)
#
# print img_diff(img1, img2)

# url = IMAGES_URL + str(16568)
# print url
#
# cv2.imshow('result', url_to_image(IMAGES_URL + str(get_random_image_id())))
# while cv2.waitKey(1) != 27:
#     pass