import os
import time
import random

# Imports the Google Cloud client library
import cv2
from google.cloud import vision
from google.cloud.vision import types

from service_access import get_labeled_images, get_all_images, set_image_score, get_img_by_id
from cloud_vision import label_detection_traffic_sign, label_web_det_traffic_sign
from utils.image_caching import ImageCache
from global_settings import get_settings


DATASET_STRUCTURE_SIGN = [
    ["pericolo",[2,3]],
    ["precedenza",[2,4]],
    ["divieto",[2,5]],
    ["obbligo",[2,6]],
    ["autovelox",[2,172]],
    ["altro",[2,173]],
    ["semaforo",[2,174]]
]

N_IMG = 6000
SIGNAL_SCORE_ID = 3

all = False
cache = ImageCache(get_settings().get_img_cache_path())

# Autenticazione a Google (con file json creato tramite Google Cloud Platform)
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "ProgettoMLDM-traffic-signs-38611b4202f7.json"

# Instantiates a client
client = vision.ImageAnnotatorClient()

num_img = {}

if all:
    image_ids = get_all_images()
    random.shuffle(image_ids)
    total_sign = 6000
else:
    image_ids = []
    for i in DATASET_STRUCTURE_SIGN:
        lbl = i[0]
        im_ids = [(img_id, lbl) for img_id in get_labeled_images(i[1][0], i[1][1], N_IMG)]
        image_ids.extend(im_ids)
        num_img[lbl] = [len(im_ids), 0]
    total_sign = len(image_ids)
    lbl = "nosig"
    image_ids.extend([(img_id, lbl) for img_id in get_labeled_images(1, 2, N_IMG)])
    random.shuffle(image_ids)

predicted_signs = 0
nosig_predicted_signs = 0

nps_filepath = "/home/simona/ProgettoMLDM/dip_project/offline_elaborations/google_api/nosig_predicted_signs_2/"
notfound_filepath = "/home/simona/ProgettoMLDM/dip_project/offline_elaborations/google_api/ts_not_found_2/"

for index, (image_id, label) in enumerate(image_ids):

    print "\n", index, label, image_id

    err = True
    image = None
    try:
        content = cache.read_image(image_id)
        image = types.Image(content=content)

        score = label_web_det_traffic_sign(client, image)

        if score > 0:
            predicted_signs += 1
            print "TRAFFIC SIGN FOUND!"
            if label != "nosig":
                num_img[label][1] += 1
            else:
                nosig_predicted_signs += 1
                cv2.imwrite(nps_filepath + str(image_id) + ".jpeg", get_img_by_id(image_id))
            print num_img
            print nosig_predicted_signs
        else:
            if label != "nosig":
                print "TS NOT FOUND!"
                cv2.imwrite(notfound_filepath + str(image_id) + ".jpeg", get_img_by_id(image_id))

        set_image_score(image_id, SIGNAL_SCORE_ID, score)

    except Exception as ex:
        time.sleep(3)
        print ex

print "\nTotal signs: %d \nPredicted signs: %d" % (total_sign, predicted_signs)
print num_img
print "\nNo signs predicted as signs: %d" % nosig_predicted_signs
