import io
import os

# Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types

from cloud_vision import label_detection_traffic_sign, web_detection, label_web_det_traffic_sign

FOLDER = os.path.join(os.path.dirname(__file__), 'Img_traffic_signs_prova')


# Autentication to Google (using file json created with Google Cloud Platform)
# Without this the script will give an error
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "ProgettoMLDM-traffic-signs-38611b4202f7.json"

# Instantiates a client
client = vision.ImageAnnotatorClient()

# label detection test performed using 9 different images
for i in range(1, 10):
    # The name of the image file to annotate
    file_name = FOLDER + '/%d.jpeg' % i
    print("\n\n"+file_name)

    # Loads the image into memory
    with io.open(file_name, 'rb') as image_file:
        content = image_file.read()

    image = types.Image(content=content)

    # Performs label detection on the image file
    # print "\nLabel detection with traffic_light = true:"
    # score_tl = label_detection_traffic_sign(client, image)  # default traffic_lights is True
    #
    # print "\nLabel detection with traffic_light = false:"
    # score_no_tl = label_detection_traffic_sign(client, image, traffic_lights=False)
    #
    # print "\nLabel detection with verbose = true:"
    # score_verbose = label_detection_traffic_sign(client, image, verbose=True)
    #
    print "\nWeb detection:"
    wd = web_detection(client, image)

    print "\nLabel and Web detection \n"
    lwd = label_web_det_traffic_sign(client, image, verbose=True)
