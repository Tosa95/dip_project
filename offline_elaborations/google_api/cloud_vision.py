# Google Cloud client library
from google.cloud import vision


def label_detection_traffic_sign(client, image, traffic_lights=True, verbose=False):
    """
    Performs label detection on the image file passed as input and assign a score to the image to classify it as a
    traffic sign or not.
    :param client:
    :param image: The file containing the image
    :param traffic_lights: If this boolean is true the images recognised as traffic light are considered sign, if false
    those images will be classified as "not a sign" (even if the label "traffic light" is detected it won't increase
    the final score of the image)
    :param verbose: if true this method will also print all the labels detected and their score
    :return: An int (score) which represent the probability of the image to be a traffic sign (zero if no traffic sign
    related labels have been detected)
    """
    response = client.label_detection(image=image)
    labels = response.label_annotations

    count_labels = 0
    score = 0.00

    if verbose:
        print('Labels:')

    for label in labels:
        description = label.description

        if verbose:
            print(description + " %1.2f" % label.score)

        if (description == "Traffic sign") or (description == "Sign") or (description == "Signage") \
                or (description == "Logo") or (description == "Symbol") or (description == "Stop sign"):
            score += label.score
            count_labels += 1
        elif traffic_lights:
            if description == "Traffic light":
                score += label.score
                count_labels += 1

    if score != 0.00:
        score = score/count_labels

    print("Final score: %.2f" % score)

    return score


def web_detection(client, image):
    response = client.web_detection(image=image)
    annotations = response.web_detection

    if annotations.best_guess_labels:
        for label in annotations.best_guess_labels:
            print('\nBest guess label: {}'.format(label.label))

    if annotations.web_entities:
        print('\n{} Web entities found: '.format(len(annotations.web_entities)))

        for entity in annotations.web_entities:
            print('\n\tScore      : {}'.format(entity.score))
            print(u'\tDescription: {}'.format(entity.description))


def label_web_det_traffic_sign(client, image, traffic_lights=True, verbose=False):
    response = client.label_detection(image=image)
    labels = response.label_annotations

    count_labels = 0
    score = 0.00

    if verbose:
        print('Labels:')

    for label in labels:
        description = label.description

        if verbose:
            print(description + " %1.2f" % label.score)

        if (description == "Traffic sign") or (description == "Sign") or (description == "Signage") \
                or (description == "Logo") or (description == "Symbol") or (description == "Stop sign"):
            score += label.score
            count_labels += 1
        elif traffic_lights:
            if description == "Traffic light":
                score += label.score
                count_labels += 1

    response = client.web_detection(image=image)
    annotations = response.web_detection

    if annotations.web_entities:
        if verbose:
            print('\n{} Web entities found: '.format(len(annotations.web_entities)))

        for entity in annotations.web_entities:
            if verbose:
                print(entity.description + " %1.2f" % entity.score)

            if (entity.description == "Traffic sign") or (entity.description == "Sign") \
                    or (entity.description == "Signage") or (entity.description == "Logo") \
                    or (entity.description == "Symbol") or (entity.description == "Stop sign"):
                score += entity.score
                count_labels += 1
            elif traffic_lights:
                if entity.description == "Traffic light":
                    score += entity.score
                    count_labels += 1

    if score != 0.00:
        score = score / count_labels

    print("\nFinal score: %.2f" % score)

    return score


def detect_labels_uri(client, uri):
    """
    Detects labels in the file located in Google Cloud Storage or on the Web and prints those labels.
    """
    image = vision.types.Image()
    image.source.image_uri = uri

    label_detection(client, image)


def label_detection(client, image):
    """
    Performs label detection on the image file and prints the labels.
    """
    response = client.label_detection(image=image)
    labels = response.label_annotations

    print('Labels:')
    for label in labels:
        print(label.description)
