import cv2

from service_access import get_img_by_id
from utils.dataset_creator import get_random_dataset_flatten
from utils.image_extenders import FlipImageExtender
from utils.images import LastSeen

DATASET_STRUCTURE = [
    #["a", [[6,104]]],
    ["b", [[4,58]]],
    #["c", [[6,91]]]
]

rows = 2
cols = 10

ids = get_random_dataset_flatten(DATASET_STRUCTURE, cols)

fie = FlipImageExtender()

ls = LastSeen(img_per_column=rows,columns=cols)

for id in ids:
    img = get_img_by_id(id)
    ls.add_img(img)
    ls.add_img(fie.extend(img))


cv2.imshow("test",ls.get_grid())
cv2.waitKey()