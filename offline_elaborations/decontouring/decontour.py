import random
from math import sqrt

import cv2
import advanced_region_growing
import numpy as np

from global_settings import get_settings
from utils.image_caching import ImageCache
from utils.image_extenders import ImageExtender

def resize(img, megapixels):

    megapixels = megapixels*1000000

    height, width, _ = img.shape

    original_aspect_ratio = float(height)/width

    new_width = sqrt(megapixels/original_aspect_ratio)

    new_height = original_aspect_ratio*new_width

    res = cv2.resize(img, (int(new_width), int(new_height)))

    return res, new_width/width, new_height/height

def get_principal_shape_area(img, bs, no_avg_th_range=(2, 13), avg_th_range=(10, 28), do_closing=True, take_greater_th=0.35):
    height, width, _ = img.shape

    # imgb = cv2.GaussianBlur(img, (3, 3), 0).astype(np.uint8)
    imgb = cv2.bilateralFilter(img, 7, 75, 75)

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))

    for th in range(*no_avg_th_range):

        result_no_avg = advanced_region_growing.get_principal_shape_area(imgb, th, bs, 0.9,
                                                                         3.0, 0.12, 128, 15, 0)
        if result_no_avg[0, 0] != -1:
            # print th
            break

    #	result_no_avg = cv2.dilate(result_no_avg, kernel, iterations=1)
    if do_closing:
        result_no_avg = cv2.morphologyEx(result_no_avg, cv2.MORPH_CLOSE, kernel)
    result_no_avg = cv2.GaussianBlur(result_no_avg, (5, 5), 0)

    for th in range(*avg_th_range):

        result_avg = advanced_region_growing.get_principal_shape_area(imgb, th, bs, 0.9,
                                                                      3.0, 0.12, 128, 15, 1)
        if result_avg[0, 0] != -1:
            # print th
            break

    #	result_no_avg = cv2.dilate(result_no_avg, kernel, iterations=1)
    if do_closing:
        result_avg = cv2.morphologyEx(result_avg, cv2.MORPH_CLOSE, kernel)
    result_avg = cv2.GaussianBlur(result_avg, (5, 5), 0)

    center_row = int(height / 2)
    center_col = int(width / 2)

    has_center_no_avg = True if result_no_avg[center_row, center_col] >= 0.99 else False
    has_center_avg = True if result_avg[center_row, center_col] >= 0.99 else False

    if has_center_no_avg and not has_center_avg:
        final_result = result_no_avg
    elif has_center_avg and not has_center_no_avg:
        final_result = result_avg
    elif has_center_no_avg or has_center_avg:
        totpix = float(width * height)
        nz_avg = int(np.count_nonzero(result_avg))
        nz_no_avg = int(np.count_nonzero(result_no_avg))
        diff = float(abs(nz_avg - nz_no_avg) * 1.001) / totpix
        # print "%d %d %f" % (nz_no_avg, nz_avg, diff)

        # if diff < take_greater_th:
        #     final_result = result_avg if nz_avg > nz_no_avg else result_no_avg
        # else:
        #     final_result = result_avg if nz_avg < nz_no_avg else result_no_avg

        final_result = np.clip(np.add(result_avg, result_no_avg), 0.0, 1.0)
    else:
        final_result = None

    if final_result is not None:
        final_result = np.repeat(final_result[:, :, np.newaxis], 3, axis=2)

    return final_result

def get_principal_shape_area_deb(img, bs, no_avg_th_range=(2, 13), avg_th_range=(10, 28), do_closing=True, take_greater_th=0.35):
    height, width, _ = img.shape

    # imgb = cv2.GaussianBlur(img, (3, 3), 0).astype(np.uint8)
    imgb = cv2.bilateralFilter(img, 7, 75, 75)

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))

    for th in range(*no_avg_th_range):

        result_no_avg = advanced_region_growing.get_principal_shape_area(imgb, th, bs, 0.9,
                                                                         3.0, 0.12, 128, 15, 0)
        if result_no_avg[0, 0] != -1:
            # print th
            break

    #	result_no_avg = cv2.dilate(result_no_avg, kernel, iterations=1)
    if do_closing:
        result_no_avg = cv2.morphologyEx(result_no_avg, cv2.MORPH_CLOSE, kernel)
    result_no_avg = cv2.GaussianBlur(result_no_avg, (5, 5), 0)

    for th in range(*avg_th_range):

        result_avg = advanced_region_growing.get_principal_shape_area(imgb, th, bs, 0.9,
                                                                      3.0, 0.12, 128, 15, 1)
        if result_avg[0, 0] != -1:
            # print th
            break

    #	result_no_avg = cv2.dilate(result_no_avg, kernel, iterations=1)
    if do_closing:
        result_avg = cv2.morphologyEx(result_avg, cv2.MORPH_CLOSE, kernel)
    result_avg = cv2.GaussianBlur(result_avg, (5, 5), 0)

    center_row = int(height / 2)
    center_col = int(width / 2)

    has_center_no_avg = True if result_no_avg[center_row, center_col] >= 0.99 else False
    has_center_avg = True if result_avg[center_row, center_col] >= 0.99 else False

    if has_center_no_avg and not has_center_avg:
        final_result = result_no_avg
    elif has_center_avg and not has_center_no_avg:
        final_result = result_avg
    elif has_center_no_avg or has_center_avg:
        totpix = float(width * height)
        nz_avg = int(np.count_nonzero(result_avg))
        nz_no_avg = int(np.count_nonzero(result_no_avg))
        diff = float(abs(nz_avg - nz_no_avg) * 1.001) / totpix
        # print "%d %d %f" % (nz_no_avg, nz_avg, diff)

        # if diff < take_greater_th:
        #     final_result = result_avg if nz_avg > nz_no_avg else result_no_avg
        # else:
        #     final_result = result_avg if nz_avg < nz_no_avg else result_no_avg

        final_result = np.clip(np.add(result_avg, result_no_avg), 0.0, 1.0)
    else:
        final_result = np.zeros((height,width))

    final_result = np.repeat(final_result[:, :, np.newaxis], 3, axis=2)
    result_avg = np.repeat(result_avg[:, :, np.newaxis], 3, axis=2)
    result_no_avg = np.repeat(result_no_avg[:, :, np.newaxis], 3, axis=2)

    return result_avg, result_no_avg, final_result

def swap_background (img, new_back, mm, bs, do_closing = True):

    height, width, _ = img.shape

    res_best = get_principal_shape_area(img, bs, do_closing=do_closing)

    if res_best is not None:

        img_no_back = img * res_best
        back_no_img = new_back * (1 - res_best)

        back_swap_img = img_no_back + back_no_img

        pred_lbl, pred_value = mm.get_prediction_label_perc(back_swap_img)

        if pred_lbl == "sig":
            pred_perc = pred_value
        else:
            pred_perc = 1 - pred_value

        return back_swap_img, pred_perc

    return new_back, -1.0

def swap_background_w_mask_deb (img, mask, new_back):

    height, width, _ = img.shape

    new_back = same_res_crop(img, new_back)

    res_best = mask

    if res_best is not None:

        img_no_back = img * res_best
        back_no_img = new_back * (1 - res_best)

        back_swap_img = img_no_back + back_no_img

        return back_swap_img

    return new_back, -1.0

def swap_background_mask(img,mask,new_back):
    height, width, _ = img.shape

    new_back = same_res_crop(img, new_back)

    res_best = mask

    if res_best is not None:

        img_no_back = img * res_best
        back_no_img = new_back * (1 - res_best)

        back_swap_img = img_no_back + back_no_img

        return back_swap_img

    return None

def swap_background_mask_prediction (img, mask, new_back, mm, ref_back=None):

    swapped = swap_background_mask(img,mask,new_back)

    if swapped is not None:

        if ref_back is None:
            pred_lbl, pred_value = mm.get_prediction_label_perc(swapped)
        else:
            swapped_ref = swap_background_mask(img,mask,ref_back)
            pred_lbl, pred_value = mm.get_prediction_label_perc(swapped_ref)

        if pred_lbl == "sig":
            pred_perc = pred_value
        else:
            pred_perc = 1 - pred_value

        # print "P: %.3f" % pred_perc

        return swapped, pred_perc

    return new_back, -1.0

def same_res_crop(img, back):

    ih, iw, _ = img.shape
    bh, bw, _ = back.shape

    if bh<ih or bw<iw:
        return cv2.resize(back, (iw,ih))

    h_diff = bh - ih
    w_diff = bw - iw

    start_h = random.randint(0,h_diff)
    start_w = random.randint(0,w_diff)

    return back[start_h:start_h+ih,start_w:start_w+iw,:]

class BackgroundSwapCompoundImageExtender(ImageExtender):

    def __init__(self, img_morph_extenders, img_non_morph_extendes, back_extenders, background_ids, base_shapes,
                 sign_decision_model_manager, img_cache=ImageCache(get_settings().get_img_cache_path()),
                 resize_mp=0.01, reference_back_id=None, threshold_perc=0.3, force_failure=False):
        self._img_morph_extenders = img_morph_extenders
        self._img_non_morph_extenders = img_non_morph_extendes
        self._back_extenders = back_extenders
        self._background_ids = background_ids
        self._img_cache = img_cache
        self._base_shapes = base_shapes
        self._sign_decision_model_manager = sign_decision_model_manager
        self._resize_mp = resize_mp
        self._reference_back_id = reference_back_id
        self._threshold_perc = threshold_perc
        self._ref_back = img_cache.get_image_by_id(reference_back_id)
        self._force_failure = force_failure

    def extend_no_back(self, img):
        img_morph = self._img_morph_extenders.extend(img)
        img_ext = self._img_non_morph_extenders.extend(img_morph)

        return img_ext

    def extend(self, img, replication=True):

        img, _, _ = resize(img,self._resize_mp)

        seed = random.randint(0,12345678789)

        mask = get_principal_shape_area(img, self._base_shapes)

        if mask is not None:

            random.seed(seed)
            img_morph = self._img_morph_extenders.extend(img, replication=False)

            m_height, m_width, _ = img_morph.shape

            random.seed(seed)
            mask_morph = self._img_morph_extenders.extend(mask, replication=False)

            img_ext = self._img_non_morph_extenders.extend(img_morph)

            img_ext = cv2.resize(img_ext, (m_width, m_height))

            back = self._img_cache.get_image_by_id(random.choice(self._background_ids))

            # back,_,_ = resize(back, self._resize_mp)

            back_ext = self._back_extenders.extend(back)

            res = swap_background_mask(img_ext, mask_morph, back_ext)
            _, perc = swap_background_mask_prediction(img, mask, back_ext, self._sign_decision_model_manager, ref_back=self._ref_back)

            if perc > self._threshold_perc and not self._force_failure:
                return res
            else:
                return self._img_non_morph_extenders.extend(self._img_morph_extenders.extend(img, replication=True))

        else:
            return self.extend_no_back(img)



