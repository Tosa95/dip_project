import os
import pickle
from io import open
import numpy as np



def load_shapes_plots():

    fname = os.path.dirname(os.path.os.path.realpath(__file__)) + "/contours.pickle"
	
    if not os.path.isfile(fname):
        print "ERROR!!!"
    else:

        with open(fname, "rb") as f:
            plots = pickle.load(f)

    return plots

# plots = load_shapes_plots()
# correlation = get_correlation(plots["circle"], plots["triangle"])
# print np.argmax(correlation)
# print correlation[np.argmax(correlation)]
# print correlation
