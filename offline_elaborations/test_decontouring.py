from decontouring.load_shapes import load_shapes_plots
from decontouring.decontour import get_principal_shape_area, swap_background, BackgroundSwapCompoundImageExtender, \
    get_principal_shape_area_deb, swap_background_w_mask_deb
from global_settings import get_settings
from service_access import get_img_by_id, url_to_image
from utils.dataset_creator import get_random_dataset_flatten
import cv2
import advanced_region_growing
import numpy as np

from utils.image_caching import ImageCache
from utils.image_extenders import CompoundImageExtender, PerspectiveImageExtender, TranslationImageExtender, \
    BlurImageExtender, NoiseImageExtender, LuminosityImageExtender, ColourTemperatureImageExtender, \
    SaturationImageExtender, ALL_IMAGES_EXTENDERS
from utils.images import LastSeen, put_text_on_image
import random

from utils.model_manager import ModelManager

base_shapes = load_shapes_plots()

bs = {}

for shape_name, shape_contours in base_shapes.iteritems():
    bs[shape_name] = [sc.tolist() for sc in shape_contours]

DATASET_STRUCTURE = [
    # 	["div_sosta", [[5,36]]],
    # 	["dritto", [[4,57]]],
    # 	["rotatoria", [[6,113]]],
    # 	["bus", [[5,20]]],
    # ["110", [[5, 170]]],
    # 	["stambecco", [[6,111]]],
    ["detriti", [[6,115]]],
    # ["si", [[1, 1]]]
]

DATASET_STRUCTURE_NO = [
    ["no", [[1, 2]]]
]

ids_no = get_random_dataset_flatten(DATASET_STRUCTURE_NO, 10000)

id_check = 11551

mm = ModelManager("signosig_ep17_vl0.148822210102_va0.95037593985", get_settings().get_models_path(),
                  image_cache=ImageCache(get_settings().get_img_cache_path()))

bscie = BackgroundSwapCompoundImageExtender(
    img_morph_extenders=CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30, 50)),
            PerspectiveImageExtender(dy_range=(30, 65)),
            PerspectiveImageExtender(dz_range=(10, 20)),
            TranslationImageExtender(x_range=(0.05, 0.15)),
            TranslationImageExtender(y_range=(0.05, 0.15)),
            BlurImageExtender(x_range=(0.03, 0.07)),
            BlurImageExtender(y_range=(0.03, 0.07)),
        ],
        (2, 5),
        final_crop=0.0
    ),
    img_non_morph_extendes=CompoundImageExtender(
        [
            NoiseImageExtender(amt_range=(5, 20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6)),
            ColourTemperatureImageExtender(b_range=(0.8, 1.2), r_range=(0.8, 1.2)),
            SaturationImageExtender(amt_range=(0.5, 1.5))
        ],
        (2, 4),
        final_crop=0.0
    ),
    back_extenders=CompoundImageExtender(
        [
            NoiseImageExtender(amt_range=(5, 20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6)),
            ColourTemperatureImageExtender(b_range=(0.9, 1.1), r_range=(0.9, 1.1)),
            SaturationImageExtender(amt_range=(0.5, 1.5))
        ],
        (2, 3)
    ),
    background_ids=ids_no,
    base_shapes=bs,
    sign_decision_model_manager=mm,
    reference_back_id=11551,
    force_failure=False
)

# for i in range(100):
#     ls_back = LastSeen(2,12)
#     for id in random.sample(ids_no,20):
#         img = get_img_by_id(id)
#         perc_img = np.zeros((1, 100, 3))
#         perc_img = put_text_on_image(perc_img, "%d" % id)
#
#         ls_back.add_img(img)
#         ls_back.add_img(perc_img)
#
#     cv2.imshow("back", ls_back.get_grid())
#     cv2.waitKey(0)
#
# back_for_checking = get_img_by_id(id_check)
#
# for i in range(50):
#
#     ids = get_random_dataset_flatten(DATASET_STRUCTURE, 20)
#
#     ids = [97943,110071,51079,78354,49838,65751,39631,82262]
#
#     ls = LastSeen(9, 10)
#
#     for id in ids:
#         img = get_img_by_id(id)
#
#         height, width, _ = img.shape
#
#         back_id = random.choice(ids_no)
#
#         back = get_img_by_id(back_id)
#
#         mask_avg, mask_no_avg, mask_ensemble = get_principal_shape_area_deb(img, bs)
#
#         # res, perc_pred = swap_background(img, back, mm, bs)
#
#         mask_ensemble_rev = 1-mask_ensemble
#         img_no_back = img * mask_ensemble
#         back_no_img = cv2.resize(back,(width,height)) * mask_ensemble_rev
#
#         back_avg = swap_background_w_mask_deb(img, mask_avg, back)
#         back_no_avg = swap_background_w_mask_deb(img, mask_no_avg, back)
#         back_ensemble = swap_background_w_mask_deb(img, mask_ensemble, back)
#
#         img_check = SaturationImageExtender(amt_range=(1.8,1.8)).extend(img)
#
#         back_check = swap_background_w_mask_deb(img_check, mask_ensemble, back_for_checking)
#
#         pred_lbl, pred_value = mm.get_prediction_label_perc(back_ensemble)
#
#         if pred_lbl == "sig":
#             pred_perc = pred_value
#         else:
#             pred_perc = 1 - pred_value
#
#         perc_img = np.zeros((1,100,3))
#
#         perc_img = put_text_on_image(perc_img, "E: %.3f" % pred_perc, (0, 255, 0) if pred_perc > 0.3 else (0, 0, 255))
#         perc_img = put_text_on_image(perc_img, "%d" % id)
#         perc_img = put_text_on_image(perc_img, "%d" % back_id)
#
#         pred_lbl, pred_value = mm.get_prediction_label_perc(back_check)
#
#         if pred_lbl == "sig":
#             pred_perc = pred_value
#         else:
#             pred_perc = 1 - pred_value
#
#         perc_img = put_text_on_image(perc_img, "C: %.3f" % pred_perc, (0, 255, 0) if pred_perc > 0.3 else (0, 0, 255))
#
#         ls.add_img(img.astype(np.uint8))
#
#         # ls.add_img((mask_avg*255).astype(np.uint8))
#         # ls.add_img(back_avg.astype(np.uint8))
#         # ls.add_img((mask_no_avg*255).astype(np.uint8))
#         # ls.add_img(back_no_avg.astype(np.uint8))
#
#         ls.add_img((mask_ensemble_rev * 255).astype(np.uint8))
#         ls.add_img(img_no_back.astype(np.uint8))
#         ls.add_img(back_no_img.astype(np.uint8))
#         ls.add_img(back.astype(np.uint8))
#
#         ls.add_img((mask_ensemble*255).astype(np.uint8))
#         ls.add_img(back_ensemble.astype(np.uint8))
#         ls.add_img(perc_img.astype(np.uint8))
#         ls.add_img(back_check.astype(np.uint8))
#
#
#         # if res is not None:
#         #     ls.add_img((img).astype(np.uint8))
#         #     ls.add_img(
#         #         (put_text_on_image(res, "%.2f" % perc_pred, (0, 255, 0) if perc_pred > 0.3 else (0, 0, 255))).astype(
#         #             np.uint8))
#         #     ls.add_img((bscie.extend(img)).astype(np.uint8))
#         # ls.add_img((img_no_back).astype(np.uint8))
#         # ls.add_img((back_swap_img).astype(np.uint8))
#
#     cv2.imshow("segmented", ls.get_grid())
#     cv2.waitKey(0)

ids = get_random_dataset_flatten(DATASET_STRUCTURE, 10)
# ids = [82262]
r, c = 7, 12
ls = LastSeen(r, c)

for i in range(r * c + 5):
    img = get_img_by_id(random.choice(ids))

    mask_avg, mask_no_avg, mask_ensemble = get_principal_shape_area_deb(img, bs)

    mask_ensemble_rev = 1 - mask_ensemble
    img_no_back = img * mask_ensemble

    height,width,_ = img.shape

    back = cv2.resize(get_img_by_id(17489),(width,height))

    back_no_img = back * mask_ensemble_rev

    res = img_no_back + back_no_img

    # img = cv2.imread("test_10p.jpg")
    #
    # sat = SaturationImageExtender(amt_range=(0.7,0.7))
    # img = sat.extend(img)
    #
    # lum = LuminosityImageExtender(amt_range=(0.7,0.7))
    # img = lum.extend(img)
    # s = random.randint(1, 1000)
    # random.seed(s)
    ls.add_img(bscie.extend(img).astype(np.uint8))
    # random.seed(s)
    # imgext = ALL_IMAGES_EXTENDERS.extend(img_no_back,replication=False)
    # ls.add_img(imgext.astype(np.uint8))
    # random.seed(s)
    # maskext = ALL_IMAGES_EXTENDERS.extend(mask_ensemble, replication=False)
    # ls.add_img((maskext*255).astype(np.uint8))
    # height, width, _ = imgext.shape
    # back = cv2.resize(get_img_by_id(17489), (width, height))
    # res = imgext*maskext + back*(1.0-maskext)
    # ls.add_img(res.astype(np.uint8))




cv2.imshow("heheh", ls.get_grid())
cv2.waitKey(0)
