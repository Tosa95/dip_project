import json
import os
from cmath import sqrt
from collections import namedtuple
from datetime import datetime

from global_settings import get_settings
from utils.dataset_creator import DatasetCreator
from utils.image_caching import ImageCache
from utils.model_manager import ModelManager
from utils.neural_training import NeuralNetworkTrainer
from utils.saving_methods import MemoryDatasetPersister
import numpy as np
import pandas as pd

ModelAndDataset = namedtuple("ModelAndDataset",["name","model_creation_func","dataset_parameters"])

class MultipleModelsTrainer(object):

    def __init__(self,job_name,models=None,tests_per_model=5,
                 models_folder=get_settings().get_models_path(),
                 image_cache=ImageCache(get_settings().get_img_cache_path()),
                 patience=5):
        self._job_name = job_name
        self._models = models
        self._tests_per_model = tests_per_model
        self._base_folder = models_folder
        self._data = None
        self._img_cache = image_cache
        self._patience = patience
        self._job_root = os.path.join(self._base_folder, self._job_name)
        self._models_path = os.path.join(self._job_root, "models")
        self._json_file_path = os.path.join(self._job_root, "data.json")
        self._excel_file_path = os.path.join(self._job_root, "data.xlsx")
        self._avoid = []

    def _prepare_folders(self):
        if not os.path.isdir(self._job_root):
            os.mkdir(self._job_root)

        if not os.path.isdir(self._models_path):
            os.mkdir(self._models_path)

    def _get_blank_model_entry(self):
        return {
            "models_names": []
        }

    def _load_data(self):
        if self._data is None:
            if os.path.isfile(self._json_file_path):
                with open(self._json_file_path,"rt") as f:
                    self._data = json.load(f)
                if self._models is not None:
                    names = []
                    for model in self._models:
                        if model.name not in self._data:
                            self._data[model.name] = self._get_blank_model_entry()
                        names.append(model.name)
                    for name in self._data:
                        if name not in names:
                            self._avoid.append(name)

            else:
                self._data = {}
                for model in self._models:
                    self._data[model.name] = self._get_blank_model_entry()

    def get_next_model(self):

        self._load_data()

        min_samples = min([len(self._data[model_name]["models_names"]) for model_name in self._data if model_name not in self._avoid])
        min_and_name = [(len(self._data[name]["models_names"]), name) for name in self._data
                        if ((name not in self._avoid) and (len(self._data[name]["models_names"]) == min_samples))][0]

        return min_and_name

    def save_data(self):
        with open(self._json_file_path,"wt") as f:
            json.dump(self._data,f,indent=4)

    def start_tests(self):

        self._prepare_folders()

        stop = False

        while not stop:
            model_samples, model_name = self.get_next_model()

            if model_samples >= self._tests_per_model:
                stop = True
            else:
                model_and_dataset = [model for model in self._models if model.name == model_name][0]
                model_creation_func = model_and_dataset.model_creation_func
                dataset_parameters = model_and_dataset.dataset_parameters

                dc = DatasetCreator("", image_cache=self._img_cache, dataset_persister=MemoryDatasetPersister())

                dm = dc.create_dataset(**dataset_parameters)

                nnt = NeuralNetworkTrainer(str("%s__%d" % (model_name,model_samples)), self._models_path, dm,
                                           model_creation_func,
                                           patience=self._patience,
                                           add_last_layer=False if dm.is_for_autoencoders() else True,
                                           loss_function="mean_squared_error" if dm.is_for_autoencoders() else
                                           "categorical_crossentropy")

                model = nnt.train()

                self._data[model_name]["models_names"].append(model.get_model_name())

                self.save_data()

    def remove_outliers(self, data, tolerance=None, remove_below=None):
        if remove_below is not None:
            data = [d for d in data if d >= remove_below]

        if tolerance is not None:
            mean = np.mean(data)
            std = np.std(data)

            accepted = std*(1+tolerance)

            res = []

            for d in data:
                dev = (d-mean)**2
                dev = abs(sqrt(dev))
                if dev <= accepted:
                    res.append(d)
            # data = [d for d in data if sqrt((d-mean)**2) < accepted]
        else:
            res = data

        return res

    def analyze(self, metric, ids_for_speed_analysis=None, outl_tolerance=None, remove_below=None):

        if ids_for_speed_analysis is None:
            ids_for_speed_analysis = []

        self._load_data()

        res = {}

        for model_name in self._data:
            res[model_name] = {}
            data = []
            sec_per_image = []
            imgs = [self._img_cache.get_image_by_id(id) for id in ids_for_speed_analysis]
            for sample_name in self._data[model_name]["models_names"]:

                mm = ModelManager(sample_name,models_folder=self._models_path)
                data.append(mm.get_descriptor()["data"][metric])

                if len(imgs) > 0:
                    # Loads the model in order to avoid loosing time during speed estimation
                    mm.get_model()

                    start = datetime.now()
                    for img in imgs:
                        mm.get_prediction(img)
                    tot_secs = (datetime.now() - start).total_seconds()
                    sec_per_image.append([tot_secs/len(ids_for_speed_analysis)])

            res[model_name]["data"] = data

            data = self.remove_outliers(data, tolerance=outl_tolerance, remove_below=remove_below)

            res[model_name]["avg"] = np.mean(data)
            res[model_name]["std"] = np.std(data)
            res[model_name]["max"] = np.max(data).item() if len(data)>0 else 0
            res[model_name]["min"] = np.min(data).item() if len(data)>0 else 0
            res[model_name]["samples"] = len(data)
            res[model_name]["avg_sec_per_image"] = np.mean(sec_per_image)


        return res

    def analyze_to_dataframe(self,metrics,outl_tolerance=None,remove_below=None):

        indices_names = ["avg","std","max","min","samples","avg_sec_per_image"]

        columns_names = [metric_name + "_" + index_name for metric_name in metrics for index_name in indices_names]

        models_dict = {}

        res = pd.DataFrame(columns=["model_name"] + columns_names)

        for metric in metrics:

            data = self.analyze(metric, outl_tolerance=outl_tolerance, remove_below=remove_below)

            for index_name in indices_names:

                column_name = metric + "_" + index_name

                for model_name in data:

                    if model_name not in models_dict:
                        models_dict[model_name] = {}

                    models_dict[model_name][column_name] = data[model_name][index_name]

        for model_name in models_dict:

            row = [model_name]

            for column_name in columns_names:
                row.append(models_dict[model_name][column_name])

            to_append = pd.DataFrame([row], columns=res.columns.values)
            res = res.append(to_append)

        res.to_excel(self._excel_file_path)

        return res
