import os
from copy import deepcopy


class WaterfallModelManager(object):

    def __init__(self, signosig_model_manager, leafnodes_model_manager, threshold=0.6):
        self._signosig_model_manager = signosig_model_manager
        self._leafnodes_model_manager = leafnodes_model_manager
        self._threshold = threshold

    def get_signosig_model(self):

        return self._signosig_model_manager.get_model

    def get_sigtype_model(self):

        return self._leafnodes_model_manager.get_model

    def get_leafnodes_m_descriptor(self):

        return self._leafnodes_model_manager.get_descriptor()

    def get_prediction_TID_VID_perc(self, img):

        lbl, perc = self._signosig_model_manager.get_prediction_label_perc(img)
        if lbl == "nosig" and perc > self._threshold:

            return 1, 2, perc

        else:

            lbl, perc = self._leafnodes_model_manager.get_prediction_label_perc(img)
            dstruct = self._leafnodes_model_manager.get_dataset_descriptor()["structure"]

            #TODO: optimize here
            label_data = [entry for entry in dstruct if entry[0] == lbl][0]

            tid, vid = label_data[1][0]

            return tid, vid, perc

    def get_prediction_label_perc(self, img, prep_image=True, verbose=False):
        """
        Returns the most probable label and its probability given an image (numpy array)
        :param img: The image
        :param prep_image: If the boolean is true the image will be preprocessed
        :param verbose:
        :return: A couple: label, probability
        """

        lbl, perc = self._signosig_model_manager.get_prediction_label_perc(img)
        if verbose:
            print "signosig: ", lbl, perc
        if lbl == "sig" or (lbl == "nosig" and perc < self._threshold):
            lbl, perc = self._leafnodes_model_manager.get_prediction_label_perc(img, prep_image)
            if verbose:
                print "sigtype: ", lbl, perc

        return lbl, perc

    def get_model_name(self):

        return self._signosig_model_manager.get_model_name() + "___" + self._leafnodes_model_manager.get_model_name() + "___threshold_%f" % self._threshold

    # def rename_files(self, new_name_template):
    #     """
    #     Renames all model files with the given template
    #     :param new_name_template: The template used to generate the new filename.
    #                               Can use all data from the model descriptor plus {old_name} for taking inserting the old
    #                               name of the files.
    #     :return:
    #     """
    #     m_list = [self._signosig_model_manager, self._leafnodes_model_manager]
    #
    #     for m in m_list:
    #         old_descriptor_file_name = m.get_descriptor_file_name()
    #         old_model_file_name = m.get_model_file_name()
    #
    #         descr = deepcopy(m.get_descriptor())
    #         descr["old_name"] = m.get_model_name()
    #
    #         m.set_model_name(new_name_template.format(**descr))
    #
    #         os.rename(old_descriptor_file_name, m.get_descriptor_file_name())
    #         os.rename(old_model_file_name, m.get_model_file_name())
    #
    # def delete_files(self):
    #     os.remove(self._signosig_model_manager.get_descriptor_file_name())
    #     os.remove(self._signosig_model_manager.get_model_file_name())
    #
    #     os.remove(self._leafnodes_model_manager.get_descriptor_file_name())
    #     os.remove(self._leafnodes_model_manager.get_model_file_name())

