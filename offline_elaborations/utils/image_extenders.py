import random
from copy import deepcopy

import numpy as np
import cv2
from abc import ABCMeta, abstractmethod

from cv2.cv import CV_BGR2HSV, CV_HSV2BGR

from utils.perspective import perspectivize_image


class ImageExtender(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def extend(self, img, replication=True):
        """
        Makes some operation on the image in order to extend the database
        :param img: The image to be modified
        :param replication: whether to replicate imae on borders or not
        :return: The modified image
        """
        pass

def get_random_positive_negative_range(range, allow_negative=True):

    sign = random.randint(0,1) if allow_negative else 0

    if sign == 0:
        return random.uniform(*range)
    else:
        return -random.uniform(*range)

class PerspectiveImageExtender(ImageExtender):

    def __init__(self, dx_range=(0,0), dy_range=(0,0), dz_range=(0,0)):
        self._dx_range = dx_range
        self._dy_range = dy_range
        self._dz_range = dz_range

    def extend(self, img, replication=True):

        h,w,_ = img.shape

        border = max(h,w)*2

        if replication:
            img = cv2.copyMakeBorder(img, border, border, border, border, cv2.BORDER_REPLICATE)

        return perspectivize_image(img,
                                   get_random_positive_negative_range(self._dx_range),
                                   get_random_positive_negative_range(self._dy_range),
                                   get_random_positive_negative_range(self._dz_range),
                                   result_image_w=w, result_image_h=h)

class TranslationImageExtender(ImageExtender):

    def __init__(self, x_range=(0,0), y_range=(0,0)):
        self._x_range = x_range
        self._y_range = y_range

    def extend(self, img, replication=True):

        h,w,_ = img.shape

        dx = int(get_random_positive_negative_range(self._x_range)*w)
        dy = int(get_random_positive_negative_range(self._y_range)*h)

        b = max(abs(dx),abs(dy))

        if replication:
            res = cv2.copyMakeBorder(img,b,b,b,b,cv2.BORDER_REPLICATE)
        else:
            res = cv2.copyMakeBorder(img, b, b, b, b, cv2.BORDER_CONSTANT, value=(0, 0, 0))

        M = np.float32([[1, 0, dx-b], [0, 1, dy-b]])
        res = cv2.warpAffine(res, M, (w, h))

        return res

class BlurImageExtender(ImageExtender):

    def __init__(self, x_range=(0, 0), y_range=(0, 0)):
        self._x_range = x_range
        self._y_range = y_range

    def extend(self, img, replication=True):
        h, w, _ = img.shape

        dx = int(get_random_positive_negative_range(self._x_range, allow_negative=False) * w)
        dy = int(get_random_positive_negative_range(self._y_range, allow_negative=False) * h)

        if dx % 2 == 0:
            dx += 1

        if dy % 2 == 0:
            dy += 1

        kernel = np.ones((dy, dx), np.float32) / (dx*dy)
        return cv2.filter2D(img, -1, kernel)

class NoiseImageExtender(ImageExtender):

    def __init__(self, amt_range=(0,0)):
        self._amt_range = amt_range

    def extend(self, img, replication=True):
        return self.noisy("gauss", img).clip(0,255).astype(np.uint8)

    def noisy(self, noise_typ, image):
        if noise_typ == "gauss":
            row, col, ch = image.shape
            mean = 0
            var = get_random_positive_negative_range(self._amt_range, allow_negative=False)
            sigma = var ** 0.5
            gauss = np.random.normal(mean, sigma, (row, col, ch))
            gauss = gauss.reshape(row, col, ch)
            noisy = image + gauss
            return noisy
        elif noise_typ == "s&p":
            row, col, ch = image.shape
            s_vs_p = 0.5
            amount = 0.004
            out = np.copy(image)
            # Salt mode
            num_salt = np.ceil(amount * image.size * s_vs_p)
            coords = [np.random.randint(0, i - 1, int(num_salt))
                      for i in image.shape]
            out[coords] = 1

            # Pepper mode
            num_pepper = np.ceil(amount * image.size * (1. - s_vs_p))
            coords = [np.random.randint(0, i - 1, int(num_pepper))
                      for i in image.shape]
            out[coords] = 0
            return out

        elif noise_typ == "poisson":
            vals = len(np.unique(image))
            vals = 2 ** np.ceil(np.log2(vals))
            noisy = np.random.poisson(image * vals) / float(vals)
            return noisy

        elif noise_typ == "speckle":
            row, col, ch = image.shape
            gauss = np.random.randn(row, col, ch)
            gauss = gauss.reshape(row, col, ch)
            noisy = image + image * gauss
            return noisy

class LuminosityImageExtender(ImageExtender):

    def __init__(self, amt_range):
        self._amt_range = amt_range

    def extend(self, img, replication=True):
        amt = get_random_positive_negative_range(self._amt_range, allow_negative=False)
        return (img.astype(np.float64)*amt).clip(0,255).astype(np.uint8)


class ColourTemperatureImageExtender(ImageExtender):

    def __init__(self, r_range=(1.0,1.0), b_range=(1.0,1.0)):
        self._r_range = r_range
        self._b_range = b_range

    def extend(self, img, replication=True):
        amt_r = get_random_positive_negative_range(self._r_range, allow_negative=False)
        amt_b = get_random_positive_negative_range(self._b_range, allow_negative=False)

        img = img.astype(np.float64)

        # Assuming BGR format
        img[:,:,0] = img[:,:,0]*amt_b
        img[:,:,2] = img[:,:,2]*amt_r

        return img.clip(0,255).astype(np.uint8)

class FlipImageExtender(ImageExtender):

    def __init__(self, flip_probability=1.0):
        self._flip_probability = flip_probability

    def extend(self, img, replication=True):

        flip = random.uniform(0,1)

        if flip > self._flip_probability:

            return img

        else:

            img = img.astype(np.float64)

            img[:, :, :] = np.flip(img,axis=1)

            return img

class SaturationImageExtender(ImageExtender):

    def __init__(self, amt_range):
        self._amt_range = amt_range

    def extend(self, img, replication=True):
        amt = get_random_positive_negative_range(self._amt_range, allow_negative=False)

        img_hsv = cv2.cvtColor(img,CV_BGR2HSV).astype(np.float64)

        img_hsv[:,:,1] = img_hsv[:,:,1]*amt

        img_hsv = img_hsv.clip(0, 255).astype(np.uint8)

        img_res = cv2.cvtColor(img_hsv,CV_HSV2BGR)

        return img_res.clip(0, 255).astype(np.uint8)

class ReplicationImageExtender(ImageExtender):

    def extend(self, img, replication=True):
        return img


class CompoundImageExtender(ImageExtender):

    def __init__(self, extenders, num_effects_range, final_crop=0.05):
        self._extenders = extenders
        self._num_effects_range = num_effects_range
        self._final_crop = final_crop

    def extend(self, img, replication=True):

        num_effects_to_apply = int(round(get_random_positive_negative_range(self._num_effects_range, allow_negative=False)))

        to_apply = random.sample(self._extenders, num_effects_to_apply)

        for ext in to_apply:

            img = ext.extend(img, replication)

        h, w, _ = img.shape

        ch = int(self._final_crop*h)
        cw = int(self._final_crop*w)

        img = img[ch:h-ch-1,cw:w-cw-1,:]

        return img


ALL_IMAGES_EXTENDERS = CompoundImageExtender(
    [
        PerspectiveImageExtender(dx_range=(30,50)),
        PerspectiveImageExtender(dy_range=(30,50)),
        PerspectiveImageExtender(dz_range=(10,20)),
        TranslationImageExtender(x_range=(0.01,0.05)),
        TranslationImageExtender(y_range=(0.01,0.05)),
        BlurImageExtender(x_range=(0.03,0.07)),
        BlurImageExtender(y_range=(0.03,0.07)),
        NoiseImageExtender(amt_range=(5,20)),
        LuminosityImageExtender(amt_range=(0.4, 1.6))
    ],
    (3,7)
)