from pprint import pprint

from keras import Model
from keras.layers import Dropout, Activation


def has_weights(layer):
    try:
        weights = layer.get_weights()[0]
        biases = layer.get_weights()[1]
        return True
    except:
        return False

class FineTuner(object):

    def __init__(self, model,verbose=False):
        self._model = model
        self._verbose = verbose

    def get_model(self):
        return self._model

    def unfreeze_all_weights(self):
        for layer in self._model.layers:
            layer.trainable = True

    def freeze_all_weights(self):
        for layer in self._model.layers:
            layer.trainable = False

    def count_weighted_layers(self):
        res = 0
        for layer in self._model.layers:
            if has_weights(layer):
                res += 1

        return res

    def remove_dropouts(self):
        x = None
        x_layer = None
        layers = list(self._model.layers)

        for layer in layers:
            if self._verbose:
                print layer.name
            if issubclass(type(layer),Dropout):
                if self._verbose:
                    print "Removing drop %s" % x_layer.name
            elif x is None:
                if self._verbose:
                    print "First = %s" % layer.name
                x = layer.output
                x_layer = layer
            else:
                x = layer(x)
                x_layer = layer

        self._model = Model(input=self._model.input, output=x)

    def sigmoid_to_relu(self):

        x = None
        x_layer = None
        layers = list(self._model.layers)

        layer_name = "act_%d"

        i = 0

        for layer in layers:
            if issubclass(type(x_layer),Activation) and x_layer.activation.func_name == "sigmoid":
                    pprint(x_layer.activation.func_name)
                    act = Activation("relu",name=layer_name%i)
                    i+=1
                    x = act(x)
                    x_layer = act
            elif x is None:
                if self._verbose:
                    print "First = %s" % layer.name
                x = layer.output
                x_layer = layer
            else:
                x = layer(x)
                x_layer = layer

        self._model = Model(input=self._model.input, output=x)

    def add_dropouts(self, dropout_perc, no_drop_from_end=0):
        x = None
        x_layer = None
        layers = list(self._model.layers)

        layer_name = "drop_%d"

        wl = self.count_weighted_layers()

        i = 0
        for layer in layers:
            if self._verbose:
                print layer.name
            if x is not None and has_weights(x_layer) and i < (wl-no_drop_from_end):
                if self._verbose:
                    print "Adding drop from %s to %s" % (x.name, layer.name)
                drop = Dropout(dropout_perc,name=layer_name%i)(x)
                i += 1
                x = layer(drop)
                x_layer = layer
            elif x is None:
                if self._verbose:
                    print "First = %s" % layer.name
                x = layer.output
                x_layer = layer
            else:
                x = layer(x)
                x_layer = layer

        self._model = Model(input=self._model.input,output=x)

    def unfreeze_one(self):

        for layer in reversed(self._model.layers):
            if not layer.trainable:
                if self._verbose:
                    print layer.name
                layer.trainable = True
                if has_weights(layer):
                    return True

        return False