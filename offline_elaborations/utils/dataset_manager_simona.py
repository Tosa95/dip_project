import json
import os
import joblib as jl

from utils.images import create_images_transformers_from_json_settings

class LabelsTraducer(object):

    def __init__(self, dataset_descriptor):
        self._dataset_descriptor = dataset_descriptor

    def get_label_from_index(self, index):
        return self._dataset_descriptor["index_to_labels"][index]

class DatasetManager(object):

    def __init__(self, datasets_folder, dataset_name, descriptor_suffix="_desc.json"):
        self._datasets_folder = datasets_folder
        self._dataset_name = dataset_name
        self._descriptor_suffix = descriptor_suffix
        self._descriptor = None
        self._dataset = None

    def get_descriptor(self):

        if self._descriptor is None:
            with open(os.path.join(self._datasets_folder + "/" + self._dataset_name + self._descriptor_suffix), "r") as input:
                self._descriptor = json.load(input)

        return self._descriptor

    def get_dataset(self):

        if self._dataset is None:
            with open(os.path.join(self._datasets_folder + "/" + self._dataset_name + ".joblib"), "rb") as input:
                self._dataset = jl.load(input)

        return self._dataset

    def get_dataset_name(self):

        return self._dataset_name

    def get_output_layer_required_size(self):

        return len(self.get_descriptor()["labels_repr"])

    def get_label_from_index(self, index):

        return LabelsTraducer(self.get_descriptor()).get_label_from_index(index)

    def get_images_transformer(self):

        return create_images_transformers_from_json_settings(self.get_descriptor()["image_transformer_settings"])
