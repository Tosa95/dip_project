import pickle
from abc import ABCMeta, abstractmethod

import joblib


class DatasetPersister(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def save(self, dataset, filepath):
        """
        Saves a dataset to a certain filepath
        :param dataset: The dataset to be saved
        :param filepath: The path in which the dataset has to be saved. The extension is added automatically
        """
        pass

    @abstractmethod
    def load(self, filepath):
        """
        Loads a dataset from a certain filepath
        :param filepath: The filepath, no extension required
        :return: The loaded dataset
        """
        pass

    @abstractmethod
    def get_extension(self):
        """
        :return: The extension associated with the saving method
        """
        pass

    def add_extension(self, filepath):
        """
        :param filepath: The filepath, no extension
        :return: The filepath with extension
        """
        return filepath + "." + self.get_extension()


class PickleDatasetPersister(DatasetPersister):

    def load(self, filepath):
        full_path = self.add_extension(filepath)

        with open(full_path, "rb") as input:
            return pickle.load(input)

    def get_extension(self):
        return "pickle"

    def save(self, dataset, filepath):
        full_path = self.add_extension(filepath)

        with open(full_path, "wb") as output:
            pickle.dump(dataset, output, protocol=pickle.HIGHEST_PROTOCOL)


class JoblibDatasetPersister(DatasetPersister):

    def load(self, filepath):
        full_path = self.add_extension(filepath)
        with open(full_path, "rb") as input:
            return joblib.load(input)

    def get_extension(self):
        return "joblib"

    def save(self, dataset, filepath):
        full_path = self.add_extension(filepath)

        with open(full_path, "wb") as output:
            joblib.dump(dataset, output)


class MemoryDatasetPersister(DatasetPersister):
    """
    Simply keeps the dataset in memory, useful when we do not want to save it
    """
    def __init__(self):
        self._dataset = None

    def load(self, filepath):
        return self._dataset

    def get_extension(self):
        return None

    def save(self, dataset, filepath):
        self._dataset = dataset
