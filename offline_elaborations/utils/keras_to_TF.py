import os
import pprint
import cv2
import numpy as np

import keras
from keras.models import model_from_json
from keras import backend as K
import tensorflow as tf
from tensorflow.python.platform import gfile

from global_settings import get_settings
from utils.images import ImagesTransformer
from utils.keras_fuck import Reshape2
from utils.model_manager import ModelManager

def prepare_model(model_manager, layers_to_remove=(keras.layers.core.Dropout,)):
    model = model_manager.get_model()
    new_model = keras.models.Sequential()

    for layer in model.layers:
        print layer
        if type(layer) in layers_to_remove:
            print "ciao"
        else:
            new_model.add(layer)

    print new_model.summary()

    return new_model

def freeze_session(session, keep_var_names=None, output_names=None, clear_devices=True):
    """
    Freezes the state of a session into a pruned computation graph.

    Creates a new computation graph where variable nodes are replaced by
    constants taking their current value in the session. The new graph will be
    pruned so subgraphs that are not necessary to compute the requested
    outputs are removed.
    @param session The TensorFlow session to be frozen.
    @param keep_var_names A list of variable names that should not be frozen,
                          or None to freeze all the variables in the graph.
    @param output_names Names of the relevant graph outputs.
    @param clear_devices Remove the device directives from the graph for better portability.
    @return The frozen graph definition.
    """
    graph = session.graph
    with graph.as_default():
        freeze_var_names = list(set(v.op.name for v in tf.global_variables()).difference(keep_var_names or []))
        output_names = output_names or []
        output_names += [v.op.name for v in tf.global_variables()]
        input_graph_def = graph.as_graph_def()
        if clear_devices:
            for node in input_graph_def.node:
                node.device = ""
        frozen_graph = tf.graph_util.convert_variables_to_constants(
            session, input_graph_def, output_names, freeze_var_names)
        return frozen_graph

def keras_model_to_tf(model, output_file_name):
    """
    Saves a keras model as a tensor flow model. This function has to be the only one called in the script and the model
    passed is the only one that has to be loaded.
    :param model_manager: The model manager
    :param output_file_name: The output files name (if relative put ./ before the name!!!)
    :return: A sort of textual description of the tf graph. From there we have to search for the input of the model
    (usually the first operation of the first layer) and the output (usually the last operation of the last layer, like
    for example softmax)
    """

    K.set_learning_phase(0)

    sess = K.get_session()

    frozen_graph = freeze_session(sess, output_names=[out.op.name for out in model.outputs])

    tf.train.write_graph(frozen_graph, "", output_file_name, as_text=False)

    # res = frozen_graph.graph.get_operations()
    # saver.save(frozen_graph, output_file_name)
    #
    # fw = tf.summary.FileWriter('logs', frozen_graph.graph)
    # fw.close()

    return [node for node in frozen_graph.node]


def keras_model_to_tf_2(model, output_file_name):
    K.set_learning_phase(0)

    saver = tf.train.Saver()

    sess = K.get_session()

    saver.save(sess, output_file_name)
    tf.train.write_graph(sess.graph_def, ".", "graph.pbtxt")


def get_probable_interesting_layers(kttf_res, interesting_terms=("activation", "dense", "conv2d", "flatten", "softmax", "cond")):
    """
    Trying to get the names of the interesting layers to be used as input or output of the graph for the movidius
    :param kttf_res:
    :param interesting_terms:
    :return:
    """

    res = []

    for op in kttf_res:

        name = op.name.lower()

        for word in interesting_terms:
            if word in name and "training" not in name and "loss" not in name:
                res.append(op.name)
                break

    return res

def test_frozen_graph(pb_graph_path, img):

    with tf.Session() as sess:

        with gfile.FastGFile(pb_graph_path,'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            sess.graph.as_default()
            g_in = tf.import_graph_def(graph_def)

        for op in sess.graph.get_operations():
            print op.name

        input_name = sess.graph.get_operations()[0].name + ":0"
        output_name = sess.graph.get_operations()[-1].name + ":0"

        print "INPUT: " + input_name
        print "OUTPUT: " + output_name

        input_tensor = sess.graph.get_tensor_by_name(input_name)
        output_tensor = sess.graph.get_tensor_by_name(output_name)

        img_trans = ImagesTransformer(40, 40)

        img_conv = img_trans.prepare_single_image_for_prediction(img)

        prediction = sess.run(output_tensor, {input_tensor: img_conv})

        print prediction

if __name__=="__main__":

    # Then we will need to call from the terminal the following command (in the directory in which we saved the model):
    # ES: mvNCCompile tf_signosig.meta -in=conv2d_1_input -on=activation_4/Softmax
    # mvNCCheck tf_sigtype_nohope.pb -on=activation_4/Softmax -S 2 -M 0.5
    # where -in is the name of the input layer and -on is the name of the output layer

    # Referrals:
    # https://www.dlology.com/blog/how-to-run-keras-model-on-movidius-neural-compute-stick/
    # https://medium.com/@pipidog/how-to-convert-your-keras-models-to-tensorflow-e471400b886a
    # https://movidius.github.io/ncsdk/tf_modelzoo.html

    phase = 1
    prep_model = True
    # models_folder = "/home/simona/ProgettoMLDM/dip_project/offline_elaborations/utils/movidius_models"
    models_folder = get_settings().get_models_path()
    model_name = "sigtype_ep3_vl1.9970635891_va0.2"
    path = os.path.join(models_folder, "test_movidius.model")
    pb_model_path = os.path.join(models_folder, "tf_sigtype_nohope.pb")

    if prep_model:
        new_model = prepare_model(ModelManager(model_name))
    else:
        new_model = ModelManager(model_name).get_model()

    new_model.save(path, overwrite=True)

    input_image = cv2.imread("movidius_models/test_image_2.jpg")

    img_resized = cv2.resize(input_image, (40, 40))
    img_resized = img_resized.astype(np.double) / 255
    img_resized = img_resized.reshape(-1, 40, 40, 3)

    #predict = new_model.predict([img_resized])
    #print predict

    # Clears keras session before loading the model from file
    K.clear_session()

    model = keras.models.load_model(path,custom_objects={"Reshape2":Reshape2})
    res = keras_model_to_tf(model, pb_model_path)
    #res = keras_model_to_tf_2(model, pb_model_path)
    # pprint.pprint(get_probable_interesting_layers(res))
    pprint.pprint([node.name for node in res])

    # Clears both keras and tf session before loading
    K.clear_session()
    tf.reset_default_graph()

    #test_frozen_graph(pb_model_path, input_image)


# Notes on ncsdk installation:
# Prefer docker!

# Clone the repo using:
#   git clone -b ncsdk2 http://github.com/Movidius/ncsdk

# cd into the ncsdk folder and issue this command in order to build the docker image:
#   docker build -t ncsdk -f ./extras/docker/Dockerfile .

