import Queue
import datetime
import os
import pprint
import time
from abc import ABCMeta, abstractmethod
from copy import deepcopy
from threading import Thread

import numpy as np
import matplotlib.pyplot as plt

from scipy import interpolate

from service_access import set_images_scores, add_image_predicted_label, add_image_predicted_labels


class SingleAxisScore(object):

    __metaclass__ = ABCMeta

    def __init__(self, pts):

        pts = sorted(pts, key=lambda p: p[0])

        x = [p[0] for p in pts]
        y = [p[1] for p in pts]

        self._pts = pts
        self._x = np.array(x)
        self._y = np.array(y)

    @abstractmethod
    def _get_score(self, x):
        pass

    def get_score(self, x, norm=True):
        if norm:
            return self.normalize(self._get_score(x))
        else:
            return self._get_score(x)

    def get_max(self):
        x = np.linspace(np.min(self._x), np.max(self._x))
        return np.max(self._get_score(x))

    def get_min(self):
        x = np.linspace(np.min(self._x), np.max(self._x))
        return np.min(self._get_score(x))

    def normalize(self, x):

        score_max = self.get_max()
        score_min = self.get_min()

        divide_by = score_max - score_min

        return (x - score_min) / divide_by

    def plot(self, norm=True):

        y = self._y
        score_max = self.get_max()
        score_min = self.get_min()

        if norm:
            y = self.normalize(y)
            score_min = 0
            score_max = 1

        axes = plt.gca()
        axes.set_ylim([score_min*0.9, score_max*1.1])

        markerline, stemlines, baseline = plt.stem(self._x, y, '-.')
        plt.setp(baseline, color='r', linewidth=2)

        x = np.linspace(np.min(self._x), np.max(self._x), 10000)

        plt.plot(x, self.get_score(x, norm), 'g')

        plt.show()


class PolyfitSingleAxisScore(SingleAxisScore):

    def __init__(self, pts, degree=None):
        super(PolyfitSingleAxisScore, self).__init__(pts)

        if degree is None:
            degree = len(pts)

        self._degree = degree

        self._poly_coeffs = np.polyfit(self._x,self._y,degree)

    def _get_score(self, x):

        return np.polyval(self._poly_coeffs, x)


class SplineSingleAxisScore(SingleAxisScore):

    def __init__(self, pts):

        super(SplineSingleAxisScore, self).__init__(pts)
        self._tck = interpolate.splrep(self._x, self._y)

    def _get_score(self, x):
        return interpolate.splev(x, self._tck)


class Interp1dSingleAxisScore(SingleAxisScore):

    def __init__(self, pts):

        super(Interp1dSingleAxisScore, self).__init__(pts)

        self._f = interpolate.interp1d(self._x, self._y)

    def _get_score(self, x):
        return self._f(x)


class ScoreCombiner(object):

    __metaclass__ = ABCMeta

    @abstractmethod
    def combine(self, scores):
        pass

class MultiplyScoreCombiner(ScoreCombiner):

    def __init__(self, weights=None):
        self._weights = weights

    def combine(self, scores):
        final_score = 1

        weights = np.array(self._weights)
        scores = np.array(scores)

        shape = scores.shape

        if len(shape) > 0:
            for i in range(len(shape[1:])):
                weights = weights[:,np.newaxis]

        tmp = np.power(scores, weights)

        return np.prod(tmp,axis=0)

class AddScoreCombiner(ScoreCombiner):

    def __init__(self, weights=None):
        self._weights = weights

    def combine(self, scores):

        weights = np.array(self._weights)

        if weights is None:
            weights = np.ones(len(scores))

        scores = np.array(scores)

        shape = scores.shape

        if len(shape) > 0:
            for i in range(len(shape[1:])):
                weights = weights[:,np.newaxis]

        return np.sum(np.multiply(scores,weights), axis=0)/np.sum(weights)

class CompoundScore(object):

    def __init__(self, single_axis_scorers, score_combiner=MultiplyScoreCombiner()):

        self._single_axis_scorers = single_axis_scorers
        self._score_combiner = score_combiner

    def get_score(self, x, norm=True):

        scores = []

        for i, scorer in enumerate(self._single_axis_scorers):
            scores .append(scorer.get_score(x[i], norm))

        return self._score_combiner.combine(scores)

    def plot(self):

        if len(self._single_axis_scorers) == 2:
            from mpl_toolkits.mplot3d import Axes3D
            import matplotlib.pyplot as plt
            from matplotlib import cm
            from matplotlib.ticker import LinearLocator, FormatStrFormatter
            import numpy as np

            fig = plt.figure()
            ax = fig.gca(projection='3d')

            # Make data.
            X = np.linspace(0, 1, 300)
            Y = np.linspace(0, 1, 300)
            X, Y = np.meshgrid(X, Y)
            Z = self.get_score([X, Y])

            # Plot the surface.
            surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                                   linewidth=0, antialiased=True)

            # Customize the z axis.
            # ax.set_zlim(-1.01, 1.01)
            ax.zaxis.set_major_locator(LinearLocator(10))
            ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

            # Add a color bar which maps values to colors.
            fig.colorbar(surf, shrink=0.5, aspect=5)

            plt.show()
        else:
            raise Exception("Can't plot with more than 3 dimensions!")


def get_tree_leafs(tree):

    leafs = []

    for v in tree["values"]:
        if v["subtype"] is None:
            leafs.append(v)
        else:
            leafs.extend(get_tree_leafs(v["subtype"]))

    return leafs


def get_lbl_samples_num(tree):

    res = {}

    for v in tree["values"]:

        res[v["name"]] = v["cnt"]

        if v["subtype"] is not None:
            res.update(get_lbl_samples_num(v["subtype"]))

    return res


class LabelsTreeHelper(object):

    def __init__(self, tree):
        self._lbl_samples_num = get_lbl_samples_num(tree)

    def get_lbl_samples_cnt(self, lbl):
        return self._lbl_samples_num[lbl]

    def get_max_samples_cnt(self):
        return max([s for s in self._lbl_samples_num.values()])

    def get_average_probable_size(self, prediction):

        res = 0

        for cls in prediction:
            res += self.get_lbl_samples_cnt(cls.lbl) * cls.perc

        return res

TYPE_MULTI_SCORE = 0

class ScoresCache(object):

    def __init__(self, send_every=50, scores_to_be_added=None, labels_to_be_added=None):
        self._scores = []
        self._to_send_queue = Queue.Queue()
        self._predictions_queue = Queue.Queue()
        self._send_every = send_every
        self._stop = False
        self._thread = Thread(target=self.sender_body)
        self._predictions = []
        self._thread_pred = Thread(target=self.prediction_sender_body)
        self._thread_presenter = Thread(target=self.state_presenter_boby)
        self._number_stat = NumberStatistics(max=scores_to_be_added)
        self._number_stat_pred = NumberStatistics(max=labels_to_be_added)
        self._cnt = 0
        self._cnt_pred = 0
        self._errors = []

    def start(self):
        self._thread.start()
        self._thread_pred.start()
        self._thread_presenter.start()

    def state_presenter_boby(self):

        print "Begin of the scoring procedure"

        while not (self._stop and self._to_send_queue.empty() and self._predictions_queue.empty()):
            # print self._number_stat.get_stat_string(unit="scores")
            # print self._number_stat_pred.get_stat_string(unit="labels")

            os.system('cls' if os.name == 'nt' else 'clear')

            print "%.1f%%  ETA:%.2fs, labels/s:%.1f, scores/s:%.1f, qsize_scores:%d, qsize_pred:%d" % (
                min(self._number_stat.get_perc(), self._number_stat_pred.get_perc()),
                max(self._number_stat.get_eta(), self._number_stat_pred.get_eta()),
                self._number_stat_pred.get_avg_speed(),
                self._number_stat.get_avg_speed(),
                self._to_send_queue.qsize(),
                self._predictions_queue.qsize()
            )

            if len(self._errors) > 0:
                pprint.pprint(self._errors)

            time.sleep(1)

    def sender_body(self):

        while not (self._stop and self._to_send_queue.empty()):
            try:
                to_send = self._to_send_queue.get(timeout=1)
                self._cnt += len(to_send)

                res = set_images_scores(to_send)

                if res["status"] != "OK":
                    self._errors.append(res)

                self._number_stat.update(self._cnt)
            except Queue.Empty:
                pass
            except Exception as ex:
                self._errors.append(ex)

            #print "QUEUE SIZE: %d|||" % self._to_send_queue.qsize()

    def prediction_sender_body(self):

        while not (self._stop and self._predictions_queue.empty()):
            try:
                to_send = self._predictions_queue.get(timeout=1)

                res = add_image_predicted_labels(to_send)
                if res["status"] != "OK":
                    self._errors.append(res)

                self._cnt_pred += len(to_send)

                self._number_stat_pred.update(self._cnt_pred)

            except Queue.Empty:
                pass
            except Exception as ex:
                self._errors.append(ex)

            #print "PRED QUEUE SIZE: %d|||" % self._predictions_queue.qsize()

    def send_all(self):
        self._to_send_queue.put(deepcopy(self._scores))
        self._scores = []

    def send_all_predictions(self):
        self._predictions_queue.put(deepcopy(self._predictions))
        self._predictions = []

    def add(self, image_id, score_id, value, tid=None, vid=None):
        self._scores.append([image_id, score_id, value, tid, vid])
        if len(self._scores) >= self._send_every:
            self.send_all()

    def add_prediction(self, image_id, value, tid, vid):
        self._predictions.append([1, image_id, value, tid, vid])
        if len(self._predictions) >= self._send_every:
            self.send_all_predictions()

    def delete_all_images_predictions(self, image_id):
        self._predictions.append([-1, image_id])
        if len(self._predictions) >= self._send_every:
            self.send_all_predictions()

    def stop(self):
        self._stop = True


class NumberStatistics(object):

    def __init__(self, initial_value=0, max=None, alpha=5, print_every_secs=1.0):
        self._last_value = initial_value
        self._last_update = datetime.datetime.now()
        self._max = max
        self._perc = 0
        self._units_per_second = 0
        self._avg_speed = 0
        self._last_print = None
        self._alpha = alpha
        self._print_every_secs = print_every_secs

    def get_stat_string(self, unit="units"):
        if self._max is not None:
            return "(%.1f/%.1f %s %.1f%% %.1f %s/s ETA:%.3fs)" % (self._last_value, self._max, unit, self._perc,
                                                        self._avg_speed, unit, self.get_eta())
        else:
            return "(%.1f %s %.1f %s/s)" % (self._last_value, unit, self._avg_speed, unit)

    def has_to_print(self):
        if self._last_print is None:
            return True

        if (datetime.datetime.now() - self._last_print).total_seconds() > self._print_every_secs:
            return True

        return False

    def set_last_print(self):
        self._last_print = datetime.datetime.now()

    def get_eta(self):

        if self._max is not None and self._avg_speed != 0:
            return (self._max - self._last_value)/self._avg_speed

        return 0

    def get_avg_speed(self):
        return self._avg_speed

    def get_last_value(self):
        return self._last_value

    def get_perc(self):
        return self._perc

    def get_units_per_seconds(self):
        return self._units_per_second

    def update(self, new_val):

        now = datetime.datetime.now()
        elasped = (now - self._last_update).total_seconds()

        if elasped > 0:
            new_speed = (new_val-self._last_value)/elasped
            old_speed = self._avg_speed
            self._units_per_second = new_speed
            self._avg_speed = (self._alpha * old_speed + new_speed)/(self._alpha+1)

        if self._max is not None:
            self._perc = (float(new_val)/self._max)*100

        self._last_update = now
        self._last_value = new_val

