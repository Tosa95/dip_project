from math import ceil

import cv2
import json
import os
import pickle
import random
import time

from global_settings import Settings, get_settings
from service_access import get_dataset, get_img_by_id
import numpy as np

from utils.dataset_manager import DatasetManager
from utils.image_extenders import CompoundImageExtender, ReplicationImageExtender
from utils.images import ImagesTransformer, LastSeen
from utils.scores import NumberStatistics


def get_full_description_dataset_name(base_name):
    return base_name + "__b{balanced}_gcl{group_count_limit}_isx{img_size_x}_isy{img_size_y}_vp{validation_perc}_ex{expanded}_vex{validation_expanded}"

class DatasetCreator(object):

    def __init__(self, datasets_folder, descriptor_suffix="_desc.json", image_cache=None, dataset_persister=get_settings().get_dataset_persister()):
        self._datasets_folder = datasets_folder
        self._descriptor_suffix = descriptor_suffix
        self._image_cache = image_cache
        self._dataset_persister = dataset_persister

    def split_list(self, lst, perc):
        """
        Splits a list in two parts, the second one of the size defined by perc
        :param lst: The list to be split
        :param perc: the split percentage
        :return: A couple of lists
        """
        split_index = int(len(lst)*perc)
        return lst[split_index:], lst[:split_index]

    def split_dataset(self, original_dataset, perc):
        """
        Splits a dataset into train and test
        :param original_dataset: The dataset to be split, in the format provided by the remote service
        :param perc: The split percentage, from 0 to 1, is the amount of data to reserve to validation
        :return: A couple of datasets: train and test
        """
        train_dataset = {}
        test_dataset = {}

        for lbl in original_dataset:
            random.shuffle(original_dataset[lbl]) # to be sure similar images aren't near
            train_lst,test_lst = self.split_list(original_dataset[lbl], perc)
            train_dataset[lbl] = train_lst
            test_dataset[lbl] = test_lst

        return train_dataset, test_dataset


    def compute_labels_representation(self, dataset_description):
        """
        Computes the output value for each of the possible labels.
        :param dataset_description: The data structure that describes the desired dataset
        :return: A copule containing:
            A map containing the association between labels and the index of the array that has to be 1
            An array that links the the index to the associated label
        """

        res_lbl_to_i = {}
        res_i_to_lbl = []
        labels = [group[0] for group in dataset_description]

        for i,label in enumerate(labels):
            res_lbl_to_i[label] = i
            res_i_to_lbl.append(label)

        return res_lbl_to_i, res_i_to_lbl


    def lbl_to_y_value(self, lbl, labels_repr):
        """
        Transforms a label into its representation used in the neural network (one hot array)
        :param lbl: The label
        :param labels_repr: The representations of labels
        :return:
        """
        index = labels_repr[lbl]
        res = np.zeros(len(labels_repr))
        res[index] = 1.0
        return res

    def get_name_given_descriptor(self, name_pattern, descriptor):
        """
        Substitutes properties into the name of the dataset
        :param name_pattern:
        :param descriptor:
        :return: The formatted name
        """
        return name_pattern.format(**descriptor)

    def elaborate_single_dataset(self, original_dataset, labels_repr, images_transformer,
                                 images_extender, group_count_limit, hyper_expansion_size,
                                 for_autorencoders, flatten_images, expand_all=False):
        """
        Function that creates a single dataset (test or train)
        :param original_dataset: The dataset in the format provided by the remote service
        :param labels_repr: The structure that maps labels to their values
        :param images_transformer: The object responsible for image transformations (made in order to adapt the images to the dataset)
        :param images_extender:  The object responsible for creating different version of the same image in order to expand the dataset
        :param group_count_limit: The maximum number of samples for each group.
               If images extender is not None, it will be used to raise the number of samples of each group to this value
        :return: X, the set of inputs and y, the set of output. They are both numpy arrays
        """

        ids = []

        for lbl in original_dataset:
            for id in original_dataset[lbl]:
                ids.append((id, self.lbl_to_y_value(lbl, labels_repr)))

        result_dataset = []

        img_cache = {}

        for index, (id, lbl) in enumerate(ids):

            print "%.2f%% %d %d %s" % (float(index)/len(ids)*100, index, id, lbl)

            err = True
            img = None
            while err:
                try:
                    if self._image_cache is None:
                        img = get_img_by_id(id)
                    else:
                        img = self._image_cache.get_image_by_id(id)
                    img_cache[id] = img
                except Exception as ex:
                    time.sleep(10)
                    print ex
                else:
                    err = False

            try:
                if not expand_all:
                    result_dataset.append((images_transformer.preprocess_image(img), lbl))
                else:
                    print "NOT ADDED DIRECTLY IN THE DATASET"
            except Exception as ex:
                print ex

        if images_extender is not None:

            tot_img = 0

            for i,lbl in enumerate(original_dataset):
                if expand_all:
                    tot_img += hyper_expansion_size
                else:
                    tot_img += hyper_expansion_size - len(original_dataset[lbl])

            ns = NumberStatistics(max=tot_img)
            curr_img_num = 0

            print "Expanding the dataset..."
            for i,lbl in enumerate(original_dataset):
                if expand_all:
                    to_be_added = hyper_expansion_size
                else:
                    to_be_added = hyper_expansion_size - len(original_dataset[lbl])
                ids_to_extend = [random.choice(original_dataset[lbl]) for _ in xrange(to_be_added)]
                for j, id in enumerate(ids_to_extend):
                    try:
                        extended_image = images_extender.extend(img_cache[id])
                        result_dataset.append((images_transformer.preprocess_image(extended_image), self.lbl_to_y_value(lbl, labels_repr)))
                        curr_img_num += 1

                        ns.update(curr_img_num)

                        if ns.has_to_print():
                            print "EXTENDING %s %d  (label %d on %d, img %d on %d)" % (
                            lbl, id, i + 1, len(original_dataset), j + 1, to_be_added)
                            print ns.get_stat_string("images")
                            ns.set_last_print()

                    except Exception as ex:
                        print ex

        random.shuffle(result_dataset)

        X = []
        y = []

        for img, lbl in result_dataset:
            if not flatten_images:
                X.append(img)
            else:
                print img.flatten().shape
                X.append(img.flatten())

            if not for_autorencoders:
                y.append(lbl)
            else:
                y.append(img.flatten())

        # Necessary for keras correct operation

        if not flatten_images:
            X = images_transformer.reshape_images_dataset(X)
        else:
            X = np.array(X).reshape(-1, X[0].shape[0])
        y = np.array(y)

        return X, y


    def create_dataset(self, name, dataset_description, images_transformer,
                       balanced=True, group_count_limit=0, validation_perc=0.2, images_extender=None,
                       expand_validation=False, for_autoencoders=False, flatten_images=False,
                       hyper_expansion_size=None, separate_validation=False, expand_all=False):
        """
        Creates a dataset in order to train a neural model, and saves it
        :param name: The name of the dataset. Can contain placeholders in the form {<param>},
                     where param is any of the keys of the dataset_descriptor map
        :param dataset_description: The description of the dataset. Is a list of groups of labels, examples:

                dataset_description = [
                    ["pericolo",[[2,3]]],
                    ["precedenza",[[2,4]]],
                    ["divieto",[[2,5]]],
                    ["obbligo",[[2,6]]],
                    ["altro",[[2,173]]],
                    ["semaforo",[[2,174]]],
                    ["nosig",[[1,2]]]
                ]

                dataset_description = [
                    ["signal", [[2,3],[2,4],[2,5],[2,6],[2,172],[2,173]]],
                    ["no_signal", [[1,2]]]
                ]

        :param images_transformer: The object responsible for image transformations (made in order to adapt the images to the dataset)
        :param images_extender:  The object responsible for creating different version of the same image in order to expand the dataset.
                                 I image extender is None no expansion will be done.
        :param group_count_limit: The maximum number of samples for each group. If 0 no limit is assumed.
               If images extender is not None, it will be used to raise the number of samples of each group to this value
        :param balanced: True: The database will be balanced with real image. If false, it can be balanced providing an image extender
        :param validation_perc: Which percentage of data should be retained in order to create the validation set
        :param expand_validation: If False, the validation set is not extended, that means that if groups are not all of
                                  the same size or greater than the limit the validation set will not be balanced. If True,
                                  also the validation set will be expanded, but ONLY by image replication: we do not want
                                  the validation set to contain artificial images.
        :param hyper_expansion_size: the size that every group has to be after expansion. By default it will be set
                                     equal to group_count_limit*(1-validation_perc)
        """

        if not separate_validation:
            full_gcl = group_count_limit
            corrected_valid_perc = validation_perc
        else:
            full_gcl = group_count_limit + group_count_limit*validation_perc
            corrected_valid_perc = validation_perc/(1.0+validation_perc)

        service_dataset = get_dataset(dataset_description, balanced=balanced, group_count_limit=full_gcl)

        labels_repr, index_to_labels = self.compute_labels_representation(dataset_description)

        if hyper_expansion_size is None:
            hyper_expansion_size = int(full_gcl*(1-corrected_valid_perc))

        dataset_descriptor = {
            "structure": dataset_description,
            "labels_repr": labels_repr,
            "index_to_labels": index_to_labels,
            "balanced": balanced,
            "group_count_limit": group_count_limit,
            "img_size_x": images_transformer.get_img_size_x(),
            "img_size_y": images_transformer.get_img_size_y(),
            "image_transformer_settings": images_transformer.get_settings(),
            "validation_perc": validation_perc,
            "expanded": images_extender is not None,
            "validation_expanded": expand_validation,
            "hyper_expansion_size": hyper_expansion_size,
            "separate_validation": separate_validation,
            "for_autoencoders": for_autoencoders,
            "expand_all": expand_all
        }

        train_dataset, test_dataset = self.split_dataset(service_dataset, corrected_valid_perc)

        X,y = self.elaborate_single_dataset(train_dataset, labels_repr, images_transformer, images_extender,
                                            int(full_gcl*(1-corrected_valid_perc)),
                                            for_autorencoders=for_autoencoders,
                                            flatten_images=flatten_images,
                                            hyper_expansion_size=hyper_expansion_size,
                                            expand_all=expand_all)

        if expand_validation:

            valid_gcl = int(full_gcl * corrected_valid_perc)
            valid_images_extender = ReplicationImageExtender()

            X_test, y_test = self.elaborate_single_dataset(test_dataset, labels_repr, images_transformer,
                                                           images_extender=valid_images_extender,
                                                           group_count_limit=valid_gcl,
                                                           for_autorencoders=for_autoencoders,
                                                           flatten_images=flatten_images,
                                                           hyper_expansion_size=valid_gcl)
        else:
            X_test, y_test = self.elaborate_single_dataset(test_dataset, labels_repr, images_transformer,
                                                           images_extender=None, group_count_limit=0,
                                                           for_autorencoders=for_autoencoders,
                                                           flatten_images=flatten_images,
                                                           hyper_expansion_size=0)

        result = {
            "X": X,
            "y": y,
            "X_test": X_test,
            "y_test": y_test
        }

        print "Saving..."

        full_name = self.get_name_given_descriptor(name, dataset_descriptor)

        full_name_descriptor = full_name + self._descriptor_suffix

        self._dataset_persister.save(result, self._datasets_folder + "/" + full_name)

        with open(os.path.join(self._datasets_folder + "/" + full_name_descriptor), "w") as output:
            json.dump(dataset_descriptor, output, indent=4)

        print "Saved."

        return DatasetManager(self._datasets_folder, full_name, dataset_persister=self._dataset_persister)

def get_random_dataset_flatten(dataset_description, tot_img, shuffle=True):

    gcl = int(ceil(float(tot_img)/len(dataset_description)))

    service_dataset = get_dataset(dataset_description, balanced=False, group_count_limit=gcl)

    res = []

    for lbl in service_dataset:
        for id in service_dataset[lbl]:
            res.append(id)

    if shuffle:
        random.shuffle(res)

    return res

def get_leafs(tree, leafs):
    for v in tree["values"]:
        if v["subtype"] is None:
            leafs.append(v)
        else:
            get_leafs(v["subtype"],leafs)

def leafs_to_dataset_struct (leafs, min_size=20):
    res = []
    for l in leafs:
        if l["cnt"] >= min_size:
            res.append([l["name"], [[l["TID"],l["ID"]]]])

    return res