import json
import os
import string
from copy import deepcopy

import numpy as np
from keras import Sequential, Model
from keras.callbacks import Callback, ModelCheckpoint, History, EarlyStopping
from keras.layers import Dense, Activation, Conv2D, Dropout, MaxPooling2D, Flatten, SeparableConv2D, Reshape, \
    BatchNormalization

from utils.fine_tuner import FineTuner
from utils.keras_fuck import Reshape2
from utils.model_manager import ModelManager


class SafeDict(dict):
    def __missing__(self, key):
        return '{' + key + '}'

def print_model_weigths(model):
    for layer in model.layers:
        weights = layer.get_weights()
        print(layer.name, weights)

def unlock_entire_model(model):
    for layer in model.layers:
        layer.trainable = True

class NeuralNetworkTrainer(object):
    """
    Class that trains neural networks
    """

    def __init__(self,
                 name,
                 models_folder,
                 dataset_manager,
                 model_creation_func,
                 monitored_quantity="val_loss",
                 patience=15,
                 max_epochs=1000,
                 descriptor_suffix="_descr.json",
                 loss_function="categorical_crossentropy",
                 add_last_layer=True,
                 metrics=('accuracy',)):
        """
        :param name: Name of the model. Can be a pattern using the model information.
                     At the moment formatting can't be used.
                     MUST NOT CONTAIN THE EXTENSION, it will be added automatically
                     Examples:

                        signosig_ep{epoch}_vl{val_loss}_va{val_acc}

        :param models_folder: Folder in which the model will be saved
        :param dataset_manager: The object representing the dataset to be used for training
        :param model_creation_func: A function that has to create the model. Should take a single parameter:
                                        X: the input to the model, useful to initialize the first level
                                    The function should create the model adding the layers that represents the network,
                                    EXCEPT THE LAST ONE, that is inferred automatically.
                                    Then, it has to return a couple:
                                        The model created
                                        a map, eventually void that represents the properties of the
                                        model. That information will be used to create the filename and will be stored inside the
                                        descriptor.
        :param monitored_quantity: The quantity that is monitored for deciding when to stop and which model is the best
                                   one.
        :param patience: How many epochs to wait without improvements before stopping the training process.
        :param max_epochs:
        :param descriptor_suffix: The string appended to the name for the descriptor
        :param loss_function: which function to use as loss (usually "categorical_crossentropy" for classification,
                              "mean_squared_error" for regression
        :param add_last_layer: True if you want the last layer to be inferred automatically (works only for classification)
        :param metrics: which metrics we want to follow in addition to loss
        """

        self._dataset_manager = dataset_manager
        self._model_creation_func = model_creation_func
        self._monitored_quantity = monitored_quantity
        self._patience = patience
        self._name = name
        self._max_epochs = max_epochs
        self._descriptor_suffix = descriptor_suffix
        self._models_folder = models_folder
        self._loss_function = loss_function
        self._add_last_layer = add_last_layer
        self._metrics = metrics

    def get_monitored_quantity(self):
        return self._monitored_quantity

    def train(self):
        """
        Trains the model
        :return: A model manager describing the best model obtained during training
        """

        X = self._dataset_manager.get_dataset()["X"]
        y = self._dataset_manager.get_dataset()["y"]
        X_test = self._dataset_manager.get_dataset()["X_test"]
        y_test = self._dataset_manager.get_dataset()["y_test"]

        do_fine_tuning = False
        incremental_fine_tuning = False
        epochs = self._max_epochs

        model, model_descriptor = self._model_creation_func(X)

        if "do_fine_tuning" in model_descriptor:
            # Leaves to the model creation function the ability to chose whether the fine tuning has to be done or not
            do_fine_tuning = model_descriptor["do_fine_tuning"]

        if "incremental_fine_tuning" in model_descriptor:
            incremental_fine_tuning = model_descriptor["incremental_fine_tuning"]

        if "fine_tuning_epochs" in model_descriptor:
            epochs = model_descriptor["fine_tuning_epochs"]

        if self._add_last_layer:

            if type(model) == Sequential:

                model.add(Dense(self._dataset_manager.get_output_layer_required_size(), activation="softmax"))

            else:

                # Here we treat the model in case it is not sequential (functional keras)

                output = model.layers[-1].get_output_at(-1)
                output = Dense(self._dataset_manager.get_output_layer_required_size(), name="final_output")(output)
                output = Activation('softmax', name="final_output_activation")(output)
                model = Model(model.input, output)

        model.compile(loss=self._loss_function,
                      optimizer='adam',
                      metrics=list(self._metrics))

        print model.summary()

        history = History()

        early_stopping = EarlyStopping(monitor=self._monitored_quantity, min_delta=0, patience=self._patience,
                                       verbose=1,
                                       mode='auto', baseline=None,
                                       restore_best_weights=True)

        full_name = string.Formatter().vformat(self._name, (), SafeDict(model_descriptor))

        model_descriptor["dataset_descriptor"] = self._dataset_manager.get_descriptor()

        model_checkpoint = ModelCheckpointEnhanced(self._models_folder, full_name, model_descriptor, self._descriptor_suffix,
                                                   monitor=self._monitored_quantity, verbose=1, save_best_only=True,
                                                   save_weights_only=False, mode='auto',
                                                   period=1)

        model.fit(X, y, batch_size=32, epochs=epochs, validation_data=(X_test, y_test),
                  callbacks=[history, early_stopping, model_checkpoint])

        if do_fine_tuning:
            # Fine tuning is done when the knowledge reusing models are use.
            # Usually, in order to force the added layers to use the old layers internal representation, old weights are
            # frozen (kept fixed) for the first training round (the one done with the model.fit instruction above).
            # After that, we can say that the network reached the (near-) optimum by changing only the weight of the
            # newly added layers. Usually this is not an optimum overall, so in this fine tuning phase we unlock all
            # weights and let the training continue in order to reach a better result (which is usually reached).
            # By these two steps we obtain the following behavior:
            # STEP 1: old weights are kept fixed and new ones are optimized. In this way the new levels will use the
            # output of the old ones and the training process will not interfere with the old layers. This is done
            # because the old weights contain knowledge coming from past trainings of the model. Changing immediately
            # those weights would result in a destruction of the knowledge we are trying to reuse.
            # STEP 2: after the first step training we assume that the new layers have adapted to the internal
            # representation of the knowledge owned by the old model inside its weights. So now we are ready to
            # enhance that knowledge by letting the training algorithm change also old weights.

            print "FINE TUNING!!!"

            ft = FineTuner(model)

            if incremental_fine_tuning:

                while ft.unfreeze_one():
                    model = ft.get_model()
                    model.compile(loss=self._loss_function,
                                  optimizer='adam',
                                  metrics=list(self._metrics))

                    print model.summary()

                    model.fit(X, y, batch_size=32, epochs=epochs, validation_data=(X_test, y_test),
                              callbacks=[history, early_stopping, model_checkpoint])

                # Final round with max epochs
                model.compile(loss=self._loss_function,
                              optimizer='adam',
                              metrics=list(self._metrics))

                model.fit(X, y, batch_size=32, epochs=self._max_epochs, validation_data=(X_test, y_test),
                          callbacks=[history, early_stopping, model_checkpoint])

            else:
                ft.unfreeze_all_weights()
                model = ft.get_model()
                model.compile(loss=self._loss_function,
                              optimizer='adam',
                              metrics=list(self._metrics))

                print model.summary()

                model.fit(X, y, batch_size=32, epochs=self._max_epochs, validation_data=(X_test, y_test),
                          callbacks=[history, early_stopping, model_checkpoint])

        return ModelManager(models_folder=self._models_folder, model_name=model_checkpoint.get_best_name())


def get_standard_model_creation_func(
        conv_filters_num,
        num_conv,
        dense_size,
        num_dense,
        dropout_perc
):

    def create_model(X):

        model = Sequential()

        model.add(Conv2D(conv_filters_num, (7, 7), input_shape=X.shape[1:]))
        model.add(Activation('relu'))
        model.add(Dropout(dropout_perc))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        for i in xrange(num_conv - 1):
            model.add(Conv2D(conv_filters_num, (7, 7)))
            model.add(Activation('relu'))
            model.add(Dropout(dropout_perc))
            model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors

        for i in xrange(num_dense):
            model.add(Dense(dense_size))
            model.add(Activation('relu'))
            model.add(Dropout(dropout_perc))

        descriptor = {
            "conv_filters_num": conv_filters_num,
            "dense_size": dense_size,
            "num_conv": num_conv,
            "num_dense": num_dense,
            "dropout_perc": dropout_perc
        }

        return model, descriptor

    return create_model


def get_autoenc_model_creation_func(
        conv_filters_num,
        num_conv,
        dropout_perc,
        direct_dense_size,
        intermediate_vector_size,
        inverse_dense_size,
        output_sz,
        max_poolings=None,
        filter_sizes=None,
        num_filters=None,
        depth_mults=None
):

    if max_poolings is None:
        max_poolings = [(1, 1)] * num_conv

    if filter_sizes is None:
        filter_sizes = [(5, 5)] * num_conv

    if num_filters is None:
        num_filters = [conv_filters_num] * num_conv

    if depth_mults is None:
        depth_mults = [1] * num_conv

    def create_model(X):

        model = Sequential()

        if num_conv>0:

            for i in xrange(num_conv):

                if i == 0:
                    model.add(SeparableConv2D(num_filters[i], filter_sizes[i], padding="same",
                                              depth_multiplier=depth_mults[i], input_shape=X.shape[1:]))
                else:
                    model.add(SeparableConv2D(num_filters[i], filter_sizes[i], padding="same",
                                              depth_multiplier=depth_mults[i]))
                model.add(Activation('relu'))
                model.add(Dropout(dropout_perc))
                if max_poolings[i] != (1,1):
                    model.add(MaxPooling2D(pool_size=max_poolings[i]))

            model.add(Flatten())
        else:
            model.add(Flatten(input_shape=X.shape[1:]))  # this converts our 3D feature maps to 1D feature vectors

        for sz in direct_dense_size:
            model.add(Dense(sz))
            model.add(Activation('relu'))
            model.add(Dropout(dropout_perc))

        model.add(Dense(intermediate_vector_size))
        model.add(Activation('sigmoid', name="intermediate_vector"))
        model.add(Dropout(dropout_perc))

        for sz in inverse_dense_size:
            model.add(Dense(sz))
            model.add(Activation('relu'))
            model.add(Dropout(dropout_perc))

        model.add(Dense(output_sz))
        model.add(Activation('sigmoid', name="output"))

        descriptor = {
            "conv_filters_num": conv_filters_num,
            "direct_dense": direct_dense_size,
            "inverse_dense": inverse_dense_size,
            "num_conv": num_conv,
            "dropout_perc": dropout_perc
        }

        return model, descriptor

    return create_model

def get_dw_class_model_creation_func(
        conv_filters_num,
        num_conv,
        dropout_perc,
        dense_size,
        max_poolings=None,
        filter_sizes=None,
        num_filters=None,
        depth_mults=None,
        batch_norm=False
):

    if max_poolings is None:
        max_poolings = [(1, 1)] * num_conv

    if filter_sizes is None:
        filter_sizes = [(5, 5)] * num_conv

    if num_filters is None:
        num_filters = [conv_filters_num] * num_conv

    if depth_mults is None:
        depth_mults = [1] * num_conv

    def create_model(X):

        model = Sequential()

        if num_conv>0:

            for i in xrange(num_conv):

                if i == 0:
                    model.add(SeparableConv2D(num_filters[i], filter_sizes[i], padding="same",
                                              depth_multiplier=depth_mults[i], input_shape=X.shape[1:]))
                else:
                    model.add(SeparableConv2D(num_filters[i], filter_sizes[i], padding="same",
                                              depth_multiplier=depth_mults[i]))

                if batch_norm:
                    model.add(BatchNormalization())

                model.add(Activation('relu'))

                model.add(Dropout(dropout_perc))

                if max_poolings[i] != (1,1):
                    model.add(MaxPooling2D(pool_size=max_poolings[i]))

            model.add(Flatten())
        else:
            model.add(Flatten(input_shape=X.shape[1:]))  # this converts our 3D feature maps to 1D feature vectors

        for sz in dense_size:
            model.add(Dense(sz))

            if batch_norm:
                model.add(BatchNormalization())

            model.add(Activation('relu'))
            model.add(Dropout(dropout_perc))

        descriptor = {
            "conv_filters_num": conv_filters_num,
            "dense": dense_size,
            "num_conv": num_conv,
            "dropout_perc": dropout_perc,
            "max_poolings": max_poolings,
            "filter_sizes": filter_sizes,
            "num_filters": num_filters,
            "depth_mults": depth_mults,
            "batch_norm": batch_norm
        }

        return model, descriptor

    return create_model


def get_no_drop_model_creation_func(
        conv_filters_num,
        num_conv,
        dense_size,
        num_dense
):

    def create_model(X):

        model = Sequential()

        model.add(Conv2D(conv_filters_num, (3, 3), activation="sigmoid", input_shape=X.shape[1:]))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        for i in xrange(num_conv - 1):
            model.add(Conv2D(conv_filters_num, (3, 3), activation="sigmoid"))
            model.add(MaxPooling2D(pool_size=(2, 2)))

        # shape = model.layers[-1].output_shape
        # final_size = shape[1]*shape[2]*shape[3]
        #print shape

        model.add(Flatten())

        # model.add(Reshape2((final_size,)))  # this converts our 3D feature maps to 1D feature vectors

        for i in xrange(num_dense):
            model.add(Dense(dense_size, activation="sigmoid"))

        descriptor = {
            "conv_filters_num": conv_filters_num,
            "dense_size": dense_size,
            "num_conv": num_conv,
            "num_dense": num_dense
        }

        return model, descriptor

    return create_model

def get_dense_model_creation_func(
        dense_size
):

    def create_model(X):

        model = Sequential()


        for i,sz in enumerate(dense_size):
            if i == 0:
                model.add(Dense(sz, activation="relu", input_shape=X[0].shape))
            else:
                model.add(Dense(sz, activation="relu"))

        descriptor = {
            "dense_size": dense_size
        }

        return model, descriptor

    return create_model


def get_reuse_model_creation_func(
        model_manager,
        how_many_layers_to_remove=2,
        cut_at_layer_name=None,
        do_fine_tuning=True,
        incremental_fine_tuning=True,
        add_layers_function=None,
        freeze_layers=True,
        add_dropouts=False,
        drop_perc=0.2,
        sigmoid_to_relu=False,
        fine_tuning_epochs=1000
):
    """
    This is an helper function that returns a function that is capable to adapt a model to a new task by removing some
    layers and eventually add in new ones.
    :param model_manager: The class representing the old model
    :param how_many_layers_to_remove: An integer that specifies how many layers we want to remove. Usually we want to remove
                                      all the output layers (so last activation layer + last dense layer -->
                                      in that case this parameter would be 2)
    :param cut_at_layer_name: The name of the layer where the model has to be cut. If this is different from None the parameter
                              how_many_layers_to_remove will have no effect.
    :param do_fine_tuning: Whether the fine tuning should be performed or not. (see the train method of NeuralNetworkTrainer for details)
    :param incremental_fine_tuning: Whether the fine tuning should be performed in an incremental way or not. (see the train method of NeuralNetworkTrainer for details)
    :param add_layers_function: Optional. If specified it has to be a function that accepts a model, adds layers to it,
                                and returns the new model along with a descriptor of it, that will be concatenated to
                                the descriptor of the main model.
                                WARNING: the model will be a Model class, so functional keras has to be used. Refer to
                                the train method of NeuralNetworkTrainer in order to see how such model has to be treated.
    :param freeze_layers: boolean. If True old layers weights will be frozen. (see the train method of NeuralNetworkTrainer for details)
    :param add_dropouts: Whether dropouts should be added or not (if the model already has dropouts they will be removed first.
    :param drop_perc: The dropout percentag that has to be used
    :param sigmoid_to_relu: If true, all sigmoid activations will be changed to relus. This will destroy the model in most cases
    :param fine_tuning_epochs: How many epochs use for each fine tuning step
    :return: The function that creates the model
    """

    def create_model(X):

        model = model_manager.get_model()

        if cut_at_layer_name is None:
            cut_layer = model.layers[-(how_many_layers_to_remove + 1)].output
        else:
            cut_layer = model.get_layer(cut_at_layer_name).output

        model = Model(model.input, cut_layer)
        # Be careful. From now on the model is no more a sequential one but is instance of the Model class. This means
        # that functional keras has to be used in order to correctly use it.
        # Switched to functional keras because no way was found to use a simple sequential model.

        if freeze_layers:
            # Freezing all layers of the truncated model: we want to use those weights during training,
            # changing only the output layer.
            # In this way we force our model to use the old representation, then we will need to fine tune the model
            for layer in model.layers:
                layer.trainable = False

        if add_dropouts:
            ft = FineTuner(model)
            ft.remove_dropouts()
            ft.add_dropouts(drop_perc)
            model = ft.get_model()

        if sigmoid_to_relu:
            ft = FineTuner(model)
            ft.sigmoid_to_relu()
            model = ft.get_model()

        descriptor = {}
        if add_layers_function is not None:
            model, descriptor = add_layers_function(model)

        descriptor["original_model"] = model_manager.get_model_name()
        descriptor["do_fine_tuning"] = do_fine_tuning
        descriptor["incremental_fine_tuning"] = incremental_fine_tuning
        descriptor["fine_tuning_epochs"] = fine_tuning_epochs

        return model, descriptor

    return create_model

class ModelCheckpointEnhanced(Callback):
    """Save the model after every epoch.

    `filepath` can contain named formatting options,
    which will be filled the value of `epoch` and
    keys in `logs` (passed in `on_epoch_end`).

    For example: if `filepath` is `weights.{epoch:02d}-{val_loss:.2f}.hdf5`,
    then the model checkpoints will be saved with the epoch number and
    the validation loss in the filename.

    # Arguments
        filepath: string, path to save the model file.
        monitor: quantity to monitor.
        verbose: verbosity mode, 0 or 1.
        save_best_only: if `save_best_only=True`,
            the latest best model according to
            the quantity monitored will not be overwritten.
        mode: one of {auto, min, max}.
            If `save_best_only=True`, the decision
            to overwrite the current save file is made
            based on either the maximization or the
            minimization of the monitored quantity. For `val_acc`,
            this should be `max`, for `val_loss` this should
            be `min`, etc. In `auto` mode, the direction is
            automatically inferred from the name of the monitored quantity.
        save_weights_only: if True, then only the model's weights will be
            saved (`model.save_weights(filepath)`), else the full model
            is saved (`model.save(filepath)`).
        period: Interval (number of epochs) between checkpoints.
    """

    def __init__(self, models_folder, name, model_descriptor, descriptor_suffix, monitor='val_loss', verbose=0,
                 save_best_only=False, save_weights_only=False,
                 mode='auto', period=1, extension=".model"):
        super(ModelCheckpointEnhanced, self).__init__()
        self.monitor = monitor
        self.verbose = verbose
        self.models_folder = models_folder
        self.name = name
        self.filepath = os.path.join(models_folder, name)
        self.save_best_only = save_best_only
        self.save_weights_only = save_weights_only
        self.period = period
        self.epochs_since_last_save = 0
        self.last_saved = None
        self.best_data = None
        self.best_descriptor = None
        self.best_name = None
        self.model_descriptor = model_descriptor
        self.descriptor_suffix = descriptor_suffix
        self.extension = extension

        if mode not in ['auto', 'min', 'max']:
            print('ModelCheckpoint mode %s is unknown, '
                          'fallback to auto mode.' % (mode))
            mode = 'auto'

        if mode == 'min':
            self.monitor_op = np.less
            self.best = np.Inf
        elif mode == 'max':
            self.monitor_op = np.greater
            self.best = -np.Inf
        else:
            if 'acc' in self.monitor or self.monitor.startswith('fmeasure'):
                self.monitor_op = np.greater
                self.best = -np.Inf
            else:
                self.monitor_op = np.less
                self.best = np.Inf

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        self.epochs_since_last_save += 1
        if self.epochs_since_last_save >= self.period:
            self.epochs_since_last_save = 0
            full_name = self.name.format(epoch=epoch + 1, **logs)
            filepath = self.filepath.format(epoch=epoch + 1, **logs)
            if self.save_best_only:
                current = logs.get(self.monitor)
                if current is None:
                    print('Can save best model only with %s available, '
                                  'skipping.' % (self.monitor))
                else:
                    if self.monitor_op(current, self.best):
                        if self.verbose > 0:
                            print('\nEpoch %05d: %s improved from %0.5f to %0.5f,'
                                  ' saving model to %s'
                                  % (epoch + 1, self.monitor, self.best,
                                     current, filepath))
                        self.best = current

                        # Renaming and then removing in order to avoid data loss in case of errors

                        if self.last_saved is not None:
                            last_model_path = self.last_saved + self.extension
                            last_descriptor_path = self.last_saved + self.descriptor_suffix

                            os.rename(last_model_path, last_model_path + "_back")
                            os.rename(last_descriptor_path, last_descriptor_path + "_back")

                        if self.save_weights_only:
                            self.model.save_weights(filepath + self.extension, overwrite=True)
                        else:
                            self.model.save(filepath + self.extension, overwrite=True)

                        # Saving descriptor
                        current_descriptor = deepcopy(self.model_descriptor)
                        current_descriptor["data"] = logs
                        with open(filepath + self.descriptor_suffix, "wt") as output:
                            json.dump(current_descriptor, output, indent=4)

                        if self.last_saved is not None:
                            os.remove(last_model_path + "_back")
                            os.remove(last_descriptor_path + "_back")

                        self.last_saved = filepath
                        self.best_data = logs
                        self.best_descriptor = current_descriptor
                        self.best_name = full_name

                    else:
                        if self.verbose > 0:
                            print('\nEpoch %05d: %s did not improve from %0.5f' %
                                  (epoch + 1, self.monitor, self.best))
            else:
                #TODO: in case we want to save every checkpoint this part has to be adapted
                if self.verbose > 0:
                    print('\nEpoch %05d: saving model to %s' % (epoch + 1, filepath))
                if self.save_weights_only:
                    self.model.save_weights(filepath, overwrite=True)
                else:
                    self.model.save(filepath, overwrite=True)

    def get_best_data(self):
        return self.best_data

    def get_best_descriptor(self):
        return self.best_descriptor

    def get_best_name(self):
        return self.best_name
