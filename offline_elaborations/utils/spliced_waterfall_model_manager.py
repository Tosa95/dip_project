import os
from copy import deepcopy


class SplicedWaterfallModelManager(object):

    def __init__(self, signosig_model_manager, leafnodes_model_manager_w_nosign, leafnodes_model_manager,
                 low_threshold=0.65, high_threshold=0.9, sig_threshold=0.0):
        self._signosig_model_manager = signosig_model_manager
        self._leafnodes_model_manager = leafnodes_model_manager
        self._leafnodes_model_manager_w_nosign = leafnodes_model_manager_w_nosign
        self._low_threshold = low_threshold
        self._high_threshold = high_threshold
        self._sig_threshold = sig_threshold

    def get_signosig_model(self):

        return self._signosig_model_manager.get_model

    def get_sigtype_model(self):

        return self._leafnodes_model_manager.get_model

    def get_sigtype_w_nosign_model(self):

        return self._leafnodes_model_manager_w_nosign.get_model

    def get_leafnodes_m_descriptor(self):

        return self._leafnodes_model_manager.get_descriptor()

    def get_prediction_TID_VID_perc(self, img):

        # TODO: fix this 

        raise Exception("Not implemented.")

    def get_prediction_label_perc(self, img, prep_image=True, verbose=False):
        """
        Returns the most probable label and its probability given an image (numpy array)
        :param img: The image
        :param prep_image: If the boolean is true the image will be preprocessed
        :param verbose:
        :return: A couple: label, probability
        """

        lbl, perc = self._signosig_model_manager.get_prediction_label_perc(img)
        if verbose:
            print "signosig: ", lbl, perc
        if (lbl == "sig" and perc > self._sig_threshold) or (lbl == "nosig" and perc < self._low_threshold):
            lbl, perc = self._leafnodes_model_manager.get_prediction_label_perc(img, prep_image)
            if verbose:
                print "sigtype: ", lbl, perc
        elif (lbl == "sig" and perc < self._sig_threshold) or (lbl == "nosig" and perc < self._high_threshold):
            lbl, perc = self._leafnodes_model_manager_w_nosign.get_prediction_label_perc(img, prep_image)
            if verbose:
                print "sigtype: ", lbl, perc

        return lbl, perc

    def get_model_name(self):

        return self._signosig_model_manager.get_model_name() + "___" + self._leafnodes_model_manager_w_nosign.get_model_name()\
               + "___" + self._leafnodes_model_manager.get_model_name() + "___low_threshold_%.2f" % self._low_threshold \
               + "_high_threshold_%.2f" % self._high_threshold + "___sig_threshold_%.2f" % self._sig_threshold
