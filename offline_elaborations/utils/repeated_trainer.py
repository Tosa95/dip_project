from copy import deepcopy


class RepeatedTrainer(object):

    def __init__(self, nnt, how_many_retrains=5, best_model_filename_template="{old_name}_RT_BEST"):
        self._nnt = nnt
        self._how_many_retrains = how_many_retrains
        self._best_model_filename_template = best_model_filename_template

    def single_train(self):
        curr_nnt = deepcopy(self._nnt)
        return curr_nnt.train()

    def _better_greater(self, q1, q2):
        return q1 > q2

    def _better_less(self, q1, q2):
        return q1 < q2

    def get_monitored_quantity_name(self):
        return self._nnt.get_monitored_quantity()

    def better(self, mm1, mm2):

        mon_q = self.get_monitored_quantity_name()

        # Checks if we want to keep models with higher or lower values in the monitored quantity
        # Code adapted from keras ModelCheckpoint object
        if 'acc' in mon_q or mon_q.startswith('fmeasure'):
            better_f = self._better_greater
        else:
            better_f = self._better_less

        if better_f(mm1.get_validation_data()[mon_q], mm2.get_validation_data()[mon_q]):
            return True

        return False

    def get_monitored_quantity_value(self, mm):

        return mm.get_validation_data()[self.get_monitored_quantity_name()]

    def train(self, starting_model=None):

        best = starting_model

        for i in xrange(self._how_many_retrains):

            print "REP_TRAIN: Round %d" % (i+1)

            curr_mm = self.single_train()

            if best is None or self.better(curr_mm, best):

                if best is not None:
                    best.delete_files()
                    print "REP_TRAIN: kept because it's better than best. %s was %.3f and becomes %.3f" % \
                          (self.get_monitored_quantity_name(), self.get_monitored_quantity_value(best),
                           self.get_monitored_quantity_value(curr_mm))

                else:
                    print "REP_TRAIN: kept because it's first round. %s = %.3f" % \
                          (self.get_monitored_quantity_name(), self.get_monitored_quantity_value(curr_mm))

                # TODO: here we are overwriting old best model without making a backup before. That is not good.
                curr_mm.rename_files(self._best_model_filename_template)

                best = curr_mm

            else:

                curr_mm.delete_files()

                print "REP_TRAIN: not kept because it's worse than best. %s was %.3f for best, trained is %.3f" % \
                      (self.get_monitored_quantity_name(), self.get_monitored_quantity_value(best),
                       self.get_monitored_quantity_value(curr_mm))

        return best






