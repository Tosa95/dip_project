import os
import shutil
from collections import namedtuple
from copy import deepcopy

import cv2
import json

import keras

import numpy as np
from keras import Model

from global_settings import get_settings
from service_access import get_img_by_id, url_to_image
from utils.dataset_creator import get_random_dataset_flatten
from utils.dataset_manager import LabelsTraducer
from utils.image_caching import ImageCache
from utils.images import create_images_transformers_from_json_settings, LastSeen, resize_img_to_mp, put_text_on_image

from keras import backend as K

from utils.keras_fuck import Reshape2

LblTidVidPerc = namedtuple('LblTidVidPerc', ['lbl', 'tid', 'vid', 'perc'])

class ModelManager(object):

    def __init__(self, model_name=None, models_folder=get_settings().get_models_path(), descriptor_suffix="_descr.json",
                 image_cache=ImageCache(get_settings().get_img_cache_path()),
                 model=None, model_descriptor=None, backup_files_suffix="_back", always_reload_model=False):
        self._model_name = model_name
        self._model = model
        self._descriptor = model_descriptor
        self._descriptor_suffix = descriptor_suffix
        self._models_folder = models_folder
        self._image_cache = image_cache
        self._backup_files_suffix = backup_files_suffix
        self._autoenc_to_vec = None
        self._autoenc_vec_to_img = None
        self._always_reload_model = always_reload_model

    def get_model_name(self):
        return self._model_name

    def set_model_name(self, new_name):
        self._model_name = new_name

    def get_model_file_name(self):
        return self._models_folder + "/" + self._model_name + ".model"

    def get_descriptor_file_name(self):
        return self._models_folder + "/" + self._model_name + self._descriptor_suffix

    def get_model(self):

        if self._model is None or self._always_reload_model:
            self._model = keras.models.load_model(str(self.get_model_file_name()),
                                                  custom_objects={"Reshape2":Reshape2})

        return self._model

    def get_descriptor(self):

        if self._descriptor is None:
            with open(self.get_descriptor_file_name(), "rt") as input:
                self._descriptor = json.load(input)

        return self._descriptor

    def get_validation_data(self):

        return self.get_descriptor()["data"]

    def get_dataset_structure(self):

        return self.get_descriptor()["dataset_descriptor"]["structure"]

    def get_images_transformer(self):

        return create_images_transformers_from_json_settings(
            self.get_descriptor()["dataset_descriptor"]["image_transformer_settings"])

    def get_prediction(self, img, prep_image=True):

        if prep_image:
            img_prepared = self.get_images_transformer().prepare_single_image_for_prediction(img)
        else:
            x, y, ch = img.shape
            img_prepared = img.reshape(-1, x, y, ch)
        prediction = self.get_model().predict([img_prepared])[0]

        return prediction

    def get_prediction_TID_VID_perc(self, img):
        lbl, perc = self.get_prediction_label_perc(img)

        dstruct = self.get_dataset_descriptor()["structure"]

        #TODO: optimize here
        label_data = [entry for entry in dstruct if entry[0] == lbl][0]

        tid, vid = label_data[1][0]

        return tid, vid, perc

    def get_autoenc_predicted_vector(self, img):
        img_prepared = self.get_images_transformer().prepare_single_image_for_prediction(img)

        if self._autoenc_to_vec is None:
            model = self.get_model()
            self._autoenc_to_vec = K.function([model.input],[model.get_layer("intermediate_vector").output])

        prediction = self._autoenc_to_vec([img_prepared])[0]

        return prediction

    def get_autoenc_image_from_vector(self, vector):

        model = self.get_model()

        if self._autoenc_vec_to_img is None:
            index = None
            for idx, layer in enumerate(model.layers):
                if layer.name == "intermediate_vector":
                    index = idx
                    break

            self._autoenc_vec_to_img = K.function([model.layers[index+1].input], [model.layers[-1].output])

        prediction = self._autoenc_vec_to_img([np.reshape(vector,(-1,np.max(vector.shape)))])[0]

        return (np.reshape(prediction,(40,40,3))*255).astype(np.uint8)

    def get_prediction_lbl_TID_VID_perc_vector(self, img):

        pred = self.get_prediction(img)

        lbl_trad = LabelsTraducer(self.get_dataset_descriptor())
        dstruct = self.get_dataset_descriptor()["structure"]

        res = []

        for i, perc in enumerate(pred):
            lbl = lbl_trad.get_label_from_index(i)
            # TODO: optimize here
            label_data = [entry for entry in dstruct if entry[0] == lbl][0]
            tid, vid = label_data[1][0]
            res.append(LblTidVidPerc(lbl, tid, vid, perc))

        res = sorted(res, key=lambda t: t.perc, reverse=True)

        return res

    def get_prediction_label_perc(self,img,prep_image=True):
        """
        Returns the most probable label and its probability given an image (numpy array)
        :param img: The image
        :param prep_image: If the boolean is true the image will be preprocessed
        :return: A couple: label, probability
        """
        prediction = self.get_prediction(img, prep_image)
        index = np.argmax(prediction)
        perc = np.max(prediction)
        lbl = LabelsTraducer(self.get_dataset_descriptor()).get_label_from_index(index)
        return lbl, perc

    def get_dataset_descriptor(self):

        return self.get_descriptor()["dataset_descriptor"]

    def simple_test(self, rows, cols):
        """
        Tests the network by taking random images from the service, with a distribution compatible with the one used
        to train the network.
        It will display a rectangle of images rowsxcols
        :param rows: How many image in the rows
        :param cols: How many image in the columns
        """

        ls = LastSeen(rows, cols, 150)

        ids = get_random_dataset_flatten(self.get_dataset_structure(), rows * cols + 10)

        for id in ids:
            print id
            if self._image_cache is None:
                img = get_img_by_id(id)
            else:
                img = self._image_cache.get_image_by_id(id)

            lbl, perc = self.get_prediction_label_perc(img)

            img = resize_img_to_mp(img, 0.04)
            img = put_text_on_image(img, "%s [%.2f]" % (lbl, perc))

            ls.add_img(img)

        cv2.imshow("img", ls.get_grid())
        cv2.waitKey(0)

    def simple_test_from_urls(self, rows, cols, img_urls):
        """
        Tests the network by taking images from a list of urls.
        It will display a rectangle of images rowsxcols
        :param rows: How many image in the rows
        :param cols: How many image in the columns
        :param img_urls: The list of urls
        """

        ls = LastSeen(rows, cols, 150)

        for i,url in enumerate(img_urls):

            try:
                print "%d on %d" % (i+1, len(img_urls))

                if self._image_cache is None:
                    img = url_to_image(url)
                else:
                    img = self._image_cache.get_image_by_url(url)

                lbl, perc = self.get_prediction_label_perc(img)

                img = resize_img_to_mp(img, 0.04)
                img = put_text_on_image(img, "%s [%.2f]" % (lbl, perc))

                ls.add_img(img)

            except Exception as e:
                print e

        cv2.imshow("img", ls.get_grid())
        cv2.waitKey(0)

    def rename_files(self, new_name_template):
        """
        Renames all model files with the given template
        :param new_name_template: The template used to generate the new filename.
                                  Can use all data from the model descriptor plus {old_name} for taking inserting the old
                                  name of the files.
        :return:
        """

        old_descriptor_file_name = self.get_descriptor_file_name()
        old_model_file_name = self.get_model_file_name()

        descr = deepcopy(self.get_descriptor())
        descr["old_name"] = self._model_name

        self._model_name = new_name_template.format(**descr)

        os.rename(old_descriptor_file_name, self.get_descriptor_file_name())
        os.rename(old_model_file_name, self.get_model_file_name())

    def delete_files(self):
        os.remove(self.get_descriptor_file_name())
        os.remove(self.get_model_file_name())


