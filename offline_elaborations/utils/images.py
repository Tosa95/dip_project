from abc import ABCMeta, abstractmethod
from math import floor, sqrt

import cv2
import numpy as np

from copy_images import chunks


class BasicImagesTransformer(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def preprocess_image(self, img):
        pass

    @abstractmethod
    def reshape_images_dataset(self, dataset):
        pass

    @abstractmethod
    def get_settings(self):
        pass

    @abstractmethod
    def get_img_size_x(self):
        pass

    @abstractmethod
    def get_img_size_y(self):
        pass


class ImagesTransformer(BasicImagesTransformer):

    def __init__(self, img_size_x, img_size_y, channels=3):
        self._img_size_x = img_size_x
        self._img_size_y = img_size_y
        self._channels = channels

    def get_settings(self):
        return {
            "img_size_x": self._img_size_x,
            "img_size_y": self._img_size_y,
            "channels": self._channels
        }

    def get_img_size_x(self):
        return self._img_size_x

    def get_img_size_y(self):
        return self._img_size_y

    def get_img_tot_pixels(self):
        return self._img_size_x*self._img_size_y*self._channels

    def preprocess_image(self, img):
        img_resized = cv2.resize(img, (self._img_size_x, self._img_size_y))

        img_resized = img_resized.astype(np.double) / 255

        return img_resized

    def reshape_images_dataset(self, dataset):
        return np.array(dataset).reshape(-1, self._img_size_x, self._img_size_y, self._channels)

    def prepare_single_image_for_prediction(self, img):
        img = self.preprocess_image(img)
        return img.reshape(-1, self._img_size_x, self._img_size_y, self._channels)


class DoNotTransformImagesTransformer(BasicImagesTransformer):
    """
    Stub class used when no transformations are needed (example: for the computation of the confusion matrix)
    """

    def get_settings(self):
        return {"name": "DoNotTransformImagesTransformer"}

    def get_img_size_x(self):
        return None

    def get_img_size_y(self):
        return None

    def reshape_images_dataset(self, dataset):
        return dataset

    def preprocess_image(self, img):
        return img


class LastSeen:
    def __init__(self, img_per_column=5, columns=3, pixels=100):
        self._img_per_column = img_per_column
        self._columns = columns
        self._max_img = img_per_column * columns
        self._pixels = pixels
        self._imglist = []

    def resize_img(self, img):

        height, width, _ = img.shape

        if height > width:
            resize_ratio = float(self._pixels) / height
        else:
            resize_ratio = float(self._pixels) / width

        return cv2.resize(img, (int(width * resize_ratio), int(height * resize_ratio)))

    def add_img(self, img):
        if len(self._imglist) >= self._max_img:
            del self._imglist[0]

        self._imglist.append(self.resize_img(img))

    def get_top_left_point(self, linear_index):

        x = int(floor(float(linear_index) / self._img_per_column))
        y = linear_index - x * self._img_per_column

        return y * self._pixels, x * self._pixels

    def get_grid(self):

        result = np.ones((self._pixels * self._img_per_column, self._pixels * self._columns, 3))*128

        for i, img in enumerate(self._imglist):
            h, w, _ = img.shape
            y, x = self.get_top_left_point(i)
            result[y:y + h, x:x + w] = img

        return result.astype(np.uint8)


def create_images_transformers_from_json_settings(settings):
    return ImagesTransformer(**settings)


def put_text_on_image(img, text, color=(0, 255, 0)):
    h, w, c = img.shape

    new_img = np.zeros((h + 40, w, c), np.uint8)

    new_img[:h, :w, :] = img

    h, w, c = new_img.shape

    font = cv2.FONT_HERSHEY_COMPLEX_SMALL

    text_lines = list(chunks(text, 15))

    dy = 15
    y0 = h - len(text_lines) * dy

    for i, line in enumerate(text_lines):
        y = y0 + i * dy
        # print y
        cv2.putText(new_img, line.encode('utf-8'), (0, y), font, 0.9, color, 1)

    # cv2.putText(new_img, text.encode('utf-8'), (0, h - 7), font, 0.9, color, 1)

    return new_img


def resize_img_to_mp(img, megapixels):
    megapixels = megapixels * 1000000

    height, width, _ = img.shape

    original_aspect_ratio = float(height) / width

    new_width = sqrt(megapixels / original_aspect_ratio)

    new_height = original_aspect_ratio * new_width

    res = cv2.resize(img, (int(new_width), int(new_height)))

    return res
