import numpy as np
import os
import io

import cv2
import hashlib

from service_access import url_to_image, get_img_url


class ImageCache(object):

    def __init__(self, cache_folder):
        self._cache_folder = cache_folder

    def url_to_filename(self, url):
        m = hashlib.md5()
        m.update(url)
        return m.hexdigest()

    def get_image_by_url(self, url):
        filename = self.url_to_filename(url)
        full_name = self._cache_folder + "/" + filename + ".jpg"
        if not os.path.isfile(full_name):
            img = url_to_image(url)
            if img is None:
                return None
            cv2.imwrite(full_name, img)
            # print "Image cache MISS!"

        #print full_name

        return cv2.imread(full_name)

    def get_image_by_id(self, id):
        url = get_img_url(id)

        return self.get_image_by_url(url)

    def read_image(self, id):
        url = get_img_url(id)
        filename = self.url_to_filename(url)
        full_name = self._cache_folder + "/" + filename + ".jpg"
        if not os.path.isfile(full_name):
            img = url_to_image(url)
            if img is None:
                return None
            cv2.imwrite(full_name, img)
            # print "Image cache MISS!"

        with io.open(full_name, 'rb') as image_file:
            image = image_file.read()

        return image

