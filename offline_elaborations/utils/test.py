# from pprint import pprint
#
# import cv2
#
# from global_settings import Settings, get_settings
# from service_access import get_label_type_tree
#
# from utils.dataset_creator import DatasetCreator, get_full_description_dataset_name, get_leafs, leafs_to_dataset_struct
# from utils.image_caching import ImageCache
# from utils.image_extenders import CompoundImageExtender, PerspectiveImageExtender, TranslationImageExtender, \
#     BlurImageExtender, NoiseImageExtender, LuminosityImageExtender, ALL_IMAGES_EXTENDERS
# from utils.images import ImagesTransformer, DoNotTransformImagesTransformer
# from utils.saving_methods import MemoryDatasetPersister
#
# SETTINGS = get_settings()
# DATASETS_FOLDER = SETTINGS.get_dataset_path()
# MIN_IMAGES_PER_LABEL = 10
# IMAGES_PER_LABEL_AFTER_EXPANSION = 30
#
# tree = get_label_type_tree()
# leafs = []
#
# get_leafs(tree,leafs)
#
# DATASET_STRUCTURE = leafs_to_dataset_struct(leafs, MIN_IMAGES_PER_LABEL)
#
# pprint(DATASET_STRUCTURE)
#
# print "Found %d labels with more than %d images" % (len(DATASET_STRUCTURE), MIN_IMAGES_PER_LABEL)
#
# dc = DatasetCreator(DATASETS_FOLDER, image_cache=ImageCache(SETTINGS.get_img_cache_path()),
#                     dataset_persister=MemoryDatasetPersister())
#
# dm = dc.create_dataset(
#     get_full_description_dataset_name("leafnodes"),
#     DATASET_STRUCTURE,
#     DoNotTransformImagesTransformer(),
#     balanced=False,
#     group_count_limit=IMAGES_PER_LABEL_AFTER_EXPANSION,
#     images_extender=ALL_IMAGES_EXTENDERS,
#     expand_validation=True
# )
#
#
# for img in dm.get_dataset()["X"]:
#     cv2.imshow("test", img)
#     cv2.waitKey(10000)
#
import json

from utils.scores import PolyfitSingleAxisScore, SplineSingleAxisScore, Interp1dSingleAxisScore, CompoundScore, \
    AddScoreCombiner, MultiplyScoreCombiner


# score_sig_p = SplineSingleAxisScore([(1, 0.5), (0.65, 0.95), (0.95, 0.95), (0.4, 0.5), (0.45, 0.5), (0.5, 0.5), (0.55, 0.5), (0.6, 0.5),
#                                      (0.2, 0.1), (0.1, 0.05), (0, 0)])

score_sig_p = Interp1dSingleAxisScore([(1, 0.5), (0.95, 0.95), (0.9, 1), (0.65, 0.85), (0.6, 0.7),  (0.4, 0.65),
                                       (0.39,0.00001),  (0, 0.00001)])

score_leaf_p = Interp1dSingleAxisScore([(0, 0), (0.3,1), (0.7,0.9), (1,0.5)])

score_snum = Interp1dSingleAxisScore([(0,1), (1,0)])

cs = CompoundScore([score_leaf_p, score_snum], MultiplyScoreCombiner([1.4,2]))

# print cs.get_score([0.9, 0.10])

score_snum.plot()
score_leaf_p.plot()
score_sig_p.plot()

cs.plot()

# # from service_access import get_label_type_tree
# # from utils.scores import LabelsTreeHelper
# #
# # tree = get_label_type_tree()
# #
# # lth = LabelsTreeHelper(tree)
#
# # print "no", lth.get_lbl_samples_cnt("no")
# # print "semaforo_verde", lth.get_lbl_samples_cnt("semaforo_verde")
# # print lth.get_max_samples_cnt()
#
#
# # score_sig_p.plot()
# # score_snum.plot()

# scores = []
#
# scores.append([1, 1, 1, None, None])
# scores.append([1, 1, 1, None, None])
# scores.append([1, 1, 1, None, None])
# scores.append([1, 1, 1, None, None])
#
# print json.dumps(scores)
