import cv2
from copy import copy
from math import cos, sin, pi, radians

import numpy as np



def get_camera_matrix(f, px, py):

    f = float(f)
    px = float(px)
    py = float(py)

    return np.matrix([
        [f, 0, px, 0],
        [0, f, py, 0],
        [0, 0,  1, 0]
    ])

def get_X_rotation_matrix(theta):
    return np.matrix([
        [1,          0,           0],
        [0, cos(theta), -sin(theta)],
        [0, sin(theta),  cos(theta)]
    ])

def get_Y_rotation_matrix(theta):
    return np.matrix([
        [cos(theta),  0, sin(theta)],
        [0,           1,          0],
        [-sin(theta), 0, cos(theta)]
    ])

def get_Z_rotation_matrix(theta):
    return np.matrix([
        [cos(theta),  -sin(theta), 0],
        [sin(theta),   cos(theta), 0],
        [0,                     0, 1]
    ])

def rotate_3d_point (pt, tx, ty, tz):
    pt = np.transpose(np.matrix(pt))

    return get_X_rotation_matrix(tx) * get_Y_rotation_matrix(ty) * get_Z_rotation_matrix(tz) * pt

def rotate_square (square, tx, ty, tz):

    res = []

    for pt in square:
        ptt = copy(pt)
        ptt[2] = 0

        ptt = rotate_3d_point(ptt, tx, ty, tz)

        ptt[2] += pt[2]

        res.append([ptt[0, 0], ptt[1, 0], ptt[2, 0]])

    return res

def get_2d_point(matrix, point_3d):

    point_3d = copy(point_3d)
    point_3d.append(1)

    pt2d = np.dot(matrix, np.transpose(np.matrix(point_3d)))
    pt2d = np.transpose(pt2d / pt2d[2]) # To non homogeneous coords

    return [pt2d[0, 0], pt2d[0, 1]]

def get_square_2d_pts (square, camera_matrix):
    return [get_2d_point(camera_matrix, pt3) for pt3 in square]


def perspectivize_image(img, dx, dy, dz, dist_from_camera=0, cam_f=1000,
                        result_image_w=None, result_image_h=None):
    """
    Changes the perspective of the image
    :param img: The original image
    :param dx: Angle on x, in degrees
    :param dy: Angle on y, in degrees
    :param dz: Angle on z, in degrees
    :param dist_from_camera:
    :param cam_f:
    :param result_image_w:
    :param result_image_h:
    :return: The new image
    """

    h, w, _ = img.shape

    if result_image_w is None:
        result_image_w = w

    if result_image_h is None:
        result_image_h = h

    sqx = w/2.0
    sqy = h/2.0
    sqz = cam_f + dist_from_camera

    square_3d = [[-sqx, -sqy, sqz], [sqx, -sqy, sqz], [-sqx, sqy, sqz], [sqx, sqy, sqz]]

    camera_matrix = get_camera_matrix(cam_f, result_image_w / 2.0, result_image_h / 2)

    square_3d_rotated = rotate_square(square_3d, radians(dx), radians(dy), radians(dz))
    square_2d = get_square_2d_pts(square_3d_rotated, camera_matrix)

    pts1 = np.float32([[0, 0], [w, 0], [0, h], [w, h]])
    pts2 = np.float32(square_2d)

    M = cv2.getPerspectiveTransform(pts1, pts2)
    dst = cv2.warpPerspective(img, M, (result_image_w, result_image_h))

    return dst