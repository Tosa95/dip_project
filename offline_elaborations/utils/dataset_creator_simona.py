import cv2
import json
import os
import joblib as jl
import random
import time

from global_settings import Settings
from service_access import get_dataset, get_img_by_id
import numpy as np

from utils.images import ImagesTransformer, LastSeen


def get_full_description_dataset_name(base_name):
    return base_name + "__b{balanced}_gcl{group_count_limit}_isx{img_size_x}_isy{img_size_y}_vp{validation_perc}_ex{expanded}"


class DatasetCreator(object):

    def __init__(self, datasets_folder, descriptor_suffix="_desc.json", image_cache=None):
        self._datasets_folder = datasets_folder
        self._descriptor_suffix = descriptor_suffix
        self._image_cache = image_cache

    def split_list(self, lst, perc):
        """
        Splits a list in two parts, the second one of the size defined by perc
        :param lst: The list to be split
        :param perc: the split percentage
        :return: A couple of lists
        """
        split_index = int(len(lst)*perc)
        return lst[split_index:], lst[:split_index]

    def split_dataset(self, original_dataset, perc):
        """
        Splits a dataset into train and test
        :param original_dataset: The dataset to be split, in the format provided by the remote service
        :param perc: The split percentage, from 0 to 1, is the amount of data to reserve to validation
        :return: A couple of datasets: train and test
        """
        train_dataset = {}
        test_dataset = {}

        for lbl in original_dataset:
            random.shuffle(original_dataset[lbl]) # to be sure similar images aren't near
            train_lst,test_lst = self.split_list(original_dataset[lbl], perc)
            train_dataset[lbl] = train_lst
            test_dataset[lbl] = test_lst

        return train_dataset, test_dataset

    def compute_labels_representation(self, dataset_description):
        """
        Computes the output value for each of the possible labels.
        :param dataset_description: The data structure that describes the desired dataset
        :return: A copule containing:
            A map containing the association between labels and the index of the array that has to be 1
            An array that links the the index to the associated label
        """

        res_lbl_to_i = {}
        res_i_to_lbl = []
        labels = [group[0] for group in dataset_description]

        for i,label in enumerate(labels):
            res_lbl_to_i[label] = i
            res_i_to_lbl.append(label)

        return res_lbl_to_i, res_i_to_lbl

    def lbl_to_y_value(self, lbl, labels_repr):
        """
        Transforms a label into its representation used in the neural network (one hot array)
        :param lbl: The label
        :param labels_repr: The representations of labels
        :return:
        """
        index = labels_repr[lbl]
        res = np.zeros(len(labels_repr))
        res[index] = 1.0
        return res

    def get_name_given_descriptor(self, name_pattern, descriptor):
        """
        Substitutes properties into the name of the dataset
        :param name_pattern:
        :param descriptor:
        :return: The formatted name
        """
        return name_pattern.format(**descriptor)

    def elaborate_single_dataset(self, original_dataset, labels_repr, images_transformer,
                                 images_extender, group_count_limit):
        """
        Function that creates a single dataset (test or train)
        :param original_dataset: The dataset in the format provided by the remote service
        :param labels_repr: The structure that maps labels to their values
        :param images_transformer: The object responsible for image transformations (made in order to adapt the images to the dataset)
        :param images_extender:  The object responsible for creating different version of the same image in order to expand the dataset
        :param group_count_limit: The maximum number of samples for each group.
               If images extender is not None, it will be used to raise the number of samples of each group to this value
        :return: X, the set of inputs and y, the set of output. They are both numpy arrays
        """

        ids = []

        for lbl in original_dataset:
            for id in original_dataset[lbl]:
                ids.append((id, self.lbl_to_y_value(lbl, labels_repr)))

        result_dataset = []

        img_cache = {}

        for index, (id, lbl) in enumerate(ids):

            print "%.2f%% %d %d %s" % (float(index)/len(ids)*100, index, id, lbl)

            # ho tolto il ciclo su err perche' senno' quando trovava un'immagine corrotta andava in loop infinito
            img = None
            try:
                if self._image_cache is None:
                    img = get_img_by_id(id)
                else:
                    img = self._image_cache.get_image_by_id(id)
                img_cache[id] = img
            except Exception as ex:
                print("Id img: " + str(id))
                time.sleep(10)
                print ex

            try:
                result_dataset.append((images_transformer.preprocess_image(img), lbl))
            except Exception as ex:
                print ex

        if images_extender is not None:

            print "Expanding the dataset..."
            for lbl in original_dataset:
                to_be_added = group_count_limit - len(original_dataset[lbl])
                ids_to_extend = [random.choice(original_dataset[lbl]) for _ in xrange(to_be_added)]
                for id in ids_to_extend:
                    try:
                        print "EXTENDING %s %d" % (lbl, id)
                        extended_image = images_extender.extend(img_cache[id])
                        result_dataset.append((images_transformer.preprocess_image(extended_image), self.lbl_to_y_value(lbl, labels_repr)))

                    except Exception as ex:
                        print ex

        random.shuffle(result_dataset)

        X = []
        y = []

        for img, lbl in result_dataset:
            X.append(img)
            y.append(lbl)

        # Necessary for keras correct operation
        X = images_transformer.reshape_images_dataset(X)
        y = np.array(y)

        return X, y

    def create_dataset(self, name, dataset_description, images_transformer,
                       balanced=True, group_count_limit=0, validation_perc=0.2, images_extender=None):
        """
        Creates a dataset in order to train a neural model, and saves it
        :param name: The name of the dataset. Can contain placeholders in the form {<param>},
                     where param is any of the keys of the dataset_descriptor map
        :param dataset_description: The description of the dataset. Is a list of groups of labels, examples:

                dataset_description = [
                    ["pericolo",[[2,3]]],
                    ["precedenza",[[2,4]]],
                    ["divieto",[[2,5]]],
                    ["obbligo",[[2,6]]],
                    ["altro",[[2,173]]],
                    ["semaforo",[[2,174]]],
                    ["nosig",[[1,2]]]
                ]

                dataset_description = [
                    ["signal", [[2,3],[2,4],[2,5],[2,6],[2,172],[2,173]]],
                    ["no_signal", [[1,2]]]
                ]

        :param images_transformer: The object responsible for image transformations (made in order to adapt the images to the dataset)
        :param images_extender:  The object responsible for creating different version of the same image in order to expand the dataset.
                                 I image extender is None no expansion will be done.
        :param group_count_limit: The maximum number of samples for each group. If 0 no limit is assumed.
               If images extender is not None, it will be used to raise the number of samples of each group to this value
        :param balanced: True: The database will be balanced with real image. If false, it can be balanced providing an image extender
        :param validation_perc: Which percentage of data should be retained in order to create the validation set
        """

        service_dataset = get_dataset(dataset_description, balanced=balanced, group_count_limit=group_count_limit)
        labels_repr, index_to_labels = self.compute_labels_representation(dataset_description)

        dataset_descriptor = {
            "structure": dataset_description,
            "labels_repr": labels_repr,
            "index_to_labels": index_to_labels,
            "balanced": balanced,
            "group_count_limit": group_count_limit,
            "img_size_x": images_transformer.get_img_size_x(),
            "img_size_y": images_transformer.get_img_size_y(),
            "image_transformer_settings": images_transformer.get_settings(),
            "validation_perc": validation_perc,
            "expanded": images_extender is not None
        }

        train_dataset, test_dataset = self.split_dataset(service_dataset, validation_perc)

        X,y = self.elaborate_single_dataset(train_dataset, labels_repr, images_transformer, images_extender, int(group_count_limit*(1-validation_perc)))
        X_test, y_test = self.elaborate_single_dataset(test_dataset, labels_repr, images_transformer, images_extender=None, group_count_limit=0)

        result = {
            "X": X,
            "y": y,
            "X_test": X_test,
            "y_test": y_test
        }

        print "Saving..."

        full_name = self.get_name_given_descriptor(name, dataset_descriptor)

        full_name_dataset = full_name + ".joblib"
        full_name_descriptor = full_name + self._descriptor_suffix

        with open(os.path.join(self._datasets_folder + "/" + full_name_dataset), "wb") as output:
            jl.dump(result, output)

        with open(os.path.join(self._datasets_folder + "/" + full_name_descriptor), "w") as output:
            json.dump(dataset_descriptor, output, indent=4)

        print "Saved."


def get_random_dataset_flatten(dataset_description, tot_img, shuffle=True):

    gcl = tot_img/len(dataset_description)

    service_dataset = get_dataset(dataset_description, balanced=False, group_count_limit=gcl)

    res = []

    for lbl in service_dataset:
        for id in service_dataset[lbl]:
            res.append(id)

    if shuffle:
        random.shuffle(res)

    return res
