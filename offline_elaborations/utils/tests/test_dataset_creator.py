from global_settings import Settings, get_settings

from utils.dataset_creator import DatasetCreator, get_full_description_dataset_name
from utils.image_caching import ImageCache
from utils.image_extenders import CompoundImageExtender, PerspectiveImageExtender, TranslationImageExtender, \
    BlurImageExtender, NoiseImageExtender, LuminosityImageExtender, ALL_IMAGES_EXTENDERS
from utils.images import ImagesTransformer
from utils.saving_methods import MemoryDatasetPersister

SETTINGS = get_settings()
DATASET_FOLDER = SETTINGS.get_dataset_path()

DATASET_STRUCTURE = [
    ["pericolo",[[2,3]]],
    ["precedenza",[[2,4]]],
    ["divieto",[[2,5]]],
    ["obbligo",[[2,6]]],
    ["autovelox",[[2,172]]],
    ["altro",[[2,173]]],
    ["semaforo",[[2,174]]],
    ["nosig",[[1,2]]]
]

dc = DatasetCreator(DATASET_FOLDER, image_cache=ImageCache(SETTINGS.get_img_cache_path()),
                    dataset_persister=MemoryDatasetPersister())

dm = dc.create_dataset(
    get_full_description_dataset_name("sigtype"),
    DATASET_STRUCTURE,
    ImagesTransformer(40,40),
    balanced=False,
    group_count_limit=1,
    images_extender=ALL_IMAGES_EXTENDERS,
    separate_validation=True,
    validation_perc=100,
    hyper_expansion_size=100
)

print dm.get_dataset()["X"].shape
print dm.get_dataset()["X_test"].shape