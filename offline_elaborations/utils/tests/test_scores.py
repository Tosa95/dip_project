from utils.scores import Interp1dSingleAxisScore, CompoundScore, MultiplyScoreCombiner

score_sig_p = Interp1dSingleAxisScore([(1, 0.5), (0.95, 0.95), (0.9, 1), (0.65, 0.85), (0.6, 0.7),  (0.4, 0.65),
                                       (0.39,0.00001),  (0, 0.00001)])
score_leaf_p = Interp1dSingleAxisScore([(0, 0), (0.3,1), (0.7,0.9), (1,0.5)])
score_snum = Interp1dSingleAxisScore([(0,1), (1,0)])

# cs = CompoundScore([score_sig_p, score_leaf_p, score_snum], MultiplyScoreCombiner([1,1.4,2]))


cs = CompoundScore([score_sig_p, score_leaf_p], MultiplyScoreCombiner([1,1.4]))
# cs = CompoundScore([score_sig_p, score_snum], MultiplyScoreCombiner([1,2]))
# cs = CompoundScore([score_leaf_p, score_snum], MultiplyScoreCombiner([1.4,2]))


cs.plot()