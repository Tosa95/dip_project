from pprint import pprint

from service_access import get_label_type_tree
from utils.dataset_creator import get_leafs

import pandas as pd

tree = get_label_type_tree()
leafs = []

get_leafs(tree,leafs)

pprint(leafs)

res = pd.DataFrame([[l["name"],l["cnt"]] for l in leafs], columns=["label","count"])

res.to_excel("sign_distr.xlsx")