import pprint
import random
from os import remove

from service_access import get_all_images
from utils.multiple_models_trainer import MultipleModelsTrainer

# ids = random.sample(get_all_images(),20)

mmt = MultipleModelsTrainer("test")

print "val_acc"
pprint.pprint(mmt.analyze("val_acc", remove_below=0.02))

print "val_loss"
pprint.pprint(mmt.analyze("val_loss"))

mmt.analyze_to_dataframe(["val_acc", "val_loss"], remove_below=0.02)