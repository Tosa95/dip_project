import random

import cv2

from service_access import get_img_by_id
from utils.dataset_creator import get_random_dataset_flatten
from utils.image_extenders import TranslationImageExtender, PerspectiveImageExtender, ALL_IMAGES_EXTENDERS, \
    BlurImageExtender, NoiseImageExtender, LuminosityImageExtender, ColourTemperatureImageExtender, \
    SaturationImageExtender, CompoundImageExtender
from utils.images import LastSeen

DATASET_STRUCTURE = [
    ["aaa",[[6,88]]]
]

rows = 7
cols = 10

# rows = 3
# cols = 3

ids = random.sample(get_random_dataset_flatten(DATASET_STRUCTURE, rows * cols + 10),5)
# ids = [61798]

imgs = [get_img_by_id(id) for id in ids]

# imgext = ALL_IMAGES_EXTENDERS
# imgext = ColourTemperatureImageExtender(b_range=(0.9,1.1),r_range=(0.9,1.1))
# imgext = SaturationImageExtender(amt_range=(0.5,1.5))
# imgext = LuminosityImageExtender(amt_range=(0.3,1.8))

imgext=CompoundImageExtender(
    [
        PerspectiveImageExtender(dx_range=(30,50)),
        PerspectiveImageExtender(dy_range=(30,50)),
        PerspectiveImageExtender(dz_range=(10,20)),
        TranslationImageExtender(x_range=(0.01,0.05)),
        TranslationImageExtender(y_range=(0.01,0.05)),
        BlurImageExtender(x_range=(0.03,0.07)),
        BlurImageExtender(y_range=(0.03,0.07)),
        NoiseImageExtender(amt_range=(5,20)),
        LuminosityImageExtender(amt_range=(0.5, 1.5)),
        ColourTemperatureImageExtender(b_range=(0.9,1.1), r_range=(0.9,1.1)),
        SaturationImageExtender(amt_range=(0.5,1.5))
    ],
    (5,8)
)


# ls = LastSeen(img_per_column=8,columns=10)
ls = LastSeen(img_per_column=rows,columns=cols)

for i in range(100):
    imge = imgext.extend(random.choice(imgs))
    ls.add_img(imge)


cv2.imshow("test",ls.get_grid())
cv2.waitKey()