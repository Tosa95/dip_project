import cv2

from service_access import get_img_by_id
from utils.dataset_creator import get_random_dataset_flatten
from utils.images import LastSeen

DATASET_STRUCTURE = [
    ["aaa",[[6,88]]]
]

rows = 7
cols = 10

ids = get_random_dataset_flatten(DATASET_STRUCTURE, rows * cols + 10)

ls = LastSeen(rows, cols, 100)

for id in ids:

    ls.add_img(get_img_by_id(id))

cv2.imshow("img", ls.get_grid())
cv2.waitKey(0)