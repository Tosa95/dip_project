import sys

sys.path.append("../../")

from utils.resnets import get_dw_resnet_class_model_creation_func

from keras import Model
from keras.layers import Dense, Activation, Dropout

from decontouring.decontour import BackgroundSwapCompoundImageExtender
from decontouring.load_shapes import load_shapes_plots
from global_settings import get_settings
from service_access import get_label_type_tree
from utils.dataset_creator import get_leafs, leafs_to_dataset_struct, get_random_dataset_flatten
from utils.image_caching import ImageCache
from utils.image_extenders import CompoundImageExtender, PerspectiveImageExtender, TranslationImageExtender, \
    BlurImageExtender, NoiseImageExtender, LuminosityImageExtender, ColourTemperatureImageExtender, \
    SaturationImageExtender
from utils.images import ImagesTransformer
from utils.model_manager import ModelManager
from utils.multiple_models_trainer import MultipleModelsTrainer, ModelAndDataset
from utils.neural_training import get_dw_class_model_creation_func, get_standard_model_creation_func, \
    get_reuse_model_creation_func, get_autoenc_model_creation_func

tree = get_label_type_tree()
leafs = []

get_leafs(tree,leafs)

DATASET_STRUCTURE = leafs_to_dataset_struct(leafs, min_size=10)

DATASET_STRUCTURE_AE = [grp for grp in DATASET_STRUCTURE if grp[0] != "no"]

DATASET_STRUCTURE_SIGTYPE = [
    ["pericolo",[[2,3]]],
    ["precedenza",[[2,4]]],
    ["divieto",[[2,5]]],
    ["obbligo",[[2,6]]],
    ["autovelox",[[2,172]]],
    ["altro",[[2,173]]],
    ["semaforo",[[2,174]]],
    ["nosig",[[1,2]]]
]

DATASET_STRUCTURE_NO = [
    ["no", [[1, 2]]]
]

ids_no = get_random_dataset_flatten(DATASET_STRUCTURE_NO, 2000)

mm = ModelManager("signosig_ep17_vl0.148822210102_va0.95037593985", get_settings().get_models_path(),
                  image_cache=ImageCache(get_settings().get_img_cache_path()))

base_shapes = load_shapes_plots()

bs = {}

for shape_name, shape_contours in base_shapes.iteritems():
    bs[shape_name] = [sc.tolist() for sc in shape_contours]

BACK_SWAP_EXTENDER = BackgroundSwapCompoundImageExtender(
    img_morph_extenders=CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30, 50)),
            PerspectiveImageExtender(dy_range=(30, 65)),
            PerspectiveImageExtender(dz_range=(10, 20)),
            TranslationImageExtender(x_range=(0.05, 0.15)),
            TranslationImageExtender(y_range=(0.05, 0.15)),
            BlurImageExtender(x_range=(0.03, 0.07)),
            BlurImageExtender(y_range=(0.03, 0.07)),
        ],
        (2, 5),
        final_crop=0.0
    ),
    img_non_morph_extendes=CompoundImageExtender(
        [
            NoiseImageExtender(amt_range=(5, 20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6)),
            ColourTemperatureImageExtender(b_range=(0.8, 1.2), r_range=(0.8, 1.2)),
            SaturationImageExtender(amt_range=(0.5, 1.5))
        ],
        (2, 4),
        final_crop=0.0
    ),
    back_extenders=CompoundImageExtender(
        [
            NoiseImageExtender(amt_range=(5, 20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6)),
            ColourTemperatureImageExtender(b_range=(0.9, 1.1), r_range=(0.9, 1.1)),
            SaturationImageExtender(amt_range=(0.5, 1.5))
        ],
        (2, 3)
    ),
    background_ids=ids_no,
    base_shapes=bs,
    sign_decision_model_manager=mm,
    reference_back_id=11551
)

DATASET_PARAMETERS_SIGTYPE = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE_SIGTYPE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 3000,
    "expand_validation": True
}

DATASET_PARAMETERS = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 100,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6))
        ],
        (3,7)
    ),
    "expand_validation": True
}

DATASET_LITTLEST = {"name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 10,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6))
        ],
        (3,7)
    ),
    "expand_validation": True,
    "separate_validation": True,
    "validation_perc": 10
}

DATASET_PARAMETERS_HYPEREXP = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 100,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6))
        ],
        (3,7)
    ),
    "expand_validation": True,
    "hyper_expansion_size": 500
}

DATASET_PARAMETERS_HYPEREXP_1_5_k = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 100,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6))
        ],
        (3,7)
    ),
    "expand_validation": True,
    "hyper_expansion_size": 1500
}

DATASET_PARAMETERS_HYPEREXP_BEST = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 1500,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6))
        ],
        (3,7)
    ),
    "expand_validation": True,
    "hyper_expansion_size": 1500
}

DATASET_PARAMETERS_AE = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE_AE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 700,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6)),
            ColourTemperatureImageExtender(b_range=(0.9,1.1), r_range=(0.9,1.1)),
            SaturationImageExtender(amt_range=(0.5,1.5))
        ],
        (5,9)
    ),
    "expand_validation": True,
    "for_autoencoders": True
}

DATASET_PARAMETERS_HYPEREXP_BEST_FULL_TRANS = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 1500,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6)),
            ColourTemperatureImageExtender(b_range=(0.9,1.1), r_range=(0.9,1.1)),
            SaturationImageExtender(amt_range=(0.5,1.5))
        ],
        (5,9)
    ),
    "expand_validation": True,
    "hyper_expansion_size": 1500
}

DATASET_PARAMETERS_HYPEREXP_BEST_BACK_SWAP = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 1200,
    "images_extender": BACK_SWAP_EXTENDER,
    "expand_validation": True,
    "hyper_expansion_size": 1200
}

DATASET_PARAMETERS_HYPEREXP_BEST_BACK_SWAP_EA = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 1200,
    "images_extender": BACK_SWAP_EXTENDER,
    "expand_validation": True,
    "hyper_expansion_size": 1200,
    "expand_all": True
}


DATASET_PARAMETERS_HYPEREXP_20_to_500 = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 20,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6))
        ],
        (3,7)
    ),
    "expand_validation": True,
    "hyper_expansion_size": 500,
    "separate_validation": True,
    "validation_perc": 10
}

DATASET_PARAMETERS_HYPEREXP_20_to_500_FULL_TRANS = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 20,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6)),
            ColourTemperatureImageExtender(b_range=(0.9,1.1), r_range=(0.9,1.1)),
            SaturationImageExtender(amt_range=(0.5,1.5))
        ],
        (5,9)
    ),
    "expand_validation": True,
    "hyper_expansion_size": 500,
    "separate_validation": True,
    "validation_perc": 10
}

DATASET_PARAMETERS_HYPEREXP_20_to_500_BACK_SWAP = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 20,
    "images_extender": BACK_SWAP_EXTENDER,
    "expand_validation": True,
    "hyper_expansion_size": 500,
    "separate_validation": True,
    "validation_perc": 10
}

DATASET_PARAMETERS_HYPEREXP_1_to_200_FULL_TRANS = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 1,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6)),
            ColourTemperatureImageExtender(b_range=(0.9,1.1), r_range=(0.9,1.1)),
            SaturationImageExtender(amt_range=(0.5,1.5))
        ],
        (5,9)
    ),
    "expand_validation": True,
    "hyper_expansion_size": 200,
    "separate_validation": True,
    "validation_perc": 200
}

DATASET_PARAMETERS_HYPEREXP_1_to_200_BACK_SWAP = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 1,
    "images_extender": BACK_SWAP_EXTENDER,
    "expand_validation": True,
    "hyper_expansion_size": 200,
    "separate_validation": True,
    "validation_perc": 200
}


DATASET_PARAMETERS_NOEXP = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 100,
    "expand_validation": True
}

DATASET_PARAMETERS_NOEXP_BAL = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": True,
    "group_count_limit": 0,
    "expand_validation": True
}


DATASET_PARAMETERS_NOEXP_300 = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 300,
    "expand_validation": True
}

DATASET_PARAMETERS_NOEXP_500 = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 500,
    "expand_validation": True
}

DATASET_PARAMETERS_NOEXP_BIG = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 1500,
    "expand_validation": True
}

DATASET_PARAMETERS_BIG = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 500,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6))
        ],
        (3,7)
    ),
    "expand_validation": True
}


def KSAE_add_layers(model):
    output = model.layers[-1].get_output_at(-1)
    output = Dense(128, name="supplementar_dense")(output)
    output = Activation('relu', name="supplementar_dense_activation")(output)
    output = Dropout(0.4, name="supplementar_dense_dropout")(output)
    model = Model(model.input, output)

    return model, {}

mmt = MultipleModelsTrainer("test", patience=5, tests_per_model=20, models=[
    ModelAndDataset(
        name = "ST0",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=3,
            num_conv=2,
            dropout_perc=0.2,
            max_poolings=[(2, 2), (2, 2)],
            depth_mults=[1, 1],
            dense_size=[32]
        ),
        dataset_parameters=DATASET_PARAMETERS_SIGTYPE
    ),
    ModelAndDataset(
        name = "ST1",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=8,
            num_conv=2,
            dropout_perc=0.2,
            max_poolings=[(2, 2), (2, 2)],
            depth_mults=[1, 1],
            dense_size=[32]
        ),
        dataset_parameters=DATASET_PARAMETERS_SIGTYPE
    ),
    ModelAndDataset(
        name = "ST2",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=16,
            num_conv=2,
            dropout_perc=0.2,
            max_poolings=[(2, 2), (2, 2)],
            depth_mults=[2, 2],
            dense_size=[32]
        ),
        dataset_parameters=DATASET_PARAMETERS_SIGTYPE
    ),
    ModelAndDataset(
        name = "ST3",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=2,
            dropout_perc=0.2,
            max_poolings=[(2, 2), (2, 2)],
            depth_mults=[2, 2],
            dense_size=[64]
        ),
        dataset_parameters=DATASET_PARAMETERS_SIGTYPE
    ),
    ModelAndDataset(
        name = "ST4",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=48,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 2],
            dense_size=[128, 64]
        ),
        dataset_parameters=DATASET_PARAMETERS_SIGTYPE
    ),
    ModelAndDataset(
        name = "M1",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=16,
            num_conv=2,
            dropout_perc=0.2,
            max_poolings=[(2, 2), (2, 2)],
            depth_mults=[2, 2],
            dense_size=[100]
        ),
        dataset_parameters=DATASET_PARAMETERS
    ),
    ModelAndDataset(
        name = "M3",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS
    ),
    ModelAndDataset(
        name = "M3_bn",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100],
            batch_norm=True
        ),
        dataset_parameters=DATASET_PARAMETERS
    ),
    ModelAndDataset(
        name = "M3_moredrop",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.4,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS
    ),
    ModelAndDataset(
        name = "M3_nodrop",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.0,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS
    ),
    ModelAndDataset(
        name = "M3_no_exp",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_NOEXP
    ),
    ModelAndDataset(
        name = "M3_no_exp_bal",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_NOEXP_BAL
    ),
    ModelAndDataset(
        name = "M3_no_exp_300",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_NOEXP_300
    ),
    ModelAndDataset(
        name = "M3_no_exp_500",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_NOEXP_500
    ),
    ModelAndDataset(
        name = "M3_no_exp_big",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_NOEXP_BIG
    ),
    ModelAndDataset(
        name = "M3_hyperexp",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_HYPEREXP
    ),
    ModelAndDataset(
        name = "M3_hyperexp_1_5_k",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_HYPEREXP_1_5_k
    ),
    ModelAndDataset(
        name = "M3_hyperexp_best",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_HYPEREXP_BEST
    ),
    ModelAndDataset(
        name = "M3_hyperexp_best_full_trans_moredrop",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.4,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_HYPEREXP_BEST_FULL_TRANS
    ),
    ModelAndDataset(
        name = "M3_hyperexp_best_full_trans_moredrop_backswap",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.4,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_HYPEREXP_BEST_BACK_SWAP
    ),
    ModelAndDataset(
        name = "M3_hyperexp_best_full_trans_moredrop_backswap_ea",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.4,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_HYPEREXP_BEST_BACK_SWAP_EA
    ),
    ModelAndDataset(
        name = "M3_hyperexp_20_to_500",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_HYPEREXP_20_to_500
    ),
    ModelAndDataset(
        name = "M3_hyperexp_20_to_500_full_trans",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_HYPEREXP_20_to_500_FULL_TRANS
    ),
    ModelAndDataset(
        name = "M3_hyperexp_20_to_500_back_swap",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_HYPEREXP_20_to_500_BACK_SWAP
    ),
    ModelAndDataset(
        name = "M3_hyperexp_20_to_500_full_trans_more_drop",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.4,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_HYPEREXP_20_to_500_FULL_TRANS
    ),
    ModelAndDataset(
        name = "M3_hyperexp_20_to_500_back_swap_more_drop",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.4,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_HYPEREXP_20_to_500_BACK_SWAP
    ),
    ModelAndDataset(
        name = "M3_hyperexp_1_to_200_full_trans",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_HYPEREXP_1_to_200_FULL_TRANS
    ),
    # ModelAndDataset(
    #     name = "M3_hyperexp_1_to_200_back_swap",
    #     model_creation_func=get_dw_class_model_creation_func(
    #         conv_filters_num=32,
    #         num_conv=3,
    #         dropout_perc=0.2,
    #         max_poolings=[(1, 1), (2, 2), (2, 2)],
    #         depth_mults=[2, 2, 3],
    #         dense_size=[120, 100]
    #     ),
    #     dataset_parameters=DATASET_PARAMETERS_HYPEREXP_1_to_200_BACK_SWAP
    # ),
    ModelAndDataset(
        name = "M2",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=3,
            num_conv=2,
            dropout_perc=0.2,
            max_poolings=[(3, 3), (2, 2)],
            depth_mults=[1, 1],
            dense_size=[32]
        ),
        dataset_parameters=DATASET_PARAMETERS
    ),
    ModelAndDataset(
        name = "M4",
        model_creation_func=get_standard_model_creation_func(
            conv_filters_num=64,
            num_conv=2,
            dense_size=64,
            num_dense=2,
            dropout_perc=0.2
        ),
        dataset_parameters=DATASET_PARAMETERS
    ),
    ModelAndDataset(
        name = "M5",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=64,
            num_conv=2,
            dropout_perc=0.2,
            max_poolings=[(2, 2), (2, 2)],
            depth_mults=[2, 2],
            dense_size=[64, 64]
        ),
        dataset_parameters=DATASET_PARAMETERS
    ),
    ModelAndDataset(
        name = "M6",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[200, 128]
        ),
        dataset_parameters=DATASET_PARAMETERS
    ),
    ModelAndDataset(
        name = "M6_big_dset",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[200, 128]
        ),
        dataset_parameters=DATASET_PARAMETERS_BIG
    ),
    ModelAndDataset(
        name = "M7",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=64,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS
    ),
    ModelAndDataset(
        name = "M8",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=16,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS
    ),
    ModelAndDataset(
        name = "M3_big_dset",
        model_creation_func=get_dw_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.2,
            max_poolings=[(1, 1), (2, 2), (2, 2)],
            depth_mults=[2, 2, 3],
            dense_size=[120, 100]
        ),
        dataset_parameters=DATASET_PARAMETERS_BIG
    ),
    ModelAndDataset(
        name = "RN1_best_back_swap",
        model_creation_func=get_dw_resnet_class_model_creation_func(
            conv_filters_num=64,
            num_conv=3,
            dropout_perc=0.4,
            max_poolings=[(2, 2), (2, 2), (2, 2)],
            depth_mults=[2,3,4],
            dense_size=[100],
            batch_norm=True
        ),
        dataset_parameters=DATASET_PARAMETERS_HYPEREXP_BEST_BACK_SWAP
    ),
    # ModelAndDataset(
    #     name="AE1",
    #     model_creation_func=get_autoenc_model_creation_func(
    #         conv_filters_num=32,
    #         num_conv=4,
    #         dropout_perc=0.3,
    #         direct_dense_size=[400,300],
    #         intermediate_vector_size=256,
    #         inverse_dense_size=[300, 400],
    #         output_sz=4800,
    #         max_poolings=[(1,1),(1,1),(2,2),(2,2)],
    #         depth_mults=[2,2,3,4]
    #     ),
    #     dataset_parameters=DATASET_PARAMETERS_AE
    # ),
    # ModelAndDataset(
    #     name="KSAE1",
    #     model_creation_func=get_reuse_model_creation_func(
    #         ModelManager("autoenc_BEST_ext2"),
    #         cut_at_layer_name="intermediate_vector"
    #     ),
    #     dataset_parameters=DATASET_PARAMETERS
    # ),
    # ModelAndDataset(
    #     name="KSAE2",
    #     model_creation_func=get_reuse_model_creation_func(
    #         ModelManager("autoenc_BEST_ext2", always_reload_model=True),
    #         cut_at_layer_name="intermediate_vector"
    #     ),
    #     dataset_parameters=DATASET_PARAMETERS
    # ),
    # ModelAndDataset(
    #     name="KSAE3",
    #     model_creation_func=get_reuse_model_creation_func(
    #         ModelManager("autoenc_BEST_ext2", always_reload_model=True),
    #         cut_at_layer_name="intermediate_vector",
    #         add_dropouts=True
    #     ),
    #     dataset_parameters=DATASET_PARAMETERS
    # ),
    # ModelAndDataset(
    #     name="KSAE4",
    #     model_creation_func=get_reuse_model_creation_func(
    #         ModelManager("autoenc_BEST_ext2", always_reload_model=True),
    #         cut_at_layer_name="intermediate_vector",
    #         add_dropouts=True,
    #         incremental_fine_tuning=True,
    #         fine_tuning_epochs=30,
    #         drop_perc=0.4
    #     ),
    #     dataset_parameters=DATASET_PARAMETERS
    # ),
    # ModelAndDataset(
    #     name="KSAE5",
    #     model_creation_func=get_reuse_model_creation_func(
    #         ModelManager("autoenc_BEST_ext2", always_reload_model=True),
    #         cut_at_layer_name="intermediate_vector",
    #         add_dropouts=True,
    #         incremental_fine_tuning=True,
    #         fine_tuning_epochs=200,
    #         drop_perc=0.4
    #     ),
    #     dataset_parameters=DATASET_PARAMETERS
    # ),
    # ModelAndDataset(
    #     name="KSAE6",
    #     model_creation_func=get_reuse_model_creation_func(
    #         ModelManager("autoenc_BEST_ext2", always_reload_model=True),
    #         cut_at_layer_name="intermediate_vector",
    #         add_dropouts=True,
    #         incremental_fine_tuning=True,
    #         fine_tuning_epochs=2000,
    #         drop_perc=0.4
    #     ),
    #     dataset_parameters=DATASET_PARAMETERS
    # ),
])

print mmt.get_next_model()

mmt.start_tests()