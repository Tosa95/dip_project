from unittest import TestCase

from utils.fine_tuner import FineTuner
from utils.model_manager import ModelManager


class TestFineTuner(TestCase):
    def test_unfreeze_one(self):
        mm = ModelManager("autoenc_BEST_ext2")

        ft = FineTuner(mm.get_model())

        ft.sigmoid_to_relu()

        ft.remove_dropouts()
        # print ft.get_model().summary()
        ft.add_dropouts(0.2,no_drop_from_end=1)
        # print ft.get_model().summary()
        ft.freeze_all_weights()
        print ft.get_model().summary()

        while ft.unfreeze_one():
            print ft.get_model().summary()