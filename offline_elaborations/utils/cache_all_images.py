import time

from global_settings import get_settings
from service_access import get_all_images
from utils.image_caching import ImageCache

cache = ImageCache(get_settings().get_img_cache_path())

ids = get_all_images()

for i, id in enumerate(ids):

    try:
        print "%d of %d (%.2f%%)" % (i + 1, len(ids), float(i + 1) / len(ids) * 100)
        cache.get_image_by_id(id)
    except Exception as e:
        print e
        time.sleep(10)

