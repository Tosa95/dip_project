
import sys
sys.path.append("../")

import cv2
import numpy as np
from mvnc import mvncapi as mvnc
# get the first NCS device by its name.  For this program we will always open the first NCS device.
from utils.images import ImagesTransformer

devices = mvnc.enumerate_devices()
# get the first NCS device by its name.  For this program we will always open the first NCS device.
dev = mvnc.Device(devices[0])
print dev

dev.open()

# Read a compiled network graph from file (set the graph_filepath correctly for your graph file)
with open("movidius_models/graph", mode='rb') as f:
    graphFileBuff = f.read()

graph = mvnc.Graph('graph1')

#graph.allocate(dev, graphFileBuff)

# Allocate the graph on the device and create input and output Fifos
in_fifo, out_fifo = graph.allocate_with_fifos(dev, graphFileBuff)

input_image = cv2.imread("movidius_models/test_image_2.jpg")

img_resized = cv2.resize(input_image, (40, 40))
img_resized = img_resized.astype(np.float32) / 255
#img_resized = img_resized.reshape(-1, 40, 40, 3)

#img_trans = ImagesTransformer(40,40)

#input_image = img_trans.prepare_single_image_for_prediction(input_image)

#cv2.imwrite("testtt.jpg", (img_resized * 255).astype("uint8"))

# Write the input to the input_fifo buffer and queue an inference in one call
graph.queue_inference_with_fifo_elem(in_fifo, out_fifo, img_resized.astype("float32"), 'user object')

# Read the result to the output Fifo
output, userobj = out_fifo.read_elem()
print('Predicted:', output.argmax(), output, userobj)

#in_fifo.destroy()
#out_fifo.destroy()
#graph.destroy()
dev.close()
print('Finished')

