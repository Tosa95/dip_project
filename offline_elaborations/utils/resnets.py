import keras
from keras import Input, Model
from keras.layers import MaxPooling2D, Conv2D, BatchNormalization, Activation, Dropout, Flatten, Dense, SeparableConv2D


def resnet_convolutional_unit(x,filters,
                              kernel_size = (3,3), strides=(1,1),
                              pool=False,pool_size=(2,2),
                              batch_norm=False,
                              dropout=True, dropout_perc=0.30):

    res = x

    if pool:
        x = MaxPooling2D(pool_size=pool_size)(x)
        res = Conv2D(filters=filters,kernel_size=[1, 1],strides=pool_size,
                     padding="same")(res)

    if batch_norm:
        out = BatchNormalization()(x)
    else:
        out = x

    out = Activation("relu")(out)

    if dropout:
        out = Dropout(dropout_perc)(out)

    out = Conv2D(filters=filters, kernel_size=kernel_size, strides=strides, padding="same")(out)

    if batch_norm:
        out = BatchNormalization()(out)

    out = Activation("relu")(out)

    if dropout:
        out = Dropout(dropout_perc)(out)

    out = Conv2D(filters=filters, kernel_size=kernel_size, strides=strides, padding="same")(out)

    out = keras.layers.add([res,out])

    return out

def resnet_dw_convolutional_unit(x,filters,
                              kernel_size = (3,3), strides=(1,1),
                              pool=False,pool_size=(2,2),
                              batch_norm=False,
                              depth_mult=1,
                              dropout=True, dropout_perc=0.30):

    res = x

    if pool:
        x = MaxPooling2D(pool_size=pool_size)(x)
        res = SeparableConv2D(filters=filters,kernel_size=[1, 1],depth_multiplier=depth_mult,strides=pool_size,
                     padding="same")(res)

    if batch_norm:
        out = BatchNormalization()(x)
    else:
        out = x

    out = Activation("relu")(out)

    if dropout:
        out = Dropout(dropout_perc)(out)

    out = SeparableConv2D(filters=filters, kernel_size=kernel_size,depth_multiplier=depth_mult,strides=strides, padding="same")(out)

    if batch_norm:
        out = BatchNormalization()(out)

    out = Activation("relu")(out)

    if dropout:
        out = Dropout(dropout_perc)(out)

    out = SeparableConv2D(filters=filters, kernel_size=kernel_size,depth_multiplier=depth_mult,strides=strides, padding="same")(out)

    out = keras.layers.add([res,out])

    return out

def get_resnet_class_model_creation_func(
        conv_filters_num,
        num_conv,
        dropout_perc,
        dense_size,
        max_poolings=None,
        filter_sizes=None,
        num_filters=None,
        batch_norm=False
):

    if max_poolings is None:
        max_poolings = [(1, 1)] * num_conv

    if filter_sizes is None:
        filter_sizes = [(5, 5)] * num_conv

    if num_filters is None:
        num_filters = [conv_filters_num] * num_conv

    def create_model(X):

        input = Input(X.shape[1:])
        x = input

        x = Conv2D(filters=num_filters[0], kernel_size=filter_sizes[0], strides=[1, 1], padding="same")(x)

        if num_conv>0:

            for i in xrange(num_conv):
                pool = True if max_poolings[i][0] > 1 else False
                x = resnet_convolutional_unit(x,num_filters[i],filter_sizes[i],pool=pool,
                                              pool_size=max_poolings[i],batch_norm=batch_norm,dropout_perc=dropout_perc)

        if batch_norm:
            x = BatchNormalization()(x)

        x = Activation("relu")(x)
        x = Dropout(dropout_perc)(x)

        x = Flatten()(x)

        for sz in dense_size:

            x = Dense(sz)(x)
            if batch_norm:
                x = BatchNormalization()(x)
            x = Activation('relu')(x)
            x = Dropout(dropout_perc)(x)

        model = Model(input, x)

        descriptor = {
            "conv_filters_num": conv_filters_num,
            "dense": dense_size,
            "num_conv": num_conv,
            "dropout_perc": dropout_perc,
            "max_poolings": max_poolings,
            "filter_sizes": filter_sizes,
            "num_filters": num_filters,
            "batch_norm": batch_norm
        }

        return model, descriptor

    return create_model


def get_dw_resnet_class_model_creation_func(
        conv_filters_num,
        num_conv,
        dropout_perc,
        dense_size,
        max_poolings=None,
        filter_sizes=None,
        num_filters=None,
        depth_mults=None,
        batch_norm=False
):

    if max_poolings is None:
        max_poolings = [(1, 1)] * num_conv

    if filter_sizes is None:
        filter_sizes = [(5, 5)] * num_conv

    if num_filters is None:
        num_filters = [conv_filters_num] * num_conv

    if depth_mults is None:
        depth_mults = [1] * num_conv

    def create_model(X):

        input = Input(X.shape[1:])
        x = input

        x = SeparableConv2D(filters=num_filters[0], kernel_size=filter_sizes[0], depth_multiplier=depth_mults[0], strides=[1, 1], padding="same")(x)

        if num_conv>0:

            for i in xrange(num_conv):
                pool = True if max_poolings[i][0] > 1 else False
                x = resnet_dw_convolutional_unit(x,num_filters[i],filter_sizes[i],pool=pool,
                                              pool_size=max_poolings[i],batch_norm=batch_norm,dropout_perc=dropout_perc,
                                                 depth_mult=depth_mults[i])

        if batch_norm:
            x = BatchNormalization()(x)

        x = Activation("relu")(x)
        x = Dropout(dropout_perc)(x)

        x = Flatten()(x)

        for sz in dense_size:

            x = Dense(sz)(x)
            if batch_norm:
                x = BatchNormalization()(x)
            x = Activation('relu')(x)
            x = Dropout(dropout_perc)(x)

        model = Model(input, x)

        descriptor = {
            "conv_filters_num": conv_filters_num,
            "dense": dense_size,
            "num_conv": num_conv,
            "dropout_perc": dropout_perc,
            "max_poolings": max_poolings,
            "filter_sizes": filter_sizes,
            "num_filters": num_filters,
            "batch_norm": batch_norm
        }

        return model, descriptor

    return create_model
