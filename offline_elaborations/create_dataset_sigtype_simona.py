from global_settings import Settings, get_settings

from utils.dataset_creator_simona import DatasetCreator, get_full_description_dataset_name
from utils.image_caching import ImageCache
from utils.image_extenders import CompoundImageExtender, PerspectiveImageExtender, TranslationImageExtender, \
    BlurImageExtender, NoiseImageExtender, LuminosityImageExtender
from utils.images import ImagesTransformer

SETTINGS = get_settings()
#TEST_RESOL_FOLDER = SETTINGS.get_resol_test_path()
DATASET_FOLDER = SETTINGS.get_dataset_path()

DATASET_STRUCTURE = [
    ["pericolo",[[2,3]]],
    ["precedenza",[[2,4]]],
    ["divieto",[[2,5]]],
    ["obbligo",[[2,6]]],
    ["autovelox",[[2,172]]],
    ["altro",[[2,173]]],
    ["semaforo",[[2,174]]],
    ["nosig",[[1,2]]]
]

cie = CompoundImageExtender(
    [
        PerspectiveImageExtender(dx_range=(30,50)),
        PerspectiveImageExtender(dy_range=(30,50)),
        PerspectiveImageExtender(dz_range=(10,20)),
        TranslationImageExtender(x_range=(0.05,0.15)),
        TranslationImageExtender(y_range=(0.05,0.15)),
        BlurImageExtender(x_range=(0.03,0.07)),
        BlurImageExtender(y_range=(0.03,0.07)),
        NoiseImageExtender(amt_range=(5,20)),
        LuminosityImageExtender(amt_range=(0.4, 1.6))
    ],
    (3, 7)
)

dc = DatasetCreator(DATASET_FOLDER, image_cache=ImageCache(SETTINGS.get_img_cache_path()))

dc.create_dataset(
    get_full_description_dataset_name("sigtype"),
    DATASET_STRUCTURE,
    ImagesTransformer(40, 40),
    balanced=False,
    group_count_limit=3500,
    images_extender=cie
)
