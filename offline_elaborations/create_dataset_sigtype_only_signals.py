from global_settings import Settings, get_settings

from utils.dataset_creator import DatasetCreator, get_full_description_dataset_name
from utils.image_caching import ImageCache
from utils.image_extenders import CompoundImageExtender, PerspectiveImageExtender, TranslationImageExtender, \
    BlurImageExtender, NoiseImageExtender, LuminosityImageExtender
from utils.images import ImagesTransformer

SETTINGS = get_settings()
DATASETS_FOLDER = SETTINGS.get_dataset_path()

DATASET_STRUCTURE = [
    ["pericolo",[[2,3]]],
    ["precedenza",[[2,4]]],
    ["divieto",[[2,5]]],
    ["obbligo",[[2,6]]],
    ["altro",[[2,173]]],
    ["semaforo",[[2,174]]]
]

dc = DatasetCreator(DATASETS_FOLDER, image_cache=ImageCache(SETTINGS.get_img_cache_path()))

dc.create_dataset(
    get_full_description_dataset_name("sigtype_sigonly"),
    DATASET_STRUCTURE,
    ImagesTransformer(40,40),
    balanced=True,
    group_count_limit=3500,
    images_extender=None
)