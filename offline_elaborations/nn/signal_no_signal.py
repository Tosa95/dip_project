from global_settings import get_settings
from utils.dataset_manager import DatasetManager
from utils.neural_training import NeuralNetworkTrainer, get_standard_model_creation_func

SETTINGS = get_settings()

# Net parameters
CONV_FILTERS_NUM = 32
DENSE_SIZE = 32
NUM_CONV = 2
NUM_DENSE = 1
DROPOUT_PERC = 0.2

# Training parameters
PATIENCE = 150
EPOCHS = 1000

dm = DatasetManager(SETTINGS.get_dataset_path(), "signosig__bFalse_gcl15000_isx40_isy40_vp0.2_exTrue_vexFalse")
MONITORED_QUANTITY = 'val_loss'
CHECKPOINT_NAME = "signosig_ep{epoch}_vl{val_loss}_va{val_acc}"
MODELS_FOLDER = SETTINGS.get_models_path()

nnt = NeuralNetworkTrainer(
    CHECKPOINT_NAME,
    MODELS_FOLDER,
    dm,
    get_standard_model_creation_func(
        CONV_FILTERS_NUM,
        NUM_CONV,
        DENSE_SIZE,
        NUM_DENSE,
        DROPOUT_PERC
    ),
    patience=30
)

nnt.train()