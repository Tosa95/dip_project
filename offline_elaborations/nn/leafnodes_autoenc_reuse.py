from service_access import get_label_type_tree
from utils.dataset_creator import get_leafs, leafs_to_dataset_struct
from utils.image_extenders import CompoundImageExtender, PerspectiveImageExtender, TranslationImageExtender, \
    BlurImageExtender, NoiseImageExtender, LuminosityImageExtender, ColourTemperatureImageExtender, \
    SaturationImageExtender
from utils.images import ImagesTransformer
from utils.model_manager import ModelManager
from utils.multiple_models_trainer import MultipleModelsTrainer, ModelAndDataset
from utils.neural_training import get_dw_class_model_creation_func, get_standard_model_creation_func, \
    get_reuse_model_creation_func

tree = get_label_type_tree()
leafs = []

get_leafs(tree,leafs)

DATASET_STRUCTURE = leafs_to_dataset_struct(leafs, min_size=10)

DATASET_PARAMETERS = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 100,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6))
        ],
        (3,7)
    ),
    "expand_validation": True
}

DATASET_LITTLEST = {"name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 10,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6))
        ],
        (3,7)
    ),
    "expand_validation": True,
    "separate_validation": True,
    "validation_perc": 10
}

DATASET_PARAMETERS_HYPEREXP = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 100,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6))
        ],
        (3,7)
    ),
    "expand_validation": True,
    "hyper_expansion_size": 500
}

DATASET_PARAMETERS_HYPEREXP_1_5_k = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 100,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6))
        ],
        (3,7)
    ),
    "expand_validation": True,
    "hyper_expansion_size": 1500
}

DATASET_PARAMETERS_HYPEREXP_BEST = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 1500,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6))
        ],
        (3,7)
    ),
    "expand_validation": True,
    "hyper_expansion_size": 1500
}

DATASET_PARAMETERS_HYPEREXP_BEST_FULL_TRANS = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 1500,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6)),
            ColourTemperatureImageExtender(b_range=(0.9,1.1), r_range=(0.9,1.1)),
            SaturationImageExtender(amt_range=(0.5,1.5))
        ],
        (5,9)
    ),
    "expand_validation": True,
    "hyper_expansion_size": 1500
}


DATASET_PARAMETERS_HYPEREXP_20_to_500 = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 20,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6))
        ],
        (3,7)
    ),
    "expand_validation": True,
    "hyper_expansion_size": 500,
    "separate_validation": True,
    "validation_perc": 10
}

DATASET_PARAMETERS_HYPEREXP_20_to_500_FULL_TRANS = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 20,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6)),
            ColourTemperatureImageExtender(b_range=(0.9,1.1), r_range=(0.9,1.1)),
            SaturationImageExtender(amt_range=(0.5,1.5))
        ],
        (5,9)
    ),
    "expand_validation": True,
    "hyper_expansion_size": 500,
    "separate_validation": True,
    "validation_perc": 10
}

DATASET_PARAMETERS_HYPEREXP_1_to_200_FULL_TRANS = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 1,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6)),
            ColourTemperatureImageExtender(b_range=(0.9,1.1), r_range=(0.9,1.1)),
            SaturationImageExtender(amt_range=(0.5,1.5))
        ],
        (5,9)
    ),
    "expand_validation": True,
    "hyper_expansion_size": 200,
    "separate_validation": True,
    "validation_perc": 200
}


DATASET_PARAMETERS_NOEXP = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 100,
    "expand_validation": True
}

DATASET_PARAMETERS_NOEXP_BAL = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": True,
    "group_count_limit": 0,
    "expand_validation": True
}


DATASET_PARAMETERS_NOEXP_300 = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 300,
    "expand_validation": True
}

DATASET_PARAMETERS_NOEXP_500 = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 500,
    "expand_validation": True
}

DATASET_PARAMETERS_NOEXP_BIG = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 1500,
    "expand_validation": True
}

DATASET_PARAMETERS_BIG = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 500,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6))
        ],
        (3,7)
    ),
    "expand_validation": True
}


mmt = MultipleModelsTrainer("leaf_autoenc", patience=5, tests_per_model=20, models=[
    ModelAndDataset(
        name="KSAE3",
        model_creation_func=get_reuse_model_creation_func(
            ModelManager("autoenc_BEST_ext2", always_reload_model=True),
            cut_at_layer_name="intermediate_vector",
            add_dropouts=True,
            sigmoid_to_relu=True
        ),
        dataset_parameters=DATASET_PARAMETERS
    ),
])

print mmt.get_next_model()

mmt.start_tests()