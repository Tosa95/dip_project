import pprint

from keras import Model
from keras.layers import Dense, Activation, Dropout

from service_access import get_label_type_tree
from utils.dataset_creator import get_leafs, leafs_to_dataset_struct
from utils.image_extenders import CompoundImageExtender, PerspectiveImageExtender, TranslationImageExtender, \
    BlurImageExtender, NoiseImageExtender, LuminosityImageExtender, ColourTemperatureImageExtender, \
    SaturationImageExtender
from utils.images import ImagesTransformer
from utils.model_manager import ModelManager
from utils.multiple_models_trainer import MultipleModelsTrainer, ModelAndDataset
from utils.neural_training import get_dw_class_model_creation_func, get_standard_model_creation_func, \
    get_reuse_model_creation_func, get_autoenc_model_creation_func
from utils.resnets import get_resnet_class_model_creation_func, get_dw_resnet_class_model_creation_func

tree = get_label_type_tree()
leafs = []

get_leafs(tree,leafs)

DATASET_STRUCTURE = leafs_to_dataset_struct(leafs, min_size=10)

DATASET_STRUCTURE_AE = [grp for grp in DATASET_STRUCTURE if grp[0] != "no"]

DATASET_PARAMETERS = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 500,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30, 50)),
            PerspectiveImageExtender(dy_range=(30, 50)),
            PerspectiveImageExtender(dz_range=(10, 20)),
            TranslationImageExtender(x_range=(0.05, 0.07)),
            TranslationImageExtender(y_range=(0.05, 0.07)),
            BlurImageExtender(x_range=(0.03, 0.07)),
            BlurImageExtender(y_range=(0.03, 0.07)),
            NoiseImageExtender(amt_range=(5, 20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6)),
            ColourTemperatureImageExtender(b_range=(0.9, 1.1), r_range=(0.9, 1.1)),
            SaturationImageExtender(amt_range=(0.5, 1.5))
        ],
        (5, 9)
    ),
    "expand_validation": True,
    "hyper_expansion_size": 500
}

DATASET_LITTLEST = {"name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 10,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6))
        ],
        (3,7)
    ),
    "expand_validation": True,
    "separate_validation": True,
    "validation_perc": 10
}

DATASET_PARAMETERS_HYPEREXP_BEST_FULL_TRANS = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 1200,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.05,0.07)),
            TranslationImageExtender(y_range=(0.05,0.07)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6)),
            ColourTemperatureImageExtender(b_range=(0.9,1.1), r_range=(0.9,1.1)),
            SaturationImageExtender(amt_range=(0.5,1.5))
        ],
        (5,9)
    ),
    "expand_validation": True,
    "hyper_expansion_size": 1200
}

mmt = MultipleModelsTrainer("resnets", patience=10, tests_per_model=20, models=[
    ModelAndDataset(
        name = "RN0",
        model_creation_func=get_dw_resnet_class_model_creation_func(
            conv_filters_num=32,
            num_conv=3,
            dropout_perc=0.4,
            max_poolings=[(1, 1)] + [(2, 2)]*2,
            depth_mults=[2,2,3],
            dense_size=[100]
        ),
        dataset_parameters=DATASET_PARAMETERS
    ),
    ModelAndDataset(
        name = "RN1",
        model_creation_func=get_dw_resnet_class_model_creation_func(
            conv_filters_num=64,
            num_conv=3,
            dropout_perc=0.4,
            max_poolings=[(2, 2), (2, 2), (2, 2)],
            depth_mults=[2,3,4],
            dense_size=[100],
            batch_norm=True
        ),
        dataset_parameters=DATASET_PARAMETERS
    ),
    ModelAndDataset(
        name = "RN1_best",
        model_creation_func=get_dw_resnet_class_model_creation_func(
            conv_filters_num=64,
            num_conv=3,
            dropout_perc=0.4,
            max_poolings=[(2, 2), (2, 2), (2, 2)],
            depth_mults=[2,3,4],
            dense_size=[100],
            batch_norm=True
        ),
        dataset_parameters=DATASET_PARAMETERS_HYPEREXP_BEST_FULL_TRANS
    ),
])





TRAIN = True

if TRAIN:
    print mmt.get_next_model()

    mmt.start_tests()
else:
    mmt = MultipleModelsTrainer("resnets")

    print "val_acc"
    pprint.pprint(mmt.analyze("val_acc"))

    print "val_loss"
    pprint.pprint(mmt.analyze("val_loss"))

    mmt.analyze_to_dataframe(["val_acc", "val_loss"])