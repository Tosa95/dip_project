import json
import os

import numpy as np

from global_settings import get_settings
from service_access import get_label_type_tree
from utils.dataset_creator import DatasetCreator, get_full_description_dataset_name, get_leafs, leafs_to_dataset_struct
from utils.image_caching import ImageCache
from utils.image_extenders import ALL_IMAGES_EXTENDERS
from utils.images import ImagesTransformer
from utils.neural_training import NeuralNetworkTrainer, get_standard_model_creation_func, get_reuse_model_creation_func

SETTINGS = get_settings()
MIN_IMAGES_PER_LABEL = 20
GROUP_COUNT_LIMIT = 200
DATA_FILENAME = "test_EXP.json"

# Net parameters
CONV_FILTERS_NUM = 32
DENSE_SIZE = 32
NUM_CONV = 2
NUM_DENSE = 1
DROPOUT_PERC = 0.2

# Training parameters
PATIENCE = 5

CHECKPOINT_NAME_EXP = "leafnodes_EXP_ep{epoch}_vl{val_loss}_va{val_acc}"
CHECKPOINT_NAME_BAL = "leafnodes_BAL_ep{epoch}_vl{val_loss}_va{val_acc}"
CHECKPOINT_NAME_NBAL = "leafnodes_NBAL_ep{epoch}_vl{val_loss}_va{val_acc}"
MODELS_FOLDER = SETTINGS.get_models_path()

tree = get_label_type_tree()
leafs = []
get_leafs(tree,leafs)

DATASET_STRUCTURE = leafs_to_dataset_struct(leafs, MIN_IMAGES_PER_LABEL)

dc = DatasetCreator(SETTINGS.get_dataset_path(), image_cache=ImageCache(SETTINGS.get_img_cache_path()))

print "Creating expanded dataset"

dm_expanded = dc.create_dataset(
    get_full_description_dataset_name("leafnodes"),
    DATASET_STRUCTURE,
    ImagesTransformer(40,40),
    balanced=False,
    group_count_limit=GROUP_COUNT_LIMIT,
    images_extender=ALL_IMAGES_EXTENDERS
)

print "Creating balanced dataset"

dm_balanced = dc.create_dataset(
    get_full_description_dataset_name("leafnodes"),
    DATASET_STRUCTURE,
    ImagesTransformer(40,40),
    balanced=True
)

print "Creating unbalaced dataset"

dm_non_balanced = dc.create_dataset(
    get_full_description_dataset_name("leafnodes"),
    DATASET_STRUCTURE,
    ImagesTransformer(40,40),
    balanced=False,
    group_count_limit=GROUP_COUNT_LIMIT
)

ROUNDS = 100

tests = [
        {
            "cname": CHECKPOINT_NAME_EXP,
            "dset": dm_expanded,
            "dname": "EXP"
        },
        {
            "cname": CHECKPOINT_NAME_BAL,
            "dset": dm_balanced,
            "dname": "BAL"
        },
        {
            "cname": CHECKPOINT_NAME_NBAL,
            "dset": dm_non_balanced,
            "dname": "NBAL"
        }
    ]

def do_cmp():

    try:
        with open(os.path.join(SETTINGS.get_dataset_path(), DATA_FILENAME), "rt") as input:
            res = json.load(input)

        print res
    except:
        res = []

    for i in xrange(ROUNDS):

        print "Round %d" % i

        round_data = {}

        for t in tests:

            print "TEST %s" % t["dname"]

            nnt = NeuralNetworkTrainer(
                t["cname"],
                MODELS_FOLDER,
                t["dset"],
                get_standard_model_creation_func(
                    CONV_FILTERS_NUM,
                    NUM_CONV,
                    DENSE_SIZE,
                    NUM_DENSE,
                    DROPOUT_PERC
                ),
                patience=PATIENCE
            )

            mm = nnt.train()

            round_data[t["dname"]] = mm.get_validation_data()

        res.append(round_data)

        with open(os.path.join(SETTINGS.get_dataset_path(), DATA_FILENAME),"wt") as output:
            json.dump(res, output, indent=4)


def do_avg():

    with open(os.path.join(SETTINGS.get_dataset_path(), DATA_FILENAME), "rt") as input:
        data = json.load(input)

    print "Num samples: %d" % len(data)

    values = ["val_loss", "val_acc"]

    for t in tests:

        t_res = t["dname"] + ": "

        for v in values:
            mean = np.mean(np.array([d[t["dname"]][v] for d in data]))
            std = np.std(np.array([d[t["dname"]][v] for d in data]))
            t_res += "   %s --> %.3f (+-%.3f)   "%(v, mean, std)

        print t_res

#do_cmp()
do_avg()