import numpy as np
import cv2
import datetime

from global_settings import get_settings
from utils.image_caching import ImageCache
from utils.model_manager import ModelManager
from utils.waterfall_model_manager import WaterfallModelManager
from utils.spliced_waterfall_model_manager import SplicedWaterfallModelManager
from utils.dataset_manager_simona import DatasetManager
from utils.images import DoNotTransformImagesTransformer
from utils.image_extenders import ALL_IMAGES_EXTENDERS
from utils.dataset_creator import DatasetCreator, get_full_description_dataset_name
from service_access import get_label_info
from utils.saving_methods import MemoryDatasetPersister

SETTINGS = get_settings()
DATASETS_FOLDER = SETTINGS.get_dataset_path()
IMAGES_PER_LABEL_AFTER_EXPANSION = 20000

DATASET_STRUCTURE = [
    ["pericolo",[[2,3]]],
    ["precedenza",[[2,4]]],
    ["divieto",[[2,5]]],
    ["obbligo",[[2,6]]],
    ["autovelox",[[2,172]]],
    ["altro",[[2,173]]],
    ["semaforo",[[2,174]]],
    ["nosig",[[1,2]]]
]

URL_ICON = "http://multisalakingv2.ddns.net:8081/segnali/images/icons/"
FILE_NAME = get_settings().get_html_conf_matrix_path() + "/confusion_matrix"
SINGLE_MODEL_FILE_NAME = FILE_NAME + "_%s_DATASET_%s.html"


def get_html_image(img_name, label):
    if img_name is not None:
        html_img = "<th><img src='" + URL_ICON + str(img_name) + "' style='width:70px; height:70px;'></th>"
    else:
        html_img = "<th style='height:70px; width:70px;'>" + label + "</th>"

    return html_img


def get_cell_string(confusion_m, k, j, perc, num_tot_col):
    if perc:
        val = confusion_m[k][j]
        p = val/num_tot_col*100
        r = "%.2f" % p
        return r + "&#37;"
    else:
        return str(confusion_m[k][j])


def create_confusion_matrix(mm_list, dataset_m, perc=False, red_threshold=0.1, filename=None):

    colors = ["#00004d", "#0047b3", "#193366", "#000066", "#132639", "#400080", "#1a0033"]
    red_colors = ["darkred", "red", "#b30000", "#4d0000", "#ff0000", "#b3003b", "#660022"]

    dataset_descriptor = dataset_m.get_descriptor()
    ds = dm.get_dataset()

    size = len(dataset_descriptor['structure'])
    n_models = len(mm_list)

    conf_matrix_list = []

    for n in range(n_models):
        conf_matrix_list.append(np.zeros((size, size), dtype=int))
    positions = {}
    labels = []
    accuracies = []
    acc_without_nosig = []

    for i, lbl_list in enumerate(dataset_descriptor['structure']):
        lbl = lbl_list[0]
        tid = lbl_list[1][0][0]
        vid = lbl_list[1][0][1]
        label_info = get_label_info(tid, vid)
        image_name = label_info['image']
        labels.append((lbl, tid, vid, image_name))
        positions[lbl] = i

    for i, img in enumerate(ds['X']):
        if img is not None:
            index = np.argmax(ds['y'][i])
            lbl = dataset_descriptor["index_to_labels"][index]
            try:
                for n, mm in enumerate(mm_list):
                    predicted_lbl, p = mm.get_prediction_label_perc(img)
                    print i, lbl, positions[lbl], predicted_lbl, positions[predicted_lbl]
                    conf_matrix_list[n][positions[predicted_lbl]][positions[lbl]] += 1
            except Exception as e:
                print e

    for n, mm in enumerate(mm_list):
        confusion_m = conf_matrix_list[n]
        cm_without_nosig = confusion_m[:, :-1]
        accuracy = confusion_m.diagonal().sum()/float(confusion_m.sum())
        a_without_nosig = cm_without_nosig.diagonal().sum()/float(cm_without_nosig.sum())

        conf_matrix_list.append(confusion_m)
        accuracies.append(accuracy)
        acc_without_nosig.append(a_without_nosig)

        print "\nModel name:", mm.get_model_name()
        print "\n", confusion_m
        print "\nAccuracy:  ", accuracy
        print "\nAccuracy without nosign column:  ", a_without_nosig

    html = """
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Confusion matrix</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            <style>
            tbody td:nth-of-type(even){background:#f9f9f9;}
	        tbody td:nth-of-type(odd){background:#e6f7ff;}
	        .rotate {
              text-align: center;
              white-space: nowrap;
              vertical-align: middle;
              width: 3em;
            }
            .rotate div {
                 -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
                   -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
              -webkit-transform: rotate(-90.0deg);  /* Saf3.1+, Chrome */
                         filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                     -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
                     margin-left: -5em;
                     margin-right: -5em;
            }
            </style>
        </head>
        <body>
            <div style="text-align:center; padding-left:30px; padding-right:30px;">
                <h1 style="margin-top:25px; margin-bottom:15px;">Confusion matrix</h1> """

    for n, mm in enumerate(mm_list):
        html += """
        <div style="margin-bottom:7px;">
            %d) &emsp; Model name: <span style='color:%s'>""" % (n+1, colors[n]) + mm.get_model_name() + \
                "</span> <br> Accuracy: <b style='font-size:20px;'>%.3f" % accuracies[n] +\
                "</b> &emsp;Accuracy without nosign column: <b style='font-size:20px;'>%.3f" % acc_without_nosig[n] + """</b>
        </div> """

    html += """
                <div style="margin-bottom:20px; margin-top:15px;">
                    Dataset: """ + dm.get_dataset_name() + """
                </div>
                <div>
                    <table border=1 style="align:center; margin-left:auto; margin-right:auto; margin-bottom:30px;">
                         <col>
                         <colgroup span='""" + str(size) + """'></colgroup>
                         <tr>
                             <th rowspan="2" colspan="2" border=0></th>
                             <th colspan='""" + str(size) + """' scope="colgroup" style='padding-top:10px; 
                             padding-bottom:10px; background-color:#007acc; color:white;'>ACTUAL CLASS</th>
                         </tr>
                         <tr>
                         """

    for l in labels:
        html += get_html_image(l[3], l[0])

    html += "</tr>"

    for k, l in enumerate(labels):
        html += "<tr>"
        if k == 0:
            html += "<th rowspan='" + str(size) + "' class='rotate' style='background-color:#007acc; color:white;'>" \
                                                  "<div>PREDICTED CLASS</div></th>"
        html += get_html_image(l[3], l[0])
        for j in range(size):
            num_tot_col = float(conf_matrix_list[0][:, j].sum())
            html += "<td>"
            if j == k:
                for n in range(n_models):
                    html += "<b><div style='color:%s'>" % colors[n] + get_cell_string(conf_matrix_list[n], k, j,
                                                                                      perc, num_tot_col) + "</div></b>"
            else:
                for n in range(n_models):
                    confusion_m = conf_matrix_list[n]
                    val = confusion_m[k][j]
                    if val > num_tot_col * red_threshold:
                        html += "<div style='color:%s;'>" % red_colors[n] + "<b>" + get_cell_string(confusion_m, k, j,
                                                                                                    perc, num_tot_col) + "</b></div>"
                    else:
                        html += "<div style='color:%s'>" % colors[n] + get_cell_string(confusion_m, k, j, perc, num_tot_col) + "</div>"
        html += "</td>"
        html += "</tr>"

    html += """
                    </table>
                </div>
            </div>
        </body>
        </html>
    """

    if filename is None:
        if n_models == 1:
            file_name = SINGLE_MODEL_FILE_NAME % (mm_list[0].get_model_name(), dm.get_dataset_name())
        else:
            file_name = FILE_NAME + "_comp_" + datetime.datetime.now().strftime("%Y-%m-%d_%H:%M") + "_perc%s" % str(perc)
    else:
        file_name = FILE_NAME + filename + "_perc%s" % str(perc)

    with open(file_name + ".html", "w") as html_file:
        html_file.write(html)


if __name__ == '__main__':

    mm_list = []

    model_name = "sigtype_ep22_vl0.426999311128_va0.884418901736"
    mod_manager = ModelManager(model_name, get_settings().get_models_path(),
                               image_cache=ImageCache(get_settings().get_img_cache_path()))

    sns_model_name = "signosig_ep36_vl0.22417925785_va0.922087205487"
    st_w_nosign_model_name = "sigtype_ep22_vl0.426999311128_va0.884418901736"
    st_model_name = "sigtype_without_nosig_ep22_vl0.330800220748_va0.913043478261"

    sns_mm = ModelManager(sns_model_name, SETTINGS.get_models_path(),
                          image_cache=ImageCache(SETTINGS.get_img_cache_path()))
    st_w_ns_mm = ModelManager(st_w_nosign_model_name, SETTINGS.get_models_path(),
                              image_cache=ImageCache(SETTINGS.get_img_cache_path()))
    st_mm = ModelManager(st_model_name, SETTINGS.get_models_path(),
                         image_cache=ImageCache(SETTINGS.get_img_cache_path()))

    wmm_05 = WaterfallModelManager(sns_mm, st_mm, threshold=0.5)
    swmm_06509 = SplicedWaterfallModelManager(sns_mm, st_w_ns_mm, st_mm, low_threshold=0.65, high_threshold=0.9)
    swmm_09095 = SplicedWaterfallModelManager(sns_mm, st_w_ns_mm, st_mm, low_threshold=0.9, high_threshold=0.95)
    swmm_070908 = SplicedWaterfallModelManager(sns_mm, st_w_ns_mm, st_mm, low_threshold=0.7, high_threshold=0.9, sig_threshold=0.8)

    #mm_list.append(mod_manager)
    mm_list.append(wmm_05)
    mm_list.append(swmm_06509)
    #mm_list.append(swmm_09095)
    mm_list.append(swmm_070908)

    dc = DatasetCreator(DATASETS_FOLDER, image_cache=ImageCache(SETTINGS.get_img_cache_path()),
                        dataset_persister = MemoryDatasetPersister())

    dm = dc.create_dataset(
            get_full_description_dataset_name("sigtype"),
            DATASET_STRUCTURE,
            DoNotTransformImagesTransformer(),
            balanced=False,
            group_count_limit=IMAGES_PER_LABEL_AFTER_EXPANSION,
            validation_perc=0.0
            )

    filename = "_waterfall_wmm05_swmm06509_swmm070908_comp"
    create_confusion_matrix(mm_list, dm, perc=False, filename=filename)
