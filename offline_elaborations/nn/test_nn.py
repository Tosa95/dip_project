from global_settings import get_settings
from utils.image_caching import ImageCache
from utils.model_manager import ModelManager

mm = ModelManager("leafnodes_KRM_ep11_vl0.835633068498_va0.800550206409", get_settings().get_models_path(),
                  image_cache=ImageCache(get_settings().get_img_cache_path()))

for i in xrange(100):
    mm.simple_test(5,9)

# mm.simple_test_from_urls(5,9,[
#     "https://images.homedepot-static.com/productImages/ca3263e5-d194-4d4f-ba93-ce96b729116f/svn/brady-stock-signs-75205-64_1000.jpg",
#     "https://cdn.playbuzz.com/cdn/da764bdb-29ec-4832-8f06-623386291276/cd51fbfb-bdc5-4d33-817b-7264a0b1712c.jpg",
#     "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6n5lANG53sWvPrc_FIYuV_8thpXwE2ev5g2_i5KpGlloufBmZjw",
#     "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSZYMSjVj890iJfdWhG_FIxcdiO8nFxXF7At2-Q_hJ27awUFRJT",
#     "https://piximus.net/media/4918/bizarre-real-life-road-signs-2.jpg",
#     "https://www.thelocal.se/userdata/images/article/46f19373eeda62f101829758a0826f02500db1c1237c89148dbd260739c12c74.jpg",
#     "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRqJp7LrfriZv4cMEb11Up4xZ1Pgxm668u5UE5DXuP6kRtPUwM-",
#     "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR0tBQrIsfuZ0RblvvdpmoiziXftIsC86bZLPsee8FxmsM_Z372",
#     "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQMUBY2qCuMziyrkM03K_3ivxY0CWBPAWLJlGOYfugBTRlxAsBlgQ",
#     "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRLh3f-MpbDOx94aVdS4uRS8eIIrI2ZXfHhXyKo_MAp7KySG80J",
#     "https://www.catster.com/wp-content/uploads/2017/08/A-fluffy-cat-looking-funny-surprised-or-concerned.jpg",
#     "https://www.comune.casina.re.it/wp-content/uploads/2018/10/carraia-in-poncema-300x199.jpg",
#     "http://www.italia.it/fileadmin/src/img/cluster_gallery/Citta_d_arte_Milano/Duomo-Milano.jpg",
#     "http://www.romainmaschera.it/files/2017/12/15/01466-1-maschera-unicorno.jpg",
#     "http://www.asciify.net/ascii/thumb/4454/asdasd.png",
#     "https://o.aolcdn.com/images/dims3/GLOB/crop/3598x1803+0+84/resize/630x315!/format/jpg/quality/85/http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F4c78eb855f9cbd3973cbe7fa64440bd7%2F206530050%2FRTX6AVH8.jpeg",
#     "https://s3-eu-central-1.amazonaws.com/cagliaripad-s3/wp-content/uploads/2019/01/12112214/310x0_1523470685356.LP_7685305.jpg",
#     "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTNykkMk3dnyaJv-8EZz8a4AxCRZpkMUHIbXotUku8bTUIcnBRl"
#
# ])
