from global_settings import get_settings
from utils.dataset_manager import DatasetManager
from utils.model_manager import ModelManager
from utils.neural_training import NeuralNetworkTrainer, get_standard_model_creation_func, \
    get_no_drop_model_creation_func, get_autoenc_model_creation_func
from utils.repeated_trainer import RepeatedTrainer

SETTINGS = get_settings()

# Training parameters
PATIENCE = 15
EPOCHS = 1000

dm = DatasetManager(SETTINGS.get_dataset_path(), "autoencs__bFalse_gcl700_isx40_isy40_vp0.2_exTrue_vexFalse")
MONITORED_QUANTITY = 'val_loss'
CHECKPOINT_NAME = "autoenc_ext2_ep{epoch}_vl{val_loss}_va{val_acc}"
MODELS_FOLDER = SETTINGS.get_models_path()

HOW_MANY_RETRAINS = 100
BEST_NAME = "autoenc_BEST_ext2"

print dm.get_dataset()["y"][0]

nnt = NeuralNetworkTrainer(
    CHECKPOINT_NAME,
    MODELS_FOLDER,
    dm,
    get_autoenc_model_creation_func(
        conv_filters_num=32,
        num_conv=4,
        dropout_perc=0.0,
        direct_dense_size=[400,300],
        intermediate_vector_size=256,
        inverse_dense_size=[300, 400],
        output_sz=dm.get_images_transformer().get_img_tot_pixels(),
        max_poolings=[(1,1),(1,1),(2,2),(2,2)],
        depth_mults=[2,2,3,4]
    ),
    patience=PATIENCE,
    add_last_layer=False,
    loss_function="mean_squared_error"
)

rt = RepeatedTrainer(nnt, how_many_retrains=HOW_MANY_RETRAINS, best_model_filename_template=BEST_NAME)

starting_model = None
try:
    starting_model = ModelManager(BEST_NAME)
    starting_model.get_descriptor()
except:
    starting_model = None

mm = rt.train(starting_model=starting_model)