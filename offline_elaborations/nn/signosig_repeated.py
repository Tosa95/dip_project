from pprint import pprint

from global_settings import get_settings
from utils.dataset_manager import DatasetManager
from utils.model_manager import ModelManager
from utils.neural_training import NeuralNetworkTrainer, get_standard_model_creation_func
from utils.repeated_trainer import RepeatedTrainer

SETTINGS = get_settings()

# Num of retrains
HOW_MANY_RETRAINS = 150
BEST_NAME = "signosig_4040_BEST2"

# Net parameters
CONV_FILTERS_NUM = 64
DENSE_SIZE = 64
NUM_CONV = 2
NUM_DENSE = 2
DROPOUT_PERC = 0.2

# Training parameters
PATIENCE = 10
EPOCHS = 1000

dm = DatasetManager(SETTINGS.get_dataset_path(), "signosig__bFalse_gcl15000_isx40_isy40_vp0.2_exTrue_vexFalse")
MONITORED_QUANTITY = 'val_loss'
CHECKPOINT_NAME = "signosig_ep{epoch}_vl{val_loss}_va{val_acc}"
MODELS_FOLDER = SETTINGS.get_models_path()


nnt = NeuralNetworkTrainer(
    CHECKPOINT_NAME,
    MODELS_FOLDER,
    dm,
    get_standard_model_creation_func(
        CONV_FILTERS_NUM,
        NUM_CONV,
        DENSE_SIZE,
        NUM_DENSE,
        DROPOUT_PERC
    ),
    patience=PATIENCE
)

rt = RepeatedTrainer(nnt, how_many_retrains=HOW_MANY_RETRAINS, best_model_filename_template=BEST_NAME)

starting_model = None
try:
    starting_model = ModelManager(BEST_NAME)
    starting_model.get_descriptor()
except:
    starting_model = None

mm = rt.train(starting_model=starting_model)

# pprint(mm.get_descriptor())
# pprint(mm.get_validation_data())