import random
from copy import deepcopy

import cv2

from service_access import get_img_by_id, url_to_image, get_dataset
from utils.dataset_creator import get_random_dataset_flatten
from utils.images import put_text_on_image, LastSeen
from utils.model_manager import ModelManager
import numpy as np

from utils.video_manager import VideoManager

mm = ModelManager("autoenc_BEST_ext2")

n_img = 10


# for i in range(20):
#     ids = get_random_dataset_flatten(mm.get_dataset_structure(), n_img)
#     # ids.append("https://partylandia.it/8952-thickbox_default/cialda-tonda-in-ostia-unicorno-decorazioni-torta-compleanno-dischi-commestibile.jpg")
#
#     ls = LastSeen(2,n_img)
#
#     # ids = [5579,5006,13048,28201,29421,19134,28773,31207,41187,36818,13560,
#     #        "https://partylandia.it/8952-thickbox_default/cialda-tonda-in-ostia-unicorno-decorazioni-torta-compleanno-dischi-commestibile.jpg"]
#
#     for id in ids:
#
#         if type(id) == int:
#             img = get_img_by_id(id)
#         else:
#             img = url_to_image(id)
#
#         pred = mm.get_prediction(img)
#
#         # print mm.get_autoenc_predicted_vector(img)
#
#         img_pred = (np.reshape(pred,(40,40,3))*255).astype(np.uint8)
#
#         img_pred = cv2.resize(img_pred,(img.shape[0],img.shape[0]))
#
#         ls.add_img(img)
#         ls.add_img(img_pred)
#
#     cv2.imshow("orig vs predicted", ls.get_grid())
#     cv2.waitKey()


img = get_img_by_id(28773)
# img = get_img_by_id(13048)

vector = mm.get_autoenc_predicted_vector(img)

img2 = get_img_by_id(16138)

vector2 = mm.get_autoenc_predicted_vector(img2)

SCALE = 2

print vector.shape

vals = np.linspace(0,1,200)

vm = VideoManager("output")

imgshape = (img.shape[1]*SCALE, img.shape[0]*SCALE)

for el in range(max(vector.shape)):
    vec2 = deepcopy(vector)
    vec2[0][el] = 0
    print el
    print "+"
    for v in vals:
        vec2[0][el] = vector[0][el] + v

        if vec2[0][el] <= 1:

            img_pred = mm.get_autoenc_image_from_vector(vec2)

            img_pred = cv2.resize(img_pred, imgshape)

            cv2.imshow("orig", img)
            img_pred = put_text_on_image(img_pred, "%d, %.3f" % (el,vec2[0][el]))

            vm.add_frame(img_pred)

            cv2.imshow("pred", img_pred)
            cv2.waitKey(10)

vm.close()


#
# # Define the codec and create VideoWriter object
# imgshape = (img.shape[0]*SCALE, img.shape[0]*SCALE)
# fourcc = cv2.cv.CV_FOURCC(*'XVID')
# out = cv2.VideoWriter('output.avi',fourcc, 60.0, imgshape)
#
# for el in range(max(vector.shape)):
# #for el in range(2):
#     vec2 = deepcopy(vector)
#     vec2[0][el] = 0
#     print el
#     print "+"
#     for v in vals:
#         vec2[0][el] = vector[0][el] + v
#
#         if vec2[0][el] <= 1:
#
#             img_pred = mm.get_autoenc_image_from_vector(vec2)
#
#             img_pred = cv2.resize(img_pred, imgshape)
#
#             out.write(img_pred)
#
#             cv2.imshow("orig", img)
#             img_pred = put_text_on_image(img_pred, "%d, %.3f" % (el,vec2[0][el]))
#
#             cv2.imshow("pred", img_pred)
#             cv2.waitKey(10)
#     # print "-"
#     # for v in vals:
#     #     vec2[0][el] = vector[0][el] - v
#     #
#     #     if vec2[0][el]>=0:
#     #
#     #         img_pred = mm.get_autoenc_image_from_vector(vec2)
#     #
#     #         img_pred = cv2.resize(img_pred, imgshape)
#     #
#     #         out.write(img_pred)
#     #
#     #         cv2.imshow("orig", img)
#     #         img_pred = put_text_on_image(img_pred, "%d, %.3f" % (el,vec2[0][el]))
#     #
#     #         cv2.imshow("pred", img_pred)
#     #         cv2.waitKey(10)
#
# out.release()




imgshape = (img.shape[0]*SCALE, img.shape[0]*SCALE)

vals = np.linspace(0,1,30)

# for el in range(max(vector.shape)):
#     vec2 = deepcopy(vector)
#     vec2[0][el] = 0
#     print el
#     print "+"
#     ls = LastSeen(4, 8,200)
#
#     img_orig = put_text_on_image(img, "original")
#     ls.add_img(img_orig)
#
#     for v in vals:
#
#         vec2[0][el] = vector[0][el] + v
#
#         if vec2[0][el] <= 1:
#
#             img_pred = deepcopy(mm.get_autoenc_image_from_vector(vec2))
#             img_pred = cv2.resize(img_pred, imgshape)
#             img_pred = put_text_on_image(img_pred, "%d, %.3f" % (el, vec2[0][el]))
#
#             ls.add_img(img_pred)
#
#     cv2.imshow("orig", ls.get_grid())
#     cv2.waitKey(0)



imgshape = (img.shape[0]*SCALE, img.shape[0]*SCALE)

vals = np.linspace(-0.1,0.1,30)
nf_range = (2,2)

def compute_direction(nf, dimensionality=max(vector.shape)):
    indices = random.sample(range(dimensionality),nf)

    res = np.zeros(dimensionality)

    for i in indices:
        res[i] = random.uniform(0,1)

    res = res/(np.sqrt(np.sum(np.power(res,2))))

    return np.reshape(res, vector.shape)



for i in range(100):

    nf=random.randint(nf_range[0],nf_range[1])
    direction = compute_direction(nf)

    vec2 = deepcopy(vector)
    print i
    print "+"
    ls = LastSeen(4, 8,200)

    img_orig = put_text_on_image(img, "original")
    ls.add_img(img_orig)

    for v in vals:

        vec2 = vec2+direction*v

        img_pred = deepcopy(mm.get_autoenc_image_from_vector(vec2))
        img_pred = cv2.resize(img_pred, imgshape)
        # img_pred = put_text_on_image(img_pred, "%d, %.3f" % (el, vec2[0][el]))

        ls.add_img(img_pred)

    cv2.imshow("orig", ls.get_grid())
    cv2.waitKey(0)




# for i in xrange(10000):
#     vec = (np.random.rand(*vector.shape))
#     img_pred = mm.get_autoenc_image_from_vector(vec)
#
#     img_pred = cv2.resize(img_pred, (img.shape[0]*SCALE, img.shape[0]*SCALE))
#
#     #img_pred = put_text_on_image(img_pred, "%d, %.3f" % (el,vec2[0][el]))
#     cv2.imshow("pred", img_pred)
#     cv2.waitKey(1000)


# DATASET_STRUCTURE = [
#     ["avanti",[[4,57]]],
#     ["stop",[[3,44]]],
#     ["preced",[[3,43]]],
#     ["limit50", [[5,165]]],
#     ["mix", [[4,57],[3,44],[3,43],[5,165]]]
# ]
#
# ds = get_dataset(DATASET_STRUCTURE, balanced=False, group_count_limit=30)
#
# vectors = {}
#
# for lbl in ds:
#     print lbl
#     sum = 0
#     for i1 in ds[lbl]:
#         for i2 in ds[lbl]:
#             v1 = mm.get_autoenc_predicted_vector(get_img_by_id(i1))
#             v2 = mm.get_autoenc_predicted_vector(get_img_by_id(i2))
#             sum += np.sum(np.power(v1 - v2,2))
#     print sum/(len(ds[lbl])**2)


# vals2 = np.linspace(0,1,100)
#
# for v in vals2:
#     vec = vector*v + vector2*(1.0-v)
#
#     img_pred = mm.get_autoenc_image_from_vector(vec)
#
#     img_pred = cv2.resize(img_pred, (img.shape[0]*2, img.shape[0]*2))
#
#     cv2.imshow("orig", img)
#     cv2.imshow("pred", img_pred)
#     cv2.waitKey(10)
#
# print np.sum(np.power(vector - vector2,2))