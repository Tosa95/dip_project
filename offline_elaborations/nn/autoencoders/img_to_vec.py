import random
from pprint import pprint

from global_settings import get_settings
from service_access import get_all_images, get_img_by_id, get_dataset
from utils.image_caching import ImageCache
from utils.model_manager import ModelManager

import csv as csv_

mm = ModelManager("autoenc_BEST_ext2")

#image_ids = get_all_images()

# DATASET_STRUCTURE = [
#     ["sign",[[1,1]]]
# ]

DATASET_STRUCTURE = [
    ["triangle",[[2,3],[2,4]]],
    ["divieto",[[2,5]]],
    ["obbligo",[[2,6]]],
    ["autovelox",[[2,172]]],
    ["altro",[[2,173]]],
    ["semaforo",[[2,174]]],
    ["no",[[1,2]]]
]

dataset = get_dataset(DATASET_STRUCTURE,False,group_count_limit=500)

res = {}

img_cache = ImageCache(get_settings().get_img_cache_path())

for lbl in dataset:

    image_ids = dataset[lbl]

    for i,id in enumerate(image_ids):
        print float(i)/len(image_ids)

        try:

            img = img_cache.get_image_by_id(id)

            vector = mm.get_autoenc_predicted_vector(img)

            res[id] = (lbl,vector)

        except Exception as ex:

            print ex

csv = []

head = ["ID", "lbl"]

for i,_ in enumerate(res[dataset["altro"][0]][1][0]):
    head.append("el%d" % i)

csv.append(head)

for id,lbl_vec in res.iteritems():

    lbl,vec = lbl_vec

    row = [id,lbl]

    for el in vec[0]:
        row.append(el)

    csv.append(row)

pprint(csv)

with open("img_vecs_2.csv", "wt") as file:
    writer = csv_.writer(file)
    writer.writerows(csv)