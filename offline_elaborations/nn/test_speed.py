import sys
sys.path.append("../")


import datetime
import os
from numpy.ma import mean

from general_utils.parallel.parallel_executor import ParallelExecutor
from global_settings import get_settings
from utils.dataset_creator import get_random_dataset_flatten
from utils.image_caching import ImageCache
from utils.model_manager import ModelManager


MODELS = [
    ("RN1_best__2", os.path.join(get_settings().get_models_path(),"resnets/models")),
    ("M3__2", os.path.join(get_settings().get_models_path(),"test/models"))
]

N_IMG = 3000
SAMPLES = 3

imgcache = ImageCache(get_settings().get_img_cache_path())

for m in MODELS:

    model = ModelManager(*m)

    imgs_per_sec_array = []

    for i in xrange(SAMPLES):

        ids = get_random_dataset_flatten(model.get_dataset_structure(),N_IMG)

        # Needed in order to be sure the model is loaded before the performance evaluation starts
        model.get_prediction(imgcache.get_image_by_id(ids[0]))

        images = []

        print "Getting images"

        for id in ids:
            images.append(imgcache.get_image_by_id(id))

        print "Evaluating performance"

        pexec = ParallelExecutor(8)

        begin = datetime.datetime.now()

        def elab(imgs):

            img_count = 0

            for img in imgs:
                try:
                    model.get_prediction(img)
                    img_count += 1
                except:
                    print "Wrong format image, skipped"

            return img_count


        nimg = pexec.elaborate_all(images,elab,lambda c: sum(c))

        # for img in images:
        #     model.get_prediction(img)

        tot_secs = (datetime.datetime.now() - begin).total_seconds()
        imgs_per_sec = nimg/tot_secs

        imgs_per_sec_array.append(imgs_per_sec)

        print "%s ---> Predicted %d images in %f seconds. %f images per second" % (m[0], nimg, tot_secs, imgs_per_sec)

        pexec.stop()

    print "%s ---> Avg images per second %f" % (m[0], mean(imgs_per_sec_array))