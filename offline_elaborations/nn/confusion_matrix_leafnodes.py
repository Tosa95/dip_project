import numpy as np
import cv2
import datetime

from global_settings import get_settings
from utils.image_caching import ImageCache
from utils.model_manager import ModelManager
from utils.waterfall_model_manager import WaterfallModelManager
from utils.spliced_waterfall_model_manager import SplicedWaterfallModelManager
from utils.dataset_manager_simona import DatasetManager
from utils.images import DoNotTransformImagesTransformer
from utils.image_extenders import ALL_IMAGES_EXTENDERS
from utils.dataset_creator import DatasetCreator, get_full_description_dataset_name, leafs_to_dataset_struct, get_leafs
from service_access import get_label_info, get_label_type_tree
from utils.saving_methods import MemoryDatasetPersister

SETTINGS = get_settings()
DATASETS_FOLDER = SETTINGS.get_dataset_path()
MIN_IMAGES_PER_LABEL = 15
IMAGES_PER_LABEL_AFTER_EXPANSION = 20000

tree = get_label_type_tree()
leafs = []

get_leafs(tree, leafs)

DATASET_STRUCTURE = leafs_to_dataset_struct(leafs, MIN_IMAGES_PER_LABEL)

URL_ICON = "http://multisalakingv2.ddns.net:8081/segnali/images/icons/"
FILE_NAME = get_settings().get_html_conf_matrix_path() + "/confusion_matrix"
SINGLE_MODEL_FILE_NAME = FILE_NAME + "_%s_DATASET_%s.html"


def get_html_image(img_name, label):
    if img_name is not None:
        html_img = """<img src='""" + URL_ICON + str(img_name) + """' class='s5050'>"""
    else:
        html_img = """<span class='s5050' style="display:inline-block;">""" + label + """</span>"""

    return html_img


def get_cell_string(confusion_m, k, j, perc, num_tot_col):
    if perc:
        val = confusion_m[k][j]
        p = val/num_tot_col*100
        r = "%.2f" % p
        return r + "&#37;"
    else:
        return str(confusion_m[k][j])


def create_confusion_matrix(mm_list, dataset_m, perc=False, red_threshold=0.1, filename=None):

    colors = ["#00004d", "#0047b3", "#193366", "#000066", "#132639", "#400080", "#1a0033"]
    red_colors = ["darkred", "red", "#b30000", "#4d0000", "#ff0000", "#b3003b", "#660022"]

    dataset_descriptor = dataset_m.get_descriptor()
    ds = dm.get_dataset()

    size = len(dataset_descriptor['structure'])
    n_models = len(mm_list)

    conf_matrix_list = []

    for n in range(n_models):
        conf_matrix_list.append(np.zeros((size, size), dtype=int))
    positions = {}
    labels = []
    accuracies = []
    acc_without_nosig = []

    for i, lbl_list in enumerate(dataset_descriptor['structure']):
        lbl = lbl_list[0]
        tid = lbl_list[1][0][0]
        vid = lbl_list[1][0][1]
        label_info = get_label_info(tid, vid)
        image_name = label_info['image']
        labels.append((lbl, tid, vid, image_name))
        positions[lbl] = i

    for i, img in enumerate(ds['X']):
        if img is not None:
            index = np.argmax(ds['y'][i])
            lbl = dataset_descriptor["index_to_labels"][index]
            try:
                for n, mm in enumerate(mm_list):
                    predicted_lbl, p = mm.get_prediction_label_perc(img)
                    print i, lbl, positions[lbl], predicted_lbl, positions[predicted_lbl]
                    conf_matrix_list[n][positions[predicted_lbl]][positions[lbl]] += 1
            except Exception as e:
                print e

    for n, mm in enumerate(mm_list):
        confusion_m = conf_matrix_list[n]
        cm_without_nosig = confusion_m[:, :-1]
        accuracy = confusion_m.diagonal().sum()/float(confusion_m.sum())
        a_without_nosig = cm_without_nosig.diagonal().sum()/float(cm_without_nosig.sum())

        conf_matrix_list.append(confusion_m)
        accuracies.append(accuracy)
        acc_without_nosig.append(a_without_nosig)

        print "\nModel name:", mm.get_model_name()
        print "\n", confusion_m
        print "\nAccuracy:  ", accuracy
        print "\nAccuracy without nosign column:  ", a_without_nosig

    red_list = []

    html = """
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Confusion matrix</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
            <link href="./tablesorter-master/css/theme.blue.css" rel="stylesheet">
            <script src="./tablesorter-master/js/jquery.min.js"></script>
            <script src="./tablesorter-master/js/jquery.tablesorter.js"></script>
            <script src="./tablesorter-master/js/jquery.tablesorter.widgets.js"></script>
            <style>
            .rotate {
            text-align: center;
            white-space: nowrap;
            margin-top: auto;
            margin-bottom: auto;
            vertical-align: middle;
            width: 51px;
            }
            .inner {
            position: absolute;
            top: 50%;
            left: 45%; 
            }
            .rotate div {
            transform:  translateX(-50%) translateY(-50%) rotate(-90deg);
            }
            .scrollerX {
            overflow: auto;
            }
            .s5050 {
            width:50px; height:50px;
            }
            /* TABLE BACKGROUND color (match the original theme)  
            table.hover-highlight td:before,
            table.focus-highlight td:before {
              background: #fff;
            } */

            /* ODD ZEBRA STRIPE color (needs zebra widget) */
            .hover-highlight .odd td:before, .hover-highlight .odd th:before,
            .focus-highlight .odd td:before, .focus-highlight .odd th:before {
              background:#e6f7ff;
            }
            /* EVEN ZEBRA STRIPE color (needs zebra widget) */
            .hover-highlight .even td:before, .hover-highlight .even th:before,
            .focus-highlight .even td:before, .focus-highlight .even th:before {
              background:#f9f9f9;
            }

            /* HOVER ROW highlight colors 
            .hover-highlight tbody tr:hover {
              background-color: #b3ffd9;
            } */
            /* HOVER COLUMN highlight colors 
            .hover-highlight tbody tr td:hover::after,
            .hover-highlight tbody tr th:hover::after {
              background-color: #b3ffd9;
            } */
            
            /* FOCUS ROW highlight color (touch devices) */
            .focus-highlight td:focus::before, .focus-highlight th:focus::before {
              background-color: lightblue;
            }
            /* FOCUS COLUMN highlight color (touch devices) */
            .focus-highlight td:focus::after, .focus-highlight th:focus::after {
              background-color: lightblue;
            }
            /* FOCUS CELL highlight color */
            .focus-highlight th:focus, .focus-highlight td:focus,
            .focus-highlight .even th:focus, .focus-highlight .even td:focus,
            .focus-highlight .odd th:focus, .focus-highlight .odd td:focus {
              z-index:-50;
              background-color: #d9d9d9;  /* light gray */
            }

            /* ************************************************* */
            /* **** No need to modify the definitions below **** */
            /* ************************************************* */
            .focus-highlight td:focus::after, .focus-highlight th:focus::after,
            .hover-highlight td:hover::after, .hover-highlight th:hover::after {
              content: '';
              position: absolute;
              width: 100%;
              height: 999em;
              left: 0;
              top: -555em;
              z-index: -1;
            }
            .focus-highlight td:focus::before, .focus-highlight th:focus::before {
              content: '';
              position: absolute;
              width: 999em;
              height: 100%;
              left: -555em;
              top: 0;
              z-index: -2;
            }
            /* required styles */
            .hover-highlight,
            .focus-highlight {
              overflow: hidden;
            }
            .hover-highlight td, .hover-highlight th,
            .focus-highlight td, .focus-highlight th {
              position: relative;
              outline: 0;
            }
            /* override the tablesorter theme styling */
            table.hover-highlight, table.hover-highlight tbody > tr > td,
            table.focus-highlight, table.focus-highlight tbody > tr > td,
            /* override zebra styling */
            table.hover-highlight tbody tr.even > th,
            table.hover-highlight tbody tr.even > td,
            table.hover-highlight tbody tr.odd > th,
            table.hover-highlight tbody tr.odd > td,
            table.focus-highlight tbody tr.even > th,
            table.focus-highlight tbody tr.even > td,
            table.focus-highlight tbody tr.odd > th,
            table.focus-highlight tbody tr.odd > td {
              background: transparent;
            }
            /* table background positioned under the highlight */
            table.hover-highlight td:before,
            table.focus-highlight td:before {
              content: '';
              position: absolute;
              width: 100%;
              height: 100%;
              left: 0;
              top: 0;
              z-index: -3;
            }
            </style>
        </head>
        
        <body>
            <div style="text-align:center; padding-left:30px; padding-right:30px;">
                <h1 style="margin-top:25px; margin-bottom:15px;">Confusion matrix</h1> """

    for n, mm in enumerate(mm_list):
        html += """
            <div style="margin-bottom:7px;">
                %d) &emsp; Model name: <span style='color:%s'>""" % (n+1, colors[n]) + mm.get_model_name() + """</span> 
                    <br> Accuracy: <b style='font-size:20px;'>%.3f""" % accuracies[n] + """</b> 
                    &emsp;Accuracy without nosign column: <b style='font-size:20px;'>%.3f""" % acc_without_nosig[n] + """</b>
            </div> """

    html += """
                <div style="margin-bottom:20px; margin-top:15px;">
                    Dataset: """ + dm.get_dataset_name() + """
                </div>
                
                <div style="align:center; margin-left:auto; margin-right:auto; width:95%;"> 
                    <div class="row">
                        <div class="col-1" style="vertical-align:middle; margin-top:auto; margin-bottom:auto;">
                            <div id="topDiv" style="width:145px"></div>
                        </div>
                        <div class="col-11" style="margin:0px; padding:0px;">
                            <div style="padding-top:10px; padding-bottom:10px; background-color:#007acc; color:white; border:black solid 1px;">ACTUAL CLASS</div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-1">
                            <div id="bottomDiv" style="width:145px;"></div>
                        </div>
                        <div class="col-11" style="margin:0px; padding:0px;">
                            <div class="scrollerX" style="overflow:hidden; border-left:black solid 1px; border-right:black solid 1px;"> 
                                <table id="topSign" width="100%" border=1 style="align:center; margin-left:auto; margin-right:auto; margin-bottom:0px;">
                                     <tr>"""

    for i, l in enumerate(labels):
        html += """<th><a href='#""" + str(i) + str(i) + """' style='color:black;'>
                        """ + get_html_image(l[3], l[0]) + """
                    </a></th>"""

    html += """         <th><div style='width:18px;'></div></th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    
        <div class="row" style="margin-bottom:40px;"> 
            <div class="col-1">
                <div class="row" style="padding:0px; height:750px;">
                    <div class='rotate' style='display:inline-block; background-color:#007acc; color:white; height:100%; border:black solid 1px; border-right:none; margin-left:25.3%;'>
                        <div class="inner">PREDICTED CLASS </div>
                    </div>
                    <div class="scrollerY" style="overflow:hidden; display:inline-block; max-height:750px; border:black solid 1px;">
                        <table id="lateralSign" border=1 style="margin-bottom:30px; margin-top:0px;">"""

    for k, l in enumerate(labels):
        html += """
                    <tr>
                        <th>
                        <a href='#""" + str(k) + str(k) + """' style='color:black;'>
                        """ + get_html_image(l[3], l[0]) + """
                        </a>
                        </th>
                    </tr>"""

    html += """
                    </table>
                </div>
            </div>
        </div>
        
        <div class="col-11" style="margin:0px; padding:0px;">
            <div class="scrollerX scrollerY" style="max-height:750px; overflow:auto; border:black solid 1px;">
                <table class="focus-highlight" width="100%" id="dtHorizontalVertical" border=1>
                """

    for k, l in enumerate(labels):
        html += "<tr>"
        for j in range(size):
            num_tot_col = float(conf_matrix_list[0][:, j].sum())
            html += "<td id='" + str(k) + str(j) + "'>"
            if j == k:
                for n in range(n_models):
                    html += """<div style='font-weight:bold;'>
                                <div class='s5050' style='color:%s;'>""" % colors[n] \
                            + get_cell_string(conf_matrix_list[n], k, j, perc, num_tot_col) + """
                            </div>
                            </div>"""
            else:
                for n in range(n_models):
                    confusion_m = conf_matrix_list[n]
                    val = confusion_m[k][j]
                    if val > num_tot_col * red_threshold:
                        html += "<div class='s5050' style='color:%s;'>" % red_colors[n] + "<b>" + get_cell_string(confusion_m, k, j,
                                                                                                    perc, num_tot_col) + "</b></div>"
                        red_list.append((l, k, j, val, n))
                    else:
                        if val != 0:
                            html += "<div class='s5050' style='color:%s;'>" % colors[n] + get_cell_string(confusion_m, k, j, perc, num_tot_col) + "</div>"
                        else:
                            html += "<div class='s5050' style='color:#b3b3b3;'>" + get_cell_string(confusion_m, k, j, perc, num_tot_col) + "</div>"
            html += "</td>"
        html += "</tr>"

    html += """
                                </table>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                <div style="padding-left:90px; padding-right:90px; margin-bottom:70px;">
                    <h3 style="margin-bottom:10px; color:#330000;">Red list:</h3>
                    <div style="line-height:75px;">
                    """

    for (l, k, j, val, n) in red_list:
        html += "<span style='border:gray solid 1px; padding-top:0px; " \
                "padding-bottom:0px; padding-left:5px; padding-right:5px; margin:10px; display:inline-block;'>" \
                "<a href='#" + str(k) + str(j) + "' style='color:black;'>" \
                + get_html_image(labels[j][3], labels[j][0]) + "<span class='s5050' style='color:%s;'>" % red_colors[n] \
                + "<b>&emsp;" + str(val) + "&emsp;</b></span>" + get_html_image(l[3], l[0]) \
                + """</a></span>
                """

    html += """
                    </div>
                </div>
        
            </div>
            
            <script type="text/javascript">
                $('.scrollerX').scroll(function(e) {
                    $('.scrollerX').scrollLeft(e.target.scrollLeft);
                });
                $('.scrollerY').scroll(function(e) {
                    $('.scrollerY').scrollTop(e.target.scrollTop);
                });
                
                $(function() {
                // call the tablesorter plugin
                    $("table").tablesorter({
                        theme: 'blue',
                        // initialize zebra striping of the table
                        widgets: ["zebra"],
                        // change the default striping class names
                        // updated in v2.1 to use widgetOptions.zebra = ["even", "odd"]
                        // widgetZebra: { css: [ "normal-row", "alt-row" ] } still works
                        widgetOptions : {
                          zebra : [ "even", "odd" ]
                        }
                      });
                    if ( $('.focus-highlight').length ) {
                      $('.focus-highlight').find('td, th')
                        .attr('tabindex', '1')
                        // add touch device support
                        .on('touchstart', function() {
                          $(this).focus();
                        });
                    }
                  });
                  
                var table = document.getElementById("dtHorizontalVertical"),rIndex,cIndex;
                // table rows
                for(var i = 0; i < table.rows.length; i++)
                {
                    // row cells
                    for(var j = 0; j < table.rows[i].cells.length; j++)
                    {
                        table.rows[i].cells[j].addEventListener("focusout", focusoutCell)
                        function focusoutCell() {
                            $('#topDiv').html(""); 
                            $('#bottomDiv').html("");
                        }
                        table.rows[i].cells[j].onclick = function()
                        {
                            rIndex = this.parentElement.rowIndex + 1; // considera la prima riga 0
                            cIndex = this.cellIndex+1;
                            sumVal = 0;
                            correct = 0;
                            for(var i = 0; i < table.rows.length; i++)
                            {   
                                val = parseInt(table.rows[i].cells[cIndex-1].firstChild.textContent);
                                sumVal = sumVal + val;
                                if(i == cIndex-1)
                                    correct = val;
                            }
                            //alert("Row: "+rIndex+" , Cell: "+cIndex+", Sum: "+sumVal+", Correct: "+correct);
                            $('#topDiv').html("Tot: " + sumVal);
                            $('#bottomDiv').html("Correct: " + correct + "<br>(" + (correct/parseFloat(sumVal)*100).toFixed(1) + "%)");
                        };
                    }
                }
            </script>
            
        </body>
        </html>
    """

    if filename is None:
        if n_models == 1:
            file_name = SINGLE_MODEL_FILE_NAME % (mm_list[0].get_model_name(), dm.get_dataset_name())
        else:
            file_name = FILE_NAME + "_comp_" + datetime.datetime.now().strftime("%Y-%m-%d_%H:%M") + "_perc%s" % str(perc)
    else:
        file_name = FILE_NAME + filename + "_perc%s" % str(perc)

    with open(file_name + ".html", "w") as html_file:
        html_file.write(html)


if __name__ == '__main__':

    mm_list = []

    model_name = "leafnodes_ep20_vl0.745779804332_va0.819523809524"
    mod_manager = ModelManager(model_name, get_settings().get_models_path(),
                               image_cache=ImageCache(get_settings().get_img_cache_path()))

    mm_list.append(mod_manager)

    dc = DatasetCreator(DATASETS_FOLDER, image_cache=ImageCache(SETTINGS.get_img_cache_path()),
                        dataset_persister = MemoryDatasetPersister())

    dm = dc.create_dataset(
            get_full_description_dataset_name("leafnodes"),
            DATASET_STRUCTURE,
            DoNotTransformImagesTransformer(),
            balanced=False,
            group_count_limit=IMAGES_PER_LABEL_AFTER_EXPANSION,
            validation_perc=0.0
            )

    filename = "_leafnodes_2"
    create_confusion_matrix(mm_list, dm, perc=False, filename=filename)
