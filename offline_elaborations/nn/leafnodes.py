from pprint import pprint

from global_settings import get_settings
from utils.dataset_manager import DatasetManager
from utils.neural_training import NeuralNetworkTrainer, get_standard_model_creation_func

SETTINGS = get_settings()

# Net parameters
CONV_FILTERS_NUM = 32
DENSE_SIZE = 64
NUM_CONV = 2
NUM_DENSE = 2
DROPOUT_PERC = 0.2

# Training parameters
PATIENCE = 5  # prima era a 1
EPOCHS = 1000

dm = DatasetManager(SETTINGS.get_dataset_path(), "leafnodes__bFalse_gcl700_isx40_isy40_vp0.2_exTrue_vexTrue")
MONITORED_QUANTITY = 'val_loss'
CHECKPOINT_NAME = "leafnodes_ep{epoch}_vl{val_loss}_va{val_acc}"
MODELS_FOLDER = SETTINGS.get_models_path()

nnt = NeuralNetworkTrainer(
    CHECKPOINT_NAME,
    MODELS_FOLDER,
    dm,
    get_standard_model_creation_func(
        CONV_FILTERS_NUM,
        NUM_CONV,
        DENSE_SIZE,
        NUM_DENSE,
        DROPOUT_PERC
    ),
    patience=PATIENCE
)

mm = nnt.train()

pprint(mm.get_descriptor())
pprint(mm.get_validation_data())