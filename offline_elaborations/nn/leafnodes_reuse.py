from keras import Model
from keras.layers import Activation, Dense

from global_settings import get_settings
from utils.dataset_manager import DatasetManager
from utils.model_manager import ModelManager
from utils.neural_training import NeuralNetworkTrainer, get_standard_model_creation_func, get_reuse_model_creation_func

SETTINGS = get_settings()

# Net parameters
CONV_FILTERS_NUM = 32
DENSE_SIZE = 32
NUM_CONV = 2
NUM_DENSE = 1
DROPOUT_PERC = 0.2

# Training parameters
EPOCHS = 1000

dm = DatasetManager(SETTINGS.get_dataset_path(), "leafnodes__bFalse_gcl200_isx40_isy40_vp0.2_exTrue")
MONITORED_QUANTITY = 'val_loss'
CHECKPOINT_NAME = "leafnodes_KRM_ep{epoch}_vl{val_loss}_va{val_acc}"
MODELS_FOLDER = SETTINGS.get_models_path()

def add_layers(model):
    output = model.layers[-1].output
    output = Dense(64, name="supplementar_dense")(output)
    output = Activation('relu', name="supplementar_dense_activation")(output)
    model = Model(model.input, output)

    return model, {}

nnt = NeuralNetworkTrainer(
    CHECKPOINT_NAME,
    MODELS_FOLDER,
    dm,
    get_reuse_model_creation_func(
        ModelManager("sigtype_ep26_vl0.465845957661_va0.86301369793", get_settings().get_models_path()),
        how_many_layers_to_remove=2,
        add_layers_function=add_layers
    ),
    patience=30
)

nnt.train()