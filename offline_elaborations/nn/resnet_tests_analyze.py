import pprint

from utils.multiple_models_trainer import MultipleModelsTrainer

mmt = MultipleModelsTrainer("resnets")

print "val_acc"
pprint.pprint(mmt.analyze("val_acc", outl_tolerance=1.2))

print "val_loss"
pprint.pprint(mmt.analyze("val_loss"))

mmt.analyze_to_dataframe(["val_acc", "val_loss"])