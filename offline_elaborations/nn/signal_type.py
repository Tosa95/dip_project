from global_settings import get_settings
from utils.dataset_creator import DatasetCreator
from utils.dataset_manager import DatasetManager
from utils.image_caching import ImageCache
from utils.image_extenders import ALL_IMAGES_EXTENDERS
from utils.images import ImagesTransformer
from utils.neural_training import NeuralNetworkTrainer, get_standard_model_creation_func, \
    get_no_drop_model_creation_func, get_dense_model_creation_func
from utils.saving_methods import MemoryDatasetPersister

SETTINGS = get_settings()

DATASET_FOLDER = SETTINGS.get_dataset_path()

DATASET_STRUCTURE = [
    ["pericolo",[[2,3]]],
    ["precedenza",[[2,4]]],
    ["divieto",[[2,5]]],
    ["obbligo",[[2,6]]],
    ["autovelox",[[2,172]]],
    ["altro",[[2,173]]],
    ["semaforo",[[2,174]]],
    ["nosig",[[1,2]]]
]

dc = DatasetCreator(DATASET_FOLDER, image_cache=ImageCache(SETTINGS.get_img_cache_path()), dataset_persister=MemoryDatasetPersister())

dm = dc.create_dataset(
    "",
    DATASET_STRUCTURE,
    ImagesTransformer(40,40),
    balanced=False,
    group_count_limit=100,
    images_extender=ALL_IMAGES_EXTENDERS
)

# Net parameters
CONV_FILTERS_NUM = 32
DENSE_SIZE = 32
NUM_CONV = 2
NUM_DENSE = 1
DROPOUT_PERC = 0.2

# Training parameters
PATIENCE = 15
EPOCHS = 1000

MONITORED_QUANTITY = 'val_loss'
CHECKPOINT_NAME = "sigtype_ep{epoch}_vl{val_loss}_va{val_acc}"
MODELS_FOLDER = SETTINGS.get_models_path()

nnt = NeuralNetworkTrainer(
    CHECKPOINT_NAME,
    MODELS_FOLDER,
    dm,
    get_no_drop_model_creation_func(
        CONV_FILTERS_NUM,
        NUM_CONV,
        DENSE_SIZE,
        NUM_DENSE
    ),
    patience=10
)

nnt.train()