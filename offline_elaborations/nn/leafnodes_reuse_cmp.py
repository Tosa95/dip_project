import json
import os

import numpy as np

from keras import Model
from keras.layers import Activation, Dense

from global_settings import get_settings
from utils.dataset_manager import DatasetManager
from utils.model_manager import ModelManager
from utils.neural_training import NeuralNetworkTrainer, get_standard_model_creation_func, get_reuse_model_creation_func

SETTINGS = get_settings()
dm = DatasetManager(SETTINGS.get_dataset_path(), "leafnodes__bFalse_gcl50_isx40_isy40_vp0.2_exTrue")

ROUNDS = 100

def do_cmp():
    # Net parameters
    CONV_FILTERS_NUM = 32
    DENSE_SIZE = 32
    NUM_CONV = 2
    NUM_DENSE = 1
    DROPOUT_PERC = 0.2

    # Training parameters
    PATIENCE = 15

    CHECKPOINT_NAME_KRM = "leafnodes_KRM_ep{epoch}_vl{val_loss}_va{val_acc}"
    CHECKPOINT_NAME_CM = "leafnodes_ep{epoch}_vl{val_loss}_va{val_acc}"
    MODELS_FOLDER = SETTINGS.get_models_path()

    def add_layers(model):
        output = model.layers[-1].output
        output = Dense(64, name="supplementar_dense")(output)
        output = Activation('relu', name="supplementar_dense_activation")(output)
        model = Model(model.input, output)

        return model, {}

    try:
        with open(os.path.join(SETTINGS.get_dataset_path(), "test_KRM_2_%s.json" % dm.get_name()), "rt") as input:
            res = json.load(input)

        print res
    except:
        res = []

    for i in xrange(ROUNDS):

        print "Round %d" % i

        nntKRM = NeuralNetworkTrainer(
            CHECKPOINT_NAME_KRM,
            MODELS_FOLDER,
            dm,
            get_reuse_model_creation_func(
                ModelManager("sigtype_ep26_vl0.465845957661_va0.86301369793", get_settings().get_models_path()),
                how_many_layers_to_remove=2,
                # add_layers_function=add_layers
            ),
            patience=PATIENCE
        )

        nntCM = NeuralNetworkTrainer(
            CHECKPOINT_NAME_CM,
            MODELS_FOLDER,
            dm,
            get_standard_model_creation_func(
                CONV_FILTERS_NUM,
                NUM_CONV,
                DENSE_SIZE,
                NUM_DENSE,
                DROPOUT_PERC
            ),
            patience=PATIENCE
        )

        mmKRM = nntKRM.train()
        mmCM = nntCM.train()

        res.append({
            "KRM": mmKRM.get_validation_data(),
            "CM": mmCM.get_validation_data()
        })

        with open(os.path.join(SETTINGS.get_dataset_path(), "test_KRM_2_%s.json" % dm.get_name()),"wt") as output:
            json.dump(res, output, indent=4)

def do_avg():

    with open(os.path.join(SETTINGS.get_dataset_path(), "test_KRM_2_%s.json" % dm.get_name()), "rt") as input:
        data = json.load(input)

    print "Num samples: %d" % len(data)

    lossCM = np.mean(np.array([d["CM"]["val_loss"] for d in data]))
    std_loss_CM = np.std(np.array([d["CM"]["val_loss"] for d in data]))
    lossKRM = np.mean(np.array([d["KRM"]["val_loss"] for d in data]))
    std_loss_KRM = np.std(np.array([d["KRM"]["val_loss"] for d in data]))

    accCM = np.mean(np.array([d["CM"]["val_acc"] for d in data]))
    std_acc_CM = np.std(np.array([d["CM"]["val_acc"] for d in data]))
    accKRM = np.mean(np.array([d["KRM"]["val_acc"] for d in data]))
    std_acc_KRM = np.std(np.array([d["KRM"]["val_acc"] for d in data]))


    print "loss: CM %.3f (+-%.3f), KRM %.3f (+-%.3f)" % (lossCM, std_loss_CM, lossKRM, std_loss_KRM)
    print "acc: CM %.3f (+-%.3f), KRM %.3f (+-%.3f)" % (accCM, std_acc_CM, accKRM, std_acc_KRM)

#do_cmp()
do_avg()