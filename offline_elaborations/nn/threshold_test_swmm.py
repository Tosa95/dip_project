import numpy as np

from global_settings import get_settings
from utils.image_caching import ImageCache
from utils.images import DoNotTransformImagesTransformer
from utils.model_manager import ModelManager
from utils.spliced_waterfall_model_manager import SplicedWaterfallModelManager
from utils.dataset_creator import DatasetCreator, get_full_description_dataset_name
from utils.saving_methods import MemoryDatasetPersister

SETTINGS = get_settings()
DATASETS_FOLDER = SETTINGS.get_dataset_path()

# MIN_IMAGES_PER_LABEL = 10
IMAGES_PER_LABEL_AFTER_EXPANSION = 15000

#DATASET_STRUCTURE = leafs_to_dataset_struct(leafs, MIN_IMAGES_PER_LABEL)
DATASET_STRUCTURE = [
    ["pericolo",[[2,3]]],
    ["precedenza",[[2,4]]],
    ["divieto",[[2,5]]],
    ["obbligo",[[2,6]]],
    ["autovelox",[[2,172]]],
    ["altro",[[2,173]]],
    ["semaforo",[[2,174]]],
    ["nosig",[[1,2]]]
]

sns_model_name = "signosig_ep36_vl0.22417925785_va0.922087205487"
st_w_nosign_model_name = "sigtype_ep22_vl0.426999311128_va0.884418901736"
st_model_name = "sigtype_without_nosig_ep22_vl0.330800220748_va0.913043478261"

sns_mm = ModelManager(sns_model_name, SETTINGS.get_models_path(),
                      image_cache=ImageCache(SETTINGS.get_img_cache_path()))
st_w_ns_mm = ModelManager(st_w_nosign_model_name, SETTINGS.get_models_path(),
                          image_cache=ImageCache(SETTINGS.get_img_cache_path()))
st_mm = ModelManager(st_model_name, SETTINGS.get_models_path(),
                     image_cache=ImageCache(SETTINGS.get_img_cache_path()))

dc = DatasetCreator(DATASETS_FOLDER, image_cache=ImageCache(SETTINGS.get_img_cache_path()),
                        dataset_persister = MemoryDatasetPersister())
dm = dc.create_dataset(
        get_full_description_dataset_name("sigtype"),
        DATASET_STRUCTURE,
        DoNotTransformImagesTransformer(),
        balanced=False,
        group_count_limit=IMAGES_PER_LABEL_AFTER_EXPANSION,
        validation_perc=0.0
        )
ds = dm.get_dataset()

low_th = np.array([0.5, 0.6, 0.7, 0.8])
high_th = np.array([0.6, 0.7, 0.8, 0.9])
sig_th = np.array([0.5, 0.6, 0.7, 0.8, 0.9])

best_low_th = None
best_high_th = None
best_sig_th = None
best_acc_without_nosig_col = None
best_mm_acc = None

r = ""

for l in low_th:
    for h in high_th[high_th > l]:
        for s in sig_th:
            accuracy = 0
            acc_without_nosig_col = 0
            num_sign = 0
            swmm = SplicedWaterfallModelManager(sns_mm, st_w_ns_mm, st_mm, low_threshold=l, high_threshold=h, sig_threshold=s)
            for i, img in enumerate(ds['X']):
                if img is not None:
                    index = np.argmax(ds['y'][i])
                    lbl = dm.get_descriptor()["index_to_labels"][index]
                    predicted_lbl, perc = swmm.get_prediction_label_perc(img)
                    print i, lbl, predicted_lbl
                    if lbl != "nosig":
                        num_sign += 1
                    if predicted_lbl == lbl:
                        accuracy += 1
                        if predicted_lbl != "nosig":
                            acc_without_nosig_col += 1

            accuracy /= float(i)
            acc_without_nosig_col /= float(num_sign)

            r += "\n" + str(l) + " " + str(h) + " " + str(s) + " %.3f " % accuracy + " %.3f " % acc_without_nosig_col

            print "\n", l, h, s, accuracy, acc_without_nosig_col

            if best_acc_without_nosig_col is None or best_acc_without_nosig_col < acc_without_nosig_col:
                best_low_th = l
                best_high_th = h
                best_sig_th = s
                best_acc_without_nosig_col = acc_without_nosig_col
                best_mm_acc = accuracy

                r += "\nNEW BEST!\n"
                print "\nNEW BEST!\n"

r += "\n\nBEST: %.2f %.2f %.2f %.3f %.3f" % (best_low_th, best_high_th, best_sig_th, best_mm_acc, best_acc_without_nosig_col)
print "\n\n\n\n\n", r
