import sys

sys.path.append("../")

from utils.image_extenders import CompoundImageExtender, PerspectiveImageExtender, TranslationImageExtender, \
    BlurImageExtender, NoiseImageExtender, LuminosityImageExtender, ColourTemperatureImageExtender, \
    SaturationImageExtender
from utils.images import ImagesTransformer
from utils.multiple_models_trainer import MultipleModelsTrainer, ModelAndDataset

from utils.resnets import get_dw_resnet_class_model_creation_func

DATASET_STRUCTURE = [
    ["sig", [[1,1]]],
    ["nosig", [[1,2]]]
]

DATASET_PARAMETERS = {
    "name": "",
    "dataset_description": DATASET_STRUCTURE,
    "images_transformer": ImagesTransformer(40,40),
    "balanced": False,
    "group_count_limit": 15000,
    "images_extender": CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30, 50)),
            PerspectiveImageExtender(dy_range=(30, 50)),
            PerspectiveImageExtender(dz_range=(10, 20)),
            TranslationImageExtender(x_range=(0.05, 0.07)),
            TranslationImageExtender(y_range=(0.05, 0.07)),
            BlurImageExtender(x_range=(0.03, 0.07)),
            BlurImageExtender(y_range=(0.03, 0.07)),
            NoiseImageExtender(amt_range=(5, 20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6)),
            ColourTemperatureImageExtender(b_range=(0.9, 1.1), r_range=(0.9, 1.1)),
            SaturationImageExtender(amt_range=(0.5, 1.5))
        ],
        (5, 9)
    ),
    "expand_validation": True,
    "hyper_expansion_size": 30000
}

mmt = MultipleModelsTrainer("signosig", patience=10, tests_per_model=20, models=[
    ModelAndDataset(
        name = "RN1",
        model_creation_func=get_dw_resnet_class_model_creation_func(
            conv_filters_num=64,
            num_conv=3,
            dropout_perc=0.4,
            max_poolings=[(2, 2), (2, 2), (2, 2)],
            depth_mults=[2,3,4],
            dense_size=[100],
            batch_norm=True
        ),
        dataset_parameters=DATASET_PARAMETERS
    ),
])

mmt.start_tests()