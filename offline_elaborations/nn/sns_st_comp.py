import numpy as np
import time

from global_settings import get_settings
from utils.dataset_manager_simona import DatasetManager
from utils.dataset_manager_simona import LabelsTraducer
from utils.dataset_creator import DatasetCreator, get_full_description_dataset_name
from utils.images import DoNotTransformImagesTransformer
from utils.image_caching import ImageCache
from utils.model_manager import ModelManager
from utils.waterfall_model_manager import WaterfallModelManager
from utils.saving_methods import MemoryDatasetPersister

IMAGES_PER_LABEL_AFTER_EXPANSION = 5000

SETTINGS = get_settings()
DATASETS_FOLDER = SETTINGS.get_dataset_path()

THRESHOLD = 0.5
DATASET_STRUCTURE = [
    ["pericolo",[[2,3]]],
    ["precedenza",[[2,4]]],
    ["divieto",[[2,5]]],
    ["obbligo",[[2,6]]],
    ["autovelox",[[2,172]]],
    ["altro",[[2,173]]],
    ["semaforo",[[2,174]]],
    ["nosig",[[1,2]]]
]

modelManagers = []
accuracies = []
tt = []
ct = []

dc = DatasetCreator(DATASETS_FOLDER, image_cache=ImageCache(SETTINGS.get_img_cache_path()),
                        dataset_persister = MemoryDatasetPersister())

dm = dc.create_dataset(
        get_full_description_dataset_name("sigtype"),
        DATASET_STRUCTURE,
        DoNotTransformImagesTransformer(),
        balanced=False,
        group_count_limit=IMAGES_PER_LABEL_AFTER_EXPANSION,
        validation_perc=0.0
        )

ds = dm.get_dataset()

sns_model_name = "signosig_ep36_vl0.22417925785_va0.922087205487"
st_model_name = "sigtype_ep22_vl0.426999311128_va0.884418901736"

sns_mm = ModelManager(sns_model_name, get_settings().get_models_path(),
                    image_cache=ImageCache(get_settings().get_img_cache_path()))
st_mm = ModelManager(st_model_name, get_settings().get_models_path(),
                    image_cache=ImageCache(get_settings().get_img_cache_path()))

waterfall_mm = WaterfallModelManager(sns_mm, st_mm, THRESHOLD)

st_model_name = "sigtype_ep22_vl0.426999311128_va0.884418901736"

st_w_ns_mm = ModelManager(st_model_name, get_settings().get_models_path(),
                    image_cache=ImageCache(get_settings().get_img_cache_path()))

modelManagers.append(st_w_ns_mm)
modelManagers.append(waterfall_mm)

for mm in modelManagers:
    t_start = time.time()
    c_start = time.clock()

    accuracy = 0
    i = 0

    for i, img in enumerate(ds['X']):
        if img is not None:
            index = np.argmax(ds['y'][i])
            lbl = dm.get_descriptor()["index_to_labels"][index]
            predicted_lbl, perc = mm.get_prediction_label_perc(img)
            print i, lbl, predicted_lbl
            if predicted_lbl == lbl:
                accuracy += 1

    accuracy /= float(i)
    accuracies.append(accuracy)

    t_stop = time.time()
    c_sttop = time.clock()

    tt.append(t_stop - t_start)
    ct.append(c_sttop - c_start)

print "\nAccuracy sigtype model: ", accuracies[0], " time.time: " + str(tt[0]) + " time.clock: " + str(ct[0])
print "Accuracy waterfall model: ", accuracies[1], " time.time: " + str(tt[1]) + " time.clock: " + str(ct[1])

