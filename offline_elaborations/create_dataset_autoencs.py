from global_settings import Settings, get_settings
from service_access import get_label_type_tree

from utils.dataset_creator import DatasetCreator, get_full_description_dataset_name, get_leafs, leafs_to_dataset_struct
from utils.image_caching import ImageCache
from utils.image_extenders import CompoundImageExtender, PerspectiveImageExtender, TranslationImageExtender, \
    BlurImageExtender, NoiseImageExtender, LuminosityImageExtender, ALL_IMAGES_EXTENDERS
from utils.images import ImagesTransformer

SETTINGS = get_settings()
DATASET_FOLDER = SETTINGS.get_dataset_path()

DATASET_STRUCTURE = [
    ["pericolo",[[2,3]]],
    ["precedenza",[[2,4]]],
    ["divieto",[[2,5]]],
    ["obbligo",[[2,6]]]
]

tree = get_label_type_tree()
leafs = []

get_leafs(tree,leafs)

DATASET_STRUCTURE = leafs_to_dataset_struct(leafs, min_size=10)

DATASET_STRUCTURE = [grp for grp in DATASET_STRUCTURE if grp[0] != "no"]

dc = DatasetCreator(DATASET_FOLDER, image_cache=ImageCache(SETTINGS.get_img_cache_path()))

dc.create_dataset(
    get_full_description_dataset_name("autoencs"),
    DATASET_STRUCTURE,
    ImagesTransformer(40,40),
    balanced=False,
    group_count_limit=700,
    images_extender=ALL_IMAGES_EXTENDERS,
    for_autoencoders=True
)