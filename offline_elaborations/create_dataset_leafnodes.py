from pprint import pprint

from global_settings import Settings, get_settings
from service_access import get_label_type_tree

from utils.dataset_creator import DatasetCreator, get_full_description_dataset_name, get_leafs, leafs_to_dataset_struct
from utils.image_caching import ImageCache
from utils.image_extenders import CompoundImageExtender, PerspectiveImageExtender, TranslationImageExtender, \
    BlurImageExtender, NoiseImageExtender, LuminosityImageExtender, ALL_IMAGES_EXTENDERS
from utils.images import ImagesTransformer
from utils.saving_methods import MemoryDatasetPersister

SETTINGS = get_settings()
DATASETS_FOLDER = SETTINGS.get_dataset_path()
MIN_IMAGES_PER_LABEL = 10
IMAGES_PER_LABEL_AFTER_EXPANSION = 1000

tree = get_label_type_tree()
leafs = []

get_leafs(tree,leafs)

DATASET_STRUCTURE = leafs_to_dataset_struct(leafs, MIN_IMAGES_PER_LABEL)

pprint(DATASET_STRUCTURE)

print "Found %d labels with more than %d images" % (len(DATASET_STRUCTURE), MIN_IMAGES_PER_LABEL)

dc = DatasetCreator(DATASETS_FOLDER, image_cache=ImageCache(SETTINGS.get_img_cache_path()))

dc.create_dataset(
    get_full_description_dataset_name("leafnodes"),
    DATASET_STRUCTURE,
    ImagesTransformer(40,40),
    balanced=False,
    group_count_limit=IMAGES_PER_LABEL_AFTER_EXPANSION,
    images_extender=CompoundImageExtender(
        [
            PerspectiveImageExtender(dx_range=(30,50)),
            PerspectiveImageExtender(dy_range=(30,50)),
            PerspectiveImageExtender(dz_range=(10,20)),
            TranslationImageExtender(x_range=(0.1,0.15)),
            TranslationImageExtender(y_range=(0.1,0.15)),
            BlurImageExtender(x_range=(0.03,0.07)),
            BlurImageExtender(y_range=(0.03,0.07)),
            NoiseImageExtender(amt_range=(5,20)),
            LuminosityImageExtender(amt_range=(0.4, 1.6))
        ],
        (3,7)
    ),
    expand_validation=True
)
