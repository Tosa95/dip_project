import cv2
import time

from service_access import get_all_images, get_img_by_id, delete_image

FROM_ID = 30000

image_ids = get_all_images()

print image_ids

for i, img_id in enumerate(image_ids):

    if img_id >= FROM_ID:

        err = True
        trials = 0
        while err and trials < 5:
            try:
                img = get_img_by_id(img_id)

                if i%100 == 0:
                    print float(i)/len(image_ids), img_id
            except cv2.error as excv:
                print "%d should be removed" % img_id
                delete_image(img_id)
                break
            except Exception as ex:
                trials += 1
                print "ERR %d" % trials
                print type(ex)
                time.sleep(0.5)
            else:
                err = False