from datetime import datetime

import cv2
import time

from urllib3.util import wait

from global_settings import get_settings
from image_score import ImageScorer
from service_access import get_all_unscored_images, add_image_score, get_all_images, set_image_score, get_img_by_id, \
    get_label_type_tree, set_images_scores
from utils.image_caching import ImageCache
from utils.model_manager import ModelManager
from utils.scores import Interp1dSingleAxisScore, CompoundScore, LabelsTreeHelper, AddScoreCombiner, \
    MultiplyScoreCombiner, ScoresCache, PolyfitSingleAxisScore

import numpy as np

VERBOSE = False

SIGNAL_PREDICTION_SCORE_ID = 1
STRANGE_THING_SCORE_ID = 2
TYPE_VALUE_PREDICTOR_SCORE_ID = 4
LABELING_ORDERING_SCORE_ID = 5

N_SCORES_PER_IMAGE = 4
N_LABELS_PER_IMAGE = 6

RESCORE_ALL_IMAGES = False
STARTING_ID = 0

# Takes into account the level of certainity of the model. We prefer samples with a certainity around 0.9
# (too high is bad: the model is already certain on the image)
score_sig_p = Interp1dSingleAxisScore([(1, 0.5), (0.95, 0.95), (0.9, 1), (0.65, 0.85), (0.6, 0.7),  (0.4, 0.65),
                                       (0.39,0.00001),  (0, 0.00001)])
score_leaf_p = Interp1dSingleAxisScore([(0, 0), (0.3,1), (0.7,0.9), (1,0.5)])
score_snum = Interp1dSingleAxisScore([(0,1), (1,0)])
cs = CompoundScore([score_sig_p, score_leaf_p, score_snum], MultiplyScoreCombiner([1,1.4,2]))


img_cache = ImageCache(get_settings().get_img_cache_path())

scorer = ImageScorer(ModelManager("signosig_ep17_vl0.148822210102_va0.95037593985", get_settings().get_models_path()),
                     image_cache=img_cache)

# type_value_predictor = ModelManager("leafnodes_BEST_60lab")
type_value_predictor = ModelManager("leafnodes_BEST_64lab")

print "Getting labels tree..."
tree = get_label_type_tree()
lth = LabelsTreeHelper(tree)

print "Getting images ids to score..."
if RESCORE_ALL_IMAGES:
    image_ids = get_all_images()
else:
    image_ids = get_all_unscored_images(1)

image_ids = [iid for iid in image_ids if iid >= STARTING_ID]

# print image_ids

sc = ScoresCache(send_every=200, scores_to_be_added=len(image_ids)*N_SCORES_PER_IMAGE, labels_to_be_added=len(image_ids)*N_LABELS_PER_IMAGE)
sc.start()

for i, img_id in enumerate(image_ids):

    err = True
    trials = 0
    while err and trials < 5:
        try:
            begin = datetime.now()

            sig_perc, strange_sign_score = scorer.get_score(img_id)
            all_leafnodes_percs = type_value_predictor.get_prediction_lbl_TID_VID_perc_vector(img_cache.get_image_by_id(img_id))

            lbl, tid, vid, best_leafnode_prediction_perc = all_leafnodes_percs[0].lbl, all_leafnodes_percs[0].tid, \
                                                           all_leafnodes_percs[0].vid, all_leafnodes_percs[0].perc.item()

            samples_count_relative = float(lth.get_average_probable_size(all_leafnodes_percs))/lth.get_max_samples_cnt()

            pred_perc_diminuished = best_leafnode_prediction_perc

            if sig_perc < 0.4:
                best_leafnode_prediction_perc = 0
                pred_perc_diminuished = 0.1



            labeling_score = cs.get_score([sig_perc, pred_perc_diminuished, samples_count_relative]).item()

            if VERBOSE:
                print all_leafnodes_percs
                print tid, vid, best_leafnode_prediction_perc
                print "%.3f, %.3f, %.3f" % (sig_perc * best_leafnode_prediction_perc, samples_count_relative, labeling_score)
                print img_id, sig_perc, strange_sign_score, tid, vid, best_leafnode_prediction_perc, i, len(image_ids)
                print datetime.now() - begin

            begin = datetime.now()

            scores = []

            sc.add(img_id, SIGNAL_PREDICTION_SCORE_ID, float(sig_perc))
            sc.add(img_id, STRANGE_THING_SCORE_ID, float(strange_sign_score))
            sc.add(img_id, TYPE_VALUE_PREDICTOR_SCORE_ID, float(best_leafnode_prediction_perc), int(tid), int(vid))
            sc.add(img_id, LABELING_ORDERING_SCORE_ID, float(labeling_score))

            sc.delete_all_images_predictions(img_id)
            for pred in all_leafnodes_percs[:N_LABELS_PER_IMAGE]:
                # TODO: optimize by adding predictions in chunks as previously done with scores
                sc.add_prediction(img_id, pred.perc.item(), pred.tid, pred.vid)

            if VERBOSE:
                print datetime.now() - begin

        except cv2.error as excv:
            print "OPENCV_error, malformed image"
            break
        except Exception as ex:
            trials += 1
            print "ERR %d" % trials
            print type(ex)
            time.sleep(0.5)
        else:
            err = False

sc.send_all()
sc.send_all_predictions()
time.sleep(1)
sc.stop()

