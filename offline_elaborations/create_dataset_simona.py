from global_settings import get_settings
from utils.dataset_creator import DatasetCreator, get_full_description_dataset_name
from utils.image_caching import ImageCache
from utils.image_extenders import CompoundImageExtender, PerspectiveImageExtender, TranslationImageExtender, \
    BlurImageExtender, NoiseImageExtender, LuminosityImageExtender, ALL_IMAGES_EXTENDERS
from utils.images import ImagesTransformer

SETTINGS = get_settings()
DATASETS_FOLDER = SETTINGS.get_dataset_path()

DATASET_STRUCTURE = [
    ["sig", [[2,3],[2,4],[2,5],[2,6],[2,172],[2,173],[2,174]]],
    ["nosig", [[1,2]]]
]

dc = DatasetCreator(DATASETS_FOLDER, image_cache=ImageCache(SETTINGS.get_img_cache_path()))

dc.create_dataset(
    get_full_description_dataset_name("signosig"),
    DATASET_STRUCTURE,
    ImagesTransformer(40,40),
    balanced=False,
    group_count_limit=15000,
    images_extender=ALL_IMAGES_EXTENDERS
)

