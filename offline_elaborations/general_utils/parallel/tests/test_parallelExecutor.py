from unittest import TestCase

from general_utils.parallel.parallel_executor import ParallelExecutor


class TestParallelExecutor(TestCase):

    def test_base(self):

        data = range(1200)

        pexec = ParallelExecutor(4)

        res = pexec.elaborate_all(data, lambda c: sum(c), lambda c: sum(c))

        self.assertEqual(res, sum(data))

        pexec.stop()

    def test_division(self):

        data = range(1200)

        divisions = {"calls": 0}

        def acc(cr):
            divisions["divs"] = len(cr)
            return sum(cr)

        def elab(chunk):
            divisions["calls"] += 1
            return sum(chunk)

        pexec = ParallelExecutor(4)

        res = pexec.elaborate_all(data, elab, acc)

        pexec.stop()

        self.assertEqual(divisions["divs"], 4)
        self.assertEqual(divisions["calls"], 4)
