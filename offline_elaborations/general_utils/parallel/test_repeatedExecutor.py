import time
from unittest import TestCase

from general_utils.parallel.repeated_executor import RepeatedExecutor


class TestRepeatedExecutor(TestCase):

    def test_repeat_every_100ms(self):

        a = {"cnt": 0}

        def inc():
            a["cnt"] += 1

        rexec = RepeatedExecutor(inc, 0.1)
        rexec.start()

        time.sleep(1.05)

        rexec.stop()

        time.sleep(1)

        self.assertEqual(a["cnt"], 10)
