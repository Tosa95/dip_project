import sched, time
import threading
from threading import Thread

SLEEP_MAX_DURATION_SECS = 0.5

class RepeatedExecutor:

    def __init__(self, func, every_secs, sleep_max_duration=SLEEP_MAX_DURATION_SECS):
        self._thread = Thread(target=self._run)
        self._func = func
        self._time = every_secs
        self._stop = False
        self._sleep_max_duration = sleep_max_duration

    def _run(self):

        while not self._stop:
            # print "AAAAA" + str(threading.current_thread().ident)
            self._func()

            remaining_to_sleep = self._time
            while remaining_to_sleep > 0:
                sleep_time = min([remaining_to_sleep, self._sleep_max_duration])
                time.sleep(sleep_time)
                remaining_to_sleep -= sleep_time
                if self._stop:
                    break

    def start(self, initial_delay=0):
        #TODO: move into thread
        time.sleep(initial_delay)
        self._thread.start()

    def stop(self):
        self._stop = True
