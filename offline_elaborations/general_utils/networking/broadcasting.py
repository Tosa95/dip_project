import time
import socket
from _socket import AF_INET, SOCK_DGRAM
from threading import Thread

DEFAULT_SOCKET_TIMEOUT = 1.0
SOCK_MAX_RECV_SIZE = 1024
BROADCAST_ADDRESS = '255.255.255.255'


class SocketListener:

    def __init__(self, socket_family, socket_type, host_port, on_receive_func, host_address='',
                 socket_timeout=DEFAULT_SOCKET_TIMEOUT, socket_max_recv_size=SOCK_MAX_RECV_SIZE):
        self._thread = Thread(target=self._body)
        self._sock = socket.socket(socket_family, socket_type)
        self._sock.settimeout(socket_timeout)
        self._host_port = host_port
        self._host_address = host_address
        self._stop = False
        self._on_receive = on_receive_func
        self._sock_max_recv_size = socket_max_recv_size

    def start(self):
        self._thread.start()

    def stop(self):
        self._stop = True
        self._thread.join()

    def _body(self):

        self._sock.bind((self._host_address, self._host_port))

        while not self._stop:
            try:
                m = self._sock.recvfrom(self._sock_max_recv_size)
                self._on_receive(m)
            except socket.timeout:
                pass

        self._sock.close()

def send_dgram(msg, dest_addr, dest_port):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.sendto(msg, (dest_addr, dest_port))

def send_broadcast_dgram(msg, dest_port):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    s.sendto(msg, (BROADCAST_ADDRESS, dest_port))

class BroadcastDiscoveryServer:

    def __init__(self, port, msg=""):
        self._sl = SocketListener(AF_INET, SOCK_DGRAM, port, self._on_recv)
        self._msg = msg

    def start(self):
        self._sl.start()

    def _on_recv(self, m):
        port = int(m[0])
        send_dgram(self._msg, m[1][0], port)

    def stop(self):
        self._sl.stop()


def discover_servers(remote_port, local_port=12345, wait_time=1.0):

    res = []

    def recv(m):
        res.append((m[1][0], m[0]))

    sl = SocketListener(AF_INET, SOCK_DGRAM, local_port, recv)
    sl.start()

    send_broadcast_dgram(local_port.__str__(), remote_port)

    time.sleep(wait_time)

    sl.stop()

    return res