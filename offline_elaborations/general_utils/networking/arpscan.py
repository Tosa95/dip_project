def scan(ip_range, interface):
    from scapy.all import srp, Ether, ARP, conf
    conf.verb = 0
    ans, uans = srp(Ether(dst="FF:FF:FF:FF:FF:FF") / ARP(pdst=ip_range), timeout=2, iface=interface, inter=0.1)

    res = []

    for snd, rcv in ans:
        src_mac = rcv.sprintf(r"%Ether.src%")
        src_ip = rcv.sprintf(r"%ARP.psrc%")
        res.append((src_mac, src_ip))

    return res