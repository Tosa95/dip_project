import requests

DEFAULT_URL_CHECK_TIMEOUT = 2.0

def url_ok(url, timeout=DEFAULT_URL_CHECK_TIMEOUT):
    try:
        r = requests.head(url, timeout=timeout)
        return r.status_code == 200
    except Exception as e:
        return False

if __name__=="__main__":
    print url_ok("http://multisalakingv2.ddns.net:8080/multisalakingo/", 1)
    print url_ok("http://www.google.com", 1)
    print url_ok("http://192.168.1.33:5000/get_stats", 1)