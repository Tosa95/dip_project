import json
from os.path import dirname

from utils.saving_methods import PickleDatasetPersister, JoblibDatasetPersister

SETTINGS = {
    "davide": {
        "DATASET_PATH": "/root/datasets",
        "MODELS_PATH": "/root/models",
        "IMAGE_CACHE_PATH": "/root/imgcache",
        "DATASET_PERSISTER": PickleDatasetPersister(),
        "HTML_CONF_MATRIX_PATH": "/root/html_conf_matrix"
    },

    "simona": {
        "DATASET_PATH": "/home/simona/datasets",
        "RESOL_TEST_PATH": "/home/simona/datasets/resolution_test",
        "MODELS_PATH": "/home/simona/models",
        "MOD_RESOL_TEST_PATH": "/home/simona/models/resolution_test",
        "IMAGE_CACHE_PATH": "/home/simona/imgcache",
        "DATASET_PERSISTER": JoblibDatasetPersister(),
        "HTML_CONF_MATRIX_PATH": "/home/simona/html_conf_matrix"
    },

    "pi": {
        "DATASET_PATH": "/home/pi/datasets",
        "RESOL_TEST_PATH": "/home/pi/datasets/resolution_test",
        "MODELS_PATH": "/home/pi/models",
        "MOD_RESOL_TEST_PATH": "/home/pi/models/resolution_test",
        "IMAGE_CACHE_PATH": "/home/pi/imgcache",
        "DATASET_PERSISTER": PickleDatasetPersister(),
        "HTML_CONF_MATRIX_PATH": "/home/pi/html_conf_matrix"
    }
}


class Settings:

    def __init__(self, person):
        self._person = person

    def get_dataset_path(self):
        return SETTINGS[self._person]["DATASET_PATH"]

    def get_resol_test_path(self):
        return SETTINGS[self._person]["RESOL_TEST_PATH"]

    def get_models_path(self):
        return SETTINGS[self._person]["MODELS_PATH"]

    def get_mod_resol_test_path(self):
        return SETTINGS[self._person]["MOD_RESOL_TEST_PATH"]

    def get_img_cache_path(self):
        return SETTINGS[self._person]["IMAGE_CACHE_PATH"]

    def get_dataset_persister(self):
        return SETTINGS[self._person]["DATASET_PERSISTER"]

    def get_html_conf_matrix_path(self):
        return SETTINGS[self._person]["HTML_CONF_MATRIX_PATH"]


def get_settings():
    with open(dirname(__file__) + "/whoami.json", "rt") as input:
        name = json.load(input)["name"]

    return Settings(person=name)
