import keras

from dataset_utils import reshape_image_for_cnn, convert_img_color
from service_access import get_img_by_id


class ImageScorer:

    def __init__(self, model_manager, nosig_lbl="nosig", image_cache=None):

        self._model_manager = model_manager
        self._image_cache = image_cache
        self._nosig_lbl = nosig_lbl


    def get_score(self, img_id):

        if self._image_cache is None:
            img = get_img_by_id(img_id)
        else:
            img = self._image_cache.get_image_by_id(img_id)

        return self.get_score_from_img(img)

    def get_score_from_img(self, img):

        prediction = self._model_manager.get_prediction(img)
        nosig_index = self._model_manager.get_dataset_descriptor()["labels_repr"][self._nosig_lbl]

        nosig_perc = prediction[nosig_index]
        sig_perc = 1-nosig_perc

        strange_score = 1.0 - 2*abs(sig_perc - 0.5)
        return sig_perc, strange_score
