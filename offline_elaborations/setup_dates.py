import re

from datetime import datetime

from db_facade import get_db_connection, get_image_date, get_all_images_with_names, set_image_date

conn = get_db_connection()
cursor = conn.cursor()

images = get_all_images_with_names(cursor)

for ID, name in images:
    m = re.search("(\d+-\d+-\d+_\d+_\d+_\d+\.\d+)", name)
    date_str = m.group(1)
    date = datetime.strptime(date_str, "%Y-%m-%d_%H_%M_%S.%f")
    print datetime.strftime(date, "%Y-%m-%d_%H_%M_%S.%f")
    set_image_date(ID, date, cursor)

conn.commit()
cursor.close()
conn.close()