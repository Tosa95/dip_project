<?php

    require_once dirname(__FILE__). '/../factories.php';
    require_once dirname(__FILE__). '/../db_facade.php';
    require_once dirname(__FILE__). '/../view/html_basic_elements.php';
    require_once dirname(__FILE__). '/../config.php';

    session_start();
    
    #
    #
    #       IMPORTANTE!!!!!
    #
    # Script utilizzato per gestire la form tramite AJAX
    $script = incorporate_js("comments_js.php");
    
    $columns_titles = array("Nome", "Descrizione", "Locandina", "Immagine", "Inizio", "Fine", "Rilevanza");
    $columns_weights = array(10.0, 15.0, 10.0, 10.0, 10.0, 10.0, 5.0);
    $elems_per_page = 5;
    
    $ajax_page = $SITE_ROOT . "ajax/user_comments_ajax.php";
    
    
    $content = js_list($ajax_page, $columns_titles, $columns_weights, $elems_per_page, "CommentsItemsTransformer()", "tabella_eventi", "xl", FALSE, TRUE, "CommentsDataFilterView()");

    $de = get_document_elements();
    
    echo $de->info_page_template("Gestione Eventi", $script .$content, TRUE);



