<?php

    require_once dirname(__FILE__). '/../factories.php';
    require_once dirname(__FILE__). '/../db_facade.php';
    require_once dirname(__FILE__). '/../view/html_basic_elements.php';
    require_once dirname(__FILE__). '/../config.php';
    require_once dirname(__FILE__). '/../utils.php';

    session_start();
    
    $columns_titles = array("", "Titolo", "Anno", "Nazione", "Durata", "Genere", "Regia");
    $columns_weights = array(10.0, 20.0, 10.0, 10.0, 10.0, 5.0, 20.0);
    $elems_per_page = 5;
    
    #
    #
    #       IMPORTANTE!!!!!
    #
    # Script utilizzato per gestire la form tramite AJAX
    $script = incorporate_js("films_js.php");
    
    $ajax_page = $SITE_ROOT . "ajax/all_films_ajax.php";
    
    
    $content = js_list($ajax_page, $columns_titles, $columns_weights, $elems_per_page,
            "FilmsItemsTransformer()", "tabella_film", "xl", FALSE, TRUE, "FilmDataFilterView()");
    
    $modal_body = '<div class="form-row">
                        <input type="hidden" name="action" id="action" value="insert" />
                        <div class="form-group col-12">
                            <label for="titolo">Titolo</label>
                            <input name="titolo" id="titolo" type="text" class="form-control vf" placeholder="Titolo" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-titolo vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="anno">Anno</label>
                            <input name="anno" id="anno" type="text" class="form-control vf vf-inum vf-minv-1896 vf-maxv-2099" placeholder="Anno" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-anno vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="nazione">Nazione</label>
                            <input name="nazione" id="nazione" type="text" class="form-control vf vf-words" placeholder="Nazione" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-nazione vf-validation-alert"></div>
                        </div>
                        <div class="form-group col-12">
                            <label for="durata">Durata (minuti)</label>
                            <input name="durata" id="durata" type="text" class="form-control vf vf-inum" placeholder="Durata" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-durata vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="genere">Genere</label>
                            <input name="genere" id="genere" type="text" class="form-control vf vf-words" placeholder="Genere" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-genere vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="regia">Regia</label>
                            <input name="regia" id="regia" type="text" class="form-control vf" placeholder="Regia" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-regia vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="interpreti">Interpreti</label>
                            <textarea name="interpreti" id="interpreti" class="form-control vf" placeholder="Interpreti" autocomplete="off" required></textarea>
                            <div class="alert alert-danger d-none vf-alert-interpreti vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="trama">Trama</label>
                            <textarea name="trama" id="trama" class="form-control vf" placeholder="Trama" autocomplete="off" required></textarea>
                            <div class="alert alert-danger d-none vf-alert-trama vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="urltrailer">URL Trailer</label>
                            <input name="urltrailer" id="urltrailer" type="text" class="form-control vf vf-url" placeholder="URL Trailer" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-urltrailer vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="urlrecensione">URL Recensione</label>
                            <input name="urlrecensione" id="urlrecensione" type="text" class="form-control vf vf-url" placeholder="URL Recensione" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-urlrecensione vf-validation-alert"></div>
                        </div>
                        <div class="form-group col-12">
                            <label for="img-locandina">Immagine locandina</label>
                            <input name="img-locandina" id="img-locandina" type="file" class="form-control vf vf-image" required>
                            <div class="alert alert-danger d-none vf-alert-img-locandina vf-validation-alert"></div>
                        </div>
                        <div class="form-group col-12">
                            <label for="rilevanza">Rilevanza</label>
                             <input name="rilevanza" id="rilevanza" type="text" class="form-control vf vf-inum vf-maxv-10 vf-minv-1" autocomplete="off" required>
                             <div class="alert alert-danger d-none vf-alert-rilevanza vf-validation-alert">
                                
                            </div>
                        </div>
                        
                        <div class="form-group col-12 alert alert-danger d-none alert-msg">
                            <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
                        </div>

                    </div>';
                
    $modal_footer = '<button type="submit" id="add-film" class="button button-danger vf-submit">Aggiungi</button>';
    
//    $button_add_projection = bootstrap_flexbox(
//            bootstrap_modal_button_with_form("Aggiungi Film", "button-primary", "aggiungi-film", "Aggiunta di un nuovo film", $modal_body, $modal_footer, "nuovo-film", $ajax_page, "post", 'enctype="multipart/form-data"'), 
//                       "flex-column justify-content-center align-items-center");

    $button_add_projection = add_button("aggiungi-film-btn", 'data-toggle="modal" data-target="#aggiungi-film"');
    $modal = bootstrap_modal_with_form("aggiungi-film", "Aggiunta di un nuovo film", $modal_body, $modal_footer, "nuovo-film", $ajax_page, "post", 'enctype="multipart/form-data"');
    $de = get_document_elements();
    
    echo $de->info_page_template("Gestione Film", $script . $content . $button_add_projection . $modal, TRUE);

