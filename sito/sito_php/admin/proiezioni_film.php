<?php

    require_once dirname(__FILE__). '/../factories.php';
    require_once dirname(__FILE__). '/../db_facade.php';
    require_once dirname(__FILE__). '/../view/html_basic_elements.php';
    require_once dirname(__FILE__). '/../config.php';
    require_once dirname(__FILE__). '/../utils.php';

    session_start();
    
    $dbf = get_db_facade();

    $columns_titles = array("", "Titolo", "Anno", "Nazione", "Durata", "Genere", "Regia");
    $columns_weights = array(10.0, 20.0, 10.0, 10.0, 10.0, 5.0, 20.0);
    $elems_per_page = 5;
    
    #
    #
    #       IMPORTANTE!!!!!
    #
    # Script utilizzato per gestire la form tramite AJAX
    $script = incorporate_js("proiezioni_film_js.php");
    
    $ajax_page = $SITE_ROOT . "ajax/projection_film_ajax.php";
    
    $ID_film = $_GET["ID"];    
    
    $content = js_list($ajax_page, $columns_titles, $columns_weights, $elems_per_page,
            "ProjectionItemsTransformer()", "tabella_proiezioni", "xl", FALSE, TRUE, "ProjectionDataFilterView(" . $ID_film . ")");
    
    $room_selection = get_room_selection($dbf);
    
    $price_selection = get_price_selection($dbf);
    
    $modal_body = '<div class="form-row">
                        <input type="hidden" name="action" id="action" value="insert" />
                        <input type="hidden" class="vf-proj-film" name="ID_film" id="ID_film" value="' . $ID_film . '" />
                        <div class="form-group col-12">' .
                            $room_selection .
                        '
                        <div class="alert alert-danger d-none vf-alert-sala vf-validation-alert">

                        </div>
                        </div>
                        
                        <div class="form-group col-12"> 
                            <label for="inizio">Data e ora inizio</label>
                            <input name="inizio" type="datetime-local" class="form-control vf vf-projection-date vf-proj-date" id="inizio" placeholder="Dopo" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-inizio vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12"> 
                            <label for="tipo">Tipo</label>
                            <select class="form-control" id="tipo" name="tipo">
                              <option>NORMALE</option>
                              <option>SPECIALE</option>
                            </select>
                        </div>
                        
                        <div class="form-group col-12 group-prezzo d-none"> '
                            . $price_selection . 
                            '<div class="alert alert-danger d-none vf-alert-prezzo vf-validation-alert">
                                
                            </div>
                        </div>
                       

                        <div class="form-group col-12 alert alert-danger d-none alert-msg">
                            <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
                        </div>

                    </div>';
                
    $modal_footer = '<button type="submit" id="add-film" class="button button-danger vf-submit">Aggiungi</button>';
    
    $button_add_projection = bootstrap_flexbox(
            bootstrap_modal_button_with_form("Aggiungi Proiezione", "button-primary", "aggiungi-proiezione", "Aggiunta di una nuova proiezione", $modal_body, $modal_footer, "nuova-proiezione", $ajax_page, "post", 'enctype="multipart/form-data"'), 
                       "flex-column justify-content-center align-items-center");

    $de = get_document_elements();
    
    echo $de->info_page_template("Gestione proiezioni", $script . $content . $button_add_projection, TRUE);

