<?php

    require_once dirname(__FILE__). '/../factories.php';
    require_once dirname(__FILE__). '/../db_facade.php';
    require_once dirname(__FILE__). '/../view/html_basic_elements.php';
    require_once dirname(__FILE__). '/../config.php';

    session_start();
    
    #
    #
    #       IMPORTANTE!!!!!
    #
    # Script utilizzato per gestire la form tramite AJAX
    $script = incorporate_js("events_js.php");
    
    $columns_titles = array("Nome", "Descrizione", "Locandina", "Immagine", "Inizio", "Fine", "Rilevanza");
    $columns_weights = array(10.0, 15.0, 10.0, 10.0, 10.0, 10.0, 5.0);
    $elems_per_page = 5;
    
    $ajax_page = $SITE_ROOT . "ajax/all_events_ajax.php";
    
    
    $content = js_list($ajax_page, $columns_titles, $columns_weights, $elems_per_page, "EventsItemsTransformer()", "tabella_eventi", "xl", FALSE, TRUE, "EventsDataFilterView()");
    
    $modal_body = '<div class="form-row">
                        <div class="form-group col-12">
                            <label for="nome">Nome</label>
                            <input name="nome" id="nome" type="text" class="form-control vf" placeholder="Nome" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-nome vf-validation-alert"></div>
                        </div>
                        <div class="form-group col-12">
                            <label for="descrizione">Descrizione</label>
                            <textarea name="descrizione" id="descrizione" class="form-control vf" placeholder="Descrizione" autocomplete="off" required></textarea>
                        <div class="alert alert-danger d-none vf-alert-descrizione vf-validation-alert"></div>
                        </div>
                        <div class="form-group col-12">
                            <label for="locandina">Immagine locandina</label>
                            <input name="locandina" id="locandina" type="file" class="form-control vf vf-image" required>
                            <div class="alert alert-danger d-none vf-alert-locandina vf-validation-alert"></div>
                        </div>
                        <div class="form-group col-12">
                            <label for="immagine">Immagine estesa</label>
                            <input name="immagine" id="immagine" type="file" class="form-control vf vf-image" required>
                            <div class="alert alert-danger d-none vf-alert-immagine vf-validation-alert"></div>
                        </div>
                        <div class="form-group col-12">
                            <label for="inizio">Inizio</label>
                            <input name="inizio" id="inizio" type="datetime-local" class="form-control vf vf-dt-comp-durata-lt-1" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-inizio vf-validation-alert"></div>
                        </div>
                        <div class="form-group col-12">
                            <label for="fine">Fine</label>
                            <input name="fine" id="fine" type="datetime-local" class="form-control vf vf-dt-comp-durata-lt-2" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-fine vf-validation-alert"></div>
                        </div>
                        
                        <div class="form-group col-12">
                            <label for="rilevanza">Rilevanza</label>
                            <input name="rilevanza" id="rilevanza" type="text" class="form-control vf vf-inum vf-minv-1 vf-maxv-10" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-rilevanza vf-validation-alert"></div>
                        </div>
                        <div class="form-group col-12">
                            <input name="azione" value="aggiungi" type="hidden">
                        </div>
                        
                        <div class="form-group col-12 alert alert-danger alert-msg d-none">
                            <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
                        </div>
                        
                    </div>';
                
    $modal_footer = '<button id="submit_button" type="submit" class="button button-danger vf-submit">Aggiungi</button>';
    
    $button_add_event = bootstrap_flexbox(
            bootstrap_modal_button_with_form("Aggiungi Evento", "button-primary", "aggiungi-evento", "Aggiunta di un nuovo evento", $modal_body, $modal_footer, "nuovo-evento", $ajax_page, "post", 'enctype="multipart/form-data"'), 
                       "flex-column justify-content-center align-items-center");

    $de = get_document_elements();
    
    echo $de->info_page_template("Gestione Eventi", $script .$content . $button_add_event, TRUE);


