<?php

    require_once dirname(__FILE__) . '/view/document_elements.php';
    require_once dirname(__FILE__) . '/view/html_basic_elements.php';
    require_once dirname(__FILE__) . '/utils.php';
    require_once dirname(__FILE__) . '/factories.php';
    
//    $dbf = get_db_facade();
//    
//    $original_table = $dbf->get_projection_room_status(1551);
//    
//    $original_table[5][7] = SeatStatus::PURCHASED;
//    $original_table[5][13] = SeatStatus::SELECTED;
//    $original_table[0][17] = SeatStatus::RESERVED;
//    
//    $table = array();
//    
//    for ($row = 0; $row < count($original_table); $row++)
//    {
//        $new_row = array();
//        for ($col = 0; $col < count($original_table[$row]); $col++)
//        {
//            $id = $row . "x" . $col;
//            if ($original_table[$row][$col] == SeatStatus::NOT_PRESENT)
//            {
//                $new_row[] = '';
//            } 
//            else if ($original_table[$row][$col] == SeatStatus::FREE)
//            {
//                $new_row[] = '<img class="img-fluid seat" id="'. $id .'" src="images/vectorial/free_place.png">';
//            }
//            else if ($original_table[$row][$col] == SeatStatus::BOOKED)
//            {
//                $new_row[] = '<img class="img-fluid seat" id="'. $id .'" src="images/vectorial/booked_place.png">';
//            }
//            else if ($original_table[$row][$col] == SeatStatus::PURCHASED)
//            {
//                $new_row[] = '<img class="img-fluid seat" id="'. $id .'" src="images/vectorial/purchased_place.png">';
//            }
//            else if ($original_table[$row][$col] == SeatStatus::SELECTED)
//            {
//                $new_row[] = '<img class="img-fluid seat" id="'. $id .'" src="images/vectorial/selected_place.png">';
//            }
//            else if ($original_table[$row][$col] == SeatStatus::RESERVED)
//            {
//                $new_row[] = '<img class="img-fluid seat" id="'. $id .'" src="images/vectorial/reserved_place.png">';
//            }
//            
//        }
//        $table[] = $new_row;
//    }
//    
//    $info = table($table);
//    
//    $info .= '<script type="text/javascript">'
//            . '$( document ).ready(function() {
//                    var matrix = ' . to_javascript_matrix($original_table) . ';
//                    $(".seat").click(function() {
//                        var id = $(this).attr("id");
//                        var row = id.split("x")[0];
//                        var col = id.split("x")[1];
//                        if (matrix[row][col] == '. SeatStatus::FREE .')
//                        {
//                            $(this).attr("src","images/vectorial/selected_place.png");
//                            matrix[row][col] = '. SeatStatus::SELECTED .';
//                        }
//                        else if (matrix[row][col] == '. SeatStatus::SELECTED .')
//                        {
//                            $(this).attr("src","images/vectorial/free_place.png");
//                            matrix[row][col] = '. SeatStatus::FREE .';
//                        }
//                    });
//               });</script>';
    $dbf = get_db_facade();
    
//    function get_icon_button($icon, $color)
//    {
//        global $SITE_ROOT;
//        
//        $icon_path = $SITE_ROOT . "/images/vectorial/" . $icon;
//        
//        return '<div class="button button-circular d-inline-flex justify-content-center align-items-center" style="background-color:'.$color.';"> <img class="img-fluid" src="'. $icon_path .'"/> </div>';
//    }
    
//    function get_text_icon_button($text, $icon, $color)
//    {
//        global $SITE_ROOT;
//        
//        $icon_path = $SITE_ROOT . "/images/vectorial/" . $icon;
//        
//        return '<div class="button button-text-icon d-inline-flex justify-content-center align-items-center" style="background-color:'.$color.';"> '
//                . '<img class="img-fluid" style="height:50%;margin-left:10px;" src="' . $icon_path . '"/>'
//                . '<div class="button-text-separator"></div> '
//                . '<div class="button-text-container">' . $text . '</div> '
//                . '</div>';
//    }
    
print_r($dbf->get_comments());

    echo get_document_elements()->info_page_template("Shee(i)t",
            '<div class="button-on-top"><div class="button button-circular-big '
            . 'd-inline-flex justify-content-center align-items-center"><div class="d-inline-block align-middle">+</div></div></div>' . '<div class="d-flex">' . get_text_icon_button("CANCELLA", "delete.png", $DELETE_COLOR) . 
            bootstrap_text_icon_button_for_modal(ButtonTypes::DELETE, "myModal") . 
            get_text_icon_button("ACQUISTA", "purchase.png", $PURCHASE_COLOR) . "</div>" .
            '<div class="d-flex">' . get_icon_button("delete.png", $DELETE_COLOR) . 
            get_icon_button("modify.png", $MODIFY_COLOR) . 
            get_icon_button("purchase.png", $PURCHASE_COLOR) . "</div>");

