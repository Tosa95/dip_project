<?php

    require_once dirname(__FILE__) . '/config.php';
    require_once dirname(__FILE__) . '/utils.php';
    require_once dirname(__FILE__) . '/crypto.php';
    require_once dirname(__FILE__) . '/view/document_elements.php';
    require_once dirname(__FILE__) . '/factories.php';
    require_once dirname(__FILE__) . '/mails.php';
    
    session_start();
    
    $de = get_document_elements();

    # Se non stiamo accedendo qua dal link email e non abbiamo ancora cliccato su continua 
    # (ossia siamo al passo appena
    # successivo a quando l'utente ha cliccato su password dimenticata)
    if (!isset($_GET["key"]) && !isset($_GET["sendmail"]) && !isset($_POST["key"]))
    {
        echo $de->info_page_template("Password dimenticata", 
                "<p>Capita a tutti di dimenticare le password, non ti preoccupare!</p>" .
                "<p>La procedura per reimpostarla è molto semplice. Dicci il tuo username e clicca su continua" .
                " e ti verrà mandata una mail all'indirizzo utilizzato in fase di registrazione." .
                " Una volta arrivata aprila e segui le istruzioni.</p>" .
                '<form id="login_form" class="needs-validation" action="password_dimenticata.php" method="get">
                    <div class="form-row">
                            <div class="form-group col-12">
                                <label for="username">Nome utente</label>
                                <input name="username" id="username" type="text" class="form-control" id="username" placeholder="Nome utente" required>
                            </div>
                    </div>
                    <button id="submit_button" type="submit" name="sendmail" value="sendmail" class="button button-primary">Continua</button>
                </form>', TRUE);
    }
    else if (isset($_GET["sendmail"])) # Se abbiamo cliccato su continua e dobbiamo quindi inviare la mail
    {
        $pswu = get_password_utils();
        
        $key = get_password_utils()->get_password_reset_key();
        $reset_url = $SITE_ADDRESS . $SITE_ROOT . "password_dimenticata.php?key=" . $key;
        
        $user_mail = get_db_facade()->get_user_email($_GET["username"]);
        
        if (!is_null($user_mail))
        {
            send_password_change_email($reset_url, $user_mail);
            get_db_facade()->add_password_reset_key($key, $_GET["username"]);
            echo $de->info_page_template("Password dimenticata", "Abbiamo inviato la mail. Controlla la tua casella di posta e segui le istruzioni", TRUE);
        }
        else
        {
            echo $de->info_page_template("Password dimenticata", "Siamo spiacenti, l'username indicato non esiste.", TRUE);
        }
    }
    else if (isset($_GET["key"])) # Se abbiamo aperto il link della mail
    {
        $key = $_GET["key"];
        $username = get_db_facade()->get_password_reset_key_username($key);
        
        if ($username != NULL)
        {
            $head = "<p>Ciao " . $username . ",</p>" . "<p>Inserisci ora la nuova password:</p>";
            $form = '<script src="js/registration_validation.php"></script>
                    <form id="registration_form" class="needs-validation" action="password_dimenticata.php" method="post">
                        <input type="hidden" name="key" value="' . $key . '">
                        <input type="hidden" name="username" value="' . $username . '">
                        <div class="form-row">
                            <div class="form-group col-12">
                                <label for="password">Password</label>
                                <input name="password" type="password" class="form-control vf vf-password vf-eq-password vf-reset-on-error" id="password" placeholder="Password" required>
                                <div class="alert alert-danger d-none vf-alert-password vf-validation-alert"></div>
                            </div>
                            <div class="form-group col-12">
                                <label for="password_check">Conferma password</label>
                                <input type="password" class="form-control vf vf-eq-password vf-reset-on-error" id="password_check" placeholder="Conferma password" required>
                                <div class="alert alert-danger d-none vf-alert-password_check vf-validation-alert"></div>
                            </div>
                        </div>
                        <button id="submit_button" type="submit" name="reset" value="reset" class="button button-primary vf-submit">Reimposta Password</button>
                    </form>';
            
            echo $de->info_page_template("Password dimenticata", $head . $form, TRUE);
        }
        else
        {
            echo $de->info_page_template("Password dimenticata", "Siamo spiacenti, la chiave non è più valida.", TRUE);
        }
    }
    else if (isset($_POST["key"])) # L'utente ha già inserito la nuova password
    {
        get_db_facade()->set_key_as_used($_POST["key"]);
        $pass_utils = new PasswordUtils($HASH_ALGORITHM_REPETITIONS, $HASH_ALGORITHM, $SALT_LENGTH);
        $salt = $pass_utils->get_random_salt();
        $hash = $pass_utils->hash_password($_POST["password"], $salt);
        get_db_facade()->set_user_password($_POST["username"], $hash, $salt);
        header('Location: login.php');
    }

