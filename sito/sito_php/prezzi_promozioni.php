<?php

    require_once dirname(__FILE__) . '/view/document_elements.php';
    require_once dirname(__FILE__) . '/view/price_view.php';
    require_once dirname(__FILE__). '/factories.php';
    require_once dirname(__FILE__). '/db_facade.php';
    
    session_start();
    
    $de = get_document_elements();
    $dbf = get_db_facade();
    
    $price_ranges = $dbf->get_all_prices();
    
    $info = present_price_ranges($price_ranges, $ALONE_PRICE_ALIGN);
    
    echo $de->info_page_template("Prezzi e promozioni", $info, TRUE);

