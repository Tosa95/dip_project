<?php

    require_once dirname(__FILE__). '/factories.php';
    require_once dirname(__FILE__). '/config.php';
    
    session_start();
    
    if (isset($_SESSION["username"]))
    {
        $dbf = get_db_facade();
        
        $utype = $dbf->get_user_type($_SESSION["username"]);
        
        $de = get_document_elements();
        
        if ($utype >= 2)
        {
        
            $plp = get_projection_list_presenter();
            
            $script = incorporate_js("classifier2_js.php") . incorporate_js("images_js.php");

            $content = '<div class="d-flex align-items-center justify-content-center"><img id="current" class="img-fluid" style="width:400px"/></div>';

            $content .= '<div class="container mt-5" id="images-cont"></div>';
            
            $content .= '<div class="d-flex align-items-center justify-content-center"><div id="next_button" class="button button-primary d-inline-block mt-5">NEXT</div></div>';
            
            $content .= '<div class="container mt-5" id="stats"></div>';

            echo $de->info_page_template("Classifier2", $content . $script, TRUE);
        }
        else
        {
            echo $de->info_page_template("Classifier2", "Non hai i privilegi per classificare le immagini", TRUE);
        }
    }
    else
    {
        header('Location: login.php');
    }

