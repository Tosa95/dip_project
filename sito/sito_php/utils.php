<?php

    require_once dirname(__FILE__) . '/config.php';
    
    /**
     * Ritorna la versione Javascript di un array di regex php (come stringa).
     * 
     * @param $php_array Array da convertire
     * @return string regex
     */
    function to_regex_js_array($php_array)
    {
        $res = "[";

                    for ($i = 0; $i < count($php_array); $i++)
                    {
                        $res .= "/" . $php_array[$i] . "/";

                        if ($i < count($php_array)-1)
                        {
                            $res .= ",";
                        }
                    }

        $res .= "]";
                    
        return $res;
    }
    
    function to_javascript_matrix($matrix)
    {
        return json_encode($matrix);
    }
    
    function date_to_day_name_number_month_time($date)
    {
        $days = array("Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato");
        
        $res = array(

            "nome_giorno" => $days[(int)date('w', strtotime($date))],
            "num_giorno" =>  date('d', strtotime($date)),
            "num_mese" => date('m', strtotime($date)),
            "orario" => date('H:i', strtotime($date))

        );

        return $res;
                
    }
    
    /**
     * Funzione che a partire dal codice del giorno, ne restituisce il nome.
     * 
     * @param type $day_code Codice del giorno (0: domenica, ...)
     * @return string Nome del giorno corrispondente
     */
    function day_code_to_day_name($day_code)
    {
        $days = array("Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato");
        
        if($day_code < count($days))
        {
           return $days[$day_code]; 
        }
        else
        {
            return NULL;
        }
        
    }
    
    function user_input_to_db($input)
    {
        # return str_replace("\n", "<br>", htmlentities($input));
        return iconv(mb_detect_encoding($input), "UTF-8", $input);
    }
    
    function old_db_to_raw_string($html_string)
    {
        return html_entity_decode(html_entity_decode(str_replace("<br>", "\n", $html_string)));
    }
    
    function db_to_html($raw_string)
    {
        return str_replace("\n", "<br>", htmlspecialchars($raw_string));
    }
    
    function db_to_field_value($raw_string)
    {
        return htmlspecialchars($raw_string);
    }
    
    function db_to_string($db_data)
    {
//        return html_entity_decode(str_replace("<br>", "\n", $db_data));
        return str_replace("<br>", "\n", $db_data);

    }
    
    function clean_strange_chars($str)
    {
        return preg_replace("/[^a-zA-Z0-9]+/", "", $str);
    }
    
    function incorporate_js($js_script_name)
    {
        global  $SITE_ROOT;
        return '<script type="text/javascript" src="'. $SITE_ROOT . "js/" . $js_script_name . '"></script>';
    }
    
    function utf8ize($d) {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = utf8ize($v);
            }
        } else if (is_string ($d)) {
            return utf8_encode($d);
        }
        return $d;
    }
    
    function get_room_selection($dbf, $selected_one=null)
    {
        $rooms = $dbf->get_rooms();
        
        $room_selection_options = "";
    
        foreach ($rooms as $room)
        {
            if ($selected_one === null || $room["ID"] !== $selected_one)
            {
                $room_selection_options .= '<option value="' . $room["ID"] . '">' . $room["nome"] . '</option>';
            }
            else
            {
                $room_selection_options .= '<option value="' . $room["ID"] . '" selected="selected">' . $room["nome"] . '</option>';
            }
        }

        $room_selection = ' <label for="sala">Sala:</label>
                            <select class="form-control vf vf-proj-room" id="sala" name="sala">' .
                              $room_selection_options .
                            '</select>';
        
        return $room_selection;
    }
    
    function get_price_selection($dbf, $selected_one=null)
    {
        $prices = $dbf->get_special_prices();
        
        $price_selection_options = '';
    
        foreach ($prices as $price)
        {
            if ($selected_one === null || $price["ID"] !== $selected_one)
            {
                $price_selection_options .= '<option value="' . $price["ID"] . '">' . $price["nome"] . ' (' . number_format($price["valore"],2) . htmlentities('€') . ')</option>';
            }
            else
            {
                $price_selection_options .= '<option value="' . $price["ID"] . '" selected="selected">' . $price["nome"] . ' (' . number_format($price["valore"],2) . htmlentities('€') . ')</option>';
            }
                
        }

        $price_selection = ' <label for="prezzo">Fascia di prezzo:</label>
                            <select class="form-control vf" id="prezzo" name="prezzo">' .
                              $price_selection_options .
                            '</select>';
        
        return $price_selection;
    }
    
    
    
