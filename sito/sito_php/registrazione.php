<?php

    require_once dirname(__FILE__) . '/config.php';
    require_once dirname(__FILE__) . '/utils.php';
    require_once dirname(__FILE__) . '/db_facade.php';
    require_once dirname(__FILE__) . '/crypto.php';
    require_once dirname(__FILE__) . '/view/document_elements.php';
    require_once dirname(__FILE__). '/factories.php';

    session_start();
    
    $de = get_document_elements();
    
    # Se è stato cliccato il bottone di registrazione
    if (isset($_POST["username"]))
    {
        $dbf = get_db_facade();
        
        # Controllo se l'username scelto è già presente
        if ($dbf->user_present($_POST["username"]))
        {
            # Nota: qua è impossibile arrivarci
            echo $de->info_page_template("Registrazione", bootstrap_flexbox("<span><h2>Nome utente già presente</h2></span>" . '<span><a href="' . $SITE_ROOT . 'registrazione.php"><button class="button button-primary">Torna alla registrazione</button></a></span>', "flex-column"));
        }
        else 
        {
            $pass_utils = get_password_utils();
            $salt = $pass_utils->get_random_salt();
            $hash = $pass_utils->hash_password($_POST["password"], $salt);

            $success = $dbf->add_user(user_input_to_db($_POST["username"]), $hash, $salt, UserType::BASIC, user_input_to_db($_POST["nome"]), 
                    user_input_to_db($_POST["cognome"]), user_input_to_db($_POST["email"]), isset($_POST["telefono"])?user_input_to_db($_POST["telefono"]):null);

            if ($success)
            {
                $_SESSION["username"] = user_input_to_db($_POST["username"]);
                header('Location: index.php');
            }
            else
            {
                # Nota: qua è impossibile arrivarci
                echo $de->info_page_template("Registrazione", bootstrap_flexbox("<span><h2>Nome utente già presente</h2></span>" . '<span><a href="' . $SITE_ROOT . 'registrazione.php"><button class="button button-primary">Torna alla registrazione</button></a></span>', "flex-column"));
            }
        }
    }
    else # Se non è stato ancora cliccato il bottone di registrazione
    {
        
        $form = '<form id="registration_form" class="needs-validation" action="registrazione.php" method="post">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="username">Nome utente</label>
                            <input name="username" id="username" type="text" class="form-control vf vf-username vf-username-present" id="username" placeholder="Nome utente" required>
                            <div class="alert alert-danger d-none vf-alert-username vf-validation-alert"></div>  
                        </div>
                        <div class="form-group col-md-3">
                            <label for="password">Password</label>
                            <input name="password" type="password" class="form-control vf vf-password vf-eq-password vf-reset-on-error" id="password" placeholder="Password" required>
                            <div class="alert alert-danger d-none vf-alert-password vf-validation-alert"></div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="password_check">Conferma password</label>
                            <input type="password" class="form-control vf vf-eq-password vf-reset-on-error" id="password_check" placeholder="Conferma password" required>
                            <div class="alert alert-danger d-none vf-alert-password_check vf-validation-alert"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="nome">Nome</label>
                            <input name="nome" id="nome" type="text" class="form-control vf vf-words" id="nome" placeholder="Nome" required>
                            <div class="alert alert-danger d-none vf-alert-nome vf-validation-alert"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="cognome">Cognome</label>
                            <input name="cognome" id="cognome" type="text" class="form-control vf vf-words" id="cognome" placeholder="Cognome" required>
                            <div class="alert alert-danger d-none vf-alert-cognome vf-validation-alert"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="email">Email</label>
                            <input name="email" type="email" class="form-control vf vf-email vf-eq-email" id="email" placeholder="nome@dominio.boh" required>
                            <div class="alert alert-danger d-none vf-alert-email vf-validation-alert"></div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="email">Conferma email</label>
                            <input type="email" class="form-control vf vf-eq-email" id="email-check" placeholder="nome@dominio.boh" required>
                            <div class="alert alert-danger d-none vf-alert-email-check vf-validation-alert"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="telefono">Numero di telefono</label>
                            <input name="telefono" type="text" class="form-control vf vf-phone" id="telefono" placeholder="1234 5678910">
                            <div class="alert alert-danger d-none vf-alert-telefono vf-validation-alert"></div>
                        </div>
                    </div>
                    <button id="submit_button" type="submit" class="button button-primary vf-submit">Registrati</button>
                </form>';

        echo $de->info_page_template("Registrazione", $form, TRUE);
    }

