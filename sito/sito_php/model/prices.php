<?php

    class Price
    {
        function __construct($name, $price, $description=NULL) 
        {
            $this->name = $name;
            $this->price = $price;
            $this->description = $description;
        }
        
        function get_price()
        {
            return $this->price;
        }
        
        function get_name()
        {
            return $this->name;
        }
        
        function get_description()
        {
            return $this->description;
        }
    }
    
    class PriceRange
    {
        function __construct($day_name, $range_name, $prices, $description=NULL) 
        {
            $this->day_name = $day_name;
            $this->range_name = $range_name;
            $this->prices = $prices;
            $this->description = $description;
        }
        
        function get_day_name()
        {
            return $this->day_name;
        }
        
        function get_range_name()
        {
            return $this->range_name;
        }
        
        function get_prices()
        {
            return $this->prices;
        }
        
        function get_description()
        {
            return $this->description;
        }
    }

