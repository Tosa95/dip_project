<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    require_once dirname(__FILE__) . '/view/document_elements.php';
    require_once dirname(__FILE__) . '/view/html_basic_elements.php';
    require_once dirname(__FILE__) . '/view/price_view.php';
    require_once dirname(__FILE__) . '/utils.php';
    require_once dirname(__FILE__) . '/factories.php';
    require_once dirname(__FILE__) . '/mails.php';
 
$dbf = get_db_facade();

# print_r($dbf->get_incompatible_projections(85, "2018-06-28 05:00:00", 507));
# print_r(urldecode(json_encode(array("ID_sala"=>85,"data_ora"=>"2018-06-28 05:00:00","ID_film"=>507))));
    
$form = '
                <form id="registration_form" class="needs-validation" action="registrazione.php" method="post">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="username">Nome utente</label>
                            <input name="username" id="username" type="text" class="form-control vf  vf-username vf-username-present" id="username" placeholder="Nome utente" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-username vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="password">Password</label>
                            <input name="password" type="password" class="form-control vf vf-eq-password vf-password" id="password" placeholder="Password" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-password vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="password_check">Conferma password</label>
                            <input type="password" class="form-control vf vf-eq-password" id="password_check" placeholder="Conferma password" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-password_check vf-validation-alert">
                                
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="nome">Nome</label>
                            <input name="nome" id="nome" type="text" class="form-control vf vf-func-somma_max_30-b-2" id="nome" placeholder="Nome" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-nome vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="cognome">Cognome</label>
                            <input name="cognome" id="cognome" type="text" class="form-control vf vf-func-somma_max_30-b-1" id="cognome" placeholder="Cognome" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-cognome vf-validation-alert">
                                
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input name="email" type="email" class="form-control vf vf-email" id="email" placeholder="nome@dominio.boh" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-email vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="telefono">Numero di telefono</label>
                            <input name="telefono" type="text" class="form-control vf vf-phone" id="telefono" placeholder="1234 5678910" autocomplete="off">
                            <div class="alert alert-danger d-none vf-alert-telefono vf-validation-alert">
                                
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="prima">Data prima</label>
                            <input name="prima" type="date" class="form-control vf vf-dt-comp-a-lt-1" id="prima" placeholder="Prima" autocomplete="off" required/>
                            <div class="alert alert-danger d-none vf-alert-prima vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="dopo">Data dopo</label>
                            <input name="dopo" type="date" class="form-control vf vf-dt-comp-a-lt-2" id="dopo" placeholder="Dopo" autocomplete="off">
                            <div class="alert alert-danger d-none vf-alert-dopo vf-validation-alert">
                                
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="ccard">Carta di credito</label>
                            <input name="ccard" type="text" class="form-control vf vf-credit" id="ccard" placeholder="Carta di credito" autocomplete="off" required/>
                            <div class="alert alert-danger d-none vf-alert-ccard vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="iban">Codice IBAN</label>
                            <input name="iban" type="text" class="form-control vf vf-iban" id="iban" placeholder="IBAN" autocomplete="off">
                            <div class="alert alert-danger d-none vf-alert-iban vf-validation-alert">
                                
                            </div>
                        </div>
                    </div>
                    <button id="submit_button" type="submit" class="button button-primary vf-submit">Registrati</button>
                </form>';
    
    $script = incorporate_js("payment_js.php");
    
    $films = $dbf->get_all_films(FALSE, "è", "", "");
    print_r($films);
    
    echo get_document_elements()->info_page_template("Shee(i)t 2: la vendetta", 
                $form . $script . info_button("In italia ogni giorno nuove persone diventano povere. Dato che ormai non sono più una minoranza, abbiamo una tariffa anche per loro. Ricordati di portare la certificazione ISEE per dimostrare il tuo stato di povertà.")
            );  

