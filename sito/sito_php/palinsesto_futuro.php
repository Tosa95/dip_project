<?php
    
    require_once dirname(__FILE__). '/factories.php';
    
    session_start();
    
    $plp = get_projection_list_presenter();
    $de = get_document_elements();
    
    # Nel palinsesto futuro ci saranno le proiezioni future
    $future = TRUE;
    
    $content = $plp->projections_list($future);

    echo $de->basic_page_template($content, TRUE, TRUE);