<?php
    
    require_once dirname(__FILE__). '/../factories.php';
    require_once dirname(__FILE__). '/../db_facade.php';
    require_once dirname(__FILE__). '/../view/html_basic_elements.php';
    require_once dirname(__FILE__). '/../config.php';
   
    
    session_start();
    
    $script = incorporate_js("mie_proiezioni_js.php");
    
    $dbf = get_db_facade();
    
    $content = '';
    
    if (isset($_SESSION["username"]))
    {
        $user_projections = $dbf->get_user_projections($_SESSION["username"]);

        $content = js_list($SITE_ROOT . "ajax/mie_proiezioni_ajax.php", 
                array("", "Titolo", "Sala", "Data proiezione", "Data prenotazione", "Costo", "Posti", ""),
                array(10.0, 20.0, 10.0, 10.0, 10.0, 5.0, 20.0, 10.0), 5,
                "MieProiezioniItemsTransformer()", "tabella_proiezioni", "xl", FALSE);
        
    }
    
    
    $de = get_document_elements();
    
    echo $de->info_page_template("Le mie Proiezioni", $script . $content, TRUE);
    