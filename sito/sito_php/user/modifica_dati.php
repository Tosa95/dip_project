<?php

    require_once dirname(__FILE__) . '/../config.php';
    require_once dirname(__FILE__) . '/../utils.php';
    require_once dirname(__FILE__) . '/../db_facade.php';
    require_once dirname(__FILE__) . '/../crypto.php';
    require_once dirname(__FILE__) . '/../view/document_elements.php';
    require_once dirname(__FILE__). '/../factories.php';

    session_start();
    
    $de = get_document_elements();
    $dbf = get_db_facade();
    
    # Se è stato cliccato il bottone di modifica
    if (isset($_POST["username"]))
    {
        $username = $_SESSION["username"];
        
        # Controllo se l'username scelto è già presente
        $new_name = $_POST["nome"];
        $new_surname = $_POST["cognome"];
        $new_email = $_POST["email"];
        $new_phone = $_POST["telefono"];
        
        $dbf->set_user_data($username, user_input_to_db($new_name), user_input_to_db($new_surname),
                user_input_to_db($new_email), user_input_to_db($new_phone));
        
        if (isset($_POST["password_modified"]))
        {
            $pass_utils = get_password_utils();
            $salt = $pass_utils->get_random_salt();
            $hash = $pass_utils->hash_password($_POST["password"], $salt);
            $dbf->set_user_password($_POST["username"], $hash, $salt);
        }
        
        header('Location: ' . $SITE_ROOT . 'index.php');
    }
    else # Se non è stato ancora cliccato il bottone di modifica
    {
        
        $username = $_SESSION["username"];
        
        $user_data = $dbf->get_user_data($username);
        
        $modify_modal_body = "Sei sicuro di voler modificare i dati?";
        $modify_modal_footer = '<button type="submit" name="confirm-modify" id="confirm-modify" class="button button-danger">Conferma modifiche</button>' .
                                '<button type="button" class="button button-neutral" data-dismiss="modal">Annulla</button>';
        $modify_modal = bootstrap_modal("conferma-modifica-dati", "Conferma modifica dati", $modify_modal_body, $modify_modal_footer);
        
        $script = incorporate_js("modify_data_js.php");
        
        $form = '
                <form id="registration_form" class="needs-validation" action="modifica_dati.php" method="post">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="username">Nome utente</label>
                            <input value="' . $username . '" name="username" id="username" type="text" class="form-control" id="username" placeholder="Nome utente" required readonly>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="password">Password</label>
                            <input value="Ciao1" name="password" type="password" class="form-control select-on-click vf vf-password vf-eq-password vf-reset-on-error" id="password" placeholder="Password" required>
                            <div class="alert alert-danger d-none vf-alert-password vf-validation-alert"></div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="password_check">Conferma password</label>
                            <input value="Ciao1" type="password" class="form-control select-on-click vf vf-eq-password vf-reset-on-error" id="password_check" placeholder="Conferma password" required>
                            <div class="alert alert-danger d-none vf-alert-password_check vf-validation-alert"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="nome">Nome</label>
                            <input value="' . $user_data["nome"] . '" name="nome" id="nome" type="text" class="form-control vf vf-words" id="nome" placeholder="Nome" required>
                            <div class="alert alert-danger d-none vf-alert-nome vf-validation-alert"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="cognome">Cognome</label>
                            <input value="' . $user_data["cognome"] . '" name="cognome" id="cognome" type="text" class="form-control vf vf-words" id="cognome" placeholder="Cognome" required>
                            <div class="alert alert-danger d-none vf-alert-cognome vf-validation-alert"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="email">Email</label>
                            <input value="' . $user_data["email"] . '" name="email" type="email" class="form-control vf vf-email vf-eq-email" id="email" placeholder="nome@dominio.boh" required>
                            <div class="alert alert-danger d-none vf-alert-email vf-validation-alert"></div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="email">Conferma email</label>
                            <input value="' . $user_data["email"] . '" type="email" class="form-control vf vf-eq-email" id="email-check" placeholder="nome@dominio.boh" required>
                            <div class="alert alert-danger d-none vf-alert-email-check vf-validation-alert"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="telefono">Numero di telefono</label>
                            <input value="' . $user_data["telefono"] . '" name="telefono" type="text" class="form-control vf vf-phone" id="telefono" placeholder="1234 5678910">
                            <div class="alert alert-danger d-none vf-alert-telefono vf-validation-alert"></div>
                        </div>
                    </div>
                    <button type="submit" name="modify" id="modify" class="button button-primary vf-submit">Modifica</button>
                        ' . $modify_modal . ' 
                </form>';

        echo $de->info_page_template("Modifica dati account utente", $script . $form, TRUE);
    }

