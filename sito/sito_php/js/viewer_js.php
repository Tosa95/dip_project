//<script type="text/javascript">

<?php require_once dirname(__FILE__). '/../config.php' ?>;

var labels_manager = null;
var preloader = null;

function visualize_images(label_type, label_value)
{
    $("#images-cont").empty().append("Caricamento...");
    $.ajax({
        // URL verso la quale inviare la richiesta
        url: "/segnali/ajax/images_ajax.php?operation=get_label_images&&label_type=" + label_type + "&label_value=" + label_value,
        // Dati da inviare al server
        method: "GET",
        dataType: 'json',
        type: "GET", // For jQuery < 1.9
        // Funzione di callback una volta ricevuta la risposta 
        success: function(dt)
        {
            images_viewer = new ImagesViewer($("#images-cont"), dt["ids"]);
            images_viewer.show();
        },
        error: function(err)
        {
            console.log(err);
        }
    });
}

function visualize_images_predicted(label_type, label_value)
{
    $("#images-cont").empty().append("Caricamento...");
    $.ajax({
        // URL verso la quale inviare la richiesta
        url: "/segnali/ajax/images_ajax.php?operation=get_label_images_predicted&&label_type=" + label_type + "&label_value=" + label_value,
        // Dati da inviare al server
        method: "GET",
        dataType: 'json',
        type: "GET", // For jQuery < 1.9
        // Funzione di callback una volta ricevuta la risposta 
        success: function(dt)
        {
            images_viewer = new ImagesViewer($("#images-cont"), dt["ids"], true);
            images_viewer.show();
        },
        error: function(err)
        {
            console.log(err);
        }
    });
}

function get_labels_tree()
{
    $.ajax({
        // URL verso la quale inviare la richiesta
        url: "/segnali/ajax/images_ajax.php?operation=get_label_type_tree&label_type=1",
        // Dati da inviare al server
        method: "GET",
        dataType: 'json',
        type: "GET", // For jQuery < 1.9
        // Funzione di callback una volta ricevuta la risposta 
        success: function(dt)
        {
            var container = $("#categories");
            var preload_container = $("#img-preload");
//            console.log(preload_container.length);
            
            labels_manager = new LabelsManager(container, dt["tree"], $("#current"), 
            $("#current-thumbnail"), null, true, true, true, function(value_data){
                
                if (value_data["image"] !== null)
                {
                    $("#current-thumbnail-cont").removeClass("d-none");
                    $("#current-thumbnail-cont").addClass("d-flex");
                    $("#current-thumbnail").attr("src", "<?php echo $ICON_FOLDER?>" + value_data["image"]);
                } else {
                    $("#current-thumbnail-cont").removeClass("d-flex");
                    $("#current-thumbnail-cont").addClass("d-none");
                }
            });
            
            $("#view_button").click(function() {
                last_sel = labels_manager.get_last_selected();
                visualize_images(last_sel[0], last_sel[1]);
            });
            
            $("#predicted_button").click(function() {
                last_sel = labels_manager.get_last_selected();
                visualize_images_predicted(last_sel[0], last_sel[1]);
            });
            
            
            
//            console.log(dt["tree"]);
        },
        error: function(err)
        {
            console.log(err);
        }
    });
}

$( document ).ready(function() {

    get_labels_tree();
    
    let stats_update_delay = 10000;
    
    function update_stats()
    {
        $.ajax({
            // URL verso la quale inviare la richiesta
            url: "/segnali/ajax/images_ajax.php?operation=get_stats",
            // Dati da inviare al server
            method: "GET",
            dataType: 'json',
            type: "GET", // For jQuery < 1.9
            // Funzione di callback una volta ricevuta la risposta 
            success: function(dt)
            {
                let stats = dt["stats"];
                
                
                let txt = "<br/><br/>";
                
                Object.keys(stats).forEach(function(key) {
                    value = stats[key];
                    txt += key + ": " + value.toFixed(3) + "<br/>";
                });
                
                $("#stats").html(txt);
                window.setTimeout(update_stats, stats_update_delay);

            },
            error: function(err)
            {
                console.log(err);
                window.setTimeout(update_stats, stats_update_delay);
            }
        });
    }
    
    window.setTimeout(update_stats, stats_update_delay);
    
});


