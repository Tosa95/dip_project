//<script type="text/javascript">

<?php require_once dirname(__FILE__). '/../config.php' ?>;

var labels_manager = null;
var preloader = null;

function get_labels_tree()
{
    $.ajax({
        // URL verso la quale inviare la richiesta
        url: "/segnali/ajax/images_ajax.php?operation=get_label_type_tree&label_type=1",
        // Dati da inviare al server
        method: "GET",
        dataType: 'json',
        type: "GET", // For jQuery < 1.9
        // Funzione di callback una volta ricevuta la risposta 
        success: function(dt)
        {
            var container = $("#categories");
            var preload_container = $("#img-preload");
//            console.log(preload_container.length);
            
            preloader = new Preloader(7, preload_container);
            labels_manager = new LabelsManager(container, dt["tree"], $("#current"), 
            $("#current-thumbnail"), preloader);
            
//            console.log(dt["tree"]);
        },
        error: function(err)
        {
            console.log(err);
        }
    });
}

$( document ).ready(function() {

    get_labels_tree();
    
    let stats_update_delay = 10000;
    
    function update_stats()
    {
        $.ajax({
            // URL verso la quale inviare la richiesta
            url: "/segnali/ajax/images_ajax.php?operation=get_stats",
            // Dati da inviare al server
            method: "GET",
            dataType: 'json',
            type: "GET", // For jQuery < 1.9
            // Funzione di callback una volta ricevuta la risposta 
            success: function(dt)
            {
                let stats = dt["stats"];
                
                
                let txt = "<br/><br/>";
                
                Object.keys(stats).forEach(function(key) {
                    value = stats[key];
                    txt += key + ": " + value.toFixed(3) + "<br/>";
                });
                
                $("#stats").html(txt);
                window.setTimeout(update_stats, stats_update_delay);

            },
            error: function(err)
            {
                console.log(err);
                window.setTimeout(update_stats, stats_update_delay);
            }
        });
    }
    
    window.setTimeout(update_stats, stats_update_delay);
    
});


