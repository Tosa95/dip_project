//<script type="text/javascript">

<?php

    header("Content-type:text/javascript");
    require_once dirname(__FILE__) . '/../config.php';
    require_once dirname(__FILE__) . '/../utils.php';
    require_once dirname(__FILE__) . '/../db_facade.php';
    
?>
    
    function visualize_prices_if_needed()
    {
        console.log($(this).val());
        if ($(this).val()==="SPECIALE")
        {
            console.log($(this).closest("form").find(".group-prezzo").length);
            
            $(this).closest("form").find(".group-prezzo").fadeIn();
            $(this).closest("form").find("#prezzo").attr("required",1);
        }
        else
        {
            $(this).closest("form").find(".group-prezzo").fadeOut();
            $(this).closest("form").find("#prezzo").removeAttr("required");
        }
    }
    
   /**
    * Oggetto che si occupa di rappresentare un filtro per i dati (classe astratta,
    * derivare da questa per definire nuovi tipi di visualizzatori di filtri)
    * @type type
    */
   class ProjectionDataFilterView extends DataFilterView
   {
       /**
        * 
        * @param {type} ID L'id del film da visualizzare
        */
       constructor(ID)
       {
           super();
           this.ID = ID;
       }

       get_filter_callback()
       {
           var self = this;
           
           return function()
           {
                var only_future_checkbox = $("#only_future");
                var begin = $("#begin");
                var end = $("#end");
                var date_form = $("#date_form");
                
                // Se è stato il cambiamento della checkbox a scaturire l'evento, 
                // resetto le date. Questo per fare in modo che non si possano impostare
                // contemporaneamente i filtri sulle date ed il filtro "proiezioni future"
                if($(this).attr("id") === "only_future")
                {
                    date_form[0].reset();
                    re_validate_all([date_form.find("#begin")[0], date_form.find("#end")[0]]);
                }
                // Se sono state le date, tolgo la spunta dalla checkbox
                else if($(this).attr("id") === "date_form")
                {
                   only_future_checkbox.prop("checked", false);
                }
                
                var only_future = false;
                if (only_future_checkbox.is(":checked"))
                {
                    only_future = true;
                }
                
                var filter = {"ID": self.ID, "only_future": only_future, "begin": begin.val(), "end": end.val()};
                
                if (self.callback !== null)
                {
                    self.callback(filter);
                }
               
           };
       }

       get_filter_view()
       {
           var filter_container = $('<div class="container-fluid"></div>');
           
           // Mando già come filtro quello contenente il solo ID: voglio visualizzare
           // solo le proiezioni relative al film selezionato!!!
           var filter = {"ID": this.ID, "only_future": true};
           
           this.callback(filter);
           
           var only_future_checkbox = $('<div class="form-check">' +
                '<label class="form-check-label">' +
                  '<input type="checkbox" class="form-check-input" id="only_future" value="" checked>Visualizza solo proiezioni future' +
                '</label>' +
              '</div>');
      
           var form = $(`<form id="date_form">
                                <div class="form-row row pt-2">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="begin" class="col-2 col-form-label">Da</label>
                                            <div class="col-10">
                                                <input name="begin" type="date" class="form-control vf vf-dt-comp-a-lte-1" id="begin" placeholder="Prima" autocomplete="off"/>
                                            </div>
                                            <div class="alert alert-danger d-none vf-alert-begin vf-validation-alert">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="end" class="col-2 col-form-label">A</label>
                                            <div class="col-10">
                                                <input name="end" type="date" class="form-control vf vf-dt-comp-a-lte-2" id="end" placeholder="Dopo" autocomplete="off"/>
                                            </div>
                                            <div class="alert alert-danger d-none vf-alert-end vf-validation-alert">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </form>`);
      
           filter_container.append(form);
           filter_container.append(only_future_checkbox);
           
           form.on("input", this.get_filter_callback());
           only_future_checkbox.find("#only_future").on("input", this.get_filter_callback());

           return filter_container;
       }
   }

    /**
     * Classe che si occupa di rappresentare gli elementi della pagina proiezioni_film.php
     * @type type 
     */
    class ProjectionItemsTransformer extends ItemsTransformer
    {    
        on_item_clicked(item)
        {
            console.log(item[1]);
        }
        
        get_no_items_message()
        {
            return "Nessuna proiezione da visualizzare.";
        }

        get_presentable_data_common(item, columns_names)
        {
            var modals = $(item[9]);
            
            var ID_table = "tabella_proiezioni";
            var ID_programmazione = item[10];
            
            var ID_form_delete = "form-elimina-proiezione-" + ID_programmazione;
            var ID_modal_delete = "conferma-eliminazione-" + ID_programmazione;
            
            var form_delete = modals.find("#" + ID_form_delete);
            var modal_delete = modals.find('#'+ID_modal_delete);
            
            modal_form_ajax_submit(form_delete, modal_delete, ID_table);
            
            var ID_form_modify = "form-modifica-proiezione-" + ID_programmazione;
            var ID_modal_modify = "conferma-modifica-" + ID_programmazione;
            
            var form_modify = modals.find("#" + ID_form_modify);
            var modal_modify = modals.find('#'+ID_modal_modify);
            
            modal_form_ajax_submit(form_modify, modal_modify, ID_table);
            
            form_modify.find(".group-prezzo").hide();
            form_modify.find(".group-prezzo").removeClass("d-none");
            
            if (form_modify.find("#tipo").val()==="SPECIALE")
            {
                form_modify.find(".group-prezzo").show();
            }
            
            form_modify.find(".tipo").on("input", visualize_prices_if_needed);
            
            return modals;
        }

        get_presentable_data_big(item, columns_names)
        {
            return this.get_presentable_data_little(item, columns_names);
        }

        get_presentable_data_little(item, columns_names)
        {

            var image = item[3];
            var title = item[2];
            var total_places = item[5];
            var non_free_places = item[6];
            var perc = (non_free_places/total_places)*100;

            var moment_var = moment(item[0]);
            var date = moment_var.locale('it').format("dddd DD MMMM YYYY HH:mm");
            date = date.charAt(0).toUpperCase() + date.slice(1);

            var moment_end = moment(item[7]);
            var end = moment_end.locale('it').format("HH:mm");

            var info_container = get_image_card_empty_content();

            var date_room = $('<span class="image-card-date-room"><span>' + date + ' &rarr; ' + end + '</span><span class="room-label">' + item[1] + '</span></span>');
            info_container.append(date_room);

            var type_and_price_range = $('<span class="image-card-info-field"><b>Tipo:</b> ' + item[4] + ', ' + item[11] +  " (" + item[12] + ' €)</span>');
            info_container.append(type_and_price_range);
            
            var progress_bar = '<div class="progress d-inline-block progress-in-flex">'+
                                    '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"' +
                                    'aria-valuemin="0" aria-valuemax="100" style="width:' + perc + '%">' +
                                      perc.toFixed(2) + "% (" + non_free_places + "/" + total_places + ")" +
                                    '</div>' +
                                  '</div>';
                          
            var fullness = $('<div class="d-flex align-items-center justify-content-center full-width"><span class="image-card-info-field"><b>Riempimento:</b></span> ' + progress_bar + '</div>');
            info_container.append(fullness);
            
            var actions = item[8];
            info_container.append(get_actions_container(actions));

            return get_image_card(image, title, info_container);
        }
    }

    
    $( document ).ready(function() {
                            
        console.log("In films_js");
        
        $(".group-prezzo").hide();
        $(".group-prezzo").removeClass("d-none");
        
        $("#tipo").on("input", visualize_prices_if_needed);

        var form = $("#nuova-proiezione");
        var modal = $('#aggiungi-proiezione');
        var table_id = "tabella_proiezioni";
        
        modal_form_ajax_submit(form, modal, table_id);
   });
   
   
   

