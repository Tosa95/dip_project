//<script type="text/javascript">

/*
 * Script dinamico per la validazione della form di registrazione.
 */

<?php

    header("Content-type:text/javascript");
    require_once dirname(__FILE__) . '/../config.php';
    require_once dirname(__FILE__) . '/../utils.php';
    require_once dirname(__FILE__) . '/../db_facade.php';
?>
 
class ReservationManager
{
    constructor()
    {
        this.matrix = get_js_argument("seat_table");
        this.prices = get_js_argument("prices");
        console.log(JSON.stringify(this.prices));
        for(var price of this.prices)
        {
            price['num'] = 0;
        }
        this.selected_places = 0;
        this.write_data_into_form ();
    }
    
    manage_counters()
    {
        var prices = this.prices;

        for (var i = 0; i < prices.length; i++)
        {
            var id = "seat-type-" + i;
            var counter = $("#" + id);

            var inc = counter.find(".inc");

            inc.click(this.get_counter_click_callback(counter, i, +1));

            var dec = counter.find(".dec");
            dec.click(this.get_counter_click_callback(counter, i, -1));
        }
        
        this.set_non_assigned();
    }
     
    get_assigned_places()
    {
        var sum=0;
        for(var price of this.prices)
        {
            sum += price['num'];
        }
        return sum;
    }
     
    get_unassigned_places()
    {
        return this.selected_places - this.get_assigned_places();
    }
     
    inc_selected_places()
    {
        this.selected_places += 1;
        this.prices[this.get_integer_index()]['num']++;
        this.set_non_assigned();
        this.refresh_counters();
    }
    
    dec_one_counter()
    {
        for(var price of this.prices)
        {
            if (price['num'] > 0)
            {
                price['num'] -= 1;
                break;
            }
        }
        this.refresh_counters();
    }
    
    dec_one_counter_no_index(index)
    {
       var i = 0;
       for(var price of this.prices)
        {
            if (price['num'] > 0 && i !== index)
            {
                price['num'] -= 1;
                break;
            }
            i++;
        }
        this.refresh_counters(); 
    }
    
    dec_selected_places()
    {
        if (this.get_unassigned_places() > 0 )
        {
            this.selected_places -= 1;
            this.set_non_assigned();
        }
        else
        {
            console.log("else");
            this.selected_places -= 1;
            this.dec_one_counter();
        }
    }
     
    set_non_assigned()
    {
        var field = $("#non-assigned");
        var value_field = field.find(".value span");
        var unassigned = this.get_unassigned_places();
        value_field.text(unassigned);
        if (unassigned === 0)
        {
            field.fadeOut("fast");
        } else {
            field.fadeIn("fast");
        }
    }
     
    manage_table ()
    {
        var matrix = this.matrix;
        $("#selected_matrix").val(JSON.stringify(matrix));
        var self = this;
        $(".seat").click(function() {
            var id = $(this).attr("id");
            var row = id.split("x")[0];
            var col = id.split("x")[1];
            if (matrix[row][col] === <?php echo SeatStatus::FREE ?>)
            {
                // Devisualizzo il messaggio di errore nel caso fosse apparso quando ho
                // cliccato su acquista o prenota 
                $("#data-form-msg").addClass("d-none");
                $(this).attr("src","images/vectorial/selected_place.png");
                matrix[row][col] = <?php echo SeatStatus::SELECTED ?>;
                self.inc_selected_places();
            }
            else if (matrix[row][col] === <?php echo SeatStatus::SELECTED ?>)
            {
                $(this).attr("src","images/vectorial/free_place.png");
                matrix[row][col] = <?php echo SeatStatus::FREE ?>;
                self.dec_selected_places();
            }
            self.write_data_into_form();
        });
    }
    
    animate_unassigned(blinks)
    {
        var value_field = $("#non-assigned .value");
        
        // Per evitare problemi nel caso si clicchi più volte su acquista/prenota 
        // senza che tutti i posti siano assegnati alla relativa categoria 
        // (ATTENZIONE: va messo assolutamente prima di prendere il ccolore!!!)
        value_field.finish();
        
        var original_color = value_field.css("background-color");
        var blink_color = "#f98d34";
        var current = 0;
        
        function blink()
        {
            if (blinks > 0)
            {
                if (current === 0)
                {
                    value_field.animate({"background-color":blink_color}, "fast", blink);
//                    console.log(current + " " + blink_color + " " + blinks);
                    current = 1;
                }
                else
                {
                    value_field.animate({"background-color":original_color}, "fast", blink);
//                    console.log("else " + current + " " + original_color + " " + blinks);
                    current = 0;
                    blinks--;
                }
            }
        }
        
        
        blink();
    }
    
    manage_form()
    {
        var self = this;
        
        function prevent(event)
        {
            if (self.get_unassigned_places()>0 || self.selected_places === 0)
            {
                event.stopPropagation();
                event.preventDefault();
                self.animate_unassigned(3);
                
                if (self.selected_places === 0)
                {
                    $("#data-form-msg").html("Devi selezionare almeno un posto prima di poter prenotare o acquistare!");
                    $("#data-form-msg").removeClass("d-none");
                }
            }
        }

        $('#submit_purchase_button').click(prevent);
        $('#submit_reserve_button').click(prevent);
        
    }
    
    get_integer_index()
    {
        var i = 0;
        for (var price of this.prices)
        {
            if (price["nome_prezzo"] === "INTERO")
            {
                return i;
            }
            i++;
        }
        return 0;
    }
    
    get_counter_click_callback(counter, index, direction)
    {
        var prices=this.prices;
        var self = this;
        return function(){
            var value_field = counter.find(".value");
            var prev_value = prices[index]['num'];
            
            var current_value = prev_value + direction;

            if (current_value >= 0 && self.get_unassigned_places()-direction >= 0)
            {
                value_field.text(current_value);
                prices[index]['num'] = current_value;
            }
            self.set_non_assigned();
            self.refresh_counters(); 
        };


    }
    
    refresh_counters()
    {
        var prices = this.prices;

        for (var i = 0; i < prices.length; i++)
        {
            var id = "seat-type-" + i;
            var counter = $("#" + id);

            counter.find(".value").text(prices[i]['num']);
        }
        
        $("#total .value span").text(this.get_total().toFixed(2) + " €");
        
        this.write_data_into_form ();
    }
    
    get_total()
    {
        var sum = 0;
        for (var price of this.prices)
        {
            var price_amount = Number(price["valore"]);
            var num_seats = Number(price["num"]);
            sum += num_seats * price_amount;
        }
        return sum;
    }
    
    write_data_into_form ()
    {
        $("#selected_matrix").val(JSON.stringify(this.matrix));
        $("#prices").val(JSON.stringify(this.prices));
    }
    
}


$( document ).ready(function() {
                            
                            var manager = new ReservationManager();
                            manager.manage_table();
                            manager.manage_counters();
                            manager.manage_form();
                            
                            var acq_form = $("#form-conferma-acquisto");
                            
                            // Form di acquisto mandata in ajax
                            callback_form_ajax_submit(acq_form,"POST",function(data)
                            {
                                if (data["status"] === "OK")
                                {
                                    acq_form.find(".alert-msg").addClass("d-none");
                                    var data_form = $("#reservation_form");
                                    var challenge = acq_form.find("#challenge").val();
                                    var credit_card = acq_form.find("#credit_card").val();
                                    data_form.append('<input type="hidden" name="signature" value="' + data["signature"] + '"/>');
                                    data_form.append('<input type="hidden" name="challenge" value="' + challenge + '"/>');
                                    data_form.append('<input type="hidden" name="credit_card" value="' + credit_card + '"/>');
                                    data_form.append('<input type="hidden" name="action" value="purchase"/>');
                                    data_form.submit();
                                }
                                else
                                {
                                    acq_form.find(".alert-msg").removeClass("d-none");
                                    acq_form.find(".alert-msg").html("<strong>Si è verificato un errore!</strong><br>" + data["msg"]);
                                }
                            });
                            
                       });

