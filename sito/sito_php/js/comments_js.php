//<script type="text/javascript">

<?php

    header("Content-type:text/javascript");
    require_once dirname(__FILE__) . '/../config.php';
    require_once dirname(__FILE__) . '/../utils.php';
    require_once dirname(__FILE__) . '/../db_facade.php';
    
?>
    
    class CommentsDataFilterView extends DataFilterView
    {

        get_filter_callback()
        {
            var self = this;

            return function()
            {
                var search = $('#search_events');
                var checkbox = $('#only_current');
                var date_form = $("#date_form");
                var begin = date_form.find("#begin");
                var end = date_form.find("#end");
                
                
                // Se è stato il cambiamento della checkbox a scaturire l'evento, 
                // resetto le date. Questo per fare in modo che non si possano impostare
                // contemporaneamente i filtri sulle date ed il filtro "eventi correnti"
                if($(this).attr("id") === "only_current")
                {
                    date_form[0].reset();
                    re_validate_all([date_form.find("#begin")[0], date_form.find("#end")[0]]);
                }
                // Se sono state le date, tolgo la spunta dalla checkbox
                else if($(this).attr("id") === "date_form")
                {
                    checkbox.prop("checked", false);
                }
        
                
                var c_only = false;
                
                if(checkbox.is(':checked')) 
                {
                     c_only = true;
                }
                

                var filter = {"string_filter": search.val(), "only_current":c_only, "begin": begin.val(), "end": end.val()};
                
                if (self.callback !== null)
                {
                    self.callback(filter);
                }
                console.log(JSON.stringify(filter));
            };
        }


        get_filter_view()
        {

//            var search = $('<input type="text" class="string-filter-input" id="search_events" placeholder="Cerca"/>');
//            var only_current = $('<label class="form-check-label"><input class="form-check-input" id="only_current" type="checkbox"/> Mostra solo eventi correnti</label>');
//            var date_form = $(`<form id="date_form">
//                                <div class="form-row row pt-2">
//                                    <div class="col-md-6">
//                                        <div class="form-group row">
//                                            <label for="begin" class="col-2 col-form-label">Da</label>
//                                            <div class="col-10">
//                                                <input name="begin" type="date" class="form-control vf vf-dt-comp-a-lte-1" id="begin" placeholder="Prima" autocomplete="off"/>
//                                            </div>
//                                            <div class="alert alert-danger d-none vf-alert-begin vf-validation-alert">
//
//                                            </div>
//                                        </div>
//                                    </div>
//                                    <div class="col-md-6">
//                                        <div class="form-group row">
//                                            <label for="end" class="col-2 col-form-label">A</label>
//                                            <div class="col-10">
//                                                <input name="end" type="date" class="form-control vf vf-dt-comp-a-lte-2" id="end" placeholder="Dopo" autocomplete="off"/>
//                                            </div>
//                                            <div class="alert alert-danger d-none vf-alert-end vf-validation-alert">
//
//                                            </div>
//                                        </div>
//                                    </div>
//                                </div>
//                             </form>`);
//
//
//            var filter_container = $('<div class="container-fluid"></div>');
//
//            var filter_container_row = $('<div class="d-flex row justify-content-center align-items-center"></div>');
//            filter_container.append(filter_container_row);
//
//            var search_col = $('<div class="col-12 py-2"></div>');
//            search_col.append(search);
//
//            var only_current_col = $('<div class="col-12 col-lg-4 text-center"></div>');
//            only_current_col.append(only_current);
//
//            var date_form_col = $('<div class="col-12 text-center"></div>');
//            date_form_col.append(date_form);
//
//            filter_container_row.append(search_col);
//            filter_container_row.append(date_form_col);
//            filter_container_row.append(only_current_col);
//
//            // Aggiungo i listeners
//            search.on("input", this.get_filter_callback());
//            date_form.on("input", this.get_filter_callback());
//            only_current.find("#only_current").change(this.get_filter_callback());
//
//            return filter_container;

              return "";
        }
    }
   
   
    /**
    * Classe che si occupa di rappresentare gli elementi della pagina eventi.php
    * @type type 
    */
    class CommentsItemsTransformer extends ItemsTransformer
    {
        get_no_items_message()
        {
            return "Nessun evento da visualizzare.";
        }
       
        get_presentable_data_common(item, columns_names)
        { 
            return "";
        }

        get_presentable_data_big(item, columns_names)
        {
            var info_container = get_image_card_empty_content();

            var date = date_standard_format(item["data"]);

            var first_line = '<div class="d-flex justify-content-start">'
                    + '<span class="image-card-info-field mr-4"> <b>ID:</b> ' + item["ID"] + '</span>'
                    + '<span class="image-card-info-field mr-4"> <b>Username:</b> ' + item["username"] + '</span>'
                    + '<span class="image-card-info-field mr-4"> <b>Pagina:</b> ' + item["pagina"] + '</span>'
                    + '<span class="image-card-info-field mr-4"> <b>Data:</b> ' + date + '</span>'
                   + '</div>';
           
           info_container.append($(first_line));
           info_container.append(get_image_card_long_field("Commento:", item["descrizione"]));
                                                    
           return info_container;
        }

        get_presentable_data_little(item, columns_names)
        {
             return this.get_presentable_data_big(item, columns_names);
        }
    }

    $( document ).ready(function() {
                            
        console.log("In events_js");

        var table_id = "tabella_eventi";
        var form_add = $("#nuovo-evento");
        var modal_add = $('#aggiungi-evento');

        modal_form_ajax_submit(form_add, modal_add, table_id);
        
   });