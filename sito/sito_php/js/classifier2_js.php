//<script type="text/javascript">

<?php require_once dirname(__FILE__). '/../config.php' ?>;

var labels_manager = null;
var preloader = null;

let IMAGES_TO_VISUALIZE = 200;

function visualize_images_to_classify(num_img=100)
{
    $("#images-cont").empty().append("Caricamento...");
    $.ajax({
        // URL verso la quale inviare la richiesta
        url: "/segnali/ajax/images_ajax.php?operation=get_ordered_images_to_be_labeled&num_images=" + num_img,
        // Dati da inviare al server
        method: "GET",
        dataType: 'json',
        type: "GET", // For jQuery < 1.9
        // Funzione di callback una volta ricevuta la risposta 
        success: function(dt)
        {
            images_viewer = new ImagesViewer($("#images-cont"), dt["data"], false, true);
            images_viewer.show();
            $("html, body").animate({ scrollTop: 0 }, "fast");
        },
        error: function(err)
        {
            console.log(err);
        }
    });
}

$( document ).ready(function() {

    visualize_images_to_classify(IMAGES_TO_VISUALIZE);
    
    let stats_update_delay = 10000;
    
    $("#next_button").click(function (){
        visualize_images_to_classify(IMAGES_TO_VISUALIZE);
    });
    
    function update_stats()
    {
        $.ajax({
            // URL verso la quale inviare la richiesta
            url: "/segnali/ajax/images_ajax.php?operation=get_stats",
            // Dati da inviare al server
            method: "GET",
            dataType: 'json',
            type: "GET", // For jQuery < 1.9
            // Funzione di callback una volta ricevuta la risposta 
            success: function(dt)
            {
                let stats = dt["stats"];
                
                
                let txt = "<br/><br/>";
                
                Object.keys(stats).forEach(function(key) {
                    value = stats[key];
                    txt += key + ": " + value.toFixed(3) + "<br/>";
                });
                
                $("#stats").html(txt);
                window.setTimeout(update_stats, stats_update_delay);

            },
            error: function(err)
            {
                console.log(err);
                window.setTimeout(update_stats, stats_update_delay);
            }
        });
    }
    
    window.setTimeout(update_stats, stats_update_delay);
    
});


