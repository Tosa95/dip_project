//<script type="text/javascript">

<?php require_once dirname(__FILE__). '/../config.php' ?>;

let ICON_FOLDER = "<?php echo $ICON_FOLDER?>";
let IMAGE_FOLDER = "<?php echo $SIGNALS_FOLDER?>";
let SERVICE_ADDRESS = "<?php echo $REMOTE_IMAGES_SERVICE_PATH?>";

class Preloader
{
    constructor(num_preloads, images_preloaders_container)
    {
        this.num_preloads = num_preloads;
        this.images_preloaders_container = images_preloaders_container;
        
        this.list = [];
        this.when_got = null
        this.filling = false
        this.fill(null);
    }
    
    get(when_got)
    {
        console.log("Inside preloader");

        if (this.list.length > 0)
        {
            console.log("In queue");
            let val = this.list[0];
            $("#preload_" + val).remove();
            this.list.splice(0, 1); //Removing first element
            when_got(val);
        }
        else
        {
            console.log("Not in queue");
            this.when_got = when_got;
        }
        
        if (!this.filling){
            this.fill(null);
        }
    }
    
    fill()
    {
        let self = this;
        self.filling = true;
        console.log("Filling");
        if (this.list.length < this.num_preloads)
        {
            $.ajax({
                // URL verso la quale inviare la richiesta
                url: "/segnali/ajax/images_ajax.php?operation=get_random_id&label_type=1",
                // Dati da inviare al server
                method: "GET",
                dataType: 'json',
                type: "GET", // For jQuery < 1.9
                // Funzione di callback una volta ricevuta la risposta 
                success: function(dt)
                {
                    let val = dt["ID_image"];
                    console.log("Filling, received ");
                    console.log(dt);
                    if (val !== undefined)
                    {
                        self.images_preloaders_container.append($('<img class="d-inline-block" style="position:absolute;top:-5000px;left:-5000px;" id="preload_' + val + '" src="' + SERVICE_ADDRESS + "/images/" + val + '"/>'));
                        if (self.when_got !== null)
                        {
                            self.when_got(val);
                            self.when_got = null;
                        }
                        else
                        {
                            self.list.push(val);
                        }
                        self.fill();
                    }
                },
                error: function(err)
                {
                    console.log(err);
                }
            });
        }
        else
        {
            self.filling=false;
        }
    }
}

class LabelsManager
{
    constructor(container, tree, image_container, image_thumbnail_container, 
                preloader, view_only=false, draw_counts=false, draw_ids=false,
                on_label_selected=null)
    {
        this.selected = [];
        this.tree = tree;
        this.container = container;
        this.image_container = image_container;
        this.image_thumbnail_container = image_thumbnail_container;
        this.image_id = null;
        this.preloader = preloader;
        this.view_only = view_only;
        this.draw_counts = draw_counts;
        this.draw_ids = draw_ids;
        this.on_label_selected = on_label_selected;
        
        this.reset();
    }
    
    get_last_level_button_callback(label_id, value_id, value_data)
    {
        self = this;
        return function(){
            if (self.on_label_selected !== null)
            {
                self.on_label_selected(value_data);
            }
            self.selected.push([label_id, value_id]);
            self.draw_container();
//            console.log(self.selected);
        };
    }
    
    get_middle_level_button_callback(label_id, value_id, selected_index, value_data)
    {
        var self = this;
        return function(){
            self.selected[selected_index] = [label_id, value_id];
            if (self.on_label_selected !== null)
            {
                self.on_label_selected(value_data);
            }
//            console.log("SELECTED");
//            console.log(selected_index);
//            console.log(self.selected);
            
            if (selected_index < self.selected.length)
            {
                self.selected.splice(selected_index + 1);
            }
            
//            console.log(self.selected); 
            self.draw_container();
        };
    }
    
    get_ok_button_callback()
    {
        self = this;
        
        return function () {
            
            $.ajax({
              // URL verso la quale inviare la richiesta
              url: "/segnali/ajax/images_ajax.php",
              // Dati da inviare al server
              method: "GET",
              dataType: 'json',
              type: "GET", // For jQuery < 1.9
              data: {
                  "image_id": self.image_id,
                  "labels": self.selected,
                  "operation": "add_all_labels"
              },
              // Funzione di callback una volta ricevuta la risposta 
              success: function(dt)
              {
//                  console.log(dt);
                  if (dt["status"]!=="OK")
                  {
                      alert("PROBLEMA nell'inserimento delle etichette. Interrompi la classificazione e contatta l'amministratore di sistema");
                  }
              },
              error: function(err)
              {
                  console.log("ERROR");
                  console.log(err["responseText"]);
              }
          });
            
          self.reset();  
        };
    }
    
    draw_additional_data(additional_data)
    {
        return (this.draw_counts?'<div class="button-cnt-container">' + additional_data["cnt"] + '</div>':'')
             + (this.draw_counts?'<div class="button-cnt-container">' + additional_data["perc"].toFixed(2) + '%</div>':'')
             + (this.draw_ids?'<div class="button-cnt-container">TID: ' + additional_data["TID"] + ' VID: ' + additional_data["ID"] + '</div>':'')
             + (this.draw_counts?'<div class="button-cnt-container ' + (additional_data["predicted"]>0?"pred_more_than_0":"") + '">PRED:' + additional_data["predicted"] + '</div>':'');
    }
    
    get_text_icon_button(text, color, additional_data)
    {
        
        //let icon_path = $SITE_ROOT . "/images/vectorial/" . $icon;
        
        return $('<div class="button button-text-icon d-inline-flex justify-content-center align-items-center ' + color + '" >'
                + '<div class="button-text-container">' + text
                + this.draw_additional_data(additional_data)
                + '</div></div>');
    }
    
    get_icon_button(icon, color, additional_data)
    {
        let img_path = ICON_FOLDER + icon;
        console.log(img_path);
        console.log(encodeURI(img_path));
        return $('<div class="button d-flex align-items-center justify-content-center ' + color + '">'
                   + '<div class="button-text-container"><img id="current" src="' + encodeURI(img_path) + '" class="img-fluid" style="width:70px"/>'
                   + this.draw_additional_data(additional_data)
                   + '</div></div>');
    }
    
    get_button(text, icon, color, additional_data)
    {
        if (icon === null)
        {
            return this.get_text_icon_button(text,color,additional_data);
        }
        else
        {
            return this.get_icon_button(icon,color,additional_data);
        }
    }
    
    draw_next_label(data)
    {
        var values = data["values"];
            
        this.container.append('<div class="d-flex label_desc">' + data["description"] + "</div>");

        var values_container = $('<div class="container d-flex justify-content-around flex-wrap"></div>');

        for (let i = 0; i<values.length; i++)
        { 
            //let btn = $('<span class="button button-text-icon button-primary label_value">' + values[i]["name"] + "</span>");
            let btn = this.get_button(values[i]["name"], values[i]["image"], "button-neutral", values[i]);
            btn.click(this.get_last_level_button_callback(data["ID"], values[i]["ID"], values[i]));
            values_container.append(btn);
//            console.log(values[i]["name"]);
        }

        this.container.append(values_container);
        
        return values_container.offset().top;
    }
    
    draw_selected_label(data, value_id, selected_index)
    {
        var values = data["values"];
            
        this.container.append('<div class="d-flex label_desc">' + data["description"] + "</div>");

        var values_container = $('<div class="container d-flex justify-content-around flex-wrap"></div>');

        for (let i = 0; i<values.length; i++)
        {
            let value = values[i];
            
            let color = "button-neutral";
            
            if (value["ID"] === value_id)
            {
                color = "button-primary";
            }
            
            //let btn = $('<span class="button button-text-icon ' + color + ' label_value"><span class="button-text-container">' + value["name"] + "</span></span>");
            let btn = this.get_button(values[i]["name"], values[i]["image"], color, values[i]);
            btn.click(this.get_middle_level_button_callback(data["ID"], values[i]["ID"], selected_index, values[i])); //Qua come i ci va l'indice di selected, invece noi ci mettiamo l'indice dell'array di values, che non ha senso
            values_container.append(btn);
//            console.log(values[i]["name"]);
        }

        this.container.append(values_container);
    }
    
    get_subtype(node, value_id)
    {
        for (let value of node["values"])
        {
            if (value["ID"] === value_id)
            {
                return value["subtype"];
            }
        }
        return null;
    }
    
    draw_container()
    {
        this.container.empty();
        
        let curr_node = this.tree;
        
        // This.selected contains a pair of label_type_ID and its corresponding 
        // label_value_ID 
        
        let i = 0;
        
        // Drawing all label for which a vale has already been selected
        for (let label_id_value of this.selected)
        {
            let value_id = label_id_value[1];
            
            this.draw_selected_label(curr_node, value_id, i++);
            curr_node = this.get_subtype(curr_node, value_id);
//            console.log(curr_node);
        }
        
        if (curr_node !== null)
        {
            // Drawing the first label for which a value hasn't been selected
            // and scrolling to the start of the set of values
            let scroll_pos = this.draw_next_label(curr_node);
            $("html, body").animate({ scrollTop: scroll_pos - 150 }, "fast");
        }
        else if (!this.view_only)
        {
            // Leaf of the tree, showing confirmation button
            var ok_button = $('<div class="button button-primary button-confirm" id="refresh_btn">CONFERMA</div>');
            
            ok_button.click(this.get_ok_button_callback());
            
            this.container.append(ok_button);
        }
    }
    
    update_image()
    {
        var self = this;
        self.image_container.attr("src", "");
        self.image_thumbnail_container.attr("src", "");
        self.container.empty();
        self.container.append("<div>Caricamento...</div>");
        this.preloader.get(function(val)
            {
                console.log(val);
                self.image_id = val;
                console.log(self.image_id);
                self.image_container.attr("src", SERVICE_ADDRESS + "/images/" + self.image_id);
                self.image_thumbnail_container.attr("src", SERVICE_ADDRESS + "/images/" + self.image_id);
                console.log(self.image_container.length); 
                self.draw_container();
            });
    }
    
    reset()
    {
        this.selected = [];
        
        if (!this.view_only)
        {
            this.update_image();
        }
        else
        {
            this.draw_container();
        }
        //$("html, body").animate({ scrollTop: 0 }, "fast");
        
        
    }
    
    get_last_selected()
    {
        return this.selected[this.selected.length - 1];
    }
}

class ImagesViewer
{
    constructor(container, images, draw_accept_button=false, fast_classification_conf=false)
    {
        this.container = container;
        this.images = images;
        this.draw_accept_button = draw_accept_button;
        this.fast_classification_conf = fast_classification_conf;
    }

    get_reject_button_callback(image_data)
    {
        let image_id = image_data["image_id"];
        
        let self = this;
        return function(evt){
            let cont = $(this).closest(".act-cont");
            cont.empty();
            cont.append("loading");
            
            $.ajax({
                // URL verso la quale inviare la richiesta
                url: "/segnali/ajax/images_ajax.php",
                // Dati da inviare al server
                method: "GET",
                dataType: 'json',
                type: "GET", // For jQuery < 1.9
                data: {
                    "image_id": image_id,
                    "operation": "delete_all_image_labels"
                },
                // Funzione di callback una volta ricevuta la risposta 
                success: function(dt)
                {
  //                  console.log(dt);
                    if (dt["status"]!=="OK")
                    {
                        alert("PROBLEMA nell'eliminazione delle etichette. Interrompi la classificazione e contatta l'amministratore di sistema");
                    }else{
                        cont.empty();
                        cont.append("REJECTED");
                    }
                },
                error: function(err)
                {
                    console.log("ERROR");
                    console.log(err["responseText"]);
                }
            });
        };
    }
    
    get_revert_accept_button_callback(image_data)
    {
        let image_id = image_data["image_id"];
        
        let self = this;
        return function(evt){
            let cont = $(this).closest(".act-cont");
            cont.empty();
            cont.append("loading");
            
            $.ajax({
                // URL verso la quale inviare la richiesta
                url: "/segnali/ajax/images_ajax.php",
                // Dati da inviare al server
                method: "GET",
                dataType: 'json',
                type: "GET", // For jQuery < 1.9
                data: {
                    "image_id": image_id,
                    "operation": "delete_all_image_labels"
                },
                // Funzione di callback una volta ricevuta la risposta 
                success: function(dt)
                {
  //                  console.log(dt);
                    if (dt["status"]!=="OK")
                    {
                        alert("PROBLEMA nell'eliminazione delle etichette. Interrompi la classificazione e contatta l'amministratore di sistema");
                    }else{
                        cont.empty();
                        cont.replaceWith(self.get_actions_container(image_data));
                    }
                },
                error: function(err)
                {
                    console.log("ERROR");
                    console.log(err["responseText"]);
                }
            });
        };
    }
    
    get_revert_not_a_sign_button_callback(image_data)
    {
        //Does the exact same thing as needed in this case: removing all labels
        return this.get_revert_accept_button_callback(image_data);
    }
    
    get_revert_other_sign_button_callback(image_data)
    {
        let image_id = image_data["image_id"];
        let self = this;
        
        return function(evt){
            
            let cont = $(this).closest(".act-cont");
            cont.empty();
            cont.append("loading");
            
            $.ajax({
                // URL verso la quale inviare la richiesta
                url: "/segnali/ajax/images_ajax.php",
                // Dati da inviare al server
                method: "GET",
                dataType: 'json',
                type: "GET", // For jQuery < 1.9
                data: {
                    "image_id": image_id,
                    "operation": "delete_image_from_labeling_queue"
                },
                // Funzione di callback una volta ricevuta la risposta 
                success: function(dt)
                {
  //                  console.log(dt);
                    if (dt["status"]!=="OK")
                    {
                        alert("PROBLEMA nell'eliminazione delle immagini dalla coda. Interrompi la classificazione e contatta l'amministratore di sistema");
                    }else{
                        cont.empty();
                        cont.replaceWith(self.get_actions_container(image_data));
                    }
                },
                error: function(err)
                {
                    console.log("ERROR");
                    console.log(err["responseText"]);
                }
            });
            
        };
    }
    
    get_revert_button(callback)
    {
        let rev_btn = $('<div class="button button-delete">REVERT</div>');
        rev_btn.click(callback);
        return rev_btn;
    }
    
    get_actions_container(image_data, left_cont=null, right_cont=null)
    {
        let image_id = image_data["image_id"];
    
        let actions_cont = $('<div class="act-cont d-flex align-items-center flex-column justify-content-center"></div>');
        
        if (this.draw_accept_button)
        {
            
            let btn_accept = $('<div class="button button-purchase">ACCEPT</div>');
            btn_accept.click(this.get_accept_button_callback(image_data));
            actions_cont.append(btn_accept);
            
            let btn_other_sign = $('<div class="button button-primary">OTHER</div>');
            btn_other_sign.click(this.get_other_sign_button_callback(image_data));
            actions_cont.append(btn_other_sign);
            
            let btn_not_a_sign = $('<div class="button button-delete">NO SIGN</div>');
            btn_not_a_sign.click(this.get_not_a_sign_button_callback(image_data));
            actions_cont.append(btn_not_a_sign);
            
        } else if (this.fast_classification_conf)
        {
            
            let labels_cont = $('<div class="d-flex mb-3 mt-3 align-items-center flex-row flex-wrap justify-content-center"></div>');
            
            for (let l of image_data["pred_labels"])
            {
                let btn = $('<div class="button button-accept"></div>');
                if (l["lbl_info"]["image"] !== null)
                {
                    btn.append($('<img class="little-label-repr" src="' + "<?php echo $ICON_FOLDER?>" + l["lbl_info"]["image"] + '"/>'));
                } else {
                    btn.append(l["lbl_info"]["name"]);
                }
                btn.click(this.get_accept_button_callback(image_data,l["path"],l["lbl_info"]["image"]));
                labels_cont.append(btn);
            }
            
            actions_cont.append(labels_cont);
            
            let btns_cont = $('<div class="d-flex align-items-center flex-row justify-content-center"></div>');
        
            
            let btn_other_sign = $('<div class="button button-primary">OTHER</div>');
            btn_other_sign.click(this.get_other_sign_button_callback(image_data));
            btns_cont.append(btn_other_sign);
            
            let btn_not_a_sign = $('<div class="button button-delete">NO SIGN</div>');
            btn_not_a_sign.click(this.get_not_a_sign_button_callback(image_data));
            btns_cont.append(btn_not_a_sign);
            
            actions_cont.append(btns_cont);
            
        } else {
            let btn = $('<div class="button button-delete">REJECT</div>');
            btn.click(this.get_reject_button_callback(image_data));
            actions_cont.append(btn);
        }
        
        return actions_cont;
    }
    
    get_accept_button_callback(image_data, lbl_path=null, lbl_icon=null)
    {
        let image_id = image_data["image_id"];
        
        let path = image_data["path"];
        
        if (lbl_path!==null)
        {
            path = lbl_path;            
        }

        console.log(path);

        let self = this;
        return function(evt){
            let cont = $(this).closest(".act-cont");
            cont.empty();
            cont.append($('<div class="status">loading</div>'));

            $.ajax({
                // URL verso la quale inviare la richiesta
                url: "/segnali/ajax/images_ajax.php",
                // Dati da inviare al server
                method: "GET",
                dataType: 'json',
                type: "GET", // For jQuery < 1.9
                data: {
                    "image_id": image_id,
                    "labels": path,
                    "operation": "add_all_labels"
                },
                // Funzione di callback una volta ricevuta la risposta 
                success: function(dt)
                {
  //                  console.log(dt);
                    if (dt["status"]!=="OK")
                    {
                        alert("PROBLEMA nell'inserimento delle etichette. Interrompi la classificazione e contatta l'amministratore di sistema");
                    }else{
                        cont.empty();
                        if (lbl_icon !== null)
                        {
                            cont.append($('<img class="little-label-repr" src="' + "<?php echo $ICON_FOLDER?>" + lbl_icon + '"/>'));
                        }
                        cont.append("ACCEPTED");
                        cont.append(self.get_revert_button(self.get_revert_accept_button_callback(image_data)));
                        
                    }
                },
                error: function(err)
                {
                    console.log("ERROR");
                    console.log(err["responseText"]);
                }
            });


            evt.stopPropagation();
        };
    }

    get_other_sign_button_callback(image_data)
    {
        let self = this;
        let image_id = image_data["image_id"];
        
        return function(evt){
            
            let cont = $(this).closest(".act-cont");
            cont.empty();
            cont.append($('<div class="status">loading</div>'));

            $.ajax({
                // URL verso la quale inviare la richiesta
                url: "/segnali/ajax/images_ajax.php",
                // Dati da inviare al server
                method: "GET",
                dataType: 'json',
                type: "GET", // For jQuery < 1.9
                data: {
                    "image_id": image_id,
                    "operation": "add_image_to_labeling_queue"
                },
                // Funzione di callback una volta ricevuta la risposta 
                success: function(dt)
                {
  //                  console.log(dt);
                    if (dt["status"]!=="OK")
                    {
                        alert("PROBLEMA nell'inserimento in coda. Interrompi la classificazione e contatta l'amministratore di sistema");
                    }else{
                        cont.empty();
                        cont.append("OTHER");
                        cont.append(self.get_revert_button(self.get_revert_other_sign_button_callback(image_data)));
                        
                    }
                },
                error: function(err)
                {
                    console.log("ERROR");
                    console.log(err["responseText"]);
                }
            });
            
            evt.stopPropagation();
        };
    }
    
    get_not_a_sign_button_callback(image_data)
    {
        let self = this;
        let image_id = image_data["image_id"];
        let not_a_sign_path = [[1,2]];
        
        return function(evt){
            
            let cont = $(this).closest(".act-cont");
            cont.empty();
            cont.append($('<div class="status">loading</div>'));

            $.ajax({
                // URL verso la quale inviare la richiesta
                url: "/segnali/ajax/images_ajax.php",
                // Dati da inviare al server
                method: "GET",
                dataType: 'json',
                type: "GET", // For jQuery < 1.9
                data: {
                    "image_id": image_id,
                    "labels": not_a_sign_path,
                    "operation": "add_all_labels"
                },
                // Funzione di callback una volta ricevuta la risposta 
                success: function(dt)
                {
  //                  console.log(dt);
                    if (dt["status"]!=="OK")
                    {
                        alert("PROBLEMA nell'inserimento delle etichette. Interrompi la classificazione e contatta l'amministratore di sistema");
                    }else{
                        cont.empty();
                        cont.append("NO SIGN");
                        cont.append(self.get_revert_button(self.get_revert_not_a_sign_button_callback(image_data)));
                        
                    }
                },
                error: function(err)
                {
                    console.log("ERROR");
                    console.log(err["responseText"]);
                }
            });
            
            evt.stopPropagation();
        };
    }

    get_image_button(image_data)
    {
        let image_id = image_data["image_id"];
        let burst_id = image_data["burst_id"];
        let score = image_data["score"];

    
        let cont = $('<div class="imgcont d-flex align-items-center flex-column justify-content-center"></div>');
        
        if (this.fast_classification_conf)
        {
            cont = $('<div class="imgcont-big d-flex align-items-center flex-column justify-content-center"></div>');
        }
        
        let top_cont = $('<div class="imgcont d-flex align-items-center flex-row justify-content-center"></div>');
        let left_cont = $('<div class="imgcont d-flex align-items-center flex-row justify-content-center"></div>');
        let right_cont = $('<div class="imgcont d-flex align-items-center flex-row justify-content-center"></div>');
        
        top_cont.append(left_cont);
        top_cont.append($('<div class="button-text-container"><img class="imgcont-sz" src="' + SERVICE_ADDRESS + "/images/" + image_id + '"/></div>'));
        top_cont.append(right_cont);
    
        cont.append(top_cont);
        cont.append($('<div class="button-cnt-container">BID: ' + burst_id + '</div>'));
        cont.append($('<div class="button-cnt-container">SCORE: ' + score.toFixed(3) + '</div>'));



        cont.append(this.get_actions_container(image_data, left_cont, right_cont));

        return cont;
    }

    show()
    {
        this.container.empty();
        
        var images_container = $('<div class="container d-flex justify-content-around flex-wrap"></div>');
        
        if (this.images.length > 0)
        {
            for (let img of this.images)
            {
                images_container.append(this.get_image_button(img));
            }

            this.container.append(images_container);
        }else{
            this.container.append("Nothing to show");
        }
        
    }

}