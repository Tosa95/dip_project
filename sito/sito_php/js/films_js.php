//<script type="text/javascript">

<?php

    header("Content-type:text/javascript");
    require_once dirname(__FILE__) . '/../config.php';
    require_once dirname(__FILE__) . '/../utils.php';
    require_once dirname(__FILE__) . '/../db_facade.php';
    
?>
    
   /**
    * Oggetto che si occupa di rappresentare un filtro per i dati (classe astratta,
    * derivare da questa per definire nuovi tipi di visualizzatori di filtri)
    * @type type
    */
    class FilmDataFilterView extends DataFilterView
    {

        get_filter_callback()
        {
            var self = this;

            return function()
            {

                var search = $('#search_films');
                var checkbox = $('#only_current');
                var date_form = $("#date_form");
                var begin = date_form.find("#begin");
                var end = date_form.find("#end");
                
                
                // Se è stato il cambiamento della checkbox a scaturire l'evento, 
                // resetto le date. Questo per fare in modo che non si possano impostare
                // contemporaneamente i filtri sulle date ed il filtro "eventi correnti"
                if($(this).attr("id") === "only_current")
                {
                    date_form[0].reset();
                    re_validate_all([date_form.find("#begin")[0], date_form.find("#end")[0]]);
                }
                // Se sono state le date, tolgo la spunta dalla checkbox
                else if($(this).attr("id") === "date_form")
                {
                    checkbox.prop("checked", false);
                }


                var c_only = false;

                if(checkbox.is(':checked')) {
                     c_only = true;
                }

                var filter = {"string_filter": search.val(), "only_current":c_only, "begin": begin.val(), "end": end.val()};

                if (self.callback !== null)
                {
                    self.callback(filter);
                }
                console.log(JSON.stringify(filter));
            };
        }

        get_filter_view()
        {

            var search = $('<input type="text" class="form-control" id="search_films" placeholder="Cerca"/>');
            var only_current = $('<label class="form-check-label"><input class="form-check-input" id="only_current" type="checkbox"/> Mostra solo film in proiezione</label>');
            var date_form = $(`<form id="date_form">
                                <div class="form-row row pt-2">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="begin" class="col-2 col-form-label">Da</label>
                                            <div class="col-10">
                                                <input name="begin" type="date" class="form-control vf vf-dt-comp-a-lte-1" id="begin" placeholder="Prima" autocomplete="off"/>
                                            </div>
                                            <div class="alert alert-danger d-none vf-alert-begin vf-validation-alert">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="end" class="col-2 col-form-label">A</label>
                                            <div class="col-10">
                                                <input name="end" type="date" class="form-control vf vf-dt-comp-a-lte-2" id="end" placeholder="Dopo" autocomplete="off"/>
                                            </div>
                                            <div class="alert alert-danger d-none vf-alert-end vf-validation-alert">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </form>`);


            var filter_container = $('<div class="container-fluid"></div>');

            var filter_container_row = $('<div class="d-flex row justify-content-center align-items-center"></div>');
            filter_container.append(filter_container_row);

            var search_col = $('<div class="col-12 py-2"></div>');
            search_col.append(search);

            var only_current_col = $('<div class="col-12 col-lg-4 text-center"></div>');
            only_current_col.append(only_current);

            var date_form_col = $('<div class="col-12 text-center"></div>');
            date_form_col.append(date_form);
            
            filter_container_row.append(search_col);
            filter_container_row.append(date_form_col);
            filter_container_row.append(only_current_col);

            // Aggiungo i listeners
            search.on('input', this.get_filter_callback());
            date_form.on("input", this.get_filter_callback());
            only_current.find("#only_current").change(this.get_filter_callback());

            return filter_container;
        }
    }

    /**
     * Classe che si occupa di rappresentare gli elementi della pagina film.php
     * @type type 
     */
    class FilmsItemsTransformer extends ItemsTransformer
    {    
        on_item_clicked(item)
        {
            var ID_film = item[12];
            
            window.location.href = "<?php echo $SITE_ROOT?>admin/proiezioni_film.php?ID=" + ID_film;
            console.log(item[1]);
        }
        
        get_no_items_message()
        {
            return "Nessun film da visualizzare.";
        }

        get_presentable_data_common(item, columns_names)
        {
            var modals = $(item[14]);
            
            var ID_table = "tabella_film";
            var ID_film = item[12];
            
            var ID_form_delete = "form-elimina-film-" + ID_film;
            var ID_modal_delete = "conferma-eliminazione-" + ID_film;
            
            var form_delete = modals.find("#" + ID_form_delete);
            var modal_delete = modals.find('#'+ID_modal_delete);
            
            modal_form_ajax_submit(form_delete, modal_delete, ID_table);
            
            var ID_form_modify = "form-modifica-film-" + ID_film;
            var ID_modal_modify = "conferma-modifica-" + ID_film;
            
            var form_modify = modals.find("#" + ID_form_modify);
            var modal_modify = modals.find('#' + ID_modal_modify);
            
            modal_form_ajax_submit(form_modify, modal_modify, ID_table);
            
            return modals;
        }

        get_presentable_data_big(item, columns_names)
        {
            return this.get_presentable_data_little(item,columns_names);
        }

        get_presentable_data_little(item, columns_names)
        {

            var image = item[0];
            var title = item[1];

            var info_container = get_image_card_empty_content();



            var year = $('<span class="image-card-info-field"><b>Anno:</b> ' + item[2] + '</span>');
            info_container.append(year);

            var nation = $('<span class="image-card-info-field"><b>Nazione:</b> ' + item[3] + '</span>');
            info_container.append(nation);

            var duration = $('<span class="image-card-info-field"><b>Durata:</b> ' + item[4] + '</span>');
            info_container.append(duration);

            var genre = $('<span class="image-card-info-field"><b>Genere:</b> ' + item[5] + '</span>');
            info_container.append(genre);

            var direction = $('<span class="image-card-info-field"><b>Regia:</b> ' + item[6] + '</span>');
            info_container.append(direction); 



            var interprets = get_image_card_long_field("Interpreti:", item[7]);
            info_container.append(interprets);

            var plot = get_image_card_long_field("Trama:", item[8]);
            info_container.append(plot);

            var trailer = get_image_card_long_field("URL trailer:", '<a href="' + item[9] + '">' + item[9] + '</a>');
            info_container.append(trailer);

            var review = get_image_card_long_field("URL recensione:", '<a href="' + item[10] + '">' + item[10] + '</a>');
            info_container.append(review);

            var relevance = $('<span class="image-card-info-field"><b>Rilevanza:</b> ' + item[11] + '</span>');
            info_container.append(relevance); 

    //        var actions = item[7];
    //        actions = actions.replace(/conferma-eliminazione/g, "conferma-eliminazione-small");
    //        actions = actions.replace(/conferma-acquisto/g, "conferma-acquisto-small");

            var actions = item[13];
            info_container.append(get_actions_container(actions));

            return get_image_card(image, title, info_container);
        }
    }

    
    $( document ).ready(function() {
                            
        console.log("In films_js");

        var form = $("#nuovo-film");
        var modal = $('#aggiungi-film');
        var table_id = "tabella_film";
        
        modal_form_ajax_submit(form, modal, table_id);
   });