//<script type="text/javascript">

/*
 * Script dinamico per la validazione della form di registrazione.
 */

<?php

    header("Content-type:text/javascript");
    require_once dirname(__FILE__) . '/../config.php';
    require_once dirname(__FILE__) . '/../utils.php';

?>
  
/**
 * Oggetto che si occupa di visualizzare le varie parti della lista (base della 
 * gerarchia)
 * @type type
 */  
class ListPiecesPresenter
{

    constructor()
    {
        this.list_presenter = null;
    }
    
    /**
     * Imposta l'oggetto list presenter
     * @param {type} list_presenter
     * @returns {undefined}
     */
    set_list_presenter(list_presenter)
    {
        this.list_presenter = list_presenter;
    }

    /**
     * Ritorna il codice html per visualizzare un singolo dato
     * @param {type} item_data Dato da visualizzare
     * @param {type} columns_names Nomi delle colonne
     * @returns {undefined}
     */
    present_item(item_data, columns_names)
    {
        
    }
    
    /**
     * Visualizza la testata della lista
     * @param {type} columns_names
     * @returns {undefined}
     */
    present_head(columns_names)
    {
        
    }
    
    /**
     * Visualizza il filtro che permette di visualizzare solo certi dati
     * @returns {undefined}
     */
    present_filter()
    {
        
    }
    
    /**
     * Visualizza il piede della lista (di solito i bottoncini per cambiare pagina)
     * @returns {undefined}
     */
    present_foot()
    {
        
    }
    
    /**
     * Ritorna il container da utilizzare per le varie parti della tabella
     * @param {type} id Id da dare al container 
     * @returns {String}
     */
    get_container(id)
    {
        return '<div id="' + id + '"></div>';
    }
    
    /**
     * Ritorna un container senza id
     * @returns {String}
     */
    get_container()
    {
        return '<div></div>';
    }
}

/**
 * Oggetto che si occupa di rappresentare un filtro per i dati (classe astratta,
 * derivare da questa per definire nuovi tipi di visualizzatori di filtri)
 * @type type
 */
class DataFilterView
{
    /**
     * Costruisce un oggetto data filter view 
     * @param {type} callback La funzione di callback da cambiare quando il filtro viene cambiato (accetta un json rappresentante il filtro)
     * @returns {DataFilterView}
     */
    constructor()
    {
        this.callback = null;
    }
    
    set_callback(callback)
    {
        this.callback = callback;
    }
    
    get_filter_view()
    {
        return "";
    }
}

/**
 * Oggetto che si occupa di rappresentare un filtro per i dati (classe astratta,
 * derivare da questa per definire nuovi tipi di visualizzatori di filtri)
 * @type type
 */
class StringDataFilterView extends DataFilterView
{
    
    get_filter_view()
    {
        var input = $('<input type="text" class="string-filter-input" placeholder="Cerca"/>');
        
        var self = this;
        
        input.on('input', function()
        {
            
            var value = input.val();
            var filter = {"string_filter": value};
            if (self.callback !== null)
            {
                self.callback(filter);
            }
            console.log(value);
            
        });
        
        return input;
    }
}

/**
 * Oggetto che si occupa di rappresentare le varie parti della lista in modo 
 * basico (stringhe)
 * @type type
 */
class BasicListPiecesPresenter extends ListPiecesPresenter
{
    
    present_item(item_data)
    {
        return "<div>" + JSON.stringify(item_data) + "</div>";
    }
    
    present_head()
    {
        return "<div>" + JSON.stringify(this.list_presenter.columns_names) + "</div>";
    }
    
    
    
    present_foot()
    {
        var items_data = this.list_presenter.items_data;
        var items_per_page = this.list_presenter.items_per_page;
        var num_pages = Math.ceil(items_data.length/items_per_page);
            
        var content = $('<div></div>');
            
        for (var i = 0; i < num_pages; i++)
        {
            var button = $('<button>' + (i+1) + '</button>');
            
            button.click( this.list_presenter.get_click_data_change_handler(i) );
            
            content.append(button);
        }
        
        return content;
    }
}

/**
 * Oggetto che rappresenta i vari elementi della lista in modo graficamente in
 * linea con lo stile del sito
 * @type type
 */
class FancyListPiecesPresenter extends ListPiecesPresenter
{
    /**
     * Costruttore
     * @param {type} items_transformer
     * @param {type} pass_to_card_on
     * @returns {FancyListPiecesPresenter}
     */
    constructor(items_transformer, pass_to_card_on, data_filter_view)
    {
        super();
        this.items_transformer = items_transformer;
        this.pass_to_card_on = pass_to_card_on;
        
        if (data_filter_view !== undefined)
        {   
            this.data_filter_view = data_filter_view;
        }
        else
        {
            this.data_filter_view = new DataFilterView();
        }
    }

    get_no_items_message()
    {
        return $('<div class="d-flex align-items-center justify-content-center"><div class="info-range-label my-3">' + this.items_transformer.get_no_items_message() + '</div></div>');
    }

    get_only_on_little_devices_class()
    {
        return "d-flex d-" + this.pass_to_card_on + "-none";
    }
    
    get_only_on_big_devices_class()
    {
        return "d-none d-" + this.pass_to_card_on + "-flex";
    }
    
    present_item(item_data)
    {
        // Parti in comune (di solito sono modali)
        
        var common = $(this.items_transformer.get_presentable_data_common(item_data, this.list_presenter.columns_names));
        
        // Visualizzazione in grande
        
        var item_data_transformed = this.items_transformer.get_presentable_data_big(item_data, this.list_presenter.columns_names, common);
        
        
        var item = $('<div class="content-card ' + this.get_only_on_big_devices_class() + '" ></div>');
        
        if (item_data_transformed instanceof Array)
        {
            

            for (var i = 0; i < this.list_presenter.columns_names.length; i++)
            {

                var cell_content = item_data_transformed[i];
                var perc = this.list_presenter.columns_percentages[i];

                var content_span = $('<span style="width:' + perc + '%; text-align:center;"></span>').append(cell_content);

                item.append(content_span);
            }
        }
        else
        {
            item.append(item_data_transformed);
        }
        
        item.click(this.items_transformer.get_item_clicked_callback(item_data));
        
        
        // Visualizzazione in piccolo
        var item_card = $('<div class="content-card ' + this.get_only_on_little_devices_class() + '" ></div>');
        item_card.append(this.items_transformer.get_presentable_data_little(item_data, this.list_presenter.columns_names, common));
        item_card.click(this.items_transformer.get_item_clicked_callback(item_data));
        

        
        

        return $("<div></div>").append(item).append(item_card).append(common);
    }
    
    present_filter(filter_callback)
    {
        var filter = $('<div class="list-head"></div>');
        
        // Dico che la view del filtro deve richiamare la funzione filter_callback(passata dall'esterno) quando
        // il filtro cambia
        this.data_filter_view.set_callback(filter_callback);
        
//        console.log(this.data_filter_view.get_filter_view());
        
        filter.append(this.data_filter_view.get_filter_view());
        
        return filter;
    }
    
    present_head()
    {
        // Visualizzo la head solo se sono nella visualizzazione in grande
        var head = $('<div class="list-head ' + this.get_only_on_big_devices_class() + '"></div>');
        
        for (var i = 0; i < this.list_presenter.columns_names.length; i++)
        {
            var cn = this.list_presenter.columns_names[i];
            var perc = this.list_presenter.columns_percentages[i];
            
            head.append('<span style="width:' + perc + '%; text-align:center;">' + cn + '</span>');
        }
        
        return head;
    }
    
    get_page_selection_callback (index, button)
    {
        var items_per_page = this.list_presenter.items_per_page;
        var sel_start = index*items_per_page;
        var sel_end = (index+1)*items_per_page;
        
        var items_data = this.list_presenter.items_data;
        
        var lp = this.list_presenter;
        
        return function()
        {
            // Qua siamo nel click
            var pos = 3;
            function wait()
            {
                pos = -pos;
                button.find(".footer-text").animate({"top": pos+"px"}, wait);
            }

            button.find(".footer-text").animate({"top": pos+"px"}, wait);
            items_data.get_data(sel_start, sel_end, 
                function (data){
                    lp.get_present_data_callback()(data);
                    
                    // Stops buttons animations
                    button.find(".footer-text").stop();
                    button.find(".footer-text").animate({"top": "0px"});
                    $("#" + lp.placeholder_id + " .footer-cell").stop();
                    $("#" + lp.placeholder_id + " .footer-cell").css({"background-color":"<?php echo $FOOTER_BUTTON_COLOR?>"});
                    button.css({"background-color":"<?php echo $FOOTER_BUTTON_COLOR_CLICKED?>"});
                    reload_buttons_colors();
                }
            );
        };
    }
    
    get_callback_present_foot()
    {
        var lp = this.list_presenter; 
        var self = this;
          
        return function(items_number)
        {
            console.log("here");
            var items_data = self.list_presenter.items_data;
            console.log("here");
            var items_per_page = self.list_presenter.items_per_page;
            var num_pages = Math.ceil(items_number/items_per_page);
            var placeholder_id = self.list_presenter.placeholder_id;
            // Visualizzazione in grande
            console.log(num_pages);
            var content = $('<div class="list-footer ' + self.get_only_on_big_devices_class() + '"></div>');

            for (var i = 0; i < num_pages; i++)
            {
                console.log("index: " + i);
                var button = $('<div class="button button-circular footer-cell"><span class="footer-text">' + (i+1) + '</span></div>');

                button.click( self.get_page_selection_callback(i, button) );

                // QUA
                if (i === 0)
                {
                    button.css({"background-color":"<?php echo $FOOTER_BUTTON_COLOR_CLICKED?>"});
                    reload_buttons_colors();
                }
                content.append(button);
            }

            // Visualizzazione in piccolo
            var content_little_screens = $('<div class="image-card-footer-container ' + self.get_only_on_little_devices_class() + '"></div>');
            var load_more_button = $('<div class="button button-load-more"><span class="footer-text">Altro...</span></div>');
            load_more_button.append('<div class="last_index d-none">' + items_per_page + '</div>');
            load_more_button.click(
                    function()
                    {
                        //$(this).find(".footer-text").stop();
                        var end = $(this).find(".last_index").text();
                        var end = Number(end) + items_per_page;
                        $(this).find(".last_index").text(end);
                        var btn = $(this);
                        items_data.get_data(0, end, function(data){
                            lp.get_present_data_callback()(data);
                            btn.find(".footer-text").stop();
                            btn.find(".footer-text").css({"left": "0px"});
                            btn.find(".footer-text").text("Altro...");
                        });

                        var pos = 3;
                        function wait()
                        {
                            pos = -pos;
                            $(this).animate({"left": pos + "px"}, "fast", wait);

                        }
                        $(this).find(".footer-text").text("Caricamento...");
                        $(this).find(".footer-text").animate({"left": pos + "px"}, "fast", wait);
                    }
            );
            content_little_screens.append(load_more_button);

            self.list_presenter.foot.empty();
            return self.list_presenter.foot.append(content).append(content_little_screens); 
        };
    }
    
    present_foot()
    {
        this.list_presenter.items_data.get_length(this.get_callback_present_foot());
        return $('<div class="image-card-footer-container">Caricamento...</div>');
    }
}

/**
 * Classe che si occupa di rendere presentabili gli oggetti.
 * Se si vuole cambiare il modo con cui gli oggetti sono rappresentati
 * creare specializzazioni di questa classe e passarle a ListPresenter
 * @type type */
class ItemsTransformer 
{
    /**
     * Funzione richiamata automaticamente quando si esegue un click su un item
     * @param {type} item L'item su cui l'utente ha cliccato
     * @returns {undefined}     */
    on_item_clicked(item)
    {
        console.log("Clicked on " + JSON.stringify(item[1]));
    }
    
    /**
     * Ritorna una closure in grado di gestire l'evento click su un item
     * @param {type} item L'item su cui l'utente ha cliccato
     * @returns {Function}     */
    get_item_clicked_callback(item)
    {
        var self = this;
        return function(event){
            var target = $(event.target);
            var target_parents = target.parents();
            
            // Se è dentro un bottone, sarà il suo gestore di eventi a 
            // occuparsi delle azioni da svolgere
            var inside_button = false;
            
            var is_button = target.hasClass("button");
            
            // Ciclo per controllare se ci si trova in un bottone
            target_parents.each(
                 function(){
                     
                    var tp = $(this);
                    if (tp.hasClass("button"))
                    {
                        inside_button = true;
                    }
                 }
            );

            if (!is_button && !inside_button)
            {
                self.on_item_clicked(item);
            }
        };
    }
    
    /**
     * Ritorna il messaggio da visualizzare in caso che non ci siano elementi
     * @returns {String}     
     */
    get_no_items_message()
    {
        return "Nessun item da visualizzare.";
    }
    
    /**
     * Funzione per dare elementi in comune tra la visualizzazione in grande ed in piccolo
     * @param {type} item Array dei dati
     * @param {type} columns_names Nomi delle colonne
     * @returns {String}     */
    get_presentable_data_common(item, columns_names)
    {
        return "";
    }
    
    /**
     * Ritorna un array di item presentabili per la visualizzazione in grande
     * @param {type} item Array di dati (una riga della lista)
     * @param {type} columns_names Nomi delle colonne
     * @returns {unresolved}
     */
    get_presentable_data_big(item, columns_names, common)
    {
        return item;
    }
    
    /**
     * Ritorna un singolo oggetto visualizzabile che riassume il contenuto dei
     * dati da visualizzare
     * @param {type} item Array di dati (una riga della lista)
     * @param {type} columns_names Nomi delle colonne 
     */
    get_presentable_data_little(item, columns_names, common)
    {
        var res = $('<div class="image-card"></div>');
        
        for (var i = 0; i < columns_names.length; i++)
        {
            var field = item[i];
            var cname = columns_names[i];
            res.append('<div class="image-card-row-container"><span class="image-card-text-container"><b>' +
                    cname + ':</b></span><span class="image-card-text-container"> ' + field + '</span></div>');
        }
        
        return res;
    }
}

function get_image_card(image, title, content)
{
    var res = $('<div class="image-card"></div>');

    var head = $('<div class="image-card-head" style="background-image:url(' +  "<?php echo $IMAGES_FOLDER?>" + image + ')"></div>');
    res.append(head);

    var head_overlay = $('<div class="image-card-head-overlay"><div>');
    head_overlay.append('<div class="image-card-title">' + title + '</div>');
    head.append(head_overlay);
    
    res.append(content);
    
    return res;
}

function get_image_card_empty_content()
{
    return $('<div class="image-card-info-container"></div>');
}

function get_image_card_long_field(label, content)
{
    var res = $('<div class="image-card-long-field-container"></div>');
    res.append($('<div class="image-card-long-field-label"><b>' + label + '</b></div>'));
    res.append($('<div class="image-card-long-field-value">' + content + '</div>'));
    return res;
}

function get_actions_container(actions)
{
    return $('<span class="image-card-info-actions">' + actions + '</span>');
}

/**
 * Funzione che trasforma i pesi in percentuali (normalizzati in modo che la
 * somma faccia 100)
 * @param {type} weights Array di pesi
 * @returns {Array|weights_to_percentage.perc} */
function weights_to_percentage(weights)
{
    var sum = 0;
    for (var w of weights)
    {
        sum += w;
    }
    var perc = [];
    for (var w of weights)
    {
        perc.push((w*100)/sum);
    }
    return perc;
}

/**
 * Oggetto che astrae il concetto di sorgente dati
 * @type type */
class ListDataSource
{
    constructor()
    {
        this.last_start = 0;
        this.last_end = 0;
    }
    /**
     * Ritorna i dati sottoforma di array, eventualmente tagliato
     * @param {type} start Indice di inizio (incluso)
     * @param {type} end Indice di fine (escluso)
     * @param {type} callback Funzione chiamata una volta completato il caricamento dei dati
     * @returns {unresolved}
     */
    get_data(start, end, callback)
    {
        this.last_start = start;
        this.last_end = end;
        return null;
    }
    
    get_last_data(callback)
    {
        this.get_data(this.last_start, this.last_end, callback);
    }
    
    get_length(callback)
    {
        return 0;
    }
    
    set_filter(filter)
    {
        return null;
    }
    
}

/**
 * Rappresenta una sorgente dati basata su un array
 * @type type */
class ArrayListDataSource extends ListDataSource
{
    /**
     * Costruttore
     * @param {type} arr Array di dati
     * @returns {ArrayListDataSource}
     */
    constructor(arr)
    {
        this.array = arr;
    }
    
    get_data(start, end, callback)
    {
        super.get_data(start, end, callback);
        callback(this.array.slice(start, end));
    }
    
    get_length(callback)
    {
        callback(this.array.length);
    }
    
    set_filter(filter)
    {
        return null;
    }
    
}

/**
 * Rappresenta una sorgente dati ajax
 * 
 * La pagina ajax deve essere invocabile con il metodo get e deve accettare
 * i seguenti parametri:
 * 
 * - start: indice di inizio dei dati
 * - end: indice di fine dei dati
 * - length: invece di ritornare i dati deve ritornare la quantità di dati
 * 
 * (vedere la pagina ajax/test_data.php per un esempio)
 * @type type */
class AjaxListDataSource extends ListDataSource
{
    /**
     * Costruttore
     * @param {type} ajax_page nome della pagina ajax
     * @returns {AjaxListDataSource}
     */
    constructor(ajax_page)
    {
        super();
        this.ajax_page = ajax_page;
        this.filter = null;
    }
    
    get_data(start, end, callback)
    {
        super.get_data(start, end, callback);
        
        var args = {"start": start, "end": end};
        
        if (this.filter !== null)
        {
            args["filter"] = JSON.stringify(this.filter);
        }
        
//        $.get(this.ajax_page, args, callback,"json");

        $.ajax({
            // URL verso la quale inviare la richiesta
            url: this.ajax_page,
            // Dati da inviare al server
            data: args,
            method: 'GET',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'GET', // For jQuery < 1.9
            // Funzione di callback una volta ricevuta la risposta
            success: function(dt) 
            {
                // Si arriva qua solo se la risposta è in formato json corretto
                callback(dt);
            },
            error: function(err)
            {
                // Si finisce qua in caso di errore http oppure in caso di converisone
                // JSON fallita
                console.log(err["responseText"]);
            }
        });
    }
    
    get_length(callback)
    {
        var args = {"length":true};
        
        if (this.filter !== null)
        {
            args["filter"] = JSON.stringify(this.filter);
        }
        
        $.get(this.ajax_page, args,callback,"json");
    }
    
    set_filter(filter)
    {
        this.filter = filter;
    }
}

/**
 * Oggetto che si occupa di visualizzare una lista di dati in modo altamente 
 * customizzabile
 */
class ListPresenter
{
    /**
     * Costruttore
     * @param {type} items_data Un oggetto di tipo DataSource che rappresenta i dati da visualizzare
     * @param {type} columns_names Lista di stringhe che rappresentano i nomi delle colonne
     * @param {type} columns_weights Lista di numeri che rapprensentano i pesi delle colonne (verranno poi trasformati in percentuali, se si vuole si possono usare direttamente delle percentuali)
     * @param {type} placeholder_id Id del div che dovrà contenere la lista (ATTENZIONE: tale div sarà svuotato!)
     * @param {type} pieces_presenter Oggetto di tipo PiecePresenter che si occupa di visualizzare le varie parti della tabella (head, content e footer)
     * @param {type} present_head Booleano che permette di decidere se visualizzare o meno la testata della tabella
     * @param {type} divide_items Booleano che dice se dividere gli items in pagine o meno
     * @param {type} items_per_page Quanti item visualizzare per pagina (si applice solo se divide_items = true)
     * @param {type} present_filter Booleano che dice se visualizzare o meno il filtro 
     * @returns {ListPresenter}
     */
    constructor(items_data, columns_names, columns_weights, placeholder_id,
                pieces_presenter, present_head, divide_items, 
                items_per_page, present_filter)
    {
        pieces_presenter.set_list_presenter(this);
        this.pieces_presenter = pieces_presenter;
        this.present_head = present_head;
        
        if (present_filter === undefined)
        {
            this.present_filter = false;
        }
        else
        {
            this.present_filter = present_filter;
        }
        
        this.divide_items = divide_items;
        this.items_per_page = items_per_page;
        this.items_data = items_data;
        this.columns_names = columns_names;
        this.placeholder_id = placeholder_id;
        this.columns_percentages = weights_to_percentage(columns_weights);
        this.head = null;
        this.content = null;
        this.foot = null;
        
        // Dice se la lista è già presentata completamente o no
        this.loaded = false;
    } 
    
    get_no_items_message()
    {
        return "Nessun item da visualizzare.";
    }
    
    get_reload_callback()
    {
        var self = this;
        
        return function(){self.reload_last_data();console.log("Reloaded");};
    }
    
    /**
     * Visualizza la lista di dati
     */
    present()
    {
        var placeholder = $("#" + this.placeholder_id);

        
        var reload_hidden_button = $('<div class="hidden-button reload"></div>');
        
        reload_hidden_button.click(this.get_reload_callback());
        
        // Svuoto il placeholder in modo da iniettare il nuovo contenuto
        placeholder.empty();
        
        placeholder.append(reload_hidden_button);
        
        var filter = $(this.pieces_presenter.get_container(this.placeholder_id + '_filter'));
        var head = $(this.pieces_presenter.get_container(this.placeholder_id + '_head'));
        var content = $(this.pieces_presenter.get_container(this.placeholder_id + '_content'));
        var foot = $(this.pieces_presenter.get_container(this.placeholder_id + '_foot'));
        
        this.head = head;
        this.foot = foot;
        this.content = content;
        
        if (this.present_filter)
        {
            placeholder.append(filter);
            filter.append(this.pieces_presenter.present_filter(this.get_filter_change_callback()));
        }
        
        if (this.present_head)
        {
            placeholder.append(head);
            head.append(this.pieces_presenter.present_head());
        }
        
        placeholder.append(content);
        
        if (this.divide_items)
        {
            placeholder.append(foot);
            foot.append(this.pieces_presenter.present_foot());
        }
        
        var lp = this;
        
        this.loaded = true;
        
        this.load_first_data();

    }
    
    /**
     * Ritorna una funzione da chiamare per visualizzare i dati (serve come supporto all'asincronia di ajax)
     * @returns {Function}
     */
    get_present_data_callback()
    {
        var lp = this;
        return function(data){lp.present_list_only(data);};
    }   
    
    /**
     * Funzione che refresha solo il contenuto della lista (non head e footer)
     * @param {type} data
     * @returns {undefined}
     */
    present_list_only(data)
    {
        // Serve per poter accedere all'oggetto pieces presenter all'interno della closure
        var pp = this.pieces_presenter;
        
        // Prima di svuotare la lista, la nascondo
        this.content.fadeOut("fast", function() {
            // Svuota la lista
            $(this).empty();
            
            if (data.length>0)
            {
                // Inserisco i dati
                for (var item of data)
                {
                    $(this).append(pp.present_item(item, this.columns_names));
                }
            }
            else
            {
                $(this).append(pp.get_no_items_message());
            }
            // Rivisualizzo la lista
            $(this).fadeIn("fast");
         });

        
    }
    
    /**
     * Funzione di supporto al cambiamento di pagina nella visualizzazione grande
     * @param {type} index Indice della pagina da visualizzare
     * @returns {Function}
     */
    get_click_data_change_handler(index)
    {
        var items_per_page = this.items_per_page;
        var sel_start = index*items_per_page;
        var sel_end = (index+1)*items_per_page;
        
        var items_data = this.items_data;
        
        var lp = this;
        
        return function()
        {
            items_data.get_data(sel_start, sel_end, lp.get_present_data_callback());
        };
    }
    
    reload_last_data()
    {
        this.items_data.get_last_data(this.get_present_data_callback());
    }
    
    load_first_data()
    {
        this.items_data.get_data(0,this.items_per_page, this.get_present_data_callback());
        
        var foot_container = $('#' + this.placeholder_id + '_foot');
        
        foot_container.empty();
        
        foot_container.append(this.pieces_presenter.present_foot());
    }
    
    get_filter_change_callback()
    {
        var self = this;
        
        return function(filter)
        {
            self.items_data.set_filter(filter);
            
            // Potrebbe essere che venga invocato il filtro di callback prima che 
            // la lista sia caricata. In tal caso vogliamo impostare il filtro per i
            // dati ma non vogliamo che vengano caricati
            if (self.loaded)
            {
                self.load_first_data();
            }
            console.log(JSON.stringify(filter));
        };
    }
}
    


