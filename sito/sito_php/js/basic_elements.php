//<script type="text/javascript">

/*
 * Script dinamico per la validazione della form di registrazione.
 */

<?php

    header("Content-type:text/javascript");
    require_once dirname(__FILE__) . '/../config.php';
    require_once dirname(__FILE__) . '/../utils.php';

?>

   

function get_js_argument(name)
{
    return JSON.parse($('#__js_arg_' + name).text());
}

// Aumenta o diminuisce la luminosità di un colore. 
// hex: il colore in formato rgb esadecimale (#xxxxxx)
// lum: di quanto cambiare la luminosità (da -1 a 1)
// Copiato da: https://stackoverflow.com/questions/16787880/how-can-we-maker-color-darker-than-given-color-in-jquery
function color_luminance(hex, lum) {
    // validate hex string
    hex = String(hex).replace(/[^0-9a-f]/gi, '');
    if (hex.length < 6) {
        hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
    }
    lum = lum || 0;
    // convert to decimal and change luminosity
    var rgb = "#", c, i;
    for (i = 0; i < 3; i++) {
        c = parseInt(hex.substr(i*2,2), 16);
        c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
        rgb += ("00"+c).substr(c.length);
    }
    return rgb;
}

// Ritorna la rappresentazione esadecimale di un colore partendo da quellargb(xxx,xxx,xxx)
// colorval: la rappresentazione rgb
function hexc(colorval) {
    var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    if (parts)
    {
        delete(parts[0]);
        for (var i = 1; i <= 3; ++i) {
            parts[i] = parseInt(parts[i]).toString(16);
            if (parts[i].length === 1) parts[i] = '0' + parts[i];
        }
        return '#' + parts.join('');
    } else {
        return colorval;
    }
}
    
function resize_grid()
{
    var width = $(".grid-image").width();
    $(".grid-image").each(
            function ()
            {
                var width = $(this).width();
                var aspect_ratio = $(this).find(".aspect-ratio").text();
                var height = aspect_ratio * width;
                $(this).height(height);
            }
    );
    
}

function on_mobile()
{
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}

function attach_fancy_grid_callbacks()
{
    if( on_mobile() ) {
        // su dispositivi mobili
        $(".grid-overlay").css({"display": "block"}); 
    }
    $(".grid-image").hover(
            function (){
                $(this).stop();
                $(this).animate({ "background-size": "110%" }, "fast");
                $(this).find(".grid-overlay").stop();
                $(this).find(".grid-overlay").fadeIn("fast");
            },
            function (){
                $(this).stop();
                $(this).animate({ "background-size": "100%" }, "fast");
                $(this).find(".grid-overlay").stop();
                $(this).find(".grid-overlay").fadeOut("fast");
            }
    );
    
    resize_grid();
    $(window).resize(resize_grid);
}

function attach_buttons_callback()
{
    var i;
    $(".button").each(
            function() {
                if (!$(this).hasClass("_attached"))
                {
                    var color = $(this).css("background-color");
                    $(this).append('<div class="color" style="display:none;">' + color + '</div>');
                    
                    function button_down(){
                            
                            var original_color = $(this).children(".color").text();
                            
                            var new_color = color_luminance(hexc(original_color), <?php echo $BUTTON_LUMINANCE_CLICK ?>);
//                            alert(original_color + " " + new_color +  " " + $(this).text());
                            $(this).finish();
                            $(this).animate({ "background-color": new_color }, "fast",
                            function()
                            {
//                                alert($(this).css("background-color"));
                            });
                        };
                    function button_up(){
//                            alert("touchend");
                            var original_color = $(this).children(".color").text();
                            $(this).finish();
                            if (!$(this).hasClass('hovered'))
                            {
//                                alert("to original color");
                                $(this).animate({ "background-color": original_color }, "fast");
                            }
                            else
                            {
                                var new_color = color_luminance(hexc(original_color), <?php echo $BUTTON_LUMINANCE_HOVER ?>);
                                $(this).animate({ "background-color": new_color }, "fast");
                            }
                        };
                    
                    if (!on_mobile())
                    {
                        $(this).hover(
                            function (){
                                var original_color = $(this).children(".color").text();
                                var new_color = color_luminance(hexc(original_color), <?php echo $BUTTON_LUMINANCE_HOVER ?>);

                                $(this).animate({ "background-color": new_color }, "fast");
                                $(this).addClass('hovered');
                            },
                            function (){
                                var original_color = $(this).children(".color").text();
                                $(this).stop();
                                $(this).animate({ "background-color": original_color }, "fast");
                                $(this).removeClass('hovered');
                         });
                        $(this).mousedown(button_down);
                        $(this).mouseup(button_up);
                     }
                     else
                     {
                        $(this).bind("touchstart", button_down);
                        $(this).bind("touchend", button_down);
                     }
                     
                     $(this).mousedown(button_down);
                     $(this).mouseup(button_up);
                    $(this).addClass("_attached");
                }
            }
            
    );
}
 
function basic_list_item(item, columns_names)
{
    return "<div>" + JSON.stringify(item) + "</div>";
}

function fancy_basic_item(item, columns_names)
{
    
}

function form_ajax_submit(form, on_success, on_error)
{
    form.submit(function(event)
        {
            // Evito che la form venga inviata nel modo classico
            event.preventDefault();

            // Recupero i dati della form
            var form_data = new FormData(form.get(0));

            // Avviso che sto caricando i dati
            form.find(".alert-msg").html('<strong>Caricamento...</strong>');

            // Rendo visibile il div, togliendogli la classe d-none
            form.find(".alert-msg").removeClass('d-none');

            // Faccio la richiesta ajax, non modificare i parametri please che se se ne modifica anche uno
            // solo salta tutto
            $.ajax({
                // URL verso la quale inviare la richiesta
                url: $(this).attr("action"),
                // Dati da inviare al server
                data: form_data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                dataType: 'json',
                type: 'POST', // For jQuery < 1.9
                // Funzione di callback una volta ricevuta la risposta
                complete: function(dt) {

                    // Funzione richiamata al termine della richiesta ajax (da controllare se la
                    // richiesta HTTP (e non ajax, quello controlliamo già) ha avuto successo, ma per
                    // ora lasciamo perdere)

                    // OTTENGO i dati della risposta

                    if (dt["responseJSON"] !== undefined)
                    {
                        var json_data = dt["responseJSON"];

                        if (json_data["result"] === "KO")
                        {
                            // Avviso degli errori che si sono verificati, andando a modificare il div con classe alert
                            form.find(".alert-msg").html('<strong>Si sono verificati degli errori:</strong><br>' + 
                                    json_data["errors"].join('<br>'));

                            // Rendo visibile il div, togliendogli la classe d-none
                            form.find(".alert-msg").removeClass('d-none');
                            
                            on_error(dt);
                        }
                        else if (json_data["result"] === "OK")
                        {
                            // De-visualizzo il messaggio di errore (così se ri apro il modale non lo vedo)
                            form.find(".alert-msg").addClass('d-none');
                            // Svuoto tutta la form
                            form[0].reset();
                            form.removeClass("was-validated");
                            
                            on_success(dt);
                        } 
                        else //Risposta errata da parte della pagina ajax
                        {
                            // Avviso degli errori che si sono verificati, andando a modificare il div con classe alert
                            form.find(".alert-msg").html('<strong>Risposta errata da parte della pagina ajax!</strong>' + 
                                JSON.stringify(dt));

                            console.log(dt["responseText"]);

                            // Rendo visibile il div, togliendogli la classe d-none
                            form.find(".alert-msg").removeClass('d-none');
                            
                            on_error(dt);
                        }
                    }
                    else
                    {
                        // Avviso degli errori che si sono verificati, andando a modificare il div con classe alert
                        form.find(".alert-msg").html('<strong>La risposta non è in formato JSON:</strong><br>' + 
                                JSON.stringify(dt));

                        // Rendo visibile il div, togliendogli la classe d-none
                        form.find(".alert-msg").removeClass('d-none');
                        
                        on_error(dt);
                    }
                }
            });
    });
}

/**
 * Funzione per eseguire il submit di una form attraverso ajax e ricevere i dati
 * di ritorno attraverso un callback.
 * 
 * @argument {type} form: la form (presa con jquery)
 * @argument {type} method: "GET" o "POST"
 * @argument {type} callback_success: metodo che viene richiamato in caso di successo passando come unico parametro
 *                                    i dati già parsati dal json
 * @argument {type} callback_error: metodo che viene richiamato in caso di insuccesso passando come unico parametro la
 *                                  stringa (raw) ritornata dalla pagina chiamata   
 * 
 * @type FormData */
function callback_form_ajax_submit(form, method, callback_success, callback_error)
{
    console.log("Entro in callbackformajaxsubmit");
    form.submit(function(event)
    {
        // Evito che la form venga inviata nel modo classico
        event.preventDefault();

        // Recupero i dati della form
        var form_data = form.serializeArray();

        // Avviso che sto caricando i dati
        form.find(".alert-msg").html('<strong>Caricamento...</strong>');

        // Rendo visibile il div, togliendogli la classe d-none
        form.find(".alert-msg").removeClass('d-none');

        $.ajax({
            // URL verso la quale inviare la richiesta
            url: $(this).attr("action"),
            // Dati da inviare al server
            data: form_data,
            method: method,
            dataType: 'json',
            type: method, // For jQuery < 1.9
            // Funzione di callback una volta ricevuta la risposta
            success: function(dt)
            {
                form.find(".alert-msg").addClass('d-none');
                form.removeClass("was-validated");
                callback_success(dt);
                form[0].reset();
            },
            error: function(err)
            {
                // Si finisce qua in caso di errore http oppure in caso di converisone
                // JSON fallita
                if (callback_error !== undefined)
                {
                    callback_error(err["responseText"]);
                    form.find(".alert-msg").addClass('d-none');
                }
                else
                {
                    // Avviso degli errori che si sono verificati, andando a modificare il div con classe alert
                    form.find(".alert-msg").html('<strong>Si è verificato un errore:</strong><br>' + 
                            err["responseText"]);

                    // Rendo visibile il div, togliendogli la classe d-none
                    form.find(".alert-msg").removeClass('d-none');
                }
            }
        });

    });
}

function modal_form_ajax_submit(form, modal, table_id)
   {
        form.submit(function(event)
            {
                // Evito che la form venga inviata nel modo classico
                event.preventDefault();

                // Id del bottone (finto) che si usa per avviare la procedura di reload della tabella (ti spiegherò...)
                var reload_button = $("#" + table_id + " .reload");

                // Recupero i dati della form
                var form_data = new FormData(form.get(0));

                // Avviso che sto caricando i dati
                form.find(".alert-msg").html('<strong>Caricamento...</strong>');

                // Rendo visibile il div, togliendogli la classe d-none
                form.find(".alert-msg").removeClass('d-none');

                // Faccio la richiesta ajax, non modificare i parametri please che se se ne modifica anche uno
                // solo salta tutto
                $.ajax({
                    // URL verso la quale inviare la richiesta
                    url: $(this).attr("action"),
                    // Dati da inviare al server
                    data: form_data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: 'POST',
                    dataType: 'json',
                    type: 'POST', // For jQuery < 1.9
                    // Funzione di callback una volta ricevuta la risposta
                    complete: function(dt) {

                        // Funzione richiamata al termine della richiesta ajax (da controllare se la
                        // richiesta HTTP (e non ajax, quello controlliamo già) ha avuto successo, ma per
                        // ora lasciamo perdere)

                        // OTTENGO i dati della risposta
                        
                        if (dt["responseJSON"] !== undefined)
                        {
                            var json_data = dt["responseJSON"];

                            if (json_data["result"] === "KO")
                            {
                                // Avviso degli errori che si sono verificati, andando a modificare il div con classe alert
                                form.find(".alert-msg").html('<strong>Si sono verificati degli errori:</strong><br>' + 
                                        json_data["errors"].join('<br>'));

                                // Rendo visibile il div, togliendogli la classe d-none
                                form.find(".alert-msg").removeClass('d-none');
                            }
                            else if (json_data["result"] === "OK")
                            {
                                // Dico che la tabella deve essere ricaricata (ma non ricarica l'intera pagina)
                                reload_button.trigger("click");
                                // De-visualizzo il messaggio di errore (così se ri apro il modale non lo vedo)
                                form.find(".alert-msg").addClass('d-none');
                                // Svuoto tutta la form nuovo-film
                                form[0].reset();
                                // De-visualizzo il modale
                                modal.modal('toggle');
                                
                                console.log("MODAL TOGGLED");
                                
                                form.removeClass("was-validated");
                            } 
                            else //Risposta errata da parte della pagina ajax
                            {
                                // Avviso degli errori che si sono verificati, andando a modificare il div con classe alert
                                form.find(".alert-msg").html('<strong>Risposta errata da parte della pagina ajax!</strong>' + 
                                    JSON.stringify(dt));

                                console.log(dt["responseText"]);

                                // Rendo visibile il div, togliendogli la classe d-none
                                form.find(".alert-msg").removeClass('d-none');
                            }
                        }
                        else
                        {
                            // Avviso degli errori che si sono verificati, andando a modificare il div con classe alert
                            form.find(".alert-msg").html('<strong>La risposta non è in formato JSON:</strong><br>' + 
                                    JSON.stringify(dt));

                            // Rendo visibile il div, togliendogli la classe d-none
                            form.find(".alert-msg").removeClass('d-none');
                        }
                    }
                });
        });
   }

/**
 * Visualizza una lista di dati 
 * @param {type} data Un array di array associativi rappresentanti i dati
 * @param {type} placeholder_id L'id del tag in cui si desidera che i dati vengano visualizzati
 * @param {type} list_data_presenter Funzione che crea l'oggetto visivo a partire dal dato
 * @returns {undefined} */
function list(data, columns_names, placeholder_id, list_data_presenter)
{
    placeholder = $("#" + placeholder_id);
    
    // Svuoto il placeholder in modo da iniettare il nuovo contenuto
    placeholder.empty();
    
    data.forEach(function (item, index)
            {
                placeholder.append(basic_list_item(item, columns_names));
            }
    );
}

function reload_buttons_colors()
{
    $(".button").each(
            function() {
                $(this).find(".color").text($(this).css('background-color'));
            });
}

function date_standard_format(raw_date)
{
    var moment_var = moment(raw_date);
    var date = moment_var.locale('it').format("dddd DD MMMM YYYY HH:mm");
    date = date.charAt(0).toUpperCase() + date.slice(1);
    
    return date;
}

function attach_user_comments_callbacks()
{
    var form = $("#_user_comment_form");
    console.log(form.length);
    form_ajax_submit(form, function(){}, function(){});
}

$( document ).ready(function() {
    
    attach_fancy_grid_callbacks();
    attach_buttons_callback();
    attach_user_comments_callbacks();
    
    // DOM LVL4
    var observer = new MutationObserver(function(mutations) {
            attach_buttons_callback();
    });

    // Notify me of everything!
    var observerConfig = {
            attributes: true, 
            childList: true, 
            characterData: true,
            subtree: true
    };

    // Node, config
    // In this case we'll listen to all changes to body and child nodes
    var targetNode = document.body;
    observer.observe(targetNode, observerConfig);
    });

