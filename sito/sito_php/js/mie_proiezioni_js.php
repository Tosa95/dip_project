//<script type="text/javascript">

<?php

    header("Content-type:text/javascript");
    require_once dirname(__FILE__) . '/../config.php';
    require_once dirname(__FILE__) . '/../utils.php';
    require_once dirname(__FILE__) . '/../db_facade.php';
    
?>
    
    /**
     * Classe che si occupa di rappresentare gli elementi della pagina mie_proiezioni.php
     * @type type
     */
    class MieProiezioniItemsTransformer extends ItemsTransformer
    {

        get_no_items_message()
        {
            return "Nessuna proiezione da visualizzare.";
        }

        get_presentable_data_common(item, columns_names)
        {
            var modals = $(item[8]);
            var ID_prenotazione = item[10].toString();
            
            var purchase_modal = modals.find("#conferma-acquisto-" + ID_prenotazione);

            console.log(ID_prenotazione);
            
            var purchase_form = modals.find("#form-conferma-acquisto-" + ID_prenotazione);
            
            
            callback_form_ajax_submit(purchase_form, "POST", function(data){
                
                //console.log(form.attr("id"));
            
                if (data["status"] === "OK")
                {
                    purchase_form.find(".alert-msg").addClass("d-none");
                    var credit_card = purchase_form.find("#credit_card").val();
                    
                    $.ajax({
                        // URL verso la quale inviare la richiesta
                        url: "<?php echo $SITE_ROOT ?>ajax/mie_proiezioni_ajax.php",
                        // Dati da inviare al server
                        data: {"ID_prenotazione": ID_prenotazione, "credit_card": credit_card, "action": "purchase"},
                        method: "POST",
                        dataType: 'json',
                        type: "POST", // For jQuery < 1.9
                        // Funzione di callback una volta ricevuta la risposta
                        success: function(dt)
                        {
                            var reload_button = $("#tabella_proiezioni .reload");
                            reload_button.trigger("click");
                            // Chiudo il modale
                            purchase_modal.modal("toggle");
                        },
                        error: function(err)
                        {
                            console.log(err["responseText"]);
                            
                        }
                    });

                }
                else
                {
                    purchase_form.find(".alert-msg").removeClass("d-none");
                    purchase_form.find(".alert-msg").html("<strong>Si è verificato un errore!</strong><br>" + data["msg"]);
                }
                

            });
            
            
            var modal_delete = modals.find("#conferma-eliminazione-" + ID_prenotazione);
            var form_delete = modals.find("#form-conferma-eliminazione-" + ID_prenotazione);
                      
            modal_form_ajax_submit(form_delete, modal_delete, "tabella_proiezioni");
            
            
            return modals;
        }

        get_presentable_data_big(item, columns_names, common)
        {
            return this.get_presentable_data_little(item, columns_names, common);
        }

        get_presentable_data_little(item, columns_names, common)
        {     
            
            var image = item[0];
            var title = item[1];

            var info_container = get_image_card_empty_content();

            var moment_var = moment(item[3],"[*] DD/MM hh:mm");
            var date = moment_var.locale('it').format("dddd DD MMMM HH:mm");
            date = date.charAt(0).toUpperCase() + date.slice(1);
            
            var moment_end = moment(item[9]);
            var end = moment_end.locale('it').format("HH:mm");

            var date_room = $('<span class="image-card-date-room"><span>' + date + ' &rarr; ' + end + '</span><span class="room-label">' + item[2] + '</span></span>');
            info_container.append(date_room);

            var seats = $('<span class="image-card-info-field"><b>Posti:</b> ' + item[6] + '</span>');
            info_container.append(seats);

            var reservation_date = $('<span class="image-card-info-field"><b>Data prenotazione:</b> ' + item[4] + '</span>');
            info_container.append(reservation_date);

            var price = $('<span class="image-card-info-field"><b>Prezzo:</b> ' + item[5] + '</span>');
            info_container.append(price);

            var actions = item[7];
            info_container.append(get_actions_container(actions));



            return get_image_card(image, title, info_container);
        }
    }
    
    

