//<script type="text/javascript">

<?php

        header("Content-type:text/javascript");
        require_once dirname(__FILE__) . '/../config.php';
        require_once dirname(__FILE__) . '/../utils.php';
        require_once dirname(__FILE__) . '/../db_facade.php';

?>

    $( document ).ready(function() {

        var id_modale = "#conferma-modifica-dati";
        var clicked_on_modify = true;
        console.log($("#registration_form").length);
        $("#registration_form").submit(function(evt)
            {
               if (clicked_on_modify)
               {
                   evt.preventDefault();
                   $(id_modale).modal("toggle");
               } 
            }
        );
        $("#confirm-modify").click(function()
        {
           clicked_on_modify = false; 
        });
        
        function set_password_modified()
        {
            if ($("#password_modified").length === 0)
            {
                $("#registration_form").append('<input type="hidden" name="password_modified" id="password_modified" value="1"/>');
            }
        }
        
        $("#password").on("input", set_password_modified);
        $("#password_check").on("input", set_password_modified);
    });
