//<script type="text/javascript">

/*
 * Script per la validazione dei campi mediante apposite classi
 */

<?php

    header("Content-type:text/javascript");
    require_once dirname(__FILE__) . '/../../config.php';
    require_once dirname(__FILE__) . '/../../utils.php';
    require_once dirname(__FILE__) . '/checks.php';
    require_once dirname(__FILE__) . '/class_associations.php';
    require_once dirname(__FILE__) . '/compare_functions.php';
?>







class CompoundCheck extends BaseCheck
{
    constructor()
    {
        super();
        // Array che contiene tutti i possibili check base da fare

        this.checks = checks;

        
    }
    
    get_all_checks_to_be_done(field)
    {
        var res = [];
        var jq_field = $(field);
        
        // Ciclo su tutti i possibili check
        for (var check_name of Object.keys(this.checks))
        {
            if (jq_field.hasClass(check_name))
            {
                res.push(this.checks[check_name]);
            }
        }
        
        
        if (jq_field.is("[required]"))
        {
            res.push(new RequiredCheck());
        }
        
        // Ciclo su tutte le classi del campo per vedere se ha delle classi di uguaglianza
        // o min o max
        var class_list = jq_field.attr('class').split(/\s+/);
        
        for (var cls of class_list)
        {
            res = res.concat(get_variable_classes_checks(cls,field));
        }
        
        return res;
    }
    
    set_custom_validation(field)
    {
        var errors = this.validate(field);
        var alert = $(field).closest("form").find(".vf-alert-" + field.id);
        
        var show_only_first_error = <?php echo ($SHOW_ONLY_FIRST_ERROR?"true":"false") ?>;
        
        if (errors === true)
        {
            if (alert.length > 0)
            {
                alert.fadeOut(function()
                {
                });
                
            }
            field.setCustomValidity("");

        }
        else
        {
            if (alert.length > 0)
            {
                if (show_only_first_error)
                {
                    
                    alert.html(errors[0]);
//                    console.log("SET: " + alert.html());
                }
                else
                {
                    alert.html(errors.join("<br>"));
                }
                if (alert.hasClass("d-none"))
                {
                    alert.hide();
                    alert.removeClass("d-none");
                }
                alert.fadeIn();
            }

            if (show_only_first_error)
            {
                field.setCustomValidity(errors[0]);
            }
            else
            {
                field.setCustomValidity(errors.join("  ---  "));
            }

        }
    }
    
    // Il metodo validate può ritornare true se la validazione ha avuto successo
    // oppure un ARRAY di stringhe di errore che descrivono il problema
    validate(field)
    {
        var checks_to_be_done = this.get_all_checks_to_be_done(field);
        var errors = [];
        for (var check of checks_to_be_done)
        {
            var check_result = check.validate(field);
//            console.log(check_result);
            // Se il check fallisce
            if (check_result !== true)
            {
//                console.log("check result: ");
//                console.log(check_result);
                errors = errors.concat(check_result);
                
            }
        }
        
        console.log("errors: ");
        console.log(errors);
        
        if (errors.length === 0)
        {
            return true;
        }
        else
        {
            return errors;
        }
    }
}
 


function re_validate_all(fields)
{
    for (var field of fields)
    {
            $(field).addClass("vf-issued-input");
//            $(field).trigger("input");
            get_validation_callback(field)();
    }
}
 
function get_validation_callback(field)
{
    return function(){
        console.log("validating " + field.name);
        var check = new CompoundCheck();
        check.set_custom_validation(field);
    };
}

function attach_validation_callbacks()
{
    // Tutti i campi da validare devono avere la classe vf
    var to_be_validated = $(".vf");
    
    for (var field of to_be_validated)
    {
        var jq_field = $(field);
        if (!jq_field.hasClass("vf-attached"))
        {
            jq_field.addClass("vf-attached");
            jq_field.on("input", get_validation_callback(field));
        }
    }
    
    var submits = $(".vf-submit");
    
    for (var submit of submits)
    {
        var jq_submit = $(submit);
        
        if (!jq_submit.hasClass("vf-attached"))
        {
            jq_submit.addClass("vf-attached");
            $(submit).click(function()
                {
                    var to_be_reset = $(".vf-reset-on-error");

                    
                    for (var field of to_be_reset)
                    {
                        var check = new CompoundCheck();
                        var errors = check.validate(field);
                        
                        if (errors!==true)
                        {
                            $(field).val("");
                        }
                    }
                    
                    re_validate_all($(this).closest("form").find(".vf"));
                    $(this).closest("form").addClass("was-validated");
                });
        }
    }
    
}
    
$(document).ready(
        function()
        {
            // Utilizzo il DOM di livello 4 per ricevere notifiche sulle modifiche al
            // documento in modo da attaccare i callback anche ai campi innestati dopo
            // il caricamento del documento
            var observer = new MutationObserver(function(mutations) {
                    attach_validation_callbacks();
            });
            
            // Notify me of everything!
            var observerConfig = {
                    attributes: true, 
                    childList: true, 
                    characterData: true,
                    subtree: true
            };

            // Node, config
            // In this case we'll listen to all changes to body and child nodes
            var targetNode = document.body;
            observer.observe(targetNode, observerConfig);
            
            // Chiamo una prima volta la funzione che attacca i callback
            attach_validation_callbacks();
            
            var suppress_bubbles = <?php echo ($SUPPRESS_BUBBLES?"true":"false") ?>;
            
            if (suppress_bubbles)
            {
                document.addEventListener('invalid', (function(){
                    return function(e) {
                      //prevent the browser from showing default error bubble / hint
                      e.preventDefault();
                      // optionally fire off some custom validation handler
                      // myValidation();
                    };
                })(), true);
            }
        }
);



