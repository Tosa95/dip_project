//<script type="text/javascript">

/*
 * Script per la validazione dei campi mediante apposite classi
 */

<?php

    require_once dirname(__FILE__) . '/../../config.php';
    require_once dirname(__FILE__) . '/../../utils.php';

?>
    
// Array contenente (quasi) tutti i possibili check. Sono esclusi quelli con classi variabili
var checks = {
            "vf-username-present": new UsernamePresentCheck("<?php echo $SITE_ROOT?>ajax/user_ajax.php"),
            
            "vf-username": new ComboCheck([new SimpleRegexCheck(/<?php echo $USERNAME_PATTERN?>/, "<?php echo $USERNAME_ERROR_MSG?>"), 
                                            new MinLengthCheck(<?php echo $MIN_USERNAME_LENGTH?>), 
                                            new MaxLengthCheck(<?php echo $MAX_USERNAME_LENGTH?>)]),
                                
            "vf-name": new ComboCheck([new SimpleRegexCheck(/<?php echo $NAME_SURNAME_PATTERN?>/, "<?php echo $NAME_SURNAME_ERROR_MSG?>"), 
                                            new MinLengthCheck(<?php echo $MIN_NAME_SURNAME_LENGTH?>), 
                                            new MaxLengthCheck(<?php echo $MAX_NAME_SURNAME_LENGTH?>)]),
                                
            "vf-email": new SimpleRegexCheck(/<?php echo $EMAIL_PATTERN?>/, "<?php echo $EMAIL_ERROR_MSG?>"),
            
            "vf-phone": new SimpleRegexCheck(/<?php echo $PHONE_PATTERN?>/, "<?php echo $PHONE_ERROR_MSG?>"),
            
            "vf-cf": new SimpleRegexCheck(/<?php echo $CF_PATTERN?>/i, "<?php echo $CF_ERROR_MSG?>"),
            
            "vf-iban": new SimpleRegexCheck(/<?php echo $IBAN_PATTERN?>/i, "<?php echo $IBAN_ERROR_MSG?>"),
            
            "vf-credit": new SimpleRegexCheck(/<?php echo $CREDIT_CARD_PATTERN?>/i, "<?php echo $CREDIT_CARD_ERROR_MSG?>"),
            
            "vf-dnum": new SimpleRegexCheck(/<?php echo $DECIMAL_NUMBER_PATTERN?>/i, "<?php echo $DECIMAL_NUMBER_ERROR_MSG?>"),
            
            "vf-inum": new SimpleRegexCheck(/<?php echo $INTEGER_NUMBER_PATTERN?>/i, "<?php echo $INTEGER_NUMBER_ERROR_MSG?>"),
            
            "vf-birth": new AgeCheck(0, <?php echo $MAX_AGE_YEARS ?>),
            
            "vf-address": new SimpleRegexCheck(/<?php echo $ADDRESS_PATTERN?>/i, "<?php echo $ADDRESS_ERROR_MSG?>"),
            
            "vf-words": new SimpleRegexCheck(/<?php echo $WORDS_PARTTERN?>/, "<?php echo $WORDS_ERROR_MSG?>"),
            
            "vf-url": new SimpleRegexCheck(/<?php echo $URL_PATTERN?>/i, "<?php echo $URL_ERROR_MSG?>"),
            
            "vf-projection-date": new AgeCheck(-1, 0),
            
            "vf-image": new ExtensionCheck(["jpg","png","gif","jpeg"]),
            
            "vf-document": new ExtensionCheck(["doc","docx","pdf"]),
            
            "vf-password": new ComboCheck([new MultipleConstraintCheck(<?php echo to_regex_js_array($PASSWORD_CONTAINS)?>, "<?php echo $PASSWORD_CONTAINS_ERROR_MESSAGE?>"),
                                                new MinLengthCheck(<?php echo $PASSWORD_MIN_LENGTH?>), 
                                                new MaxLengthCheck(<?php echo $PASSWORD_MAX_LENGTH?>)])
                                    
        };
   
   /**
    * Funzione che si occupa di trovare le classi variabili e di assegnare i relativi check
    * @param {type} cls
    * @param {type} field
    * @returns {Array|get_variable_classes_checks.res}    */
   function get_variable_classes_checks(cls, field)
   {
        var res = [];
       
        if (cls.startsWith("vf-eq-"))
        {
            res.push(new EqualityCheck(cls));
        }

        if (cls.startsWith("vf-minl-"))
        {
            var class_split = cls.split("-");
            var min_chars = Number(class_split[class_split.length-1]);
            res.push(new MinLengthCheck(min_chars));
        }

        if (cls.startsWith("vf-maxl-"))
        {
            var class_split = cls.split("-");
            var max_chars = Number(class_split[class_split.length-1]);
            res.push(new MaxLengthCheck(max_chars));
        }

        if (cls.startsWith("vf-minv-"))
        {
            var class_split = cls.split("-");
            var min_value = Number(class_split[class_split.length-1].replace('p','.'));
            res.push(new MinValueCheck(min_value));
        }

        if (cls.startsWith("vf-maxv-"))
        {
            var class_split = cls.split("-");
            var max_value = Number(class_split[class_split.length-1].replace('p','.'));
            res.push(new MaxValueCheck(max_value));
        }

        if (cls.startsWith("vf-dt-comp-"))
        {
            res.push(new DateOrderCheck());
        }

        if (cls.startsWith("vf-num-comp-"))
        {
            res.push(new ValueOrderCheck());
        }
        
        if (cls.startsWith("vf-proj-"))
        {
            res.push(new IncompatibleProjectionCheck("<?php echo $SITE_ROOT?>ajax/incompatible_projections_ajax.php"));
        }
        
        if (cls.startsWith("vf-func-"))
        {
            var func_name = get_class_term_num(cls,2);
            var func = window[func_name];
            res.push(new FunctionCheck(func));
        }
        
        return res;
   }
   
/**
 *
 * ALTRE CLASSI IMPORTANTI NON PRESENTI NELL'ASSOCIAZIONE PERCHE' DINAMICHE O SPECIALI:
 * 
 * .vf
 * 
 *  IMPORTANTE!!!!
 *  Classe speciale da mettere a tutti i campi che devono essere validati!
 *  
 *  
 * .vf-alert-<id>
 * 
 *  IMPORTANTE!!!
 *  Classe speciale che designa l'alert da utilizzare per riportare gli errori di un campo con id <id>
 * 
 * 
 * .vf-submit
 *  
 *  IMPORTANTE!!!
 *  va messo a tutti i submit delle form da validare
 * 
 * 
 * .vf-eq-<eq_class>
 * 
 *  Tutti i campi con una classe che inizia per vf-eq- e che hanno anche la stessa parte <eq_class> devono essere uguali
 * 
 * 
 * .vf-minl-<num>
 * 
 *  Il campo deve essere lungo almeno <num> caratteri
 *  
 *  
 * .vf-maxl-<num>
 * 
 *  Il campo deve essere lungo al massimo <num> caratteri
 *  
 *  
 * .vf-minv-<num>
 * 
 *  Il campo deve avere un valore numerico di almeno <num>
 *  OSS: si possono inserire valori decimali mettendo p al posto del punto decimale, es: .vf-min-12p3 --> almeno 12.3
 *  
 * .vf-maxv-<num>
 * 
 *  Il campo deve avere un valore numerico di massimo <num>
 *  OSS: si possono inserire valori decimali mettendo p al posto del punto decimale, es: .vf-mmax-12p3 --> al max 12.3 
 *  
 * .vf-dt-comp-<comp_group>-<operation>-<order>
 * 
 *  Il campo data in questione deve rispettare una certa relazione d'ordine rispetto ad un altro campo data.
 *      <comp_group> è un identificativo che identifica il singolo gruppo di ordine, può essere scelto arbitrariamente, basta che entrambi abbiano lo stesso gruppo
 *      <operation> può assumere i valori:
 *          lt: lower than
 *          bt: bigger than
 *          lte: lower than or equal
 *          bte: bigger than or equal
 *      <order> specifica l'ordine dei campi nell'operazione di confronto
 *      
 *      esempio:
 *          se il campo data1 ha la classe vf-dt-comp-group1-lt-1
 *          e il campo data2 ha la classe vf-dt-comp-group1-lt-2
 *          è come dire data1<data2
 *          
 * .vf-num-comp-<comp_group>-<operation>-<order>
 * 
 *  Il campo numerico in questione deve rispettare una certa relazione d'ordine rispetto ad un altro campo numerico.
 *      <comp_group> è un identificativo che identifica il singolo gruppo di ordine, può essere scelto arbitrariamente, basta che entrambi abbiano lo stesso gruppo
 *      <operation> può assumere i valori:
 *          lt: lower than
 *          bt: bigger than
 *          lte: lower than or equal
 *          bte: bigger than or equal
 *      <order> specifica l'ordine dei campi nell'operazione di confronto
 *      
 *      esempio:
 *          se il campo num1 ha la classe vf-num-comp-group1-lt-1
 *          e il campo num2 ha la classe vf-num-comp-group1-lt-2
 *          è come dire num1<numdata2
 *   
 *   
 * .vf-func-<nome_funzione>-<gruppo>-<numero_parametro> 
 *     
 *   Valuta la validità del campo usando una funzione javascript. 
 *      <nome_funzione> è il nome di una funzione javascript definita globalmente
 *          in ingresso avrà il valore dei campi segnati dalle classi nell'ordine sepcificato
 *          e dopo di essi anche i riferimenti ai campi stessi
 *          la funzione deve ritornare: true, se tutto è corretto
 *          una stringa o un array di stringhe identificanti il problema che si è
 *          riscontrato se vi è qualche errore
 *      <gruppo> identificatore del gruppo di input da validare insieme 
 *      <numero_parametro> a quale parametro della funzione corrisponde l'inputù
 *      
 *      es: 
 *      
 *          ho un campo num1 con la classe vf-func-somma-gruppo1-1 e un campo 
 *          num2 con la classe vf-func-somma-gruppo1-1
 *          
 *          dovrò aver definito da qualche parte la funzione
 *          
 *              function somma(num1, num2, field1, field2)
 *              {
 *                  var n1 = Number(num1);
 *                  var n2 = Number(num2);
 *                  
 *                  if (n1 + n2 < 30)
 *                  {
 *                      return true;
 *                  }
 *                  else
 *                  {
 *                     return "La somma del campo " + field1.name + " e del campo " + field2.name " deve essere < 30";
 *                  }
 *             }
 *             
 *          ovviamente si possono tralasciare gli ultimi due parametri e prendere in ingresso solo i due valori:
 *          
 *              function somma(num1, num2)
 *              {
 *                  var n1 = Number(num1);
 *                  var n2 = Number(num2);
 *                  
 *                  if (n1 + n2 < 30)
 *                  {
 *                      return true;
 *                  }
 *                  else
 *                  {
 *                     return "La somma dei campi deve essere < 30";
 *                  }
 *             } 
 *             
 *          OSS: si può applicare a più di due campi!!!
 *          OSS2: si consiglia di scrivere tutte le funzioni di comparazione in compare_functions.php in general_validation
 *          
 *          
 * .vf-reset-on-error:
 *      
 *      I campi con questa classe saranno resettati una volta cliccato su submit in caso di errore. Si usa di 
 *      solito per i campi password.
 */
    
    
/**
 * 
 * SE LE COSE NON FUNZIONANO
 * 
 * Controlla che tutti i campi abbiano il proprio alert vf-alert-<id>
 * Controlla che tutti i campi abbiano la classe vf
 * Controlla che tutti i bottoni submit abbiano la classe vf-submit
 * 
 */
