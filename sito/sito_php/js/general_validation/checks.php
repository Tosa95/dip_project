//<script type="text/javascript">

/*
 * Script per la validazione dei campi mediante apposite classi
 */

<?php

    require_once dirname(__FILE__) . '/../../config.php';
    require_once dirname(__FILE__) . '/../../utils.php';

?>

class OperandsFinder
{
    constructor(base_class_name)
    {
        this.base_class_name = base_class_name;
    }
    
    get_class_of_interest (field)
    {
        var classes = $(field).attr("class").split(/\s+/);

        for (var cls of classes)
        {
            if (cls.startsWith(this.base_class_name))
            {
                return cls;
            }
        }
    }
    
    get_operation_number (field)
    {

        var class_split = this.get_class_of_interest(field).split("-");
        var sz = class_split.length;
        return {"op": class_split[sz-2], "num": Number(class_split[sz-1])};

    }

    get_other_operand (field)
    {
        var op_num = this.get_operation_number(field);

        

        var class_split_other = this.get_class_of_interest(field).split("-").slice(0,-1);

        var other_num = ((op_num["num"] === 1)?2:1);

        class_split_other.push(other_num);

        var other_class = class_split_other.join("-");
        
        console.log(other_class);

        return $(field).closest("form").find("." + other_class)[0];
    }

    get_ordered_operands_and_operation (field)
    {
        var op_num = this.get_operation_number(field);
        var other = this.get_other_operand(field);

        if (op_num["num"] === 1)
        {
            return {"operation": op_num["op"], "op1": field, "op2": other};
        }
        else
        {
            return {"operation": op_num["op"], "op1": other, "op2": field};
        }
    }
}



function get_class_starting_with(field, start)
{
    var classes = $(field).attr("class").split(/\s+/);

    for (var cls of classes)
    {
        if (cls.startsWith(start))
        {
            return cls;
        }
    }
    
    return null;
}

function get_class_term_num(cls, term_num)
{
    return cls.split("-")[term_num];
}

function get_class_last_term(cls)
{
    return cls.split("-").pop();
}

function get_class_first_terms(cls)
{
    var split = cls.split("-");
    return split.slice(0,split.length-1).join("-");
}

function get_all_operands(cls, field)
{
    var common_name = get_class_first_terms(cls);
    
    var form = $(field).closest("form");
    
    var all_fields = form.find("*");
            
    var fields = [];
    var re = new RegExp(common_name);
    
    for (var field of all_fields)
    {
        if (field.className.match(re))
        {
            fields.push(field);
        }
    }
    
    var ordered = fields.sort(
            function(field1, field2)
            {
                var num1 = Number(get_class_last_term(get_class_starting_with(field1, common_name)));
                var num2 = Number(get_class_last_term(get_class_starting_with(field2, common_name)));
                
                return num1-num2;
            }
    );
    return [ordered, fields];
}

function get_value(field)
{
//    if (field.tagName === "INPUT")
//    {
//        return field.value;
//    }
//    else
//    {
//        return field.value;
//    }

    return field.value;
}
    
// Classe base che rappresenta un possibile check da fare sui campi
class BaseCheck
{
    // Il metodo validate può ritornare true se la validazione ha avuto successo
    // oppure un ARRAY di stringhe di errore che descrivono il problema
    validate(field)
    {
        return true;
    }
}

class FunctionCheck extends BaseCheck
{
    constructor(func)
    {
        super();
        this.func = func;
    }
    
    validate(field)
    {
        console.log("AAAAAAAAA");
        var operands = get_all_operands(get_class_starting_with(field, "vf-func-"), field);
        var ordered_fields = operands[0];
        var fields = operands[1];
        var params = [];
        
        for (var fieldv of ordered_fields)
        {
            params.push(get_value(fieldv));
        }
        
        for (var fieldv of ordered_fields)
        {
            params.push(fieldv);
        }
        
        if (!($(field).hasClass("vf-issued-input")))
        {
            re_validate_all(fields);
        }
        else
        {
            $(field).removeClass("vf-issued-input");
        }
        
        var func_res = this.func.apply(field,params);
        
        if (func_res === true)
        {
            return func_res;
        }
        else if (Array.isArray(func_res))
        {
            return func_res;
        }
        else
        {
            return [func_res];
        }
    }
}

class MinValueCheck extends BaseCheck
{
    constructor(min_val)
    {
        super();
        this.min_val = min_val;
    }
    
    validate(field)
    {
        var num = Number(get_value(field));
        if (num < this.min_val)
        {
            return ["Il campo deve avere un valore minimo di " + this.min_val];
        }
        else
        {
            return true;
        }
    }
}

class MaxValueCheck extends BaseCheck
{
    constructor(max_val)
    {
        super();
        this.max_val = max_val;
    }
    
    validate(field)
    {
        var num = Number(get_value(field));
        if (num > this.max_val)
        {
            return ["Il campo deve avere un valore massimo di " + this.max_val];
        }
        else
        {
            return true;
        }
    }
}

class MinLengthCheck extends BaseCheck
{
    constructor(min_len)
    {
        super();
        this.min_len = min_len;
    }
    
    validate(field)
    {
        if (get_value(field).length < this.min_len)
        {
            return ["Il campo deve avere una lunghezza minima di " + this.min_len + " caratteri"];
        }
        else
        {
            return true;
        }
    }
}

class MaxLengthCheck extends BaseCheck
{
    constructor(max_len)
    {
        super();
        this.max_len = max_len;
    }
    
    validate(field)
    {
        
        if (get_value(field).length > this.max_len)
        {
            return ["Il campo deve avere una lunghezza massima di " + this.max_len + " caratteri"];
        }
        else
        {
            return true;
        }
    }
}

class SimpleRegexCheck extends BaseCheck
{
    constructor(regex, err_msg)
    {
        super();
        this.regex = regex;
        this.err_msg = err_msg;
    }
    
    validate(field)
    {
        if (!this.regex.test(get_value(field)) && get_value(field) !== "")
        {
            return [this.err_msg];
        }
        else
        {
            return true;
        }
    }
}

class MultipleConstraintCheck extends BaseCheck
{
    constructor(constraints, err_msg)
    {
        super();
        this.constraints = constraints;
        this.err_msg = err_msg;
    }
    
    validate(field)
    {
        var ok = true;
        
        for (var constraint of this.constraints)
        {
            if (!constraint.test(get_value(field)))
            {
                ok = false;
            }
        }
        
        
        if (!ok)
        {
            return [this.err_msg];
        }
        else
        {
            return true;
        }
    }
}

class UsernamePresentCheck extends BaseCheck
{
    constructor(ajax_page)
    {
        super();
        
        this.ok = true;
        this.prev_state = true;
        this.ajax_page = ajax_page;
    }
    
    set_ok(value)
    {
        this.ok = value;
    }
    
    validate(field)
    {
        var username = get_value(field);
        var self = this;
        
        if (username !== "")
        {
            if (!($(field).hasClass("vf-issued-input")))
            {
                $.getJSON(this.ajax_page, 
                    {"username":username}, function (data){

                        var present = data["user_present"];
                        if (present)
                        {
                            self.set_ok(false);
                        }
                        else
                        {
                            self.set_ok(true);

                        }

                        $(field).addClass("vf-issued-input");
                        $(field).trigger("input");

                });

                return this.prev_state;
            }
            else
            {
                $(field).removeClass("vf-issued-input");
                if (this.ok)
                {
                    var res = true;
                    this.prev_state = res;
                    return res;
                }
                else
                {
                    var res = ["Il nome utente scelto è già presente"];
                    this.prev_state = res;
                    return res;
                }
            }
        }
        else
        {
            return true;
        }
    }
}

class IncompatibleProjectionCheck extends BaseCheck
{
    constructor(ajax_page)
    {
        super();
        this.ajax_page = ajax_page;
    }
    
    
    
    validate(field)
    {
        var field_class = get_class_starting_with(field,"vf-proj-");
        var type = get_class_last_term(field_class);
        var other_class = get_class_first_terms(field_class) + "-" + (type==="date"?"room":"date");
        
        var other_field = $(field).closest("form").find("."+other_class)[0];
        
        var date;
        var ID_room;
        
        var ID_film = null;
        var ID_proiezione = null;
        
        if ($(field).closest("form").find(".vf-proj-film").length > 0)
        {
            ID_film = $(field).closest("form").find(".vf-proj-film").val();
        }
        
        if ($(field).closest("form").find(".vf-proj-id").length > 0)
        {
            ID_proiezione = $(field).closest("form").find(".vf-proj-id").val();
        }
        
        console.log(ID_film, ID_proiezione);
        
        if (type==="date")
        {
            date = get_value(field);
            ID_room = Number(get_value(other_field));
        }
        else
        {
            date = get_value(other_field);
            ID_room = Number(get_value(field));
        }
        
        console.log(date, ID_room, type, other_class, other_field, get_value(other_field));

        if ($(field).closest("form").find(".vf-temp-error").length===0)
        {
            $(field).closest("form").append('<div class="d-none vf-temp-error"></div>');
        }

        if (!($(field).hasClass("vf-issued-input")))
        {
            
            function manage_data(data)
            {
                var num_conflicting = data.length;
                    console.log("num_conflicting: " + num_conflicting);
                    console.log(data);

                    var conflicts = [];
                    
                    
                    
                    for (var conflict of data)
                    {
                        var moment_var = moment(conflict["data_ora"]);
                        var date = moment_var.locale('it').format("dddd DD MMMM HH:mm");
                        date = date.charAt(0).toUpperCase() + date.slice(1);
                        
                        conflicts.push("<li><em>" + conflict["titolo"] + "</em>, " + conflict["nome_sala"] + ", " + date + "</li>");
                    }
                    
                    if (num_conflicting > 0)
                    {
                        $(field).closest("form").find(".vf-temp-error").html("Questa scelta di sala e orario genera i seguenti conflitti:<br><br><ul>" + conflicts.join("<br>") + "</ul>");
                    }
                    else
                    {
                        $(field).closest("form").find(".vf-temp-error").html("");
                    }
                    
                    re_validate_all([field, other_field]);
            }
            
            if (ID_film !== null)
            {
                $.ajax({
                    // URL verso la quale inviare la richiesta
                    url: this.ajax_page,
                    // Dati da inviare al server
                    data: {"ID_sala":ID_room, "data_ora":date, 
                    "ID_film":ID_film},
                    method: 'GET',
                    dataType: 'json',
                    type: 'GET', // For jQuery < 1.9
                    // Funzione di callback una volta ricevuta la risposta
                    success: manage_data,
                    error: function(err)
                    {
                        // Si finisce qua in caso di errore http oppure in caso di converisone
                        // JSON fallita
                        console.log("Errore nella richiesta di incompatibilità tra proiezioni", err["responseText"]);
                    }
                });
//                $.getJSON(this.ajax_page,{"ID_sala":ID_room, "data_ora":date, 
//                    "ID_film":ID_film}, manage_data);
            }
            else
            {
                console.log(ID_room,date,ID_proiezione);
                
                $.ajax({
                    // URL verso la quale inviare la richiesta
                    url: this.ajax_page,
                    // Dati da inviare al server
                    data: {"ID_sala":ID_room, "data_ora":date, 
                    "ID_programmazione":ID_proiezione},
                    method: 'GET',
                    dataType: 'json',
                    type: 'GET', // For jQuery < 1.9
                    // Funzione di callback una volta ricevuta la risposta
                    success: manage_data,
                    error: function(err)
                    {
                        // Si finisce qua in caso di errore http oppure in caso di converisone
                        // JSON fallita
                        console.log("Errore nella richiesta di incompatibilità tra proiezioni", err["responseText"]);
                    }
                });
                
//                $.getJSON(this.ajax_page,{"ID_sala":ID_room, "data_ora":date, 
//                    "ID_programmazione":ID_proiezione}, manage_data);
            }
            
            
        } 
        else
        {
            $(field).removeClass("vf-issued-input");
        }
        
        if ($(field).closest("form").find(".vf-temp-error").text()!=="")
        {
            return [$(field).closest("form").find(".vf-temp-error").html()];
        }

        return true;

    }
}


class EqualityCheck extends BaseCheck
{
    constructor(eq_class)
    {
        super();
        this.eq_class = eq_class;
    }
    
    validate(field)
    {
        var fields_with_eq_class = $(field).closest("form").find("." + this.eq_class);
        
        var value = get_value(field);
        var equal = true;
        
        for (var eq_field of fields_with_eq_class)
        {
            if (get_value(eq_field) !== value)
            {
                equal = false;
            }
        }
        
        // Devo aggiornare le informazioni di validità anche di tutti gli altri campi
        // con la  stessa classe di uguaglianza. Per fare ciò andrò a triggerare l'evento
        // input su di essi. Uso la classe vf-issued-input per evitare dei cicli infiniti:
        // se arrivo qua e il mio oggetto ha la classe vf-issued-input significa che arrivo
        // da un evento triggerato automaticamente e quindi non devo validare anche gli altri
        // di nuovo!!!
        if (!($(field).hasClass("vf-issued-input")))
        {
            re_validate_all(fields_with_eq_class);
        }
        else
        {
            $(field).removeClass("vf-issued-input");
        }
        
        if (equal)
        {
           return true;
        }
        else
        {
            var eq_class_split = this.eq_class.split("-");
            var class_name = eq_class_split[eq_class_split.length-1];
            return ["I campi " + class_name + " devono coincidere!"];
        }
        
        
    }
}

class RequiredCheck extends BaseCheck
{
    validate(field)
    {
        if (get_value(field).length===0)
        {
            return ["Questo campo è obbligatorio!"];
        }
        else
        {
            return true;
        }
    }
}

class ExtensionCheck extends BaseCheck
{
    constructor(extensions)
    {
        super();
        this.extensions = extensions;
    }
    
    validate(field)
    {
        var ok = false;
        
        if (get_value(field).length === 0)
        {
            return true;
        }
        
        var extension = get_value(field).split('\\').pop().split('.').pop();
        
        console.log(extension);
        console.log(this.extensions);
        
        for (var supp_ext of this.extensions)
        {
            if (supp_ext.toLowerCase() === extension.toLowerCase())
            {
                ok = true;
            }
        }
        
        if (!ok)
        {
            return ["Sono ammessi solo file con estensione " + this.extensions.join(", ")];
        }
        else
        {
            return true;
        }
    }
}

class ComboCheck extends BaseCheck
{
    constructor(checks)
    {
        super();
       
        this.checks = checks;
    }
    
    // Il metodo validate può ritornare true se la validazione ha avuto successo
    // oppure un ARRAY di stringhe di errore che descrivono il problema
    validate(field)
    {
        var checks_to_be_done = this.checks;
        var errors = [];
        for (var check of checks_to_be_done)
        {
            var check_result = check.validate(field);
            // Se il check fallisce
            if (check_result !== true)
            {
                errors = errors.concat(check_result);
                
            }
        }
        
        if (errors.length === 0)
        {
            return true;
        }
        else
        {
            return errors;
        }
    }
}

class ValueOrderCheck extends BaseCheck
{
    validate(field)
    {
        var ordered = new OperandsFinder("vf-num-comp-").get_ordered_operands_and_operation(field);
        
        console.log("ORDEREDDDD");
        console.log(ordered);
        
        var err = null;
        
        var num1 = Number(ordered["op1"].value);
        var num2 = Number(ordered["op2"].value);
        
        var name1 = ordered["op1"].name;
        var name2 = ordered["op2"].name;
        
        switch(ordered["operation"])
        {
            case 'lt':
                if (!(num1 < num2))
                {
                    err = "Il campo " + name1 + " deve avere un valore minore di quello del campo " + name2;
                }
                break;
            case 'bt':
                if (!(num1 > num2))
                {
                    err = "Il campo " + name1 + " deve avere un valore maggiore di quello del campo " + name2;
                }
                break;
            case 'lte':
                if (!(num1 <= num2))
                {
                    err = "Il campo " + name1 + " deve avere un valore minore o uguale a quello del campo " + name2;
                }
                break;
            case 'bte':
                if (!(num1 >= num2))
                {
                    err = "Il campo " + name1 + " deve avere un valore maggiore o uguale a quello del campo " + name2;
                }
                break;
        }
        
        
        if (!($(field).hasClass("vf-issued-input")))
        {
            re_validate_all([ordered["op1"], ordered["op2"]]);
        }
        else
        {
            $(field).removeClass("vf-issued-input");
        }
        
        if (err === null)
        {
            return true;
        }
        else
        {
            return [err];
        }
        
        
    }
}

class DateOrderCheck extends BaseCheck
{
    
    validate(field)
    {
        var ordered = new OperandsFinder("vf-dt-comp-").get_ordered_operands_and_operation(field);
        
//        console.log("ORDEREDDDD");
//        console.log(ordered);
        
        var err = null;
        
        var d1 = new Date(ordered["op1"].value);
        var d2 = new Date(ordered["op2"].value);
        
        var n1 = ordered["op1"].name;
        var n2 = ordered["op2"].name;
        
        switch(ordered["operation"])
        {
            case 'lt':
                if (!(d1 < d2))
                {
                    err = "Il campo " + n1 + " deve avere una data minore di quella del campo " + n2;
                }
                break;
            case 'bt':
                if (!(d1 > d2))
                {
                    err = "Il campo " + n1 + " deve avere una data maggiore di quella del campo " + n2;
                }
                break;
            case 'lte':
                if (!(d1 <= d2))
                {
                    err = "Il campo " + n1 + " deve avere una data minore o uguale a quella del campo " + n2;
                }
                break;
            case 'bte':
                if (!(d1 >= d2))
                {
                    err = "Il campo " + n1 + " deve avere una data maggiore o uguale a quella del campo " + n2;
                }
                break;
        }
        
        
        if (!($(field).hasClass("vf-issued-input")))
        {
            re_validate_all([ordered["op1"], ordered["op2"]]);
        }
        else
        {
            $(field).removeClass("vf-issued-input");
        }
        
        // Se uno dei due è vuoto non ha senso ritornare errori
        if (ordered["op1"].value === "" || ordered["op2"].value === "")
        {
            return true;
        }
        
        if (err === null)
        {
            return true;
        }
        else
        {
            return [err];
        }
        
        
    }
}

class AgeCheck extends BaseCheck
{
    constructor(min_age, max_age)
    {
        super();
        this.max_age = max_age;
        this.min_age = min_age;
    }
    
    validate(field)
    {
        var input_date = new Date(get_value(field));
        var current_date = new Date();
        
        var time_diff = current_date.getTime() - input_date.getTime();
        var diff_years = time_diff / (1000 * 3600 * 24 * 365);
        
        if (diff_years > this.max_age)
        {
            return ["La data che hai inserito è troppo remota, si accettano solo date con distanza massima di " + this.max_age + " ann" + (this.max_age===1?"o":"i") + " dal momento attuale"];
        }
        
        if (diff_years < this.min_age)
        {
            return ["La data che hai inserito è troppo futura, si accettano solo date con distanza massima di " + -this.min_age + " ann" + (-this.min_age===1?"o":"i") + " nel futuro rispetto al momento attuale"];
        }
        
        return true;
        
    }
}