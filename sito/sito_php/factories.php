<?php

    require_once dirname(__FILE__) . '/config.php';
    require_once dirname(__FILE__) . '/db_facade.php';
    require_once dirname(__FILE__) . '/crypto.php';
    require_once dirname(__FILE__) . '/view/projection_list.php';
    require_once dirname(__FILE__) . '/view/document_elements.php';
    require_once dirname(__FILE__) . '/view/reservations.php';
    
    /**
     * Factories che consentono la costruzione di oggetti delle seguenti classi:
     * DBFacade, DocumentElements, ProjectionListPresenter, ReservationsPresenter
     */
    
    function get_db_facade()
    {
        global $DB_HOST, $DB_NAME, $DB_USER, $DB_PSW, $SHOW_PROJECTION_FOR_DAYS, $PASSWORD_RESET_KEY_VALIDITY;
        return new DBFacade($DB_HOST, $DB_NAME, $DB_USER, $DB_PSW, $SHOW_PROJECTION_FOR_DAYS, $PASSWORD_RESET_KEY_VALIDITY);
    }
    
    function get_document_elements()
    {
        global $BG_COLOR, $SITE_ROOT, $CONTAINER_FLUID, $IMAGES_FOLDER, $GATHER_USER_COMMENTS;
        return new DocumentElements(get_db_facade(), $BG_COLOR, isset($_SESSION["username"])?$_SESSION["username"]:NULL, 
                $SITE_ROOT, $CONTAINER_FLUID, $IMAGES_FOLDER, $GATHER_USER_COMMENTS);
    }
    
    function get_projection_list_presenter()
    {
        global $TIME_TABLE_CLASSES, $TITLE_COLOR, $TIME_TABLE_FIRST_COLUMN_PERCENTAGE,
                $CONTAINER_FLUID, $IMAGES_FOLDER, $SITE_ROOT, $FILM_CARD_ASPECT_RATIO;
        return new ProjectionListPresenter(get_db_facade(), $TIME_TABLE_CLASSES, 
                $TITLE_COLOR, $TIME_TABLE_FIRST_COLUMN_PERCENTAGE, $CONTAINER_FLUID,
                $IMAGES_FOLDER, $SITE_ROOT, $FILM_CARD_ASPECT_RATIO);
    }
    
    function get_reservations_presenter()
    {
        global $SITE_ROOT;
        return new ReservationsPresenter(get_db_facade(), $SITE_ROOT);
    }

    function get_password_utils()
    {
        global $HASH_ALGORITHM_REPETITIONS, $HASH_ALGORITHM, $SALT_LENGTH, $PASSWORD_RESET_KEY_SECRET, $PAYMENT_SERVICE_HASH_ALGORITHM, $PAYMENT_SERVICE_SECRET;
        
        return new PasswordUtils($HASH_ALGORITHM_REPETITIONS, $HASH_ALGORITHM, $SALT_LENGTH, $PASSWORD_RESET_KEY_SECRET, $PAYMENT_SERVICE_SECRET, $PAYMENT_SERVICE_HASH_ALGORITHM);
    }
