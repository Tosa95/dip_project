<?php

    require_once dirname(__FILE__). '/factories.php';
    require_once dirname(__FILE__). '/config.php';
    
    session_start();
    
    if (isset($_SESSION["username"]))
    {
        $dbf = get_db_facade();
        
        $utype = $dbf->get_user_type($_SESSION["username"]);
        
        $de = get_document_elements();
        
        if ($utype >= 2)
        {
        
            $plp = get_projection_list_presenter();
            
            $script = incorporate_js("classifier_js.php") . incorporate_js("images_js.php");

            $content = '<div class="d-flex align-items-center justify-content-center"><img id="current" class="img-fluid" style="width:400px"/></div>';

            $content .= '<div class="d-flex align-items-center justify-content-center img-thumbnail-cont"><img id="current-thumbnail" src="http://localhost:5000/images/1800" class="img-fluid img-thumbnail"/></div>';

            
            $content .= '<div class="container" id="categories"></div>';
            
            $content .= '<div class="container" id="stats"></div>';
            
            $content .= '<div class="container" id="img-preload"></div>';


            echo $de->info_page_template("Classificazione", $content . $script, TRUE);
        }
        else
        {
            echo $de->info_page_template("Classificazione", "Non hai i privilegi per classificare le immagini", TRUE);
        }
    }
    else
    {
        header('Location: login.php');
    }

