<?php

    require_once dirname(__FILE__). '/../factories.php';
    require_once dirname(__FILE__). '/../db_facade.php';
    require_once dirname(__FILE__). '/../utils.php';
    
    header("Content-type:text/json");
    
    session_start();
    
    $dbf = get_db_facade();
    
    if (isset($_SESSION["username"]) && $dbf->get_user_type($_SESSION["username"]) == UserType::ADMINISTRATOR)
    {
        $username = $_SESSION["username"];
        
        $films = $dbf->get_all_films();
        
        $data = projections_to_table($films);
        
        if (!isset($_POST["action"]))
        {
            $data = get_filtered_data(get_db_facade());

            if (!isset($_GET["length"]))
            {
                $start = 0;
                if (isset($_GET["start"]))
                {
                    $start = $_GET["start"];
                } 
                else 
                {
                    $start = 0;
                }

                if (isset($_GET["end"]))
                {
                    $end = $_GET["end"];
                } 
                else 
                {
                    $end = null;
                }

                if (is_null($end))
                {
                    echo json_encode(array_slice($data, $start));
                }
                else
                {
                    echo json_encode(array_slice($data, $start, $end-$start));
                }
            }
            else if(isset($_GET["length"]))
            {
                echo json_encode(count($data));
            }
        }
        else # Se l'amministratore ha eseguito il submit di qualche form
        {
            
            if ($_POST["action"] === "insert" )
            {
                insert_new_projection($dbf);
            }
            else if ($_POST["action"] === "delete")
            {
                delete_projection($dbf);
            }
            else if ($_POST["action"] === "modify")
            {
                modify_projection($dbf);
            }
          
        }
    }
    
    function get_filtered_data($dbf)
    {
        if (isset($_GET["filter"]) && !is_null($_GET["filter"]))
        {
            $temp = array();
            $filter_json = json_decode($_GET["filter"], true);
            $filter_string = $filter_json["string_filter"];
            $only_current = $filter_json["only_current"];
            $filter_begin = isset($filter_json["begin"])?$filter_json["begin"]:"";
            $filter_end = isset($filter_json["end"])?$filter_json["end"]:"";
            

            $films = $dbf->get_all_films($only_current, $filter_string, $filter_begin, $filter_end);

            return projections_to_table($films);

        }
        else
        {
            return projections_to_table($dbf->get_all_films());
        }
    }
    
    function insert_new_projection($dbf)
    {
        global $SITE_ROOT;
        # Devo inserire il film nel DB. Ritorno una risposta json come segue:
        # {
        #       result: "OK" ==> tutto a posto | "KO" ==> Si sono verificati errori
        #       errors: elenco (array) degli errori che si sono verificati (non c'è se result è "OK")
        # }

        # Contiene l'elenco degli errori che si sono verificati
        $errors = array();

        $target_dir = $_SERVER["DOCUMENT_ROOT"] . $SITE_ROOT . "images/";

        $poster_file_type = strtolower(pathinfo(basename($_FILES["img-locandina"]["name"]), PATHINFO_EXTENSION));
        $poster_file_name = str_replace(" ", "_", clean_strange_chars(strtolower($_POST["titolo"]))) . "." . $poster_file_type;

        $poster_target_file = $target_dir . $poster_file_name;
        $upload_ok = 1;

        # Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["img-locandina"]["tmp_name"]);
        if($check !== false) 
        {
            $upload_ok = 1;
        } 
        else 
        {
            $errors[] = "Il file non è un'immagine!";
            $upload_ok = 0;
        }

        # Allow certain file formats
        if($poster_file_type != "jpg" && $poster_file_type != "png" && $poster_file_type != "jpeg"
        && $poster_file_type != "gif" ) 
        {
            $errors[] = "Sono permesse solo le estensioni JPG, JPEG, PNG e GIF.";
            $upload_ok = 0;
        }

        # Controllo se il film che sto per aggiungere è già presente
        if ($dbf->film_present($_POST["titolo"]))
        {
            $errors[] = "Il film è già presente nel database.";
            $upload_ok = 0;
        }

        # Check if $upload_ok is set to 0 by an error
        if ($upload_ok == 0) 
        {
            $errors[] = "Spiacenti, il tuo file non è stato caricato.";

            # Stampo perchè sono giunto ad una situazione in cui non posso proseguire
            # Result a ko indica che ci sono stati degli errori
            echo json_encode(array("result"=>"KO", "errors"=>$errors));
        } 
        else # se è tutto ok provo a salvare l'immagine di locandina
        {
            if (move_uploaded_file($_FILES["img-locandina"]["tmp_name"], $poster_target_file)) 
            {

                $dbf->add_film(user_input_to_db($_POST["titolo"]), user_input_to_db($_POST["anno"]), user_input_to_db($_POST["nazione"]), user_input_to_db($_POST["durata"]),
                            user_input_to_db($_POST["genere"]), user_input_to_db($_POST["regia"]), user_input_to_db($_POST["interpreti"]), user_input_to_db($_POST["trama"]),
                            user_input_to_db($_POST["urltrailer"]), user_input_to_db($_POST["urlrecensione"]), user_input_to_db($_POST["rilevanza"]), $poster_file_name); 

                # Avviso dell'avvenuto inserimento
                echo json_encode(array("result"=>"OK"));
            }
            else 
            {

                $errors[] = "Si è verificato un errore nel caricamento dell'immagine di copertina.";

                # Stampo perchè sono giunto ad una situazione in cui non posso proseguire
                # Result a ko indica che ci sono stati degli errori
                echo json_encode(array("result"=>"KO", "errors"=>$errors));
            }
        }
    }
    
    function delete_image($img_name)
    {
        try{
            unlink($img_name);
        }
        catch(Exception $err)
        {

        }
    }
    
    function delete_projection($dbf)
    {
        global $SITE_ROOT;
        
        

        $target_dir = $_SERVER["DOCUMENT_ROOT"] . $SITE_ROOT . "images/";

        $old_film_data = $dbf->get_film($_POST["ID"]);

        $img_locandina = $old_film_data["nome_locandina"];
        delete_image($target_dir . $img_locandina);
        
        $dbf->delete_film($_POST["ID"]);
        
        echo json_encode(array("result"=>"OK"));
    }
    
    function modify_projection($dbf)
    {
        global $SITE_ROOT;
        
        $target_dir = $_SERVER["DOCUMENT_ROOT"] . $SITE_ROOT . "images/";
        
        $old_film_data = $dbf->get_film($_POST["ID"]);
        
        $img_locandina = $old_film_data["nome_locandina"];
        
        $errors = array();
        
        if(!empty($_FILES["img-locandina"]["tmp_name"]))
        {
            $upload_ok = 1;
            
            $image_file_type = strtolower(pathinfo(basename($_FILES["img-locandina"]["name"]), PATHINFO_EXTENSION));
            $image_file_name = str_replace(" ", "_", clean_strange_chars(strtolower($_POST["titolo"]))) . rand(1, 20000) . "-img." . $image_file_type;

            $image_target_file = $target_dir . $image_file_name;


            # Check if image file is a actual image or fake image
            $check_image = getimagesize($_FILES["img-locandina"]["tmp_name"]);

            if($check_image) 
            {
            } 
            else 
            {
                $errors[] = "Il file dell'immagine deve essere un'immagine!";
                $upload_ok = 0;
            }

            # Allow certain file formats
            if($image_file_type != "jpg" && $image_file_type != "png" && $image_file_type != "jpeg"
            && $image_file_type != "gif" ) 
            {
                $errors[] = "Sono permesse solo le estensioni JPG, JPEG, PNG e GIF per l'immagine.";
                $upload_ok = 0;
            }

            # Se finora non ci sono stati errori, provo a salvare l'immagine
            if($upload_ok)
            {

                if (move_uploaded_file($_FILES["img-locandina"]["tmp_name"], $image_target_file))
                {
                    delete_image($target_dir . $img_locandina);
                    $img_locandina = $image_file_name;
                    
                } 
                else
                {
                    $errors[] = "Impossibile caricare la nuova immagine"; 
                }

            }
        }
        
        if (count($errors) == 0)
        {
            
            $dbf->modify_film(
                        $_POST["ID"],
                        user_input_to_db($_POST["titolo"]),
                        user_input_to_db($_POST["anno"]),
                        user_input_to_db($_POST["nazione"]),
                        user_input_to_db($_POST["durata"]),
                        user_input_to_db($_POST["genere"]),
                        user_input_to_db($_POST["regia"]),
                        user_input_to_db($_POST["interpreti"]),
                        user_input_to_db($_POST["trama"]),
                        user_input_to_db($_POST["urltrailer"]),
                        user_input_to_db($_POST["urlrecensione"]),
                        user_input_to_db($_POST["rilevanza"]),
                        $img_locandina
                    );

            echo json_encode(array("result"=>"OK"));
        }
        else
        {
            echo json_encode(array("result"=>"KO","errors"=>$errors));
        }
    }
    
    function get_delete_modal ($film, $modal_id)
    {
        global $SITE_ROOT;
        
        $ID_film = $film["ID"];
        $ajax_page = $SITE_ROOT . "ajax/all_films_ajax.php"; 
        $modal_delete_body = '<div class="form-row">
                                <div class="form-group col-12 alert alert-danger d-none">
                                </div>
                                <div class="form-group col-12">' .
                                    'Sei sicuro di voler eliminare il film "' . db_to_html($film["titolo"]) . '"?' . '
                                    Ricorda che cos&igrave; facendo tutti coloro che hanno prenotato/acquistato biglietti
                                    per tale film <strong>dovranno essere risarciti!</strong> <br>
                                    <strong>Questa &egrave; una misura molto drastica che andrebbe presa solo in situazioni eccezionali.</strong> <br>
                                    Se vuoi solo modificare un film non eliminarlo per poi ricrearlo, ma clicca
                                    su modifica!
                                </div>

                                <input name="ID" value="' . db_to_field_value($ID_film) . '" type="hidden"/>
                                <input type="hidden" name="action" id="action" value="delete" />

                                <div class="form-group col-12 alert alert-danger d-none alert-msg">
                                    <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
                                </div>

                             </div>';
        $modal_delete_footer = '<button id="delete_button" type="submit" class="button button-danger">Elimina</button>';

        return bootstrap_modal_with_form($modal_id, 
                                                "Elimina Film", $modal_delete_body, 
                                                $modal_delete_footer, "form-elimina-film-" . $ID_film, 
                                                $ajax_page, "post", 'enctype="multipart/form-data"');

    }

    
    function get_modify_modal($film, $modal_id)
    {
        global $SITE_ROOT;
        
        $ID_film = $film["ID"];
        $ajax_page = $SITE_ROOT . "ajax/all_films_ajax.php"; 
        
        $modal_body = '<div class="form-row">
                        <input type="hidden" name="action" id="action" value="modify" />
                        <input type="hidden" name="ID" id="ID" value="' . db_to_field_value($ID_film) . '" />
                        <div class="form-group col-12">
                            <label for="titolo">Titolo</label>
                            <input name="titolo" id="titolo" type="text" class="form-control vf" placeholder="Titolo" value="' . db_to_field_value($film["titolo"]) . '" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-titolo vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="anno">Anno</label>
                            <input name="anno" id="anno" type="text" class="form-control vf vf-inum vf-minv-1896 vf-maxv-2099" placeholder="Anno" value="' . db_to_field_value($film["anno"]) . '" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-anno vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="nazione">Nazione</label>
                            <input name="nazione" id="nazione" type="text" class="form-control vf vf-words" placeholder="Nazione" value="' . db_to_field_value($film["nazione"]) . '" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-nazione vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="durata">Durata</label>
                            <input name="durata" id="durata" type="text" class="form-control vf vf-inum" placeholder="Durata" value="' . db_to_field_value($film["durata"]) . '" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-durata vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="genere">Genere</label>
                            <input name="genere" id="genere" type="text" class="form-control vf vf-words" placeholder="Genere" value="' . db_to_field_value($film["genere"]) . '" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-genere vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="regia">Regia</label>
                            <input name="regia" id="regia" type="text" class="form-control vf" placeholder="Regia" value="' . db_to_field_value($film["regia"]) . '" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-regia vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="interpreti">Interpreti</label>
                            <textarea name="interpreti" id="interpreti" class="form-control vf" placeholder="Interpreti" autocomplete="off" required>' . db_to_field_value($film["interpreti"]) . '</textarea>
                            <div class="alert alert-danger d-none vf-alert-interpreti vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="trama">Trama</label>
                            <textarea name="trama" id="trama" class="form-control vf" placeholder="Trama" autocomplete="off" required>'. db_to_field_value($film["trama"]) .'</textarea>
                            <div class="alert alert-danger d-none vf-alert-trama vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="urltrailer">URL Trailer</label>
                            <input name="urltrailer" id="urltrailer" type="text" class="form-control vf vf-url" placeholder="URL Trailer" value="' . db_to_field_value($film["url_trailer"]) . '" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-urltrailer vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="urlrecensione">URL Recensione</label>
                            <input name="urlrecensione" id="urlrecensione" type="text" class="form-control vf vf-url" placeholder="URL Recensione" value="' . db_to_field_value($film["url_recensione"]) . '" autocomplete="off" required>
                            <div class="alert alert-danger d-none vf-alert-urlrecensione vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="img-locandina">Immagine locandina</label>
                            <input name="img-locandina" id="img-locandina" type="file" class="form-control vf vf-image">
                            <div class="alert alert-danger d-none vf-alert-img-locandina vf-validation-alert">
                                
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="rilevanza">Rilevanza</label>
                             <input name="rilevanza" id="rilevanza" type="text" class="form-control vf vf-inum vf-minv-1 vf-maxv-10" value="' . db_to_field_value($film["rilevanza"]) . '" autocomplete="off" required>
                             <div class="alert alert-danger d-none vf-alert-rilevanza vf-validation-alert">
                                
                            </div>
                        </div>
                        
                        <div class="form-group col-12 alert alert-danger d-none alert-msg">
                            <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
                        </div>

                    </div>';
                
        $modal_footer = '<button type="submit" id="add-film" class="button button-danger vf-submit">Modifica</button>';

        return bootstrap_modal_with_form($modal_id, 
                                                "Modifica Film", $modal_body, 
                                                $modal_footer, "form-modifica-film-" . $ID_film, 
                                                $ajax_page, "post", 'enctype="multipart/form-data"');
    }

    function projections_to_table($films)
    {
        global $IMAGES_FOLDER, $SITE_ROOT;
        $dbf = get_db_facade();
        
        $table = array();
        
        foreach ($films as $film)
        {
            $t_row = array();
            
            $t_row[] = db_to_html($film["nome_locandina"]);
            $t_row[] = db_to_html($film["titolo"]); //1
            $t_row[] = db_to_html($film["anno"]); //2
            $t_row[] = db_to_html($film["nazione"]); //3
            $t_row[] = db_to_html($film["durata"]); //4
            $t_row[] = db_to_html($film["genere"]); //5
            $t_row[] = db_to_html($film["regia"]); //6
            $t_row[] = db_to_html($film["interpreti"]); //7
            $t_row[] = db_to_html($film["trama"]); //8
            $t_row[] = db_to_html($film["url_trailer"]); //9
            $t_row[] = db_to_html($film["url_recensione"]); //10
            $t_row[] = db_to_html($film["rilevanza"]); //11
            $t_row[] = db_to_html($film["ID"]); //12
            
            // Creo le azioni (bottoni) elimina e modifica
            $delete_modal_id = "conferma-eliminazione-" . $film["ID"];
            $modal_delete = get_delete_modal($film, $delete_modal_id);
            $modal_delete_button = bootstrap_text_icon_button_for_modal(ButtonTypes::DELETE, $delete_modal_id);
            
            $modify_modal_id = "conferma-modifica-" . $film["ID"];
            $modal_modify = get_modify_modal($film, $modify_modal_id);
            $modal_modify_button = bootstrap_text_icon_button_for_modal(ButtonTypes::MODIFY, $modify_modal_id);
            
            $projections_button = bootstrap_text_icon_button_for_modal(ButtonTypes::PROJECTIONS, "");
            
            $t_row[] = div($modal_delete_button . $modal_modify_button . $projections_button, "", //13
                       "display: inline-flex; flex-direction:row; align-items: center;");
            $t_row[] = div($modal_delete . $modal_modify); //14
            
            
            $table[] = $t_row;
        }
        
        return $table;
    }