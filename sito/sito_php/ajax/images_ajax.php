<?php

    require_once dirname(__FILE__). '/../factories.php';
    require_once dirname(__FILE__). '/../db_facade.php';
    require_once dirname(__FILE__). '/../config.php';
    
    session_start();

    if (isset($_SESSION["username"]))
    {
        $dbf = get_db_facade();

        if (isset($_GET["operation"]) && $_GET["operation"]=="get_random_id" && isset($_GET["label_type"]))
        {
            header("Content-type:text/json");
            echo file_get_contents( $IMAGES_SERVICE_PATH . '/get_random_image_for_human_classification');
        }
        else if (isset($_GET["operation"]) && $_GET["operation"]=="get_label_type_tree" && isset($_GET["label_type"]))
        {
            header("Content-type:text/json");
            echo file_get_contents($IMAGES_SERVICE_PATH . '/get_label_type_tree_w_counts/' . $_GET["label_type"]);
        }
        else if (isset($_GET["operation"]) && $_GET["operation"]=="get_stats")
        {
            header("Content-type:text/json");
            echo file_get_contents($IMAGES_SERVICE_PATH . '/get_stats');
        }
        else if (isset($_GET["operation"]) && $_GET["operation"]=="get_image" && isset($_GET["image_id"]))
        {
            header('Content-type: image/jpeg;');
            echo file($IMAGES_SERVICE_PATH . '/images/' . $_GET["image_id"]);
        }
        else if (isset($_GET["operation"]) && $_GET["operation"]=="add_all_labels" && isset($_GET["labels"]) && isset($_GET["image_id"]))
        {
            header("Content-type:text/json");
            $uid = $dbf->get_ID_utente($_SESSION["username"]);
            $iid = $_GET["image_id"];
            $labels = $_GET["labels"];
            $error = FALSE;
            
            foreach ($labels as $label)
            {
                $result = json_decode(file_get_contents($IMAGES_SERVICE_PATH . '/add_label/' . $iid . '/' . $label[0] . '/' . $label[1] . '/' . $uid), TRUE);

                if ($result["status"] !== "OK")
                {
                    $error = TRUE;
                    break;
                }
            }

            if ($error)
            {
                echo json_encode(array("status"=>"KO", "msg"=>"Non è stato possibile inserire le etichette"));
            }
            else
            {
                echo json_encode(array("status"=>"OK", "msg"=>"Tutte le etichette sono state inserite"));
            }
        }
        else if (isset($_GET["operation"]) && $_GET["operation"]=="get_label_images" 
                && isset($_GET["label_type"]) && isset($_GET["label_value"]))
        {
            header("Content-type:text/json");
            echo file_get_contents($IMAGES_SERVICE_PATH . '/get_label_images/' . $_GET["label_type"] . '/' . $_GET["label_value"]);
        }
        else if (isset($_GET["operation"]) && $_GET["operation"]=="get_label_images_predicted" 
                && isset($_GET["label_type"]) && isset($_GET["label_value"]))
        {
            header("Content-type:text/json");
            echo file_get_contents($IMAGES_SERVICE_PATH . '/get_label_images_predicted/' . $_GET["label_type"] . '/' . $_GET["label_value"]);
        }
        else if (isset($_GET["operation"]) && $_GET["operation"]=="delete_all_image_labels" 
                && isset($_GET["image_id"]))
        {
            header("Content-type:text/json");
            echo file_get_contents($IMAGES_SERVICE_PATH . '/delete_all_image_labels/' . $_GET["image_id"]);
        }
        else if (isset($_GET["operation"]) && $_GET["operation"]=="add_image_to_labeling_queue" 
                && isset($_GET["image_id"]))
        {
            header("Content-type:text/json");
            echo file_get_contents($IMAGES_SERVICE_PATH . '/add_image_to_labeling_queue/' . $_GET["image_id"]);
        }
        else if (isset($_GET["operation"]) && $_GET["operation"]=="delete_image_from_labeling_queue" 
                && isset($_GET["image_id"]))
        {
            header("Content-type:text/json");
            echo file_get_contents($IMAGES_SERVICE_PATH . '/delete_image_from_labeling_queue/' . $_GET["image_id"]);
        }
        else if (isset($_GET["operation"]) && $_GET["operation"]=="get_ordered_images_to_be_labeled" 
                && isset($_GET["num_images"]))
        {
            header("Content-type:text/json");
            echo file_get_contents($IMAGES_SERVICE_PATH . '/get_ordered_images_to_be_labeled/' . $_GET["num_images"]);
        }
        else
        {
            echo "Operazione non supportata";
            print_r($_GET);
        }
    }

