<?php

    require_once dirname(__FILE__). '/../factories.php';
    require_once dirname(__FILE__). '/../db_facade.php';

    session_start();
    
    header("Content-type:text/json");
    
    $dbf = get_db_facade();
    
    if (isset($_SESSION["username"]) && $dbf->get_user_type($_SESSION["username"]) == UserType::ADMINISTRATOR)
    {
        $ID_sala = $_GET["ID_sala"];
        $data_ora = $_GET["data_ora"];
        
        $ID_film = NULL;
        
        if (!isset($_GET["ID_film"]))
        {
            $proj_info = $dbf->get_projection_info($_GET["ID_programmazione"]);
            $ID_film = $proj_info["ID_film"];
            echo json_encode(utf8ize($dbf->get_incompatible_projections($ID_sala, $data_ora, $ID_film, $_GET["ID_programmazione"])));
        }
        else
        {
            $ID_film = $_GET["ID_film"];
            echo json_encode(utf8ize($dbf->get_incompatible_projections($ID_sala, $data_ora, $ID_film)));
        }
        
    }
