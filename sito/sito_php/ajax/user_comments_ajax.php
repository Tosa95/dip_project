<?php

    require_once dirname(__FILE__). '/../factories.php';
    require_once dirname(__FILE__). '/../db_facade.php';
    require_once dirname(__FILE__). '/../config.php';
    
    header("Content-type:text/json");
   
    
    session_start();
    
    
    if (true)
    {
        //$username = $_SESSION["username"];
        $dbf = get_db_facade();

//        $user_projections = $dbf->get_user_projections($username);
//
        $data = $dbf->get_comments();
        
        if(!isset($_POST["action"]))
        {
            if (!isset($_GET["length"]))
            {

                if (isset($_GET["start"]))
                {
                    $start = $_GET["start"];
                } 
                else 
                {
                    $start = 0;
                }

                if (isset($_GET["end"]))
                {
                    $end = $_GET["end"];
                } 
                else 
                {
                    $end = null;
                }

                if (is_null($end))
                {
                    echo json_encode(array_slice($data, $start));
                }
                else
                {
                    echo json_encode(array_slice($data, $start, $end-$start));
                }
            }
            else
            {
                echo json_encode(count($data));
            }
        }
        else
        {

            $dbf = get_db_facade();
            
            if ($_POST["action"] == "delete" && $GATHER_USER_COMMENTS)
            {
                #Rimozione commento
            }
            elseif($_POST["action"] == "add" && $GATHER_USER_COMMENTS)
            {
                #Aggiunta commento
                
                $username = NULL;
                
                if (isset($_POST["username"]))
                {
                    $username =$_POST["username"];
                }
                
                $dbf->add_user_comment(user_input_to_db($_POST["comment"]), 
                        $username, user_input_to_db($_POST["page"])); 

                # Avviso dell'avvenuto inserimento
                echo json_encode(array("result"=>"OK"));
            }

        }
        
    }
    
    
    function get_delete_modal ($ID_prenotazione, $modal_id, $form_id)
    {
        global $SITE_ROOT;
        
        $ajax_page = $SITE_ROOT . "ajax/mie_proiezioni_ajax.php"; 
        $modal_delete_body = '<div class="form-row">
                                <div class="form-group col-12 alert alert-danger d-none">
                                </div>
                                <div class="form-group col-12">
                                    Sei sicuro di voler eliminare la prenotazione e tutti i posti associati? 
                                </div>

                                <input name="ID_prenotazione" value="' . $ID_prenotazione . '" type="hidden"/>
                                <input type="hidden" name="action" value="delete"/>

                                <div class="form-group col-12 alert alert-danger d-none alert-msg">
                                    
                                </div>

                             </div>';
        $modal_delete_footer = '<button id="delete_button" type="submit" class="button button-danger">Elimina</button>';

        return bootstrap_modal_with_form($modal_id, 
                                                "Elimina Prenotazione", $modal_delete_body, 
                                                $modal_delete_footer, $form_id, 
                                                $ajax_page, "post", 'enctype="multipart/form-data"');

    }

