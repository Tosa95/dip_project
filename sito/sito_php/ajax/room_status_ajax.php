<?php

    require_once dirname(__FILE__). '/../factories.php';
    require_once dirname(__FILE__). '/../db_facade.php';

    header("Content-type:text/json");
    
    if (isset($_GET["ID_programmazione"]))
    {
        echo json_encode(get_db_facade()->get_projection_room_status($_GET["ID_programmazione"]));
    }

