<?php

    require_once dirname(__FILE__). '/../factories.php';
    require_once dirname(__FILE__). '/../db_facade.php';
    require_once dirname(__FILE__). '/../utils.php';

    header("Content-type:text/json");

  
    session_start();
    
    $dbf = get_db_facade();
    
    if (isset($_SESSION["username"]) && $dbf->get_user_type($_SESSION["username"]) == UserType::ADMINISTRATOR)
    {
        $username = $_SESSION["username"];
        
        $events = $dbf->get_all_events();
        
        $data = events_to_table($events);
        
        if (isset($_GET["filter"]) && !is_null($_GET["filter"]))
        {
            $temp = array();
            
            $filter_json = json_decode($_GET["filter"], TRUE);
            //print_r($filter_json);
            $filter_string = $filter_json["string_filter"];
            $only_current = $filter_json["only_current"];
            $filter_begin = isset($filter_json["begin"])?$filter_json["begin"]:"";
            $filter_end = isset($filter_json["end"])?$filter_json["end"]:"";
            
            $events = "";
            
            if($only_current)
            {
                $events = $dbf->get_current_events($filter_string);
            }
            else
            {
                $events = $dbf->get_all_events($filter_string, $filter_begin, $filter_end);
            }

            $data = events_to_table($events);

        }
        

        if (!isset($_GET["length"]) && !isset($_POST["nome"]))
        {

            if (isset($_GET["start"]))
            {
                $start = $_GET["start"];
            } 
            else 
            {
                $start = 0;
            }

            if (isset($_GET["end"]))
            {
                $end = $_GET["end"];
            } 
            else 
            {
                $end = null;
            }

            if (is_null($end))
            {
                echo json_encode(array_slice($data, $start));
            }
            else
            {
                echo json_encode(array_slice($data, $start, $end-$start));
            }
        }
        else if(isset($_GET["length"]))
        {
            echo json_encode(count($data));
        }
        # Se l'amministratore ha cliccato su "Aggiungi" nel modale, su "Modifica" o su "Elimina"
        else if(isset($_POST["azione"]))
        {
            # Se voglio aggiungere l'evento
            if($_POST["azione"] == "aggiungi")
            {
                aggiungi_evento($dbf);
            }
            else if ($_POST["azione"] == "modifica")
            {
                modifica_evento($dbf);
            }
            else if ($_POST["azione"] == "elimina")
            {
                elimina_evento($dbf);
            }

        }
        
    }
    
    
    # Funzioni di aggiunta, modifica e rimozione dell'evento.
    
    function aggiungi_evento($dbf)
    {
        global $SITE_ROOT;
        
        # Devo inserire l'evento nel DB. Ritorno una risposta json come segue:
        # {
        #       result: "OK" ==> tutto a posto | "KO" ==> Si sono verificati errori
        #       errors: elenco (array) degli errori che si sono verificati (non c'è se result è "OK")
        # }

        # Contiene l'elenco degli errori che si sono verificati
        $errors = array();

        $target_dir = $_SERVER["DOCUMENT_ROOT"] . $SITE_ROOT . "images/";

        $poster_file_type = strtolower(pathinfo(basename($_FILES["locandina"]["name"]), PATHINFO_EXTENSION));
        $poster_file_name = str_replace(" ", "_", clean_strange_chars(strtolower($_POST["nome"]))) . "-loc." . $poster_file_type;

        $poster_target_file = $target_dir . $poster_file_name;

        $image_file_type = strtolower(pathinfo(basename($_FILES["immagine"]["name"]), PATHINFO_EXTENSION));
        $image_file_name = str_replace(" ", "_", clean_strange_chars(strtolower($_POST["nome"]))) . "-img." . $image_file_type;

        $image_target_file = $target_dir . $image_file_name;


        $upload_ok = 1;

        # Check if image file is a actual image or fake image
        $check_poster = getimagesize($_FILES["locandina"]["tmp_name"]);
        $check_image = getimagesize($_FILES["immagine"]["tmp_name"]);

        if($check_image && $check_poster) 
        {
            $upload_ok = 1;
        } 
        else 
        {
            $errors[] = "Entrambi i files devono essere immagini!";
            $upload_ok = 0;
        }

        # Allow certain file formats
        if($poster_file_type != "jpg" && $poster_file_type != "png" && $poster_file_type != "jpeg"
        && $poster_file_type != "gif" ) 
        {
            $errors[] = "Sono permesse solo le estensioni JPG, JPEG, PNG e GIF per la locandina.";
            $upload_ok = 0;
        }
        if($image_file_type != "jpg" && $image_file_type != "png" && $image_file_type != "jpeg"
        && $image_file_type != "gif" ) 
        {
            $errors[] = "Sono permesse solo le estensioni JPG, JPEG, PNG e GIF per l'immagine estesa.";
            $upload_ok = 0;
        }

        # Controllo se l'evento che sto per aggiungere è già presente
        if ($dbf->event_present($_POST["nome"]))
        {
            $errors[] = "L'evento è già presente nel database.";
            $upload_ok = 0;
        }

        # Check if $upload_ok is set to 0 by an error
        if ($upload_ok == 0) 
        {
            $errors[] = "Spiacenti, c'è stato un problema con il caricamento dei files.";

            echo json_encode(array("result"=>"KO", "errors"=>$errors));

        } 
        else # In questo caso non ci sono stati problemi, provo a salvare le immagini
        {
            if (move_uploaded_file($_FILES["immagine"]["tmp_name"], $image_target_file))
            {
                if (move_uploaded_file($_FILES["locandina"]["tmp_name"], $poster_target_file))
                {
                    # A questo punto non ci sono stati problemi nè nel caricamento che
                    # nello spostamento dei files temporanei.
                    # 
                    # Nota: ho già controllato sopra se l'evento fosse già presente.

                    $dbf->add_event($_POST["nome"], $_POST["descrizione"], $poster_file_name, $image_file_name, 
                           $_POST["inizio"], $_POST["fine"], $_POST["rilevanza"]);

                    echo json_encode(array("result"=>"OK"));
                }
                else
                {
                    $errors[] = "Si è verificato un errore nel caricamento della locandina.";                        
                }

            }
            else
            {
                $errors[] = "Si è verificato un errore nel caricamento delll'immagine estesa.";

                echo json_encode(array("result"=>"KO", "errors"=>$errors));
            }

        }
    }
    
    
    function modifica_evento($dbf)
    {
        global $SITE_ROOT;
        
        # Devo modificare l'evento nel DB. Ritorno una risposta json come segue:
        # {
        #       result: "OK" ==> tutto a posto | "KO" ==> Si sono verificati errori
        #       errors: elenco (array) degli errori che si sono verificati (non c'è se result è "OK")
        # }

        # Contiene l'elenco degli errori che si sono verificati
        $errors = array();

        if($dbf->event_present($_POST["nome-old"]))
        {

            $target_dir = $_SERVER["DOCUMENT_ROOT"] . $SITE_ROOT . "images/";

            $upload_image_ok = 1;
            $upload_poster_ok = 1;

            # Setto il nome dell'immagine estesa a quello vecchio. Se l'utente seleziona 
            # un'immagine nuova, questo verrà sovrascritto. Lo stesso vale per la locandina.
            $image_file_name = $_POST["immagine-old"];
            $poster_file_name = $_POST["locandina-old"];


            # Se l'utente ha selezionato una nuova immagine estesa, la provo a caricare
            if(!empty($_FILES["immagine"]["tmp_name"]))
            {       

                $image_file_type = strtolower(pathinfo(basename($_FILES["immagine"]["name"]), PATHINFO_EXTENSION));
                $image_file_name = str_replace(" ", "_", clean_strange_chars(strtolower($_POST["nome"]))) . rand(1, 20000) . "-img." . $image_file_type;

                $image_target_file = $target_dir . $image_file_name;


                # Check if image file is a actual image or fake image
                $check_image = getimagesize($_FILES["immagine"]["tmp_name"]);

                if($check_image) 
                {
                    $upload_image_ok = 1;
                } 
                else 
                {
                    $errors[] = "Il file dell'immagine estesa deve essere un'immagine!";
                    $upload_ok = 0;
                }

                # Allow certain file formats
                if($image_file_type != "jpg" && $image_file_type != "png" && $image_file_type != "jpeg"
                && $image_file_type != "gif" ) 
                {
                    $errors[] = "Sono permesse solo le estensioni JPG, JPEG, PNG e GIF per l'immagine estesa.";
                    $upload_image_ok = 0;
                }

                # Se finora non ci sono stati errori, provo a salvare l'immagine
                if($upload_image_ok)
                {
                    # Rinomino la vecchia immagine
                    rename($target_dir . $_POST["immagine-old"], $target_dir . $_POST["immagine-old"] . "old");

                    if (move_uploaded_file($_FILES["immagine"]["tmp_name"], $image_target_file))
                    {
                        # Elimino l'immagine vecchia
                        unlink($target_dir . $_POST["immagine-old"] . "old");
                    }
                    else
                    {
                        # Ripristino l'immagine di vecchia che avevo precedentemente rinominato
                        rename($target_dir . $_POST["immagine-old"] . "old", $target_dir . $_POST["immagine-old"]);

                        $errors[] = "Si è verificato un errore nel caricamento delll'immagine estesa.";
                        $upload_image_ok = 0;
                    }
                }

            }


            # Se l'utente ha selezionato una nuova locandina, la provo a caricare
            if(!empty($_FILES["locandina"]["tmp_name"]))
            {

                $poster_file_type = strtolower(pathinfo(basename($_FILES["locandina"]["name"]), PATHINFO_EXTENSION));
                $poster_file_name = str_replace(" ", "_", clean_strange_chars(strtolower($_POST["nome"]))) . rand(1, 20000) . "-loc." . $poster_file_type;

                $poster_target_file = $target_dir . $poster_file_name;


                # Check if image file is a actual image or fake image
                $check_poster = getimagesize($_FILES["locandina"]["tmp_name"]);

                if($check_poster) 
                {
                    $upload_poster_ok = 1;
                } 
                else 
                {
                    $errors[] = "Il file della locandina deve essere un'immagine!";
                    $upload__poster_ok = 0;
                }

                # Allow certain file formats
                if($poster_file_type != "jpg" && $poster_file_type != "png" && $poster_file_type != "jpeg"
                && $poster_file_type != "gif" ) 
                {
                    $errors[] = "Sono permesse solo le estensioni JPG, JPEG, PNG e GIF per la locandina.";
                    $upload_poster_ok = 0;
                }


                # Se finora non ci sono stati errori, provo a salvare la locandina
                if($upload_poster_ok)
                {
                    # Rinomino la vecchia locandina
                    rename($target_dir . $_POST["locandina-old"], $target_dir . $_POST["locandina-old"] . "old");

                    if (move_uploaded_file($_FILES["locandina"]["tmp_name"], $poster_target_file))
                    {
                        # Elimino la locandina vecchia
                        unlink($target_dir . $_POST["locandina-old"] . "old");
                    }
                    else
                    {
                        # Ripristino la locandina vecchia che avevo precedentemente rinominato
                        rename($target_dir . $_POST["locandina-old"] . "old", $target_dir . $_POST["locandina-old"]);

                        $errors[] = "Si è verificato un errore nel caricamento della locandina.";
                        $upload_image_ok = 0;
                    }
                }

            }
            else
            {
                $poster_file_name = $_POST["locandina-old"];
            }


            # Se entrambi i caricamenti sono andati a buon fine, modifico l'evento nel db
            # Nota: se l'utente non ha inserito nuove immagini, le variabili
            #   $upload_image_ok e $upload_poster_ok sono a 1 di default
            #   $image_file_name e $poster_file_name sono settate con i nomi delle immagini originali di default
            if($upload_image_ok && $upload_poster_ok)
            {
                $dbf->modify_event($_POST["ID"], $_POST["nome"], $_POST["descrizione"], $poster_file_name, 
                                               $image_file_name, $_POST["inizio"], $_POST["fine"], $_POST["rilevanza"]);

                echo json_encode(array("result"=>"OK"));
            }
            else
            {
                $errors[] = "Si sono verificati dei problemi col caricamento dei files.";

                echo json_encode(array("result"=>"KO", "errors"=>$errors));
            }

        }
        else
        {
            $errors[] = "Evento non trovato!";

            echo json_encode(array("result"=>"KO", "errors"=>$errors));
        }
    }
    
    
    function elimina_evento($dbf)
    {
        global $SITE_ROOT;
        
        # Devo eliminare l'evento nel DB. Ritorno una risposta json come segue:
        # {
        #       result: "OK" ==> tutto a posto | "KO" ==> Si sono verificati errori
        #       errors: elenco (array) degli errori che si sono verificati (non c'è se result è "OK")
        # }

        # Contiene l'elenco degli errori che si sono verificati
        $errors = array();

        if($dbf->event_present($_POST["nome"]))
        {
            $dbf->delete_event($_POST["ID"]);

            # Elimino le immagini (locandina ed immagine estesa)
            $target_dir = $_SERVER["DOCUMENT_ROOT"] . $SITE_ROOT . "images/";
            unlink($target_dir . $_POST["locandina"]);
            unlink($target_dir . $_POST["immagine"]);

            echo json_encode(array("result"=>"OK"));
        }
        else
        {
            $errors[] = "Evento non trovato!";

            echo json_encode(array("result"=>"KO", "errors"=>$errors));
        }
    }
    
    
    # Funzioni che creano una tabella con gli eventi
    
    
    function handler ( $errno , $errstr )
    {
        echo json_encode(array("result"=>"KO", "errors"=>$errstr));
    }
    
    set_error_handler("handler");
    
    function events_to_table($events)
    {
        global $IMAGES_FOLDER, $SITE_ROOT;
        $dbf = get_db_facade();
        
        $table = array();
        
        foreach ($events as $event)
        {
            $t_row = array();
            
            $t_row[] = db_to_html($event["nome"]);
            $t_row[] = db_to_html($event["descrizione"]);
            $t_row[] = db_to_html($event["locandina"]);
            $t_row[] = db_to_html($event["immagine"]);
            $t_row[] = db_to_html($event["inizio"]);
            $t_row[] = db_to_html($event["fine"]);
            $t_row[] = db_to_html($event["rilevanza"]);
            
            
            $ajax_page = $SITE_ROOT . "ajax/all_events_ajax.php";
            
            $modal_modify_body = '<div class="form-row">
                                    
                                    <div class="form-group col-12">
                                        <label for="nome">Nome</label>
                                        <input name="nome" value="' . db_to_field_value($event["nome"]) . '" id="nome" type="text" class="form-control vf" placeholder="Nome" autocomplete="off" required>
                                        <div class="alert alert-danger d-none vf-alert-nome vf-validation-alert"></div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="descrizione">Descrizione</label>
                                        <textarea name="descrizione" id="descrizione" class="form-control vf" placeholder="Descrizione" autocomplete="off" required>' . db_to_field_value($event["descrizione"]) . '</textarea>
                                        <div class="alert alert-danger d-none vf-alert-descrizione vf-validation-alert"></div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="locandina">Immagine locandina</label>
                                        <input name="locandina" id="locandina" type="file" class="form-control vf vf-image">
                                        <div class="alert alert-danger d-none vf-alert-locandina vf-validation-alert"></div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="immagine">Immagine estesa</label>
                                        <input name="immagine" id="immagine" type="file" class="form-control vf vf-image">
                                        <div class="alert alert-danger d-none vf-alert-immagine vf-validation-alert"></div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="inizio">Inizio</label>
                                        <input name="inizio" value="' . date("Y-m-d\TH:i", strtotime($event["inizio"])) . '" id="inizio" type="datetime-local" class="form-control vf vf-dt-comp-durata-lt-1" autocomplete="off" required>
                                        <div class="alert alert-danger d-none vf-alert-inizio vf-validation-alert"></div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="fine">Fine</label>
                                        <input name="fine" value="' . date("Y-m-d\TH:i", strtotime($event["fine"])) . '" id="fine" type="datetime-local" class="form-control vf vf-dt-comp-durata-lt-2" autocomplete="off" required>
                                        <div class="alert alert-danger d-none vf-alert-fine vf-validation-alert"></div>
                                    </div>

                                    <div class="form-group col-12">
                                        <label for="rilevanza">Rilevanza</label>
                                        <input name="rilevanza" value="' . db_to_field_value($event["rilevanza"]) . '" id="rilevanza" type="text" class="form-control vf vf-inum vf-minv-1 vf-maxv-10" autocomplete="off" required>
                                        <div class="alert alert-danger d-none vf-alert-rilevanza vf-validation-alert"></div>
                                    </div>
                                    <div class="form-group col-12 alert alert-danger alert-msg d-none">
                                    </div>
                                    
                                    <input name="ID" value="' . db_to_field_value($event["ID"]) . '" type="hidden">
                                    <input name="nome-old" value="' . db_to_field_value($event["nome"]) . '" type="hidden">
                                    <input name="locandina-old" value="' . db_to_field_value($event["locandina"]) . '" type="hidden">
                                    <input name="immagine-old" value="' . db_to_field_value($event["immagine"]) . '" type="hidden">
                                    <input name="azione" value="modifica" type="hidden">
                        
                                 </div>';
                
            $modal_modify_footer = '<button id="modify_button" type="submit" class="button button-danger vf-submit">Modifica</button>';
            
            $modify_button = bootstrap_circular_button_for_modal(ButtonType::MODIFY, "modale-modifica-evento-" . $event["ID"]);
            $modify_modal = bootstrap_modal_with_form("modale-modifica-evento-" . $event["ID"], "Modifica Evento", $modal_modify_body, $modal_modify_footer, "form-modifica-evento-" . $event["ID"], $ajax_page, "post", 'enctype="multipart/form-data"');


            
            $modal_delete_body = '<div class="form-row">
                                    <div class="form-group col-12 alert alert-danger d-none">
                                    </div>
                                    <div class="form-group col-12">' .
                                        'Sei sicuro di voler eliminare l\'evento "' . db_to_html($event["nome"]) . '"?' . '
                                    </div>
                                    
                                    <input name="ID" value="' . db_to_field_value($event["ID"]) . '" type="hidden">
                                    <input name="nome" value="' . db_to_field_value($event["nome"]) . '" type="hidden">
                                    <input name="locandina" value="' . db_to_field_value($event["locandina"]) . '"id="locandina" type="hidden">
                                    <input name="immagine" value="' . db_to_field_value($event["immagine"]) . '"id="immagine" type="hidden">
                                    <input name="azione" value="elimina" type="hidden">
                                    
                                 </div>';
                
            $modal_delete_footer = '<button id="delete_button" type="submit" class="button button-danger">Elimina</button>';
           
            $delete_button = bootstrap_circular_button_for_modal(ButtonType::DELETE, "modale-elimina-evento-" . $event["ID"]);
            $delete_modal = bootstrap_modal_with_form("modale-elimina-evento-" . $event["ID"], "Elimina Evento", $modal_delete_body, $modal_delete_footer, "form-elimina-evento-" . $event["ID"], $ajax_page, "post", 'enctype="multipart/form-data"');

            
            $t_row[] = div($modify_button . $delete_button, "", "display: inline-flex; flex-direction:row; align-items: center;");

            $t_row[] = $event["ID"];
            
            $t_row[] = div($modify_modal . $delete_modal);
            
            $table[] = $t_row;
        }
        
        return $table;
    }
