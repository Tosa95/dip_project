<?php

    require_once dirname(__FILE__). '/../factories.php';
    require_once dirname(__FILE__). '/../db_facade.php';
    require_once dirname(__FILE__). '/../utils.php';
    
    header("Content-type:text/json");
    
    session_start();
    
    $dbf = get_db_facade();
    
    if (isset($_SESSION["username"]) && $dbf->get_user_type($_SESSION["username"]) == UserType::ADMINISTRATOR)
    {
        $username = $_SESSION["username"];
        
        if (!isset($_POST["action"]))
        {
            $data = get_filtered_data($dbf);

            if (!isset($_GET["length"]))
            {
                $start = 0;
                if (isset($_GET["start"]))
                {
                    $start = $_GET["start"];
                } 
                else 
                {
                    $start = 0;
                }

                if (isset($_GET["end"]))
                {
                    $end = $_GET["end"];
                } 
                else 
                {
                    $end = null;
                }

                if (is_null($end))
                {
                    echo json_encode(utf8ize(array_slice($data, $start)));
                }
                else
                {
                    echo json_encode(utf8ize(array_slice($data, $start, $end-$start)));
                }
            }
            else if(isset($_GET["length"]))
            {
                echo json_encode(count($data));
            }
        }
        else # Se l'amministratore ha eseguito il submit di qualche form
        {
            
            if ($_POST["action"] === "insert" )
            {
                insert_new_projection($dbf);
            }
            else if ($_POST["action"] === "delete")
            {
                delete_projection($dbf);
            }
            else if ($_POST["action"] === "modify")
            {
                modify_projection($dbf);
            }
          
        }
    }
    
    function get_filtered_data($dbf)
    {
        $data = array();
        
        if (isset($_GET["filter"]) && !is_null($_GET["filter"]))
        {
            $filter_json = json_decode($_GET["filter"], true);
            $begin = isset($filter_json["begin"])?$filter_json["begin"]:"";
            $end = isset($filter_json["end"])?$filter_json["end"]:"";
            
            $proj = $dbf->get_film_projections($filter_json["ID"], $filter_json["only_future"], $begin, $end);

            $data = projections_to_table($proj);
            
        }
        
        return $data;
    }
    
    function insert_new_projection($dbf)
    {
        global $SITE_ROOT;
        
        $ID = $dbf->add_projection($_POST["ID_film"], $_POST["sala"], 
                $_POST["inizio"], $_POST["tipo"]);
        
        if ($_POST["tipo"] == ProjectionType::SPECIAL)
        {
            $dbf->set_projection_price($ID, $_POST["prezzo"]);
        }
        
        echo json_encode(array("result"=>"OK"));
    }
    
    function delete_projection($dbf)
    {
        global $SITE_ROOT;
        
        $dbf->delete_projection($_POST["ID"]);
        
        echo json_encode(array("result"=>"OK"));
        
    }
    
    function modify_projection($dbf)
    {
        global $SITE_ROOT;
        
        $dbf->set_projection($_POST["ID_programmazione"], $_POST["sala"], 
                $_POST["inizio"], $_POST["tipo"]);
        
        if ($_POST["tipo"] == ProjectionType::SPECIAL)
        {
//            print_r($_POST["ID_programmazione"]);
//            print_r($_POST["prezzo"]);
            $dbf->set_projection_price($_POST["ID_programmazione"], $_POST["prezzo"]);
        }
        
        echo json_encode(array("result"=>"OK"));
        
        
    }
    
    function get_delete_modal ($projection, $modal_id)
    {
        global $SITE_ROOT;
        
        $ID_proiezione = $projection["ID_programmazione"];
        $ajax_page = $SITE_ROOT . "ajax/projection_film_ajax.php"; 
        $modal_delete_body = '<div class="form-row">
                                <div class="form-group col-12 alert alert-danger d-none">
                                </div>
                                <div class="form-group col-12">' .
                                    'Sei sicuro di voler eliminare la proiezione selezionata? <br>
                                    Ricorda che cos&igrave; facendo tutti coloro che hanno prenotato/acquistato biglietti
                                    per tale proiezione <strong>dovranno essere risarciti!</strong> <br>
                                    <strong>Questa &egrave; una misura molto drastica che andrebbe presa solo in situazioni eccezionali.</strong> <br>
                                    Se vuoi solo spostare una prenotazione non eliminarla per poi ricrearla, ma clicca
                                    su modifica!' .
                                '</div>

                                <input name="ID" value="' . $ID_proiezione . '" type="hidden"/>
                                <input type="hidden" name="action" id="action" value="delete" />

                                <div class="form-group col-12 alert alert-danger d-none alert-msg">
                                    <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
                                </div>

                             </div>';
        $modal_delete_footer = '<button id="delete_button" type="submit" class="button button-danger">Elimina</button>';

        return bootstrap_modal_with_form($modal_id, 
                                                "Elimina Proiezione", $modal_delete_body, 
                                                $modal_delete_footer, "form-elimina-proiezione-" . $ID_proiezione, 
                                                $ajax_page, "post", 'enctype="multipart/form-data"');

    }

    
    function get_type_options ($projection_type)
    {
        $options = array("NORMALE", "SPECIALE");
        
        $select_body = "";
        
        foreach ($options as $option)
        {
            if ($option != $projection_type)
            {
                $select_body .= "<option>$option</option>";
            }
            else
            {
                $select_body .= '<option selected="selected">' . $option . '</option>';
            }
        }
        
        return $select_body;
    }
    
    function get_modify_modal($projection, $modal_id, $dbf)
    {
        global $SITE_ROOT;
        
        $ID_proiezione = $projection["ID_programmazione"];
        $ajax_page = $SITE_ROOT . "ajax/projection_film_ajax.php"; 
        
        $room_selection = get_room_selection($dbf, $projection["ID_sala"]);
    
        $price_selection = get_price_selection($dbf);
        
        if ($projection["tipo"] === "SPECIALE")
        {
            $fascia = $dbf->get_special_projection_price($projection["ID_programmazione"]);
            $price_selection = get_price_selection($dbf, $fascia["ID"]);
        }
        
        
        
        $inizio = date("Y-m-d\TH:i", strtotime($projection["data_ora"]));
        
        $modal_body = '<div class="form-row">
                            <input type="hidden" name="action" id="action" value="modify" />
                            <input type="hidden" class="vf-proj-id" name="ID_programmazione" id="ID_programmazione" value="' . $ID_proiezione . '" />
                            <div class="form-group col-12">' .
                                $room_selection .
                            '
                            <div class="alert alert-danger d-none vf-alert-sala vf-validation-alert">

                            </div>
                            </div>

                            <div class="form-group col-12"> 
                                <label for="inizio">Data e ora inizio</label>
                                <input name="inizio" type="datetime-local" class="form-control vf vf-projection-date vf-proj-date" id="inizio" placeholder="Dopo" autocomplete="off" value="' . $inizio . '" required>
                                <div class="alert alert-danger d-none vf-alert-inizio vf-validation-alert">

                                </div>
                            </div>
                            <div class="form-group col-12"> 
                                <label for="tipo">Tipo</label>
                                <select class="form-control tipo" id="tipo" name="tipo">'
                                  .
                                  get_type_options($projection["tipo"])
                                  .
                                '</select>
                            </div>

                            <div class="form-group col-12 group-prezzo d-none"> '
                                . $price_selection . 
                                '<div class="alert alert-danger d-none vf-alert-prezzo vf-validation-alert">

                                </div>
                            </div>


                            <div class="form-group col-12 alert alert-danger d-none alert-msg">
                                <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
                            </div>

                        </div>';
                
        $modal_footer = '<button type="submit" id="add-film" class="button button-danger vf-submit">Modifica</button>';
    
        
        return bootstrap_modal_with_form($modal_id, 
                                                "Modifica Proiezione", $modal_body, 
                                                $modal_footer, "form-modifica-proiezione-" . $ID_proiezione, 
                                                $ajax_page, "post", 'enctype="multipart/form-data"');
    }

    function projections_to_table($projections)
    {
        global $IMAGES_FOLDER, $SITE_ROOT;
        $dbf = get_db_facade();
        
        $table = array();
        
        foreach ($projections as $projection)
        {
            $t_row = array();
            
            $ID_programmazione = $projection["ID_programmazione"];
            
            $prices_info = $dbf->get_prices_info_from_ID_programmazione($ID_programmazione);
            
            $t_row[] = $projection["data_ora"]; //0
            $t_row[] = $projection["nome_sala"]; //1
            $t_row[] = db_to_html($projection["titolo"]); //2
            $t_row[] = $projection["nome_locandina"]; //3
            $t_row[] = $projection["tipo"]; //4
            $t_row[] = $projection["num_posti"]; //5
            $t_row[] = $projection["posti_non_liberi"]; //6
            $t_row[] = $projection["fine"]; //7
            
            // Creo le azioni (bottoni) elimina e modifica
            $delete_modal_id = "conferma-eliminazione-" . $projection["ID_programmazione"];
            $modal_delete = get_delete_modal($projection, $delete_modal_id);
            $modal_delete_button = bootstrap_circular_button_for_modal(ButtonType::DELETE, $delete_modal_id);
            
            $modify_modal_id = "conferma-modifica-" . $projection["ID_programmazione"];
            $modal_modify = get_modify_modal($projection, $modify_modal_id, $dbf);
            $modal_modify_button = bootstrap_circular_button_for_modal(ButtonType::MODIFY, $modify_modal_id);
            
            $t_row[] = div($modal_delete_button . $modal_modify_button, "", //8
                       "display: inline-flex; flex-direction:row; align-items: center;");
            $t_row[] = div($modal_delete . $modal_modify); //9
            
            $t_row[] = $projection["ID_programmazione"]; //10
            
            $t_row[] = $prices_info[0]["nome_fascia"]; //11
            $t_row[] = number_format($prices_info[0]["valore"], 2); //12
            
            $table[] = $t_row;
        }
        
        return $table;
    }