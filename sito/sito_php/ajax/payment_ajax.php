<?php

    require_once dirname(__FILE__). '/../factories.php';
    require_once dirname(__FILE__). '/../db_facade.php';

    header("Content-type:text/json");

    if (isset($_POST["credit_card"]) && isset($_POST["challenge"]))
    {
        echo file_get_contents('http://127.0.0.1:5000/payment/' . $_POST["credit_card"] . "/" . $_POST["challenge"]);
    }
    else
    {
        echo "Devi fornire una carta di credito e una challenge per poter accedere al servizio di pagamento";
    }


