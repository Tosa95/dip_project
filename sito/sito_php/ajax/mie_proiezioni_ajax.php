<?php

    require_once dirname(__FILE__). '/../factories.php';
    require_once dirname(__FILE__). '/../db_facade.php';

    header("Content-type:text/json");
    
    function user_projections_to_table($user_projections)
    {
        global $IMAGES_FOLDER, $SITE_ROOT;
        $dbf = get_db_facade();
        
        $table = array();
        foreach ($user_projections as $proj)
        {
            $t_row = array();
            
            $t_row[] = $proj["nome_locandina"]; //0
            $t_row[] = db_to_html($proj["titolo"]); //1
            $t_row[] = db_to_html($proj["nome"]); //2
            $t_row[] = $proj["data_programmazione"]["nome_giorno"] . " " . $proj["data_programmazione"]["num_giorno"] . "/" . $proj["data_programmazione"]["num_mese"] . " " . $proj["data_programmazione"]["orario"]; //3
            $t_row[] = $proj["data_prenotazione"]["num_giorno"] . "/" . $proj["data_prenotazione"]["num_mese"] . " " . $proj["data_prenotazione"]["orario"]; //4
            
            # Otteniamo il costo totale dei posti relativi alla prenotazione
            $ID_prenotazione = $proj["ID_prenotazione"];
            $total_price = $dbf->get_total_price($ID_prenotazione);
            $t_row[] = number_format($total_price, 2) . " €"; //5
            
            # Otteniamo i posti relativi alla prenotazione
            $seats = join(", ", $dbf->get_seats($ID_prenotazione)) ;
            $t_row[] = $seats; //6
            
            
            # Se la prenotazione non è acquistata
            if(!$dbf->is_purchased($ID_prenotazione))
            {
                # Aggiungo i bottoni per l'eliminazione e per l'acquisto  
                $modal_button_delete = bootstrap_circular_button_for_modal(ButtonType::DELETE, "conferma-eliminazione-" . $ID_prenotazione);
                $modal_delete = get_delete_modal($ID_prenotazione, "conferma-eliminazione-" . $ID_prenotazione, "form-conferma-eliminazione-" . $ID_prenotazione);
            

                $purchase_msg = "Se sei sicuro di voler acquistare i posti selezionati, inserisci il numero della tua carta di credito e clicca su acquista.";
                
                $modal_button_purchase = bootstrap_circular_button_for_modal(ButtonType::PURCHASE, "conferma-acquisto-" . $ID_prenotazione);
                
                $modal_purchase = get_purchase_modal($purchase_msg, "conferma-acquisto-" . $ID_prenotazione, "form-conferma-acquisto-" . $ID_prenotazione);
                
                $t_row[] = div($modal_button_delete . $modal_button_purchase, "", "display: inline-flex; flex-direction:row; align-items: center;"); //7
                
                $t_row[] = div($modal_delete . $modal_purchase); //8
                
            }
            else
            {
                # Aggiungo la label ACQUISTATO
                $t_row[] = '<span class="purchased-label">ACQUISTATO</span>'; //7
                $t_row[] = ""; //8
            }
            
            # Aggiungo la data di fine proiezione (aggiunta postuma)
            $t_row[] = $proj["fine"]; //9
            $t_row[] = $proj["ID_prenotazione"]; //10
            $table[] = $t_row;
        }
        
        return $table;
    }
    
    session_start();
    
    
    if (isset($_SESSION["username"]))
    {
        $username = $_SESSION["username"];
        $dbf = get_db_facade();

        $user_projections = $dbf->get_user_projections($username);

        $data = user_projections_to_table($user_projections);
        
        if(!isset($_POST["action"]))
        {
            if (!isset($_GET["length"]))
            {

                if (isset($_GET["start"]))
                {
                    $start = $_GET["start"];
                } 
                else 
                {
                    $start = 0;
                }

                if (isset($_GET["end"]))
                {
                    $end = $_GET["end"];
                } 
                else 
                {
                    $end = null;
                }

                if (is_null($end))
                {
                    echo json_encode(array_slice($data, $start));
                }
                else
                {
                    echo json_encode(array_slice($data, $start, $end-$start));
                }
            }
            else
            {
                echo json_encode(count($data));
            }
        }
        else
        {

            $dbf = get_db_facade();

            if ($_POST["action"] == "delete")
            {
                if ($dbf->is_reservation_of($_SESSION["username"], $_POST["ID_prenotazione"]))
                {
                    $dbf->delete_reservation($_POST["ID_prenotazione"]);

                    echo json_encode(array("result"=>"OK"));
                }
            }
            elseif($_POST["action"] == "purchase")
            {
                if ($dbf->is_reservation_of($_SESSION["username"], $_POST["ID_prenotazione"]))
                {

                    $dbf->purchase_reservation($_POST["ID_prenotazione"]);

                    echo json_encode(array("result"=>"OK"));
                }
            }

        }
        
    }
    
    
    function get_delete_modal ($ID_prenotazione, $modal_id, $form_id)
    {
        global $SITE_ROOT;
        
        $ajax_page = $SITE_ROOT . "ajax/mie_proiezioni_ajax.php"; 
        $modal_delete_body = '<div class="form-row">
                                <div class="form-group col-12 alert alert-danger d-none">
                                </div>
                                <div class="form-group col-12">
                                    Sei sicuro di voler eliminare la prenotazione e tutti i posti associati? 
                                </div>

                                <input name="ID_prenotazione" value="' . $ID_prenotazione . '" type="hidden"/>
                                <input type="hidden" name="action" value="delete"/>

                                <div class="form-group col-12 alert alert-danger d-none alert-msg">
                                    
                                </div>

                             </div>';
        $modal_delete_footer = '<button id="delete_button" type="submit" class="button button-danger">Elimina</button>';

        return bootstrap_modal_with_form($modal_id, 
                                                "Elimina Prenotazione", $modal_delete_body, 
                                                $modal_delete_footer, $form_id, 
                                                $ajax_page, "post", 'enctype="multipart/form-data"');

    }

