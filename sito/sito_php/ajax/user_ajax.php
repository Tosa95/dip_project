<?php

    require_once dirname(__FILE__). '/../factories.php';
    require_once dirname(__FILE__). '/../db_facade.php';

    header("Content-type:text/json");
    
    if (isset($_GET["username"]))
    {
        echo json_encode(array("user_present" => get_db_facade()->user_present(user_input_to_db($_GET["username"]))));
    }
