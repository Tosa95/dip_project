<?php

    /**
     * Classe di utilità per la processazione e la gestione delle password.
     * 
     * @author Davide, Michele
     */
    class PasswordUtils
    {
        private $algorithm;
        private $times;
        private $salt_length;

        function __construct($times, $algorithm, $salt_length, $password_key_secret, $payment_service_secret, $payment_service_algorithm) 
        {
            $this->algorithm = $algorithm;
            $this->times = $times;
            $this->salt_length = $salt_length;
            $this->password_key_secret = $password_key_secret;
            $this->payment_service_secret = $payment_service_secret;
            $this->payment_service_algorithm = $payment_service_algorithm;
            
            // Chiamo per la prima volta get password reset key in modo da rendere più
            // difficile prevedere la prossima chiave
            $this->get_password_reset_key();
        }
        
        /**
         * Funzione che aggiunge del sale alla password e ne calcola l'hash.
         * 
         * @param $pwd Password da processare
         * @param $salt Sale da aggiungere
         * @return hash
         */
        function hash_password($pwd, $salt)
        {
            $res = $pwd . $salt;

            for ($i = 0; $i < $this->times; $i++)
            {
                $res = hash($this->algorithm, $res);
            }

            return $res;
        }
        
        /**
         * Funzione che verifica se un certo hash corrisponde ad una certa password,
         * (avente un determinato sale).
         * 
         * @param $pre_hashed Hash della password
         * @param $pwd Password
         * @param $salt Sale della password
         * @return True se c'è corrispondenza
         */
        function check_equality($pre_hashed, $pwd, $salt)
        {
            return $pre_hashed === $this->hash_password($pwd, $salt);
        }
        
        /**
         * Funzione che restituisce un sale casuale.
         * 
         * @return sale
         */
        function get_random_salt()
        {
            $length = $this->salt_length;

            # Genera una stringa casuale. Copiata da https://stackoverflow.com/questions/4356289/php-random-string-generator
            return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
        }
        
        /**
         * Funzione che ritorna una key random per resettare la password
         * @return type
         */
        function get_password_reset_key()
        {
            $key = $this->hash_password(mt_rand(), $this->password_key_secret);
            $this->password_key_secret = $this->hash_password($key, "aaa");
            return $key;
        }
        
        function check_payment_signature($challenge, $credit_card, $signature)
        {
            $complete_str = $this->payment_service_secret . $credit_card . $challenge;
            
            $recomputed_signature = hash($this->payment_service_algorithm, $complete_str);
            
            return $signature === $recomputed_signature;
        }
    }


