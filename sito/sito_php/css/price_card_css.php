/*<style>*/

<?php
    header("Content-type:text/css");
    require_once dirname(__FILE__) . '/../config.php';

?>

/*Stile esterno della card*/
.price-card{
    background-color: <?php echo $PRICE_CARD_COLOR ?>;
    border-radius: <?php echo $CARD_CORNER_RADIUS ?>;
    width: calc(100% - <?php echo $PRICE_CARD_MARGIN ?>*2);
    height: <?php $PRICE_CARD_HEIGHT ?>;
    box-shadow: <?php echo $CARD_SHADOW?>;
    display: inline-block;
    margin: <?php echo $PRICE_CARD_MARGIN ?>;
}

/*Stile della testata della card*/
.price-card-head{
    width: 100%;
    height: <?php echo $PRICE_CARD_HEAD_HEIGHT ?>;
    display: flex;
    border-top-left-radius: <?php echo $CARD_CORNER_RADIUS ?>;
    border-top-right-radius: <?php echo $CARD_CORNER_RADIUS ?>;
    border-bottom-left-radius: 0px;
    border-bottom-right-radius: 0px;
    
    background-size: 100%;
    background-position: center; 
    background-color: rgb(40,40,40);
    box-shadow: 0 3px 16px 0 rgba(0,0,0,0.4);
    flex-direction: column;
    align-items: center;
    justify-content: space-around;

}

/*.price-card-head{
    background-color: black;
}*/

/*Stile del nome del giorno*/
.price-card-day-name{
    color: white;
    text-align: center;
    vertical-align: middle;
    text-shadow: 0 0 0.1em white;
    font-family: 'archivo_narrow';
    font-size: 2.5em;
}

/*Stile del nome della promozione*/
.price-card-promotion-name{
    color: white;
    text-align: center;
    vertical-align: middle;
    text-shadow: 0 0 0.1em white;
    font-family: 'archivo_narrow';
    font-size: 1.7em;
}

/*Stile del prezzo principale*/
.price-card-main-price{
    display: inline-flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    font-family: 'tall_film';
    font-size: 7em;
    font-weight: bold;
    color: black;
    position: relative;
    top: -10px;
    line-height: 70%;
    text-align: center;
}

/*Stile del nome del prezzo principale*/
.price-card-main-price-name{
    font-size:0.25em;
    margin:0px;
    padding:0px;
    position:relative;
    font-family: 'archivo_narrow';
    font-weight: normal;
    top: 10px;
    color: rgb(128, 128, 128);
    text-align: center;
}

/*Stile del prezzo secondario*/
.price-card-subprice{
    font-family: 'tall_film';
    font-size: 4.5em;
    font-weight: bold;
    color: black;
    /*position: relative;*/
    /*top: -160px;*/
    padding-bottom:0px;
    margin-bottom:0px;
    line-height: 70%;
    text-align: center;
}


/*Stile del nome del prezzo secndario*/
.price-card-subprice-name{
    font-size:0.3em;
    margin:0px;
    padding:0px;
    position:relative;
    font-family: 'archivo_narrow';
    font-weight: normal;
    top: 2px;
    color: rgb(128, 128, 128);
    text-align: center;
}


/*Stile di una riga della card dei prezzi (alla fine è circa una tabella...)*/
.price-card-row{
    display:table-row;
}


