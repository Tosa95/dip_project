/*<style>*/

<?php
    header("Content-type:text/css");
    require_once dirname(__FILE__) . '/../config.php';

?>

@font-face {
   font-family: 'munich';
   src: url('<?php echo $FONTS_FOLDER . "munich.ttf" ?>') format('truetype');
}

@font-face {
   font-family: 'tall_film';
   src: url('<?php echo $FONTS_FOLDER . "tall_film.ttf" ?>') format('truetype');
}

@font-face {
   font-family: 'tall_film_expanded';
   src: url('<?php echo $FONTS_FOLDER . "tall_film_expanded.ttf" ?>') format('truetype');
}

@font-face {
   font-family: 'archivo_narrow';
   src: url('<?php echo $FONTS_FOLDER . "archivo_narrow.ttf" ?>') format('truetype');
}

@font-face {
   font-family: 'montserrat_regular';
   src: url('<?php echo $FONTS_FOLDER . "montserrat_regular.ttf" ?>') format('truetype');
}

@font-face {
   font-family: 'ubuntu-regular';
   src: url('<?php echo $FONTS_FOLDER . "ubuntu-regular.ttf" ?>') format('truetype');
}

@font-face {
   font-family: 'palanquin-regular';
   src: url('<?php echo $FONTS_FOLDER . "palanquin-regular.ttf" ?>') format('truetype');
}

@font-face {
   font-family: 'opensans-regular';
   src: url('<?php echo $FONTS_FOLDER . "opensans-regular.ttf" ?>') format('truetype');
}

@font-face {
   font-family: 'raleway-regular';
   src: url('<?php echo $FONTS_FOLDER . "raleway-regular.ttf" ?>') format('truetype');
}

body {
    margin-top: <?php echo $NAVBAR_HEIGHT ?>;
    font-family: 'opensans-regular';
    font-size: 1em;
}

@media (min-width: 992px) {
    body {
        padding-top: <?php echo $NAVBAR_HEIGHT ?>;
    }
}

.pred_more_than_0{
   font-weight: bold;
   color: #990000;
}

.imgcont {
    padding: 10px;
    border-radius: 5px;
    color: white;
    background-color: <?php echo $BUTTON_COLOR_NEUTRAL ?>;
    color: black;
    margin: 4px;
}

.imgcont-big {
    padding: 10px;
    border-radius: 5px;
    color: white;
    background-color: <?php echo $BUTTON_COLOR_NEUTRAL ?>;
    color: black;
    margin: 4px;
    min-height: 450px;
    width: 300px;
}

.home-tile {
    padding: 30px;
    border-radius: 5px;
    color: white;
    background-color: <?php echo $BUTTON_COLOR_NEUTRAL ?>;
    color: black;
    margin: 4px;
    height: 450px;
    width: 300px;
}

.tile-title{
    font-size: 24pt;
}

.button-accept {
    background-color: #74bc00;
    color: #FFFFFF;
}

.imgcont-sz{
    width: auto;
    height: 100px;
}

.little-label-repr{
    width: 60px;
    height: auto;
}

.button {
    padding: 10px;
    border-radius: 5px;
    color: white;
    box-shadow: <?php echo $BUTTON_SHADOW?>;
    border: none;
    margin: 4px;
    cursor: pointer;
    -webkit-user-select: none;  /* Chrome all / Safari all */
    -moz-user-select: none;     /* Firefox all */
    -ms-user-select: none;      /* IE 10+ */
    user-select: none;          /* Likely future */ 
}

.button:active {
    border: none;
    outline: none;
}

.button:focus {
    border: none;
    outline: none;
}

.carousel-item {
    width: 100%;
    height: 0;
    padding-bottom: 30%;
    min-height: 300px;
    background: no-repeat center center scroll;
    -webkit-background-size: contain;
    -moz-background-size: contain;
    -o-background-size: contain;
    background-size: contain;
}

.portfolio-item {
    margin-bottom: 30px;
}

/*.carousel-control-prev-icon, .carousel-control-next-icon {
    height: 6em;
    width: 6em;
    outline: black;
    background-color: rgba(0, 0, 0, 0.15);
    background-size: 90%, 90%;
    border-radius: 20%;
}*/

/*.carousel-control-prev-icon{
    background-image: url("../images/vectorial/carousel_prev.png");
    outline: black;
    border-radius: 20%;
    height: 6em;
    width: 6em;
}*/

/*.carousel-control-next-icon{
    background-image: url("../images/vectorial/carousel_next.png");
    height: 6em;
    width: 6em;
}*/

.card {
    box-shadow: <?php echo $CARD_SHADOW?>;
    margin-top: 20px;
    margin-bottom: 20px;
    transition: 0.3s;
    background: <?php echo $CARD_COLOR?>;
}

.page-content{
    margin-top: 20px;
    margin: 0 auto;
    width: 100%;
}

.standard-borders{
    margin-left: 3%;
    margin-right: 3%;
}

.card-img-left {
    border-bottom-left-radius: calc(.25rem - 1px);
    border-top-left-radius: calc(.25rem - 1px);
    float: left;
    padding-right: 1em;
    margin-bottom: -1.25em;
}

.film-image{
    display: block;
    max-width:5000px;
    max-height:500px;
    width: auto;
    height: auto;
}

dl {
    padding: 0.5em;
}

dt {
    float: left;
    clear: left;
    width: auto;
    font-weight: bold;
}

dt::after {
    content: ":";
}

dd {
    margin: 0 0 0 5em;
    padding: 0 0 0.5em 0;
}

.info-dd {
    margin: 0 2em 0 2em;
    padding: 0 2em 0 2em;
}

.info-dt {
    margin: 1em 0 0 0;
}

th, td { 
    padding: 5px; 
}

.vl {
    border-left: 6px solid green;
    height: 100%;
}

body {
    color: <?php echo $TEXT_COLOR ?>;
    background: url(../images/background.png) <?php echo $BG_COLOR ?>;
    background-repeat: repeat-y;
    background-size: 100%;
    height:100%;
    margin-top: <?php echo $NAVBAR_HEIGHT ?>;
    padding-top: 10px;
}

/* navbar */
.deep-bg {
    background-color: <?php echo $DEEP_BG_COLOR ?>;
    border-color: #E7E7E7;
    box-shadow: <?php echo $NAVBAR_SHADOW?>;
}

.title {
    color: <?php echo $TITLE_COLOR ?>;
    font-size: 2em;
    margin-top: 40px;
    margin-bottom: 20px;
    text-shadow: 0 0 0.1em white;
}

.modal {
    color: <?php echo $MODAL_TEXT_COLOR ?>;
}

.info-p {
    text-align: justify;
    text-justify: inter-word;
    font-size: 20px;
}

.map {
    min-height: 400px;
    height:100%;
}

.centered-text {
    text-align: center;
}



.user-img {
    margin: auto;
    max-height: 15px;
    padding-right: 6px;
    display: inline;
    vertical-align:middle;
    margin-bottom: 5px;
}

.user-button {          
    background-color: rgba(200, 200, 200, 0.3);
    vertical-align: middle;
    border-radius: 15px;
    margin-left: 10px;
}

.admin-img {
    margin: auto;
    max-height: 15px;
    padding-right: 6px;
    display: inline;
    vertical-align:middle;
    margin-bottom: 2px;
}

.admin-button {          
    background-color: rgba(255, 0, 0, 0.5);
    vertical-align: middle;
    border-radius: 15px;
    margin-left: 10px;
}

.admin-button:hover {          
    background-color: rgba(255, 0, 0, 0.7);
    vertical-align: middle;
    border-radius: 15px;
    margin-left: 10px;
}

.login-button {
    margin-left: 10px;
    color: rgb(255,255,255);
}

.user-button:hover {          
    background-color: rgba(200, 200, 200, 0.5);
}

.seat{
    flex-grow: 1;
    flex-basis: 0;
}

.screen{
    margin-top: 20px;
    width:100%;
    display:box;
    flex-shrink:1;
    align-self: center;
    flex: 0 0 auto;
}

.timetable-link{
    background-color: <?php echo $TIMETABLE_LINK_COLOR?>;
    vertical-align: middle;
    border-radius: 15px;
    padding: 3px;
    padding-left: 6px;
    padding-right: 6px;
    color: white;
    margin: 10px;
}

.timetable-icon-container{
    height:12px;
    position: absolute;
    top: -5px;
}

.timetable-icon{
    width: auto;
    height:100%;
}

.projection-time-container{
    display: inline-flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    position:relative;
}

.timetable-row {
    margin:10px;
}

.film-image-reservations{
    max-width: 100%;
    max-height:100%;
    max-height: 180px;
    box-shadow: <?php echo $SHADOW_COLOR?>;
}

.reservations-info{
    padding: 0px;
    margin: 0px;
    margin-bottom: 0px;
}

.btn{
    box-shadow: <?php echo $BUTTON_SHADOW?>;
}

.btn:hover{
    box-shadow: <?php echo $BUTTON_SHADOW_HOVER?>;
}

.btn:active{
    box-shadow: <?php echo $BUTTON_SHADOW_CLICK?>;
}

.btn-primary{
    background-color: <?php echo $BUTTON_COLOR?>;
    
    border-color: <?php echo $BUTTON_COLOR?>;
}

.btn-primary:hover{
    background-color: <?php echo $BUTTON_COLOR_HOVER?>;
    border-color: <?php echo $BUTTON_COLOR_HOVER?>;
}

.btn-primary:active{
    background-color: <?php echo $BUTTON_COLOR_HOVER?>;
    border-color: <?php echo $BUTTON_COLOR_HOVER?>;
}

.reservation-form{
    margin: 20px;
}

.rel{
    position: relative;
}

.spacer{
    margin: 40px;
}

.little-playbill{
    max-height: 100px;
    padding-bottom: 5px;
    padding-top: 5px;
}

.centred-vertically{
    display: table;
}

.span-centred{
    display: table-cell;
    vertical-align: middle;
}

.navbar-logo{
    max-height: 60px;
    padding-right: 20px;
}

/*footer{
   position: absolute;
   bottom: 0px;
   width:100%;
}*/

a{
    color: <?php echo $ANCHOR_TEXT_COLOR ?>;
    text-decoration: none;    
}

a:hover{
    color: <?php echo $ANCHOR_TEXT_COLOR_HOVER ?>;
    text-decoration: none;    
}

.carousel-item{
    background-size: cover;
}

.nav-higher{
    height: <?php echo $NAVBAR_HEIGHT ?>;
}

.grid-image{
    
    /*height: <?php echo $GRID_IMG_HEIGHT ?>;*/
    width: 100%;
    background-size: 100%;
    background-position: center;
    padding: 0px;
    
}

.grid-overlay{
    width: 100%;
    height: 100%;
    background: <?php echo $OVERLAY_COLOR_GRADIENT ?>;
    display: none;
}

.grid-infos{
    position:absolute;
    bottom:0;
    padding: 10px;
}

.grid-actions{
    position:absolute;
    top:0;
    padding: 20px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    width:100%;
}

.grid-title{
    color: rgb(255,255,255);
    font-size: 1.7em;
    text-shadow: 2px 2px 5px #000000;
}

.grid-genre{
    color: rgb(200,200,200);
    font-size: 1em;
    text-shadow: 2px 2px 5px #000000;
}

.navbar {
  min-height: <?php echo $NAVBAR_HEIGHT ?>;
}


.button-primary{
    background-color: <?php echo $BUTTON_COLOR_PRIMARY ?>;
}

.button-danger{
    background-color: <?php echo $BUTTON_COLOR_DANGER ?>;
}

.button-neutral{
    background-color: <?php echo $BUTTON_COLOR_NEUTRAL ?>;
    color: black;
}

.button-return-home{
    width:auto; 
    margin-bottom: 20px;
}

.projection-card{
    background-color: <?php echo $CARD_COLOR ?>;
    border-radius: <?php echo $CARD_CORNER_RADIUS ?>;
    width: 100%;
    /*margin: 10px;*/
    margin-bottom: 20px;
    box-shadow: <?php echo $CARD_SHADOW?>;
    display: block;
    position: relative;
    padding-top: 10px;
    padding-bottom: 20px;
}

.projection-card-head{
    width: 100%;
    height: <?php echo $GRID_IMG_HEIGHT ?>;
    display: block;
    border-top-left-radius: <?php echo $CARD_CORNER_RADIUS ?>;
    border-top-right-radius: <?php echo $CARD_CORNER_RADIUS ?>;
    background-size: 100%;
    background-position: center; 
    position: absolute;
    top: 0px;
    left: 0px;
    right: 0px;
    margin: 0px;
}

.projection-card-head-overlay{
    width: 100%;
    height: 100%;
    border-top-left-radius: <?php echo $CARD_CORNER_RADIUS ?>;
    border-top-right-radius: <?php echo $CARD_CORNER_RADIUS ?>;
    background: <?php echo $OVERLAY_COLOR_GRADIENT ?>;
}

.projection-card-title{
    color: rgb(255,255,255);
    font-size: 2em;
    text-shadow: 2px 2px 5px #000000;
    position:absolute;
    bottom:0;
    padding: 10px;
}

.projection-card-infos{
    margin-top: <?php echo $GRID_IMG_HEIGHT ?>;
    padding-top: 0px;
    margin-left: 20px;
    margin-right: 20px;
}

.table-cell-spacing{
    padding-top: <?php echo $VERTICAL_CELL_SPACING ?>;
    padding-bottom: <?php echo $VERTICAL_CELL_SPACING ?>;
    padding-left: <?php echo $HORIZONTAL_CELL_SPACING ?>;
    padding-right: <?php echo $HORIZONTAL_CELL_SPACING ?>;
}

.array-row{
    padding-top: <?php echo $VERTICAL_ARRAY_SPACING ?>;
    padding-bottom: <?php echo $VERTICAL_ARRAY_SPACING ?>;
}

.suggestion{
    color: #e5d88b;
    font-weight: bold;
    font-size: 1.2em;
    
}

.film-info{
    text-align: justify;
    padding: 0px;
    margin: 0px;
}


a.anchor {
    display: block;
    position: relative;
    top: -<?php echo $ANCHOR_HEIGHT?>;
    visibility: hidden;
}

.button-circular{
    border-radius: 50%;
    display: inline-block;
    width: <?php echo $CIRCULAR_BUTTON_DIAMETER?>;
    height: <?php echo $CIRCULAR_BUTTON_DIAMETER?>;
    margin: 2px;
}

.button-circular-big{
    border-radius: 50%;
    display: inline-block;
    width: calc(<?php echo $CIRCULAR_BUTTON_DIAMETER?>*3);
    height: calc(<?php echo $CIRCULAR_BUTTON_DIAMETER?>*3);
    margin: 2px;
    background-color: <?php echo $BUTTON_COLOR_PRIMARY ?>;
    font-size: 60px;
    box-shadow: 3px 3px 9px rgb(0,0,0);
}

.button-on-top {
    position: fixed;
    top: calc(100% - 150px);
    left: calc(100% - 150px);
}

.button-confirm {
    position: fixed;
    bottom: 0px;
    left: 0px;
    text-align: center;
    height: 150px;
    width: calc(100% - 10px);
    font-size: 2em;
    z-index:1000
}

.img-thumbnail-cont{
    position: fixed;
    top: 0px;
    left: 0px;
    text-align: center;
    height: 150px;
    width: 150px;
    z-index:1000000;
}

.img-thumbnail{
    height: 140px;
    width: 140px;
}

.button-text-icon{
    border-radius: <?php echo $CIRCULAR_BUTTON_DIAMETER?>;
    display: inline-block;
    min-width: 100px;
    min-height: 100px;
    text-align: center;
    height: <?php echo $CIRCULAR_BUTTON_DIAMETER?>;
    margin: 10px;
    padding: 3px;
}

.button-text-separator{
    border-left: solid #ffffff55;
    height: 80%;
    margin: 10px;
    display: inline-block;
}

.button-text-container{
    /*margin-right: 10px;*/
    display: inline-block;
}

.button-cnt-container{
    /*margin-right: 10px;*/
    display: block;
    font-size: 8pt;
}

.button-delete{
    background-color: <?php echo $DELETE_COLOR ?>;
}

.button-delete-image{
    background-image: url(<?php echo $IMAGES_FOLDER . "vectorial/delete.png" ?>);
    background-size: 90% 90%;
    background-position: center;
    height: 100%;
    width: 100%;
    background-repeat: no-repeat;
}
.button-purchase{
    background-color: <?php echo $PURCHASE_COLOR ?>;
}

.button-purchase-image{
    background-image: url(<?php echo $IMAGES_FOLDER . "vectorial/purchase.png" ?>);
    background-size: 100% 100%;
    background-position: center;
    height: 100%;
    width: 100%;
    background-repeat: no-repeat;
    position:relative;
    left: -1.5px;
}

.button-modify{
    background-color: <?php echo $MODIFY_COLOR ?>;
}

.button-modify-image{
    background-image: url(<?php echo $IMAGES_FOLDER . "vectorial/modify.png" ?>);
    background-size: 100% 100%;
    background-position: center;
    height: 120%;
    width: 120%;
    background-repeat: no-repeat;
    position:relative;
    left: -1.5px;
    top: -2px;
}

.aspect-ratio{
    display: none;
}

.purchased-label{
    color: white;
    border-radius: 10px;
    background-color: <?php echo $PURCHASE_COLOR ?>;
    font-weight: bolder;
    box-shadow: <?php echo $BUTTON_SHADOW ?>;
    color: white;
    padding: 5px;
}

.circular-button-centered-text{
    display: inline-flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
}

.string-tag{
    background-color: <?php echo $BUTTON_COLOR_NEUTRAL?>;
    color: black;
    border-radius: 50px;
    text-align: center;
}

.inc-dec-button{
    height: 30px;
    width: 30px;
    font-size: 1.2em;
}

.inc-dec-container{
    display: inline-flex;
    flex-direction: row;
    justify-content: space-around;
    align-items: center;
    width:100%;
    background-color: rgba(255,255,255,0.15);
    border-radius: 50px;
    padding: 5px;
    margin: 5px;
}

.inc-dec-internal-container{
    display: inline-flex;
    flex-direction: row;
    justify-content: space-around;
    align-items: center;
}

.inc-dec-label{
    text-align:center;
    font-weight: bold;
}

.constant-value{
    box-shadow: <?php echo $BUTTON_SHADOW ?>;
    background-color: <?php echo $BUTTON_COLOR_NEUTRAL ?>;
    height: 30px;
    width: auto;
    border-radius: 30px;
    color: black;
    text-overflow: ellipsis;
    text-align: center;
}

.stay-narrow{
    max-width: 400px;
}

.purchased-tag-color{
    background-color: <?php echo $PURCHASE_COLOR ?>;
    color: white;
}

.counter-spacing{
    margin:8px;
}   

.trailer-grid-upper-margin{
    margin-top: 30px;
}

.trailer-button{
    height: 70px;
    width: 70px;
    background-image: url(<?php echo $IMAGES_FOLDER?>vectorial/trailer_button.png);
    background-size: 100%;
    background-position: center; 
}

.trailer-button:hover{
    background-image: url(<?php echo $IMAGES_FOLDER?>vectorial/trailer_button_hover.png);
}

.trailer-button:active{
    background-image: url(<?php echo $IMAGES_FOLDER?>vectorial/trailer_button_click.png);
}

.trailer-button-margin{
    margin-top: 20px;
}

.carousel-text-shadow{
    text-shadow: 0 0 0.2em black;
}

.vf-validation-alert{
    padding: 5px;
    margin: 10px;
    background-color: rgb(240,240,240);
    border: solid 4px rgb(188, 47, 28);
    border-radius: 10px;
    font-weight: bold;
    color: black;
    box-shadow: <?php echo $BUTTON_SHADOW ?>;
}

.progress-in-flex{
    margin: 10px;
}

.progress{
    background-color: rgb(128,128,128);
    height: 30px;
    width: 100%;
}

.progress-bar{
    height: 100%;
    width: 100%;
}

.full-width{
    width:100%;
}

.info-button{
    border: solid 1px rgb(230, 230, 230);
    background-color: rgb(210,210,210);
    height: 15px;
    width: 15px;
    font-size: 10px;
    border-radius: 50px;
    color: black;
    font-weight: bold;
    display: inline-block;
    line-height: 100%;
    font-family: 'opensans-regular';
}

.info-button-text{
    position: relative;
    top: 1.5px;
    left: 0px;
    cursor: pointer;
    display: inline-block;
    font-family: 'opensans-regular';
}

.info-button-dark{
    border: solid 1px rgb(25, 25, 25);
    background-color: rgb(80, 80, 80);
    height: 15px;
    width: 15px;
    font-size: 10px;
    border-radius: 50px;
    color: black;
    font-weight: bold;
    display: inline-block;
    line-height: 100%;
    font-family: 'opensans-regular';
}

.info-button-text-dark{
    position: relative;
    top: 1.5px;
    left: 0px;
    cursor: pointer;
    display: inline-block;
    font-family: 'opensans-regular';
    color: white;
}

.info-range-label{
    color: black;
    border-radius: 50px;
    background-color: rgb(214, 214, 214);
    font-weight: bold;
    box-shadow: <?php echo $BUTTON_SHADOW ?>;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 5px;
    padding-bottom: 5px;
    text-align: center;
    display: inline-flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
}

.label_desc {
    color: <?php echo $TITLE_COLOR ?>;
    font-size: 1.5em;
    margin-top: 20px;
    margin-bottom: 10px;
    text-shadow: 0 0 0.1em white;
}


.label_value {
    color: <?php echo $TITLE_COLOR ?>;
    font-size: 1em;
    text-shadow: 0 0 0.1em white;
}