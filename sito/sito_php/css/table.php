/*<style>*/

<?php
    header("Content-type:text/css");
    require_once dirname(__FILE__) . '/../config.php';
    
    $CARD_PADDING = "20px";
?>

.list-footer{
    display: inline-flex;
    flex-direction: row;
    align-items: center;
    flex-wrap: wrap;
    width: 100%;
}

.footer-row{
}

.footer-cell{
    display:inline-flex;
    width: 30px;
    height: 30px;
    padding:0px;
    margin:5px;
    text-align: center;
    flex-direction: column;
    justify-content: center;
    cursor:pointer;
    background-color: <?php echo $FOOTER_BUTTON_COLOR ?>;
}

.footer-text{
    display: inline-block;
    vertical-align: middle;
    text-align: center;
    position: relative;
    top: -1.5px;
    position: relative;
}

.content-card{
    background-color: rgb(230,230,230);
    border-radius: <?php echo $CARD_CORNER_RADIUS ?>;
    /*width: 100%;*/
    box-shadow: <?php echo $CARD_SHADOW?>;
    
    margin-top: <?php echo $CONTENT_CARD_MARGIN ?>;
    margin-bottom: <?php echo $CONTENT_CARD_MARGIN ?>;
    color: black;
    padding: <?php echo $CARD_PADDING ?>;
    
    display: flex;
    justify-content: space-around;
    align-items: center;

}

.list-head{
    background-color: <?php echo $HEAD_CARD_COLOR ?>;
    border-radius: <?php echo $CARD_CORNER_RADIUS ?>;
    /*width: 100%;*/
    box-shadow: <?php echo $CARD_SHADOW?>;
    
    margin-bottom: <?php echo $CONTENT_CARD_MARGIN ?>;
    color: white;
    padding: 10px;
    padding-top: 5px;
    padding-bottom: 5px;
    
    display: flex;
    justify-content: space-around;
    align-items: center;

}

.list-filter{
    background-color: <?php echo $HEAD_CARD_COLOR ?>;
    border-radius: <?php echo $CARD_CORNER_RADIUS ?>;
    /*width: 100%;*/
    box-shadow: <?php echo $CARD_SHADOW?>;
    
    margin-bottom: <?php echo $CONTENT_CARD_MARGIN ?>;
    color: white;
    padding: 10px;
    padding-top: 5px;
    padding-bottom: 5px;
    
    display: flex;
    justify-content: space-around;
    align-items: center;

}

.string-filter-input{
    border-radius: <?php echo $CARD_CORNER_RADIUS ?>;
    width: 100%;
    border: none;
    padding: 5px;
}

.image-card{
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    width: 100%;
}

.image-card-row-container{
    display: inline-flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    padding-top: 10px;
    padding-bottom: 10px;
}

.image-card-text-container{
    margin: 5px;
    text-align: right;
}

.image-card-footer-container{
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    width: 100%;
}

.button-load-more{
    background-color: <?php echo $FOOTER_BUTTON_COLOR ?>;
    cursor: pointer;
    position: relative;
}

.flex-table{
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    width: 100%;
}

.flex-row{
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    width: 100%;
}

.flex-cell{
    display: inline-flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    flex-grow: 1;
    flex-basis: 0;
    /*flex: 0 0 auto;*/
    width: 100%;
}

.image-card-head{
    width: calc(100% + 2*<?php echo $CARD_PADDING?>);
    height: <?php echo $GRID_IMG_HEIGHT ?>;
    display: block;
    border-top-left-radius: <?php echo $CARD_CORNER_RADIUS ?>;
    border-top-right-radius: <?php echo $CARD_CORNER_RADIUS ?>;
    background-size: 100%;
    background-position: center; 
    position: relative;
    top: -<?php echo $CARD_PADDING?>;
}

.image-card-head-overlay{
    width: 100%;
    height: 100%;
    border-top-left-radius: <?php echo $CARD_CORNER_RADIUS ?>;
    border-top-right-radius: <?php echo $CARD_CORNER_RADIUS ?>;
    background: <?php echo $OVERLAY_COLOR_GRADIENT ?>;
}

.image-card-title{
    color: rgb(255,255,255);
    font-size: 2em;
    text-shadow: 2px 2px 5px #000000;
    position:absolute;
    bottom:0;
    padding: 10px;
}

.image-card-info-container{
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: flex-start;
}

.image-card-date-room{
    font-size: 1.1em;
    font-weight: bolder;
    padding-top: 10px;
    padding-bottom: 10px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 100%;
}

.room-label{
    color: white;
    border-radius: 50px;
    background-color: <?php echo $BUTTON_COLOR_PRIMARY ?>;
    font-weight: bold;
    box-shadow: <?php echo $BUTTON_SHADOW ?>;
    color: white;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 5px;
    padding-bottom: 5px;
    text-align: center;
}

.image-card-info-field{
    padding-top: 3px;
    padding-bottom: 3px;
}

.image-card-info-actions{
    padding-top: 15px;
    padding-bottom: 5px;
    align-self: center;
}

.image-card-long-field-container{
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: flex-start;
}

.image-card-long-field-value{
    margin-left: 20px;
    margin-top: 5px;
}

.image-card-long-field-label{
    margin-top: 10px;
}



.hidden-button {
    display: none;
}

.image-card a{
    color: <?php echo $BUTTON_COLOR_PRIMARY ?>;
}