<?php
    
    require_once dirname(__FILE__). '/factories.php';
    
    session_start();
    
    $info = '<div class="container-fluid">' .
            '<img class="img-fluid mx-auto d-block" src="images/sala.png"><br/>' .
            '<p class="info-p">' . "La <strong>multisala KING</strong> è stata inaugurata il 18 dicembre 2003. Dispone di 1284 posti suddivisi in 5 sale: la più grande da 454, due da 221, una da 211 e la più piccola da 177. Tutte e cinque le sale sono disposte a gradoni con una distanza da fila a fila di 1,20 mt. E' una distanza ragguardevole che consente di avere un'ottima libertà sia di movimento che di seduta. 

                La multisala KING è davvero europea: le poltrone sono progettate e realizzate appositamente per KING dalla Ezcaray (Spagna) per consentire il miglior confort dello spettatore. Sono tutte numerate sia davanti che dietro e dotate di portabicchiere. In alcune questo manca perché il bracciolo si solleva permettendo alle due poltrone di diventare un comodissimo divanetto.
                Tali poltrone sono chiamate SITTING-LOVE.

                Le macchine da proiezione arrivano dalla Germania: KINOTON firma questa installazione tutta elettronica che garantisce una nitidezza e una qualità dell’immagine ineguagliabile.</p>"

            . '<p class="info-p">' . "Gli schermi PERLUX sono inglesi e anch’essi danno il loro contributo per ottenere la massima luminosità dall’immagine.
                Capitolo a parte l’acustica delle sale: tutto è stato curato nei minimi particolari per garantire la qualità più sofisticata. Le 5 sale sono strutturalmente separate le une dalle altre per evitare qualsiasi ponte acustico fastidiosissimo per questi ambienti. Sono stati utilizzati materiali per la maggior parte sconosciuti in Italia per coperture e tamponamenti, così come particolare cura e attenzione sono stati utilizzati per la realizzazione dei rivestimenti all’interno delle sale.
                Sia il sistema di riscaldamento che di condizionamento sono stati studiati e realizzati per ridurre in modo drastico il rumore di fondo. Lo spettatore che entra in una sala KING avverte immediatamente quest’atmosfera avvolgente e di silenzio ovattato che poi lascia il posto ad un suono perfetto esaltato dal sistema DOLBY DIGITAL con parametri che ampiamente rientrano nella certificazione THX.
                La multisala KING apre ogni giorno alle 19,00 per poi presentare due proiezioni serali. Nei festivi invece la struttura apre le sue porte alle 13,30 perché ci sono proiezioni fin dal primo pomeriggio.
                Ai nostri clienti sono chiesti due piccoli sacrifici : la puntualità, dal momento che NON è consentito l’ingresso in sala a spettacolo iniziato e il divieto di fumare in ogni zona della multisala. Sono piccoli sacrifici che chiediamo a tutti per rispetto verso il pubblico già accomodato in sala nel primo caso e per salvaguardare la salute di tutti nel secondo. Per questo ringraziamo tutti già fin d’ora.</p>"
            . '<p class="info-p"><strong>Le nostre sale:</strong><br/><ul>'
            . '<li class="info-p">' . "Tutte le nostre sale hanno una disposizione
                a gradoni con una distanza da fila a fila di 1,20 mt. 
                Poltrone <b>SITTING-LOVE</b>, progettate e realizzate appositamente per il KING.
                Schermo inglese <b>PERLUX</b>, per garantire la massima luminosità.
                Sistema audio <b>DOLBY DIGITAL</b>, con parametri che rientrano ampliamente per ottenere la certificazione THX.</li>"
            
            . '<li class="info-p">' . "La sala 1 è la più grande e capiente del KING.
                Può contenere infatti ben 454 posti. Le sale 2 e 3 contengono rispettivamente 
                221 posti, la sala 5 poco meno, 211, mentre la sala 4 ospita 177 poltrone."
            . '</ul></p>'
            . '<p class="info-p"><strong>I nostri servizi</strong> <br/> <ul>'
            . '<li class="info-p"><strong>Bar:</strong><br/>' . "I bar sono due: uno nella hall con produzione propria di pop-corn "
            . "sempre caldo e distribuzione di bevande, uno al piano inferiore (vi si accede da una scala posta dietro il bar centrale)"
            . " con PANINOTECA e CAFFETTERIA dove è possibile consumare uno squisito panino caldo, una fumante ciccolata calda con panna"
            . ", una tazza di buon the di qualita' da scegliere in innumerevoli fragranze, anche esotiche, accompagnate da pasticcini di"
            . " alta pasticceria, seduti comodamente ai nostri tavolini PRIMA o DOPO le nostre proiezioni.</li>"
            . '<li class="info-p"><strong>Cielo stellato:</strong><br/>' . "Al centro del salone della Multisala KING, "
            . "volgendo lo sguardo in alto si può ammirare una vera e propria 'chicca', unica nel suo genere. "
            . "Il soffitto, infatti, mostra una sorta di squarcio su di un cielo virtuale riprodotto nei minimi dettagli. "
            . "Si possono infatti riconoscere molte costellazioni del nostro sistema solare.Un effetto che lascià veramente "
            . "a bocca aperta.</li>"
            . '<li class="info-p"><strong>Caramelleria:</strong><br/>' . "Per i più golosi, squisite caramelle da gustare prima,"
            . " durante e dopo la visione dei nostri film.</li>"
            . '</ul></p>'
            .'</div>';
    
    $de = get_document_elements();
    
    echo $de->info_page_template("La multisala", $info, TRUE);

