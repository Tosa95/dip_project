<?php
    
    require_once dirname(__FILE__). '/factories.php';
    require_once dirname(__FILE__). '/view/html_basic_elements.php';
    
    session_start();
    
    $plp = get_projection_list_presenter();
    $de = get_document_elements();
    
    # Nel palinsesto ci saranno le proiezioni correnti
    $future = FALSE;
    
    $content = $plp->projections_list($future);

    echo $de->basic_page_template($content, TRUE, TRUE);