<?php

    /*
     * File di configurazione globale.
     */
    
    date_default_timezone_set("Europe/Rome");
    ini_set('default_charset', 'utf-8');

    # Indirizzo del sito
    $SITE_ADDRESS = "http://localhost";
    # Path base del sito
    $SITE_ROOT = "/segnali/";
    # Path della directory contenente le immagini
    $IMAGES_FOLDER = $SITE_ROOT . "images/";
    # Path della directory contenente i font
    $FONTS_FOLDER = $SITE_ROOT . "fonts/";
    # Path della directory contenente le icone delle varie tipologie di cartelli
    $ICON_FOLDER = "images/icons/";
    # Path della directory contenente le immagini dei segnali catturate
    $SIGNALS_FOLDER = "images/signals/";
    # Semplifica la rappresentazione del film su dispositivi più piccoli di large,
    # eliminando la tabella oraria ed altri campi
    $REMOVE_FIELDS_ON = "lg";
    
    
    # Colore di background
    $BG_COLOR = "#333333";
    # Colore di background della navbar
    $DEEP_BG_COLOR = "#111111";
    # Colore dei titoli
    $TITLE_COLOR = "#ffffff";
    
    # Colore delle card
    $CARD_COLOR = "rgb(80, 80, 80)";
    # Colore delle shadows
    $SHADOW_COLOR = "rgba(0,0,0,0.4)";
    # Colore delle shadows con hover
    $SHADOW_COLOR_HOVER = "rgba(0,0,0,0.8)";
    # Colore delle shadows con click
    $SHADOW_COLOR_CLICK = "rgba(0,0,0,1)";
    # Ombra della navbar
    $NAVBAR_SHADOW = "3px 3px 5px " . $SHADOW_COLOR;
    
    # ---------- Proprietà grafiche dei componenti base ----------
    # Incremento di luminosità di un bottone in hover (da -1 a 1)
    $BUTTON_LUMINANCE_HOVER = 0.2;
    
    # Incremento di luminosità di un bottone in click (da -1 a 1)
    $BUTTON_LUMINANCE_CLICK = -0.2;
    
    # Ombra di un bottone
    $BUTTON_SHADOW = "1px 1px 3px " . $SHADOW_COLOR;
    # Ombra di un bottone in hover
    $BUTTON_SHADOW_HOVER = "0.5px 0.5px 3px " . $SHADOW_COLOR_HOVER;
    # Ombra di un bottone in click
    $BUTTON_SHADOW_CLICK = "0.3px 0.3px 3px " . $SHADOW_COLOR_CLICK;
    # Ombra delle card
    $CARD_SHADOW = "3px 3px 5px " . $SHADOW_COLOR;
    # Colore di sfondo della card dei prezzi
    $PRICE_CARD_COLOR = "rgb(200, 200, 200)";
    # Margine sulle card dei prezzi
    $PRICE_CARD_MARGIN = "20px";
    # Altezza testata card dei prezzi
    $PRICE_CARD_HEAD_HEIGHT = "150px";
    # Altezza card dei prezzi
    $PRICE_CARD_HEIGHT = "400px";
    # Allineamento dei prezzi nel caso in cui rimangano soli su una riga
    $ALONE_PRICE_ALIGN = "left";
    
    # ---------- Colore bottoni ----------
    # Colore dei bottoni primari
    $BUTTON_COLOR_PRIMARY = "rgb(232, 127, 0)";
    # Colore bottoni danger
    $BUTTON_COLOR_DANGER = "rgb(179, 0, 0)";
    # Colore bottoni neutri
    $BUTTON_COLOR_NEUTRAL = "rgb(200, 200, 200)";
    # Colore tasto orari
    $TIMETABLE_LINK_COLOR = $BUTTON_COLOR_PRIMARY;
    # Colore tasto eliminazione
    $DELETE_COLOR = $BUTTON_COLOR_DANGER;
    # Colore tasto compra
    $PURCHASE_COLOR = "rgb(21, 117, 29)";
    # Colore tasto modifica
    $MODIFY_COLOR = "rgb(18, 163, 204)";
    # Colore tasto proiezioni
    $PROJECTIONS_COLOR = $BUTTON_COLOR_PRIMARY;
    
    # Text color
    $TEXT_COLOR = "rgb(200, 200, 200)";
    # Colore del testo dei pop up modal
    $MODAL_TEXT_COLOR = "rgb(0, 0, 0)";
    # Anchor text color
    $ANCHOR_TEXT_COLOR = "rgb(230, 230, 230)";
    # Anchor text color in hover
    $ANCHOR_TEXT_COLOR_HOVER = "rgb(255, 255, 255)";
    # Altezza della navbar
    $NAVBAR_HEIGHT = "70px";
    # Altezza di riposizionamento delle anchor
    $ANCHOR_HEIGHT = "100px";
    # Diametro dei pusanti circolari
    $CIRCULAR_BUTTON_DIAMETER = "40px";
    
    # Gradiente colore dell'overlay
    $OVERLAY_COLOR_GRADIENT = "linear-gradient(to top, rgba(0,0,0,1), rgba(0,0,0,0.6), rgba(0,0,0,0.0))";
    
    # ---------- Card dei film ----------
    # Altezza delle locandine nella griglia
    $GRID_IMG_HEIGHT = "200px";
    # Raggio smusso vertici delle card
    $CARD_CORNER_RADIUS = "5px";
    # Aspect ration (height/width) delle card dei film
    $FILM_CARD_ASPECT_RATIO = 2.7/2;
    
    # ---------- Liste ----------
    # Colore della testa della lista
    $HEAD_CARD_COLOR = "rgb(120, 120, 120)";
    # Colore dei pulstanti del footer se non cliccati
    $FOOTER_BUTTON_COLOR = $HEAD_CARD_COLOR;
    # Colore dei pulsanti del footer cliccati (pagina selezionata)
    $FOOTER_BUTTON_COLOR_CLICKED = $BUTTON_COLOR_PRIMARY;
    # Colore delle card utilizzate per rappresentare i contenuti della lista
    $CONTENT_CARD_COLOR = "rgb(200, 200, 200)";
    # Margini tra le card dei contenuti
    $CONTENT_CARD_MARGIN = "10px";
    
    # ---------- Tabelle ----------
    # Spazio orizzontale tra le celle
    $HORIZONTAL_CELL_SPACING = "5px";
    # Spazio verticale tra le celle
    $VERTICAL_CELL_SPACING = "5px";
    # Spazio verticale tra le righe per la funzione present_array
    $VERTICAL_ARRAY_SPACING = "6px";
    
    # Flag per decidere se si vuole un container fluid (occupano tutta la larghezza
    # disponibile) oppure un container semplice (a larghezza massima fissata).
    $CONTAINER_FLUID = FALSE; 
    
    # Classi per la tabella oraria dei film
    $TIME_TABLE_CLASSES = "d-none d-md-block";
    # Larghezza percentuale della prima colonna della tabella oraria, ossia quella
    # contenente nome del giorno, numero e mese.
    $TIME_TABLE_FIRST_COLUMN_PERCENTAGE = 40;
    
    # Dati di accesso al database
    $DB_USER = "sito";
    $DB_PSW = "SsCFcAnYg3TTaBa3";
    $DB_NAME = "segnali";
    $DB_HOST = "localhost";
    
    # ATTENZIONE: in caso di connessione fallita al database di XAMPP l'errore
    # potrebbe essere dovuto al fatto che l'utente è impostato sull'host % e ciò
    # non è compatibile con DB_HOST = localhost. Soluzione: sostituire % con 
    # localhost sui privilegi dell'utente in phpmyadmin
    
    # Quanti giorni di programmazione visualizzare
    $SHOW_PROJECTION_FOR_DAYS = 7;
    
    # ---------- VALIDAZIONE ----------
    
    # Se abilitato, elimina le "bolle" usate di default dai browser per visualizzare gli
    # errori. In questo caso, però bisogna mettere un alert per ogni campo con classe vf-alert-<id campo>
    $SUPPRESS_BUBBLES = TRUE;
    
    # Se abilitato, mostra solo il primo errore in caso di input non valido
    $SHOW_ONLY_FIRST_ERROR = TRUE;
    
    # Costanti per la validazione della form di registrazione
    $MIN_USERNAME_LENGTH = 4;
    $MAX_USERNAME_LENGTH = 12;
    $MIN_NAME_SURNAME_LENGTH = 2;
    $MAX_NAME_SURNAME_LENGTH = 30;
    
    # Pattern che l'username deve matchare per essere considerato valido
    $USERNAME_PATTERN = '^[A-Za-z0-9]*$';
    # Una breve descrizione testuale del significato della regexp di qui sopra. 
    # Non deve contenere informazioni sulla lunghezza
    $USERNAME_PATTERN_DESCRIPTION = "lettere e numeri";
    $USERNAME_ERROR_MSG = "Il nome utente può contenere solo lettere e numeri";
    
    # Pattern che nome e cognome devono matchare per essere considerati validi
    $NAME_SURNAME_PATTERN = '^[a-zA-Z\sàòèéìù]*$';
    # Una breve descrizione testuale del significato della regexp di qui sopra. 
    # Non deve contenere informazioni sulla lunghezza
    $NAME_SURNAME_PATTERN_DESCRIPTION = "lettere (anche accentate) e spazi";
    $NAME_SURNAME_ERROR_MSG = "Il nome può contenere solo $NAME_SURNAME_PATTERN_DESCRIPTION";
    
    # Elenco di pattern che devono esserci all'interno della password.
    $PASSWORD_CONTAINS = array("[A-Z]", "[a-z]", "[0-9]");
    # Nel caso in cui una delle condizioni sopra specificate non sia rispettata, l'utente verrà avvisato con il seguente messaggio:
    $PASSWORD_CONTAINS_ERROR_MESSAGE = "La password deve contenere almeno una lettera minuscola, una lettera maiuscola e un numero";
    
    $PASSWORD_MIN_LENGTH = 5;
    $PASSWORD_MAX_LENGTH = 20;
    
    # Pattern per la validazione del numero di telefono
    $PHONE_PATTERN = '^(\+39\s*)?(0|3)\d{2,3}(\s|-)*\d{6,7}$';
    $PHONE_ERROR_MSG = "Questo non è un numero di telefono valido";
    
    # Pattern per la validazione delle email, copiato da http://emailregex.com/ in versione javascript
    $EMAIL_PATTERN = "^(([^<>()\[\]\\.,;:\s@\"]+(\.[^<>()\[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$";
    $EMAIL_ERROR_MSG = "Questo non è un indirizzo email valido";
    
    # Pattern per la validazione del codice fiscale
    $CF_PATTERN = '^(?:(?:[B-DF-HJ-NP-TV-Z]|[AEIOU])[AEIOU][AEIOUX]|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}[\dLMNP-V]{2}(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[1256LMRS][\dLMNP-V])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM])(?:[A-MZ][1-9MNP-V][\dLMNP-V]{2}|[A-M][0L](?:[1-9MNP-V][\dLMNP-V]|[0L][1-9MNP-V]))[A-Z]$';
    $CF_ERROR_MSG = "Questo non è un codice fiscale valido";
    
    # Pattern per la validazione dell'indirizzo
    $ADDRESS_PATTERN = '^(via|piazza|strada)?\s+[a-zA-Z0-9\s]{2,}\s*\,?\s+\d{1,}$';
    $ADDRESS_ERROR_MSG = "Questo non è un indirizzo valido";
    
    # Pattern per la validazione del codice iban (es: IT17X0605502100000001234567)
    $IBAN_PATTERN = '^[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}$';
    $IBAN_ERROR_MSG = "Questo non è un codice iban valido";
    
    # Pattern per la validazione del codice iban (es: IT17X0605502100000001234567)
    $CREDIT_CARD_PATTERN = '^(4[0-9]{12}(?:[0-9]{3})?|(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}|3[47][0-9]{13})$';
    $CREDIT_CARD_ERROR_MSG = "Questo non è un codice valido per una carta di credito!";
    
    # Pattern per i numeri decimali (possono avere il punto)
    $DECIMAL_NUMBER_PATTERN = '^\d*\.?\d*$';
    $DECIMAL_NUMBER_ERROR_MSG = "Questo non è un numero decimale";
    
    # Pattern per i numeri interi
    $INTEGER_NUMBER_PATTERN = '^\d*$';
    $INTEGER_NUMBER_ERROR_MSG = "Questo non è un numero intero";
    
    # Pattern per i campi composti da sole parole
    $WORDS_PARTTERN = '^[a-zA-Z\s]*$';
    $WORDS_ERROR_MSG = "Questo campo deve essere composto da sole parole!";
    
    # Pattern per gli URL
    $URL_PATTERN = "^(https?:\/\/)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)$";
    $URL_ERROR_MSG = "Questo campo deve contenere un URL";
    
    # Massima età consentita per una persona
    $MAX_AGE_YEARS = 130;
    
    # ---------- CRYPTO ---------
    
    # Algoritmo di hashing da utilizzare per il salvataggio delle password nel db
    $HASH_ALGORITHM = "sha256";
    
    # Quante volte ripetere l'hashing per il salvataggio delle password nel db
    $HASH_ALGORITHM_REPETITIONS = 5;
    
    # Quanti caratteri utilizzare come sale per il salvataggio delle password nel db
    $SALT_LENGTH = 5;
    
    # Segreto utilizzato per generare le chiavi temporanee per cambiare la password
    $PASSWORD_RESET_KEY_SECRET = "annamariadoyouknowwhatbootstrapIIISSSSS?";
    
    #Segreto utilizzato per controllare la validità della signature del servizio di pagamento
    $PAYMENT_SERVICE_SECRET = "really_secure_secret_222333!!!???";
    
    $PAYMENT_SERVICE_HASH_ALGORITHM = "sha256";
    
    # Tempo per cui le chiavi per il cambio password restano valide (in ore!)
    $PASSWORD_RESET_KEY_VALIDITY = 2;
    
    # ---------- MAIL ----------
    
    $SMTP_SERVER = 'smtp.gmail.com';
    $SMTP_PORT = 587;
    $SMTP_AUTH = 'tls';
    $BOT_MAIL = 'multisalakingbot@gmail.com';
    $BOT_PASSWORD = 'annamaria15';

    # --------- RACCOLTA COMMENTI UTENTI ----------
    $GATHER_USER_COMMENTS = true;
    
    # --------- SERVIZIO WEB IMMAGINI -----
    $MAIN_ENV = True;
    $IMAGES_SERVICE_PATH = "___IMAGES_SERVICE_PATH___";
    $REMOTE_IMAGES_SERVICE_PATH = "___REMOTE_IMAGES_SERVICE_PATH___";