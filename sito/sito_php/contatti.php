<?php
    
    require_once dirname(__FILE__). '/factories.php';
    
    session_start();
    
    $info = '<div class="row">'
                . '<div class="col-12 col-md-6 ">'
                    . '<h2> MULTISALA KING </h2> '
                    . '<p class="info-p">'
                    . 'Lonato del Garda (BS) <br/>'
                    . 'SS567 SS Desenzano-Castiglione <br/>'
                    . 'tel. 030 9913670 <br/>'
                    . '<a href="mailto:multisalakingbot@gmail.com">multisalakingbot@gmail.com</a>'
                    . '</p>'
                    . '<img src="images/multisala.png" class="img-thumbnail"/>'
                    . '<div class="d-lg-none">'
                    . '<p><hr class="hline"/></p></div>'
                . '</div>'
                . '<div class="col-12 col-md-6">'
                    . '<iframe class="embed-responsive map" frameborder="0" style="border:0" '
                    . 'src="https://www.google.com/maps/embed/v1/place?q=multisala%20king&'
                    . 'key=AIzaSyDAy4kGT0cuTt1LQk3TYb-D6SkEmVHOn8Y" allowfullscreen></iframe>'
                . '</div>'
            . '</div>';
    
    echo get_document_elements()->info_page_template("Contatti", $info, TRUE);

