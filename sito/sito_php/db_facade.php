<?php

    require_once dirname(__FILE__) . '/utils.php';

    /**
     * Classe facciata per il Database. Contiene metodi per la connessione al DB
     * e per ottenere informazioni dallo stesso.
     *
     * @author Davide, Michele
     */
    class DBFacade
    {
        private $db_host;
        private $db_name;
        private $db_user;
        private $db_psw;

        function __construct($db_host, $db_name, $db_user, $db_psw, $show_projections_for_days, $password_key_validity) 
        {
            $this->db_host = $db_host; 
            $this->db_name = $db_name; 
            $this->db_user = $db_user;
            $this->db_psw = $db_psw;
            $this->show_projections_for_days = $show_projections_for_days;
            $this->password_key_validity = $password_key_validity;
        }
        
        /**
         * Funzione che esegue la connessione al DB e restituisce il relativo
         * oggetto.
         * 
         * @return connessione
         */
        private function connect()
        {
            $connection = new mysqli(
                        $this->db_host,
                        $this->db_user,
                        $this->db_psw,
                        $this->db_name
                    );

            if ($connection->connect_error)
            {
                die("Connessione fallita: " . $connection->connect_error);
            }
            
            $connection->set_charset("utf8");
            
            return $connection;
        }
        
        /**
         * Funzione che permette l'aggiunta di un nuovo utente.
         * 
         * @param $username
         * @param $password
         * @param $salt Sale crittografico
         * @param $tipo
         * @param $nome
         * @param $cognome
         * @param $email
         * @param $telefono
         * @return boolean TRUE se l'aggiunta va a buon fine
         */
        public function add_user(
                
                    $username,
                    $password,
                    $salt,
                    $tipo,
                    $nome,
                    $cognome,
                    $email,
                    $telefono
                
                )
        {
            
            $query = "INSERT INTO utente (username, password, tipo, nome, cognome"
                    . ", email, telefono, salt) VALUES (?,?,?,?,?,?,?,?)";
            
            $conn = $this->connect();
            
            # Per prevenire SQL injection si prepara la query ed in un secondo
            # momento si associano i parametri alla stessa per poi eseguirla.
            $prep_query = $conn->prepare($query);
            
            $prep_query->bind_param("ssssssss", $username, $password, $tipo, 
                    $nome, $cognome, $email, $telefono, $salt);

            $prep_query->execute();
            
            if ($conn->affected_rows == 1)
            {
                $res = TRUE;
            }
            else
            {
                $res = FALSE;
            }
            
            $conn->close();
            
            return $res;
        }
        
        /**
         * Funzioe che ritorna il tipo di utente.
         * 
         * @param type $username
         * @return \Exception
         */
        function get_user_type($username)
        {
            $query = "SELECT tipo FROM utente WHERE username = ?";
            
            $conn = $this->connect();
            
            # Esecuzione query
            $prep_query = $conn->prepare($query);
            $prep_query->bind_param("s", $username);
            $prep_query->execute();
            $qres = mysqli_fetch_all($prep_query->get_result(), MYSQLI_ASSOC);
            
            $conn->close(); 
            
            if(count($qres) > 0)
            {
                return $qres[0]["tipo"];
            }
            else
            {
                return new Exception("Utente non trovato!");
            }
        }
        
        /**
         * Funzione che verifica se l'utente è già presente nel db.
         * 
         * @param type $username The username
         */
        function user_present($username)
        {
            $query = "SELECT * FROM utente WHERE utente.username=?";
            
            $conn = $this->connect();
            
            # Esecuzione query
            $prep_query = $conn->prepare($query);
            $prep_query->bind_param("s", $username);
            $prep_query->execute();
            $res = mysqli_fetch_all($prep_query->get_result(), MYSQLI_ASSOC);
            
            $conn->close();
            
            return count($res)>0;
        }
        
        /**
         * Funzione che restituisce l'hash della password ed il sale relativi ad
         * un determinato username.
         * 
         * @param $username Username del quale si desidera avere hash e sale
         * @return Hash della password e sale
         * @throws Exception Nel caso in cui non esista l'utente
         */
        public function get_password_hash_and_salt ($username)
        {
            $query = "SELECT utente.password, utente.salt FROM utente WHERE utente.username=?";
            
            $conn = $this->connect();
            
            # Esecuzione query delle prenotazioni
            $prep_query = $conn->prepare($query);
            $prep_query->bind_param("s", $username);
            $prep_query->execute();
            $res = mysqli_fetch_all($prep_query->get_result(), MYSQLI_ASSOC);
            
            $conn->close();
           
            if (count($res) == 1)
            {
                return $res[0];
            }
            else
            {
                throw new Exception("Utente non trovato!");
            }
        }
        
        /**
         * Funzione che restituisce l'ID di un utente fornendo in ingresso il suo
         * username.
         * 
         * @param type $username
         * @return string ID_utente
         * @throws Exception Nel caso in cui non esista l'username
         */
        function get_ID_utente($username)
        {
            $query_username = "SELECT * FROM utente WHERE utente.username=?";
            
            $conn = $this->connect();
            
            # Esecuzione query
            $prep_query = $conn->prepare($query_username);
            $prep_query->bind_param("s", $username);
            $prep_query->execute();
            $res = mysqli_fetch_all($prep_query->get_result(), MYSQLI_ASSOC);
            
            $conn->close();
            
            if(count($res) > 0)
            {
                return $res[0]["ID"];
            }
            else
            {
                throw new Exception("Utente non trovato!");
            }
        }
        
        /**
         * Ritorna la mail di un utente
         * @param type $username Lo username dell'utente
         * @return type
         */
        function get_user_email($username)
        {
            $query = "SELECT email FROM utente WHERE username=?";

            $conn = $this->connect();

            # Esecuzione query
            $prep_query = $conn->prepare($query);
            print_r($conn->error);
            $prep_query->bind_param("s", $username);
            $prep_query->execute();
            $qres = mysqli_fetch_all($prep_query->get_result(), MYSQLI_ASSOC);
            
            $conn->close();
            
            if (count($qres) == 0)
            {
                return NULL;
            } else {
                return $qres[0]["email"];
            }
        }
        
        /**
         * Ritorna i dati di un utente
         * @param type $username Lo username dell'utente
         * @return type
         */
        function get_user_data($username)
        {
            $query = "SELECT * FROM utente WHERE username=?";

            $conn = $this->connect();

            # Esecuzione query
            $prep_query = $conn->prepare($query);
            print_r($conn->error);
            $prep_query->bind_param("s", $username);
            $prep_query->execute();
            $qres = mysqli_fetch_all($prep_query->get_result(), MYSQLI_ASSOC);
            
            $conn->close();
            
            if (count($qres) == 0)
            {
                return NULL;
            } else {
                return $qres[0];
            }
        }
        
        /**
         * Aggiunge una chiave di reset della password al db
         * @param type $key Chiave
         * @param type $username Nome dell'utente
         */
        function add_password_reset_key($key, $username)
        {
            $query = "INSERT INTO chiavi_reset_password(chiave,ID_utente) VALUES (?, ?)";

            $ID_utente = $this->get_ID_utente($username);
            
            $conn = $this->connect();
            
            # Esecuzione query
            $prep_query = $conn->prepare($query);
            $prep_query->bind_param("sd", $key, $ID_utente);
            $prep_query->execute();
            
            $conn->close();
        }
        
        /**
         * Ritorna l'username associato ad una chiave di reset della password
         * @param type $key
         * @return type
         */
        function get_password_reset_key_username($key)
        {
            $query = "SELECT * FROM chiavi_reset_password " .
                     "INNER JOIN utente ON utente.ID = chiavi_reset_password.ID_utente " .
                     "WHERE chiave = ? AND data > DATE_SUB(NOW(), INTERVAL ? HOUR) AND " .
                     "utilizzata = ?";
            
            $accepted_status = KeyStatus::NON_USED;
            
            $conn = $this->connect();

            # Esecuzione query
            $prep_query = $conn->prepare($query);
            print_r($conn->error);
            $prep_query->bind_param("sdd", $key, $this->password_key_validity, $accepted_status);
            $prep_query->execute();
            $qres = mysqli_fetch_all($prep_query->get_result(), MYSQLI_ASSOC);
            
            $conn->close();
            
            if (count($qres) == 0)
            {
                return NULL;
            }
            else
            {
                return $qres[0]["username"];
            }
        }
        
        /**
         * Segna la chiave come usata
         * @param type $key
         * @return type
         */
        function set_key_as_used ($key)
        {
            $query = "UPDATE chiavi_reset_password
                      SET utilizzata = ?
                      WHERE chiave = ?";
            
            $used_state = KeyStatus::USED;
            
            $conn = $this->connect();
            
            # Esecuzione query
            $prep_query = $conn->prepare($query);
            print_r($conn->error);
            $prep_query->bind_param("ds", $used_state, $key);
            $prep_query->execute();
            
            $conn->close();
        }
        
        /**
         * Imposta una nuova password per un utente
         * @param type $username Il nome utente
         * @param type $new_password La nuova password
         * @param type $new_salt Il nuovo sale
         */
        function set_user_password($username, $new_password, $new_salt)
        {
            $query = "UPDATE utente
                      SET password = ?, salt = ?
                      WHERE username = ?";
            
            $conn = $this->connect();
            
            # Esecuzione query
            $prep_query = $conn->prepare($query);
            print_r($conn->error);
            $prep_query->bind_param("sss", $new_password, $new_salt, $username);
            $prep_query->execute();
            
            $conn->close();
        }
        
        /**
         * Imposta una nuova password per un utente
         * @param type $username Il nome utente
         * @param type $new_password La nuova password
         * @param type $new_salt Il nuovo sale
         */
        function set_user_data($username, $new_name, $new_surname, $new_email, $new_phone)
        {
            $query = "UPDATE utente
                      SET nome = ?, cognome = ?, email=?, telefono=?
                      WHERE username = ?";
            
            $conn = $this->connect();
            
            # Esecuzione query
            $prep_query = $conn->prepare($query);
            print_r($conn->error);
            $prep_query->bind_param("sssss", $new_name, $new_surname, $new_email, $new_phone, $username);
            $prep_query->execute();
            
            $conn->close();
        }
        
        
    }


/**
 * Classe astratta che rappresenta i possibili stati dei posti in sala.
 * 
 * @author Davide, Michele
 */
abstract class SeatStatus 
{
    const NOT_PRESENT = 0;
    const FREE = 1;
    const BOOKED = 2;
    const PURCHASED = 3;
    const SELECTED = 4;
    const RESERVED = 5;
}


/**
 * Classe astratta che rappresenta i possibili stati delle prenotazioni
 * 
 * @author Davide, Michele
 */
abstract class ReservationStatus 
{
    const BOOKED = 0;
    const PURCHASED = 1;
    const RESERVED = 2;
}

/**
 * Classe astratta che rappresenta i possibili stati delle chiavi di reset delle password
 * 
 * @author Davide, Michele
 */
abstract class KeyStatus 
{
    const NON_USED = 0;
    const USED = 1;
}


abstract class UserType
{
    const BASIC = 0;
    const OPERATOR = 1;
    const ADMINISTRATOR = 2;
}

abstract class ProjectionType
{
    const NORMAL = "NORMALE";
    const TD = "3D";
    const SPECIAL = "SPECIALE";
}

abstract class CommentStatus
{
    const NOT_SOLVED = 0;
    const SOLVED = 1;
    const BOTH = 2;
}