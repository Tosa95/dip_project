<?php

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    //Load Composer's autoloader
    require_once dirname(__FILE__) . '/vendor/autoload.php';
    require_once dirname(__FILE__) . '/config.php';

function send_password_change_email($reset_url, $to)
{
    global $SMTP_SERVER, $SMTP_AUTH, $SMTP_PORT, $BOT_MAIL, $BOT_PASSWORD;
    
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = $SMTP_SERVER;                           // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $BOT_MAIL;                          // SMTP username
        $mail->Password = $BOT_PASSWORD;                      // SMTP password
        $mail->SMTPSecure = $SMTP_AUTH;                       // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $SMTP_PORT;                             // TCP port to connect to

        //Recipients
        $mail->setFrom($BOT_MAIL, 'MultisalaKingBot');
        $mail->addAddress($to);     // Add a recipient

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Password reset ' . date("d/m/Y H:i:s");
        $mail->Body    = 'Clicca <a href="' . $reset_url . '"><b>qui</b></a> per resettare la tua password!';
        $mail->AltBody = 'Per resettare la password vai qui: ' . $reset_url;

        $mail->send();
        
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
}
