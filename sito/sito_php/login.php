<?php

    require_once dirname(__FILE__) . '/config.php';
    require_once dirname(__FILE__) . '/utils.php';
    require_once dirname(__FILE__) . '/crypto.php';
    require_once dirname(__FILE__) . '/view/document_elements.php';
    require_once dirname(__FILE__) . '/factories.php';
    
    session_start();
    
    $de = get_document_elements();
    
    # Se è in corso una sessione per quell'username
    if (isset($_SESSION["username"]))
    {
        # Se non viene premuto il tasto "logout"
        if (!isset($_GET["logout"]))
        {
            # Nota: qua è impossibile arrivarci
            echo $de->info_page_template("Login", 
                    "<h1>Sei già loggato, " . $_SESSION["username"] . "!</h1>" . 
                    '<form id="login_form" class="needs-validation" action="login.php" method="get">
                        <input type="hidden" name="logout"></input> 
                        <button id="submit_button" type="submit" class="button button-primary">Log Out</button>
                    </form>');
        }
        else # Se l'utente ha premuto il tasto di logout,
        {
            # Redirecto alla home eliminando la sessione
            header('Location: index.php');
            session_destroy();
        }
    } 
    else # Se non esiste la sessione
    {
        # Se l'utente non ha cliccato sul tasto "login"
        if (!isset($_POST["username"]))
        {
            $redirect_field = "";
            
            # Se è impostato il campo redirect del GET dobbiamo inoltrarlo nella richiesta POST
            # per fare ciò lo mettiamo in un campo hidden del form
            if(isset($_GET["redirect"]))
            {
                $redirect_field = '<input type="hidden" name="redirect" value="' . $_GET["redirect"] . '"/>';
            }
            
            $form = bootstrap_column('

                    <form id="login_form" class="needs-validation" action="login.php" method="post">' .
                        $redirect_field .
                        '<div class="form-row">
                            <div class="form-group col-12">
                                <label for="username">Nome utente</label>
                                <input name="username" id="username" type="text" class="form-control" id="username" placeholder="Nome utente" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-12">
                                <label for="password">Password</label>
                                <input name="password" type="password" class="form-control" id="password" placeholder="Password" required>
                            </div>
                        </div>
                        <button id="submit_button" type="submit" class="button button-primary"><i class="fa fa-sign-in"></i> Login</button>
                        
                    </form>', "col-12");
            
            $registration = bootstrap_column('<a class="mx-2" href="registrazione.php">Non sei ancora registrato? Registrati ora!</a>', 'col-12');
            $forgotten_password = bootstrap_column('<a class="mx-2" href="password_dimenticata.php">Password dimenticata</a>', "col-12");
            
            echo $de->info_page_template("Login", bootstrap_row($form . $registration . $forgotten_password), TRUE);
        }
        else # Se l'utente ha cliccato sul tasto "login"
        {
            try
            {
                $psw_and_salt = get_db_facade()->get_password_hash_and_salt(user_input_to_db($_POST["username"]));
                $psw = $psw_and_salt["password"];
                $salt = $psw_and_salt["salt"];

                $pass_utils = get_password_utils();

                $correct = $pass_utils->check_equality($psw, $_POST["password"], $salt);
                
                # Se c'è corrispondenza tra username e password
                if ($correct)
                {
                    # Creo una nuova sessione per quell'username e redirecto alla home
                    # Lo faccio passare dalla funzione user_input_to_db in modo che 
                    # nella session mi tengo lo username come è salvato nel db
                    $_SESSION["username"] = user_input_to_db($_POST["username"]);
                    
                    # Se nel POST è impostato il campo redirect, torno a quella pagina
                    if(isset($_POST["redirect"]))
                    {
                        header('Location: ' . $_POST["redirect"]);
                    }
                    # Se non è impostato $_POST["redirect"] vado alla home
                    else
                    {
                        header('Location: index.php');
                    }
                    
                }
                else 
                {
                    echo $de->info_page_template("Login", bootstrap_flexbox("<span><h2>Nome utente o password errati</h2></span>" . '<span><a href="' . $SITE_ROOT . 'login.php"><button class="button button-primary">Torna al login</button></a></span>', "flex-column"));
                }
            }
            catch (Exception $e)
            {
                echo $de->info_page_template("Login", bootstrap_flexbox("<span><h2>Nome utente o password errati</h2></span>" . '<span><a href="' . $SITE_ROOT . 'login.php"><button class="button button-primary">Torna al login</button></a></span>', "flex-column") );
            }
        }
  
    }
