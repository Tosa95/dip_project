<?php

    require_once dirname(__FILE__) . '/document_elements.php';
    
    /**
     * Classe presenter per ProjectionList.
     * 
     * @author Davide, Michele
     */
    class ProjectionListPresenter
    {
        function __construct($dbf, $time_table_classes, $title_color, $first_column_percentage,
                $container_fluid, $image_folder, $site_root, $film_aspect_ratio) 
        {
            $this->dbf = $dbf;
            $this->time_table_classes = $time_table_classes;
            $this->title_color = $title_color;
            $this->first_column_percentage = $first_column_percentage;
            $this->container_fluid = $container_fluid;
            $this->image_folder = $image_folder;
            $this->site_root = $site_root;
            $this->film_aspect_ratio = $film_aspect_ratio;
        }

        /**
         * Funzione che rappresenta i dati (trama, attori, ecc) di un film
         * @param type $film_data Dati del film, come array associativo
         * @return type
         */
        function present_film_data($film_data)
        {
            // Dice quali dati visualizzare anche sui dispositivi piccoli
            // (inultilizzato al momento)
            $fields = array(
                "nazione" => TRUE,
                "durata" => TRUE,
                "anno" => TRUE,
                "genere" => TRUE,
                "regia" => TRUE,
                "interpreti" => FALSE,
                "trama" => FALSE
            );

            $content = array();

            foreach($fields as $field=>$show_on_small_devices)
            {   
                $content['<b style="padding-right: 10px;">' . ucfirst($field) . "</b>"] = '<p class="film-info">' . db_to_html($film_data[$field]) . '</p>';
                # Nota: ucfirst($str) rende maiuscola la prima lettera di $str
            }
            
            return present_dictionary($content, 0, 
                    new Alignement(VerticalAlign::TOP, HorizontalAlign::LEFT), 
                    new Alignement(VerticalAlign::CENTER, HorizontalAlign::LEFT));
        }
        
        /**
         * Ritorna la rappresentazione grafica delle proiezioni programmate per un certo film
         * @param type $ID_film ID del tilm
         * @return type
         */
        function get_time_table($ID_film)
        {
            $time_table_data = $this->dbf->get_time_table($ID_film);
            
            $table = array();
            
            foreach($time_table_data as $day=>$projections)
            {
                
                if (!array_key_exists($day,$table))
                {
                    $table[$day] = array();
                }
                
                foreach ($projections as $projection)
                {
                    $button = '<a class="timetable-link button" href="' . 
                            "prenotazione.php?ID_programmazione=" . $projection["ID"] . '">' . $projection["orario"] . '</a>';
                    $container = div($button, "projection-time-container");
                    
                    if (!is_null($projection["img"]))
                    {
                        $img = div('<img src="' . $this->image_folder . $projection["img"] . '" class="timetable-icon tooltipster" title="' . $projection["nome"] . '"/>', "timetable-icon-container m-0 p-0");
                        $container = div($button . $img, "projection-time-container");
                    }
                    
                    $table[$day][] = $container;
                }
            }
            
            $dict = array();
            
            foreach ($table as $day=>$times)
            {
                $dict["<b>".$day."</b>"] = present_array($times, 4);
            }
            
            return present_dictionary($dict, 20, 
                    new Alignement(VerticalAlign::CENTER, HorizontalAlign::LEFT), 
                    new Alignement(VerticalAlign::CENTER, HorizontalAlign::LEFT));
        }

        /**
         * Ritorna la grigliet Tina per visualizzare le locandine dei film in proiezione
         * @param boolean $future TRUE se si desiderano le proiezioni future,
         *                        FALSE se si desiderano quelle correnti
         * @return type
         */
        function get_projections_grid($future)
        {
            if($future)
            {
                $projections_data = $this->dbf->get_future_films();
            }
            else
            {
                $projections_data = $this->dbf->get_current_films();
            }
            
            
            $grid_data = array();
            
            foreach ($projections_data as $film_data)
            {
                
                $titolo = '<div class="grid-title">' . db_to_html($film_data["titolo"]) . '</div>';
                $genere = '<div class="grid-genre">' . db_to_html($film_data["genere"]) . '</div>';
                $trailer = '<object><a href="' . $film_data["url_trailer"] . '"><div class="trailer-button trailer-grid-upper-margin"></div></a></object>';
                
                $grid_single_data = array("info"=>$titolo.$genere,
                                          "actions"=>$trailer,
                                          "image"=>($this->image_folder . $film_data["nome_locandina"]),
                                          "link"=>  $this->site_root . (($future) ? "palinsesto_futuro.php#" : "palinsesto.php#") . $film_data["ID"]);
                
                $grid_data[] = $grid_single_data;
                
            }
            
            return fancy_grid($grid_data, $this->film_aspect_ratio);
        }
        

        /**
         * Ritorna il codice html per la visualizzazione di tutte le proiezioni
         * @param boolean $future TRUE se si desiderano le proiezioni future,
         *                        FALSE se si desiderano quelle correnti
         * @return type
         */
        function get_all_projections_rows($future)
        {
            if($future)
            {
                $projections_data = $this->dbf->get_future_films();
            }
            else
            {
                $projections_data = $this->dbf->get_current_films();
            }
            
            $rows = "";

            foreach ($projections_data as $film_data)
            {
                $rows .= $this->get_film_card($film_data, $future);
            }

            return $rows ;
        }

        /**
         * Ritorna il codice html per la visualizzazione di tutte le proiezioni
         * già dentro tutti i container necessari
         * @param boolean $future TRUE se si desiderano le proiezioni future,
         *                        FALSE se si desiderano quelle correnti
         * @return type
         */
        function projections_list($future)
        {
            
            return '<div class="' . (($this->container_fluid)?"container-fluid":"container") . '">

                        <div class="row">
                            <div class="col-12">
                                <h1 class="title">' . ($future ? "Prossimamente" : "Palinsesto") . '</h1>
                            </div>
                        </div>'

                        . $this->get_all_projections_rows($future) .

                   '</div>';
        }
        
        /**
         * Ritorna la testata delle card dei film (locandina in sfondo + overlay
         * con titolo)
         * @param type $film_data
         * @return type
         */
        function get_film_card_head($film_data)
        {
            return '<a class="anchor" name="' . $film_data["ID"] . '"></a>'.div(
                    div(div(db_to_html($film_data["titolo"]), "projection-card-title"),"projection-card-head-overlay"),
                    "projection-card-head",
                    "background-image:url(" . $this->image_folder . $film_data["nome_locandina"] . ")");
        }
        
        /**
         * Ritorna il contenuto della card del singolo film (descrizione + tabella orari)
         * @param type $film_data
         * @param boolean $future TRUE se la card è di un film futuro,
         *                        FALSE se la card è relativa ad un film corrente
         * @return type
         */
        function get_film_card_infos($film_data, $future)
        {
            return div(bootstrap_row(
                    bootstrap_column($this->present_film_data($film_data),"col-12 " . (($future) ? "" : "col-lg-7")) .
                        
                    (($future) ? "" : bootstrap_column('<div class="d-block d-lg-none" style="padding: 10px"></div><p class="suggestion">Clicca su un orario per prenotare:</p>' . $this->get_time_table($film_data["ID"]), "col-12 col-lg-5"))
       
                   ), "projection-card-infos");
            
        }
          
        /**
         * Ritorna una card di un film pronta da visualizzare
         * @param type $film_data Dati del film (array associativo)
         * @param boolean $future TRUE se la card è di un film futuro,
         *                        FALSE se la card è relativa ad un film corrente
         * @return type
         */
        function get_film_card($film_data, $future)
        {
            $trailer_button = div('<a href="' . $film_data["url_trailer"] . '">' . div("Trailer", "button button-primary trailer-button-margin", "width:auto;") . '</a>', "d-flex justify-content-center");
            return div(
                                
                                div($this->get_film_card_head($film_data) . $this->get_film_card_infos($film_data, $future)) . $trailer_button,
                                
                                "projection-card d-flex flex-column"
                   );
            

        }
        
    }
    
    

    

