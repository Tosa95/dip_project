<?php
    
    /**
     * File di utilità contentente funzioni che realizzano gli elementi base di
     * html (con bootstrap 4) in formato stringa.
     */

    require_once dirname(__FILE__) . '/../config.php';

    /**
     * Funzione che crea un <div>.
     * 
     * @param $content Contenuto del <div>
     * @param $classes Classi da applicare al <div> (messe in una stringa, separate da spazi)
     * @return string <div>
     */
    function div($content, $classes = "", $style="", $id="")
    {
        $res = '<div id="' . $id . '" class="' . $classes . '" style="' . $style . '">' . $content . '</div>';
        return $res; 
    }
    
    /**
     * Funzione che crea un <a>
     * @param type $href href dell'<a>
     * @param type $content contenuto dell'<a>
     * @return type
     */
    function a($content, $href)
    {
        return '<a href="' . $href . '">' . $content . '</a>';
    }
    
    /**
     * Funzione che crea uno <span>.
     * 
     * @param $content Contenuto dello <span>
     * @param $classes Classi da applicare allo <span>
     * @param $style Stile css da applicare allo <span>
     * @return string <span>
     */
    function span($content, $classes = "", $style = "", $id = "")
    {
        $res = '<span id="' . $id . '" class="' . $classes . '" style="' . $style . '">' . $content . '</span>';
        return $res; 
    }
    
    /**
     * Funzione che crea un <img>.
     * 
     * @param $src Source dell'<img>
     * @param $classes Classi da applicare allo <img>
     * @return string <img>
     */
    function img($src, $classes = "")
    {
        return '<img src="' . $src . '" class="' . $classes . '"/>';
    }
    
    /**
     * Funzione che crea un container (ossia un div con classe container) bootstrap.
     * 
     * @param $content Contenuto del container
     * @param $classes Classi del container (messe in una stringa, separate da spazi)
     * @param $fluid Booleano che indica se si desidera un container-fluid
     * @return string Container
     */
    function bootstrap_container($content, $classes = "", $fluid = TRUE, $style="")
    {
        if($fluid)
        {
           return div($content, $classes . " container-fluid", $style);
        }
        else
        {
           return div($content, $classes . " container", $style);
        }
            
    }
    
    /**
     * Funzione che crea una row (ossia un div con classe row) bootstrap.
     * 
     * @param $content Contenuto della row
     * @param $classes Classi della row (messe in una stringa, separate da spazi)
     * @return string Row
     */
    function bootstrap_row($content, $classes = "")
    {
        return div($content, $classes . " row");
    }
    
    /**
     * Funzione che crea una column (ossia un semplice div) bootstrap.
     * 
     * @param $content Contenuto della column
     * @param $classes Classi della column (messe in una stringa, separate da spazi)
     * @return string Column
     */
    function bootstrap_column($content, $classes = "", $style="")
    {
        return div($content, $classes, $style);
    }
    
    /**
     * Funzione che crea una card bootstrap.
     * 
     * @param $content Contenuto della row
     * @param $classes Classi della row (messe in una stringa, separate da spazi)
     * @return string Row
     */
    function generic_card($content)
    {
        global $CONTAINER_FLUID;
        
        $card = div( bootstrap_container($content, "", $CONTAINER_FLUID), "projection-card");
        
        return $card;
    }
    
    /**
     * Funzione che crea una flexbox di bootstrap.
     * 
     * @param type $content Contenuto della flexbox
     * @param type $classes Classi della flexbox
     * @return string Flexbox
     */
    function bootstrap_flexbox($content, $classes="")
    {
        return '<div class="d-flex '. $classes . '">' . $content . '</div>';
    }
    
     function bootstrap_modal ($modal_id, $modal_title, $modal_body, $modal_footer)
    {
        return '<!-- The Modal -->
                <div class="modal fade" id="' . $modal_id . '">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">' . $modal_title . '</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <!-- Modal body -->
                            <div class="modal-body">
                                ' . $modal_body . '
                            </div>

                            <!-- Modal footer -->
                            <div class="modal-footer">
                                ' . $modal_footer . '
                            </div>

                        </div>
                    </div>
                </div>';
    }
    
    function bootstrap_modal_footer_with_redirect ($href_redirect, $confirm_button_text = "Conferma")
    {
        return '<a href="' . $href_redirect . '" class="button button-danger">' . $confirm_button_text . '</a>' .
        '<button type="button" class="button button-neutral" data-dismiss="modal">Annulla</button>';
    }
    
    function circular_button_with_modal($button_type, $modal_id, $modal_title, $modal_body, $modal_footer)
    {
        $button = circular_button($button_type, 'data-toggle="modal" data-target="#' . $modal_id . '"');
        $modal = bootstrap_modal($modal_id, $modal_title, $modal_body, $modal_footer);
        return $button . $modal;
    }
    
    function bootstrap_modal_button($button_text, $button_classes, $modal_id, $modal_title, $modal_body, $modal_footer)
    {
        $modal_button = '<!-- Button to Open the Modal -->
                            <button type="button" class="button ' . $button_classes . '" data-toggle="modal" data-target="#' . $modal_id . '">
                                ' . $button_text . '
                            </button>
                            
                            <!-- The Modal -->
                            <div class="modal fade" id="' . $modal_id . '">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">' . $modal_title . '</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>

                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            ' . $modal_body . '
                                        </div>

                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            ' . $modal_footer . '
                                        </div>

                                    </div>
                                </div>
                            </div>';
        
        return $modal_button;
    }
    
    
    function bootstrap_modal_with_form($modal_id, $modal_title, $modal_body, $modal_footer, $form_id, $form_action, $form_method, $form_attributes)
    {
        return '<!-- The Modal -->
                    <div class="modal fade" id="' . $modal_id . '">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title">' . $modal_title . '</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <form id="' . $form_id .'" action="' . $form_action . '" method="' . $form_method . '"  accept-charset="UTF-8" ' . $form_attributes . '>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        ' . $modal_body . '
                                    </div>

                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        ' . $modal_footer . '
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>';
    }
    
    function bootstrap_button_for_modal($button_text, $button_classes, $modal_id)
    {
        return '<!-- Button to Open the Modal -->
                            <button type="button" class="button ' . $button_classes . '" data-toggle="modal" data-target="#' . $modal_id . '">
                                ' . $button_text . '
                            </button>';
    }
    
    function bootstrap_modal_button_with_form($button_text, $button_classes, $modal_id, $modal_title, $modal_body, $modal_footer, $form_id, $form_action, $form_method, $form_attributes)
    {
        $modal_button = bootstrap_button_for_modal($button_text, $button_classes, $modal_id);
        $modal = bootstrap_modal_with_form($modal_id, $modal_title, $modal_body, $modal_footer, $form_id, $form_action, $form_method, $form_attributes);
        
        return $modal_button . $modal;
    }
    
    function bootstrap_circular_button_for_modal($button_type, $modal_id)
    {
        return '<!-- Button to Open the Modal -->
                <button class="button button-circular button-' . $button_type . '" data-toggle="modal" data-target="#' . $modal_id . '">' . div("","button-" . $button_type . "-image") . '</button>
                ';
    }
    
    class ButtonType2
    {
        function __construct($text, $icon, $color) 
        {
            $this->text = $text;
            $this->icon = $icon;
            $this->color = $color;
        }
        
        function get_text()
        {
            return $this->text;
        }
        
        function get_icon()
        {
            return $this->icon;
        }
        
        function get_color()
        {
            return $this->color;
        }
    }
    
    abstract class ButtonTypes
    {
        const DELETE = 0;
        const PURCHASE = 1;
        const MODIFY = 2;
        const PROJECTIONS = 3;
    }
    
    $BUTTON_TYPES = array(
        ButtonTypes::DELETE=> new ButtonType2("ELIMINA", "delete.png", $DELETE_COLOR),
        ButtonTypes::PURCHASE=> new ButtonType2("ACQUISTA", "purchase.png", $PURCHASE_COLOR),
        ButtonTypes::MODIFY=> new ButtonType2("MODIFICA", "modify.png", $MODIFY_COLOR),
        ButtonTypes::PROJECTIONS=> new ButtonType2("PROIEZIONI", "projections.png", $PROJECTIONS_COLOR)
    );
    
    function bootstrap_text_icon_button_for_modal($button_type, $modal_id)
    {
        global $BUTTON_TYPES;
        $bttype = $BUTTON_TYPES[$button_type];
        
        return get_text_icon_button($bttype->get_text(), $bttype->get_icon(), $bttype->get_color(), 'data-toggle="modal" data-target="#' . $modal_id . '"');
    }
    
    function bootstrap_modal_circular_button_with_form($button_type, $modal_id, $modal_title, $modal_body, $modal_footer, $form_id, $form_action, $form_method, $form_attributes)
    {
        $modal_button = bootstrap_circular_button_for_modal($button_type, $modal_id);
        $modal = bootstrap_modal_with_form($modal_id, $modal_title, $modal_body, $modal_footer, $form_id, $form_action, $form_method, $form_attributes);
        
        return $modal_button . $modal;
    }
    
    
    function bootstrap_modal_confirm_button($button_text, $button_classes, $modal_id, $modal_title, $modal_body, $href_redirect, $confirm_button_text = "Conferma")
    {
        $modal_footer = '<a href="' . $href_redirect . '" class="btn btn-danger">' . $confirm_button_text . '</a>' .
                        '<button type="button" class="btn" data-dismiss="modal">Annulla</button>';
        
        $modal_confirm_button = bootstrap_modal_button($button_text, $button_classes, $modal_id, $modal_title, $modal_body, $modal_footer);
        
        return $modal_confirm_button;
    }
    
    function get_purchase_modal($message, $modal_id, $form_id)
    {
        global $SITE_ROOT;
        
        $ajax_page = $SITE_ROOT . "ajax/payment_ajax.php";
        
        $challenge = rand(10,10000000);
        
        $cc_input = '<div class="form-row">
                            <input name="challenge" type="hidden" id="challenge" value="' . $challenge . '"/>
                            <div class="form-group col-12"> 
                                <label for="credit_card">Carta di credito</label>
                                <input name="credit_card" type="text" class="form-control vf" id="credit_card" placeholder="Codice carta di credito" autocomplete="off" required>
                                <div class="alert alert-danger d-none vf-alert-credit_card vf-validation-alert">

                                </div>
                            </div>

                            <div class="form-group col-12 alert alert-danger d-none alert-msg">
                                <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
                            </div>

                        </div>';
                
        
        $body =  div($message, "mb-3") . $cc_input;
        
        $footer = '<button type="submit" class="button button-danger vf-submit">Acquista</button>'
                  . '<button type="button" class="btn" data-dismiss="modal">Annulla</button>';
        
        return bootstrap_modal_with_form($modal_id, "Conferma acquisto", $body, 
                                         $footer, $form_id, $ajax_page,
                                        "post", 'enctype="multipart/form-data"');
    }
    
    /**
    * Funzione che crea una tabella a partire da una matrice di dati.
    * 
    * @param $data Matrice dei dati rappresentante gli elementi html 
    *      da inserire all'interno delle celle della tabella. La matrice può avere 
    *      righe di lunghezza diversa. In tal caso tutte le righe saranno paddate 
    *      alla dimensione di quella più lunga.
    * @param $first_col_percentage Percentuale di spazio orizzontale occupato dalla 
    *      prima colonna. Se NULL si da lo stesso spazio a tutte le colonne, altrimenti 
    *      lo spazio dopo la prima colonna viene ripartito uniformemente fra le 
    *      altre colonne.
    * @param $aspect_ratio Rapporto width/height che deve essere mantenuto anche 
    *      in caso di resize.
    * @param $center_text TRUE: testo centrato orizzontalmente all'interno delle
    *      celle, FALSE altrimenti.
    * @param $classes classi da aggiungere alla tabella (messe in una stringa, separate da spazi)
    * @return string Tabella
    */
    function table($data, $first_col_percentage = NULL, $aspect_ratio = NULL,
                   $center_text = TRUE, $classes = "", $row_classes = "", $columns_percentage=NULL)
    {
        $height = count($data);

        $res = "";

        for($row = 0; $row < $height; $row++)
        {
            $width = count($data[$row]);
            $row_content = "";

            $default_col_percentage = is_null($first_col_percentage) ? 100.0/$width : (100.0-$first_col_percentage)/($width-1);
    
            $style_first_col = 'width: ' . $first_col_percentage . '%; display: inline-block;';

            for($col = 0; $col < $width; $col++)
            {   
                $col_percentage = $default_col_percentage;
                if (!is_null($columns_percentage))
                {
                    $col_percentage = $columns_percentage[$col];
                } 
                elseif (!is_null($first_col_percentage) && $col==0) 
                {
                    $col_percentage = $first_col_percentage;
                }
                
                $style = 'width: ' . $col_percentage . '%; display: inline-block;';
                
                $row_content .= span($data[$row][$col], "span-centred", $style);


            }

            $res .= bootstrap_row(bootstrap_column($row_content, "col-12" . ($center_text ? " centered-text centred-vertically" : "") ), $row_classes);

        }

        return bootstrap_container($res, $classes);

    }
    
    function table2 ($data)
    {
        $content = "";
        
        foreach ($data as $row_data)
        {
            $row_content = "";
            $width = count($row_data);
    
            foreach ($row_data as $cell_data)
            {
                $row_content .= div($cell_data,"flex-cell");
            }
            
            $content .= div($row_content, "flex-row");
        }
        
        return div($content, "flex-table");
    }
    
    
    function fancy_grid($data, $aspect_ratio = 1)
    {
        $content = "";
        foreach ($data as $single_data)
        {
            $brief_info = '<div class="grid-infos">' . $single_data["info"] . '</div>';
            $actions = '<div class="grid-actions">' . $single_data["actions"] . '</div>';
            
            $overlay = '<a href="' . $single_data["link"] . '"><div class="grid-overlay">' . $actions  . $brief_info . '</div></a>';
            
            $aspect_ratio_hidden = '<span class="aspect-ratio">' . $aspect_ratio . "</span>";
            $content.= bootstrap_column(div($overlay.$aspect_ratio_hidden,"grid-image","background-image:url(" . $single_data["image"] . ");"), "col-sm-6 col-md-4 col-lg-3");
        }

        return bootstrap_container(bootstrap_row($content, "no-gutters"), "no-gutters", FALSE);
    }
    
    abstract class VerticalAlign
    {
        const TOP = "top";
        const CENTER = "middle";
        const BOTTOM = "bottom";
    }
    
    abstract class HorizontalAlign
    {
        const LEFT = "left";
        const CENTER = "center";
        const RIGHT = "right";
    }
    
    class Alignement
    {
        private $vertical;
        private $horizontal;
        
        function __construct($vertical, $horizontal)
        {
            $this->vertical = $vertical;
            $this->horizontal = $horizontal;
        }
        
        function get_vertical_alignement()
        {
            return $this->vertical;
        }
        
        function get_horizontal_alignement()
        {
            return $this->horizontal;
        }
    }
    
    function present_dictionary($dictionary, $key_percentage, $key_alignement, $value_alignement)
    {
        if ($key_percentage > 0)
        {
        
            $value_percentage = 100.0 - $key_percentage;
        
            $key_style = 'width: ' . $key_percentage . '%; display: table-cell; vertical-align: ' . $key_alignement->get_vertical_alignement() . '; text-align: ' . $key_alignement->get_horizontal_alignement() . ';';
            $value_style = 'width: ' . $value_percentage . '%; display: table-cell; vertical-align: ' . $value_alignement->get_vertical_alignement() . '; text-align: ' . $value_alignement->get_horizontal_alignement() . ';';
        }
        else
        {
            $key_style = 'max-width: 100%; white-space: nowrap; display: table-cell; vertical-align: ' . $key_alignement->get_vertical_alignement() . '; text-align: ' . $key_alignement->get_horizontal_alignement() . ';';
            $value_style = 'display: table-cell; vertical-align: ' . $value_alignement->get_vertical_alignement() . '; text-align: ' . $value_alignement->get_horizontal_alignement() . ';';
        }
        
        $content = "";
        
        foreach ($dictionary as $key => $value)
        {
            $key_div = div($key, "table-cell-spacing", $key_style);
            $value_div = div($value, "table-cell-spacing", $value_style);
            
            $content .= div($key_div . $value_div, "", "display: table-row;");
        }
        
        if ($key_percentage > 0)
        {
            return div($content, "", "display: table; table-layout: fixed; width: 100%;");
        }
        else
        {
            return div($content, "", "display: table; width: 100%;");
        }
    }
    
    function present_array($array, $elems_per_row)
    {
        $element_percentage = 100.0 / $elems_per_row;
        
        $element_style = 'width: ' . $element_percentage . '%; display: inline-block;';
        
        $content = "";
        
        $row_div = "";
        
        $i = 0;
        foreach ($array as $element)
        {
            $element_div = div($element, "", $element_style);
            
            $row_div .= $element_div;
            
            $i += 1;
            
            if ($i == 4)
            {
                $content .= div($row_div, "array-row");
                $row_div = "";
                $i = 0;
            }
            
            
        }
        
        $content .= div($row_div, "array-row");
        
        return bootstrap_container($content);
    }
    
    abstract class ButtonType
    {
        const DELETE = "delete";
        const PURCHASE = "purchase";
        const MODIFY = "modify";
    }
    
    function circular_button($type, $additional_attributes="")
    {
        return '<button class="button button-circular button-' . $type . '" '. 
                $additional_attributes . '>' . div("","button-" . $type . "-image") . '</button>';
    }
    
    function add_button($id, $additional_attributes="")
    {
        return '<div class="button-on-top" id="' . $id . '" ' . $additional_attributes . '><div class="button button-circular-big '
            . 'd-inline-flex justify-content-center align-items-center"><div class="d-inline-block align-middle">+</div></div></div>';
    }
    
    function get_icon_button($icon, $color)
    {
        global $SITE_ROOT;
        
        $icon_path = $SITE_ROOT . "/images/vectorial/" . $icon;
        
        return '<div class="button button-circular d-inline-flex justify-content-center align-items-center" style="background-color:'.$color.';"> <img class="img-fluid" src="'. $icon_path .'"/> </div>';
    }
    
    function get_text_icon_button($text, $icon, $color, $additional_attributes='')
    {
        global $SITE_ROOT;
        
        $icon_path = $SITE_ROOT . "/images/vectorial/" . $icon;
        
        return '<div class="button button-text-icon d-inline-flex justify-content-center align-items-center" style="background-color:'.$color.';" ' . $additional_attributes . '> '
                . '<img class="img-fluid" style="height:50%;margin-left:10px;" src="' . $icon_path . '"/>'
                . '<div class="button-text-separator"></div> '
                . '<div class="button-text-container">' . $text . '</div> '
                . '</div>';
    }
    
    function js_list ($ajax_source, $columns_names, $columns_weights, $elem_per_page,
            $items_trans, $id, $pass_to_card_on, $present_head=TRUE, $present_filter=FALSE,
            $data_filter_view = "DataFilterView()")
    {
        return '<div id="' . $id . '"></div>'.
                '<script>'
                . 'var data = new AjaxListDataSource("' . $ajax_source . '");' .
                'var present_head =' . ($present_head?"true":"false") .  ';' .
                'var present_filter =' . ($present_filter?"true":"false") .  ';' .
                'var data_filter_view = new ' . $data_filter_view .';'.
                'var LP = new ListPresenter(data, ' . json_encode($columns_names) . ', ' . json_encode($columns_weights) . ', "' . $id . '",'
                . ' new FancyListPiecesPresenter(new ' . $items_trans . ', "' . $pass_to_card_on . '", data_filter_view), present_head, true, ' . $elem_per_page . ', present_filter);' .
                'LP.present();'
                . '</script>';
    }
    
    function centered_text_circular_button($text, $classes="")
    {
        return span(span($text), "button button-circular circular-button-centered-text " . $classes);
    }
    
    function centered_string_tag($text, $classes="")
    {
        return span(span($text), "button string-tag circular-button-centered-text " . $classes);
    }
    
    function inc_dec_field ($label, $initial_count, $id="")
    {
        # flex-basis a 0 serve a ridistribuire in modo dinamico tutto lo spazio
        # occupato da un oggetto anzichè ridistribuire solo lo spazio non usato
        return span(
                    span($label, "inc-dec-label", "flex-grow: 2; flex-basis: 0;") . 
                    span(
                            centered_text_circular_button("-", "button-primary inc-dec-button dec") . 
                            span($initial_count,"value","margin: 5px;") .  centered_text_circular_button("+", "button-primary inc-dec-button inc")
                    , "inc-dec-internal-container", "flex-grow: 1;  flex-basis: 0;")
                , "inc-dec-container", "", $id);
    }
    
    function number_field ($label, $number, $classes="", $id="", $tag_classes="")
    {
        return span(
                    span($label, "inc-dec-label", "flex-grow: 2;  flex-basis: 0;") . 
                    span(centered_text_circular_button($number, "constant-value value " . $tag_classes)
                    , "inc-dec-internal-container", "flex-grow: 1;  flex-basis: 0;")
                , "inc-dec-container " . $classes, "", $id);
    }
    
    function string_field ($label, $string, $classes="", $id="", $tag_classes="")
    {
        return span(
                    span($label, "inc-dec-label", "flex-grow: 2;  flex-basis: 0;") . 
                    span(centered_string_tag($string, "value " . $tag_classes)
                    , "inc-dec-internal-container", "flex-grow: 1;  flex-basis: 0;")
                , "inc-dec-container " . $classes, "", $id);
    }
    
    function js_argument ($name, $value)
    {
        return div ( htmlentities(json_encode($value)), "d-none", "", "__js_arg_" . $name);
    }
    
    function console ($text)
    {
        echo '<script>console.log(\''.$text.'\')</script>';
    }
    
    function info_button ($info_text, $dark = FALSE)
    {
        if($dark)
        {
            return '<div class="info-button-dark tooltipster" title="' . $info_text . '">' . div("?", "info-button-text-dark") . '</div>';
        }
        else
        {
            return '<div class="info-button tooltipster" title="' . $info_text . '">' . div("?", "info-button-text") . '</div>';
        }
        
    }