    <?php

    

    require_once dirname(__FILE__) . '/../config.php';
    require_once dirname(__FILE__) . '/html_basic_elements.php';
    
    /**
     * Classe di utilità contentente funzioni che realizzano gli elementi fondamentali
     * della pagina html, come head, body, footer, navbar, tabelle, etc.
     * 
     * @author Davide, Michele
     */
    class DocumentElements
    {
        
        function __construct($dbf, $bg_color, $username, $site_root, $container_fluid, 
                $images_folder, $gather_user_comments) 
        {
            $this->bg_color = $bg_color;
            $this->dbf = $dbf;
            $this->username = $username;
            $this->site_root = $site_root;
            $this->container_fluid = $container_fluid;
            $this->images_folder = $images_folder;
            $this->gather_user_comments = $gather_user_comments;
        }
        
        /**
         * Funzione che crea l'<html> della pagina.
         * 
         * @param $content Contenuto dell'<html> (ossia <head> e <body>)
         * @return string <html>
         */
        function html_document($content)
        {
            
            $html_doc = '<!DOCTYPE html>
                            <html lang="it">' 
                                . $content . 
                            '</html>';

            return $html_doc;
        }

        /**
         * Funzione che crea l'<head> della pagina html.
         * 
         * @return string <head>
         */
        function head()
        {
            $head = '<head>
                        <meta charset="utf-8">
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                        <meta name="description" content="">
                        <meta name="author" content="">

                        <title>LabelIT</title>

                        <!-- Bootstrap core CSS -->
                        <link href="' . $this->site_root . 'vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

                        <!-- Custom styles for this template -->
                        <link href="' . $this->site_root . 'css/css.php" rel="stylesheet">
                            
                        <!-- Stili per visualizzare le card dei prezzi -->
                        <link href="' . $this->site_root . 'css/price_card_css.php" rel="stylesheet">

                        <link href="' . $this->site_root . 'css/table.php" rel="stylesheet">

                        <!-- Icona -->
                        <link href="' . $this->site_root . 'logo.png" rel="icon">

                        <!-- Bootstrap core JavaScript -->
                        <script src="' . $this->site_root . 'js/moment.js"></script>
                        <script src="' . $this->site_root . 'js/moment-with-locales.js"></script>
                        <script src="' . $this->site_root . 'vendor/jquery/jquery.min.js"></script>
                        <script src="' . $this->site_root . 'vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
                        <script src="' . $this->site_root . 'js/basic_elements.php"></script>
                        <script src="' . $this->site_root . 'js/tables.php"></script>
                        <script src="' . $this->site_root . 'js/tooltipster_enabler.php"></script>'
                        .'<script src="' . $this->site_root . 'js/general_validation/general_validation.php"></script>'
                        .'<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
                        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
                        crossorigin="anonymous"></script>
                        <script src="https://cdn.jsdelivr.net/gh/jquery-form/form@4.2.2/dist/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>
                        
                        <!-- Tooltipster -->
                        <link rel="stylesheet" type="text/css" href="' . $this->site_root . 'js/tooltipster/dist/css/tooltipster.bundle.css" />
                        <link rel="stylesheet" type="text/css" href="' . $this->site_root . 'js/tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-punk.min.css" />
                        <script type="text/javascript" src="' . $this->site_root . 'js/tooltipster/dist/js/tooltipster.bundle.js"></script>    
                        
                    </head>';

            return $head;
        }

        /**
         * Funzione che crea il <body> della pagina html.
         * 
         * @param $content Contenuto del <body>
         * @return string <body>
         */
        function body($content)
        {
            global $BG_COLOR;

            return '<body>'.$content.'</body>';

        }
        
        /**
         * Funzione che crea il bottone utente da aggiungere alla navbar, che mostra
         * l'utente loggato.
         * 
         * @return Bottone utente
         */
        function get_user_navbar_item()
        {
              
            return '<li class="nav-item dropdown user-button d-none d-lg-inline-block">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span><img class="user-img" src="' . $this->site_root . 'images/vectorial/user.png">
                            <strong>'. $this->username .'</strong></span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="' . $this->site_root . 'user/mie_proiezioni.php">Le mie proiezioni</a>
                        <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="' . $this->site_root . 'user/modifica_dati.php">Modifica dati</a>
                            <a class="dropdown-item" href="' . $this->site_root . 'login.php?logout">Logout</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown d-inline-block d-lg-none">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <strong>'. $this->username .'</strong></span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="' . $this->site_root . 'user/mie_proiezioni.php">Le mie proiezioni</a>
                        <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="' . $this->site_root . 'user/modifica_dati.php">Modifica dati</a>
                            <a class="dropdown-item" href="' . $this->site_root . 'login.php?logout">Logout</a>
                        </div>
                    </li>';
        }
        
        
        function get_admin_navbar_items()
        {
            return '<li class="nav-item dropdown admin-button d-none d-lg-inline-block">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span><img class="admin-img" src="' . $this->site_root . 'images/vectorial/admin.png">
                            <strong>Admin</strong></span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="' . $this->site_root . 'admin/film.php">Gestione Film</a>
                            <a class="dropdown-item" href="' . $this->site_root . 'admin/eventi.php">Gestione Eventi</a>
                            <a class="dropdown-item" href="' . $this->site_root . 'admin/commenti.php">Commenti</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown d-inline-block d-lg-none">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <strong>Admin</strong></span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="' . $this->site_root . 'admin/film.php">Gestione Film</a>
                            <a class="dropdown-item" href="' . $this->site_root . 'admin/eventi.php">Gestione Eventi</a>
                            <a class="dropdown-item" href="' . $this->site_root . 'admin/commenti.php">Commenti</a>
                        </div>
                    </li>';
    
        }   
        
        /**
         * Funzione che crea la <nav> della pagina html.
         * 
         * @return string <nav>
         */
        function navbar()
        {
            global $MAIN_ENV;
            return '<!-- Navigation -->
                    <nav class="navbar fixed-top navbar-expand-lg navbar-dark deep-bg fixed-top">
                        <div class="container-fluid  standard-borders">
                        

                            <img src="' . $this->images_folder . 'vectorial/logo.png" class="img-fluid navbar-logo"/>
                            <a class="navbar-brand" href="' . $this->site_root . 'index.php">LabelIT' . ($MAIN_ENV?"":"[TEST]") . '</a>
                            
                            <button class="navbar-toggler navbar-toggler-right" type="button" 
                                    data-toggle="collapse" data-target="#navbarResponsive" 
                                    aria-controls="navbarResponsive" aria-expanded="false" 
                                    aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
                                
                                    <li class="nav-item">
                                        <a class="nav-link" href="' . $this->site_root . 'classifier.php">Classifier</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="' . $this->site_root . 'classifier2.php">Classifier2</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="' . $this->site_root . 'viewer.php">Viewer</a>
                                    </li>' .
                                    //((isset($this->username) && $this->dbf->get_user_type($this->username) == UserType::ADMINISTRATOR) ? $this->get_admin_navbar_items() : '') .
                                    (isset($this->username) ? $this->get_user_navbar_item() : '<li class="nav-item">
                                        <a class="nav-link" href="' . $this->site_root . 'login.php">Login</a>
                                    </li>').
                                '</ul>
                            </div>
                        </div>
                    </nav>';
        }

        /**
         * Funzione che crea il <footer> della pagina html.
         * 
         * @return string <footer>
         */
        function footer()
        {
            $res = '<!-- Footer -->';
            
            if ($this->gather_user_comments)
            {
                $ajax_page = $this->site_root . "ajax/user_comments_ajax.php";
                $res .= '<div class="container"><form id="_user_comment_form" class="needs-validation" action="' . $ajax_page . '" method="post">
                            <input type="hidden" name="action" value="add"/>
                            <input type="hidden" name="page" value="'. $_SERVER['PHP_SELF'] .'"/>'.
                            (isset($this->username)?'<input type="hidden" name="username" value="'. $this->username .'"/>':"").
                            '<div class="form-row">
                                <div class="form-group col-md-10">
                                    <label for="comment">Commento</label>
                                    <textarea name="comment" id="comment" type="text" class="form-control vf" id="username" placeholder="Commento riguardante l\'usabilità del sistema" autocomplete="off" required></textarea>
                                    <div class="alert alert-danger d-none vf-alert-comment vf-validation-alert">

                                    </div>
                                </div>
                                <div class="form-group col-md-2 d-flex justify-content-center">
                                    <button name="add" id="submit_button" type="submit" class="button button-primary vf-submit flex-grow-1">INVIA</button>
                                </div>
                            </div>
                            <div class="form-group col-12 alert alert-danger d-none alert-msg vf-validation-alert">
                                    
                            </div>
                        </form></div>';
            }
            
            $res .= '<footer class="py-5 deep-bg">
                        <div class="container">
                            <p class="m-0 text-center text-white">Copyright &copy; DM for Multisala King 2018</p>
                            <span class="d-none d-lg-block" style="color:rgb(17,17,17);">aaaaaaaaaaaaaPerEssereUnVeroSitoDelKingaaaaaaaaaaaaaaaaaaaaa</span>
                        </div>
                    </footer>';
            
            return $res;
        }


        /**
         * Fuzione che crea gli indicatori sottostanti alle immagini del carosello.
         * Tali indicatori fungono anche da barra di navigazione tra gli elementi
         * del carosello stesso.
         * 
         * @param $carousel_data Dati relativi al carosello (array associativo img_name=>img_src)
         * @return string Indicatori
         */
        function get_carousel_indicators($carousel_data)
        {
            $i = 0;
            $res = "";

            foreach ($carousel_data as $img_name=>$img_src)
            { 
                if ($i == 0)
                {
                    # Il primo elemento del carosello è quello attivo di default
                    $res .= '<li data-target="#carousel" data-slide-to="0" class="active"></li>';
                } 
                else 
                {
                    $res .= '<li data-target="#carousel" data-slide-to="' . $i . '"></li>';
                }

                $i++;
            }
            return $res;
        }

        /**
         * Fuzione che crea gli oggetti del carosello.
         * 
         * @param $events Dati relativi al carosello (array associativo img_name=>img_src)
         * @return string Oggetti del carosello
         */
        function get_carousel_items($events)
        {
            $res = "";
            $i = 0;
            foreach ($events as $event)
            {
                if ($i == 0)
                {
                    $res .= '<div class="carousel-item active" style="background-image: url(\''
                            . "images/" . $event["immagine"] .
                            '\')">'
                            . '<div class="carousel-caption carousel-text-shadow">
                                <h3>' . db_to_html($event["nome"]) . '</h3>
                                <p>' . db_to_html($event["descrizione"]) . '</p>
                              </div>'
                            . '</div>';
//                    $res .= '<div class="carousel-item active">' .
//                            '<img src="images/' . $img_name . '" class="img-fluid"/></div>';
                } 
                else 
                {
                    $res .= '<div class="carousel-item" style="background-image: url(\''
                            . "images/" . $event["immagine"] .
                            '\')">'
                            . '<div class="carousel-caption carousel-text-shadow">
                                <h3>' . db_to_html($event["nome"]) . '</h3>
                                <p>' . db_to_html($event["descrizione"]) . '</p>
                              </div>'
                            . '</div>';
//                    $res .= '<div class="carousel-item">' .
//                            '<img src="images/' . $img_name . '" class="img-fluid"/></div>';
                }

                $i++;
            }
            return $res;
        }

        /**
         * Fuzione che crea un carosello.
         * 
         * @param $carousel_data Dati relativi al carosello (array associativo img_name=>img_src)
         * @return string Carosello
         */
        function carousel()
        { 
            $carousel_data = $this->dbf->get_current_events();
            return '<header>
                        <div id="carousel" class="carousel slide" data-ride="carousel">

                            <!-- dots under carousel -->
                            <ol class="carousel-indicators">'
                                . $this->get_carousel_indicators($carousel_data) .
                            '</ol>

                            <!-- slides -->
                            <div class="carousel-inner" role="listbox">'
                                . $this->get_carousel_items($carousel_data) .
                            '</div>

                            <!-- Left and right controls -->
                            <a class="carousel-control-prev" href="#carousel" data-slide="prev">
                              <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a class="carousel-control-next" href="#carousel" data-slide="next">
                              <span class="carousel-control-next-icon"></span>
                            </a>
                        </div>
                  </header>';
            
//                            <!-- left/right controls -->
//                            <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
//                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
//                                <span class="sr-only">Previous</span>
//                            </a>
//                            <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
//                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
//                                <span class="sr-only">Next</span>
//                            </a>
        }
        
        /**
         * Funzione che crea un bottoncino "Torna alla Home".
         * 
         * @return type
         */
        function return_home_button()
        {
            $home = $this->site_root . "index.php";
            
            return bootstrap_flexbox(
                        a(div("Torna alla Home", "button button-primary button-return-home"), $home), 
                    "justify-content-center");
        }
        
        /**
         * Funzione che crea una pagina html basilare, con navbar, carosello (se desiderato),
         * bottone per tornare alla home (se desiderato) e footer.
         * 
         * @param $content Contenuto della pagina html
         * @param $draw_carousel TRUE se si vuole disegnare il carosello
         * @param $draw_return_home_button TRUE se si vuole disegnare il bottone per tornare alla home
         * @return string Pagina html
         */
        function basic_page_template($content, $draw_carousel = FALSE, $draw_return_home_button = FALSE)
        {
            return $this->html_document(
                        $this->head() .
                        $this->body(
                            $this->navbar() .
                            ($draw_carousel ? $this->carousel() : "") .
                            $content .
                            ($draw_return_home_button ? $this->return_home_button() : "") .
                            $this->footer()
                        )
                    );
        }

        /**
         * Funzione che crea una pagina html di informazione.
         * 
         * @param $title Titolo della pagina
         * @param $content Contenuto della pagina html
         * @param $draw_return_home_button TRUE se si vuole disegnare il bottone per tornare alla home
         * @return string Pagina html
         */
        function info_page_template($title, $content, $draw_return_home_button = FALSE)
        {
            $title_par = '<p class="title">' . $title . '</p>';

            # La pagina deve avere il titolo e deve incapsulare il contenuto in una card
            $title_and_content = bootstrap_container($title_par . generic_card($content), "", $this->container_fluid);

            return $this->basic_page_template($title_and_content, FALSE, $draw_return_home_button);
        }
    }
    
    