<?php

    require_once dirname(__FILE__) . '/html_basic_elements.php';
    require_once dirname(__FILE__) . '/../utils.php';
    
    /**
     * Classe presenter per le prenotazioni.
     *
     * @author Davide, Michele
     */
    class ReservationsPresenter 
    {

        function __construct($dbf, $site_root) 
        {
            $this->dbf = $dbf;
            $this->site_root = $site_root;
        }
        
        /**
         * Funzione che crea la griglia della sala (con posti acquistati, riservati, 
         * prenotati,liberi) per una determinata programmazione.
         * 
         * @param $ID_programmazione
         * @return string Griglia della sala
         */
        function get_room_table($ID_programmazione)
        {
            $original_table = $this->dbf->get_projection_room_status($ID_programmazione);
            $table_hidden = js_argument("seat_table", $original_table);
            //console(json_encode($table_hidden));
            $table = array();
            

            for ($row = 0; $row < count($original_table); $row++)
            {
                $new_row = array();
                for ($col = 0; $col < count($original_table[$row]); $col++)
                {
                    $id = $row . "x" . $col;
                    
                    if ($original_table[$row][$col] == SeatStatus::NOT_PRESENT)
                    {
                        # Aggiungo nella riga uno spazio vuoto
                        $new_row[] = '';
                    } 
                    else if ($original_table[$row][$col] == SeatStatus::FREE)
                    {
                        # Aggiungo nella riga un posto libero (rappresentato dalla relativa immagine), assegnandogli l'id creato
                        $new_row[] = '<img class="img-fluid seat" id="'. $id .'" src="images/vectorial/free_place.png">';
                    }
                    else if ($original_table[$row][$col] == SeatStatus::BOOKED)
                    {
                        $new_row[] = '<img class="img-fluid seat" id="'. $id .'" src="images/vectorial/booked_place.png">';
                    }
                    else if ($original_table[$row][$col] == SeatStatus::PURCHASED)
                    {
                        $new_row[] = '<img class="img-fluid seat" id="'. $id .'" src="images/vectorial/purchased_place.png">';
                    }
                    else if ($original_table[$row][$col] == SeatStatus::SELECTED)
                    {
                        $new_row[] = '<img class="img-fluid seat" id="'. $id .'" src="images/vectorial/selected_place.png">';
                    }
                    else if ($original_table[$row][$col] == SeatStatus::RESERVED)
                    {
                        $new_row[] = '<img class="img-fluid seat" id="'. $id .'" src="images/vectorial/reserved_place.png">';
                    }

                }
                
                $table[] = $new_row;
            }

            $res = table2($table);

            # Aggiungo l'immagine dello schermo sotto quella dei posti
            $res .= bootstrap_flexbox('<img class="screen" src="images/vectorial/screen.png"/>', "justify-content-center");

            $res .= $table_hidden;
            //print_r($table_hidden);
            
            $res .= '<script src="' . $this->site_root . "js/reservations_js.php" . '" type="text/javascript"></script>';

            return $res;
        }
        
        /**
         * Funzione che crea l'intestazione per la pagina di prenotazione relativa
         * ad una determinata proiezione.
         * 
         * @param type $ID_programmazione
         * @param type $foot_message Messaggio da aggiungere sotto l'intestazione
         * @return type
         */
        function get_reservation_heading($ID_programmazione, $foot_message)
        {
            $projection_info = $this->dbf->get_projection_info($ID_programmazione);
            
            $prices_info = $this->dbf->get_prices_info_from_ID_programmazione($ID_programmazione);
            
            $info_label = div(span($prices_info[0]["nome_fascia"], "mx-2") . info_button($prices_info[0]["descrizione_fascia"], TRUE), "info-range-label");
            
            
            return bootstrap_container(

                    bootstrap_row(

                    bootstrap_flexbox(

                            div('<h2>'. $projection_info["nome_sala"] . "</h2>", "card-title") . 
                            div("<h5>" . $projection_info["nome_giorno"] . " " . $projection_info["num_giorno"] . "/" . $projection_info["num_mese"] . " " . $projection_info["orario"] . ", <b>" . $info_label . "</b></h5>", "").
                            div("<h6>ATTENZIONE: ricorda che per poter utilizzare la propria prenotazione è necessario arrivare alle casse 30 minuti prima dell'orario di inizio della proiezione</h6>", "").
                            div('<span class="align-text-bottom">' . $foot_message . '</span>')

                    , "col-12 col-sm-7 col-md-9 flex-column justify-content-between").

                        bootstrap_container('<img src="images/' . $projection_info["nome_locandina"] . '" class="img-fluid float-right img-thumbnail film-image-reservations">', "d-none d-sm-block col-sm-5 col-md-3")                        
                    )   
                , "reservations-info") . '<div class="spacer"></div>';
        }
        
        function get_reservation_prices_selectors($ID_programmazione)
        {
            $prices = $this->dbf->get_prices_from_projection($ID_programmazione);
            $prices_info = $this->dbf->get_prices_info_from_ID_programmazione($ID_programmazione);
            
            $prices_hidden = js_argument("prices", $prices);
            
            $result = "";
            
            $index = 0;
            
            foreach ($prices_info as $price)
            {
                $label = bootstrap_flexbox(info_button($price["descrizione_prezzo"]) . span($price["nome_prezzo"] . " " . number_format($price["valore"],2) . " €", "mx-2"), "justify-content-center align-items-center");
                $result .= bootstrap_column(inc_dec_field($label, 0, "seat-type-" . $index), "col-12 col-md-6 col-lg-4 d-flex", "text-align: center;");
                
                $index++;                
            }
            
            $non_assigned_seats = bootstrap_row(bootstrap_column(number_field("NON ASSEGNATI", 0, "", "non-assigned"),"col-12 d-flex"), "mx-auto justify-content-center row stay-narrow counter-spacing");
            $total = bootstrap_row(bootstrap_column(number_field("TOTALE", "0.00€", "", "total", "purchased-tag-color"),"col-12 d-flex"), "mx-auto justify-content-center row stay-narrow counter-spacing");
            
            $info_label_class = "margin-top: 20px; margin-bottom: 10px; font-size: 1.4em;text-align: justify; text-justify: inter-word;";
            $select_label = div("Seleziona tipologia dei posti:","",$info_label_class);
            
            $disclaimer = div("RICORDA: non mentire sulla tipologia di posti assegnati in quanto verrà controllata"
                    . " in loco l'applicabilità delle varie tariffe e, in caso non venga rispettata, farai perdere tempo"
                    . " sia a noi che ai tuoi amici! <br> Grazie per la collaborazione.","",$info_label_class);
            
            return  $prices_hidden . $select_label . bootstrap_row($result, "mx-auto justify-content-center") . $non_assigned_seats . $total . $disclaimer;
        }

    }
