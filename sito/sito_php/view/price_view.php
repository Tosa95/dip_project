<?php

    require_once dirname(__FILE__) . '/../model/prices.php';
    
    
    function fancy_euro ($price)
    {
        return str_replace("€", '<span style="color:rgb(188, 75, 0);">€</span>', $price); 
    }
    
    function wrap_price($price, $subprice=TRUE)
    {
        $name_class = $subprice?"price-card-subprice-name":"price-card-main-price-name";
        $price_class = $subprice?"price-card-subprice":"price-card-main-price";
        $displacement = $subprice?"-5.5px":"-8px";
        $fancy_price = fancy_euro($price->get_price());
        $name_div = div(span($price->get_name()) . "  " . moved_info_button($price->get_description(), $displacement, TRUE), $name_class, "text-align: center;");
        
        return div(span($name_div . $fancy_price) , $price_class);
    }
    
    function get_prices_displayable_array($prices, $alone_align = HorizontalAlign::LEFT)
    {
        $arr = array();
        foreach($prices as $price)
        {
            $arr[] = wrap_price($price);
        }
        return div(present_array($arr, 2),"","text-align:" . $alone_align . "; display:table-cell; padding-bottom: 20px");
    }
    
    function moved_info_button($text, $top_displacement, $dark=FALSE)
    {
        return span(info_button($text, $dark), "", "position:relative;top:" . $top_displacement . ";");
    }
    
    function price_card($price_range, $alone_align)
    {
        $prices = $price_range->get_prices();
        $day_name = $price_range->get_day_name();
        $range_name = $price_range->get_range_name();
        $range_descritpion = $price_range->get_description();
        $main_price = $prices[0];
        unset($prices[0]);
        
        if (!is_null($day_name))
        {
            $head_content = div(div($day_name, "price-card-day-name") . div("-- " . $range_name . "  " . moved_info_button($range_descritpion, "-7px") . " --", "price-card-promotion-name"));
        }
        else
        {
            $head_content =  div(div($range_name . "  " . moved_info_button($range_descritpion, "-9px")), "price-card-day-name") ;
        }
        
        $main_price = wrap_price($main_price, FALSE);
        $prices = get_prices_displayable_array($prices, $alone_align);
        
        return div(bootstrap_flexbox(div($head_content, "price-card-head") . $main_price . $prices, "flex-column"), "price-card");
    }
    
    function present_price_ranges($ranges, $alone_align)
    {
        $column_classes = "col-xl-4 col-md-6 col-sm-12";
        
        $content = "";
        
        foreach ($ranges as $range)
        {
            $content .= div(price_card($range, $alone_align), $column_classes);
        }
        
        return bootstrap_row($content, "no-gutters");
    }

