<?php

    require_once dirname(__FILE__). '/factories.php';
    require_once dirname(__FILE__). '/config.php';
    
    session_start();
    
    if (isset($_SESSION["username"]))
    {
        $dbf = get_db_facade();
        
        $utype = $dbf->get_user_type($_SESSION["username"]);
        
        $de = get_document_elements();
        
        if ($utype >= 2)
        {
            
            $tiles .= '<div class="home-tile d-flex align-items-center flex-column justify-content-center">' .    
                      '<div class="tile-title mb-4">Classifier</div>' .
                      '<div>A single image per time will be proposed to you. Navigating through the traffic sign hierarchy you have to select the right sign and then confirm.</div>' .
                      '<a href="' . $SITE_ROOT . 'classifier.php"><div id="next_button" class="button button-primary d-inline-block mt-5">CLASSIFIER</div></a>'.
                      '</div>';
            
            $tiles .= '<div class="home-tile d-flex align-items-center flex-column justify-content-center">' .    
                      '<div class="tile-title mb-4">Classifier 2</div>' .
                      '<div>A long list of images will be proposed to you. For any of them you have to decide if it is one of the proposed sign, a sign that is not proposed (OTHER) or not a sign at all (NO SIGN). The ones classified as OTHER will be enqueued for the classification throught the classifier mode. Suggestions are provided by a neural network model.</div>' .
                      '<a href="' . $SITE_ROOT . 'classifier2.php"><div id="next_button" class="button button-primary d-inline-block mt-5">CLASSIFIER 2</div></a>'.
                      '</div>';
            
            $tiles .= '<div class="home-tile d-flex align-items-center flex-column justify-content-center">' .    
                      '<div class="tile-title mb-4">Viewer</div>' .
                      '<div>View the result of the classification. You can navigare through the sign hierarchy and visualize all the images classified as a specific label. You can also reject some wrong classifications and view the predictions for each category.</div>' .
                      '<a href="' . $SITE_ROOT . 'viewer.php"><div id="next_button" class="button button-primary d-inline-block mt-5">VIEWER</div></a>'.
                      '</div>';
            
            $content = '<div class="d-flex flex-wrap align-items-center justify-content-center">' . $tiles . '</div>';



            echo $de->info_page_template("Home", $content . $script, TRUE);
        }
        else
        {
            echo $de->info_page_template("Home", "Non hai i privilegi per classificare le immagini", TRUE);
        }
    }
    else
    {
        header('Location: login.php');
    }

