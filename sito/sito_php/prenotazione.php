<?php

    require_once dirname(__FILE__) . '/factories.php';
    require_once dirname(__FILE__) . '/view/html_basic_elements.php';

    session_start();
    
    $dbf = get_db_facade();
    $rp = get_reservations_presenter();
    $de = get_document_elements();
    
    if(isset($_SESSION["username"]))
    {
        # Se si invoca lo script perchè l'utente ha cliccato sull'orario di una 
        # programmazione per prenotare
        if (isset($_GET["ID_programmazione"]))
        {
            select_seats($dbf, $rp, $de);
        }
        # Se si invoca lo script perchè l'utente ha cliccato su "Prenota" o "Acquista"
        # sotto la rappresentazione della sala
        else if (isset($_POST["action"])) 
        {
            do_action_and_display_results($dbf, $rp, $de, $_POST["action"]);
        }
        else
        {
            print_r($_POST);
        }
        
    }
    else # Se l'utente non è loggato
    {
        login($dbf, $rp, $de);
    }
    
    /**
     * Visualizzazione della sala per la selezione dei posti
     * @param type $dbf 
     * @param type $rp
     * @param type $de
     */
    function select_seats($dbf, $rp, $de)
    {
        $ID_programmazione = $_GET["ID_programmazione"];

        $projection_info = $dbf->get_projection_info($ID_programmazione);
        $page_content = $rp->get_reservation_heading($ID_programmazione, "<h2>Seleziona i posti desiderati:<h2>");
        
        $reserve_modal_body = "Sei sicuro di voler prenotare i posti selezionati?";
        $reserve_modal_footer = '<button type="submit" name="action" value="reserve" class="button button-danger">Conferma prenotazione</button>' .
                                '<button type="button" class="button button-neutral" data-dismiss="modal">Annulla</button>';
        $reserve_modal = bootstrap_modal("conferma-prenotazione-sala", "Conferma prenotazione", $reserve_modal_body, $reserve_modal_footer);
        
       
        $purchase_modal = get_purchase_modal("Se sei sicuro di voler acquistare i posti selezionati, inserisci il numero della tua carta di credito e clicca su acquista.", "conferma-acquisto-sala", "form-conferma-acquisto");
        
        $form_content = '<form id="reservation_form" class="needs-validation" action="prenotazione.php" method="post">
                            <input type="hidden" name="selected_matrix" id="selected_matrix"/>
                            <input type="hidden" name="ID_programmazione" value="' . $ID_programmazione . '"/>
                            <input type="hidden" name="prices" id="prices"/>
                            <button type="button" id="submit_reserve_button" class="button button-primary" data-toggle="modal" data-target="#conferma-prenotazione-sala"><h3>Prenota</h3></button>
                                ' . $reserve_modal . '     
                            <button type="button" id="submit_purchase_button" class="button button-purchase" data-toggle="modal" data-target="#conferma-acquisto-sala"><h3>Acquista</h3></button>
                               
                        </form>' . $purchase_modal;
        
        $form = bootstrap_flexbox($form_content, "justify-content-center reservation-form");
        $alert = '<div id="data-form-msg" class="alert alert-danger d-none vf-validation-alert"></div>';
        
        $page_content .= $rp->get_room_table($ID_programmazione) . $rp->get_reservation_prices_selectors($ID_programmazione) .  $form . $alert;

        echo $de->info_page_template(db_to_html($projection_info["titolo"]),  $page_content);
    }
    
    function get_total ($prices, $prices_from_db)
    {
        $sum = 0;
        
        for ($i = 0; $i < count($prices_from_db); $i++)
        {
            $sum += $prices_from_db[$i]["valore"] * $prices[$i]["num"];
        }
        return $sum;
    }
    
    /**
     * Visualizza i risultati di una prenotazione
     * @param type $dbf
     * @param type $rp
     * @param type $de
     * @param type $action azione da fare ("purchase" o "reserve")
     */
    function do_action_and_display_results($dbf, $rp, $de, $action)
    {
        $ID_programmazione = $_POST["ID_programmazione"];

            
        $projection_info = $dbf->get_projection_info($ID_programmazione);
        $page_content = $rp->get_reservation_heading($ID_programmazione, "");

        #Matrice dei posti
        $selected_matrix = json_decode($_POST["selected_matrix"], true);
        #Array contenente i prezzi selezionati per i vari posti
        $prices = json_decode($_POST["prices"], true);
        #Array contenente i prezzi presi dal database
        $prices_from_db = $dbf->get_prices_from_projection($ID_programmazione);
        
        # Nota: json_decode trasforma un JSON in un oggetto php 
        # (il true serve a decodificare come array associativi anzichè oggetti)

        # Inizializzo un array di array associativi destinato a tenere il conto
        # del numero di posti ancora da inserire per ogni fascia di prezzo
        $price_ranges_counters = array();
         
        for ($i = 0; $i < count($prices_from_db); $i++)
        {
            $counter = array();
            $counter["nome_prezzo"] = $prices_from_db[$i]["nome_prezzo"];
            $counter["residui"] = $prices[$i]["num"];
            $price_ranges_counters[] = $counter;
        }
        
        
        $seats = array();

        for ($row = 0; $row < count($selected_matrix); $row++)
        {
            for ($col = 0; $col < count($selected_matrix[$row]); $col++)
            {
                if ($selected_matrix[$row][$col] == SeatStatus::SELECTED)
                {
                    $seat = array();
                    $seat["nome_posto"] = $dbf->get_seat_name($ID_programmazione, $row, $col);
                    
                    # Devo definire che prezzo associare al posto. A tale scopo scelgo una
                    # fascia di prezzo il cui contatore dei posti residui (ancora
                    # da assegnare per quella fascia di prezzo) sia > 0. 
                    
                    $search_next = TRUE;
                    
                    for($i = 0; $i < count($price_ranges_counters); $i++)
                    {
                        if($price_ranges_counters[$i]["residui"] > 0 && $search_next)
                        {
                            $seat["nome_prezzo"] = $price_ranges_counters[$i]["nome_prezzo"];
                            $price_ranges_counters[$i]["residui"]--;
                            $search_next = FALSE;
                        }
                    }
                    
                    $seats[] = $seat;
                }
            }
        }

        # Se l'utente è loggato, potrà fare la prenotazione
        # Nota. Il controllo è ridondante, serve per evitare usi scorretti del sito.
        #       Se si arriva a questo punto, l'utente dovrebbe essere necessariamente loggato.
        if(isset($_SESSION["username"]))
        {
            # Se i posti sono disponibili
            if($dbf->check_seats_available($ID_programmazione, $seats))
            {
                # Nota: Se si usa il valore dei prezzi qua prenderli da $prices_from_db e non da $prices
                # perchè $prices contiene dati che arrivano da una richiesta post e potrebbero essere stati
                # modificati dall'utente, da quell'array prendere solo il numero di posti per tipologia
                # I due array ovviamente saranno indicizzati alla pari (sono nello stesso ordine) [vedere get 
                # total per un esempio]
                
                # Aggiungo la prenotazione o l'acquisto nel database
                if($action === "reserve")
                {
                    $dbf->add_reservation($ID_programmazione, $_SESSION["username"], $seats);
                }
                else if($action === "purchase")
                {
                    $challenge = $_POST["challenge"];
                    $credit_card = $_POST["credit_card"];
                    $signature = $_POST["signature"];
                    
                    $pu = get_password_utils();
                    
                    if ($pu->check_payment_signature($challenge, $credit_card, $signature))
                    {
                        $dbf->add_purchase($ID_programmazione, $_SESSION["username"], $seats);
                    }
                    else
                    {
                        $content = '<h3>Volevi fare il furbo!</h3>E invece il nostro servizio di pagamento è fatto bene e siamo riusciti a sgamarti!' .
                        '<input type="button" class="button button-primary" value="Vai alla home" onclick="location.href=\'index.php\'"/>';

                        echo $de->info_page_template("Attenzione",  $content, TRUE);
                        return;
                    }
                }
                
                
                $seats_names = array();
                
                foreach($seats as $seat)
                {
                    $seats_names[] = $seat["nome_posto"];
                }
                
                $seats_view = string_field("POSTI SELEZIONATI", join(", ", $seats_names), "");
                
                $total = get_total($prices, $prices_from_db);
                $formatted_total = number_format($total,2) . " €";
                
                $total_view = number_field("COSTO TOTALE", $formatted_total, "", "", "purchased-tag-color");
                $page_content .= bootstrap_row(bootstrap_column($seats_view, "col-12 col-lg-6 d-flex") . bootstrap_column($total_view, "col-12 col-lg-6 d-flex"), "d-flex");

                echo $de->info_page_template(db_to_html($projection_info["titolo"]),  $page_content, TRUE);
            }
            else
            {
                $content = '<h3>Questa prenotazione è già stata effettuata!</h3>' .
                       '<input type="button" class="button button-primary" value="Vai alla home" onclick="location.href=\'index.php\'"/>';

                echo $de->info_page_template("Attenzione",  $content, TRUE);
            }

        }
        else # Se l'utente prova a prenotare ma non è loggato
        {
            $content = '<h3>Per poter prenotare devi prima effettuare il login!</h3>' .
                       '<input type="button" class="button button-primary" value="Vai al login" onclick="location.href=\'login.php\'"/>';

            echo $de->info_page_template("Attenzione",  $content);
        }
    }
    
    
    /**
     * Visualizza il login in caso di utente non loggato
     * @param type $dbf
     * @param type $rp
     * @param type $de
     */
    function login($dbf, $rp, $de)
    {
        if (isset($_GET["ID_programmazione"]))
        {
            $ID_programmazione = $_GET["ID_programmazione"];
            
            $url = "prenotazione.php?ID_programmazione=" . $ID_programmazione;
            
            # Redirecto a login.php dicendo di tornare a questa pagina una volta
            # effettuato il login
            header('Location: login.php?redirect=' . urlencode($url));
        }
    }
    
    
    

        

