import hashlib
import random

import re


import os
import traceback

from datetime import datetime
from flask import Flask, flash, request, redirect, url_for, jsonify, send_from_directory, send_file
from werkzeug.utils import secure_filename
import json

from db_facade import get_db_connection, image_present, get_image_name, convert_row, get_label_type_tree, \
    get_total_images, get_total_labeled_images, get_total_images_with_label_value, get_image_date, \
    get_neighbors_from_date, get_neighbors_to_date, get_burst_id_by_image_id, new_burst, add_image_to_burst, \
    get_random_unlabeled_image, get_burst_labels_count, burst_has_labels, get_bursted_image_count, \
    get_average_burst_size, get_random_images_with_label, db_interaction, add_image_score, get_all_unscored_images, \
    get_image_for_human_classification, get_image_for_human_classification_efficient, get_total_interesting_images_left, \
    get_all_images, delete_image, update_image_bursts_labeled_images, get_label_images_cnt, get_label_images, \
    get_label_images_w_bursts, set_image_score, get_label_info, \
    get_label_images_predicted_w_bursts, get_label_path, delete_all_image_labels, get_labeling_queue_length, \
    add_image_to_labeling_queue, delete_image_from_labeling_queue, get_ordered_images_to_be_labeled, \
    add_image_predicted_label, add_multiple_image_predicted_labels, add_image_to_tabu, clear_tabu_list

IMAGE_UPLOAD_FOLDER = os.path.join('images', 'signals')
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}


app = Flask(__name__)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def simple_query(body):

    try:
        return db_interaction(body)
    except Exception as ex:
        return jsonify({'status': "KO", "exc": str(ex), "traceback": traceback.format_exc()})


@app.route('/upload/image', methods=['POST'])
def upload_image():

    if request.method == 'POST':
        # check if the post request has the file part
        # if user does not select file, browser also
        # submit an empty part without filename

        errors = []
        conn = get_db_connection()
        cursor = conn.cursor()

        add_image = ("INSERT INTO images "
                     "(name, date_shot) "
                     "VALUES (%(name)s, %(date_shot)s)")

        files_num = 0
        for curr_file in request.files.items():
            curr_file_obj = curr_file[1]
            if curr_file_obj.filename == '':
                errors.append("Encountered empty file")
            elif curr_file and allowed_file(curr_file_obj.filename):
                filename = secure_filename(curr_file_obj.filename)
                print filename

                try:
                    m = re.search("(\d+-\d+-\d+_\d+_\d+_\d+\.\d+)", filename)
                    date_str = m.group(1)
                    date = datetime.strptime(date_str, "%Y-%m-%d_%H_%M_%S.%f")
                    print datetime.strftime(date, "%Y-%m-%d_%H_%M_%S.%f")
                except Exception as e:
                    errors.append("%s has no date in the filename, not added" % filename)
                    break

                img_path = os.path.join(IMAGE_UPLOAD_FOLDER, filename)
                if not image_present(filename, cursor):
                    try:
                        curr_file_obj.save(img_path)
                        files_num += 1
                        img_data = {"name": filename,
                                    "date_shot": date}
                        cursor.execute(add_image, img_data)
                    except Exception as e:
                        errors.append("Encountered an exception: %s" % str(e))
            else:
                errors.append("Encontered a file that is not allowed")

        conn.commit()
        cursor.close()
        conn.close()

        if len(errors) == 0:
            return jsonify({"status": "OK", "msg": "uploaded %d files" % files_num})
        else:
            return jsonify({"status": "KO",
                            "msg": "%d errors encountered" % len(errors),
                            "errors": errors})

    return jsonify({'status': "KO", "msg": "wrong upload method, POST is required"})


@app.route('/images/<id>')
def get_image(id):

    conn = get_db_connection()
    cursor = conn.cursor()

    name = get_image_name(id, cursor)
    res = send_from_directory(IMAGE_UPLOAD_FOLDER, name)

    conn.commit()
    cursor.close()
    conn.close()

    return res

@app.route('/get_random_unlabeled_image/<int:label_type>')
def get_random_unlabeled_image_serv(label_type):

    conn = get_db_connection()
    cursor = conn.cursor()

    query = (" SELECT ID from images "
             " WHERE ID NOT IN "
             " (SELECT ID_image FROM labels WHERE type = %s) "
             " ORDER BY RAND() "
             " LIMIT 1 ")

    cursor.execute(query, (label_type, ))


    res = None

    for row in cursor:
        res = convert_row(row)[0]

    conn.commit()
    cursor.close()
    conn.close()

    return jsonify({'status': "OK", "ID_image": res})

@app.route('/get_random_image')
def get_random_image():

    conn = get_db_connection()
    cursor = conn.cursor()

    query = (" SELECT ID from images "
             " ORDER BY RAND() "
             " LIMIT 1 ")

    cursor.execute(query)


    res = None

    for row in cursor:
        res = convert_row(row)[0]

    conn.commit()
    cursor.close()
    conn.close()

    return jsonify({'status': "OK", "ID_image": res})

@app.route('/get_random_unlabeled_and_unbursted_image')
def random_unlabeled_and_unbursted_image():

    conn = get_db_connection()
    cursor = conn.cursor()

    img_id = None
    stop = False
    i = 0

    while not stop and i < 10:

        img_id = get_random_unlabeled_image(cursor)

        bid = get_burst_id_by_image_id(img_id, cursor)
        if bid is not None:
            if not burst_has_labels(bid, cursor, min_labels=1):
                stop = True
        else:
            stop = True

        i += 1

    conn.commit()
    cursor.close()
    conn.close()

    return jsonify({'status': "OK", "ID_image": img_id, "trials": i})


@app.route('/get_random_image_for_human_classification')
def image_for_human_classification():

    def body(conn, cursor):
        img_id, stop, trials = get_image_for_human_classification_efficient(cursor)
        return jsonify({'status': "OK", "ID_image": img_id, "stopped": stop, "trials": trials})

    return simple_query(body)

@app.route('/get_random_unbursted_image')
def get_random_unbursted_image():

    conn = get_db_connection()
    cursor = conn.cursor()

    query = (" SELECT ID from images "
             " WHERE ID NOT IN "
             " (SELECT ID_image FROM burst_images) "
             " ORDER BY RAND() "
             " LIMIT 1 ")

    cursor.execute(query)


    res = None

    for row in cursor:
        res = convert_row(row)[0]

    conn.commit()
    cursor.close()
    conn.close()

    return jsonify({'status': "OK", "ID_image": res})

@app.route('/get_label_type_tree/<int:label_type>')
def get_label_type_tree_page(label_type):

    conn = get_db_connection()
    cursor = conn.cursor()

    res = get_label_type_tree(label_type, cursor)

    conn.commit()
    cursor.close()
    conn.close()

    return jsonify({'status': "OK", "tree": res})

@app.route('/get_label_path/<int:root_label_type>/<int:label_type>/<int:label_value>')
def get_label_full_path_service(root_label_type, label_type, label_value):
    conn = get_db_connection()
    cursor = conn.cursor()

    path = get_label_path(root_label_type, label_type, label_value, cursor)

    conn.commit()
    cursor.close()
    conn.close()

    return jsonify({'status': "OK", "path": path})

@app.route('/get_label_type_tree_w_counts/<int:label_type>')
def get_label_type_tree_w_counts_page(label_type):

    conn = get_db_connection()
    cursor = conn.cursor()

    res = get_label_type_tree(label_type, cursor, counts=True)

    conn.commit()
    cursor.close()
    conn.close()

    return jsonify({'status': "OK", "tree": res})


@app.route('/get_stats')
def get_stats():

    conn = get_db_connection()
    cursor = conn.cursor()

    avg_burst_size = get_average_burst_size(cursor)

    stats = {
        "tot_images": get_total_images(cursor),
        "labeled_images": get_total_labeled_images(1, cursor),
        "tot_signals": get_total_images_with_label_value(1, 1, cursor),
        "tot_bursted_images": get_bursted_image_count(cursor),
        "avg_burst_size": float(avg_burst_size) if avg_burst_size is not None else 0,
        "labeling_queue_length": get_labeling_queue_length(cursor)
        #"total_interesting_images_left": get_total_interesting_images_left(1, cursor)
    }

    stats["tot_unbursted_image"] = stats["tot_images"] - stats["tot_bursted_images"]
    stats["completion_perc"] = stats["labeled_images"]/float(stats["tot_images"])*100
    stats["signals_perc"] = stats["tot_signals"] / float(stats["labeled_images"]) * 100
    stats["speedup_factor"] = float(stats["avg_burst_size"]*stats["tot_bursted_images"] +
                              stats["tot_unbursted_image"]) / float(stats["tot_images"])

    conn.commit()
    cursor.close()
    conn.close()

    return jsonify({'status': "OK", "stats": stats})




@app.route('/add_label/<image_id>/<label_type>/<label_value>/<user_id>')
def add_label(image_id, label_type, label_value, user_id):

    conn = get_db_connection()
    cursor = conn.cursor()

    begin = datetime.now()

    query = ("INSERT INTO labels"
             "(ID_image, type, value, ID_user)"
             "VALUES (%s, %s, %s, %s)")

    cursor.execute(query, (image_id, label_type, label_value, user_id))

    delete_image_from_labeling_queue_service(image_id)

    query_time = datetime.now()

    update_image_bursts_labeled_images(image_id, cursor)

    burst_update_time = datetime.now()

    conn.commit()
    cursor.close()
    conn.close()

    print "AAAAAAAAAAAAAAA %s" % str(query_time-begin)
    print "BBBBBBBBBBBBBBB %s" % str(burst_update_time - query_time)
    return jsonify({'status': "OK", "msg": "label added", "time_query":str(query_time-begin)})

@app.route('/get_neighbors/<int:ID_image>/<int:seconds>')
def get_neighbors(ID_image, seconds):
    conn = get_db_connection()
    cursor = conn.cursor()

    date = get_image_date(ID_image, cursor)
    ids = get_neighbors_from_date(date, seconds, cursor)

    conn.commit()
    cursor.close()
    conn.close()

    return jsonify({'status': "OK", "date": date, "neighbors": ids})

@app.route('/get_neighbors_rev/<int:ID_image>/<int:seconds>')
def get_neighbors_rev(ID_image, seconds):
    conn = get_db_connection()
    cursor = conn.cursor()

    date = get_image_date(ID_image, cursor)
    ids = get_neighbors_to_date(date, seconds, cursor)

    conn.commit()
    cursor.close()
    conn.close()

    return jsonify({'status': "OK", "date": date, "neighbors": ids})

@app.route('/add_burst', methods=['POST'])
def add_burst():

    burst = request.get_json()

    if burst != None:

        has_burst = {}

        conn = get_db_connection()
        cursor = conn.cursor()

        burst_id = None

        for img_id in burst:

            bid = get_burst_id_by_image_id(img_id, cursor)

            if bid is not None:
                burst_id = bid
                has_burst[img_id] = True
                break

        if burst_id is None:
            burst_id = new_burst(cursor)

        added = 0
        for img_id in burst:
            bid = get_burst_id_by_image_id(img_id, cursor)
            if bid is None:
                add_image_to_burst(img_id, burst_id, cursor)
                added += 1

        conn.commit()
        cursor.close()
        conn.close()

        return jsonify({'status': "OK", "msg": "%d images added to burst %d" % (added, burst_id)})

    else:
        return jsonify({'status': "KO", "msg": "Data not in json format"})

@app.route('/get_random_labeled_images/<int:lbl_type>/<int:lbl_value>/<int:img_num>')
def get_random_labels_images(lbl_type, lbl_value, img_num):

    conn = get_db_connection()
    cursor = conn.cursor()

    ids = get_random_images_with_label(lbl_type, lbl_value, img_num, cursor)

    conn.commit()
    cursor.close()
    conn.close()

    return jsonify({'status': "OK", "images": ids})


@app.route('/get_dataset/<string:dataset_description>/<int:balanced>/<int:img_cnt_limit>')
def get_dataset(dataset_description, balanced, img_cnt_limit):

    balanced = False if balanced == 0 else True

    dataset_description = json.loads(dataset_description)
    # dataset_description=[[1,[[2,3],[2,4],[2,5],[2,6],[2,172],[2,173]]],[0,[[1,2]]]]

    conn = get_db_connection()
    cursor = conn.cursor()

    counts = {}

    for lbl_group in dataset_description:
        outer_lbl = lbl_group[0]
        inner_labels = lbl_group[1]
        counts[outer_lbl] = 0
        for lbl in inner_labels:
            counts[outer_lbl] += get_label_images_cnt(lbl[0], lbl[1], cursor)

    min_size = min(counts.values())

    dataset = {}

    for lbl_group in dataset_description:
        outer_lbl = lbl_group[0]
        inner_labels = lbl_group[1]

        dataset[outer_lbl] = []

        group_total_size = min_size if balanced else counts[outer_lbl]

        if img_cnt_limit > 0 and group_total_size > img_cnt_limit:
            group_total_size = img_cnt_limit

        for lbl in inner_labels:

            # How many images do we have in respect to the total images
            lbl_weight = float(get_label_images_cnt(lbl[0], lbl[1], cursor))/counts[outer_lbl]
            # Proportion on the group_total_size computed above (keeps the ratio between copules of labels in the same
            # group equal to the ratios on the full dataset)
            lbl_size = int(round(lbl_weight*group_total_size))

            dataset[outer_lbl].extend(get_random_images_with_label(lbl[0], lbl[1], lbl_size, cursor))

    conn.commit()
    cursor.close()
    conn.close()

    return jsonify({'status': "OK", "count": counts, "min": min_size, "dataset":dataset})

@app.route('/get_label_images_count/<int:lbl_type>/<int:lbl_value>')
def get_label_images_count(lbl_type, lbl_value):

    def body(conn, cursor):
        res = get_label_images_cnt(lbl_type, lbl_value, cursor)
        return jsonify({'status': "OK", "count": res})

    return simple_query(body)

@app.route('/get_label_images/<int:lbl_type>/<int:lbl_value>')
def get_label_images_service(lbl_type, lbl_value):

    def body(conn, cursor):
        res = get_label_images_w_bursts(lbl_type, lbl_value, cursor)
        return jsonify({'status': "OK", "ids": res})

    return simple_query(body)

@app.route('/get_label_images_predicted/<int:lbl_type>/<int:lbl_value>')
def get_label_images_predicted_service(lbl_type, lbl_value):

    def body(conn, cursor):
        #TODO: add bursts info
        res = get_label_images_predicted_w_bursts(lbl_type, lbl_value, cursor)
        return jsonify({'status': "OK", "ids": res})

    return simple_query(body)

@app.route('/add_image_score/<int:ID_image>/<int:ID_score>/<float:value>')
@app.route('/add_image_score/<int:ID_image>/<int:ID_score>/<float:value>/<int:ID_predicted_label_type>/<int:ID_predicted_label_value>')
def score_image(ID_image, ID_score, value, ID_predicted_label_type=None, ID_predicted_label_value=None):

    def body(conn, cursor):
        add_image_score(ID_image, ID_score, value, cursor, ID_predicted_label_type, ID_predicted_label_value)
        return jsonify({'status': "OK", "msg": "Score added"})

    return simple_query(body)

@app.route('/set_image_score/<int:ID_image>/<int:ID_score>/<float:value>')
@app.route('/set_image_score/<int:ID_image>/<int:ID_score>/<float:value>/<int:ID_predicted_label_type>/<int:ID_predicted_label_value>')
def set_image_score_service(ID_image, ID_score, value, ID_predicted_label_type=None, ID_predicted_label_value=None):

    def body(conn, cursor):
        set_image_score(ID_image, ID_score, value, cursor, ID_predicted_label_type, ID_predicted_label_value)
        return jsonify({'status': "OK", "msg": "Score set"})

    return simple_query(body)

@app.route('/set_images_scores/<string:data>')
def set_multiple_images_scores(data):
    data = json.loads(data)

    def body(conn, cursor):

        for entry in data:
            set_image_score(entry[0], entry[1], entry[2], cursor, entry[3], entry[4])

        return jsonify({'status': "OK", "msg": "Score set"})

    return simple_query(body)

@app.route('/get_unscored_images/<int:ID_score>')
def unscored_images(ID_score):

    def body(conn, cursor):
        ids = get_all_unscored_images(ID_score, cursor)
        return jsonify({'status': "OK", "images": ids})

    return simple_query(body)

@app.route('/get_all_images')
def all_images():

    def body(conn, cursor):
        ids = get_all_images(cursor)
        return jsonify({'status': "OK", "images": ids})

    return simple_query(body)

@app.route('/delete_image/<int:img_id>')
def delete_image_service(img_id):

    def body(conn, cursor):
        delete_image(img_id, cursor)
        return jsonify({'status': "OK", "msg": "image %d deleted" % img_id})

    return simple_query(body)

@app.route('/get_label_info/<int:label_type_id>/<int:label_value_id>')
def get_label_info_service(label_type_id, label_value_id):

    def body(conn, cursor):
        data = get_label_info(label_type_id, label_value_id, cursor)
        return jsonify({'status': "OK", "data": data})

    return simple_query(body)

@app.route('/delete_all_image_labels/<int:image_id>')
def delete_all_image_labels_service(image_id):

    def body(conn, cursor):
        delete_all_image_labels(image_id,cursor)
        return jsonify({'status': "OK"})

    return simple_query(body)

@app.route('/add_image_to_labeling_queue/<int:image_id>')
def add_image_to_labeling_queue_service(image_id):

    def body(conn, cursor):
        add_image_to_labeling_queue(image_id, cursor)
        return jsonify({'status': "OK"})

    return simple_query(body)

@app.route('/delete_image_from_labeling_queue/<int:image_id>')
def delete_image_from_labeling_queue_service(image_id):

    def body(conn, cursor):
        delete_image_from_labeling_queue(image_id, cursor)
        return jsonify({'status': "OK"})

    return simple_query(body)

@app.route('/get_ordered_images_to_be_labeled')
@app.route('/get_ordered_images_to_be_labeled/<int:num>')
@app.route('/get_ordered_images_to_be_labeled/<int:num>/<int:ID_score>')
def get_ordered_images_to_be_labeled_service(num=100, ID_score=5):

    def body(conn, cursor):

        clear_tabu_list(cursor)

        data = get_ordered_images_to_be_labeled(cursor, num, ID_score)

        for d in data:
            add_image_to_tabu(d["image_id"], cursor)

        return jsonify({'status': "OK", "data": data})

    return simple_query(body)

@app.route('/add_image_predicted_label/<int:ID_image>/<float:value>/<int:ID_predicted_label_type>/<int:ID_predicted_label_value>')
def add_image_predicted_label_service(ID_image, value, ID_predicted_label_type, ID_predicted_label_value):

    def body(conn, cursor):
        begin = datetime.now()
        add_image_predicted_label(ID_image, value, ID_predicted_label_type, ID_predicted_label_value, cursor)
        print datetime.now() - begin
        return jsonify({'status': "OK", "msg": "Label added"})

    return simple_query(body)

@app.route('/add_image_predicted_labels/<string:labels>')
def add_multiple_image_predicted_labels_service(labels):

    labels = json.loads(labels)

    def body(conn, cursor):
        begin = datetime.now()
        add_multiple_image_predicted_labels(labels, cursor)
        print datetime.now() - begin
        return jsonify({'status': "OK", "msg": "Label added"})

    return simple_query(body)

if __name__ == '__main__':
    app.run(threaded=True, host="0.0.0.0", debug=True)