-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Ott 12, 2018 alle 15:46
-- Versione del server: 10.1.31-MariaDB
-- Versione PHP: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `multisalaking`
--
CREATE DATABASE IF NOT EXISTS `multisalaking` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `multisalaking`;

-- --------------------------------------------------------

--
-- Struttura della tabella `chiavi_reset_password`
--

DROP TABLE IF EXISTS `chiavi_reset_password`;
CREATE TABLE `chiavi_reset_password` (
  `ID` int(11) NOT NULL,
  `chiave` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `utilizzata` int(11) DEFAULT '0',
  `ID_utente` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `chiavi_reset_password`
--

INSERT INTO `chiavi_reset_password` (`ID`, `chiave`, `data`, `utilizzata`, `ID_utente`) VALUES
(2, '2c506c23b56922a0f6bef721e5160f89ff50ae03148ecc0320e061832aa4f5d2', '2018-06-01 17:18:38', 0, 15),
(3, 'aeab5e332fe44fa69a22df678bab467c584767e192b0f7d11cf16f1b276976df', '2018-06-01 17:25:26', 1, 15),
(4, '4b03a198f7e5750f8a35d566c9865a8a4868fb083556b7521564438acb0e671b', '2018-06-01 17:26:12', 0, 15),
(5, 'db57f3383430cf197677826c132a6e56e28ea4a26f4d43bd5b177158cd77afe6', '2018-06-01 17:26:18', 1, 15),
(6, 'dcaea8a95627eebb0c68c9d668e0e50d29e6ec9ad7efd388c15f76a2ba131216', '2018-05-02 17:26:21', 0, 15),
(7, '7506ef64df1af82d44039d527bf667540d68e79c2e2ee4667f244fed43abbff1', '2018-06-01 17:26:25', 0, 15),
(8, '6ecfc1548e1aeea17527637c5d8b51692edb2a2100ec1053e3cca5388bbb05e5', '2018-06-01 17:26:29', 0, 15),
(9, 'fc74199bd48e00d1c218a9160268bd55a3910fce7a2c1f8bf699214d43643d27', '2018-06-01 17:45:55', 1, 15),
(10, 'd121721c6a3bbb3ccb239cc3e767e61f6b61ae2f9d0ec60a5bcb275941f41c5d', '2018-06-01 18:28:31', 0, 15),
(11, '790791bf7a3058f38abd962515b9f0fbc3282cfdf9fa8a84815ae1aa11893934', '2018-06-01 18:29:36', 1, 15),
(12, '2ec66feaf17a849b9a0ce780ad7c44d07715b48784b27aa32601bf53e88c8780', '2018-06-01 18:38:07', 1, 15),
(13, '997780cde5850f47f5573d3160a68836cfdcac1a708b78f49b907ea3b5c3cc94', '2018-06-01 18:41:15', 1, 15),
(14, 'a2121b60e2a85e880420471a3a210cc84f0cbc1404e00e4db7a1b3bb33447d9e', '2018-06-01 18:44:14', 1, 15),
(15, '0bfd0584148b9ee0d6090579d94c51118ce93d310906f10f8329901d5dfbec2a', '2018-06-05 07:10:02', 1, 15),
(16, '9a72b522c5795b92e4c247b6fceb763cf1c446878e308b428e9cb778ab91a1f6', '2018-06-05 08:01:14', 0, 12),
(17, 'bc85b1edf6af3746ee653e8c9364551d85daf9b2ad467a660213e8b6efbe2cf1', '2018-06-05 08:01:16', 1, 12),
(18, '871d7672d3fb35da979d517fae6d2ebffd2b55d8240712fecb1bc653e4319d6e', '2018-06-06 10:06:24', 1, 12),
(19, 'b19cea6d790a8ea782d9aec054f278dbe1f84f3c476d862abf4f7ad28b8a527a', '2018-06-29 15:38:15', 0, 15),
(20, '5488b6ec07746af08852ad14c63e09d5914531cd482a4bb87b0832d4f43a912f', '2018-06-29 16:30:00', 1, 15),
(21, '662fe7e12da9ae4b941c01abd6979816d61197bc5bd6d2788cd9feb3d8991589', '2018-06-29 16:36:18', 1, 15),
(22, '9e57e4e206ca25f644e15b611885fb8b2d1fc5a6e9e6e4d2f8ec3bd3e5b87c60', '2018-06-29 17:09:42', 0, 15),
(23, '0495e9bc073c246790759a0dbbe9b2b04cfce02239837bb953c53c9e92394395', '2018-06-29 17:10:30', 1, 15),
(24, '5187054e335d027fedd164e49c448f5e67ba627a013213a81c2b8c7d84eae839', '2018-06-29 18:49:29', 0, 15),
(25, 'ce21ce776d9f1aea5abb1ef7a8e9528a342115495d053cc73b73c8c0a62b4af4', '2018-07-01 07:00:33', 0, 15),
(26, 'a2e636f32f1d9bcd8b9127dd7bfab0d408c216f2296408c2dd3e4fb1c69621e2', '2018-10-03 08:17:12', 0, 15),
(27, 'beb99db3649242f5d8a21ddf24783f0228d370e679e5429fe48bc3deb4a0e912', '2018-10-03 08:17:51', 0, 15),
(28, '82f42657da0ca4fbb14ebedff0585876570432397504ae74ab4808b3c3b8f3f0', '2018-10-03 08:18:23', 0, 15),
(29, '5604f2d6d7e508e39a43f484d3c71f50b6f9f0b3562ef2d1280bd3e079e7103a', '2018-10-03 08:19:08', 0, 15),
(30, '0b05ee4740ff23f2e2d2f74c306d595aad537859b507633f9f1ead1b1f4003d2', '2018-10-03 09:51:12', 0, 15);

-- --------------------------------------------------------

--
-- Struttura della tabella `descrizione_prezzo`
--

DROP TABLE IF EXISTS `descrizione_prezzo`;
CREATE TABLE `descrizione_prezzo` (
  `ID` int(11) NOT NULL,
  `descrizione` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `descrizione_prezzo`
--

INSERT INTO `descrizione_prezzo` (`ID`, `descrizione`) VALUES
(1, 'Tariffa intera. Se non sai cosa scegliere vai con questa.'),
(2, 'Tutte le donne di qualsiasi età possono accedere a questa tariffa.'),
(3, 'Ridotto per ragazzi fino a  12 anni - Militari - Over 60'),
(4, 'In italia ogni giorno nuove persone diventano povere. Dato che ormai non sono più una minoranza, abbiamo una tariffa anche per loro. Ricordati di portare la certificazione ISEE per dimostrare il tuo stato di povertà.'),
(5, 'Sei uno studente delle scuole superiori o dell\'università? Questa tariffa è per te. Ricorda di portare il tesserino della scuola.');

-- --------------------------------------------------------

--
-- Struttura della tabella `evento`
--

DROP TABLE IF EXISTS `evento`;
CREATE TABLE `evento` (
  `ID` int(11) NOT NULL,
  `nome` text COLLATE utf8_bin,
  `descrizione` text COLLATE utf8_bin,
  `locandina` text COLLATE utf8_bin,
  `immagine` text COLLATE utf8_bin,
  `inizio` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fine` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `rilevanza` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `evento`
--

INSERT INTO `evento` (`ID`, `nome`, `descrizione`, `locandina`, `immagine`, `inizio`, `fine`, `rilevanza`) VALUES
(1, 'Maratona: \"Harry\" Potter', 'Tutti i film del famoso maghetto concentrati in un\'unica notte!\r\n\r\n<script> console.log(\"evt_descrizione_injection\"); </script>', '', 'hp.jpg', '2018-06-29 14:26:00', '2018-07-07 22:00:00', 10),
(2, 'Musica: Pink Floyd', 'I Pink Floyd \"tornano\", questa volta sul grande schermo! Non perderteli!', '', 'pf.jpg', '2018-06-29 14:26:27', '2018-07-28 22:00:00', 9),
(3, 'Evento Speciale: The Donald al King! <script>console.log(\"evt_titolo_injection\");</script>', 'Anche il presidente USA ama il King! Una serata dedicata alla sua visita presso di noi e alla fine si costruisce un bel muro tutti insieme! (lo pagano i messicani)', '', 'donald.jpg', '2018-06-26 09:35:00', '2018-07-28 22:00:00', 7),
(8, 'Ultima lezione di Laravel', 'L\'ultimissima lezione di Laravel, solo da noi, al King! \"aaa\"', 'ultima_lezione_di_laravel-loc.png', 'ultima_lezione_di_laravel-img.jpg', '2018-06-08 07:00:00', '2018-07-08 22:00:00', 8),
(10, 'come è?\"\"$£%&', 'asdfdsfds', 'come-loc.jpg', 'come-img.jpg', '2018-06-30 21:23:00', '2018-07-01 21:23:00', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `fascia`
--

DROP TABLE IF EXISTS `fascia`;
CREATE TABLE `fascia` (
  `ID` int(11) NOT NULL,
  `nome` text COLLATE utf8_bin NOT NULL,
  `descrizione` text COLLATE utf8_bin NOT NULL,
  `immagine` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `fascia`
--

INSERT INTO `fascia` (`ID`, `nome`, `descrizione`, `immagine`) VALUES
(2, 'Serata King', 'Una serata da re! Goditi una fantastica proiezione al prezzo più conveniente di sempre. ', NULL),
(3, 'Serata in rosa', 'Per celebrare l\'importanza di tutte le donne. Non importa che tu sia una ragazza, una mamma o una nonna: stasera il tuo ingresso al King costa di meno!', NULL),
(4, 'Mercoledì dei Poveri', 'Il giorno ideale per andare al cinema. Vestiti con i peggiori abiti che hai e ricorda di portare bibita e patatine da casa, perchè il personale del King non ci sarà.', NULL),
(5, 'Serata studenti', 'Gli studenti sono il futuro. Porta con te la carta d\'identità oppure il tesserino universitario per beneficiare di un ingresso a prezzo vantaggioso.', NULL),
(6, 'Weekend', 'Regalati un fine settimana da re e lasciati coccolare dalla comodità delle poltrone del King, piuttosto che rimanere impigliato nei meandri di una discoteca. Venerdì, Sabato e Domenica ti aspettiamo.', NULL),
(7, 'Anteprima', 'Non perderti la primissima proiezione del tuo film preferito, potrai dire di averlo visto prima di tutti gli altri!', 'vectorial/ANTEPRIMA.png'),
(8, 'Rassegna d\'Essay', 'I grandi film d\'Essay del passato e del presente, per una campagna culturale ed artistica senza eguali. Solo da noi, al King!', 'vectorial/ESSAY.png'),
(9, '3D', 'Goditi un\'esperienza unica. Il sistema Xspan 3D, prevede occhiali 3D attivi: le due lenti sono in realtà schermi LCD che si oscurano dinamicamente a seconda della scena. Lo spettatore si sentirà coinvolto fino all\'ultimo capello, ammesso che gliene rimangano.', 'vectorial/3D.png');

-- --------------------------------------------------------

--
-- Struttura della tabella `fascia_prezzo`
--

DROP TABLE IF EXISTS `fascia_prezzo`;
CREATE TABLE `fascia_prezzo` (
  `ID` int(11) NOT NULL,
  `ID_fascia` int(11) NOT NULL,
  `nome` text COLLATE utf8_bin NOT NULL,
  `valore` float NOT NULL,
  `rilevanza` int(11) NOT NULL,
  `ID_descrizione_prezzo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `fascia_prezzo`
--

INSERT INTO `fascia_prezzo` (`ID`, `ID_fascia`, `nome`, `valore`, `rilevanza`, `ID_descrizione_prezzo`) VALUES
(1, 2, 'INTERO', 4.5, 1, 1),
(2, 2, 'RIDOTTO', 4.5, 0, 3),
(3, 3, 'DONNA', 6, 2, 2),
(4, 3, 'INTERO', 7, 1, 1),
(5, 3, 'RIDOTTO', 5, 0, 3),
(6, 4, 'POVERO', 1.5, 2, 4),
(7, 4, 'INTERO', 6, 1, 1),
(8, 4, 'RIDOTTO', 5, 0, 3),
(9, 5, 'STUDENTE', 6, 2, 5),
(10, 5, 'INTERO', 7, 1, 1),
(11, 5, 'RIDOTTO', 5, 0, 3),
(12, 6, 'INTERO', 8, 1, 1),
(13, 6, 'RIDOTTO', 6.5, 0, 3),
(14, 7, 'INTERO', 8, 1, 1),
(15, 7, 'RIDOTTO', 6.5, 0, 3),
(16, 8, 'INTERO', 8, 1, 1),
(17, 8, 'RIDOTTO', 6.5, 0, 3),
(18, 9, 'INTERO', 10, 1, 1),
(19, 9, 'RIDOTTO', 8.5, 0, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `film`
--

DROP TABLE IF EXISTS `film`;
CREATE TABLE `film` (
  `ID` int(11) NOT NULL,
  `titolo` text COLLATE utf8_bin,
  `anno` int(11) DEFAULT NULL,
  `nazione` text COLLATE utf8_bin,
  `durata` int(11) DEFAULT NULL,
  `genere` text COLLATE utf8_bin,
  `regia` text COLLATE utf8_bin,
  `interpreti` text COLLATE utf8_bin,
  `trama` text COLLATE utf8_bin,
  `nome_locandina` text COLLATE utf8_bin,
  `url_trailer` text COLLATE utf8_bin,
  `url_recensione` text COLLATE utf8_bin,
  `rilevanza` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `film`
--

INSERT INTO `film` (`ID`, `titolo`, `anno`, `nazione`, `durata`, `genere`, `regia`, `interpreti`, `trama`, `nome_locandina`, `url_trailer`, `url_recensione`, `rilevanza`) VALUES
(507, 'AVENGERS INFINITY WAR', 2018, 'Usa', 140, 'Azione', 'Anthony Russo, Joe Russo', 'Scarlett Johansson, Chris Hemsworth, Elizabeth Olsen, Benedict Cumberbatch, Chris Evans, Robert Downey Jr., Zoe Saldana, Chris Pratt, Jon Favreau, Angela Bassett, Josh Brolin, Jeremy Renner, Paul Bettany, Mark Ruffalo, Gwyneth Paltrow, Don Cheadl', 'Un’impresa cinematografica senza precedenti, che è stata realizzata nell’arco di 10 anni e coinvolge tutti i personaggi dell’Universo Marvel. Gli Avengers e tanti altri eroi sono disposti a tutto per impedire al temibile Thanos di devastare completamente l’universo.', 'avengers_infinity_war.jpg', 'https://www.youtube.com/watch?v=kstJHS2IcTE', 'www.youtube.com', 2),
(508, 'LORO 1', 2018, 'Italia', 100, 'Biografico', 'Paolo Sorrentino', 'Toni Servillo, Elena Sofia Ricci, Riccardo Scamarcio, Kasia Smutniak, Euridice Axen, Fabrizio Bentivoglio, Roberto De Francesco, Dario Cantarelli, Anna Bonaiuto, Giovanni Esposito, Ugo Pagliai, Ricky Memphis, Lorenzo Gioielli, Alice Pagani, Caroline Tillette, Elena Cotta, Iaia Forte, Duccio Camerini, Yann Gael, Mattia Sbragia, Max Tortora, Milvia Marigliano, Michela Cescon, Roberto Herlitzka', 'L\'ascesa imprenditoriale e politica di Silvio Berlusconi.', 'loro_1.jpg', 'http://filmup.leonardo.it/trailers/loro1.shtml', NULL, 9),
(509, 'ESCOBAR IL FASCINO DEL MALE', 2018, 'Spagna', 110, 'Drammatico', 'Fernando León de Aranoa', 'Javier Bardem, Penélope Cruz, Peter Sarsgaard', 'La vita di Pablo Escobar dall’ascesa criminale all’inizio degli anni Ottanta fino alla morte nel 1993, passando per gli anni del narcoterrorismo, della lotta contro la possibile estradizione negli USA e del rapporto con la giornalista Virginia Vallejo che, dopo essere stata a lungo la sua amante, decise di collaborare con la giustizia favorendo la sua cattura. La storia di un rapporto passionale tra un popolare personaggio televisivo e il re indiscusso del narcotraffico internazionale, che prova fino alla fine a conciliare la sua attività criminale e politica con la vita privata e la famiglia..', 'escobar_il_fascino_del_male.jpeg', 'http://filmup.leonardo.it/trailers/lovingpablo.shtml', NULL, 8),
(510, 'RAMPAGE FURIA ANIMALE', 2018, 'Usa', 100, 'Azione', 'Brad Peyton', 'Dwayne Johnson', 'Il primatologo Davis Okoye è un uomo schivo. Condivide un legame indissolubile con George, un gorilla silverback straordinariamente intelligente di cui si occupa dalla nascita. Un esperimento genetico scorretto e dai risultati catastrofici trasforma la gentile scimmia in un’enorme creatura furiosa. A peggiorare le cose, ben presto si scopre che altri animali sono stati modificati nello stesso modo..', 'rampage_furia_animale.jpg', 'http://filmup.leonardo.it/trailers/rampage.shtml', NULL, 7),
(511, 'GAME NIGHT-INDOVINA CHI MUORE STASSERA?', 2018, 'Usa', 110, 'Commedia/Giallo', 'John Francis Daley, Jonathan Goldstein', 'Jason Bateman, Rachel McAdams', 'Max e Annie, una sera, insieme ad altre coppie con cui si riuniscono settimanalmente per una serata di giochi di società, su proposta del carismatico fratello di Max, Brooks, per gioco devono risolvere un misterioso delitto, con tanto di criminali e agenti federali improvvisati. Così anche quando Brooks viene rapito, fa tutto parte del gioco... vero? Ma non appena i sei partecipanti super competitivi si apprestano a risolvere il caso in vista della vittoria, scoprono che né il “gioco” – né tantomeno Brooks – sono quel che sembrano.', 'game_night.jpg', 'http://filmup.leonardo.it/trailers/gamenight.shtml', NULL, 6),
(512, 'L\'ISOLA DEI CANI', 2018, 'a', 110, 'Animazione', '', '', '', 'lisola_dei_cani.jpg', 'http://filmup.leonardo.it/trailers/lovingpablo.shtml', NULL, 5),
(513, 'ARRIVANO I PROF', 2018, 'Ita', 95, 'Commedia', 'Ivan Silvestrini', 'Claudio Bisio, Lino Guanciale, Maurizio Nichetti, Rocco Hunt, Maria Di Biase, Giusy Buscemi', 'Mentre (quasi) tutti festeggiano le promozioni all’esame di maturità, al liceo Alessandro Manzoni c’è grande preoccupazione: solo il 12% degli studenti è riuscito a conseguire il diploma. Il Manzoni ha un primato assoluto: è il peggior liceo d’Italia. Non sapendo più che soluzioni adottare, il Preside accoglie la proposta del Provveditore e decide di fare un ultimo, estremo, rischioso tentativo: reclutare i peggiori insegnanti in circolazione selezionati dall’algoritmo ministeriale nella speranza che dove hanno fallito i migliori, possano riuscire i peggiori....', 'arrivano_i_prof.jpg', 'http://filmup.leonardo.it/trailers/arrivanoiprof.shtml', '0', 4),
(538, 'Io il mio lavoro l\'ho feetttoooooo  <script> console.log(\"titolo_injection\"); </script>', 2017, 'Italia', 1, 'Comico e Drammatico ', 'ens  <script> console.log(\"regia_injection\"); </script>', 'Sergio Benini <script>\r\nconsole.log(\"interpreti_injection\");\r\n</script>', 'Io i file li ho caricati, il mio lavoro l\'ho feeeetttoooo,\r\n\r\nSe avete bisogno son qua. puff\r\n\r\n(heheeh gliel\'ho fatta ancora a quegli imbecilli hehehe, dovrebbero darmi l\'anello di bilbo baggings così sparisco più in fretta hehehehe)\r\n\r\n<script>\r\nconsole.log(\"trama_injection\");\r\n</script>', 'ioilmiolavorolhofatttoooooo5098-img.jpg', 'www.ilmanualedimatlab.com', 'www.ilmanualedimatlab.com', 10),
(542, 'Cybersecurity Drama è', 2018, 'Italia', 60, 'Tragicissimo', 'Netsec', 'Francesco Gringoli, Irlandese Noiseman', 'Irlandese fai pure con calma che siamo in anticipo di 1h hahaha...\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nGuys maa sono arrivato in ritardo?\r\n\r\n\r\n\r\nSi di un\'ora... \r\n\r\n\r\n\r\nNooooo ma come ho feeeetttttooo\r\n\r\nMa non è possibile\r\n\r\n\r\n\r\n(gira la testa numerose volte aiutandosi con le mani)\r\n\r\n\r\n\r\n(batte i piedi)\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n(fissa il vuoto)\r\n\r\n\r\n\r\nHAHAHAHAHAHAHAHAHAH\r\n\r\n\r\n<script>\r\nconsole.log(\"interpreti_injection\");\r\n</script>\r\n', 'cybersecurity.jpg', 'www.irlandese.com', 'www.irlandese.com', 10);

-- --------------------------------------------------------

--
-- Struttura della tabella `giorno_fascia`
--

DROP TABLE IF EXISTS `giorno_fascia`;
CREATE TABLE `giorno_fascia` (
  `ID` int(11) NOT NULL,
  `codice_giorno` int(11) NOT NULL,
  `ID_fascia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `giorno_fascia`
--

INSERT INTO `giorno_fascia` (`ID`, `codice_giorno`, `ID_fascia`) VALUES
(1, 0, 6),
(2, 1, 2),
(3, 2, 3),
(4, 3, 4),
(5, 4, 5),
(6, 5, 6),
(7, 6, 6);

-- --------------------------------------------------------

--
-- Struttura della tabella `posto`
--

DROP TABLE IF EXISTS `posto`;
CREATE TABLE `posto` (
  `ID` int(11) NOT NULL,
  `ID_sala` int(11) DEFAULT NULL,
  `nome` text COLLATE utf8_bin,
  `riga` int(11) DEFAULT NULL,
  `colonna` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `posto`
--

INSERT INTO `posto` (`ID`, `ID_sala`, `nome`, `riga`, `colonna`) VALUES
(17130, 82, 'A24', 0, 0),
(17131, 82, 'A23', 0, 1),
(17132, 82, 'A22', 0, 2),
(17133, 82, 'A21', 0, 3),
(17134, 82, 'A20', 0, 4),
(17135, 82, 'A19', 0, 5),
(17136, 82, 'A18', 0, 6),
(17137, 82, 'A17', 0, 7),
(17138, 82, 'A16', 0, 8),
(17139, 82, 'A15', 0, 9),
(17140, 82, 'A14', 0, 10),
(17141, 82, 'A13', 0, 11),
(17142, 82, 'A12', 0, 13),
(17143, 82, 'A11', 0, 14),
(17144, 82, 'A10', 0, 15),
(17145, 82, 'A9', 0, 16),
(17146, 82, 'A8', 0, 17),
(17147, 82, 'A7', 0, 18),
(17148, 82, 'A6', 0, 19),
(17149, 82, 'A5', 0, 20),
(17150, 82, 'A4', 0, 21),
(17151, 82, 'A3', 0, 22),
(17152, 82, 'A2', 0, 23),
(17153, 82, 'A1', 0, 24),
(17154, 82, 'B24', 1, 0),
(17155, 82, 'B23', 1, 1),
(17156, 82, 'B22', 1, 2),
(17157, 82, 'B21', 1, 3),
(17158, 82, 'B20', 1, 4),
(17159, 82, 'B19', 1, 5),
(17160, 82, 'B18', 1, 6),
(17161, 82, 'B17', 1, 7),
(17162, 82, 'B16', 1, 8),
(17163, 82, 'B15', 1, 9),
(17164, 82, 'B14', 1, 10),
(17165, 82, 'B13', 1, 11),
(17166, 82, 'B12', 1, 13),
(17167, 82, 'B11', 1, 14),
(17168, 82, 'B10', 1, 15),
(17169, 82, 'B9', 1, 16),
(17170, 82, 'B8', 1, 17),
(17171, 82, 'B7', 1, 18),
(17172, 82, 'B6', 1, 19),
(17173, 82, 'B5', 1, 20),
(17174, 82, 'B4', 1, 21),
(17175, 82, 'B3', 1, 22),
(17176, 82, 'B2', 1, 23),
(17177, 82, 'B1', 1, 24),
(17178, 82, 'C24', 2, 0),
(17179, 82, 'C23', 2, 1),
(17180, 82, 'C22', 2, 2),
(17181, 82, 'C21', 2, 3),
(17182, 82, 'C20', 2, 4),
(17183, 82, 'C19', 2, 5),
(17184, 82, 'C18', 2, 6),
(17185, 82, 'C17', 2, 7),
(17186, 82, 'C16', 2, 8),
(17187, 82, 'C15', 2, 9),
(17188, 82, 'C14', 2, 10),
(17189, 82, 'C13', 2, 11),
(17190, 82, 'C12', 2, 13),
(17191, 82, 'C11', 2, 14),
(17192, 82, 'C10', 2, 15),
(17193, 82, 'C9', 2, 16),
(17194, 82, 'C8', 2, 17),
(17195, 82, 'C7', 2, 18),
(17196, 82, 'C6', 2, 19),
(17197, 82, 'C5', 2, 20),
(17198, 82, 'C4', 2, 21),
(17199, 82, 'C3', 2, 22),
(17200, 82, 'C2', 2, 23),
(17201, 82, 'C1', 2, 24),
(17202, 82, 'D24', 3, 0),
(17203, 82, 'D23', 3, 1),
(17204, 82, 'D22', 3, 2),
(17205, 82, 'D21', 3, 3),
(17206, 82, 'D20', 3, 4),
(17207, 82, 'D19', 3, 5),
(17208, 82, 'D18', 3, 6),
(17209, 82, 'D17', 3, 7),
(17210, 82, 'D16', 3, 8),
(17211, 82, 'D15', 3, 9),
(17212, 82, 'D14', 3, 10),
(17213, 82, 'D13', 3, 11),
(17214, 82, 'D12', 3, 13),
(17215, 82, 'D11', 3, 14),
(17216, 82, 'D10', 3, 15),
(17217, 82, 'D9', 3, 16),
(17218, 82, 'D8', 3, 17),
(17219, 82, 'D7', 3, 18),
(17220, 82, 'D6', 3, 19),
(17221, 82, 'D5', 3, 20),
(17222, 82, 'D4', 3, 21),
(17223, 82, 'D3', 3, 22),
(17224, 82, 'D2', 3, 23),
(17225, 82, 'D1', 3, 24),
(17226, 82, 'E23', 4, 1),
(17227, 82, 'E22', 4, 2),
(17228, 82, 'E21', 4, 3),
(17229, 82, 'E20', 4, 4),
(17230, 82, 'E19', 4, 5),
(17231, 82, 'E18', 4, 6),
(17232, 82, 'E17', 4, 7),
(17233, 82, 'E16', 4, 8),
(17234, 82, 'E15', 4, 9),
(17235, 82, 'E14', 4, 10),
(17236, 82, 'E13', 4, 11),
(17237, 82, 'E12', 4, 13),
(17238, 82, 'E11', 4, 14),
(17239, 82, 'E10', 4, 15),
(17240, 82, 'E9', 4, 16),
(17241, 82, 'E8', 4, 17),
(17242, 82, 'E7', 4, 18),
(17243, 82, 'E6', 4, 19),
(17244, 82, 'E5', 4, 20),
(17245, 82, 'E4', 4, 21),
(17246, 82, 'E3', 4, 22),
(17247, 82, 'E2', 4, 23),
(17248, 82, 'F23', 5, 1),
(17249, 82, 'F22', 5, 2),
(17250, 82, 'F21', 5, 3),
(17251, 82, 'F20', 5, 4),
(17252, 82, 'F19', 5, 5),
(17253, 82, 'F18', 5, 6),
(17254, 82, 'F17', 5, 7),
(17255, 82, 'F16', 5, 8),
(17256, 82, 'F15', 5, 9),
(17257, 82, 'F14', 5, 10),
(17258, 82, 'F13', 5, 11),
(17259, 82, 'F12', 5, 13),
(17260, 82, 'F11', 5, 14),
(17261, 82, 'F10', 5, 15),
(17262, 82, 'F9', 5, 16),
(17263, 82, 'F8', 5, 17),
(17264, 82, 'F7', 5, 18),
(17265, 82, 'F6', 5, 19),
(17266, 82, 'F5', 5, 20),
(17267, 82, 'F4', 5, 21),
(17268, 82, 'F3', 5, 22),
(17269, 82, 'F2', 5, 23),
(17270, 82, 'G23', 6, 1),
(17271, 82, 'G22', 6, 2),
(17272, 82, 'G21', 6, 3),
(17273, 82, 'G20', 6, 4),
(17274, 82, 'G19', 6, 5),
(17275, 82, 'G18', 6, 6),
(17276, 82, 'G17', 6, 7),
(17277, 82, 'G16', 6, 8),
(17278, 82, 'G15', 6, 9),
(17279, 82, 'G14', 6, 10),
(17280, 82, 'G13', 6, 11),
(17281, 82, 'G12', 6, 13),
(17282, 82, 'G11', 6, 14),
(17283, 82, 'G10', 6, 15),
(17284, 82, 'G9', 6, 16),
(17285, 82, 'G8', 6, 17),
(17286, 82, 'G7', 6, 18),
(17287, 82, 'G6', 6, 19),
(17288, 82, 'G5', 6, 20),
(17289, 82, 'G4', 6, 21),
(17290, 82, 'G3', 6, 22),
(17291, 82, 'G2', 6, 23),
(17292, 82, 'H22', 7, 2),
(17293, 82, 'H21', 7, 3),
(17294, 82, 'H20', 7, 4),
(17295, 82, 'H19', 7, 5),
(17296, 82, 'H18', 7, 6),
(17297, 82, 'H17', 7, 7),
(17298, 82, 'H16', 7, 8),
(17299, 82, 'H15', 7, 9),
(17300, 82, 'H14', 7, 10),
(17301, 82, 'H13', 7, 11),
(17302, 82, 'H12', 7, 13),
(17303, 82, 'H11', 7, 14),
(17304, 82, 'H10', 7, 15),
(17305, 82, 'H9', 7, 16),
(17306, 82, 'H8', 7, 17),
(17307, 82, 'H7', 7, 18),
(17308, 82, 'H6', 7, 19),
(17309, 82, 'H5', 7, 20),
(17310, 82, 'H4', 7, 21),
(17311, 82, 'H3', 7, 22),
(17312, 82, 'I22', 8, 2),
(17313, 82, 'I21', 8, 3),
(17314, 82, 'I20', 8, 4),
(17315, 82, 'I19', 8, 5),
(17316, 82, 'I18', 8, 6),
(17317, 82, 'I17', 8, 7),
(17318, 82, 'I16', 8, 8),
(17319, 82, 'I15', 8, 9),
(17320, 82, 'I14', 8, 10),
(17321, 82, 'I13', 8, 11),
(17322, 82, 'I12', 8, 13),
(17323, 82, 'I11', 8, 14),
(17324, 82, 'I10', 8, 15),
(17325, 82, 'I9', 8, 16),
(17326, 82, 'I8', 8, 17),
(17327, 82, 'I7', 8, 18),
(17328, 82, 'I6', 8, 19),
(17329, 82, 'I5', 8, 20),
(17330, 82, 'I4', 8, 21),
(17331, 82, 'I3', 8, 22),
(17332, 82, 'L22', 9, 2),
(17333, 82, 'L21', 9, 3),
(17334, 82, 'L20', 9, 4),
(17335, 82, 'L19', 9, 5),
(17336, 82, 'L18', 9, 6),
(17337, 82, 'L17', 9, 7),
(17338, 82, 'L16', 9, 8),
(17339, 82, 'L15', 9, 9),
(17340, 82, 'L14', 9, 10),
(17341, 82, 'L13', 9, 11),
(17342, 82, 'L12', 9, 13),
(17343, 82, 'L11', 9, 14),
(17344, 82, 'L10', 9, 15),
(17345, 82, 'L9', 9, 16),
(17346, 82, 'L8', 9, 17),
(17347, 82, 'L7', 9, 18),
(17348, 82, 'L6', 9, 19),
(17349, 82, 'L5', 9, 20),
(17350, 82, 'L4', 9, 21),
(17351, 82, 'L3', 9, 22),
(17352, 83, 'A23', 0, 1),
(17353, 83, 'A22', 0, 2),
(17354, 83, 'A21', 0, 3),
(17355, 83, 'A20', 0, 4),
(17356, 83, 'A19', 0, 5),
(17357, 83, 'A18', 0, 6),
(17358, 83, 'A17', 0, 7),
(17359, 83, 'A16', 0, 8),
(17360, 83, 'A15', 0, 9),
(17361, 83, 'A14', 0, 10),
(17362, 83, 'A13', 0, 11),
(17363, 83, 'A12', 0, 13),
(17364, 83, 'A11', 0, 14),
(17365, 83, 'A10', 0, 15),
(17366, 83, 'A9', 0, 16),
(17367, 83, 'A8', 0, 17),
(17368, 83, 'A7', 0, 18),
(17369, 83, 'A6', 0, 19),
(17370, 83, 'A5', 0, 20),
(17371, 83, 'A4', 0, 21),
(17372, 83, 'A3', 0, 22),
(17373, 83, 'A2', 0, 23),
(17374, 83, 'A1', 0, 24),
(17375, 83, 'B24', 1, 0),
(17376, 83, 'B23', 1, 1),
(17377, 83, 'B22', 1, 2),
(17378, 83, 'B21', 1, 3),
(17379, 83, 'B20', 1, 4),
(17380, 83, 'B19', 1, 5),
(17381, 83, 'B18', 1, 6),
(17382, 83, 'B17', 1, 7),
(17383, 83, 'B16', 1, 8),
(17384, 83, 'B15', 1, 9),
(17385, 83, 'B14', 1, 10),
(17386, 83, 'B13', 1, 11),
(17387, 83, 'B12', 1, 13),
(17388, 83, 'B11', 1, 14),
(17389, 83, 'B10', 1, 15),
(17390, 83, 'B9', 1, 16),
(17391, 83, 'B8', 1, 17),
(17392, 83, 'B7', 1, 18),
(17393, 83, 'B6', 1, 19),
(17394, 83, 'B5', 1, 20),
(17395, 83, 'B4', 1, 21),
(17396, 83, 'B3', 1, 22),
(17397, 83, 'B2', 1, 23),
(17398, 83, 'B1', 1, 24),
(17399, 83, 'C24', 2, 0),
(17400, 83, 'C23', 2, 1),
(17401, 83, 'C22', 2, 2),
(17402, 83, 'C21', 2, 3),
(17403, 83, 'C20', 2, 4),
(17404, 83, 'C19', 2, 5),
(17405, 83, 'C18', 2, 6),
(17406, 83, 'C17', 2, 7),
(17407, 83, 'C16', 2, 8),
(17408, 83, 'C15', 2, 9),
(17409, 83, 'C14', 2, 10),
(17410, 83, 'C13', 2, 11),
(17411, 83, 'C12', 2, 13),
(17412, 83, 'C11', 2, 14),
(17413, 83, 'C10', 2, 15),
(17414, 83, 'C9', 2, 16),
(17415, 83, 'C8', 2, 17),
(17416, 83, 'C7', 2, 18),
(17417, 83, 'C6', 2, 19),
(17418, 83, 'C5', 2, 20),
(17419, 83, 'C4', 2, 21),
(17420, 83, 'C3', 2, 22),
(17421, 83, 'C2', 2, 23),
(17422, 83, 'C1', 2, 24),
(17423, 83, 'D24', 3, 0),
(17424, 83, 'D23', 3, 1),
(17425, 83, 'D22', 3, 2),
(17426, 83, 'D21', 3, 3),
(17427, 83, 'D20', 3, 4),
(17428, 83, 'D19', 3, 5),
(17429, 83, 'D18', 3, 6),
(17430, 83, 'D17', 3, 7),
(17431, 83, 'D16', 3, 8),
(17432, 83, 'D15', 3, 9),
(17433, 83, 'D14', 3, 10),
(17434, 83, 'D13', 3, 11),
(17435, 83, 'D12', 3, 13),
(17436, 83, 'D11', 3, 14),
(17437, 83, 'D10', 3, 15),
(17438, 83, 'D9', 3, 16),
(17439, 83, 'D8', 3, 17),
(17440, 83, 'D7', 3, 18),
(17441, 83, 'D6', 3, 19),
(17442, 83, 'D5', 3, 20),
(17443, 83, 'D4', 3, 21),
(17444, 83, 'D3', 3, 22),
(17445, 83, 'D2', 3, 23),
(17446, 83, 'D1', 3, 24),
(17447, 83, 'E23', 4, 1),
(17448, 83, 'E22', 4, 2),
(17449, 83, 'E21', 4, 3),
(17450, 83, 'E20', 4, 4),
(17451, 83, 'E19', 4, 5),
(17452, 83, 'E18', 4, 6),
(17453, 83, 'E17', 4, 7),
(17454, 83, 'E16', 4, 8),
(17455, 83, 'E15', 4, 9),
(17456, 83, 'E14', 4, 10),
(17457, 83, 'E13', 4, 11),
(17458, 83, 'E12', 4, 13),
(17459, 83, 'E11', 4, 14),
(17460, 83, 'E10', 4, 15),
(17461, 83, 'E9', 4, 16),
(17462, 83, 'E8', 4, 17),
(17463, 83, 'E7', 4, 18),
(17464, 83, 'E6', 4, 19),
(17465, 83, 'E5', 4, 20),
(17466, 83, 'E4', 4, 21),
(17467, 83, 'E3', 4, 22),
(17468, 83, 'E2', 4, 23),
(17469, 83, 'F23', 5, 1),
(17470, 83, 'F22', 5, 2),
(17471, 83, 'F21', 5, 3),
(17472, 83, 'F20', 5, 4),
(17473, 83, 'F19', 5, 5),
(17474, 83, 'F18', 5, 6),
(17475, 83, 'F17', 5, 7),
(17476, 83, 'F16', 5, 8),
(17477, 83, 'F15', 5, 9),
(17478, 83, 'F14', 5, 10),
(17479, 83, 'F13', 5, 11),
(17480, 83, 'F12', 5, 13),
(17481, 83, 'F11', 5, 14),
(17482, 83, 'F10', 5, 15),
(17483, 83, 'F9', 5, 16),
(17484, 83, 'F8', 5, 17),
(17485, 83, 'F7', 5, 18),
(17486, 83, 'F6', 5, 19),
(17487, 83, 'F5', 5, 20),
(17488, 83, 'F4', 5, 21),
(17489, 83, 'F3', 5, 22),
(17490, 83, 'F2', 5, 23),
(17491, 83, 'G23', 6, 1),
(17492, 83, 'G22', 6, 2),
(17493, 83, 'G21', 6, 3),
(17494, 83, 'G20', 6, 4),
(17495, 83, 'G19', 6, 5),
(17496, 83, 'G18', 6, 6),
(17497, 83, 'G17', 6, 7),
(17498, 83, 'G16', 6, 8),
(17499, 83, 'G15', 6, 9),
(17500, 83, 'G14', 6, 10),
(17501, 83, 'G13', 6, 11),
(17502, 83, 'G12', 6, 13),
(17503, 83, 'G11', 6, 14),
(17504, 83, 'G10', 6, 15),
(17505, 83, 'G9', 6, 16),
(17506, 83, 'G8', 6, 17),
(17507, 83, 'G7', 6, 18),
(17508, 83, 'G6', 6, 19),
(17509, 83, 'G5', 6, 20),
(17510, 83, 'G4', 6, 21),
(17511, 83, 'G3', 6, 22),
(17512, 83, 'G2', 6, 23),
(17513, 83, 'H22', 7, 2),
(17514, 83, 'H21', 7, 3),
(17515, 83, 'H20', 7, 4),
(17516, 83, 'H19', 7, 5),
(17517, 83, 'H18', 7, 6),
(17518, 83, 'H17', 7, 7),
(17519, 83, 'H16', 7, 8),
(17520, 83, 'H15', 7, 9),
(17521, 83, 'H14', 7, 10),
(17522, 83, 'H13', 7, 11),
(17523, 83, 'H12', 7, 13),
(17524, 83, 'H11', 7, 14),
(17525, 83, 'H10', 7, 15),
(17526, 83, 'H9', 7, 16),
(17527, 83, 'H8', 7, 17),
(17528, 83, 'H7', 7, 18),
(17529, 83, 'H6', 7, 19),
(17530, 83, 'H5', 7, 20),
(17531, 83, 'H4', 7, 21),
(17532, 83, 'H3', 7, 22),
(17533, 83, 'I22', 8, 2),
(17534, 83, 'I21', 8, 3),
(17535, 83, 'I20', 8, 4),
(17536, 83, 'I19', 8, 5),
(17537, 83, 'I18', 8, 6),
(17538, 83, 'I17', 8, 7),
(17539, 83, 'I16', 8, 8),
(17540, 83, 'I15', 8, 9),
(17541, 83, 'I14', 8, 10),
(17542, 83, 'I13', 8, 11),
(17543, 83, 'I12', 8, 13),
(17544, 83, 'I11', 8, 14),
(17545, 83, 'I10', 8, 15),
(17546, 83, 'I9', 8, 16),
(17547, 83, 'I8', 8, 17),
(17548, 83, 'I7', 8, 18),
(17549, 83, 'I6', 8, 19),
(17550, 83, 'I5', 8, 20),
(17551, 83, 'I4', 8, 21),
(17552, 83, 'I3', 8, 22),
(17553, 83, 'L22', 9, 2),
(17554, 83, 'L21', 9, 3),
(17555, 83, 'L20', 9, 4),
(17556, 83, 'L19', 9, 5),
(17557, 83, 'L18', 9, 6),
(17558, 83, 'L17', 9, 7),
(17559, 83, 'L16', 9, 8),
(17560, 83, 'L15', 9, 9),
(17561, 83, 'L14', 9, 10),
(17562, 83, 'L13', 9, 11),
(17563, 83, 'L12', 9, 13),
(17564, 83, 'L11', 9, 14),
(17565, 83, 'L10', 9, 15),
(17566, 83, 'L9', 9, 16),
(17567, 83, 'L8', 9, 17),
(17568, 83, 'L7', 9, 18),
(17569, 83, 'L6', 9, 19),
(17570, 83, 'L5', 9, 20),
(17571, 83, 'L4', 9, 21),
(17572, 83, 'L3', 9, 22),
(17573, 84, 'A29', 0, 1),
(17574, 84, 'A28', 0, 2),
(17575, 84, 'A27', 0, 3),
(17576, 84, 'A26', 0, 4),
(17577, 84, 'A25', 0, 5),
(17578, 84, 'A24', 0, 6),
(17579, 84, 'A23', 0, 7),
(17580, 84, 'A22', 0, 8),
(17581, 84, 'A21', 0, 9),
(17582, 84, 'A20', 0, 10),
(17583, 84, 'A19', 0, 11),
(17584, 84, 'A18', 0, 12),
(17585, 84, 'A17', 0, 13),
(17586, 84, 'A16', 0, 14),
(17587, 84, 'A15', 0, 16),
(17588, 84, 'A14', 0, 17),
(17589, 84, 'A13', 0, 18),
(17590, 84, 'A12', 0, 19),
(17591, 84, 'A11', 0, 20),
(17592, 84, 'A10', 0, 21),
(17593, 84, 'A9', 0, 22),
(17594, 84, 'A8', 0, 23),
(17595, 84, 'A7', 0, 24),
(17596, 84, 'A6', 0, 25),
(17597, 84, 'A5', 0, 26),
(17598, 84, 'A4', 0, 27),
(17599, 84, 'A3', 0, 28),
(17600, 84, 'A2', 0, 29),
(17601, 84, 'B30', 1, 0),
(17602, 84, 'B29', 1, 1),
(17603, 84, 'B28', 1, 2),
(17604, 84, 'B27', 1, 3),
(17605, 84, 'B26', 1, 4),
(17606, 84, 'B25', 1, 5),
(17607, 84, 'B24', 1, 6),
(17608, 84, 'B23', 1, 7),
(17609, 84, 'B22', 1, 8),
(17610, 84, 'B21', 1, 9),
(17611, 84, 'B20', 1, 10),
(17612, 84, 'B19', 1, 11),
(17613, 84, 'B18', 1, 12),
(17614, 84, 'B17', 1, 13),
(17615, 84, 'B16', 1, 14),
(17616, 84, 'B15', 1, 16),
(17617, 84, 'B14', 1, 17),
(17618, 84, 'B13', 1, 18),
(17619, 84, 'B12', 1, 19),
(17620, 84, 'B11', 1, 20),
(17621, 84, 'B10', 1, 21),
(17622, 84, 'B9', 1, 22),
(17623, 84, 'B8', 1, 23),
(17624, 84, 'B7', 1, 24),
(17625, 84, 'B6', 1, 25),
(17626, 84, 'B5', 1, 26),
(17627, 84, 'B4', 1, 27),
(17628, 84, 'B3', 1, 28),
(17629, 84, 'B2', 1, 29),
(17630, 84, 'B1', 1, 30),
(17631, 84, 'C30', 2, 0),
(17632, 84, 'C29', 2, 1),
(17633, 84, 'C28', 2, 2),
(17634, 84, 'C27', 2, 3),
(17635, 84, 'C26', 2, 4),
(17636, 84, 'C25', 2, 5),
(17637, 84, 'C24', 2, 6),
(17638, 84, 'C23', 2, 7),
(17639, 84, 'C22', 2, 8),
(17640, 84, 'C21', 2, 9),
(17641, 84, 'C20', 2, 10),
(17642, 84, 'C19', 2, 11),
(17643, 84, 'C18', 2, 12),
(17644, 84, 'C17', 2, 13),
(17645, 84, 'C16', 2, 14),
(17646, 84, 'C15', 2, 16),
(17647, 84, 'C14', 2, 17),
(17648, 84, 'C13', 2, 18),
(17649, 84, 'C12', 2, 19),
(17650, 84, 'C11', 2, 20),
(17651, 84, 'C10', 2, 21),
(17652, 84, 'C9', 2, 22),
(17653, 84, 'C8', 2, 23),
(17654, 84, 'C7', 2, 24),
(17655, 84, 'C6', 2, 25),
(17656, 84, 'C5', 2, 26),
(17657, 84, 'C4', 2, 27),
(17658, 84, 'C3', 2, 28),
(17659, 84, 'C2', 2, 29),
(17660, 84, 'C1', 2, 30),
(17661, 84, 'D29', 3, 1),
(17662, 84, 'D28', 3, 2),
(17663, 84, 'D27', 3, 3),
(17664, 84, 'D26', 3, 4),
(17665, 84, 'D25', 3, 5),
(17666, 84, 'D24', 3, 6),
(17667, 84, 'D23', 3, 7),
(17668, 84, 'D22', 3, 8),
(17669, 84, 'D21', 3, 9),
(17670, 84, 'D20', 3, 10),
(17671, 84, 'D19', 3, 11),
(17672, 84, 'D18', 3, 12),
(17673, 84, 'D17', 3, 13),
(17674, 84, 'D16', 3, 14),
(17675, 84, 'D15', 3, 16),
(17676, 84, 'D14', 3, 17),
(17677, 84, 'D13', 3, 18),
(17678, 84, 'D12', 3, 19),
(17679, 84, 'D11', 3, 20),
(17680, 84, 'D10', 3, 21),
(17681, 84, 'D9', 3, 22),
(17682, 84, 'D8', 3, 23),
(17683, 84, 'D7', 3, 24),
(17684, 84, 'D6', 3, 25),
(17685, 84, 'D5', 3, 26),
(17686, 84, 'D4', 3, 27),
(17687, 84, 'D3', 3, 28),
(17688, 84, 'D2', 3, 29),
(17689, 84, 'E29', 4, 1),
(17690, 84, 'E28', 4, 2),
(17691, 84, 'E27', 4, 3),
(17692, 84, 'E26', 4, 4),
(17693, 84, 'E25', 4, 5),
(17694, 84, 'E24', 4, 6),
(17695, 84, 'E23', 4, 7),
(17696, 84, 'E22', 4, 8),
(17697, 84, 'E21', 4, 9),
(17698, 84, 'E20', 4, 10),
(17699, 84, 'E19', 4, 11),
(17700, 84, 'E18', 4, 12),
(17701, 84, 'E17', 4, 13),
(17702, 84, 'E16', 4, 14),
(17703, 84, 'E15', 4, 16),
(17704, 84, 'E14', 4, 17),
(17705, 84, 'E13', 4, 18),
(17706, 84, 'E12', 4, 19),
(17707, 84, 'E11', 4, 20),
(17708, 84, 'E10', 4, 21),
(17709, 84, 'E9', 4, 22),
(17710, 84, 'E8', 4, 23),
(17711, 84, 'E7', 4, 24),
(17712, 84, 'E6', 4, 25),
(17713, 84, 'E5', 4, 26),
(17714, 84, 'E4', 4, 27),
(17715, 84, 'E3', 4, 28),
(17716, 84, 'E2', 4, 29),
(17717, 84, 'F29', 5, 1),
(17718, 84, 'F28', 5, 2),
(17719, 84, 'F27', 5, 3),
(17720, 84, 'F26', 5, 4),
(17721, 84, 'F25', 5, 5),
(17722, 84, 'F24', 5, 6),
(17723, 84, 'F23', 5, 7),
(17724, 84, 'F22', 5, 8),
(17725, 84, 'F21', 5, 9),
(17726, 84, 'F20', 5, 10),
(17727, 84, 'F19', 5, 11),
(17728, 84, 'F18', 5, 12),
(17729, 84, 'F17', 5, 13),
(17730, 84, 'F16', 5, 14),
(17731, 84, 'F15', 5, 16),
(17732, 84, 'F14', 5, 17),
(17733, 84, 'F13', 5, 18),
(17734, 84, 'F12', 5, 19),
(17735, 84, 'F11', 5, 20),
(17736, 84, 'F10', 5, 21),
(17737, 84, 'F9', 5, 22),
(17738, 84, 'F8', 5, 23),
(17739, 84, 'F7', 5, 24),
(17740, 84, 'F6', 5, 25),
(17741, 84, 'F5', 5, 26),
(17742, 84, 'F4', 5, 27),
(17743, 84, 'F3', 5, 28),
(17744, 84, 'F2', 5, 29),
(17745, 84, 'G29', 7, 1),
(17746, 84, 'G28', 7, 2),
(17747, 84, 'G27', 7, 3),
(17748, 84, 'G26', 7, 4),
(17749, 84, 'G25', 7, 5),
(17750, 84, 'G24', 7, 6),
(17751, 84, 'G23', 7, 7),
(17752, 84, 'G22', 7, 8),
(17753, 84, 'G21', 7, 9),
(17754, 84, 'G20', 7, 10),
(17755, 84, 'G19', 7, 11),
(17756, 84, 'G18', 7, 12),
(17757, 84, 'G17', 7, 13),
(17758, 84, 'G16', 7, 14),
(17759, 84, 'G15', 7, 16),
(17760, 84, 'G14', 7, 17),
(17761, 84, 'G13', 7, 18),
(17762, 84, 'G12', 7, 19),
(17763, 84, 'G11', 7, 20),
(17764, 84, 'G10', 7, 21),
(17765, 84, 'G9', 7, 22),
(17766, 84, 'G8', 7, 23),
(17767, 84, 'G7', 7, 24),
(17768, 84, 'G6', 7, 25),
(17769, 84, 'G5', 7, 26),
(17770, 84, 'G4', 7, 27),
(17771, 84, 'G3', 7, 28),
(17772, 84, 'G2', 7, 29),
(17773, 84, 'H29', 8, 1),
(17774, 84, 'H28', 8, 2),
(17775, 84, 'H27', 8, 3),
(17776, 84, 'H26', 8, 4),
(17777, 84, 'H25', 8, 5),
(17778, 84, 'H24', 8, 6),
(17779, 84, 'H23', 8, 7),
(17780, 84, 'H22', 8, 8),
(17781, 84, 'H21', 8, 9),
(17782, 84, 'H20', 8, 10),
(17783, 84, 'H19', 8, 11),
(17784, 84, 'H18', 8, 12),
(17785, 84, 'H17', 8, 13),
(17786, 84, 'H16', 8, 14),
(17787, 84, 'H15', 8, 16),
(17788, 84, 'H14', 8, 17),
(17789, 84, 'H13', 8, 18),
(17790, 84, 'H12', 8, 19),
(17791, 84, 'H11', 8, 20),
(17792, 84, 'H10', 8, 21),
(17793, 84, 'H9', 8, 22),
(17794, 84, 'H8', 8, 23),
(17795, 84, 'H7', 8, 24),
(17796, 84, 'H6', 8, 25),
(17797, 84, 'H5', 8, 26),
(17798, 84, 'H4', 8, 27),
(17799, 84, 'H3', 8, 28),
(17800, 84, 'H2', 8, 29),
(17801, 84, 'I28', 9, 2),
(17802, 84, 'I27', 9, 3),
(17803, 84, 'I26', 9, 4),
(17804, 84, 'I25', 9, 5),
(17805, 84, 'I24', 9, 6),
(17806, 84, 'I23', 9, 7),
(17807, 84, 'I22', 9, 8),
(17808, 84, 'I21', 9, 9),
(17809, 84, 'I20', 9, 10),
(17810, 84, 'I19', 9, 11),
(17811, 84, 'I18', 9, 12),
(17812, 84, 'I17', 9, 13),
(17813, 84, 'I16', 9, 14),
(17814, 84, 'I15', 9, 16),
(17815, 84, 'I14', 9, 17),
(17816, 84, 'I13', 9, 18),
(17817, 84, 'I12', 9, 19),
(17818, 84, 'I11', 9, 20),
(17819, 84, 'I10', 9, 21),
(17820, 84, 'I9', 9, 22),
(17821, 84, 'I8', 9, 23),
(17822, 84, 'I7', 9, 24),
(17823, 84, 'I6', 9, 25),
(17824, 84, 'I5', 9, 26),
(17825, 84, 'I4', 9, 27),
(17826, 84, 'I3', 9, 28),
(17827, 84, 'L28', 10, 2),
(17828, 84, 'L27', 10, 3),
(17829, 84, 'L26', 10, 4),
(17830, 84, 'L25', 10, 5),
(17831, 84, 'L24', 10, 6),
(17832, 84, 'L23', 10, 7),
(17833, 84, 'L22', 10, 8),
(17834, 84, 'L21', 10, 9),
(17835, 84, 'L20', 10, 10),
(17836, 84, 'L19', 10, 11),
(17837, 84, 'L18', 10, 12),
(17838, 84, 'L17', 10, 13),
(17839, 84, 'L16', 10, 14),
(17840, 84, 'L15', 10, 16),
(17841, 84, 'L14', 10, 17),
(17842, 84, 'L13', 10, 18),
(17843, 84, 'L12', 10, 19),
(17844, 84, 'L11', 10, 20),
(17845, 84, 'L10', 10, 21),
(17846, 84, 'L9', 10, 22),
(17847, 84, 'L8', 10, 23),
(17848, 84, 'L7', 10, 24),
(17849, 84, 'L6', 10, 25),
(17850, 84, 'L5', 10, 26),
(17851, 84, 'L4', 10, 27),
(17852, 84, 'L3', 10, 28),
(17853, 84, 'M28', 11, 2),
(17854, 84, 'M27', 11, 3),
(17855, 84, 'M26', 11, 4),
(17856, 84, 'M25', 11, 5),
(17857, 84, 'M24', 11, 6),
(17858, 84, 'M23', 11, 7),
(17859, 84, 'M22', 11, 8),
(17860, 84, 'M21', 11, 9),
(17861, 84, 'M20', 11, 10),
(17862, 84, 'M19', 11, 11),
(17863, 84, 'M18', 11, 12),
(17864, 84, 'M17', 11, 13),
(17865, 84, 'M16', 11, 14),
(17866, 84, 'M15', 11, 16),
(17867, 84, 'M14', 11, 17),
(17868, 84, 'M13', 11, 18),
(17869, 84, 'M12', 11, 19),
(17870, 84, 'M11', 11, 20),
(17871, 84, 'M10', 11, 21),
(17872, 84, 'M9', 11, 22),
(17873, 84, 'M8', 11, 23),
(17874, 84, 'M7', 11, 24),
(17875, 84, 'M6', 11, 25),
(17876, 84, 'M5', 11, 26),
(17877, 84, 'M4', 11, 27),
(17878, 84, 'M3', 11, 28),
(17879, 84, 'N28', 12, 2),
(17880, 84, 'N27', 12, 3),
(17881, 84, 'N26', 12, 4),
(17882, 84, 'N25', 12, 5),
(17883, 84, 'N24', 12, 6),
(17884, 84, 'N23', 12, 7),
(17885, 84, 'N22', 12, 8),
(17886, 84, 'N21', 12, 9),
(17887, 84, 'N20', 12, 10),
(17888, 84, 'N19', 12, 11),
(17889, 84, 'N18', 12, 12),
(17890, 84, 'N17', 12, 13),
(17891, 84, 'N16', 12, 14),
(17892, 84, 'N15', 12, 16),
(17893, 84, 'N14', 12, 17),
(17894, 84, 'N13', 12, 18),
(17895, 84, 'N12', 12, 19),
(17896, 84, 'N11', 12, 20),
(17897, 84, 'N10', 12, 21),
(17898, 84, 'N9', 12, 22),
(17899, 84, 'N8', 12, 23),
(17900, 84, 'N7', 12, 24),
(17901, 84, 'N6', 12, 25),
(17902, 84, 'N5', 12, 26),
(17903, 84, 'N4', 12, 27),
(17904, 84, 'N3', 12, 28),
(17905, 84, 'O28', 13, 2),
(17906, 84, 'O27', 13, 3),
(17907, 84, 'O26', 13, 4),
(17908, 84, 'O25', 13, 5),
(17909, 84, 'O24', 13, 6),
(17910, 84, 'O23', 13, 7),
(17911, 84, 'O22', 13, 8),
(17912, 84, 'O21', 13, 9),
(17913, 84, 'O20', 13, 10),
(17914, 84, 'O19', 13, 11),
(17915, 84, 'O18', 13, 12),
(17916, 84, 'O17', 13, 13),
(17917, 84, 'O16', 13, 14),
(17918, 84, 'O15', 13, 16),
(17919, 84, 'O14', 13, 17),
(17920, 84, 'O13', 13, 18),
(17921, 84, 'O12', 13, 19),
(17922, 84, 'O11', 13, 20),
(17923, 84, 'O10', 13, 21),
(17924, 84, 'O9', 13, 22),
(17925, 84, 'O8', 13, 23),
(17926, 84, 'O7', 13, 24),
(17927, 84, 'O6', 13, 25),
(17928, 84, 'O5', 13, 26),
(17929, 84, 'O4', 13, 27),
(17930, 84, 'O3', 13, 28),
(17931, 84, 'P27', 14, 3),
(17932, 84, 'P26', 14, 4),
(17933, 84, 'P25', 14, 5),
(17934, 84, 'P24', 14, 6),
(17935, 84, 'P23', 14, 7),
(17936, 84, 'P22', 14, 8),
(17937, 84, 'P21', 14, 9),
(17938, 84, 'P20', 14, 10),
(17939, 84, 'P19', 14, 11),
(17940, 84, 'P18', 14, 12),
(17941, 84, 'P17', 14, 13),
(17942, 84, 'P16', 14, 14),
(17943, 84, 'P15', 14, 16),
(17944, 84, 'P14', 14, 17),
(17945, 84, 'P13', 14, 18),
(17946, 84, 'P12', 14, 19),
(17947, 84, 'P11', 14, 20),
(17948, 84, 'P10', 14, 21),
(17949, 84, 'P9', 14, 22),
(17950, 84, 'P8', 14, 23),
(17951, 84, 'P7', 14, 24),
(17952, 84, 'P6', 14, 25),
(17953, 84, 'P5', 14, 26),
(17954, 84, 'P4', 14, 27),
(17955, 84, 'Q27', 15, 3),
(17956, 84, 'Q26', 15, 4),
(17957, 84, 'Q25', 15, 5),
(17958, 84, 'Q24', 15, 6),
(17959, 84, 'Q23', 15, 7),
(17960, 84, 'Q22', 15, 8),
(17961, 84, 'Q21', 15, 9),
(17962, 84, 'Q20', 15, 10),
(17963, 84, 'Q19', 15, 11),
(17964, 84, 'Q18', 15, 12),
(17965, 84, 'Q17', 15, 13),
(17966, 84, 'Q16', 15, 14),
(17967, 84, 'Q15', 15, 16),
(17968, 84, 'Q14', 15, 17),
(17969, 84, 'Q13', 15, 18),
(17970, 84, 'Q12', 15, 19),
(17971, 84, 'Q11', 15, 20),
(17972, 84, 'Q10', 15, 21),
(17973, 84, 'Q9', 15, 22),
(17974, 84, 'Q8', 15, 23),
(17975, 84, 'Q7', 15, 24),
(17976, 84, 'Q6', 15, 25),
(17977, 84, 'Q5', 15, 26),
(17978, 84, 'Q4', 15, 27),
(17979, 84, 'R27', 16, 3),
(17980, 84, 'R26', 16, 4),
(17981, 84, 'R25', 16, 5),
(17982, 84, 'R24', 16, 6),
(17983, 84, 'R23', 16, 7),
(17984, 84, 'R22', 16, 8),
(17985, 84, 'R21', 16, 9),
(17986, 84, 'R20', 16, 10),
(17987, 84, 'R19', 16, 11),
(17988, 84, 'R18', 16, 12),
(17989, 84, 'R17', 16, 13),
(17990, 84, 'R16', 16, 14),
(17991, 84, 'R15', 16, 16),
(17992, 84, 'R14', 16, 17),
(17993, 84, 'R13', 16, 18),
(17994, 84, 'R12', 16, 19),
(17995, 84, 'R11', 16, 20),
(17996, 84, 'R10', 16, 21),
(17997, 84, 'R9', 16, 22),
(17998, 84, 'R8', 16, 23),
(17999, 84, 'R7', 16, 24),
(18000, 84, 'R6', 16, 25),
(18001, 84, 'R5', 16, 26),
(18002, 84, 'R4', 16, 27),
(18003, 84, 'S27', 17, 3),
(18004, 84, 'S26', 17, 4),
(18005, 84, 'S25', 17, 5),
(18006, 84, 'S24', 17, 6),
(18007, 84, 'S23', 17, 7),
(18008, 84, 'S22', 17, 8),
(18009, 84, 'S21', 17, 9),
(18010, 84, 'S20', 17, 10),
(18011, 84, 'S19', 17, 11),
(18012, 84, 'S18', 17, 12),
(18013, 84, 'S17', 17, 13),
(18014, 84, 'S16', 17, 14),
(18015, 84, 'S15', 17, 16),
(18016, 84, 'S14', 17, 17),
(18017, 84, 'S13', 17, 18),
(18018, 84, 'S12', 17, 19),
(18019, 84, 'S11', 17, 20),
(18020, 84, 'S10', 17, 21),
(18021, 84, 'S9', 17, 22),
(18022, 84, 'S8', 17, 23),
(18023, 84, 'S7', 17, 24),
(18024, 84, 'S6', 17, 25),
(18025, 84, 'S5', 17, 26),
(18026, 84, 'S4', 17, 27),
(18027, 85, 'A22', 0, 0),
(18028, 85, 'A21', 0, 1),
(18029, 85, 'A20', 0, 2),
(18030, 85, 'A19', 0, 3),
(18031, 85, 'A18', 0, 4),
(18032, 85, 'A17', 0, 5),
(18033, 85, 'A16', 0, 6),
(18034, 85, 'A15', 0, 7),
(18035, 85, 'A14', 0, 8),
(18036, 85, 'A13', 0, 9),
(18037, 85, 'A12', 0, 10),
(18038, 85, 'A11', 0, 12),
(18039, 85, 'A10', 0, 13),
(18040, 85, 'A9', 0, 14),
(18041, 85, 'A8', 0, 15),
(18042, 85, 'A7', 0, 16),
(18043, 85, 'A6', 0, 17),
(18044, 85, 'A5', 0, 18),
(18045, 85, 'A4', 0, 19),
(18046, 85, 'A3', 0, 20),
(18047, 85, 'A2', 0, 21),
(18048, 85, 'B22', 1, 0),
(18049, 85, 'B21', 1, 1),
(18050, 85, 'B20', 1, 2),
(18051, 85, 'B19', 1, 3),
(18052, 85, 'B18', 1, 4),
(18053, 85, 'B17', 1, 5),
(18054, 85, 'B16', 1, 6),
(18055, 85, 'B15', 1, 7),
(18056, 85, 'B14', 1, 8),
(18057, 85, 'B13', 1, 9),
(18058, 85, 'B12', 1, 10),
(18059, 85, 'B11', 1, 12),
(18060, 85, 'B10', 1, 13),
(18061, 85, 'B9', 1, 14),
(18062, 85, 'B8', 1, 15),
(18063, 85, 'B7', 1, 16),
(18064, 85, 'B6', 1, 17),
(18065, 85, 'B5', 1, 18),
(18066, 85, 'B4', 1, 19),
(18067, 85, 'B3', 1, 20),
(18068, 85, 'B2', 1, 21),
(18069, 85, 'B1', 1, 22),
(18070, 85, 'C22', 2, 0),
(18071, 85, 'C21', 2, 1),
(18072, 85, 'C20', 2, 2),
(18073, 85, 'C19', 2, 3),
(18074, 85, 'C18', 2, 4),
(18075, 85, 'C17', 2, 5),
(18076, 85, 'C16', 2, 6),
(18077, 85, 'C15', 2, 7),
(18078, 85, 'C14', 2, 8),
(18079, 85, 'C13', 2, 9),
(18080, 85, 'C12', 2, 10),
(18081, 85, 'C11', 2, 12),
(18082, 85, 'C10', 2, 13),
(18083, 85, 'C9', 2, 14),
(18084, 85, 'C8', 2, 15),
(18085, 85, 'C7', 2, 16),
(18086, 85, 'C6', 2, 17),
(18087, 85, 'C5', 2, 18),
(18088, 85, 'C4', 2, 19),
(18089, 85, 'C3', 2, 20),
(18090, 85, 'C2', 2, 21),
(18091, 85, 'C1', 2, 22),
(18092, 85, 'D22', 3, 0),
(18093, 85, 'D21', 3, 1),
(18094, 85, 'D20', 3, 2),
(18095, 85, 'D19', 3, 3),
(18096, 85, 'D18', 3, 4),
(18097, 85, 'D17', 3, 5),
(18098, 85, 'D16', 3, 6),
(18099, 85, 'D15', 3, 7),
(18100, 85, 'D14', 3, 8),
(18101, 85, 'D13', 3, 9),
(18102, 85, 'D12', 3, 10),
(18103, 85, 'D11', 3, 12),
(18104, 85, 'D10', 3, 13),
(18105, 85, 'D9', 3, 14),
(18106, 85, 'D8', 3, 15),
(18107, 85, 'D7', 3, 16),
(18108, 85, 'D6', 3, 17),
(18109, 85, 'D5', 3, 18),
(18110, 85, 'D4', 3, 19),
(18111, 85, 'D3', 3, 20),
(18112, 85, 'D2', 3, 21),
(18113, 85, 'D1', 3, 22),
(18114, 85, 'E22', 4, 0),
(18115, 85, 'E21', 4, 1),
(18116, 85, 'E20', 4, 2),
(18117, 85, 'E19', 4, 3),
(18118, 85, 'E18', 4, 4),
(18119, 85, 'E17', 4, 5),
(18120, 85, 'E16', 4, 6),
(18121, 85, 'E15', 4, 7),
(18122, 85, 'E14', 4, 8),
(18123, 85, 'E13', 4, 9),
(18124, 85, 'E12', 4, 10),
(18125, 85, 'E11', 4, 12),
(18126, 85, 'E10', 4, 13),
(18127, 85, 'E9', 4, 14),
(18128, 85, 'E8', 4, 15),
(18129, 85, 'E7', 4, 16),
(18130, 85, 'E6', 4, 17),
(18131, 85, 'E5', 4, 18),
(18132, 85, 'E4', 4, 19),
(18133, 85, 'E3', 4, 20),
(18134, 85, 'E2', 4, 21),
(18135, 85, 'E1', 4, 22),
(18136, 85, 'F22', 5, 0),
(18137, 85, 'F21', 5, 1),
(18138, 85, 'F20', 5, 2),
(18139, 85, 'F19', 5, 3),
(18140, 85, 'F18', 5, 4),
(18141, 85, 'F17', 5, 5),
(18142, 85, 'F16', 5, 6),
(18143, 85, 'F15', 5, 7),
(18144, 85, 'F14', 5, 8),
(18145, 85, 'F13', 5, 9),
(18146, 85, 'F12', 5, 10),
(18147, 85, 'F11', 5, 12),
(18148, 85, 'F10', 5, 13),
(18149, 85, 'F9', 5, 14),
(18150, 85, 'F8', 5, 15),
(18151, 85, 'F7', 5, 16),
(18152, 85, 'F6', 5, 17),
(18153, 85, 'F5', 5, 18),
(18154, 85, 'F4', 5, 19),
(18155, 85, 'F3', 5, 20),
(18156, 85, 'F2', 5, 21),
(18157, 85, 'F1', 5, 22),
(18158, 85, 'G21', 6, 1),
(18159, 85, 'G20', 6, 2),
(18160, 85, 'G19', 6, 3),
(18161, 85, 'G18', 6, 4),
(18162, 85, 'G17', 6, 5),
(18163, 85, 'G16', 6, 6),
(18164, 85, 'G15', 6, 7),
(18165, 85, 'G14', 6, 8),
(18166, 85, 'G13', 6, 9),
(18167, 85, 'G12', 6, 10),
(18168, 85, 'G11', 6, 12),
(18169, 85, 'G10', 6, 13),
(18170, 85, 'G9', 6, 14),
(18171, 85, 'G8', 6, 15),
(18172, 85, 'G7', 6, 16),
(18173, 85, 'G6', 6, 17),
(18174, 85, 'G5', 6, 18),
(18175, 85, 'G4', 6, 19),
(18176, 85, 'G3', 6, 20),
(18177, 85, 'G2', 6, 21),
(18178, 85, 'H21', 7, 1),
(18179, 85, 'H20', 7, 2),
(18180, 85, 'H19', 7, 3),
(18181, 85, 'H18', 7, 4),
(18182, 85, 'H17', 7, 5),
(18183, 85, 'H16', 7, 6),
(18184, 85, 'H15', 7, 7),
(18185, 85, 'H14', 7, 8),
(18186, 85, 'H13', 7, 9),
(18187, 85, 'H12', 7, 10),
(18188, 85, 'H11', 7, 12),
(18189, 85, 'H10', 7, 13),
(18190, 85, 'H9', 7, 14),
(18191, 85, 'H8', 7, 15),
(18192, 85, 'H7', 7, 16),
(18193, 85, 'H6', 7, 17),
(18194, 85, 'H5', 7, 18),
(18195, 85, 'H4', 7, 19),
(18196, 85, 'H3', 7, 20),
(18197, 85, 'H2', 7, 21),
(18198, 85, 'I21', 8, 1),
(18199, 85, 'I20', 8, 2),
(18200, 85, 'I19', 8, 3),
(18201, 85, 'I18', 8, 4),
(18202, 85, 'I17', 8, 5),
(18203, 85, 'I16', 8, 6),
(18204, 85, 'I15', 8, 7),
(18205, 85, 'I14', 8, 8),
(18206, 85, 'I13', 8, 9),
(18207, 85, 'I12', 8, 10),
(18208, 85, 'I11', 8, 12),
(18209, 85, 'I10', 8, 13),
(18210, 85, 'I9', 8, 14),
(18211, 85, 'I8', 8, 15),
(18212, 85, 'I7', 8, 16),
(18213, 85, 'I6', 8, 17),
(18214, 85, 'I5', 8, 18),
(18215, 85, 'I4', 8, 19),
(18216, 85, 'I3', 8, 20),
(18217, 85, 'I2', 8, 21),
(18218, 85, 'L21', 9, 1),
(18219, 85, 'L20', 9, 2),
(18220, 85, 'L19', 9, 3),
(18221, 85, 'L18', 9, 4),
(18222, 85, 'L17', 9, 5),
(18223, 85, 'L16', 9, 6),
(18224, 85, 'L15', 9, 7),
(18225, 85, 'L14', 9, 8),
(18226, 85, 'L13', 9, 9),
(18227, 85, 'L12', 9, 10),
(18228, 85, 'L11', 9, 12),
(18229, 85, 'L10', 9, 13),
(18230, 85, 'L9', 9, 14),
(18231, 85, 'L8', 9, 15),
(18232, 85, 'L7', 9, 16),
(18233, 85, 'L6', 9, 17),
(18234, 85, 'L5', 9, 18),
(18235, 85, 'L4', 9, 19),
(18236, 85, 'L3', 9, 20),
(18237, 85, 'L2', 9, 21),
(18238, 86, 'A17', 0, 0),
(18239, 86, 'A16', 0, 1),
(18240, 86, 'A15', 0, 2),
(18241, 86, 'A14', 0, 3),
(18242, 86, 'A13', 0, 4),
(18243, 86, 'A12', 0, 5),
(18244, 86, 'A11', 0, 6),
(18245, 86, 'A10', 0, 7),
(18246, 86, 'A9', 0, 8),
(18247, 86, 'A8', 0, 9),
(18248, 86, 'A7', 0, 10),
(18249, 86, 'A6', 0, 11),
(18250, 86, 'A5', 0, 12),
(18251, 86, 'A4', 0, 13),
(18252, 86, 'A3', 0, 14),
(18253, 86, 'A2', 0, 15),
(18254, 86, 'A1', 0, 16),
(18255, 86, 'B17', 1, 0),
(18256, 86, 'B16', 1, 1),
(18257, 86, 'B15', 1, 2),
(18258, 86, 'B14', 1, 3),
(18259, 86, 'B13', 1, 4),
(18260, 86, 'B12', 1, 5),
(18261, 86, 'B11', 1, 6),
(18262, 86, 'B10', 1, 7),
(18263, 86, 'B9', 1, 8),
(18264, 86, 'B8', 1, 9),
(18265, 86, 'B7', 1, 10),
(18266, 86, 'B6', 1, 11),
(18267, 86, 'B5', 1, 12),
(18268, 86, 'B4', 1, 13),
(18269, 86, 'B3', 1, 14),
(18270, 86, 'B2', 1, 15),
(18271, 86, 'B1', 1, 16),
(18272, 86, 'B0', 1, 17),
(18273, 86, 'C17', 2, 0),
(18274, 86, 'C16', 2, 1),
(18275, 86, 'C15', 2, 2),
(18276, 86, 'C14', 2, 3),
(18277, 86, 'C13', 2, 4),
(18278, 86, 'C12', 2, 5),
(18279, 86, 'C11', 2, 6),
(18280, 86, 'C10', 2, 7),
(18281, 86, 'C9', 2, 8),
(18282, 86, 'C8', 2, 9),
(18283, 86, 'C7', 2, 10),
(18284, 86, 'C6', 2, 11),
(18285, 86, 'C5', 2, 12),
(18286, 86, 'C4', 2, 13),
(18287, 86, 'C3', 2, 14),
(18288, 86, 'C2', 2, 15),
(18289, 86, 'C1', 2, 16),
(18290, 86, 'C0', 2, 17),
(18291, 86, 'D16', 3, 1),
(18292, 86, 'D15', 3, 2),
(18293, 86, 'D14', 3, 3),
(18294, 86, 'D13', 3, 4),
(18295, 86, 'D12', 3, 5),
(18296, 86, 'D11', 3, 6),
(18297, 86, 'D10', 3, 7),
(18298, 86, 'D9', 3, 8),
(18299, 86, 'D8', 3, 9),
(18300, 86, 'D7', 3, 10),
(18301, 86, 'D6', 3, 11),
(18302, 86, 'D5', 3, 12),
(18303, 86, 'D4', 3, 13),
(18304, 86, 'D3', 3, 14),
(18305, 86, 'D2', 3, 15),
(18306, 86, 'D1', 3, 16),
(18307, 86, 'E16', 4, 1),
(18308, 86, 'E15', 4, 2),
(18309, 86, 'E14', 4, 3),
(18310, 86, 'E13', 4, 4),
(18311, 86, 'E12', 4, 5),
(18312, 86, 'E11', 4, 6),
(18313, 86, 'E10', 4, 7),
(18314, 86, 'E9', 4, 8),
(18315, 86, 'E8', 4, 9),
(18316, 86, 'E7', 4, 10),
(18317, 86, 'E6', 4, 11),
(18318, 86, 'E5', 4, 12),
(18319, 86, 'E4', 4, 13),
(18320, 86, 'E3', 4, 14),
(18321, 86, 'E2', 4, 15),
(18322, 86, 'E1', 4, 16),
(18323, 86, 'F16', 5, 1),
(18324, 86, 'F15', 5, 2),
(18325, 86, 'F14', 5, 3),
(18326, 86, 'F13', 5, 4),
(18327, 86, 'F12', 5, 5),
(18328, 86, 'F11', 5, 6),
(18329, 86, 'F10', 5, 7),
(18330, 86, 'F9', 5, 8),
(18331, 86, 'F8', 5, 9),
(18332, 86, 'F7', 5, 10),
(18333, 86, 'F6', 5, 11),
(18334, 86, 'F5', 5, 12),
(18335, 86, 'F4', 5, 13),
(18336, 86, 'F3', 5, 14),
(18337, 86, 'F2', 5, 15),
(18338, 86, 'F1', 5, 16),
(18339, 86, 'G16', 6, 1),
(18340, 86, 'G15', 6, 2),
(18341, 86, 'G14', 6, 3),
(18342, 86, 'G13', 6, 4),
(18343, 86, 'G12', 6, 5),
(18344, 86, 'G11', 6, 6),
(18345, 86, 'G10', 6, 7),
(18346, 86, 'G9', 6, 8),
(18347, 86, 'G8', 6, 9),
(18348, 86, 'G7', 6, 10),
(18349, 86, 'G6', 6, 11),
(18350, 86, 'G5', 6, 12),
(18351, 86, 'G4', 6, 13),
(18352, 86, 'G3', 6, 14),
(18353, 86, 'G2', 6, 15),
(18354, 86, 'G1', 6, 16),
(18355, 86, 'H16', 7, 1),
(18356, 86, 'H15', 7, 2),
(18357, 86, 'H14', 7, 3),
(18358, 86, 'H13', 7, 4),
(18359, 86, 'H12', 7, 5),
(18360, 86, 'H11', 7, 6),
(18361, 86, 'H10', 7, 7),
(18362, 86, 'H9', 7, 8),
(18363, 86, 'H8', 7, 9),
(18364, 86, 'H7', 7, 10),
(18365, 86, 'H6', 7, 11),
(18366, 86, 'H5', 7, 12),
(18367, 86, 'H4', 7, 13),
(18368, 86, 'H3', 7, 14),
(18369, 86, 'H2', 7, 15),
(18370, 86, 'H1', 7, 16),
(18371, 86, 'I16', 8, 1),
(18372, 86, 'I15', 8, 2),
(18373, 86, 'I14', 8, 3),
(18374, 86, 'I13', 8, 4),
(18375, 86, 'I12', 8, 5),
(18376, 86, 'I11', 8, 6),
(18377, 86, 'I10', 8, 7),
(18378, 86, 'I9', 8, 8),
(18379, 86, 'I8', 8, 9),
(18380, 86, 'I7', 8, 10),
(18381, 86, 'I6', 8, 11),
(18382, 86, 'I5', 8, 12),
(18383, 86, 'I4', 8, 13),
(18384, 86, 'I3', 8, 14),
(18385, 86, 'I2', 8, 15),
(18386, 86, 'I1', 8, 16),
(18387, 86, 'L16', 9, 1),
(18388, 86, 'L15', 9, 2),
(18389, 86, 'L14', 9, 3),
(18390, 86, 'L13', 9, 4),
(18391, 86, 'L12', 9, 5),
(18392, 86, 'L11', 9, 6),
(18393, 86, 'L10', 9, 7),
(18394, 86, 'L9', 9, 8),
(18395, 86, 'L8', 9, 9),
(18396, 86, 'L7', 9, 10),
(18397, 86, 'L6', 9, 11),
(18398, 86, 'L5', 9, 12),
(18399, 86, 'L4', 9, 13),
(18400, 86, 'L3', 9, 14),
(18401, 86, 'L2', 9, 15),
(18402, 86, 'L1', 9, 16),
(18403, 86, 'M15', 10, 2),
(18404, 86, 'M14', 10, 3),
(18405, 86, 'M13', 10, 4),
(18406, 86, 'M12', 10, 5),
(18407, 86, 'M11', 10, 6),
(18408, 86, 'M10', 10, 7),
(18409, 86, 'M9', 10, 8),
(18410, 86, 'M8', 10, 9),
(18411, 86, 'M7', 10, 10),
(18412, 86, 'M6', 10, 11),
(18413, 86, 'M5', 10, 12),
(18414, 86, 'M4', 10, 13),
(18415, 86, 'M3', 10, 14),
(18416, 86, 'M2', 10, 15);

-- --------------------------------------------------------

--
-- Struttura della tabella `prenotazione`
--

DROP TABLE IF EXISTS `prenotazione`;
CREATE TABLE `prenotazione` (
  `ID` int(11) NOT NULL,
  `ID_utente` int(11) DEFAULT NULL,
  `ID_programmazione` int(11) DEFAULT NULL,
  `stato` int(11) NOT NULL,
  `data` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `prenotazione`
--

INSERT INTO `prenotazione` (`ID`, `ID_utente`, `ID_programmazione`, `stato`, `data`) VALUES
(139, 15, 1572, 1, '2018-06-29 17:30:41'),
(140, 15, 1574, 1, '2018-06-29 19:34:36'),
(142, 15, 1572, 1, '2018-07-01 19:34:23'),
(144, 15, 1478, 1, '2018-07-01 19:36:55');

-- --------------------------------------------------------

--
-- Struttura della tabella `prenotazione_posti`
--

DROP TABLE IF EXISTS `prenotazione_posti`;
CREATE TABLE `prenotazione_posti` (
  `ID` int(11) NOT NULL,
  `ID_prenotazione` int(11) DEFAULT NULL,
  `ID_posto` int(11) DEFAULT NULL,
  `ID_fascia_prezzo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `prenotazione_posti`
--

INSERT INTO `prenotazione_posti` (`ID`, `ID_prenotazione`, `ID_posto`, `ID_fascia_prezzo`) VALUES
(509, 139, 18003, 18),
(510, 139, 18004, 18),
(511, 139, 18005, 18),
(512, 139, 18006, 18),
(513, 139, 18007, 18),
(514, 139, 18008, 18),
(515, 139, 18009, 18),
(516, 139, 18010, 18),
(517, 139, 18011, 18),
(518, 139, 18012, 18),
(519, 139, 18013, 18),
(520, 139, 18014, 18),
(521, 139, 18015, 18),
(522, 139, 18016, 18),
(523, 139, 18017, 18),
(524, 139, 18018, 18),
(525, 139, 18019, 18),
(526, 139, 18020, 18),
(527, 139, 18021, 18),
(528, 139, 18022, 18),
(529, 139, 18023, 19),
(530, 139, 18024, 19),
(531, 139, 18025, 19),
(532, 139, 18026, 19),
(533, 140, 17992, 16),
(534, 140, 17993, 16),
(535, 140, 17994, 16),
(536, 140, 18016, 16),
(537, 140, 18017, 17),
(538, 140, 18018, 17),
(545, 142, 17939, 18),
(546, 142, 17940, 19),
(547, 142, 17963, 19),
(548, 142, 17964, 19),
(553, 144, 18395, 4),
(554, 144, 18396, 4),
(555, 144, 18410, 4),
(556, 144, 18411, 5);

-- --------------------------------------------------------

--
-- Struttura della tabella `programmazione`
--

DROP TABLE IF EXISTS `programmazione`;
CREATE TABLE `programmazione` (
  `ID` int(11) NOT NULL,
  `ID_film` int(11) DEFAULT NULL,
  `data_ora` datetime DEFAULT NULL,
  `ID_sala` int(11) DEFAULT NULL,
  `tipo` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `programmazione`
--

INSERT INTO `programmazione` (`ID`, `ID_film`, `data_ora`, `ID_sala`, `tipo`) VALUES
(1478, 507, '2018-07-03 20:30:00', 86, 'NORMALE'),
(1479, 507, '2018-07-03 21:15:00', 84, 'NORMALE'),
(1500, 507, '2018-05-02 22:15:00', 85, 'NORMALE'),
(1501, 508, '2018-07-02 20:15:00', 83, 'SPECIALE'),
(1521, 509, '2018-07-02 22:35:00', 83, 'NORMALE'),
(1533, 510, '2018-07-02 20:10:00', 82, 'NORMALE'),
(1540, 511, '2018-07-02 16:00:00', 86, 'NORMALE'),
(1542, 511, '2018-06-09 20:15:00', 82, 'NORMALE'),
(1543, 511, '2018-06-27 22:35:00', 84, 'SPECIALE'),
(1545, 511, '2018-06-10 22:35:00', 84, 'NORMALE'),
(1546, 513, '2018-07-02 16:20:00', 84, 'NORMALE'),
(1550, 513, '2018-08-02 22:30:00', 86, 'SPECIALE'),
(1551, 513, '2018-08-03 20:35:00', 82, 'SPECIALE'),
(1552, 508, '2018-06-19 20:30:00', 82, 'NORMALE'),
(1553, 508, '2018-06-22 17:45:00', 85, 'NORMALE'),
(1554, 507, '2018-06-21 10:30:00', 84, 'NORMALE'),
(1555, 509, '2018-06-20 20:30:00', 82, 'NORMALE'),
(1557, 538, '2018-08-02 11:35:00', 84, 'NORMALE'),
(1560, 507, '2018-06-28 03:00:00', 85, 'NORMALE'),
(1562, 513, '2018-06-21 10:30:00', 83, 'NORMALE'),
(1563, 542, '2018-08-25 10:30:00', 82, 'SPECIALE'),
(1566, 542, '2018-08-05 22:30:00', 86, 'SPECIALE'),
(1569, 538, '2018-07-03 22:30:00', 84, 'NORMALE'),
(1570, 538, '2018-06-26 12:30:00', 84, 'NORMALE'),
(1571, 538, '2018-07-03 14:30:00', 84, 'NORMALE'),
(1572, 538, '2018-07-03 22:28:00', 84, 'SPECIALE'),
(1573, 538, '2018-07-03 01:10:00', 84, 'SPECIALE'),
(1574, 538, '2018-07-03 01:12:00', 84, 'SPECIALE'),
(1575, 512, '2018-07-03 20:20:00', 83, 'SPECIALE');

-- --------------------------------------------------------

--
-- Struttura della tabella `programmazione_fascia`
--

DROP TABLE IF EXISTS `programmazione_fascia`;
CREATE TABLE `programmazione_fascia` (
  `ID` int(11) NOT NULL,
  `ID_programmazione` int(11) NOT NULL,
  `ID_fascia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `programmazione_fascia`
--

INSERT INTO `programmazione_fascia` (`ID`, `ID_programmazione`, `ID_fascia`) VALUES
(2, 1501, 7),
(8, 1550, 8),
(9, 1563, 9),
(10, 1566, 7),
(13, 1572, 9),
(14, 1573, 7),
(15, 1574, 8),
(16, 1551, 8),
(17, 1543, 9),
(18, 1575, 8);

-- --------------------------------------------------------

--
-- Struttura della tabella `sala`
--

DROP TABLE IF EXISTS `sala`;
CREATE TABLE `sala` (
  `ID` int(11) NOT NULL,
  `nome` text COLLATE utf8_bin,
  `num_posti` int(11) DEFAULT NULL,
  `num_file` int(11) DEFAULT NULL,
  `lunghezza_fila` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `sala`
--

INSERT INTO `sala` (`ID`, `nome`, `num_posti`, `num_file`, `lunghezza_fila`) VALUES
(82, 'Sala 3', 222, 10, 25),
(83, 'Sala 2', 221, 10, 25),
(84, 'Sala 1', 454, 18, 31),
(85, 'Sala 5', 211, 10, 23),
(86, 'Sala 4', 179, 11, 18);

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

DROP TABLE IF EXISTS `utente`;
CREATE TABLE `utente` (
  `ID` int(11) NOT NULL,
  `username` text COLLATE utf8_bin,
  `password` text COLLATE utf8_bin,
  `tipo` int(11) DEFAULT NULL,
  `nome` text COLLATE utf8_bin,
  `cognome` text COLLATE utf8_bin,
  `email` text COLLATE utf8_bin,
  `telefono` text COLLATE utf8_bin,
  `salt` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`ID`, `username`, `password`, `tipo`, `nome`, `cognome`, `email`, `telefono`, `salt`) VALUES
(1, 'admin', '6d094193321b5d76f78da7390bc7c889f141f588d0c9fa2e99863e486223c9a3', 2, 'Admin', 'Admin', 'multisalakingbot@gmail.com', '0309913670', 'DxpLQ'),
(3, 'Annamaria', '1a9a282223ece5d856a837035ad9fd9cbefec9de3a3112b504d46a218095dfb9', 0, 'Annamaria', 'Doyouknow', 'what@bootstrap.is', '12312312', NULL),
(6, 'chiaroono', '47e913c083dfce525dc2e5bfa21448da0b2eb92a38ef5241e0a0487affd3673f', 0, 'Rinaldo Mario', 'Colombo', 'rmc@unibs.it', '03765566778', NULL),
(11, 'ClaudiaXxX98', 'ca782d47f8f470495a82a82923241967b982a1b3dc19536dd86cc3bda99ab408', 0, 'Claudia', 'Claudia', 'aaasd@asdasdsa.cs.vf.dd.ds.za', '04567895671', '6wxIR'),
(12, 'Michele', '10f7f7e0b279d01fd116668e076077f9efa2e0bb6ea7d92c4c4f585c10c84f28', 2, 'Michele', 'Rizzo', 'mik3.rizzo@gmail.com', '3493958746', '8iqKf'),
(13, 'MastroPino', '7f156f26528898a3a44a99fb5f24d9e63802af4ec579d154516259b0933b498c', 0, 'Giuseppe', 'Rizzo', 'giusepperizzo63@hotmail.it', '3470457677', '0vE3V'),
(14, 'andrea', '036e95081d4145458178afbefe888f5bf6cac35ed73064e4c115f435f62cccaf', 0, 'andrea', 'di filippo', 'andreadifi@gmail.com', '3466326735', 'F1yrx'),
(15, 'Davide', 'f0173a11adac19509baebe7d9c4c3c7cc686b13b01839c2d68a7619dbd9cae86', 2, 'AAAA', 'BBBBC', 'davide.tosatto95@gmail.com', '', 'whfeS');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `chiavi_reset_password`
--
ALTER TABLE `chiavi_reset_password`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_utente` (`ID_utente`);

--
-- Indici per le tabelle `descrizione_prezzo`
--
ALTER TABLE `descrizione_prezzo`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `evento`
--
ALTER TABLE `evento`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `fascia`
--
ALTER TABLE `fascia`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `fascia_prezzo`
--
ALTER TABLE `fascia_prezzo`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_fascia` (`ID_fascia`),
  ADD KEY `fk_descrizione_prezzo` (`ID_descrizione_prezzo`);

--
-- Indici per le tabelle `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `id_film` (`ID`);

--
-- Indici per le tabelle `giorno_fascia`
--
ALTER TABLE `giorno_fascia`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_fascia` (`ID_fascia`);

--
-- Indici per le tabelle `posto`
--
ALTER TABLE `posto`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_sala` (`ID_sala`),
  ADD KEY `id_posto` (`ID`);

--
-- Indici per le tabelle `prenotazione`
--
ALTER TABLE `prenotazione`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_utente` (`ID_utente`),
  ADD KEY `ID_programmazione` (`ID_programmazione`);

--
-- Indici per le tabelle `prenotazione_posti`
--
ALTER TABLE `prenotazione_posti`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_prenotazione` (`ID_prenotazione`),
  ADD KEY `ID_posto` (`ID_posto`),
  ADD KEY `ID_fascia_prezzo` (`ID_fascia_prezzo`),
  ADD KEY `id_prenotazione_posti` (`ID_prenotazione`);

--
-- Indici per le tabelle `programmazione`
--
ALTER TABLE `programmazione`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_film` (`ID_film`),
  ADD KEY `ID_sala` (`ID_sala`),
  ADD KEY `id_programmazione` (`ID`);

--
-- Indici per le tabelle `programmazione_fascia`
--
ALTER TABLE `programmazione_fascia`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_programmazione` (`ID_programmazione`),
  ADD KEY `ID_fascia` (`ID_fascia`);

--
-- Indici per le tabelle `sala`
--
ALTER TABLE `sala`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `id_sala` (`ID`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `chiavi_reset_password`
--
ALTER TABLE `chiavi_reset_password`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT per la tabella `descrizione_prezzo`
--
ALTER TABLE `descrizione_prezzo`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `evento`
--
ALTER TABLE `evento`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT per la tabella `fascia`
--
ALTER TABLE `fascia`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT per la tabella `fascia_prezzo`
--
ALTER TABLE `fascia_prezzo`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT per la tabella `film`
--
ALTER TABLE `film`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=543;

--
-- AUTO_INCREMENT per la tabella `giorno_fascia`
--
ALTER TABLE `giorno_fascia`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT per la tabella `posto`
--
ALTER TABLE `posto`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18417;

--
-- AUTO_INCREMENT per la tabella `prenotazione`
--
ALTER TABLE `prenotazione`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- AUTO_INCREMENT per la tabella `prenotazione_posti`
--
ALTER TABLE `prenotazione_posti`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=557;

--
-- AUTO_INCREMENT per la tabella `programmazione`
--
ALTER TABLE `programmazione`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1576;

--
-- AUTO_INCREMENT per la tabella `programmazione_fascia`
--
ALTER TABLE `programmazione_fascia`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT per la tabella `sala`
--
ALTER TABLE `sala`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT per la tabella `utente`
--
ALTER TABLE `utente`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `chiavi_reset_password`
--
ALTER TABLE `chiavi_reset_password`
  ADD CONSTRAINT `chiavi_reset_password_ibfk_1` FOREIGN KEY (`ID_utente`) REFERENCES `utente` (`ID`);

--
-- Limiti per la tabella `fascia_prezzo`
--
ALTER TABLE `fascia_prezzo`
  ADD CONSTRAINT `fascia_prezzo_ibfk_1` FOREIGN KEY (`ID_fascia`) REFERENCES `fascia` (`ID`),
  ADD CONSTRAINT `fk_descrizione_prezzo` FOREIGN KEY (`ID_descrizione_prezzo`) REFERENCES `descrizione_prezzo` (`ID`) ON DELETE CASCADE;

--
-- Limiti per la tabella `giorno_fascia`
--
ALTER TABLE `giorno_fascia`
  ADD CONSTRAINT `giorno_fascia_ibfk_1` FOREIGN KEY (`ID_fascia`) REFERENCES `fascia` (`ID`);

--
-- Limiti per la tabella `posto`
--
ALTER TABLE `posto`
  ADD CONSTRAINT `posto_ibfk_1` FOREIGN KEY (`ID_sala`) REFERENCES `sala` (`ID`);

--
-- Limiti per la tabella `prenotazione`
--
ALTER TABLE `prenotazione`
  ADD CONSTRAINT `fk_programmazione` FOREIGN KEY (`ID_programmazione`) REFERENCES `programmazione` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `prenotazione_ibfk_1` FOREIGN KEY (`ID_utente`) REFERENCES `utente` (`ID`);

--
-- Limiti per la tabella `prenotazione_posti`
--
ALTER TABLE `prenotazione_posti`
  ADD CONSTRAINT `fk_prenotazione` FOREIGN KEY (`ID_prenotazione`) REFERENCES `prenotazione` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `prenotazione_posti_ibfk_2` FOREIGN KEY (`ID_posto`) REFERENCES `posto` (`ID`),
  ADD CONSTRAINT `prenotazione_posti_ibfk_3` FOREIGN KEY (`ID_fascia_prezzo`) REFERENCES `fascia_prezzo` (`ID`);

--
-- Limiti per la tabella `programmazione`
--
ALTER TABLE `programmazione`
  ADD CONSTRAINT `fk_film` FOREIGN KEY (`ID_film`) REFERENCES `film` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `programmazione_ibfk_2` FOREIGN KEY (`ID_sala`) REFERENCES `sala` (`ID`);

--
-- Limiti per la tabella `programmazione_fascia`
--
ALTER TABLE `programmazione_fascia`
  ADD CONSTRAINT `fk_programmazione_fascia_programmazione` FOREIGN KEY (`ID_programmazione`) REFERENCES `programmazione` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `programmazione_fascia_ibfk_2` FOREIGN KEY (`ID_fascia`) REFERENCES `fascia` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
