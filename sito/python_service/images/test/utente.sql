-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Creato il: Ott 15, 2018 alle 08:50
-- Versione del server: 10.1.23-MariaDB-9+deb9u1
-- Versione PHP: 7.0.30-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `multisalaking`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `ID` int(11) NOT NULL,
  `username` text COLLATE utf8_bin,
  `password` text COLLATE utf8_bin,
  `tipo` int(11) DEFAULT NULL,
  `nome` text COLLATE utf8_bin,
  `cognome` text COLLATE utf8_bin,
  `email` text COLLATE utf8_bin,
  `telefono` text COLLATE utf8_bin,
  `salt` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`ID`, `username`, `password`, `tipo`, `nome`, `cognome`, `email`, `telefono`, `salt`) VALUES
(1, 'admin', '6d094193321b5d76f78da7390bc7c889f141f588d0c9fa2e99863e486223c9a3', 2, 'Admin', 'Admin', 'multisalakingbot@gmail.com', '0309913670', 'DxpLQ'),
(3, 'Annamaria', '1a9a282223ece5d856a837035ad9fd9cbefec9de3a3112b504d46a218095dfb9', 0, 'Annamaria', 'Doyouknow', 'what@bootstrap.is', '12312312', NULL),
(6, 'chiaroono', '47e913c083dfce525dc2e5bfa21448da0b2eb92a38ef5241e0a0487affd3673f', 0, 'Rinaldo Mario', 'Colombo', 'rmc@unibs.it', '03765566778', NULL),
(11, 'ClaudiaXxX98', 'ca782d47f8f470495a82a82923241967b982a1b3dc19536dd86cc3bda99ab408', 0, 'Claudia', 'Claudia', 'aaasd@asdasdsa.cs.vf.dd.ds.za', '04567895671', '6wxIR'),
(12, 'Michele', '10f7f7e0b279d01fd116668e076077f9efa2e0bb6ea7d92c4c4f585c10c84f28', 2, 'Michele', 'Rizzo', 'mik3.rizzo@gmail.com', '3493958746', '8iqKf'),
(13, 'MastroPino', '7f156f26528898a3a44a99fb5f24d9e63802af4ec579d154516259b0933b498c', 0, 'Giuseppe', 'Rizzo', 'giusepperizzo63@hotmail.it', '3470457677', '0vE3V'),
(14, 'andrea', '036e95081d4145458178afbefe888f5bf6cac35ed73064e4c115f435f62cccaf', 0, 'andrea', 'di filippo', 'andreadifi@gmail.com', '3466326735', 'F1yrx'),
(15, 'Davide', 'cc2badae8ffe49aa1d3c190f701108d272e070cf9ffd64f531ea1ba746f8ab9e', 2, 'AAAA', 'BBBBC', 'davide.tosatto95@gmail.com', '', 'jvMVL'),
(16, 'Chris', '917e8910bed4b15256e4310a773f9019d904bbf6a043819caf91959b038120cd', 2, 'Christian', 'Fonga', 'christian.fonga@hotmail.it', '343343345', 'vhwbX'),
(17, 'elena', '63a5e3389a35545e0e754ee93432206d478f06e9ac5762ce8c2e1587522a3235', 0, 'elena', 'Fonga', 'elenafong@hotmail.it', '3400890789', 'VIEdD'),
(18, 'Giulia94', '90faed23d9b234d6c5ac7d3608907ff908a9da70c6b21e9754e1910a4925afde', 0, 'Giulia', 'Bignotti', 'giuly.bignotti@gmail.com', '3453536704', 'k0yCn');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `utente`
--
ALTER TABLE `utente`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
