from db_facade import get_all_bursts_IDs, get_db_connection, get_burst_labels_count, set_burst_labels_number

conn = get_db_connection()
cursor = conn.cursor()

bursts = get_all_bursts_IDs(cursor)

for bid in bursts:
    labeled_images = get_burst_labels_count(bid, cursor)
    set_burst_labels_number(bid, labeled_images, cursor)
    print "%s: %s\n" % (bid, labeled_images)

conn.commit()
cursor.close()
conn.close()