# coding=utf-8
from copy import deepcopy

import mysql
import mysql.connector as sqlconn
from datetime import datetime

from timer import Timer


def convert_row(row):
    def to_unicode(col):
        if type(col) == bytearray:
            return col.decode('utf-8')
        return col

    return [to_unicode(col) for col in row]


def get_db_connection():
    return sqlconn.connect(host="localhost",
                           user="sito",
                           passwd="SsCFcAnYg3TTaBa3",
                           database="segnali")


def db_interaction(body):
    conn = get_db_connection()
    cursor = conn.cursor(buffered=True)

    try:
        return body(conn, cursor)
    except Exception as ex:
        raise
    finally:
        conn.commit()
        cursor.close()
        conn.close()


def image_present(name, cursor):
    query = ("SELECT * FROM images "
             "WHERE name = %s")
    cursor.execute(query, (name,))
    num = 0
    for _ in cursor:
        num += 1
    return num > 0


def get_image_name(image_id, cursor):
    query = ("SELECT name FROM images "
             "WHERE ID = %s")

    cursor.execute(query, (image_id,))

    res = ""
    for row in cursor:
        res = convert_row(row)
    return res[0]


def get_label_info(label_type_id, label_value_id, cursor):
    query = "SELECT name, description, image FROM label_value WHERE ID_label_type = %s AND ID = %s"

    cursor.execute(query, (label_type_id, label_value_id))

    res = []
    for row in cursor:
        res = convert_row(row)
    return {
        "name": res[0],
        "description": res[1],
        "image": res[2]
    }


def get_label_type_tree(root_type_id, cursor, counts=False):
    tot_images = get_total_labeled_images(root_type_id, cursor)

    query = ("SELECT label_type.ID, label_type.name, label_type.description, label_value.ID, label_value.name, "
             "label_value.description, label_value.image, label_value.ID_label_subtype FROM `label_value` "
             "LEFT JOIN label_type ON ID_label_type = label_type.ID "
             "WHERE ID_label_type = %s "
             "ORDER BY relative_order")

    cursor.execute(query, (root_type_id,))

    res = {}
    for (i, row) in enumerate(cursor):

        row = convert_row(row)

        if i == 0:
            res["ID"] = row[0]
            res["name"] = row[1]
            res["description"] = row[2]
            res["values"] = []

        value = {"TID": root_type_id, "ID": row[3], "name": row[4], "description": row[5], "image": row[6],
                 "subtype": row[7]}

        res["values"].append(value)

    if "values" in res:
        for value in res["values"]:

            if counts:
                value["cnt"] = get_label_images_cnt(res["ID"], value["ID"], cursor)
                value["perc"] = (float(value["cnt"]) / tot_images) * 100
                value["predicted"] = get_label_images_predicted_count(res["ID"], value["ID"], cursor)

            if value["subtype"] is not None:
                value["subtype"] = get_label_type_tree(value["subtype"], cursor, counts=True)

    return res


def get_label_path_rec(tree, label_type, label_value):
    if tree is None:
        return None

    for value in tree["values"]:
        if value["TID"] == label_type and value["ID"] == label_value:
            return [[value["TID"], value["ID"]]]

        path = get_label_path_rec(value["subtype"], label_type, label_value)

        if path is not None:
            return [[value["TID"], value["ID"]]] + path

    return None


def get_label_path(root_label_type, label_type, label_value, cursor):
    tree = get_label_type_tree(root_label_type, cursor)

    return get_label_path_rec(tree, label_type, label_value)


def get_total_images(cursor):
    query = ("SELECT COUNT(*) FROM images ")

    cursor.execute(query)

    res = 0
    for row in cursor:
        res = convert_row(row)

    return res[0]


def get_total_labeled_images(label_type, cursor):
    query = ("SELECT COUNT(*) FROM labels WHERE type=%s ")

    cursor.execute(query, (label_type,))

    res = 0
    for row in cursor:
        res = convert_row(row)

    return res[0]


def get_total_images_with_label_value(label_type, label_value, cursor):
    query = ("SELECT COUNT(*) FROM labels WHERE type=%s AND value=%s ")

    cursor.execute(query, (label_type, label_value))

    res = 0
    for row in cursor:
        res = convert_row(row)

    return res[0]


def get_image_date(image_id, cursor):
    query = ("SELECT date_shot FROM images WHERE ID=%s")

    cursor.execute(query, (image_id,))

    res = 0
    for row in cursor:
        res = row

    return res[0]


def get_neighbors_from_date(date, seconds, cursor):
    query = ("SELECT ID FROM images "
             "WHERE date_shot BETWEEN %s AND DATE_ADD(%s, INTERVAL %s SECOND) "
             "ORDER BY date_shot ASC")

    cursor.execute(query, (date, date, seconds))

    res = []
    for row in cursor:
        res.append(row[0])

    return res


def get_neighbors_to_date(date, seconds, cursor):
    query = ("SELECT ID FROM images "
             "WHERE date_shot BETWEEN DATE_ADD(%s, INTERVAL %s SECOND) AND %s "
             "ORDER BY date_shot DESC")

    cursor.execute(query, (date, -seconds, date))

    res = []
    for row in cursor:
        res.append(row[0])

    return res


def get_all_images_with_names(cursor):
    query = ("SELECT ID, name FROM images")

    cursor.execute(query)

    res = []
    for row in cursor:
        res.append(convert_row(row))

    return res


def set_image_date(image_id, new_date, cursor):
    query = ("UPDATE images "
             "SET date_shot = %s "
             "WHERE ID = %s")

    cursor.execute(query, (new_date, image_id))


def delete_image(image_id, cursor):
    query = (" DELETE FROM images "
             " WHERE ID=%s ")

    cursor.execute(query, (image_id,))


def get_burst_id_by_image_id(image_id, cursor):
    query = ("SELECT ID_burst FROM burst_images "
             "WHERE ID_image = %s "
             "LIMIT 1")

    cursor.execute(query, (image_id,))

    res = None
    for row in cursor:
        res = row[0]

    return res


def get_all_bursts_labels_counts(cursor, label_type=1):
    query = (" SELECT ID_burst, count(*) FROM burst_images "
             " INNER JOIN labels "
             " ON burst_images.ID_image = labels.ID_image "
             " WHERE type=%s"
             " GROUP BY ID_burst")

    cursor.execute(query, (label_type,))

    res = {}

    for row in cursor.fetchall():
        bid = row[0]
        cnt = row[1]

        res[bid] = cnt

    return res


def get_burst_labels_count(burst_id, cursor, label_type=1):
    query = ("SELECT count(*) FROM burst_images "
             "INNER JOIN labels "
             "ON burst_images.ID_image = labels.ID_image "
             "WHERE ID_burst = %s AND type=%s")

    cursor.execute(query, (burst_id, label_type))

    res = 0
    for row in cursor:
        res = row[0]

    return res


def burst_has_labels(burst_id, cursor, min_labels=1, label_type=1):
    num_labels = get_burst_labels_count(burst_id, cursor, label_type=label_type)

    return num_labels >= min_labels


def add_image_to_burst(image_id, burst_id, cursor, label_type=1):
    query = ("INSERT INTO burst_images(ID_image, ID_burst) "
             "VALUES (%s, %s)")

    update_burst_labels_number(burst_id, cursor, label_type=label_type)

    cursor.execute(query, (image_id, burst_id))


def new_burst(cursor):
    query = ("INSERT INTO bursts() "
             "VALUES () ")

    cursor.execute(query)

    return cursor.lastrowid


def get_random_unlabeled_image(cursor, label_type=1):
    query = (" SELECT ID FROM images "
             " WHERE ID NOT IN "
             " (SELECT ID_image FROM labels WHERE type = %s) "
             " ORDER BY RAND() "
             " LIMIT 1 ")

    cursor.execute(query, (label_type,))

    res = None

    for row in cursor:
        res = convert_row(row)[0]

    return res


def get_average_burst_size(cursor):
    query = ("SELECT AVG(cnt) FROM "
             "(SELECT COUNT(*) AS cnt FROM burst_images GROUP BY ID_burst) AS counts")

    cursor.execute(query)

    res = 0
    for row in cursor:
        res = row[0]

    return res


def get_bursted_image_count(cursor):
    query = ("SELECT count(*) FROM burst_images")

    cursor.execute(query)

    res = 0
    for row in cursor:
        res = row[0]

    return res


def get_random_images_with_label(lbl_type, lbl_value, img_num, cursor):
    query = (" SELECT ID_image FROM labels "
             " WHERE  type=%s AND value=%s "
             " ORDER BY RAND() "
             " LIMIT %s ")

    cursor.execute(query, (lbl_type, lbl_value, img_num))

    res = []

    for row in cursor:
        res.append(convert_row(row)[0])

    return res


def get_label_images_cnt(lbl_type, lbl_value, cursor):
    query = (
        " SELECT count(*) FROM labels "
        " WHERE  type=%s AND value=%s "
    )

    cursor.execute(query, (lbl_type, lbl_value))

    res = 0

    for row in cursor:
        res = convert_row(row)[0]

    return res


def get_label_images(lbl_type, lbl_value, cursor):
    query = (
        " SELECT ID_image FROM labels "
        " WHERE  type=%s AND value=%s "
    )

    cursor.execute(query, (lbl_type, lbl_value))

    res = []

    for row in cursor:
        res.append(convert_row(row)[0])

    return res


def get_label_images_predicted_w_bursts(lbl_type, lbl_value, cursor, score_threshold=0.4,
                                        root_label_type=1, max_burst_labeled_images=1):
    query = (
        " SELECT min(scores.ID_image), max(value), min(bimg.ID_burst), "
        " IFNULL(max(labeled_images),0) as labimg, IFNULL(bimg.ID_burst, UUID()) as burst_gb FROM scores "
        " LEFT JOIN burst_images AS bimg ON scores.ID_image = bimg.ID_image "
        " LEFT JOIN bursts ON bimg.ID_burst = bursts.ID "
        " WHERE  ID_predicted_label_type=%s AND ID_predicted_label_value=%s AND "
        " scores.ID_image NOT IN (SELECT  ID_image FROM labels WHERE labels.type = %s) AND ID_score=4 AND "
        " value > %s AND (labeled_images < %s OR ISNULL(labeled_images)) AND "
        " scores.ID_image NOT IN (SELECT ID_image FROM labeling_queue) AND "
        " (bimg.ID_burst IS NULL OR bimg.ID_burst NOT IN (SELECT ID_burst FROM labeling_queue WHERE ID_burst IS NOT NULL)) "
        " GROUP BY burst_gb "
    )

    cursor.execute(query, (lbl_type, lbl_value, root_label_type, score_threshold, max_burst_labeled_images))

    res = []

    for row in cursor:
        res.append({
            "image_id": convert_row(row)[0],
            # TODO: add real bursts info
            "burst_id": convert_row(row)[2],
            "score": convert_row(row)[1]
        })

    path = get_label_path(root_label_type, lbl_type, lbl_value, cursor)

    for d in res:
        d["path"] = path

    return res


def get_label_images_predicted_count(lbl_type, lbl_value, cursor, score_threshold=0.4,
                                     root_label_type=1, max_burst_labeled_images=1):
    query = (
        " SELECT min(scores.ID_image), IFNULL(bimg.ID_burst, UUID()) as burst_gb FROM scores "
        " LEFT JOIN burst_images AS bimg ON scores.ID_image = bimg.ID_image "
        " LEFT JOIN bursts ON bimg.ID_burst = bursts.ID "
        " WHERE  ID_predicted_label_type=%s AND ID_predicted_label_value=%s AND "
        " scores.ID_image NOT IN (SELECT  ID_image FROM labels WHERE labels.type = %s) AND ID_score=4 AND "
        " value > %s AND (labeled_images < %s OR ISNULL(labeled_images)) AND"
        " scores.ID_image NOT IN (SELECT ID_image FROM labeling_queue) AND "
        " (bimg.ID_burst IS NULL OR bimg.ID_burst NOT IN (SELECT ID_burst FROM labeling_queue WHERE ID_burst IS NOT NULL)) "
        " GROUP BY burst_gb"
        # " LIMIT 1 "
    )

    cursor.execute(query, (lbl_type, lbl_value, root_label_type, score_threshold, max_burst_labeled_images))

    cnt = 0

    for row in cursor:
        cnt += 1

    return cnt


def get_label_images_w_bursts(lbl_type, lbl_value, cursor, score_id=1):
    query = (
        " SELECT labels.ID_image, ID_burst, scores.value FROM labels "
        " LEFT JOIN burst_images ON labels.ID_image = burst_images.ID_image "
        " LEFT JOIN scores ON scores.ID_image = labels.ID_image "
        " WHERE  type=%s AND labels.value=%s AND scores.ID_score=%s"
    )

    cursor.execute(query, (lbl_type, lbl_value, score_id))

    res = []

    for row in cursor:
        res.append({
            "image_id": convert_row(row)[0],
            "burst_id": convert_row(row)[1],
            "score": convert_row(row)[2]
        })

    return res


def add_image_score(ID_image, ID_score, value, cursor, ID_predicted_label_type=None, ID_predicted_label_value=None):
    query = ("INSERT INTO scores(ID_image, ID_score, value, ID_predicted_label_type, ID_predicted_label_value) "
             "VALUES (%s, %s, %s, %s, %s)")

    cursor.execute(query, (ID_image, ID_score, value, ID_predicted_label_type, ID_predicted_label_value))


def set_image_score(ID_image, ID_score, value, cursor, ID_predicted_label_type=None, ID_predicted_label_value=None):
    query = (" INSERT INTO scores(ID_image, ID_score, value, ID_predicted_label_type, ID_predicted_label_value) "
             " VALUES (%s, %s, %s, %s, %s) "
             " ON DUPLICATE KEY UPDATE "
             " value=%s, "
             " ID_predicted_label_type=%s, "
             " ID_predicted_label_value=%s, "
             " date=CURRENT_TIMESTAMP(6)")

    cursor.execute(query, (ID_image, ID_score, value, ID_predicted_label_type, ID_predicted_label_value,
                           value, ID_predicted_label_type, ID_predicted_label_value))


def get_all_unscored_images(ID_score, cursor):
    query = (" SELECT ID FROM images "
             " WHERE ID NOT IN "
             " (SELECT ID_image FROM scores WHERE ID_score = %s) ")

    cursor.execute(query, (ID_score,))

    res = []

    for row in cursor:
        res.append(convert_row(row)[0])

    return res


def get_all_images(cursor):
    query = (" SELECT ID FROM images ")

    cursor.execute(query)

    res = []

    for row in cursor.fetchall():
        res.append(convert_row(row)[0])

    return res


def get_image_score(ID_image, ID_score, cursor):
    query = (" SELECT value FROM scores "
             " WHERE ID_image = %s AND ID_score = %s")

    cursor.execute(query, (ID_image, ID_score))

    res = None

    for row in cursor:
        res = convert_row(row)[0]

    return res


def get_random_unlabeled_and_unbursted_image(cursor, trials=10, min_labels=1):
    img_id = None
    stop = False
    i = 0

    while not stop and i < trials:

        img_id = get_random_unlabeled_image(cursor)

        bid = get_burst_id_by_image_id(img_id, cursor)
        if bid is not None:
            if not burst_has_labels(bid, cursor, min_labels=min_labels):
                stop = True
        else:
            stop = True

        i += 1

    return img_id, stop, i


def get_random_image_with_score(ID_score, predicate, cursor, trials=10, min_labels=1):
    img_id = None
    stop = False
    i = 0

    while not stop and i < trials:

        img_id, _, _ = get_random_unlabeled_and_unbursted_image(cursor, trials, min_labels)

        score = get_image_score(img_id, ID_score, cursor)

        if score is not None and predicate(score):
            stop = True

        i += 1

    return img_id, stop, i


def get_random_image_with_score_efficient(ID_score, value_min, value_max, cursor,
                                          trials=100, min_labels=1, label_type=1, forbidden=None):
    # query = (" SELECT scores.ID_image FROM scores LEFT JOIN burst_images as bimg ON scores.ID_image = bimg.ID_image"
    #          " WHERE ID_score = %s AND value >= %s AND value <= %s"
    #          " AND scores.ID_image NOT IN (SELECT ID_image FROM labels WHERE type = %s)"
    #          " AND (SELECT count(*) FROM burst_images INNER JOIN labels ON burst_images.ID_image = labels.ID_image WHERE ID_burst = bimg.ID_burst AND type=%s) < %s"
    #          " ORDER BY rand() LIMIT %s ")

    query = (
        " SELECT scores.ID_image FROM scores LEFT JOIN burst_images AS bimg ON scores.ID_image = bimg.ID_image "
        " LEFT JOIN bursts ON bimg.ID_burst = bursts.ID"
        " WHERE ID_score = %s AND value >= %s AND value <= %s"
        " AND scores.ID_image NOT IN (SELECT ID_image FROM labels WHERE type = %s)"
        " AND (bursts.labeled_images < %s  OR bursts.ID IS NULL) "
        " LIMIT %s "
    )

    # Provare con outer join

    cursor.execute(query, (ID_score, value_min, value_max, label_type, min_labels, trials))

    res = None
    trials_made = 0
    if forbidden is None:
        forbidden = ForbiddenImages()

    for row in cursor.fetchall():
        img_id = convert_row(row)[0]

        if not forbidden.is_forbidden(img_id):
            res = img_id
            break
        else:
            trials_made += 1

    return res, res is not None, trials_made


def get_random_image_from_queue(cursor, trials=100, forbidden=None):
    query = (
        " SELECT ID_image FROM labeling_queue LIMIT %s "
    )

    cursor.execute(query, (trials,))

    res = None
    trials_made = 0
    if forbidden is None:
        forbidden = ForbiddenImages()

    for row in cursor.fetchall():
        img_id = convert_row(row)[0]

        if not forbidden.is_forbidden(img_id):
            res = img_id
            break
        else:
            trials_made += 1

    return res, res is not None, trials_made


SIGNAL_PREDICTION_SCORE_ID = 1
STRANGE_THING_SCORE_ID = 2


class ForbiddenImages:
    def __init__(self, limit=100):
        self._forbidden = []
        self._limit = limit

    def add_id(self, id):
        self._forbidden.append(id)

        if len(self._forbidden) > self._limit:
            del self._forbidden[0]

    def is_forbidden(self, id):
        return id in self._forbidden


FORBIDDEN_IMAGES = ForbiddenImages()


def get_image_for_human_classification(cursor, trials=3, min_burst_labels=1, is_signal_th=0.5, is_strange_th=0.7):
    img_id, stop, trials0 = get_random_image_from_queue(cursor, trials=trials)

    if stop:
        FORBIDDEN_IMAGES.add_id(img_id)
        return img_id, stop, trials0

    img_id, stop, trials1 = get_random_image_with_score(SIGNAL_PREDICTION_SCORE_ID, lambda s: s > is_signal_th,
                                                        cursor, trials=trials, min_labels=min_burst_labels)

    if stop:
        FORBIDDEN_IMAGES.add_id(img_id)
        return img_id, stop, trials1

    img_id, stop, trials2 = get_random_image_with_score(STRANGE_THING_SCORE_ID, lambda s: s > is_strange_th,
                                                        cursor, trials=trials, min_labels=min_burst_labels)

    if stop:
        FORBIDDEN_IMAGES.add_id(img_id)

    return img_id, stop, trials1 + trials2


def get_image_for_human_classification_efficient(cursor, label_type=1, trials=100, min_burst_labels=1,
                                                 signal_min=0.6, signal_max=1.1,
                                                 strange_min=0.6, strange_max=1.1):
    timer = Timer("HUMAN CLASSIFICATION EFFICIENT")

    img_id, stop, trials0 = get_random_image_from_queue(cursor, trials=trials, forbidden=FORBIDDEN_IMAGES)

    if stop:
        FORBIDDEN_IMAGES.add_id(img_id)
        return img_id, stop, trials0

    img_id, stop, trials1 = get_random_image_with_score_efficient(SIGNAL_PREDICTION_SCORE_ID, signal_min, signal_max,
                                                                  cursor, trials=trials, min_labels=min_burst_labels,
                                                                  label_type=label_type, forbidden=FORBIDDEN_IMAGES)
    timer.phase("q1")

    if img_id is not None:
        timer.print_times()
        FORBIDDEN_IMAGES.add_id(img_id)
        return img_id, stop, trials1

    img_id, stop, trials2 = get_random_image_with_score_efficient(STRANGE_THING_SCORE_ID, strange_min, strange_max,
                                                                  cursor, trials=trials, min_labels=min_burst_labels,
                                                                  label_type=label_type, forbidden=FORBIDDEN_IMAGES)

    timer.phase("q2")

    if img_id is not None:
        timer.print_times()
        FORBIDDEN_IMAGES.add_id(img_id)
        return img_id, stop, trials1 + trials2

    interesting = get_random_interesting_image(label_type, cursor, min_labels=min_burst_labels,
                                               forbidden=FORBIDDEN_IMAGES)

    timer.phase("q3")

    if interesting is not None:
        timer.print_times()
        FORBIDDEN_IMAGES.add_id(interesting)
        return interesting, False, trials0 + trials1 + trials2
    else:
        res = get_random_unlabeled_and_unbursted_image(cursor, trials=2)[0], False, trials1 + trials2 + 1

        timer.phase("q4")
        timer.print_times()

        return res


def get_random_interesting_image(label_type, cursor, min_labels=1, forbidden=FORBIDDEN_IMAGES, limit=100):
    query = (
        " SELECT * FROM ("
        " SELECT scores.ID_image FROM scores LEFT JOIN burst_images AS bimg ON scores.ID_image = bimg.ID_image "
        " LEFT JOIN bursts ON bimg.ID_burst = bursts.ID"
        " WHERE scores.ID_image NOT IN (SELECT ID_image FROM labels WHERE type = %s)"
        " AND bursts.labeled_images < %s"
        " LIMIT %s) AS T1 "
        " ORDER BY rand()"
    )

    cursor.execute(query, (label_type, min_labels, limit))

    res = None

    for row in cursor:
        img_id = convert_row(row)[0]

        if not forbidden.is_forbidden(img_id):
            res = img_id
            break

    return res


def get_burst_labeled_images_number(id_burst, cursor, label_type=1):
    query = (
        " SELECT count(*) FROM burst_images "
        " INNER JOIN labels ON burst_images.ID_image = labels.ID_image "
        " WHERE ID_burst = %s AND type=%s ")

    cursor.execute(query, (id_burst, label_type))

    res = None

    for row in cursor:
        res = convert_row(row)[0]

    return res


def set_burst_labels_number(burst_id, labeled_images, cursor):
    query = ("UPDATE bursts "
             "SET labeled_images=%s "
             "WHERE ID = %s")

    cursor.execute(query, (labeled_images, burst_id))


def update_burst_labels_number(burst_id, cursor, label_type=1):
    labeled_images = get_burst_labels_count(burst_id, cursor, label_type=label_type)
    set_burst_labels_number(burst_id, labeled_images, cursor)


def get_all_bursts_IDs(cursor):
    query = (" SELECT ID FROM bursts ")

    cursor.execute(query)

    res = []

    for row in cursor.fetchall():
        res.append(convert_row(row)[0])

    return res


def get_total_interesting_images_left(label_type, cursor, min_labels=1):
    query = (
        " SELECT count(*) FROM images LEFT JOIN burst_images AS bimg ON images.ID = bimg.ID_image "
        " LEFT JOIN bursts ON bimg.ID_burst = bursts.ID "
        " WHERE images.ID NOT IN (SELECT ID_image FROM labels WHERE type = %s)"
        " AND bursts.labeled_images < %s")

    cursor.execute(query, (label_type, min_labels))

    res = None

    for row in cursor:
        res = convert_row(row)[0]

    return res


def get_labeling_queue_length(cursor):
    query = (" SELECT count(*) FROM labeling_queue ")

    cursor.execute(query)

    res = None

    for row in cursor:
        res = convert_row(row)[0]

    return res


def add_image_to_labeling_queue(ID_image, cursor):
    bids = get_image_bursts(ID_image, cursor)

    if len(bids) == 0:

        query = (" INSERT INTO labeling_queue(ID_image) "
                 " VALUES (%s) ")

        cursor.execute(query, (ID_image,))

    else:

        query = (" INSERT INTO labeling_queue(ID_image, ID_burst) "
                 " VALUES (%s,%s) ")

        # TODO: fix: can have problems in case an image is inside multiple bursts (improbable)
        cursor.execute(query, (ID_image, bids[0]))


def delete_image_from_labeling_queue(ID_image, cursor):
    query = (" DELETE FROM labeling_queue "
             " WHERE ID_image=%s ")

    cursor.execute(query, (ID_image,))


def get_image_bursts(image_id, cursor):
    query = (
        " SELECT burst_images.ID_burst FROM images INNER JOIN burst_images ON burst_images.ID_image = images.ID "
        " WHERE images.ID = %s"
    )

    cursor.execute(query, (image_id,))

    res = []

    for row in cursor.fetchall():
        res.append(convert_row(row)[0])

    return res


def delete_all_image_labels(image_id, cursor):
    query = (" DELETE FROM labels WHERE ID_image=%s ")

    cursor.execute(query, (image_id,))

    update_image_bursts_labeled_images(image_id, cursor)


def update_image_bursts_labeled_images(image_id, cursor, label_type=1):
    bursts = get_image_bursts(image_id, cursor)

    for bid in bursts:
        update_burst_labels_number(bid, cursor, label_type=label_type)


def get_ordered_images_to_be_labeled(cursor, num=100, ID_score=5, root_label_type=1, max_burst_labeled_images=1):
    query = (
        " SELECT min(scores.ID_image), max(value) as maxv, min(bimg.ID_burst), "
        " IFNULL(max(labeled_images),0) as labimg, IFNULL(bimg.ID_burst, UUID()) as burst_gb, min(value)FROM scores "
        " LEFT JOIN burst_images AS bimg ON scores.ID_image = bimg.ID_image "
        " LEFT JOIN bursts ON bimg.ID_burst = bursts.ID "
        " WHERE scores.ID_image NOT IN (SELECT  ID_image FROM labels WHERE labels.type = %s) AND ID_score=%s "
        " AND (labeled_images < %s OR ISNULL(labeled_images)) AND "
        " scores.ID_image NOT IN (SELECT ID_image FROM labeling_queue) AND "
        " scores.ID_image NOT IN (SELECT ID_image FROM tabu) AND "
        " (bimg.ID_burst IS NULL OR bimg.ID_burst NOT IN (SELECT ID_burst FROM labeling_queue WHERE ID_burst IS NOT NULL)) AND "
        " (bimg.ID_burst IS NULL OR bimg.ID_burst NOT IN (SELECT ID_burst FROM tabu INNER JOIN burst_images ON tabu.ID_image = burst_images.ID_image WHERE ID_burst IS NOT NULL)) "
        " GROUP BY burst_gb "
        " ORDER BY maxv DESC "
        " LIMIT %s "
    )

    cursor.execute(query, (root_label_type, ID_score, max_burst_labeled_images, num))

    res = []

    for row in cursor:
        res.append({
            "image_id": convert_row(row)[0],
            "burst_id": convert_row(row)[2],
            "score": convert_row(row)[1]
        })

    tree = get_label_type_tree(root_label_type, cursor)

    for d in res:
        d["sig_perc"] = get_image_score(d["image_id"], 1, cursor)
        d["pred_perc"] = get_image_score(d["image_id"], 4, cursor)
        d["pred_labels"] = get_image_predicted_labels(d["image_id"], tree, cursor)

    return res


def add_image_predicted_label(ID_image, value, ID_predicted_label_type, ID_predicted_label_value, cursor):
    query = (" INSERT INTO predicted_labels (ID_image, value, ID_predicted_label_type, ID_predicted_label_value) "
             " VALUES (%s, %s, %s, %s) "
             " ON DUPLICATE KEY UPDATE "
             " value=%s, "
             " ID_predicted_label_type=%s, "
             " ID_predicted_label_value=%s, "
             " date=CURRENT_TIMESTAMP(6) ")

    cursor.execute(query, (ID_image, value, ID_predicted_label_type, ID_predicted_label_value,
                           value, ID_predicted_label_type, ID_predicted_label_value))


def add_multiple_image_predicted_labels(labels, cursor):
    for lbl in labels:
        lbl = deepcopy(lbl)
        lbl.append(cursor)

        if lbl[0] == 1:
            add_image_predicted_label(*lbl[1:])
        else:
            delete_all_image_predicted_labels(lbl[1], cursor)


def get_image_predicted_labels(image_id, tree, cursor, root_label_type=1):
    query = (
        " SELECT value, ID_predicted_label_type, ID_predicted_label_value FROM predicted_labels "
        " WHERE ID_image = %s "
        " ORDER BY value DESC "
    )

    cursor.execute(query, (image_id,))

    res = []

    for row in cursor.fetchall():
        row = convert_row(row)
        res.append({
            "value": row[0],
            "tid": row[1],
            "vid": row[2],
        })

    # tree = get_label_type_tree(root_label_type, cursor)

    for lbl in res:
        lbl["lbl_info"] = get_label_info(lbl["tid"], lbl["vid"], cursor)
        lbl["path"] = get_label_path_rec(tree, lbl["tid"], lbl["vid"])

    return res

def add_image_to_tabu(ID_image, cursor):
    query = (" INSERT INTO tabu(ID_image) "
             " VALUES (%s) ")

    cursor.execute(query, (ID_image,))

def clear_tabu_list(cursor, minutes=10):
    query = (" DELETE FROM tabu WHERE date < DATE_ADD(NOW(), INTERVAL -%s MINUTE) ")

    cursor.execute(query, (minutes,))

def delete_all_image_predicted_labels(ID_image, cursor):
    query = (" DELETE FROM predicted_labels WHERE ID_image = %s ")

    cursor.execute(query, (ID_image,))

#
# CREATE TABLE `predicted_labels` (
#  `ID` int(11) NOT NULL AUTO_INCREMENT,
#  `ID_image` int(11) NOT NULL,
#  `date` datetime(6) DEFAULT CURRENT_TIMESTAMP(6),
#  `value` double NOT NULL,
#  `ID_predicted_label_type` int(11) DEFAULT NULL,
#  `ID_predicted_label_value` int(11) DEFAULT NULL,
#  PRIMARY KEY (`ID`),
#  UNIQUE KEY `same_label_once` (`ID_image`,`ID_predicted_label_type`,`ID_predicted_label_value`) USING BTREE,
#  KEY `ID_image` (`ID_image`),
#  KEY `tid_index` (`ID_predicted_label_type`),
#  KEY `vid_index` (`ID_predicted_label_value`) USING BTREE,
#  KEY `idx_pred_value` (`value`) USING BTREE,
#  CONSTRAINT `predicted_labels_ibfk_2` FOREIGN KEY (`ID_image`) REFERENCES `images` (`ID`),
#  CONSTRAINT `predicted_labels_tid_fk` FOREIGN KEY (`ID_predicted_label_type`) REFERENCES `label_type` (`ID`),
#  CONSTRAINT `predicted_labels_vid_fk` FOREIGN KEY (`ID_predicted_label_value`) REFERENCES `label_value` (`ID`)
# ) ENGINE=InnoDB AUTO_INCREMENT=888414 DEFAULT CHARSET=utf8 COLLATE=utf8_bin
