import traceback

import numpy as np
import urllib
import cv2
import requests
import time

SERVICE_ADDRESS = "http://192.168.1.20:5000"

IMAGES_URL = SERVICE_ADDRESS + "/images/"
RANDOM_IMAGES_URL = SERVICE_ADDRESS + "/get_random_image"
RANDOM_UNBURSTED_IMAGES_URL = SERVICE_ADDRESS + "/get_random_unbursted_image"
NEIGHBORS_URL = SERVICE_ADDRESS + "/get_neighbors/"
NEIGHBORS_REV_URL = SERVICE_ADDRESS + "/get_neighbors_rev/"
ADD_BURST_URL = SERVICE_ADDRESS + "/add_burst"
LABELED_IMAGES = SERVICE_ADDRESS + "/get_random_labeled_images/"

# METHOD #1: OpenCV, NumPy, and urllib
def url_to_image(url):
    # download the image, convert it to a NumPy array, and then read
    # it into OpenCV format
    resp = urllib.urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)

    # return the image
    return image

def get_random_image_id():
    r = requests.get(RANDOM_IMAGES_URL)
    return r.json()["ID_image"]

def get_random_unbursted_image_id():
    r = requests.get(RANDOM_UNBURSTED_IMAGES_URL)
    return r.json()["ID_image"]

def get_img_by_id(id):
    return url_to_image(IMAGES_URL + str(id))

def get_labeled_images(lbl_type, lbl_value, img_num):

    full_path = LABELED_IMAGES + "%d/%d/%d" % (lbl_type, lbl_value, img_num)

    # r = requests.get(full_path)
    # print r.text

    r = requests.get(full_path)
    return r.json()["images"]
