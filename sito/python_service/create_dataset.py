import os
import pickle

import time
from numpy import random

import cv2
from service_access import get_labeled_images, get_img_by_id
import numpy as np

DATASET_FOLDER = r"/home/davide/datasets"

IMG_SIZE_X = 40
IMG_SIZE_Y = 40

N_IMG = 2500

ids = [(img_id, 1) for img_id in get_labeled_images(1, 1, N_IMG)]
ids.extend([(img_id, 0) for img_id in get_labeled_images(1, 2, N_IMG)])

print len(ids)

dataset = []

for index, (id, lbl) in enumerate(ids):

    print index, id, lbl

    err = True
    img = None
    while err:
        try:
            img = get_img_by_id(id)
        except Exception as ex:
            time.sleep(10)
            print "ERR"
        else:
            err = False

    img_resized = cv2.resize(img, (IMG_SIZE_X, IMG_SIZE_Y))
    # cv2.imshow('img-' + str(id) + "-orig", img_resized)
    #img_resized = cv2.cvtColor(img_resized, cv2.COLOR_BGR2GRAY)
    # cv2.imshow('img-' + str(id), (img_resized[:, :, 0] + img_resized[:, :, 1] + img_resized[:, :, 2])/3)

    img_resized = img_resized.astype(np.double)/255

    dataset.append((img_resized, lbl))

random.shuffle(dataset)

X = []
y = []

for img, lbl in dataset:
    print lbl

    X.append(img)
    y.append(lbl)


X = np.array(X).reshape(-1, IMG_SIZE_X, IMG_SIZE_Y, 3)

result = {"X": X, "y": y}

with open(os.path.join(DATASET_FOLDER, "dataset_color.pickle"), "wb") as output:

    pickle.dump(result, output)

# while cv2.waitKey(27) != 27:
#     pass


