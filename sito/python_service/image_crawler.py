import os
import mysql.connector as sqlconn
import requests
from bs4 import BeautifulSoup
import shutil

links = {
            "pericolo": "https://www.scuolaguida.it/Segnali-di-pericolo",
            "precedenza": "https://www.scuolaguida.it/Segnali-di-precedenza",
            "divieto": "https://www.scuolaguida.it/Segnali-di-divieto",
            "obbligo": "https://www.scuolaguida.it/Segnali-di-obbligo"
        }

category_id = {
            "pericolo": 6,
            "precedenza": 3,
            "divieto": 5,
            "obbligo": 4
}

base_address = "https://www.scuolaguida.it"
icons_folder = "images/icons"


def get_db_connection():
    return sqlconn.connect(  host="localhost",
                             user="sito",
                             passwd="SsCFcAnYg3TTaBa3",
                             database="segnali")

def add_label_value(icon, id_label_type, name, description, relative_order):

    conn = get_db_connection()
    cursor = conn.cursor()

    query = ("INSERT INTO label_value"
             "(name, description, image, ID_label_type, relative_order)"
             "VALUES (%s, %s, %s, %s, %s)")

    cursor.execute(query, (name, description, icon, id_label_type, relative_order))

    conn.commit()
    cursor.close()
    conn.close()


if __name__ == "__main__":

    for (cat, link) in links.iteritems():
        page = requests.get(link)
        soup = BeautifulSoup(page.content, 'html.parser')

        for (order, img) in enumerate(list(soup.find_all(class_="img"))):
            title = list(img.find_all(class_="desc"))[0].text

            title = title.split('-')[1].strip().lower().replace(" ", "_")

            filename = "%s-%s.jpg" % (cat, title)

            img_url = base_address + img.find("img")["src"]

            file_path = os.path.join(icons_folder, filename)

            r = requests.get(img_url, stream=True)
            if r.status_code == 200:
                with open(file_path, 'wb') as f:
                    r.raw.decode_content = True
                    shutil.copyfileobj(r.raw, f)

            add_label_value(filename, category_id[cat], title, title, order)

            print filename