from datetime import datetime, timedelta


class Timer:

    def __init__(self, name="Timer"):
        self._readings = {"___init___": datetime.now()}
        self._order = ["___init___"]
        self._name = name

    def phase(self, phase_name):
        self._readings[phase_name] = datetime.now()
        self._order.append(phase_name)

    def phase_len(self, phase_name):
        prev = self._readings[self._order[self._order.index(phase_name) - 1]]
        return self._readings[phase_name] - prev

    def print_times(self):

        print self._name

        tot = timedelta()

        for p in self._order:
            if p != "___init___":
                pl = self.phase_len(p)
                tot += pl
                print "Phase %s: %s, Cum: %s" % (p, pl, tot)

        print "Total: %s" % tot
