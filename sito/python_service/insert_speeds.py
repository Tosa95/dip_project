# coding=utf-8
import mysql.connector as sqlconn

def get_db_connection():
    return sqlconn.connect(  host="localhost",
                             user="sito",
                             passwd="SsCFcAnYg3TTaBa3",
                             database="segnali",
                             use_unicode=True)

def add_label_value(icon, id_label_type, name, description, relative_order):

    conn = get_db_connection()
    cursor = conn.cursor()

    query = ("INSERT INTO label_value"
             "(name, description, image, ID_label_type, relative_order)"
             "VALUES (%s, %s, %s, %s, %s)")

    cursor.execute(query, (name, description, icon, id_label_type, relative_order))

    conn.commit()
    cursor.close()
    conn.close()


for speed in [30,50,60,70,80,90,110,130]:

    name = "divieto-velocità_massima_consentita_%d" % speed

    add_label_value(name + ".jpg", 5, name, name, -(200-speed))