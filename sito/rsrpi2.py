from time import sleep

from general_utils.files_and_folders.remote_folder_sync import LocalToRemoteFolderSync

ltrfs = LocalToRemoteFolderSync(".", "/home/pi/segnali", "192.168.1.32", "pi", "porkanna2!")
try:
    ltrfs.start()
    while True:
        sleep(1)
finally:
    ltrfs.stop()