#!/bin/bash
docker stop csegnali || true
docker rm csegnali || true
docker run --name csegnali -it \
	                -p 8081:80 \
					-p 2222:22 \
					-p 5000:5000 \
					--stop-timeout 60 \
					--init \
					--restart unless-stopped \
					--mount source=segnali_db,destination=/var/lib/mysql \
					--mount source=segnali_img,destination=/python_service/images/signals \
					segnali