
To start as service:

	docker run --name csegnali -dt \
	                -p 8081:80 \
					-p 2222:22 \
					-p 5000:5000 \
					--stop-timeout 60 \
					--init \
					--restart unless-stopped \
					--mount source=segnali_db,destination=/var/lib/mysql \
					--mount source=segnali_img,destination=/var/www/html/segnali/images/signals \
					segnali
					
		Note:
		
			- segnali_db sarà il volume che contiene tutte le tabelle di mysql in formato raw
			- segnali_img sarà il volume che contiene tutte le immagini dei segnali caricate sul sito
			
			
To build:
	
	docker build \
					--build-arg MYSQL_PASSWORD="root" \
					--build-arg ROOT_PWD="root" \
					--build-arg REMOTE_IMAGES_SERVICE_PATH="http://localhost:5000" \
					--build-arg IMAGES_SERVICE_PATH="http://localhost:5000" \
					-t segnali .
					
	
	docker build \
					--build-arg MYSQL_PASSWORD="root" \
					--build-arg ROOT_PWD="root" \
					--build-arg REMOTE_IMAGES_SERVICE_PATH="http://192.168.1.32:5000" \
					--build-arg IMAGES_SERVICE_PATH="http://192.168.1.32:5000" \
					-t segnali .
