from time import sleep

from general_utils.files_and_folders.remote_folder_sync import LocalToRemoteFolderSync

ltrfs = LocalToRemoteFolderSync("sito_php", "/var/www/html/segnali", "192.168.1.32", "root", "root", remote_port=2222)
try:
    ltrfs.start()
    while True:
        sleep(1)
finally:
    ltrfs.stop()