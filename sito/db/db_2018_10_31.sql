-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Ott 31, 2018 alle 13:18
-- Versione del server: 10.1.31-MariaDB
-- Versione PHP: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `segnali`
--
CREATE DATABASE IF NOT EXISTS `segnali` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `segnali`;

-- --------------------------------------------------------

--
-- Struttura della tabella `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `date_shot` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `images`
--

INSERT INTO `images` (`ID`, `name`, `date_shot`) VALUES
(1922, 'cap-circle-2018-06-19_13_07_37.248476.jpg', '2018-06-19 13:07:37'),
(1923, 'cap-circle-2018-06-19_12_59_51.330724.jpg', '2018-06-19 12:59:51'),
(1924, 'cap-circle-2018-06-19_13_01_21.831428.jpg', '2018-06-19 13:01:21'),
(1925, 'cap-circle-2018-06-19_13_02_22.356184.jpg', '2018-06-19 13:02:22'),
(1926, 'cap-circle-2018-06-19_13_02_49.598948.jpg', '2018-06-19 13:02:49'),
(1927, 'cap-circle-2018-06-19_13_07_12.368936.jpg', '2018-06-19 13:07:12'),
(1928, 'cap-circle-2018-06-19_12_59_33.018116.jpg', '2018-06-19 12:59:33'),
(1929, 'cap-circle-2018-06-19_12_50_24.827325.jpg', '2018-06-19 12:50:24'),
(1930, 'cap-circle-2018-06-19_13_02_43.461727.jpg', '2018-06-19 13:02:43'),
(1931, 'cap-circle-2018-06-19_13_02_58.851194.jpg', '2018-06-19 13:02:58'),
(1932, 'cap-circle-2018-06-19_12_53_58.161478.jpg', '2018-06-19 12:53:58'),
(1933, 'cap-circle-2018-06-19_12_59_52.038203.jpg', '2018-06-19 12:59:52'),
(1934, 'cap-circle-2018-06-19_13_00_31.246579.jpg', '2018-06-19 13:00:31'),
(1935, 'cap-circle-2018-06-19_12_53_10.030020.jpg', '2018-06-19 12:53:10'),
(1936, 'cap-circle-2018-06-19_12_54_14.208363.jpg', '2018-06-19 12:54:14'),
(1937, 'cap-circle-2018-06-19_13_03_02.135436.jpg', '2018-06-19 13:03:02'),
(1938, 'cap-circle-2018-06-19_13_02_06.114460.jpg', '2018-06-19 13:02:06'),
(1939, 'cap-circle-2018-06-19_12_54_36.485021.jpg', '2018-06-19 12:54:36'),
(1940, 'cap-circle-2018-06-19_13_02_34.805439.jpg', '2018-06-19 13:02:34'),
(1941, 'cap-circle-2018-06-19_13_06_34.305368.jpg', '2018-06-19 13:06:34'),
(1942, 'cap-circle-2018-06-19_13_00_21.303578.jpg', '2018-06-19 13:00:21'),
(1943, 'cap-circle-2018-06-19_13_00_31.957257.jpg', '2018-06-19 13:00:31'),
(1944, 'cap-circle-2018-06-19_13_02_15.972172.jpg', '2018-06-19 13:02:15'),
(1945, 'cap-circle-2018-06-19_12_54_14.879792.jpg', '2018-06-19 12:54:14'),
(1946, 'cap-circle-2018-06-19_12_59_37.532173.jpg', '2018-06-19 12:59:37'),
(1947, 'cap-circle-2018-06-19_13_02_22.143572_-_Copia.jpg', '2018-06-19 13:02:22'),
(1948, 'cap-circle-2018-06-19_13_01_47.559238.jpg', '2018-06-19 13:01:47'),
(1949, 'cap-circle-2018-06-19_13_03_02.952588.jpg', '2018-06-19 13:03:02'),
(1950, 'cap-circle-2018-06-19_13_02_41.047185.jpg', '2018-06-19 13:02:41'),
(1951, 'cap-circle-2018-06-19_12_59_51.802936.jpg', '2018-06-19 12:59:51'),
(1952, 'cap-circle-2018-06-19_13_02_22.808592.jpg', '2018-06-19 13:02:22'),
(1953, 'cap-circle-2018-06-19_13_06_34.488492.jpg', '2018-06-19 13:06:34'),
(1954, 'cap-circle-2018-06-19_13_02_22.143572.jpg', '2018-06-19 13:02:22'),
(1955, 'cap-circle-2018-06-19_12_57_32.190368.jpg', '2018-06-19 12:57:32'),
(1956, 'cap-circle-2018-06-19_12_57_31.742129.jpg', '2018-06-19 12:57:31'),
(1957, 'cap-circle-2018-06-19_13_02_22.588816.jpg', '2018-06-19 13:02:22'),
(1958, 'cap-circle-2018-06-19_13_01_33.918256.jpg', '2018-06-19 13:01:33'),
(1959, 'cap-circle-2018-06-19_13_02_58.273309.jpg', '2018-06-19 13:02:58'),
(1960, 'cap-circle-2018-06-19_13_06_33.960823.jpg', '2018-06-19 13:06:33'),
(1961, 'cap-circle-2018-06-19_13_03_00.763059.jpg', '2018-06-19 13:03:00'),
(1962, 'cap-circle-2018-06-19_13_02_05.105677.jpg', '2018-06-19 13:02:05'),
(1963, 'cap-circle-2018-06-19_13_02_42.116707.jpg', '2018-06-19 13:02:42'),
(1964, 'cap-circle-2018-06-19_13_01_47.324088.jpg', '2018-06-19 13:01:47'),
(1965, 'cap-circle-2018-06-19_13_02_22.361927.jpg', '2018-06-19 13:02:22'),
(1966, 'cap-circle-2018-06-19_13_03_55.315726.jpg', '2018-06-19 13:03:55'),
(1967, 'cap-circle-2018-06-19_12_50_45.446437.jpg', '2018-06-19 12:50:45'),
(1968, 'cap-circle-2018-06-19_13_01_05.778476.jpg', '2018-06-19 13:01:05'),
(1969, 'cap-circle-2018-06-19_13_00_18.956622.jpg', '2018-06-19 13:00:18'),
(1970, 'cap-circle-2018-06-19_12_53_42.070473.jpg', '2018-06-19 12:53:42'),
(1971, 'cap-circle-2018-06-19_13_03_03.212702.jpg', '2018-06-19 13:03:03'),
(1972, 'cap-circle-2018-06-19_13_03_27.288088.jpg', '2018-06-19 13:03:27'),
(1973, 'cap-circle-2018-06-19_12_58_48.804757.jpg', '2018-06-19 12:58:48'),
(1974, 'cap-circle-2018-06-19_13_01_37.102604.jpg', '2018-06-19 13:01:37'),
(1975, 'cap-circle-2018-06-19_12_59_52.297636.jpg', '2018-06-19 12:59:52'),
(1976, 'cap-circle-2018-06-19_13_02_21.885632.jpg', '2018-06-19 13:02:21'),
(1977, 'cap-circle-2018-06-19_13_03_02.675001.jpg', '2018-06-19 13:03:02'),
(1978, 'cap-circle-2018-06-19_12_52_00.019109.jpg', '2018-06-19 12:52:00'),
(1979, 'cap-circle-2018-06-19_13_01_05.096917.jpg', '2018-06-19 13:01:05'),
(1980, 'cap-circle-2018-06-19_12_59_51.563189.jpg', '2018-06-19 12:59:51'),
(1981, 'cap-circle-2018-06-19_13_02_20.793265.jpg', '2018-06-19 13:02:20'),
(1982, 'cap-circle-2018-06-19_13_14_45.628577.jpg', '2018-06-19 13:14:45'),
(1983, 'cap-circle-2018-06-19_12_59_18.649047.jpg', '2018-06-19 12:59:18'),
(1984, 'cap-circle-2018-06-19_13_07_49.589872.jpg', '2018-06-19 13:07:49'),
(1985, 'cap-circle-2018-06-19_13_01_32.184353.jpg', '2018-06-19 13:01:32'),
(1986, 'cap-circle-2018-06-19_13_00_21.723331.jpg', '2018-06-19 13:00:21'),
(1987, 'cap-circle-2018-06-19_12_52_41.455795.jpg', '2018-06-19 12:52:41'),
(1988, 'cap-circle-2018-06-19_13_03_32.279461.jpg', '2018-06-19 13:03:32'),
(1989, 'cap-circle-2018-06-19_13_01_37.485939.jpg', '2018-06-19 13:01:37'),
(1990, 'cap-circle-2018-06-19_13_01_05.431159.jpg', '2018-06-19 13:01:05'),
(1991, 'cap-circle-2018-06-19_13_01_22.315485.jpg', '2018-06-19 13:01:22'),
(1992, 'cap-circle-2018-06-19_13_02_21.622251.jpg', '2018-06-19 13:02:21'),
(1993, 'cap-circle-2018-06-19_12_53_14.817846.jpg', '2018-06-19 12:53:14'),
(1994, 'cap-circle-2018-06-19_13_02_22.811576.jpg', '2018-06-19 13:02:22'),
(1995, 'cap-circle-2018-06-19_13_00_52.278020.jpg', '2018-06-19 13:00:52'),
(1996, 'cap-circle-2018-06-19_13_01_52.165705.jpg', '2018-06-19 13:01:52'),
(1997, 'cap-circle-2018-06-19_13_02_58.564256.jpg', '2018-06-19 13:02:58'),
(1998, 'cap-circle-2018-06-19_13_02_22.586104.jpg', '2018-06-19 13:02:22'),
(1999, 'cap-circle-2018-06-19_13_02_05.597658.jpg', '2018-06-19 13:02:05'),
(2000, 'cap-circle-2018-06-19_12_55_48.519405.jpg', '2018-06-19 12:55:48'),
(2001, 'cap-circle-2018-06-19_12_55_58.337283.jpg', '2018-06-19 12:55:58'),
(2002, 'cap-circle-2018-06-19_13_00_21.948082.jpg', '2018-06-19 13:00:21'),
(2003, 'cap-circle-2018-06-19_13_01_18.965953.jpg', '2018-06-19 13:01:18'),
(2004, 'cap-circle-2018-06-19_13_00_21.504509.jpg', '2018-06-19 13:00:21'),
(2005, 'cap-circle-2018-06-19_13_02_35.280832.jpg', '2018-06-19 13:02:35'),
(2006, 'cap-circle-2018-06-19_13_03_25.135810.jpg', '2018-06-19 13:03:25'),
(2007, 'cap-circle-2018-06-19_12_54_17.608818.jpg', '2018-06-19 12:54:17'),
(2008, 'cap-circle-2018-06-19_13_02_43.215441.jpg', '2018-06-19 13:02:43'),
(2009, 'cap-circle-2018-06-19_13_08_17.975066.jpg', '2018-06-19 13:08:17'),
(2010, 'cap-circle-2018-06-19_12_59_04.737301.jpg', '2018-06-19 12:59:04'),
(2011, 'cap-circle-2018-06-19_13_01_22.566722.jpg', '2018-06-19 13:01:22'),
(2012, 'cap-circle-2018-06-19_12_50_46.264583.jpg', '2018-06-19 12:50:46'),
(2013, 'cap-circle-2018-06-19_13_07_36.319872.jpg', '2018-06-19 13:07:36'),
(2014, 'cap-circle-2018-06-19_12_54_36.781366.jpg', '2018-06-19 12:54:36'),
(2015, 'cap-circle-2018-06-19_12_59_48.891689.jpg', '2018-06-19 12:59:48'),
(2016, 'cap-circle-2018-06-19_13_01_49.065222.jpg', '2018-06-19 13:01:49'),
(2017, 'cap-circle-2018-06-19_12_58_41.577947.jpg', '2018-06-19 12:58:41'),
(2018, 'cap-circle-2018-06-19_12_59_54.676826.jpg', '2018-06-19 12:59:54'),
(2019, 'cap-circle-2018-06-19_12_59_39.605273.jpg', '2018-06-19 12:59:39'),
(2020, 'cap-circle-2018-06-19_13_02_52.982784.jpg', '2018-06-19 13:02:52'),
(2021, 'cap-circle-2018-06-19_13_07_49.778358.jpg', '2018-06-19 13:07:49'),
(2022, 'cap-circle-2018-06-19_13_09_18.167071.jpg', '2018-06-19 13:09:18'),
(2023, 'cap-circle-2018-06-19_13_08_58.561527.jpg', '2018-06-19 13:08:58'),
(2024, 'cap-circle-2018-06-19_13_08_44.993222.jpg', '2018-06-19 13:08:44'),
(2025, 'cap-circle-2018-06-19_13_08_18.401538.jpg', '2018-06-19 13:08:18'),
(2026, 'cap-circle-2018-06-19_13_14_07.462157.jpg', '2018-06-19 13:14:07'),
(2027, 'cap-circle-2018-06-19_13_14_07.619824.jpg', '2018-06-19 13:14:07'),
(2028, 'cap-circle-2018-06-19_13_08_19.058869.jpg', '2018-06-19 13:08:19'),
(2029, 'cap-circle-2018-06-19_13_08_41.290801.jpg', '2018-06-19 13:08:41'),
(2030, 'cap-circle-2018-06-19_13_08_18.624108.jpg', '2018-06-19 13:08:18'),
(2031, 'cap-circle-2018-06-19_13_09_05.656996.jpg', '2018-06-19 13:09:05'),
(2032, 'cap-circle-2018-06-19_13_14_18.095613.jpg', '2018-06-19 13:14:18'),
(2033, 'cap-circle-2018-06-19_13_14_07.311759.jpg', '2018-06-19 13:14:07'),
(2034, 'cap-circle-2018-06-19_13_08_37.990239.jpg', '2018-06-19 13:08:37'),
(2035, 'cap-circle-2018-06-19_13_10_11.405519.jpg', '2018-06-19 13:10:11'),
(2036, 'cap-circle-2018-06-19_13_14_45.238734.jpg', '2018-06-19 13:14:45'),
(2037, 'cap-circle-2018-06-19_13_09_07.170623.jpg', '2018-06-19 13:09:07'),
(2038, 'cap-circle-2018-06-19_13_10_48.046456.jpg', '2018-06-19 13:10:48'),
(2039, 'cap-circle-2018-06-19_13_08_24.183980.jpg', '2018-06-19 13:08:24'),
(2040, 'cap-circle-2018-06-19_13_08_41.101968.jpg', '2018-06-19 13:08:41'),
(2041, 'cap-circle-2018-06-19_13_10_09.796633.jpg', '2018-06-19 13:10:09'),
(2042, 'cap-circle-2018-06-19_13_09_07.397639.jpg', '2018-06-19 13:09:07'),
(2043, 'cap-circle-2018-06-19_13_10_11.175156.jpg', '2018-06-19 13:10:11'),
(2044, 'cap-circle-2018-06-19_13_09_26.242720.jpg', '2018-06-19 13:09:26'),
(2045, 'cap-circle-2018-06-19_13_14_17.936482.jpg', '2018-06-19 13:14:17'),
(2046, 'cap-circle-2018-06-19_13_09_17.594647.jpg', '2018-06-19 13:09:17'),
(2047, 'cap-circle-2018-06-19_13_09_17.995247.jpg', '2018-06-19 13:09:17'),
(2048, 'cap-circle-2018-06-19_13_08_18.835441.jpg', '2018-06-19 13:08:18'),
(2049, 'cap-circle-2018-06-19_13_08_41.467710.jpg', '2018-06-19 13:08:41');

-- --------------------------------------------------------

--
-- Struttura della tabella `labels`
--

DROP TABLE IF EXISTS `labels`;
CREATE TABLE `labels` (
  `ID` int(11) NOT NULL,
  `ID_image` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `ID_user` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `label_type`
--

DROP TABLE IF EXISTS `label_type`;
CREATE TABLE `label_type` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `label_type`
--

INSERT INTO `label_type` (`ID`, `name`, `description`) VALUES
(1, 'segnale', 'Si tratta di un segnale?'),
(2, 'tipo_segnale', 'Di che tipologia di segnale di tratta?'),
(3, 'tipo_precedenza', 'Di che segnale di precedenza si tratta?'),
(4, 'tipo_obbligo', 'Di che segnale di obbligo si tratta?'),
(5, 'tipo_divieto', 'Di che segnale di divieto si tratta?'),
(6, 'tipo_pericolo', 'Di che segnale di pericolo si tratta?');

-- --------------------------------------------------------

--
-- Struttura della tabella `label_value`
--

DROP TABLE IF EXISTS `label_value`;
CREATE TABLE `label_value` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ID_label_type` int(11) NOT NULL,
  `ID_label_subtype` int(11) DEFAULT NULL,
  `relative_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `label_value`
--

INSERT INTO `label_value` (`ID`, `name`, `description`, `image`, `ID_label_type`, `ID_label_subtype`, `relative_order`) VALUES
(1, 'si', 'Si tratta di un segnale stradale', NULL, 1, 2, 1),
(2, 'no', 'Non si tratta di un segnale stradale', NULL, 1, NULL, 2),
(3, 'pericolo', 'Si tratta di un segnale di pericolo', 'pericolo-icona.jpg', 2, 6, 1),
(4, 'precedenza', 'Si tratta di un segnale di precedenza', 'precedenza-icona.jpg', 2, 3, 2),
(5, 'divieto', 'Si tratta di un segnale di divieto', 'divieto-divieto_di_transito.jpg', 2, 5, 3),
(6, 'obbligo', 'Si tratta di un segnale di obbligo', 'obbligo-icona.jpg', 2, 4, 4),
(7, 'divieto_di_transito', 'divieto_di_transito', 'divieto-divieto_di_transito.jpg', 5, NULL, 0),
(8, 'senso_vietato', 'senso_vietato', 'divieto-senso_vietato.jpg', 5, NULL, 1),
(9, 'divieto_di_sorpasso', 'divieto_di_sorpasso', 'divieto-divieto_di_sorpasso.jpg', 5, NULL, 2),
(10, 'distanza_di_sicurezza_minima', 'distanza_di_sicurezza_minima', 'divieto-distanza_di_sicurezza_minima.jpg', 5, NULL, 3),
(12, 'divieto_di_segnalazioni_acustiche', 'divieto_di_segnalazioni_acustiche', 'divieto-divieto_di_segnalazioni_acustiche.jpg', 5, NULL, 5),
(13, 'divieto_di_sorpasso_per_i_veicoli_di_massa_a_pieno_carico_superiore_a_3,5_tonnellate', 'divieto_di_sorpasso_per_i_veicoli_di_massa_a_pieno_carico_superiore_a_3,5_tonnellate', 'divieto-divieto_di_sorpasso_per_i_veicoli_di_massa_a_pieno_carico_superiore_a_3,5_tonnellate.jpg', 5, NULL, 6),
(14, 'divieto_di_transito_ai_veicoli_a_trazione_animale', 'divieto_di_transito_ai_veicoli_a_trazione_animale', 'divieto-divieto_di_transito_ai_veicoli_a_trazione_animale.jpg', 5, NULL, 7),
(15, 'divieto_di_transito_ai_pedoni', 'divieto_di_transito_ai_pedoni', 'divieto-divieto_di_transito_ai_pedoni.jpg', 5, NULL, 8),
(16, 'divieto_di_transito_ai_velocipedi', 'divieto_di_transito_ai_velocipedi', 'divieto-divieto_di_transito_ai_velocipedi.jpg', 5, NULL, 9),
(17, 'divieto_di_transito_ai_motocicli', 'divieto_di_transito_ai_motocicli', 'divieto-divieto_di_transito_ai_motocicli.jpg', 5, NULL, 10),
(18, 'divieto_di_transito_ai_veicoli_a_braccia', 'divieto_di_transito_ai_veicoli_a_braccia', 'divieto-divieto_di_transito_ai_veicoli_a_braccia.jpg', 5, NULL, 11),
(19, 'divieto_di_transito_a_tutti_gli_autoveicoli', 'divieto_di_transito_a_tutti_gli_autoveicoli', 'divieto-divieto_di_transito_a_tutti_gli_autoveicoli.jpg', 5, NULL, 12),
(20, 'divieto_di_transito_agli_autobus', 'divieto_di_transito_agli_autobus', 'divieto-divieto_di_transito_agli_autobus.jpg', 5, NULL, 13),
(21, 'divieto_di_transito_per_i_veicoli_di_massa_a_pieno_carico_superiore_a_3,5_tonnellate', 'divieto_di_transito_per_i_veicoli_di_massa_a_pieno_carico_superiore_a_3,5_tonnellate', 'divieto-divieto_di_transito_per_i_veicoli_di_massa_a_pieno_carico_superiore_a_3,5_tonnellate.jpg', 5, NULL, 14),
(22, 'divieto_di_transito_per_i_veicoli_di_massa_a_pieno_carico_superiore_a_quella_indicata', 'divieto_di_transito_per_i_veicoli_di_massa_a_pieno_carico_superiore_a_quella_indicata', 'divieto-divieto_di_transito_per_i_veicoli_di_massa_a_pieno_carico_superiore_a_quella_indicata.jpg', 5, NULL, 15),
(23, 'divieto_di_transito_ai_veicoli_a_motore_trainanti_un_rimorchio', 'divieto_di_transito_ai_veicoli_a_motore_trainanti_un_rimorchio', 'divieto-divieto_di_transito_ai_veicoli_a_motore_trainanti_un_rimorchio.jpg', 5, NULL, 16),
(24, 'divieto_di_transito_alle_macchine_agricole', 'divieto_di_transito_alle_macchine_agricole', 'divieto-divieto_di_transito_alle_macchine_agricole.jpg', 5, NULL, 17),
(25, 'divieto_di_transito_ai_veicoli_che_trasportano_merci_pericolose', 'divieto_di_transito_ai_veicoli_che_trasportano_merci_pericolose', 'divieto-divieto_di_transito_ai_veicoli_che_trasportano_merci_pericolose.jpg', 5, NULL, 18),
(26, 'divieto_di_transito_ai_veicoli_che_trasportano_esplosivi_o_merci_facilmente_infiammabili', 'divieto_di_transito_ai_veicoli_che_trasportano_esplosivi_o_merci_facilmente_infiammabili', 'divieto-divieto_di_transito_ai_veicoli_che_trasportano_esplosivi_o_merci_facilmente_infiammabili.jpg', 5, NULL, 19),
(27, 'divieto_di_transito_ai_veicoli_che_trasportano_prodotti_suscettibili_di_contaminare_l\'acqua', 'divieto_di_transito_ai_veicoli_che_trasportano_prodotti_suscettibili_di_contaminare_l\'acqua', 'divieto-divieto_di_transito_ai_veicoli_che_trasportano_prodotti_suscettibili_di_contaminare_l\'acqua.jpg', 5, NULL, 20),
(28, 'divieto_di_transito_ai_veicoli_aventi_larghezza_superiore_a_quella_indicata', 'divieto_di_transito_ai_veicoli_aventi_larghezza_superiore_a_quella_indicata', 'divieto-divieto_di_transito_ai_veicoli_aventi_larghezza_superiore_a_quella_indicata.jpg', 5, NULL, 21),
(29, 'divieto_di_transito_ai_veicoli_aventi_altezza_superiore_a_quella_indicata', 'divieto_di_transito_ai_veicoli_aventi_altezza_superiore_a_quella_indicata', 'divieto-divieto_di_transito_ai_veicoli_aventi_altezza_superiore_a_quella_indicata.jpg', 5, NULL, 22),
(30, 'divieto_di_transito_ai_veicoli_aventi_lunghezza_superiore_a_quella_indicata', 'divieto_di_transito_ai_veicoli_aventi_lunghezza_superiore_a_quella_indicata', 'divieto-divieto_di_transito_ai_veicoli_aventi_lunghezza_superiore_a_quella_indicata.jpg', 5, NULL, 23),
(31, 'divieto_di_transito_ai_veicoli_aventi_massa_superiore_a_quella_indicata', 'divieto_di_transito_ai_veicoli_aventi_massa_superiore_a_quella_indicata', 'divieto-divieto_di_transito_ai_veicoli_aventi_massa_superiore_a_quella_indicata.jpg', 5, NULL, 24),
(32, 'via_libera', 'via_libera', 'divieto-via_libera.jpg', 5, NULL, 25),
(33, 'fine_limitazione_velocità', 'fine_limitazione_velocità', 'divieto-fine_limitazione_velocità.jpg', 5, NULL, 26),
(34, 'fine_divieto_di_sorpasso', 'fine_divieto_di_sorpasso', 'divieto-fine_divieto_di_sorpasso.jpg', 5, NULL, 27),
(35, 'fine_divieto_di_sorpasso_per_i_veicoli_di_massa_a_pieno_carico_superiore_a_3,5_tonnellate', 'fine_divieto_di_sorpasso_per_i_veicoli_di_massa_a_pieno_carico_superiore_a_3,5_tonnellate', 'divieto-fine_divieto_di_sorpasso_per_i_veicoli_di_massa_a_pieno_carico_superiore_a_3,5_tonnellate.jpg', 5, NULL, 28),
(36, 'divieto_di_sosta', 'divieto_di_sosta', 'divieto-divieto_di_sosta.jpg', 5, NULL, 29),
(37, 'divieto_di_fermata', 'divieto_di_fermata', 'divieto-divieto_di_fermata.jpg', 5, NULL, 30),
(38, 'parcheggio_autorizzato', 'parcheggio_autorizzato', 'divieto-parcheggio_autorizzato.jpg', 5, NULL, 31),
(39, 'preavviso_parcheggio_autorizzato', 'preavviso_parcheggio_autorizzato', 'divieto-preavviso_parcheggio_autorizzato.jpg', 5, NULL, 32),
(40, 'passo_carrabile', 'passo_carrabile', 'divieto-passo_carrabile.jpg', 5, NULL, 33),
(41, 'sosta_consentita_a_particolari_categorie', 'sosta_consentita_a_particolari_categorie', 'divieto-sosta_consentita_a_particolari_categorie.jpg', 5, NULL, 34),
(42, 'regolazione_flessibile_della_sosta_in_centro_abitato', 'regolazione_flessibile_della_sosta_in_centro_abitato', 'divieto-regolazione_flessibile_della_sosta_in_centro_abitato.jpg', 5, NULL, 35),
(43, 'dare_precedenza', 'dare_precedenza', 'precedenza-dare_precedenza.jpg', 3, NULL, 0),
(44, 'fermarsi_e_dare_precedenza', 'fermarsi_e_dare_precedenza', 'precedenza-fermarsi_e_dare_precedenza.jpg', 3, NULL, 1),
(45, 'preavviso_di_dare_precedenza', 'preavviso_di_dare_precedenza', 'precedenza-preavviso_di_dare_precedenza.jpg', 3, NULL, 2),
(46, 'preavviso_di_fermarsi_e_dare_precedenza', 'preavviso_di_fermarsi_e_dare_precedenza', 'precedenza-preavviso_di_fermarsi_e_dare_precedenza.jpg', 3, NULL, 3),
(47, 'intersezione_con_precedenza_a_destra', 'intersezione_con_precedenza_a_destra', 'precedenza-intersezione_con_precedenza_a_destra.jpg', 3, NULL, 4),
(48, 'dare_precedenza_nei_sensi_unici_alternati', 'dare_precedenza_nei_sensi_unici_alternati', 'precedenza-dare_precedenza_nei_sensi_unici_alternati.jpg', 3, NULL, 5),
(49, 'fine_del_diritto_di_precedenza', 'fine_del_diritto_di_precedenza', 'precedenza-fine_del_diritto_di_precedenza.jpg', 3, NULL, 6),
(50, 'intersezione_con_diritto_di_precedenza', 'intersezione_con_diritto_di_precedenza', 'precedenza-intersezione_con_diritto_di_precedenza.jpg', 3, NULL, 7),
(51, 'intersezione_a_t_con_diritto_di_precedenza', 'intersezione_a_t_con_diritto_di_precedenza', 'precedenza-intersezione_a_t_con_diritto_di_precedenza.jpg', 3, NULL, 8),
(52, 'intersezione_a_t_con_diritto_di_precedenza', 'intersezione_a_t_con_diritto_di_precedenza', 'precedenza-intersezione_a_t_con_diritto_di_precedenza.jpg', 3, NULL, 9),
(53, 'confluenza_a_destra', 'confluenza_a_destra', 'precedenza-confluenza_a_destra.jpg', 3, NULL, 10),
(54, 'confluenza_da_sinistra', 'confluenza_da_sinistra', 'precedenza-confluenza_da_sinistra.jpg', 3, NULL, 11),
(55, 'diritto_di_precedenza', 'diritto_di_precedenza', 'precedenza-diritto_di_precedenza.jpg', 3, NULL, 12),
(56, 'diritto_di_precedenza_nei_sensi_unici_alternati', 'diritto_di_precedenza_nei_sensi_unici_alternati', 'precedenza-diritto_di_precedenza_nei_sensi_unici_alternati.jpg', 3, NULL, 13),
(57, 'direzione_obbligatoria_diritto', 'direzione_obbligatoria_diritto', 'obbligo-direzione_obbligatoria_diritto.jpg', 4, NULL, 0),
(58, 'direzione_obbligatoria_a_sinistra', 'direzione_obbligatoria_a_sinistra', 'obbligo-direzione_obbligatoria_a_sinistra.jpg', 4, NULL, 1),
(59, 'direzione_obbligatoria_a_destra', 'direzione_obbligatoria_a_destra', 'obbligo-direzione_obbligatoria_a_destra.jpg', 4, NULL, 2),
(60, 'preavviso_di_direzione_obbligatoria_a_destra', 'preavviso_di_direzione_obbligatoria_a_destra', 'obbligo-preavviso_di_direzione_obbligatoria_a_destra.jpg', 4, NULL, 3),
(61, 'preavviso_di_direzione_obbligatoria_a_sinistra', 'preavviso_di_direzione_obbligatoria_a_sinistra', 'obbligo-preavviso_di_direzione_obbligatoria_a_sinistra.jpg', 4, NULL, 4),
(62, 'direzioni_consentite_a_destra_e_sinistra', 'direzioni_consentite_a_destra_e_sinistra', 'obbligo-direzioni_consentite_a_destra_e_sinistra.jpg', 4, NULL, 5),
(63, 'direzioni_consentite_diritto_e_destra', 'direzioni_consentite_diritto_e_destra', 'obbligo-direzioni_consentite_diritto_e_destra.jpg', 4, NULL, 6),
(64, 'direzioni_consentite_diritto_e_sinistra', 'direzioni_consentite_diritto_e_sinistra', 'obbligo-direzioni_consentite_diritto_e_sinistra.jpg', 4, NULL, 7),
(65, 'passaggio_obbligatorio_a_sinistra', 'passaggio_obbligatorio_a_sinistra', 'obbligo-passaggio_obbligatorio_a_sinistra.jpg', 4, NULL, 8),
(66, 'passaggio_obbligatorio_a_destra', 'passaggio_obbligatorio_a_destra', 'obbligo-passaggio_obbligatorio_a_destra.jpg', 4, NULL, 9),
(67, 'passaggi_consentiti', 'passaggi_consentiti', 'obbligo-passaggi_consentiti.jpg', 4, NULL, 10),
(68, 'rotatoria', 'rotatoria', 'obbligo-rotatoria.jpg', 4, NULL, 11),
(69, 'limite_minimo_di_velocità', 'limite_minimo_di_velocità', 'obbligo-limite_minimo_di_velocità.jpg', 4, NULL, 12),
(70, 'fine_del_limite_minimo_di_velocità', 'fine_del_limite_minimo_di_velocità', 'obbligo-fine_del_limite_minimo_di_velocità.jpg', 4, NULL, 13),
(71, 'catene_per_neve_obbligatorie', 'catene_per_neve_obbligatorie', 'obbligo-catene_per_neve_obbligatorie.jpg', 4, NULL, 14),
(72, 'percorso_pedonale', 'percorso_pedonale', 'obbligo-percorso_pedonale.jpg', 4, NULL, 15),
(73, 'fine_del_percorso_pedonale', 'fine_del_percorso_pedonale', 'obbligo-fine_del_percorso_pedonale.jpg', 4, NULL, 16),
(74, 'pista_ciclabile', 'pista_ciclabile', 'obbligo-pista_ciclabile.jpg', 4, NULL, 17),
(75, 'fine_della_pista_ciclabile', 'fine_della_pista_ciclabile', 'obbligo-fine_della_pista_ciclabile.jpg', 4, NULL, 18),
(76, 'pista_ciclabile_affiancata_al_marciapiede', 'pista_ciclabile_affiancata_al_marciapiede', 'obbligo-pista_ciclabile_affiancata_al_marciapiede.jpg', 4, NULL, 19),
(77, 'pista_ciclabile_e_percorso_pedonale_promiscui', 'pista_ciclabile_e_percorso_pedonale_promiscui', 'obbligo-pista_ciclabile_e_percorso_pedonale_promiscui.jpg', 4, NULL, 20),
(78, 'fine_della_pista_ciclabile_affiancata_al_marciapiede', 'fine_della_pista_ciclabile_affiancata_al_marciapiede', 'obbligo-fine_della_pista_ciclabile_affiancata_al_marciapiede.jpg', 4, NULL, 21),
(79, 'fine_della_pista_ciclabile_e_del_percorso_pedonale_promiscui', 'fine_della_pista_ciclabile_e_del_percorso_pedonale_promiscui', 'obbligo-fine_della_pista_ciclabile_e_del_percorso_pedonale_promiscui.jpg', 4, NULL, 22),
(80, 'percorso_riservato_a_quadrupedi_da_soma_o_da_sella', 'percorso_riservato_a_quadrupedi_da_soma_o_da_sella', 'obbligo-percorso_riservato_a_quadrupedi_da_soma_o_da_sella.jpg', 4, NULL, 23),
(81, 'fine_del_percorso_riservato_a_quadrupedi_da_soma_o_da_sella', 'fine_del_percorso_riservato_a_quadrupedi_da_soma_o_da_sella', 'obbligo-fine_del_percorso_riservato_a_quadrupedi_da_soma_o_da_sella.jpg', 4, NULL, 24),
(82, 'obbligo_di_arresto_a_controllo_doganale_al_confine_con_uno_stato_extracomunitario', 'obbligo_di_arresto_a_controllo_doganale_al_confine_con_uno_stato_extracomunitario', 'obbligo-obbligo_di_arresto_a_controllo_doganale_al_confine_con_uno_stato_extracomunitario.jpg', 4, NULL, 25),
(83, 'obbligo_di_arresto_a_posto_di_blocco_istituito_da_organi_di_polizia,_senza_bisogno_che_mostrino_la_paletta', 'obbligo_di_arresto_a_posto_di_blocco_istituito_da_organi_di_polizia,_senza_bisogno_che_mostrino_la_paletta', 'obbligo-obbligo_di_arresto_a_posto_di_blocco_istituito_da_organi_di_polizia,_senza_bisogno_che_mostrino_la_paletta.jpg', 4, NULL, 26),
(84, 'obbligo_di_arresto_per_il_pagamento_di_pedaggi_autostradali', 'obbligo_di_arresto_per_il_pagamento_di_pedaggi_autostradali', 'obbligo-obbligo_di_arresto_per_il_pagamento_di_pedaggi_autostradali.jpg', 4, NULL, 27),
(85, 'strada_dissestata', 'strada_dissestata', 'pericolo-strada_dissestata.jpg', 6, NULL, 0),
(86, 'dosso', 'dosso', 'pericolo-dosso.jpg', 6, NULL, 1),
(87, 'cunetta', 'cunetta', 'pericolo-cunetta.jpg', 6, NULL, 2),
(88, 'curva_pericolosa_a_destra', 'curva_pericolosa_a_destra', 'pericolo-curva_pericolosa_a_destra.jpg', 6, NULL, 3),
(89, 'curva_pericolosa_a_sinistra', 'curva_pericolosa_a_sinistra', 'pericolo-curva_pericolosa_a_sinistra.jpg', 6, NULL, 4),
(90, 'serie_di_curve_pericolose,_la_prima_a_destra', 'serie_di_curve_pericolose,_la_prima_a_destra', 'pericolo-serie_di_curve_pericolose,_la_prima_a_destra.jpg', 6, NULL, 5),
(91, 'serie_di_curve_pericolose,_la_prima_a_sinistra', 'serie_di_curve_pericolose,_la_prima_a_sinistra', 'pericolo-serie_di_curve_pericolose,_la_prima_a_sinistra.jpg', 6, NULL, 6),
(92, 'passaggio_a_livello_con_barriere', 'passaggio_a_livello_con_barriere', 'pericolo-passaggio_a_livello_con_barriere.jpg', 6, NULL, 7),
(93, 'passaggio_a_livello_senza_barriere', 'passaggio_a_livello_senza_barriere', 'pericolo-passaggio_a_livello_senza_barriere.jpg', 6, NULL, 8),
(94, 'croce_di_sant\'andrea', 'croce_di_sant\'andrea', 'pericolo-croce_di_sant\'andrea.jpg', 6, NULL, 9),
(95, 'doppia_croce_di_sant_andrea', 'doppia_croce_di_sant_andrea', 'pericolo-doppia_croce_di_sant_andrea.jpg', 6, NULL, 10),
(96, 'croce_di_sant_andrea_installata_verticalmente', 'croce_di_sant_andrea_installata_verticalmente', 'pericolo-croce_di_sant_andrea_installata_verticalmente.jpg', 6, NULL, 11),
(97, 'pannelli_che_indicano_la_distanza_del_passaggio_a_livello', 'pannelli_che_indicano_la_distanza_del_passaggio_a_livello', 'pericolo-pannelli_che_indicano_la_distanza_del_passaggio_a_livello.jpg', 6, NULL, 12),
(98, 'attraversamento_ferroviario', 'attraversamento_ferroviario', 'pericolo-attraversamento_ferroviario.jpg', 6, NULL, 13),
(99, 'attraversamento_pedonale', 'attraversamento_pedonale', 'pericolo-attraversamento_pedonale.jpg', 6, NULL, 14),
(100, 'attraversamento_ciclabile', 'attraversamento_ciclabile', 'pericolo-attraversamento_ciclabile.jpg', 6, NULL, 15),
(101, 'discesa_pericolosa', 'discesa_pericolosa', 'pericolo-discesa_pericolosa.jpg', 6, NULL, 16),
(102, 'salita_ripida', 'salita_ripida', 'pericolo-salita_ripida.jpg', 6, NULL, 17),
(103, 'strettoia_simmetrica', 'strettoia_simmetrica', 'pericolo-strettoia_simmetrica.jpg', 6, NULL, 18),
(104, 'strettoia_asimmetrica_a_sinistra', 'strettoia_asimmetrica_a_sinistra', 'pericolo-strettoia_asimmetrica_a_sinistra.jpg', 6, NULL, 19),
(105, 'strettoia_asimmetrica_a_destra', 'strettoia_asimmetrica_a_destra', 'pericolo-strettoia_asimmetrica_a_destra.jpg', 6, NULL, 20),
(106, 'ponte_mobile', 'ponte_mobile', 'pericolo-ponte_mobile.jpg', 6, NULL, 21),
(107, 'banchina_pericolosa', 'banchina_pericolosa', 'pericolo-banchina_pericolosa.jpg', 6, NULL, 22),
(108, 'strada_sdrucciolevole', 'strada_sdrucciolevole', 'pericolo-strada_sdrucciolevole.jpg', 6, NULL, 23),
(109, 'bambini', 'bambini', 'pericolo-bambini.jpg', 6, NULL, 24),
(110, 'animali_domestici_vaganti', 'animali_domestici_vaganti', 'pericolo-animali_domestici_vaganti.jpg', 6, NULL, 25),
(111, 'animali_selvatici_vaganti', 'animali_selvatici_vaganti', 'pericolo-animali_selvatici_vaganti.jpg', 6, NULL, 26),
(112, 'doppio_senso_di_circolazione', 'doppio_senso_di_circolazione', 'pericolo-doppio_senso_di_circolazione.jpg', 6, NULL, 27),
(113, 'circolazione_rotatoria', 'circolazione_rotatoria', 'pericolo-circolazione_rotatoria.jpg', 6, NULL, 28),
(114, 'sbocco_su_molo_o_argine', 'sbocco_su_molo_o_argine', 'pericolo-sbocco_su_molo_o_argine.jpg', 6, NULL, 29),
(115, 'materiale_instabile_su_strada', 'materiale_instabile_su_strada', 'pericolo-materiale_instabile_su_strada.jpg', 6, NULL, 30),
(116, 'caduta_massi_da_sinistra', 'caduta_massi_da_sinistra', 'pericolo-caduta_massi_da_sinistra.jpg', 6, NULL, 31),
(117, 'caduta_massi_da_destra', 'caduta_massi_da_destra', 'pericolo-caduta_massi_da_destra.jpg', 6, NULL, 32),
(118, 'semaforo_verticale', 'semaforo_verticale', 'pericolo-semaforo_verticale.jpg', 6, NULL, 33),
(119, 'semaforo_orizzontale', 'semaforo_orizzontale', 'pericolo-semaforo_orizzontale.jpg', 6, NULL, 34),
(120, 'aeromobili', 'aeromobili', 'pericolo-aeromobili.jpg', 6, NULL, 35),
(121, 'forte_vento_laterale', 'forte_vento_laterale', 'pericolo-forte_vento_laterale.jpg', 6, NULL, 36),
(122, 'pericolo_di_incendio', 'pericolo_di_incendio', 'pericolo-pericolo_di_incendio.jpg', 6, NULL, 37),
(123, 'pericolo_generico', 'pericolo_generico', 'pericolo-pericolo_generico.jpg', 6, NULL, 38),
(164, 'divieto-velocità_massima_consentita_30', 'divieto-velocità_massima_consentita_30', 'divieto-velocità_massima_consentita_30.jpg', 5, NULL, -170),
(165, 'divieto-velocità_massima_consentita_50', 'divieto-velocità_massima_consentita_50', 'divieto-velocità_massima_consentita_50.jpg', 5, NULL, -150),
(166, 'divieto-velocità_massima_consentita_60', 'divieto-velocità_massima_consentita_60', 'divieto-velocità_massima_consentita_60.jpg', 5, NULL, -140),
(167, 'divieto-velocità_massima_consentita_70', 'divieto-velocità_massima_consentita_70', 'divieto-velocità_massima_consentita_70.jpg', 5, NULL, -130),
(168, 'divieto-velocità_massima_consentita_80', 'divieto-velocità_massima_consentita_80', 'divieto-velocità_massima_consentita_80.jpg', 5, NULL, -120),
(169, 'divieto-velocità_massima_consentita_90', 'divieto-velocità_massima_consentita_90', 'divieto-velocità_massima_consentita_90.jpg', 5, NULL, -110),
(170, 'divieto-velocità_massima_consentita_110', 'divieto-velocità_massima_consentita_110', 'divieto-velocità_massima_consentita_110.jpg', 5, NULL, -90),
(171, 'divieto-velocità_massima_consentita_130', 'divieto-velocità_massima_consentita_130', 'divieto-velocità_massima_consentita_130.jpg', 5, NULL, -70);

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

DROP TABLE IF EXISTS `utente`;
CREATE TABLE `utente` (
  `ID` int(11) NOT NULL,
  `username` text COLLATE utf8_bin,
  `password` text COLLATE utf8_bin,
  `tipo` int(11) DEFAULT NULL,
  `nome` text COLLATE utf8_bin,
  `cognome` text COLLATE utf8_bin,
  `email` text COLLATE utf8_bin,
  `telefono` text COLLATE utf8_bin,
  `salt` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`ID`, `username`, `password`, `tipo`, `nome`, `cognome`, `email`, `telefono`, `salt`) VALUES
(15, 'Davide', 'cc2badae8ffe49aa1d3c190f701108d272e070cf9ffd64f531ea1ba746f8ab9e', 2, 'AAAA', 'BBBBC', 'davide.tosatto95@gmail.com', '', 'jvMVL');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `labels`
--
ALTER TABLE `labels`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_image` (`ID_image`),
  ADD KEY `fk_user` (`ID_user`),
  ADD KEY `type` (`type`),
  ADD KEY `fk_value` (`value`);

--
-- Indici per le tabelle `label_type`
--
ALTER TABLE `label_type`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `label_value`
--
ALTER TABLE `label_value`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_label_type` (`ID_label_type`),
  ADD KEY `ID_label_subtype` (`ID_label_subtype`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `images`
--
ALTER TABLE `images`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2050;

--
-- AUTO_INCREMENT per la tabella `labels`
--
ALTER TABLE `labels`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=645;

--
-- AUTO_INCREMENT per la tabella `label_type`
--
ALTER TABLE `label_type`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `label_value`
--
ALTER TABLE `label_value`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;

--
-- AUTO_INCREMENT per la tabella `utente`
--
ALTER TABLE `utente`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `labels`
--
ALTER TABLE `labels`
  ADD CONSTRAINT `fk_type` FOREIGN KEY (`type`) REFERENCES `label_type` (`ID`),
  ADD CONSTRAINT `fk_user` FOREIGN KEY (`ID_user`) REFERENCES `utente` (`ID`),
  ADD CONSTRAINT `fk_value` FOREIGN KEY (`value`) REFERENCES `label_value` (`ID`),
  ADD CONSTRAINT `labels_ibfk_1` FOREIGN KEY (`ID_image`) REFERENCES `images` (`ID`);

--
-- Limiti per la tabella `label_value`
--
ALTER TABLE `label_value`
  ADD CONSTRAINT `label_value_ibfk_1` FOREIGN KEY (`ID_label_type`) REFERENCES `label_type` (`ID`),
  ADD CONSTRAINT `label_value_ibfk_2` FOREIGN KEY (`ID_label_subtype`) REFERENCES `label_type` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
