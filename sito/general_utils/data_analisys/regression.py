import numpy as np

def multi_variable_regression (X, Y):
    Y = np.array(Y)
    X = np.array(X)
    # X = X.T  # transpose so input vectors are along the rows
    X = np.c_[X, np.ones(X.shape[0])]  # add bias term
    result = np.linalg.lstsq(X, Y, rcond=None)[0]
    return result

def compute_regression_value(X, params):
    X = np.array(X)
    X = np.c_[X, np.ones(X.shape[0])]

    return np.dot(X, params)
