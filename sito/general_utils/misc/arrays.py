def get_longer_sequence_mean_index (lst, value):

    best_begin, best_end = get_longer_sequence_indices(lst, value)

    return float(best_end + best_begin)/2

def get_longer_sequence_indices (lst, value):

    curr_begin = curr_end = -1
    best_begin = best_end = -1

    in_sequence = False

    for i, v in enumerate(lst):

        if not in_sequence and v == value:
            in_sequence = True
            curr_begin = i
            curr_end = i

        if in_sequence and v == value:
            curr_end = i

        if in_sequence and v != value:
            in_sequence = False

            if best_begin == -1 or (curr_end - curr_begin > best_end - best_begin):
                best_begin = curr_begin
                best_end = curr_end

    if in_sequence and (best_begin == -1 or (curr_end - curr_begin > best_end - best_begin)):
        best_begin = curr_begin
        best_end = curr_end

    return best_begin, best_end
