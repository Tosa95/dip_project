from unittest import TestCase

from general_utils.misc.arrays import get_longer_sequence_mean_index, get_longer_sequence_indices


class TestGet_longer_sequence_mean_index(TestCase):

    def test_works(self):

        lst = [1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,1,1,1,1,1,3,3,3,3,3,13,8,8,8,8,8,123]

        self.assertEqual(get_longer_sequence_mean_index(lst, 2), 5.5)
        self.assertEqual(get_longer_sequence_mean_index(lst, 1), 22)
        self.assertEqual(get_longer_sequence_mean_index(lst, 3), 27)
        self.assertEqual(get_longer_sequence_mean_index(lst, 13), 30)
        self.assertEqual(get_longer_sequence_mean_index(lst, 123), 36)
