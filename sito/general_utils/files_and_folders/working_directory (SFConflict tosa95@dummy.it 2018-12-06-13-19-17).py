import os


def py_charm_set_working_directory_project_root():

    cwd = os.getcwd()

    while (".idea" not in os.listdir(cwd)):
        cwd = os.path.split(cwd)[0]

    os.chdir(cwd)