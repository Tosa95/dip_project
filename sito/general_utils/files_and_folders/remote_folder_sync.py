import os
from pprint import pprint
from time import sleep

import paramiko
import pysftp
from datetime import datetime

from general_utils.parallel.repeated_executor import RepeatedExecutor


def to_linux_path(path):
    return path.replace("\\", "/")

def get_path_relative_to_folder(root, path):
    if not path.startswith(root):
        raise Exception("The given path is not a subpath of root")

    return path[len(root)+1:]

def get_all_files_recursive(dir):

    res = []

    for directory_path, subdirs, files in os.walk(dir):
        for file in files:
            try:
                abs_path = os.path.join(directory_path, file)
                rel_path = get_path_relative_to_folder(dir, abs_path)
                res.append(
                    (abs_path, rel_path, os.path.getmtime(abs_path), True)
                )
            except Exception:
                pass
        for sdir in subdirs:
            try:
                abs_path = os.path.join(directory_path, sdir)
                rel_path = get_path_relative_to_folder(dir, abs_path)
                res.append(
                    (abs_path, rel_path, os.path.getmtime(abs_path), False)
                )
            except Exception:
                pass

    return res

def load_private_key(private_key_file):
    mykey = paramiko.RSAKey.from_private_key_file(private_key_file)
    transport = paramiko.Transport(('host', 'port'))
    transport.connect('username', pkey=mykey)
    sftp = paramiko.SFTPClient.from_transport(transport)

class LocalToRemoteFolderSync:

    def __init__(self, local_folder, remote_folder, remote_host, remote_uname, remote_pwd,
                 sync_every = 2.0, private_key_file=None, remote_port=22):
        self._local_folder = local_folder
        self._remote_folder = remote_folder
        self._remote_host = remote_host
        self._remote_uname = remote_uname
        self._remote_pwd = remote_pwd
        self._sync_every = sync_every
        self._private_key_file = private_key_file
        self._files_data = {}
        self._rexec = RepeatedExecutor(self.sync, sync_every)
        self._remote_port = remote_port

    def start(self):
        self._rexec.start()

    def stop(self):
        self._rexec.stop()

    def makedirs(self, dir, sftp):
        dirlist = dir.split("/")
        for i in xrange(len(dirlist)):
            dirpath = "/".join(dirlist[:i+1])
            try:
                sftp.mkdir(dirpath)
            except Exception as e:
                pass

    def upload_file(self, file, sftp):
        try:
            sftp.put(os.path.join(self._local_folder, file), to_linux_path(file))
            print os.path.join(self._local_folder, file), to_linux_path(file), datetime.now()
            return True
        except Exception as e:
            print e
            return False

    def make_dir(self, dir, sftp):
        self.makedirs(to_linux_path(dir), sftp)

    def remove_file(self, file):
        print "Remove! " + file

    def something_to_do(self, all_files):
        for abs, rel, time, is_file in all_files:
            if rel not in self._files_data or self._files_data[rel] < time:
                return True
        return False

    def sync(self):

        all_files = get_all_files_recursive(self._local_folder)

        if self.something_to_do(all_files):
            try:
                transport = paramiko.Transport((self._remote_host, self._remote_port))

                if self._private_key_file is None:
                    transport.connect(username=self._remote_uname, password=self._remote_pwd)
                else:
                    key = paramiko.RSAKey.from_private_key_file(self._private_key_file)
                    transport.connect(username=self._remote_uname, pkey=key)

                with paramiko.SFTPClient.from_transport(transport) as sftp:
                    sftp.chdir(self._remote_folder)

                    # Creating all directories
                    for abs, rel, time, is_file in all_files:
                        if rel not in self._files_data or self._files_data[rel] < time:
                            if not is_file:
                                self.make_dir(rel, sftp)
                                self._files_data[rel] = time

                    # Copying files
                    for abs, rel, time, is_file in all_files:
                        if rel not in self._files_data or self._files_data[rel] < time:
                            if is_file:
                                ok = self.upload_file(rel, sftp)
                                if ok:
                                    self._files_data[rel] = time
            except Exception as e:
                print e
        else:
            #print "Nothing to transfer"
            pass

if __name__=="__main__":
    ltrfs = LocalToRemoteFolderSync("..\\..", "/home/pi/test", "192.168.1.33", "pi", "porkanna2!")
    try:
        ltrfs.start()
        while True:
            sleep(1)
    finally:
        ltrfs.stop()
