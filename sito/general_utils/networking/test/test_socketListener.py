from _socket import AF_INET, SOCK_DGRAM, socket
from unittest import TestCase

import time

from general_utils.networking.broadcasting import SocketListener, send_dgram, send_broadcast_dgram


class TestSocketListener(TestCase):

    def test_base_dgram(self):

        recv_str = {"str": None}

        def recv(m):
            print m
            recv_str["str"] = m[0]

        sl = SocketListener(AF_INET, SOCK_DGRAM, 12345, recv)

        sl.start()

        time.sleep(0.1)

        send_dgram('test', '127.0.0.1', 12345)

        time.sleep(1)

        sl.stop()

        self.assertEqual(recv_str["str"], "test")

    def test_multiple_dgram(self):

        recv_str = {"str": ""}

        def recv(m):
            print m
            recv_str["str"] += m[0]

        sl = SocketListener(AF_INET, SOCK_DGRAM, 12345, recv)

        sl.start()

        time.sleep(0.1)

        send_dgram('test1', '127.0.0.1', 12345)

        time.sleep(0.1)

        send_dgram('test2', '127.0.0.1', 12345)

        time.sleep(0.1)

        sl.stop()

        self.assertEqual(recv_str["str"], "test1test2")

    def test_broadcast_dgram(self):

        recv_str = {"str": None}

        def recv(m):
            print m
            recv_str["str"] = m[0]

        sl = SocketListener(AF_INET, SOCK_DGRAM, 12345, recv)

        sl.start()

        time.sleep(0.1)

        send_broadcast_dgram('test', 12345)

        time.sleep(1)

        sl.stop()

        self.assertEqual(recv_str["str"], "test")
