from math import ceil


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def chunks_parts(l, n_parts):
    n = int(ceil(float(len(l))/n_parts))
    return chunks(l, n)