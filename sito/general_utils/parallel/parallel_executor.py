from Queue import Queue, Empty
from threading import Thread, Semaphore

from general_utils.parallel.utils import chunks_parts

BLOCK_TIMEOUT = 1

def extend_acc_func (chunks_results):

    result = []

    for chunk_result in chunks_results:
        result.extend(chunk_result)

    return result

class ThreadData(object):

    def __init__(self, chunk, elab_func, acc):
        self.chunk = chunk
        self.elab_func = elab_func
        self.acc = acc

class Accumulator(object):

    def __init__(self, chunks_left_cnt, acc_func=lambda chunks_results: False):
        self.done_semaphore = Semaphore(0)
        self.done = False
        self.result = None
        self.chunks_results = []
        self.acc_func = acc_func
        self.chunks_left = chunks_left_cnt
        self.chunk_finished_mutex = Semaphore(1)

    def chunk_finished(self, result):
        self.chunk_finished_mutex.acquire()
        self.chunks_results.append(result)

        self.chunks_left -= 1

        if self.chunks_left <= 0:
            self.result = self.acc_func(self.chunks_results)
            self.done = True
            self.done_semaphore.release()

        self.chunk_finished_mutex.release()

    def wait_for_completion(self):
        if not self.done:
            self.done_semaphore.acquire()

    def get_result(self):
        self.wait_for_completion()
        return self.result

class ParallelExecutor(object):

    def __init__(self, n_threads, block_timeout=BLOCK_TIMEOUT):
        self.in_queue = Queue()

        self.n_threads = n_threads
        self.threads = [Thread(target=self.thread_body) for _ in xrange(n_threads)]

        self._stop = False

        self.block_timeout = block_timeout

        for t in self.threads:
            t.start()

    def thread_body(self):

        while not self._stop:

            try:
                data = self.in_queue.get(timeout=self.block_timeout)
                chunk = data.chunk
                elab_func = data.elab_func
                acc = data.acc

                chunk_res = elab_func(chunk)

                acc.chunk_finished(chunk_res)

            except Empty:
                pass

    def elaborate_all(self, data, elaboration_func, accumulation_func):

        chunked_data = list(chunks_parts(data, self.n_threads))

        acc = Accumulator(len(chunked_data), accumulation_func)

        for c in chunked_data:
            self.in_queue.put(ThreadData(c, elaboration_func, acc))

        return acc.get_result()

    def stop(self):
        self._stop = True

        for t in self.threads:
            t.join()