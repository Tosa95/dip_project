from unittest import TestCase
from general_utils.parallel.utils import chunks, chunks_parts


class TestChunks_parts(TestCase):

    def test_chunks(self):
        data = range(10)
        chunked = list(chunks(data, 2))

        self.assertEqual(chunked, [[0,1], [2,3], [4,5], [6,7], [8,9]])

    def test_chunks_parts(self):
        data = range(10)
        chunked = list(chunks_parts(data, 2))

        self.assertEqual(chunked, [[0, 1, 2, 3, 4], [5, 6, 7, 8, 9]])
