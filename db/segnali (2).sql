-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Ott 19, 2018 alle 07:43
-- Versione del server: 10.1.31-MariaDB
-- Versione PHP: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `segnali`
--
CREATE DATABASE IF NOT EXISTS `segnali` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `segnali`;

-- --------------------------------------------------------

--
-- Struttura della tabella `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `images`
--

INSERT INTO `images` (`ID`, `name`) VALUES
(1793, 'cap-circle-2018-06-19_13_07_37.248476.jpg'),
(1794, 'cap-circle-2018-06-19_12_59_51.330724.jpg'),
(1795, 'cap-circle-2018-06-19_13_01_21.831428.jpg'),
(1796, 'cap-circle-2018-06-19_13_02_22.356184.jpg'),
(1797, 'cap-circle-2018-06-19_13_02_49.598948.jpg'),
(1798, 'cap-circle-2018-06-19_13_07_12.368936.jpg'),
(1799, 'cap-circle-2018-06-19_12_59_33.018116.jpg'),
(1800, 'cap-circle-2018-06-19_12_50_24.827325.jpg'),
(1801, 'cap-circle-2018-06-19_13_02_43.461727.jpg'),
(1802, 'cap-circle-2018-06-19_13_02_58.851194.jpg'),
(1803, 'cap-circle-2018-06-19_12_53_58.161478.jpg'),
(1804, 'cap-circle-2018-06-19_12_59_52.038203.jpg'),
(1805, 'cap-circle-2018-06-19_13_00_31.246579.jpg'),
(1806, 'cap-circle-2018-06-19_12_53_10.030020.jpg'),
(1807, 'cap-circle-2018-06-19_12_54_14.208363.jpg'),
(1808, 'cap-circle-2018-06-19_13_03_02.135436.jpg'),
(1809, 'cap-circle-2018-06-19_13_02_06.114460.jpg'),
(1810, 'cap-circle-2018-06-19_12_54_36.485021.jpg'),
(1811, 'cap-circle-2018-06-19_13_02_34.805439.jpg'),
(1812, 'cap-circle-2018-06-19_13_06_34.305368.jpg'),
(1813, 'cap-circle-2018-06-19_13_00_21.303578.jpg'),
(1814, 'cap-circle-2018-06-19_13_00_31.957257.jpg'),
(1815, 'cap-circle-2018-06-19_13_02_15.972172.jpg'),
(1816, 'cap-circle-2018-06-19_12_54_14.879792.jpg'),
(1817, 'cap-circle-2018-06-19_12_59_37.532173.jpg'),
(1818, 'cap-circle-2018-06-19_13_02_22.143572_-_Copia.jpg'),
(1819, 'cap-circle-2018-06-19_13_01_47.559238.jpg'),
(1820, 'cap-circle-2018-06-19_13_03_02.952588.jpg'),
(1821, 'cap-circle-2018-06-19_13_02_41.047185.jpg'),
(1822, 'cap-circle-2018-06-19_12_59_51.802936.jpg'),
(1823, 'cap-circle-2018-06-19_13_02_22.808592.jpg'),
(1824, 'cap-circle-2018-06-19_13_06_34.488492.jpg'),
(1825, 'cap-circle-2018-06-19_13_02_22.143572.jpg'),
(1826, 'cap-circle-2018-06-19_12_57_32.190368.jpg'),
(1827, 'cap-circle-2018-06-19_12_57_31.742129.jpg'),
(1828, 'cap-circle-2018-06-19_13_02_22.588816.jpg'),
(1829, 'cap-circle-2018-06-19_13_01_33.918256.jpg'),
(1830, 'cap-circle-2018-06-19_13_02_58.273309.jpg'),
(1831, 'cap-circle-2018-06-19_13_06_33.960823.jpg'),
(1832, 'cap-circle-2018-06-19_13_03_00.763059.jpg'),
(1833, 'cap-circle-2018-06-19_13_02_05.105677.jpg'),
(1834, 'cap-circle-2018-06-19_13_02_42.116707.jpg'),
(1835, 'cap-circle-2018-06-19_13_01_47.324088.jpg'),
(1836, 'cap-circle-2018-06-19_13_02_22.361927.jpg'),
(1837, 'cap-circle-2018-06-19_13_03_55.315726.jpg'),
(1838, 'cap-circle-2018-06-19_12_50_45.446437.jpg'),
(1839, 'cap-circle-2018-06-19_13_01_05.778476.jpg'),
(1840, 'cap-circle-2018-06-19_13_00_18.956622.jpg'),
(1841, 'cap-circle-2018-06-19_12_53_42.070473.jpg'),
(1842, 'cap-circle-2018-06-19_13_03_03.212702.jpg'),
(1843, 'cap-circle-2018-06-19_13_03_27.288088.jpg'),
(1844, 'cap-circle-2018-06-19_12_58_48.804757.jpg'),
(1845, 'cap-circle-2018-06-19_13_01_37.102604.jpg'),
(1846, 'cap-circle-2018-06-19_12_59_52.297636.jpg'),
(1847, 'cap-circle-2018-06-19_13_02_21.885632.jpg'),
(1848, 'cap-circle-2018-06-19_13_03_02.675001.jpg'),
(1849, 'cap-circle-2018-06-19_12_52_00.019109.jpg'),
(1850, 'cap-circle-2018-06-19_13_01_05.096917.jpg'),
(1851, 'cap-circle-2018-06-19_12_59_51.563189.jpg'),
(1852, 'cap-circle-2018-06-19_13_02_20.793265.jpg'),
(1853, 'cap-circle-2018-06-19_13_14_45.628577.jpg'),
(1854, 'cap-circle-2018-06-19_12_59_18.649047.jpg'),
(1855, 'cap-circle-2018-06-19_13_07_49.589872.jpg'),
(1856, 'cap-circle-2018-06-19_13_01_32.184353.jpg'),
(1857, 'cap-circle-2018-06-19_13_00_21.723331.jpg'),
(1858, 'cap-circle-2018-06-19_12_52_41.455795.jpg'),
(1859, 'cap-circle-2018-06-19_13_03_32.279461.jpg'),
(1860, 'cap-circle-2018-06-19_13_01_37.485939.jpg'),
(1861, 'cap-circle-2018-06-19_13_01_05.431159.jpg'),
(1862, 'cap-circle-2018-06-19_13_01_22.315485.jpg'),
(1863, 'cap-circle-2018-06-19_13_02_21.622251.jpg'),
(1864, 'cap-circle-2018-06-19_12_53_14.817846.jpg'),
(1865, 'cap-circle-2018-06-19_13_02_22.811576.jpg'),
(1866, 'cap-circle-2018-06-19_13_00_52.278020.jpg'),
(1867, 'cap-circle-2018-06-19_13_01_52.165705.jpg'),
(1868, 'cap-circle-2018-06-19_13_02_58.564256.jpg'),
(1869, 'cap-circle-2018-06-19_13_02_22.586104.jpg'),
(1870, 'cap-circle-2018-06-19_13_02_05.597658.jpg'),
(1871, 'cap-circle-2018-06-19_12_55_48.519405.jpg'),
(1872, 'cap-circle-2018-06-19_12_55_58.337283.jpg'),
(1873, 'cap-circle-2018-06-19_13_00_21.948082.jpg'),
(1874, 'cap-circle-2018-06-19_13_01_18.965953.jpg'),
(1875, 'cap-circle-2018-06-19_13_00_21.504509.jpg'),
(1876, 'cap-circle-2018-06-19_13_02_35.280832.jpg'),
(1877, 'cap-circle-2018-06-19_13_03_25.135810.jpg'),
(1878, 'cap-circle-2018-06-19_12_54_17.608818.jpg'),
(1879, 'cap-circle-2018-06-19_13_02_43.215441.jpg'),
(1880, 'cap-circle-2018-06-19_13_08_17.975066.jpg'),
(1881, 'cap-circle-2018-06-19_12_59_04.737301.jpg'),
(1882, 'cap-circle-2018-06-19_13_01_22.566722.jpg'),
(1883, 'cap-circle-2018-06-19_12_50_46.264583.jpg'),
(1884, 'cap-circle-2018-06-19_13_07_36.319872.jpg'),
(1885, 'cap-circle-2018-06-19_12_54_36.781366.jpg'),
(1886, 'cap-circle-2018-06-19_12_59_48.891689.jpg'),
(1887, 'cap-circle-2018-06-19_13_01_49.065222.jpg'),
(1888, 'cap-circle-2018-06-19_12_58_41.577947.jpg'),
(1889, 'cap-circle-2018-06-19_12_59_54.676826.jpg'),
(1890, 'cap-circle-2018-06-19_12_59_39.605273.jpg'),
(1891, 'cap-circle-2018-06-19_13_02_52.982784.jpg'),
(1892, 'cap-circle-2018-06-19_13_07_49.778358.jpg'),
(1893, 'cap-circle-2018-06-19_13_09_18.167071.jpg'),
(1894, 'cap-circle-2018-06-19_13_08_58.561527.jpg'),
(1895, 'cap-circle-2018-06-19_13_08_44.993222.jpg'),
(1896, 'cap-circle-2018-06-19_13_08_18.401538.jpg'),
(1897, 'cap-circle-2018-06-19_13_14_07.462157.jpg'),
(1898, 'cap-circle-2018-06-19_13_14_07.619824.jpg'),
(1899, 'cap-circle-2018-06-19_13_08_19.058869.jpg'),
(1900, 'cap-circle-2018-06-19_13_08_41.290801.jpg'),
(1901, 'cap-circle-2018-06-19_13_08_18.624108.jpg'),
(1902, 'cap-circle-2018-06-19_13_09_05.656996.jpg'),
(1903, 'cap-circle-2018-06-19_13_14_18.095613.jpg'),
(1904, 'cap-circle-2018-06-19_13_14_07.311759.jpg'),
(1905, 'cap-circle-2018-06-19_13_08_37.990239.jpg'),
(1906, 'cap-circle-2018-06-19_13_10_11.405519.jpg'),
(1907, 'cap-circle-2018-06-19_13_14_45.238734.jpg'),
(1908, 'cap-circle-2018-06-19_13_09_07.170623.jpg'),
(1909, 'cap-circle-2018-06-19_13_10_48.046456.jpg'),
(1910, 'cap-circle-2018-06-19_13_08_24.183980.jpg'),
(1911, 'cap-circle-2018-06-19_13_08_41.101968.jpg'),
(1912, 'cap-circle-2018-06-19_13_10_09.796633.jpg'),
(1913, 'cap-circle-2018-06-19_13_09_07.397639.jpg'),
(1914, 'cap-circle-2018-06-19_13_10_11.175156.jpg'),
(1915, 'cap-circle-2018-06-19_13_09_26.242720.jpg'),
(1916, 'cap-circle-2018-06-19_13_14_17.936482.jpg'),
(1917, 'cap-circle-2018-06-19_13_09_17.594647.jpg'),
(1918, 'cap-circle-2018-06-19_13_09_17.995247.jpg'),
(1919, 'cap-circle-2018-06-19_13_08_18.835441.jpg'),
(1920, 'cap-circle-2018-06-19_13_08_41.467710.jpg');

-- --------------------------------------------------------

--
-- Struttura della tabella `labels`
--

DROP TABLE IF EXISTS `labels`;
CREATE TABLE `labels` (
  `ID` int(11) NOT NULL,
  `ID_image` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `ID_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `label_type`
--

DROP TABLE IF EXISTS `label_type`;
CREATE TABLE `label_type` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `label_type`
--

INSERT INTO `label_type` (`ID`, `name`, `description`) VALUES
(1, 'segnale', 'Si tratta di un segnale?'),
(2, 'tipo_segnale', 'Di che tipologia di segnale di tratta?');

-- --------------------------------------------------------

--
-- Struttura della tabella `label_value`
--

DROP TABLE IF EXISTS `label_value`;
CREATE TABLE `label_value` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ID_label_type` int(11) NOT NULL,
  `ID_label_subtype` int(11) DEFAULT NULL,
  `relative_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `label_value`
--

INSERT INTO `label_value` (`ID`, `name`, `description`, `image`, `ID_label_type`, `ID_label_subtype`, `relative_order`) VALUES
(1, 'si', 'Si tratta di un segnale stradale', NULL, 1, 2, 1),
(2, 'no', 'Non si tratta di un segnale stradale', NULL, 1, NULL, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

DROP TABLE IF EXISTS `utente`;
CREATE TABLE `utente` (
  `ID` int(11) NOT NULL,
  `username` text COLLATE utf8_bin,
  `password` text COLLATE utf8_bin,
  `tipo` int(11) DEFAULT NULL,
  `nome` text COLLATE utf8_bin,
  `cognome` text COLLATE utf8_bin,
  `email` text COLLATE utf8_bin,
  `telefono` text COLLATE utf8_bin,
  `salt` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`ID`, `username`, `password`, `tipo`, `nome`, `cognome`, `email`, `telefono`, `salt`) VALUES
(15, 'Davide', 'cc2badae8ffe49aa1d3c190f701108d272e070cf9ffd64f531ea1ba746f8ab9e', 2, 'AAAA', 'BBBBC', 'davide.tosatto95@gmail.com', '', 'jvMVL');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `labels`
--
ALTER TABLE `labels`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_image` (`ID_image`),
  ADD KEY `fk_user` (`ID_user`),
  ADD KEY `type` (`type`),
  ADD KEY `fk_value` (`value`);

--
-- Indici per le tabelle `label_type`
--
ALTER TABLE `label_type`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `label_value`
--
ALTER TABLE `label_value`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_label_type` (`ID_label_type`),
  ADD KEY `ID_label_subtype` (`ID_label_subtype`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `images`
--
ALTER TABLE `images`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1921;

--
-- AUTO_INCREMENT per la tabella `labels`
--
ALTER TABLE `labels`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `label_type`
--
ALTER TABLE `label_type`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `label_value`
--
ALTER TABLE `label_value`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `utente`
--
ALTER TABLE `utente`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `labels`
--
ALTER TABLE `labels`
  ADD CONSTRAINT `fk_type` FOREIGN KEY (`type`) REFERENCES `label_type` (`ID`),
  ADD CONSTRAINT `fk_user` FOREIGN KEY (`ID_user`) REFERENCES `utente` (`ID`),
  ADD CONSTRAINT `fk_value` FOREIGN KEY (`value`) REFERENCES `label_value` (`ID`),
  ADD CONSTRAINT `labels_ibfk_1` FOREIGN KEY (`ID_image`) REFERENCES `images` (`ID`);

--
-- Limiti per la tabella `label_value`
--
ALTER TABLE `label_value`
  ADD CONSTRAINT `label_value_ibfk_1` FOREIGN KEY (`ID_label_type`) REFERENCES `label_type` (`ID`),
  ADD CONSTRAINT `label_value_ibfk_2` FOREIGN KEY (`ID_label_subtype`) REFERENCES `label_type` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
