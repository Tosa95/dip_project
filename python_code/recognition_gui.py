import os
import sys
import cv2
from PyQt4 import QtGui, QtCore
import numpy as np
import config
from async.async_image_copy import AsyncImageCopier
from async.async_recognition_multicore import MulticoreAsyncRecognitor
from config import SAVE_IMAGES, set_save_images, FULL_SCREEN, SAVE_FULL_FRAMES
from lib.img_utils import resize_to_space


class Window(QtGui.QMainWindow):

    def __init__(self, full_screen=FULL_SCREEN):
        super(Window, self).__init__()
        self.setGeometry(0,0,700,400)
        self.setWindowTitle("Panda recognitor")
        self.setWindowIcon(QtGui.QIcon('pythonlogo.png'))
        self.last_seen = None
        self.current = None
        self.progress = None
        self.errors = None
        self.recognition_button = None
        self.upload_button = None
        self.recognitor = None
        self.copier = None
        self.save = SAVE_IMAGES
        self.save_full_frames = SAVE_FULL_FRAMES
        self.fulls_screen = full_screen
        self.home()

    def shutdown(self):
        os.system('shutdown now')

    def get_button(self, text):
        btn = QtGui.QPushButton(text)
        btn.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        return btn

    def cv2_image_to_qt(self, cv2img, width):

        cv2img = resize_to_space(cv2img.copy(), width)

        height, width, byteValue = cv2img.shape
        byteValue = byteValue * width

        cv2.cvtColor(cv2img, cv2.COLOR_BGR2RGB, cv2img)

        return QtGui.QPixmap(QtGui.QImage(cv2img, width, height, byteValue, QtGui.QImage.Format_RGB888))

    def update_images(self, data):

        frame, ls = data

        if frame is not None:
            w = self.current.frameGeometry().width()
            self.current.setPixmap(self.cv2_image_to_qt(frame, w))
            w = self.last_seen.frameGeometry().width()
            self.last_seen.setPixmap(self.cv2_image_to_qt(ls.get_grid().astype(np.uint8), w))

    def start_recognition(self):

        if self.recognitor is None:
            self.recognitor = MulticoreAsyncRecognitor(save=self.save, save_full_frames=self.save_full_frames)
            self.recognitor.get_list_signals().connect(self.update_images)
            self.recognitor.start()

    def stop_recognition(self):

        if self.recognitor is not None:
            self.recognitor.stop()
            self.recognitor = None

    def recognition_button_callback(self):

        if self.recognitor is None:
            self.start_recognition()
            self.recognition_button.setText("STOP")
        else:
            self.stop_recognition()
            self.recognition_button.setText("START")

    def start_copy(self):

        if self.copier is None:
            choice = QtGui.QMessageBox.question(self, 'Copia immagini',
                                       "Sei sicuro di voler copiare le immagini sul server? ",
                                       QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)

            if choice == QtGui.QMessageBox.Yes:
                self.copier = AsyncImageCopier()
                self.copier.progress.connect(lambda p:self.progress.setValue(p))
                self.copier.end.connect(self.stop_copy)
                self.copier.messages.connect(lambda e: self.errors.setText(e))
                self.copier.start()
                self.upload_button.setText("STOP UPLOAD")

    def stop_copy(self):

        if self.copier is not None:
            self.copier.stop()
            self.copier = None
            self.upload_button.setText("START IMAGES UPLOAD")

    def upload_images_button_callback(self):

        if self.copier is None:
            self.start_copy()
        else:
            self.upload_button.setEnabled(False)
            self.stop_copy()
            self.upload_button.setEnabled(True)

    def exit(self):
        self.stop_recognition()
        sys.exit()

    def home(self):

        recognition_button = self.get_button("START")
        recognition_button.clicked.connect(self.recognition_button_callback)
        self.recognition_button = recognition_button
        uploadButton = self.get_button("START IMAGES UPLOAD")
        uploadButton.clicked.connect(self.upload_images_button_callback)
        self.upload_button = uploadButton
        shutdownButton = self.get_button("SHUTDOWN")
        shutdownButton.clicked.connect(self.shutdown)
        quitButton = self.get_button("QUIT")
        quitButton.clicked.connect(self.exit)

        test_img = cv2.imread(r'disegno2.jpg')

        self.last_seen = QtGui.QLabel()
        # self.last_seen.setPixmap(self.cv2_image_to_qt(test_img))
        # self.last_seen.setScaledContents(True)
        self.last_seen.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)

        self.current = QtGui.QLabel()
        # self.current.setPixmap(self.cv2_image_to_qt(test_img))
        # self.current.setScaledContents(True)
        self.current.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)

        self.progress = QtGui.QProgressBar(self)
        self.errors = QtGui.QLabel(self)
        self.errors.setText("Nessun errore")

        buttonBox = QtGui.QVBoxLayout()
        buttonBox.addWidget(recognition_button)
        buttonBox.addWidget(uploadButton)
        buttonBox.addWidget(shutdownButton)
        buttonBox.addWidget(quitButton)

        lastSeenBox = QtGui.QVBoxLayout()
        lastSeenBox.addStretch(1)
        lastSeenBox.addWidget(self.last_seen)
        lastSeenBox.addStretch(1)
        #
        # currentBox = QtGui.QVBoxLayout()
        # currentBox.addStretch(1)
        # currentBox.addWidget(self.current)
        # currentBox.addStretch(1)

        grid = QtGui.QGridLayout()
        grid.setColumnStretch(0, 1)
        grid.setColumnStretch(1, 2)
        grid.setColumnStretch(2, 4)
        grid.addLayout(buttonBox, 0, 0)
        grid.addWidget(self.last_seen, 0, 1)
        grid.addWidget(self.current, 0, 2)
        grid.addWidget(self.progress, 1, 0, 1, 3)
        grid.addWidget(self.errors, 2, 0, 1, 3)

        wid = QtGui.QWidget(self)
        self.setCentralWidget(wid)

        wid.setLayout(grid)
        # self.showFullScreen()

        if self.fulls_screen:
            self.showFullScreen()
        else:
            self.showMaximized()

        choice = QtGui.QMessageBox.question(self, 'Attivazione salvataggio immagini',
                                        " Vuoi salvare le immagini riconosciute? ",
                                        QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)

        if choice == QtGui.QMessageBox.Yes:
            self.save = True

            choice = QtGui.QMessageBox.question(self, 'Attivazione salvataggio interi frames',
                                                " Vuoi salvare i frames interi catturati? ",
                                                QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)

            if choice == QtGui.QMessageBox.Yes:
                self.save_full_frames = True
            else:
                self.save_full_frames = False
        else:
            self.save = False


def run():
    app = QtGui.QApplication(sys.argv)
    GUI = Window()
    sys.exit(app.exec_())


if __name__ == "__main__":
    run()