import keras

from dataset_utils import reshape_image_for_cnn, convert_img_color


class ImageScorer:

    def __init__(self):
        self._model = keras.models.load_model("nn/signosig_cnn_color.model")
        self._model._make_predict_function()

    def get_score_from_img(self, img):

        img_prepared = reshape_image_for_cnn(convert_img_color(img))

        prediction = self._model.predict([img_prepared])[0][0]
        score = 1.0 - 2*abs(prediction - 0.5)
        return prediction, score
