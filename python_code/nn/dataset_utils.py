
import cv2
import numpy as np

IMG_SIZE_X = 40
IMG_SIZE_Y = 40

def convert_img_color(img):

    img_resized = cv2.resize(img, (IMG_SIZE_X, IMG_SIZE_Y))

    img_resized = img_resized.astype(np.double) / 255

    return img_resized

def reshape_image_for_cnn(img):

    h, w, c = img.shape

    return img.reshape(-1, IMG_SIZE_X, IMG_SIZE_Y, c)