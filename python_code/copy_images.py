from os import listdir
from os.path import isfile, join
import requests

from config import FILES_PER_REQUEST, UPLOAD_IMAGE_URL, ALLOWED_EXTENSIONS, IMAGES_DIRECTORY


class FileDesc:

    def __init__(self, dir_path, filename):

        self.filename = filename
        self.dir_path = dir_path
        self.abs_path = join(dir_path, filename)
        self.fd = None

    def get_abs_path(self):
        return self.abs_path

    def open(self, mode):
        self.fd = open(self.abs_path, mode)

    def close(self):
        self.fd.close()

    def __str__(self):
        return "%s" % self.abs_path

    def __repr__(self):
        return "%s" % self.abs_path


def get_dir_images(dir_path):
    return [FileDesc(dir_path, f) for f in listdir(dir_path)
            if isfile(join(dir_path, f))
            and f.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS]

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def open_all(files):
    for f in files:
        f.open("rb")

def close_all(files):
    for f in files:
        f.close()


if __name__ == "__main__":

    files = get_dir_images(IMAGES_DIRECTORY)

    chunked_files = list(chunks(files, FILES_PER_REQUEST))

    for i,chunk in enumerate(chunked_files):
        open_all(chunk)

        file_dict = {}

        for f in chunk:
            file_dict[f.filename] = f.fd

        print "Chunk %d on %d" % (i, len(chunked_files))
        r = requests.post(UPLOAD_IMAGE_URL, files=file_dict)
        print r.json()

        close_all(chunk)

# complete_file_names
#
# for img in files:
#     img_filename = join(img_dir, img)
#
#     with open(img_filename, 'rb') as f:
#         r = requests.post('http://localhost:5000/upload/image', files={img: f})
#         print r.json()