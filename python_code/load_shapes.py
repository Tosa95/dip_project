import os
import pickle
from io import open
import numpy as np

from config import CONTOUR_PLOT_POINTS
from lib.shape_matching import compute_base_shapes_contour_plot, get_correlation


def load_shapes_plots():

    if not os.path.isfile("contours.pickle"):
        plots = compute_base_shapes_contour_plot(CONTOUR_PLOT_POINTS)

        with open("contours.pickle", "wb") as f:

            pickle.dump(plots, f)
    else:

        with open("contours.pickle", "rb") as f:

            plots = pickle.load(f)

    return plots

# plots = load_shapes_plots()
# correlation = get_correlation(plots["circle"], plots["triangle"])
# print np.argmax(correlation)
# print correlation[np.argmax(correlation)]
# print correlation