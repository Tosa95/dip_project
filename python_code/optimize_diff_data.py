import random
import sys
from Queue import Queue
from datetime import datetime
from math import exp
from threading import Thread

import cv2
import os
import pickle

from lib.recognition_utils import preprocess_image, recognize_single_frame_shapes_2, get_full_res_bb
from parameters_extimation.genetic_programming import GeneticAlgorithm, get_mutate_random_gene_fn, \
    get_standard_cross_over_fn, get_standard_next_gen_fn, get_no_improve_terminate_fn, \
    get_repeated_mutate_with_p_fn
from parameters_extimation.probability_extract import log_mutate_actions

DATASETS_FOLDER = "/home/davide/datasets"
DATASET = os.path.join(DATASETS_FOLDER, "images2018-11-19 13:00:40.341455.pickle")

IMAGES_KEPT = 100

N_THREADS = 8
ACCEPTED_ERROR = 0.1
MEGAPIXELS = 0.01
MIN_CONTOUR = 10

N_PARAMS = 7
POP_SIZE = 20

MAX_GEN_NO_IMPROVE = 1000

GENFILE = "last_gen_comb_4"
BESTFILE = "best_comb_4"

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

with open(DATASET, "rb") as input:
    dataset = pickle.load(input)
    random.shuffle(dataset)

reduced_dataset = list(enumerate(dataset[:IMAGES_KEPT]))
chunked_dataset = list(chunks(reduced_dataset, int(round(float(len(reduced_dataset))/N_THREADS))))

def shuffle_dataset():
    global chunked_dataset, reduced_dataset, dataset
    random.shuffle(dataset)
    reduced_dataset = dataset[:IMAGES_KEPT]
    chunked_dataset = list(chunks(reduced_dataset, int(round(float(len(reduced_dataset)) / N_THREADS))))

print len(dataset)

# print os.path.split(os.getcwd())[0]
#
# os.chdir(os.path.split(os.getcwd())[0])

def shape_found(recognition_result, square_data, img_sz, wr, hr):

    for descriptor in recognition_result:
        given_bb = descriptor["bounding_box"]

        bb = get_full_res_bb(given_bb, wr, hr)

        x1r = bb[1]
        y1r = bb[3]
        x2r = bb[0]
        y2r = bb[2]

        x1i = square_data[0]
        y1i = square_data[1]
        x2i = square_data[2]
        y2i = square_data[3]

        diffs = [abs(x1r-x1i), abs(y1r-y1i), abs(x2r-x2i), abs(y2r-y2i)]

        err = False

        for d in diffs:

            if float(d)/img_sz > ACCEPTED_ERROR:
                err = True
                break

        if not err:
            return True

    return False



def eval(diff_data, fitment_data, ga):

    q = Queue()

    weights = fitment_data.additional_data["weights"]
    n_recs = fitment_data.additional_data["n_recs"]

    # print [("%3d"%n_rec, "%.2f"%w) for (n_rec, w) in zip(n_recs, weights)]

    def eval_part(chunk):

        tot_rec = 0
        tot_time = 0

        for img_index, data in chunk:
            img = data["img"]
            square_data = data["square_data"]

            img_sz, _, _ = img.shape

            preprocessed_frame, wr, hr = preprocess_image(img, megapixels=MEGAPIXELS)

            before = datetime.now()

            recognition_result = recognize_single_frame_shapes_2(preprocessed_frame,
                                                                 diff_data=diff_data,
                                                                 min_shape_contour_points=MIN_CONTOUR)

            # print recognition_result, square_data

            time = (datetime.now() - before).total_seconds()

            tot_time += time

            if shape_found(recognition_result, square_data, img_sz, wr, hr):
                tot_rec += 1 * weights[img_index]
                n_recs[img_index] += 1

            # print "TIME: ", time

        q.put((tot_rec, tot_time))

    threads = [Thread(target=eval_part, args=(chunk, )) for chunk in chunked_dataset]

    for t in threads:
        t.start()

    for t in threads:
        t.join()

    # print "HEREEEEE", chunked_dataset

    tot_time = 0
    tot_rec = 0

    while not q.empty():
        rec, tm = q.get()

        tot_time += tm
        tot_rec += rec

    return 1 - 0.1 * tot_time + float(tot_rec * 100) / len(reduced_dataset)


def print_state(data):
    print " "
    print data.best.fit_eval, sum(data.pop.eval_list)/len(data.pop.eval_list), data.best.genes
    with open(BESTFILE, "wb") as output:
        pickle.dump(data.best, output)

def update_weigths(data):
    weights = data.additional_data["weights"]
    n_recs = data.additional_data["n_recs"]

    recognized_using_all_children = sum([ (1 if r>0 else 0) for r in n_recs])
    print "Rec using all childrens: ", recognized_using_all_children

    avg_recs = float(sum(n_recs))/len(n_recs)

    avg_recs_perc = (avg_recs/len(data.pop))*100

    for i in xrange(len(weights)):
        rec_perc = (float(n_recs[i])/len(data.pop))*100
        weights[i] = exp((avg_recs_perc - rec_perc)*0.00000)
        n_recs[i] = 0

def on_new_gen(data):

    print " "

    print data.gen, data.best.fit_eval, sum(data.pop.eval_list)/len(data.pop.eval_list)

    weights = data.additional_data["weights"]
    n_recs = data.additional_data["n_recs"]
    print [("%3d"%n_rec, "%.2f"%w) for (n_rec, w) in zip(n_recs, weights)]

    update_weigths(data)

    with open(GENFILE, "wb") as output:
        pickle.dump(data.pop, output)

def rand_params():
    return [random.uniform(-5, 5) for _ in xrange(N_PARAMS)]

def print_perc(p):
    # sys.stdout.write("%.2f " % p)
    sys.stdout.write(".")
    sys.stdout.flush()

mutate_rnd = get_mutate_random_gene_fn([-0.1, 0.1, -1, 1, 10, -10, 0.05, -0.05, 0.01, -0.01, 0.001, -0.001])

mutate_with_p = get_repeated_mutate_with_p_fn(0.03, 10, mutate_rnd)

eval_pop = eval

cross_over = get_standard_cross_over_fn()

next_gen = get_standard_next_gen_fn()

terminate = get_no_improve_terminate_fn(MAX_GEN_NO_IMPROVE)

GA = GeneticAlgorithm(
    mutate_with_p,
    eval_pop,
    cross_over,
    next_gen,
    terminate,
    on_best_changed=print_state,
    on_new_gen=on_new_gen,
    on_next_gen_perc_change=print_perc
)

try:
    with open(GENFILE, "rb") as input:
        first_gen = pickle.load(input)
        print "First gen loaded from file"
        print first_gen
except Exception as ex:
    first_gen = [rand_params() for _ in xrange(POP_SIZE)]

result = GA.fit(
    first_gen,
    initial_additional_data={"n_recs": [0 for _ in reduced_dataset], "weights": [1.0 for _ in reduced_dataset]}
)

