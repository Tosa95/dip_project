# coding=utf-8
import os

import sys


def py_charm_set_working_directory_project_root():

    cwd = os.getcwd()

    while ".idea" not in os.listdir(cwd):
        cwd = os.path.split(cwd)[0]

    os.chdir(cwd)

# Dato che serve per poter importare i moduli, questa funzione non può essere importata :(,
# bisogna copiarne il codice prima degli import
def py_charm_add_import_path():

    cwd = os.getcwd()

    while ".idea" not in os.listdir(cwd):
        cwd = os.path.split(cwd)[0]

    sys.path.append(cwd)
