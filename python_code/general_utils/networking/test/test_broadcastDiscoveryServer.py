from unittest import TestCase

from general_utils.networking.broadcasting import BroadcastDiscoveryServer, discover_servers


class TestBroadcastDiscoveryServer(TestCase):

    def test_base(self):

        bds = BroadcastDiscoveryServer(45678, "test")
        bds.start()

        res = discover_servers(45678)

        bds.stop()

        self.assertEqual(res[0][1], "test")
