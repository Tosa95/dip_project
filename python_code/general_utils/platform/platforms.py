import platform

PLATFORM_UNKNOWN = -1
PLATFORM_WINDOWS = 0
PLATFORM_LINUX = 1
PLATFORM_MAC_OS = 2


def get_platform():
    platform_string = platform.system().lower()

    if platform_string == "windows":
        return PLATFORM_WINDOWS
    elif platform_string == "linux":
        return PLATFORM_LINUX
    elif platform_string == "darwin":
        return PLATFORM_MAC_OS
    else:
        return PLATFORM_UNKNOWN
