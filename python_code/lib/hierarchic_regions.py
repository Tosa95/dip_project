
WHITE = 0
GREY = 1
BLACK = 2

def is_bfs_ancestor(ancestor_region, region, regions):

    if region == ancestor_region:
        return True

    if region == 0:
        return False

    return is_bfs_ancestor(ancestor_region, regions[region].get_bfs_father(), regions)

def BFS(regions):

    colors = [WHITE] * len(regions)

    queue = [0]

    ordered = []

    regions[0].set_distance(0)

    colors[0] = GREY

    while len(queue) > 0:

        curr = regions[queue[0]]

        ordered.append(queue[0])
        del queue[0]

        curr_dist = curr.get_distance()

        for adj in curr.get_adiacent_areas():

            if colors[adj] == WHITE:
                colors[adj] = GREY

                curr.add_bfs_son(adj)
                regions[adj].set_bfs_father(curr.get_label())
                regions[adj].set_distance(curr_dist + 1)

                queue.append(adj)

        colors[curr.get_label()] = BLACK

    return ordered

class Neigbours:

    def __init__(self, regions, first_region_index):
        self._regions = regions
        self._neighbours = []

    def add_neighbour(self, neighbour):

        self._neighbours.append(neighbour)

    def set_sa_to_all(self, sa):

        for neighbour in self._neighbours:

            self._regions[neighbour].set_super_area(sa)
            self._regions[sa].add_inner_area(neighbour)


def find_sa(first_region_index, regions, visited):

    queue = [first_region_index]
    dist = regions[first_region_index].get_distance()
    neighbours = Neigbours(regions, first_region_index)
    upper = set()

    while True:

        while len(queue) > 0:

            curr = queue[0]
            del queue[0]

            if visited[curr] == 0: # Non capisco perche lo avevo messo --> ah si, perche se poi devo ricominciare dalle super areas mi serve di poterci ripassare, ma senza rimetterle nei neghbour
                neighbours.add_neighbour(curr)

            visited[curr] = 1

            for neigh in regions[curr].get_adiacent_areas():
                if visited[neigh] == 0 and regions[neigh].get_distance() == dist:
                    queue.append(neigh)
                elif regions[neigh].get_distance() < dist and neigh not in upper:
                    upper.add(neigh)

        if len(upper) == 1:
            neighbours.set_sa_to_all(upper.pop())
            break
        else:
            queue.extend(upper)
            upper = set()
            dist -= 1






def subregions(regions, ordered):

   visited = [0] * len(ordered)

   # Ciclo su tutti tranne che su 0

   for region_index in reversed(ordered[1:]):

       if visited[region_index] == 0:
           find_sa(region_index, regions, visited)

