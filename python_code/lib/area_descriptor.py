from ctypes import c_bool

from sparse_data import SparseArray
import numpy as np


class AreaDesriptor:

    def __init__(self, label):
        self._label = label
        self._num_points = 0
        self._num_points_bound = 0
        self._bound_points = []
        self._points = []
        self._super_area = None
        self._inner_areas = set()
        self._adiacent_areas = set()
        self._average = np.array([0,0,0], np.double)
        self._centroid = np.array([0,0], np.double)
        self._bounding_box = [-1, -1, -1, -1]
        self._ended = False
        self._distance = -1
        self._bfs_father = -1
        self._bfs_sons = []

    @classmethod
    def get_from_c_region_growing(cls, c_region):
        res = AreaDesriptor(c_region["label"])

        res._num_points = len(c_region["points"])
        res._points = c_region["points"]

        res._num_points_bound = len(c_region["boundary_points"])
        res._bound_points = c_region["boundary_points"]

        res._centroid = np.array(c_region["centroid"])
        res._average = np.array(c_region["average"])

        res._adiacent_areas = set(c_region["near_areas"])

        res._bounding_box = c_region["bounding_box"]

        res._bfs_sons = c_region["bfs_children"]
        res._bfs_father = c_region["bfs_father"]
        res._distance = c_region["distance"]

        res._inner_areas = set(c_region["inner_regions"])
        res._super_area = c_region["super_region"]

        res._ended = True

        return res

    def add_bound_point(self, point):
        self._num_points_bound += 1
        self._bound_points.append(point)

    def add_point(self, point, color):

        self._points.append(point)

        self._num_points += 1

        self._average += color
        self._centroid += point

        if self._bounding_box[0] == -1:
            self._bounding_box = [point[0], point[1], point[0], point[1]]
        else:
            row = point[0]
            col = point[1]

            if row < self._bounding_box[0]:
                self._bounding_box[0] = row
            elif row > self._bounding_box[2]:
                self._bounding_box[2] = row

            if col < self._bounding_box[1]:
                self._bounding_box[1] = col
            elif col > self._bounding_box[3]:
                self._bounding_box[3] = col

    def get_points(self):
        return self._points

    def grow_end(self):

        self._average /= self._num_points
        self._centroid /= self._num_points

        self._ended = True

    def set_ended(self):

        self._ended = True

    def get_label(self):

        return self._label

    def get_num_points(self):

        return self._num_points

    def get_num_points_bound(self):

        return self._num_points_bound

    def get_bound_points(self):

        return self._bound_points

    def get_super_area(self):

        return self._super_area

    def set_super_area(self, super):

        self._super_area = super

    def get_adiacent_areas(self):

        return self._adiacent_areas

    def get_inner_areas(self):

        return self._inner_areas

    def add_inner_area(self, inner):

        self._inner_areas.add(inner)

    def get_average(self):

        if not self._ended:
            return self._average/self._num_points
        else:
            return self._average

    def get_centroid(self):

        if not self._ended:
            return self._centroid/self._num_points
        else:
            return self._centroid

    def get_bounding_box(self):

        return self._bounding_box

    def get_distance(self):
        return self._distance

    def set_distance(self, new_dist):
        self._distance = new_dist

    def set_bfs_father(self, father):
        self._bfs_father = father

    def get_bfs_father(self):
        return self._bfs_father

    def get_bfs_sons(self):
        return self._bfs_sons

    def add_bfs_son(self, new_son):
        self._bfs_sons.append(new_son)

    def set_points(self, points):
        self._points = points
        self._num_points = len(points)

    def set_centroid(self, centroid):
        self._centroid = centroid

    def set_average(self, average):
        self._average = average

    def set_bounding_box(self, bounding_box):
        self._bounding_box = bounding_box
        self._num_points = len(bounding_box)

    def __repr__(self):

        return "lbl>" + str(self._label) + \
               " adj>" + str(self._adiacent_areas) + \
               " points>" + str(self._num_points) + \
               " average>" + str(self._average) + \
               " perim_points>" + str(self._num_points_bound) + \
               " distance>" + str(self._distance) + \
               " bfs_father>" + str(self._bfs_father) + \
               " bfs_sons>" + str(self._bfs_sons) + \
               " super_area>" + str(self._super_area) + \
               " inner_area>" + str(self._inner_areas)

def get_all_inner_regions(region_index, regions):

    queue = [region_index]

    res = []

    while len(queue) > 0:

        curr = queue[0]
        del queue[0]
        res.append(curr)

        for neigh in regions[curr].get_inner_areas():
            queue.append(neigh)

    return res


def regions_touched(p, region_map):

    height = region_map.get_height()
    width = region_map.get_width()

    res = set()

    for row_step in [-1, 0, 1]:
        for col_step in [-1, 0, 1]:
            row = p[0] + row_step
            col = p[1] + col_step

            if 0<=row<height and 0<=col<width:

                if region_map.get_label(row, col) not in res:
                    res.add(region_map.get_label(row, col))

            elif 0 not in res: #Se non sono nell'area dell'immagine, allora confino con la regione 0!
                res.add(0)

    return res


def collapse_inner_regions(region_index, regions, region_map, collapse_internal_points = True):

    color = regions[region_index].get_average()
    res = AreaDesriptor(regions[region_index].get_label())
    inner_regions = get_all_inner_regions(region_index, regions)

    if not collapse_internal_points:
        res.set_average(color)
        res.set_centroid(regions[region_index].get_centroid())
        res.set_points(regions[region_index].get_points())
        res.set_bounding_box(regions[region_index].get_bounding_box())
        res.set_ended()

    for reg in inner_regions:

        if collapse_internal_points:
            for p in regions[reg].get_points():
                res.add_point(p, color)
        for p in regions[reg].get_bound_points():
            touched = regions_touched(p, region_map)
            bound = False
            for t in touched:
                if t not in inner_regions:
                    bound = True
                    break
            if bound:
                res.add_bound_point(p)

    return res

def get_region_image (height, width, region):

    res = np.full((height, width, 3), 255, np.uint8)

    color = region.get_average()

    for p in region.get_points():

        res[p[0], p[1], :] = color

    for p in region.get_bound_points():

        res[p[0], p[1], :] = [0, 0, 0]

    centroid = region.get_centroid()

    c_color = np.array([255, 255, 255]) - res[int(centroid[0]), int(centroid[1]), :]

    res[int(centroid[0]), int(centroid[1]), :] = c_color



    return res