from area_descriptor import AreaDesriptor
from region_growing import RegionMap
import numpy as np
import advanced_region_growing


def region_growing_and_bfs(image, th, dist_from_orig = False):

    c_res = advanced_region_growing.region_growing_and_bfs(image.astype(np.uint8), int(th))

    regions = [AreaDesriptor.get_from_c_region_growing(reg) for reg in c_res["regions"]]
    map = c_res["map"]

    return regions,  RegionMap(map), c_res["ordered_regions"]

def hierarchic_region_growing(image, th):

    c_res = advanced_region_growing.hierarchic_region_growing(image.astype(np.uint8), int(th))

    regions = [AreaDesriptor.get_from_c_region_growing(reg) for reg in c_res["regions"]]
    map = c_res["map"]

    return regions, RegionMap(map), c_res["ordered_regions"]