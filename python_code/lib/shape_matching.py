import os
import random

import advanced_region_growing

from hierarchic_regions_c import hierarchic_region_growing
from math import atan2, pi, sqrt
import cv2

import numpy as np

from config import NO_MATCHING_SHAPE
from lib.area_descriptor import collapse_inner_regions, get_all_inner_regions
from lib.img_utils import resize

import matplotlib as mpl
mpl.use('Agg')


def get_distance (p1, p2):

    return sqrt((float(p1[0])-float(p2[0]))**2 + (float(p1[1])-float(p2[1]))**2)

def compute_contour_plot(region, num_points, normalize=True):

    centroid = region.get_centroid()

    # print centroid
    # print " "

    def angle(point):

        y = point[0] - centroid[0]
        x = point[1] - centroid[1]

        return atan2(x, y)

    points = sorted(region.get_bound_points(), key=angle)

    step = 2*pi/num_points

    res = np.zeros(num_points, np.double)

    # angolo attualmente considerato
    curr_angle = -pi
    # posizione corrente nel risultato (corrispondente a curr_angle/step)
    curr_position = 0
    # indice corrente all'interno dei punti di contorno
    curr_index = 0

    first_matching = True
    prev_matching_dist = None
    prev_matching_position = None

    while curr_index < len(points):

        current_point_angle = angle(points[curr_index])

        if current_point_angle >= curr_angle:
            # print points[curr_index], get_distance(centroid, points[curr_index]), angle(points[curr_index])



            if first_matching == True:
                # E' il primo che matcha, lo metto direttamente nel risultato
                first_matching = False
                res[curr_position] = get_distance(centroid, points[curr_index])

                if prev_matching_dist is not None:
                    for pos in xrange(prev_matching_position+1, curr_position):
                        interpolated_dist = ((res[curr_position] - prev_matching_dist)/(curr_position-prev_matching_position))*(pos-prev_matching_position) + prev_matching_dist
                        res[pos] = interpolated_dist

                prev_matching_dist = res[curr_position]
                prev_matching_position = curr_position

            curr_angle += step
            curr_position += 1
            if curr_position >= num_points:
                break
        else:
            curr_index += 1
            first_matching = True

    # inseriamo anche gli ultimi punti
    curr_position = num_points - 1
    # Faccio l'interpolazione usando il punto iniziale!!! (come e' giusto: la funzione di distanza e' circolare)
    res[curr_position] = get_distance(centroid, points[0])

    for pos in xrange(prev_matching_position + 1, curr_position):
        interpolated_dist = ((res[curr_position] - prev_matching_dist) / (
        curr_position - prev_matching_position)) * (pos - prev_matching_position) + prev_matching_dist
        res[pos] = interpolated_dist

    if normalize:
        # energy = np.sqrt(np.sum(np.square(res)))
        # #print "Energy " + str(energy) + " " + str(len(points)) + " " + str(region.get_label())
        # res /= energy

        max = np.max(abs(res))

        print "max " + str(max) + " " + str(len(points)) + " " + str(region.get_label())

        if max==0:
            max = 1

        res/=max

    return res

def get_correlation(contour_plot_1, contour_plot_2):

    return np.fft.irfft(np.fft.rfft(contour_plot_1) * np.conj(np.fft.rfft(contour_plot_2)))

def get_shape_distance(contour_plot_1, contour_plot_2):

    print "before correlation"

    correlation = get_correlation(contour_plot_1, contour_plot_2)

    max_of_correlation = np.argmax(correlation)

    res = np.sum(np.square(contour_plot_1 - np.roll(contour_plot_2, max_of_correlation)))

    print "after correlation"

    cp2_rolled = np.roll(contour_plot_2, max_of_correlation)

    graph_num = str(random.randint(0,100))



    # plt.figure(1)
    # plt.clf()
    # plt.subplot(2, 1, 1)
    # plt.title("The two contour functions unaligned")
    # plt.plot(range(128), contour_plot_1)
    # plt.plot(range(128), contour_plot_2)
    # plt.subplot(2,1,2)
    # plt.title("The two contour functions aligned + difference (distance:%f)" % res)
    # plt.plot(range(128),contour_plot_1)
    # plt.plot(range(128), cp2_rolled)
    # plt.plot(range(128), contour_plot_1 - cp2_rolled)
    # plt.draw()
    # plt.savefig("graphs/test" + graph_num + ".png", dpi=120)
    #
    # print "graph num: " + graph_num + " " + str(np.max(correlation))

    return res

def get_best_matching_shape_2(region_contour, base_shapes_contours, threshold):

    best_dist = 100000000
    best = NO_MATCHING_SHAPE
    best_contour = None

    for s_name, s_plots in base_shapes_contours.iteritems():
        for plot in s_plots:
            print s_name
            shape_distance = get_shape_distance(region_contour, plot)
            print s_name, shape_distance
            if shape_distance < best_dist and shape_distance < threshold:
                best_dist = shape_distance
                best = s_name
                best_contour = plot

    return {"best_match_label": best, "best_match_distance": best_dist, "best_match_contour": best_contour}

def get_best_matching_shape(region_contour, base_shapes_contours, threshold):

    return get_best_matching_shape_2(region_contour, base_shapes_contours, threshold)["best_match_label"]

def classify_relevant_shapes(regions, ordered_regions, regions_map, base_shapes_contours, shape_match_threshold, num_contour_points, min_shape_contour_points):

    res = []

    visited = [0] * len(regions)

    for lbl in ordered_regions:

        shape = regions[lbl]

        if shape.get_num_points_bound() > min_shape_contour_points and visited[lbl] == 0:

            visited[lbl] = 1

            collapsed = collapse_inner_regions(shape.get_label(), regions, regions_map)
            contour_points = collapsed.get_bound_points()
            shape_contour = np.array(advanced_region_growing.compute_contour_plot(contour_points, list(collapsed.get_centroid()), num_contour_points))

            # shape_contour = compute_contour_plot(collapsed, num_contour_points)

            best_mathing = get_best_matching_shape(shape_contour, base_shapes_contours, shape_match_threshold)

            if best_mathing != NO_MATCHING_SHAPE:
                res.append((collapsed, best_mathing))

                inner_regions = get_all_inner_regions(lbl, regions)


                for inner in inner_regions:
                    visited[inner] = 1


    return res

def compute_base_shapes_contour_plot(npoints):

    location = "base_shapes_images"

    plots = {}

    for filename in os.listdir(location):

        if filename.endswith(".png") or filename.endswith(".jpg"):

            print filename

            shape_name = os.path.splitext(filename)[0].split("_")[0]
            shape_location = os.path.join(location, filename)

            image = cv2.imread(shape_location, cv2.IMREAD_COLOR).astype(np.double)

            small, wr, hr = resize(image, 0.1)

            h, w, _ = small.shape

            blurred = cv2.GaussianBlur(small, (5, 5), 0).astype(np.double)

            regions, map, _ = hierarchic_region_growing(blurred.astype(np.uint8), 200)

            print len(regions)

            #ordered = BFS(regions)

            # print "here2"

            #subregions(regions, ordered)

            # print "here3"

            collapsed = collapse_inner_regions(map.get_label(int(h/2), int(w/2)), regions, map)

            # print collapsed
            #
            # if collapsed.get_num_points_bound() == 0:
            #     collapse_inner_regions(map.get_label(int(h / 2 ), int(w / 2)), regions, map)

            #contour_plot = compute_contour_plot(collapsed, npoints)
            contour_plot = np.array(advanced_region_growing.compute_contour_plot(collapsed.get_bound_points(), list(collapsed.get_centroid()), npoints))

            if shape_name not in plots:
                plots[shape_name] = []

            plots[shape_name].append(contour_plot)

    return plots






