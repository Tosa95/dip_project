class SparseMatrix:

    def __init__(self):

        self._data = {}

    def set(self, row, col, val):

        if row not in self._data:
            self._data[row] = {}

        self._data[row][col] = val

    def get(self, row, col):

        if row not in self._data or col not in self._data[row]:
            return 0

        return self._data[row][col]


class SparseArray:

    def __init__(self):

        self._data = {}

    def set(self, i, val):

        self._data[i] = val

    def get(self, i):

        if i not in self._data:
            return 0

        return self._data[i]

    def get_data(self):
        return self._data

    def __repr__(self):
        return str(self._data)