import os
from distutils.command.config import config
from math import sqrt, floor, ceil
import random
import datetime
import numpy as np
from scipy.odr import odr_stop

import fast_shape_matching
from config import SHAPE_MATCH_THRESHOLD, MAX_DIFF_THRESHOLD, MAX_DIFF_ON_MEAN, CONTOUR_PLOT_POINTS, \
    MIN_SHAPE_CONTOUR_POINTS, SAVE_IMAGES, DATE_FORMAT, logger, BOUNDING_BOX_ENLARGING_FACTOR, IMAGES_DIRECTORY, \
    REGION_GROWING_THRESHOLD, REGION_GROWING_THRESHOLD_EXTIMATION_BB, REGION_GROWING_THRESHOLD_EXTIMATION_FACTOR, \
    HISTOGRAM_EQUALIZE_BEFORE_PROCESSING, REGION_GROWING_THRESHOLD_EXTIMATION_VECTOR, \
    REGION_GROWING_THRESHOLD_EXTIMATION_POLY, IMAGE_BB, MEGAPIXELS, SHAPE_RECOGNITION_PARAMETERS, FULL_FRAMES_DIRECTORY
from load_shapes import load_shapes_plots
from img_utils import resize
import cv2



def print_fps(image, last_timing):
    h,w,_ = image.shape
    font = cv2.FONT_HERSHEY_COMPLEX_SMALL
    fps = 1/last_timing
    cv2.putText(image, "FPS: %d" % int(fps), (0, h-1), font, 2, (0,255,0), 4)

def print_save_state(image, save, save_full_frames):
    h, w, _ = image.shape
    text = "SAVE: ON" if save else "SAVE: OFF"
    color = (0, 255, 0) if save else (0, 0, 255)
    font = cv2.FONT_HERSHEY_COMPLEX_SMALL
    cv2.putText(image, text, (300, h-1), font, 2, color, 4)
    text = "SAVE FF: ON" if save_full_frames else "SAVE FF: OFF"
    color = (0, 255, 0) if save_full_frames else (0, 0, 255)
    font = cv2.FONT_HERSHEY_COMPLEX_SMALL
    cv2.putText(image, text, (300, h - 50), font, 2, color, 4)

def get_full_res_bb(given_bb, wr, hr):
    return [int(floor(given_bb[0] / hr)), int(floor(given_bb[1] / wr)),
            int(ceil(given_bb[2] / hr)), int(ceil(given_bb[3] / wr))]

def print_relevant_shapes(image, relevant, wr, hr):
    for descriptor in relevant:
        bb = descriptor["bounding_box"]
        label = descriptor["name"]

        bb = [int(floor(bb[0] / hr)), int(floor(bb[1] / wr)), int(ceil(bb[2] / hr)), int(ceil(bb[3] / wr))]

        # color = (random.randint(50, 255), random.randint(50, 255), random.randint(50, 255))

        color = (0, 255, 0)

        cv2.rectangle(image, (bb[1], bb[0]), (bb[3], bb[2]), color, 2)

        font = cv2.FONT_HERSHEY_COMPLEX_SMALL

        cv2.putText(image, label, (bb[1], bb[2]), font, 0.7, color, 1)

def print_relevant_shapes_bb_only(image, shapes):
    for descriptor in shapes:
        bb = descriptor["bounding_box"]
        label = descriptor["label"]

        # color = (random.randint(50, 255), random.randint(50, 255), random.randint(50, 255))

        color = (0, 255, 0)

        cv2.rectangle(image, (bb[1], bb[0]), (bb[3], bb[2]), color, 2)

        font = cv2.FONT_HERSHEY_COMPLEX_SMALL

        color = (0, 0, 255)

        cv2.putText(image, label, (bb[1], bb[2]), font, 0.7, color, 1)

def print_th_extimation_bb(image, th_ext_bb=REGION_GROWING_THRESHOLD_EXTIMATION_BB):

    height, width, _ = image.shape

    x1 = int(th_ext_bb[0] * width)
    y1 = int(th_ext_bb[1] * height)
    x2 = int(th_ext_bb[2] * width)
    y2 = int(th_ext_bb[3] * height)

    print x1, x2, y1, y2

    cv2.rectangle(image, (x1, y1), (x2, y2), (0, 255, 0), 1)

def save_full_frame(frame):
    path = os.path.join(FULL_FRAMES_DIRECTORY, "frame-" + datetime.datetime.strftime(datetime.datetime.now(),
                                                                                            DATE_FORMAT) + ".jpg")

    frame_small,_,_ = resize(frame, 0.05)

    cv2.imwrite(path, frame_small)

def enlarge_bounding_box(bb, enlarge_factor=BOUNDING_BOX_ENLARGING_FACTOR):

    y1, x1, y2, x2 = bb

    bbw = x2 - x1
    bbh = y2 - y1

    ext_w = int(bbw * enlarge_factor / 2)
    ext_h = int(bbh * enlarge_factor / 2)

    y1 -= ext_h
    y2 += ext_h
    x1 -= ext_w
    x2 += ext_w

    return [y1, x1, y2, x2]

def to_int_bb(image, bb):
    height, width, _ = image.shape

    y1, x1, y2, x2 = bb

    return [int(y1*height), int(x1*width), int(y2*height), int(x2*width)]

def crop(image, bb):

    height, width, _ = image.shape

    y1, x1, y2, x2 = bb

    if y1 < 0:
        y1 = 0

    if y2 >= height:
        y2 = height - 1

    if x1 < 0:
        x1 = 0

    if x2 >= width:
        x2 = width - 1

    return image[y1:y2, x1:x2]

def save_relevant_shapes(image, relevant, wr, hr, ls, save=SAVE_IMAGES, enlarge_factor=BOUNDING_BOX_ENLARGING_FACTOR):

    for descriptor in relevant:

        height, width, _ = image.shape

        bb = descriptor["bounding_box"]
        label = descriptor["name"]

        y1, x1, y2, x2 = [int(floor(bb[0] / hr)), int(floor(bb[1] / wr)), int(ceil(bb[2] / hr)), int(ceil(bb[3] / wr))]

        bbw = x2-x1
        bbh = y2-y1

        ext_w = int(bbw*enlarge_factor/2)
        ext_h = int(bbh*enlarge_factor/2)

        y1 -= ext_h
        y2 += ext_h
        x1 -= ext_w
        x2 += ext_w

        if y1 < 0:
            y1 = 0

        if y2 >= height:
            y2 = height - 1

        if x1 < 0:
            x1 = 0

        if x2 >= width:
            x2 = width - 1

        cropped = image[y1:y2, x1:x2]

        c_h, c_w, _ = cropped.shape

        if c_h > 0 and c_w > 0:

            ls.add_img(cropped)

            path = os.path.join(IMAGES_DIRECTORY, "cap-" + label + "-" + datetime.datetime.strftime(datetime.datetime.now(), DATE_FORMAT)  + ".jpg")

            if save:
                cv2.imwrite(path, cropped)

def crop_image(image, bb=IMAGE_BB):
    height, width, _ = image.shape

    x1 = int(bb[0] * width)
    y1 = int(bb[1] * height)
    x2 = int(bb[2] * width)
    y2 = int(bb[3] * height)

    # print "BB: ", x1, y1, x2-1, y2-1

    return image[y1:y2, x1:x2].copy()

def preprocess_image(image, histogram_eq=HISTOGRAM_EQUALIZE_BEFORE_PROCESSING, megapixels=MEGAPIXELS, to_hsv=True):

    height, width, _ = image.shape

    small, wr, hr = resize(image, megapixels)

    if histogram_eq:
        small = histogram_equalize(small.astype(np.uint8))

    h, w, _ = small.shape

    blurred = cv2.GaussianBlur(small, (5, 5), 0).astype(np.uint8)

    if to_hsv:
        blurred = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

    return blurred, wr, hr

def recognize_single_frame_shapes(image, region_growing_threshold=REGION_GROWING_THRESHOLD,
                                  min_shape_contour_points=MIN_SHAPE_CONTOUR_POINTS):

    plots = load_shapes_plots()

    return fast_shape_matching.shape_matching(image, region_growing_threshold, plots, SHAPE_MATCH_THRESHOLD,
                                                             MAX_DIFF_THRESHOLD,
                                                             MAX_DIFF_ON_MEAN,
                                                             CONTOUR_PLOT_POINTS,
                                                             min_shape_contour_points)

def recognize_single_frame_shapes_2(image, diff_data, min_shape_contour_points=MIN_SHAPE_CONTOUR_POINTS):

    plots = load_shapes_plots()

    return fast_shape_matching.shape_matching_2(image, diff_data, plots, SHAPE_MATCH_THRESHOLD,
                                                             MAX_DIFF_THRESHOLD,
                                                             MAX_DIFF_ON_MEAN,
                                                             CONTOUR_PLOT_POINTS,
                                                             min_shape_contour_points)

def recognize_single_frame_shapes_fast(image, region_growing_threshold=REGION_GROWING_THRESHOLD, min_shape_contour_points=MIN_SHAPE_CONTOUR_POINTS):

    return fast_shape_matching.shape_matching_fast(image, region_growing_threshold, CONTOUR_PLOT_POINTS, min_shape_contour_points, SHAPE_RECOGNITION_PARAMETERS)

def histogram_equalize(img):

    img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)

    # equalize the histogram of the Y channel

    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(2, 2))
    img_yuv[:, :, 0] = clahe.apply(img_yuv[:, :, 0])

    # convert the YUV image back to RGB format
    return cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)

def extimate_region_growing_threshold(image, bb=REGION_GROWING_THRESHOLD_EXTIMATION_BB,
                                      factor=REGION_GROWING_THRESHOLD_EXTIMATION_FACTOR,
                                      vector=REGION_GROWING_THRESHOLD_EXTIMATION_VECTOR,
                                      poly=REGION_GROWING_THRESHOLD_EXTIMATION_POLY):

    height, width, _ = image.shape

    x1 = int(bb[0] * width)
    y1 = int(bb[1] * height)
    x2 = int(bb[2] * width)
    y2 = int(bb[3] * height)

    cropped = image[y1:y2, x1:x2]

    cropped = cv2.cvtColor(cropped, cv2.COLOR_BGR2HSV)

    std = np.std(cropped)
    mean = np.mean(cropped[:, :, 2])

    x = mean + factor*std

    print " Mean: ", mean, " X: ", x

    return np.polyval(poly, x)

def elaborate_single_frame(image, ls, capture_time):

    begin = datetime.datetime.now()

    preprocessed_image, wr, hr = preprocess_image(image)
    recognition_results = recognize_single_frame_shapes(preprocessed_image)

    end = datetime.datetime.now()
    timing = end - begin

    save_relevant_shapes(image, recognition_results, wr, hr, ls)
    print_relevant_shapes(image, recognition_results, wr, hr)
    print_fps(image, timing + capture_time)


class LastSeen:
    def __init__(self, img_per_column=5, columns=3, pixels=100):
        self._img_per_column = img_per_column
        self._columns = columns
        self._max_img = img_per_column * columns
        self._pixels = pixels
        self._imglist = []

    def resize_img(self, img):

        height, width, _ = img.shape

        print "SIZE resize: ", height, width

        if height > width:
            resize_ratio = float(self._pixels) / height
        else:
            resize_ratio = float(self._pixels) / width

        return cv2.resize(img, (int(width*resize_ratio), int(height*resize_ratio)))

    def add_img(self, img):
        if len(self._imglist) >= self._max_img:
            del self._imglist[0]

        self._imglist.append(self.resize_img(img))

    def get_top_left_point(self, linear_index):

        x = int(floor(float(linear_index)/self._img_per_column))
        y = linear_index - x*self._img_per_column

        return y*self._pixels,x*self._pixels

    def get_grid(self):

        result = np.zeros((self._pixels*self._img_per_column, self._pixels*self._columns, 3))

        for i, img in enumerate(self._imglist):
            h,w,_ = img.shape
            y, x = self.get_top_left_point(i)
            result[y:y+h, x:x+w] = img

        return result