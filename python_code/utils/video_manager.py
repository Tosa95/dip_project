import cv2

FORMATS = {
    "MJPG": ["MJPG", "avi"],
    "X264": ["X264", "avi"],
    "XVID": ["XVID", "avi"]
}

class VideoManager:

    def __init__(self, filename, codec="XVID", fps=30.0, imgshape=None):
        self._codec = codec
        self._fps = fps
        self._imgshape = imgshape
        self._writer = None
        self._filename = filename

    def init_writer(self):
        codec = FORMATS[self._codec][0]
        extension = FORMATS[self._codec][1]
        fourcc = cv2.cv.CV_FOURCC(*codec)
        full_filename = "%s.%s" % (self._filename, extension)
        self._writer = cv2.VideoWriter(full_filename,fourcc, 60.0, self._imgshape)

    def add_frame(self, frame):

        if self._writer is None:
            if self._imgshape is None:
                h,w,_ = frame.shape
                self._imgshape = (w,h)
                self.init_writer()

        self._writer.write(frame)


    def close(self):
        if self._writer is not None:
            self._writer.release()
            self._writer = None