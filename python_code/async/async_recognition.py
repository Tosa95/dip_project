import Queue
import datetime

import cv2
from PyQt4 import QtCore
from PyQt4.QtCore import pyqtSignal, QObject, QThread

from config import logger
from lib.recognition_utils import LastSeen, elaborate_single_frame


class AsyncRecognitor(QThread):
    list_signals = pyqtSignal(list)

    def __init__(self):
        QThread.__init__(self)

        self._isRunning = True

    def get_list_signals(self):
        return self.list_signals

    def run(self):

        try:
            ls = LastSeen()
            cam = cv2.VideoCapture(0)
            while self._isRunning:
                begin = datetime.datetime.now()
                ret, frame = cam.read()
                end = datetime.datetime.now()
                # print end - begin
                elaborate_single_frame(frame, ls, end - begin)
                self.list_signals.emit([frame, ls])

            cam.release()
        except Exception as e:
            logger.error(str(e))
            print e

    def stop(self):
        self._isRunning = False
