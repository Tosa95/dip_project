import Queue
import datetime
import os
from threading import Thread

import cv2
from PyQt4 import QtCore
from PyQt4.QtCore import pyqtSignal, QObject, QThread

from config import logger, SAVE_IMAGES, SAVE_FULL_FRAMES, FULL_FRAMES_MODE_VIDEO, FULL_FRAMES_VIDEO_PARAMS, \
    FULL_FRAMES_MODE_SINGLE_FRAMES, FULL_FRAMES_DIRECTORY, DATE_FORMAT, FULL_FRAMES_VIDEO_NAME
from lib.recognition_utils import LastSeen, elaborate_single_frame, save_relevant_shapes, print_relevant_shapes, \
    print_fps, print_save_state, print_th_extimation_bb, crop_image, save_full_frame
from multicore_recognition import MultithreadRecognizer
from utils.video_manager import VideoManager


class MulticoreAsyncRecognitor(QThread):
    list_signals = pyqtSignal(list)

    def __init__(self, processes=4, required_fps=20.0, send_images_every_secs=0.15,
                 avg_pers_factor=10.0, save=SAVE_IMAGES, save_full_frames=SAVE_FULL_FRAMES,
                 full_frames_mode=FULL_FRAMES_MODE_SINGLE_FRAMES, full_frames_video_params=FULL_FRAMES_VIDEO_PARAMS):
        QThread.__init__(self)

        self._send_images_every_secs = send_images_every_secs
        self._isRunning = True
        self._save = save
        self._save_full_frames = save_full_frames
        self._avg_pers_factor = avg_pers_factor

        self._video_writer = None
        self._full_frames_mode = full_frames_mode
        self._full_frames_video_params = full_frames_video_params
        self._frames_queue = Queue.Queue(maxsize=5)
        self._stop_frames_writer = False

        self._frame_writer_thread = Thread(target=self.frames_writer_body)
        self._frame_writer_thread.start()

        self._multicore_recognizer = MultithreadRecognizer(max_enqueued_frames=processes,
                                                           recognition_processes_number=processes,
                                                           required_fps=required_fps)

    def frames_writer_body(self):

        while not self._stop_frames_writer:
            frame = self._frames_queue.get(timeout=1.0)
            if self._video_writer is None:
                file_path = os.path.join(FULL_FRAMES_DIRECTORY,
                                    FULL_FRAMES_VIDEO_NAME % datetime.datetime.strftime(datetime.datetime.now(),
                                                                          DATE_FORMAT))

                self._video_writer = VideoManager(file_path, **self._full_frames_video_params)

            self._video_writer.add_frame(frame)


    def get_list_signals(self):
        return self.list_signals

    def save_full_frame(self, frame):
        if self._full_frames_mode == FULL_FRAMES_MODE_SINGLE_FRAMES:
            save_full_frame(frame)
        else:
            self._frames_queue.put(frame)

    def run(self):
        try:
            self._multicore_recognizer.start()
            ls = LastSeen()
            last = datetime.datetime.now()
            last_sent = datetime.datetime.now()
            avg = 0.1
            while self._isRunning:
                recognized = self._multicore_recognizer.get_recognized_queue().get()
                current_time = datetime.datetime.now()
                frame = recognized["frame"]
                wr = recognized["wr"]
                hr = recognized["hr"]
                recognition_results = recognized["recognition_result"]

                timing = (current_time - last).total_seconds()

                avg = (avg*self._avg_pers_factor + timing)/(self._avg_pers_factor + 1.0)

                last = current_time

                save_relevant_shapes(frame, recognition_results, wr, hr, ls, save=self._save)
                if self._save_full_frames:
                    self.save_full_frame(frame)
                print_relevant_shapes(frame, recognition_results, wr, hr)
                print_th_extimation_bb(frame)
                print_fps(frame, avg)
                print_save_state(frame, self._save, self._save_full_frames)

                if (current_time - last_sent).total_seconds() > self._send_images_every_secs:
                    self.list_signals.emit([frame, ls])
                    last_sent = current_time

        except Exception as e:
            logger.error(str(e))
            print e
            self.stop()
            raise

    def stop(self):
        self._stop_frames_writer = True
        self._multicore_recognizer.stop()
        self._isRunning = False

        if self._video_writer is not None:
            self._video_writer.close()
            self._video_writer = None
