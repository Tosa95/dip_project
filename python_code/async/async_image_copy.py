import Queue
import datetime
from time import sleep

import cv2
from PyQt4 import QtCore

import requests
from PyQt4.QtCore import pyqtSignal, QObject, QThread

from config import logger, IMAGES_DIRECTORY, FILES_PER_REQUEST, UPLOAD_IMAGE_URL, GARBAGE_SCORE_THRESHOLD
from copy_images import get_dir_images, chunks, open_all, close_all
from lib.recognition_utils import LastSeen, elaborate_single_frame
from nn.image_score import ImageScorer


class AsyncImageCopier(QThread):
    progress = pyqtSignal(float)
    messages = pyqtSignal(str)
    end = pyqtSignal(int)

    def __init__(self):
        QThread.__init__(self)

        self._isRunning = True
        self._finished = False
        self._imgscr = ImageScorer()

    def get_list_signals(self):
        return self.list_signals

    def remove_garbage(self, files, th=GARBAGE_SCORE_THRESHOLD):

        self.messages.emit("Removing garbage...")
        self.progress.emit(0)

        imgscr = self._imgscr

        res = []

        for i, img_file in enumerate(files):

            if i%100 == 0:
                msg = "Removing garbage: examined %d(%.2f%%) of %d, kept %d(%.2f%%)" % (
                    i+1,
                    float(i+1)/len(files)*100,
                    len(files),
                    len(res),
                    float(len(res))/(i+1)*100
                )
                self.messages.emit(msg)

            try:

                path = img_file.get_abs_path()

                img = cv2.imread(path)

                prediction, score = imgscr.get_score_from_img(img)

                if prediction>th:
                    res.append(img_file)

                self.progress.emit(float(i + 1) / len(files) * 100)
                if not self._isRunning:
                    break

            except Exception as ex:

                pass

        return res

    def run(self):

        files = get_dir_images(IMAGES_DIRECTORY)

        print len(files)

        files = self.remove_garbage(files)

        print len(files)

        chunked_files = list(chunks(files, FILES_PER_REQUEST))

        self.messages.emit("Uploading...")
        self.progress.emit(0)

        for i, chunk in enumerate(chunked_files):

            try:

                open_all(chunk)

                file_dict = {}

                for f in chunk:
                    file_dict[f.filename] = f.fd

                if not self._isRunning:
                    break

                print "Chunk %d on %d" % (i, len(chunked_files))
                r = requests.post(UPLOAD_IMAGE_URL, files=file_dict)
                print r.json()

                if not self._isRunning:
                    break

                if r.json()["status"] != "OK":
                    self.messages.emit("Errore: " + str(r.json()))
                    print "Errore: " + str(r.json())

            except Exception as e:

                self.messages.emit("Errore: " + str(e))
                print "Errore: " + str(e)

            finally:

                close_all(chunk)

            self.progress.emit(float(i+1)/len(chunked_files) * 100)

        self.end.emit(0)
        self._finished = True

    def has_finished(self):
        return self._finished

    def stop(self):
        self._isRunning = False
        while not self._finished:
            sleep(0.5)
