import advanced_region_growing


def shape_matching (image, region_growing_threshold, base_shapes, shape_matching_threshold, max_diff_threshold, max_diff_on_mean,
                    num_points, min_bound_points):

    bs = {}

    for shape_name, shape_contours in base_shapes.iteritems():
        bs[shape_name] = [sc.tolist() for sc in shape_contours]

    result = advanced_region_growing.find_relevant_shapes(image, region_growing_threshold, bs, shape_matching_threshold,
                                                        max_diff_threshold, max_diff_on_mean, num_points, min_bound_points)

    return result

def shape_matching_2 (image, diff_data, base_shapes, shape_matching_threshold, max_diff_threshold, max_diff_on_mean,
                    num_points, min_bound_points):

    bs = {}

    for shape_name, shape_contours in base_shapes.iteritems():
        bs[shape_name] = [sc.tolist() for sc in shape_contours]

    result = advanced_region_growing.find_relevant_shapes_2(image, diff_data, bs, shape_matching_threshold,
                                                        max_diff_threshold, max_diff_on_mean, num_points, min_bound_points)

    return result

def regions_descriptors (image, region_growing_threshold, num_points, min_bound_points):

    result = advanced_region_growing.get_shapes_descriptors(image, region_growing_threshold, num_points, min_bound_points)

    return result

def shape_matching_fast (image, region_growing_threshold, num_points, min_bound_points, rec_data):

    result = advanced_region_growing.find_relevant_shapes_fast(image, region_growing_threshold, num_points, min_bound_points, rec_data)

    return result
