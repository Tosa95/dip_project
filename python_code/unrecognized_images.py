import os
import pickle
import random
from Queue import Queue

import time

from datetime import datetime
from threading import Thread

import cv2

from lib.recognition_utils import preprocess_image, recognize_single_frame_shapes_2, print_relevant_shapes
from parameters_extimation.extimation_utils import shape_found, chunks

BESTFILE = 'best_comb_2'
MEGAPIXELS = 0.02
MIN_CONTOUR = 10

N_THREADS = 8

DATASETS_FOLDER = "/home/davide/datasets"
DATASET = os.path.join(DATASETS_FOLDER, "images2018-11-19 13:00:40.341455.pickle")
# DATASET = os.path.join(DATASETS_FOLDER, "images2018-11-14 14:42:33.259033.pickle")
# DATASET = os.path.join(DATASETS_FOLDER, "images2018-11-21 12:14:03.032878.pickle")

os.chdir(os.path.split(os.getcwd())[0])

with open(BESTFILE, "rb") as input:
    best_individual = pickle.load(input)

with open(DATASET, "rb") as input:
    dataset = pickle.load(input)
    random.shuffle(dataset)

chunked_dataset = list(chunks(dataset, int(round(float(len(dataset))/N_THREADS))))


print best_individual.fit_eval, len(dataset), best_individual.genes

def eval(diff_data):

    q = Queue()

    def eval_part(chunk):

        tot_rec = 0
        tot_time = 0

        unrecognized = []
        recognized = []

        for data in chunk:
            img = data["img"]
            square_data = data["square_data"]

            img_sz, _, _ = img.shape

            preprocessed_frame, wr, hr = preprocess_image(img, megapixels=MEGAPIXELS)

            before = datetime.now()

            recognition_result = recognize_single_frame_shapes_2(preprocessed_frame,
                                                                 diff_data=diff_data,
                                                                 min_shape_contour_points=MIN_CONTOUR)

            # print recognition_result, square_data

            time = (datetime.now() - before).total_seconds()

            print_relevant_shapes(img, recognition_result, wr, hr)

            if not shape_found(recognition_result, square_data, img_sz, wr, hr):
                unrecognized.append(img)
                print "UNREC FOUND"
            else:
                recognized.append(img)

            tot_time += time
            tot_rec += 1 if shape_found(recognition_result, square_data, img_sz, wr, hr) else 0



            # print "TIME: ", time

        q.put((tot_rec, tot_time, unrecognized, recognized))

    threads = [Thread(target=eval_part, args=(chunk, )) for chunk in chunked_dataset]

    for t in threads:
        t.start()

    for t in threads:
        t.join()

    # print "HEREEEEE", chunked_dataset

    tot_time = 0
    tot_rec = 0
    unrec = []
    rec_l = []

    while not q.empty():
        rec, tm, ur, r = q.get()

        tot_time += tm
        tot_rec += rec
        unrec.extend(ur)
        rec_l.extend(r)

    return unrec, rec_l

unrecs, recs = eval(best_individual.genes)

print len(unrecs), len(recs)

for img in recs:
    cv2.imshow("img", img)

    if cv2.waitKey(2) == 27:
        break

    time.sleep(0.3)

for img in unrecs:
    cv2.imshow("img", img)

    if cv2.waitKey(2) == 27:
        break

    time.sleep(0.3)

