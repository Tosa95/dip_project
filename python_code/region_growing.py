import pprint
from random import randint

import numpy as np
import cv2

from lib.area_descriptor import AreaDesriptor
from lib.hierarchic_regions import BFS
from lib.sparse_data import SparseArray


class RegionMap:

    @classmethod
    def empty(cls, height, width):
        map = np.full((height + 2, width + 2), -1, dtype=np.int32)
        return cls(map)


    def __init__(self, numpy_matrix):

        height, width = numpy_matrix.shape

        self._height = height
        self._width = width

        self._map = numpy_matrix

    def get_label(self, row, col):

        if row >= self._height or row < 0 or col >= self._width or col < 0:
            return 0

        return self._map[row][col]

    def set_label(self, row, col, label):

        self._map[row][col] = label

    def get_height(self):

        return  self._height

    def get_width(self):

        return self._width


def draw_exernal_rect(map):

    height, width, = map.shape

    for row in xrange(height):
        map[row][0] = 0
        map[row][width - 1] = 0

    for col in xrange(width):
        map[0][col] = 0
        map[height - 1][col] = 0


def color_distance(c1, c2):
    return np.sum(abs(c1.astype(np.double)-c2.astype(np.double)))


def grow_region(row, col, label, regions, map, image, th, dist_from_orig=True):

    height, width, _ = image.shape

    points = [(row, col)]

    orig_color = image[row, col, :]

    region = AreaDesriptor(label)

    near_areas = set()

    map.set_label(row, col, label)

    while len(points) > 0:

        current_row = points[0][0]
        current_col = points[0][1]

        grow_region.iterations += 1

        del points[0]

        color = image[current_row,current_col,:]

        bound = False

        region.add_point(np.array([current_row, current_col]), image[current_row, current_col, :])

        for row_step in [-1, 0, 1]:
            for col_step in [-1, 0, 1]:

                if row_step != 0 or col_step != 0:

                    # print grow_region.i
                    # grow_region.i += 1

                    grow_region.square_check += 1

                    near_row = current_row + row_step
                    near_col = current_col + col_step

                    if height > near_row >= 0 and width > near_col >= 0:

                        if map.get_label(near_row, near_col) != -1 and map.get_label(near_row, near_col) != label \
                                and abs(row_step) + abs(col_step) < 2:

                            bound = True
                            #print label, map.get_label(near_row, near_col)
                            near_areas.add(map.get_label(near_row, near_col))

                        elif map.get_label(near_row, near_col) == -1:

                            if dist_from_orig:
                                dist = color_distance(orig_color, image[near_row,near_col,:])
                            else:
                                dist = color_distance(region.get_average(), image[near_row,near_col,:])

                            if dist <= th:
                                points.append((near_row, near_col))
                                map.set_label(near_row, near_col, label)
                                grow_region.num_queued += 1

                            elif abs(row_step) + abs(col_step) < 2:
                                bound = True
                                pass

                    elif abs(row_step) + abs(col_step) < 2:

                        bound = True
                        near_areas.add(0)

        if bound:
            region.add_bound_point((current_row, current_col))



    for adiacent in near_areas:

        region.get_adiacent_areas().add(adiacent)
        regions[adiacent].get_adiacent_areas().add(label)

    region.grow_end()


    return region

grow_region.num_queued = 0
grow_region.iterations = 0
grow_region.square_check = 0

def region_growing_slow(image, th, dist_from_orig = False):

    height, width, _ = image.shape

    regions = []

    regions.append(AreaDesriptor(0))
    label = 1

    map = RegionMap.empty(height, width)

    i = 0

    for row in xrange(height):
        for col in xrange(width):
            if map.get_label(row,col) == -1:

                reg = grow_region(row, col, label, regions, map, image, th, dist_from_orig)
                regions.append(reg)
                label += 1


    print "queued: " + str(grow_region.num_queued)
    print "iterations: " + str(grow_region.iterations)
    print "square_check: " + str(grow_region.square_check)

    return regions, map

def region_growing_fast(image, th, dist_from_orig = False):

    import advanced_region_growing
    c_res = advanced_region_growing.region_growing(image.astype(np.uint8), int(th))


    regions = [AreaDesriptor.get_from_c_region_growing(reg) for reg in c_res["regions"]]
    map = c_res["map"]

    return regions,  RegionMap(map)


def draw_regions (map, regions, draw_bounds=False, draw_bounding_boxes=False, draw_distance=False):

    res = np.zeros((map.get_height(), map.get_width(), 3), np.uint8)

    for row in xrange(map.get_height()):
        for col in xrange(map.get_width()):

            if draw_distance:
                res[row,col,:] = regions[map.get_label(row,col)].get_distance()/7.0*255
            else:
                res[row, col, :] = regions[map.get_label(row,col)].get_average().astype(np.uint8)

    for region in regions:

        if draw_bounds:

            for bound in region.get_bound_points():
                res[bound[0], bound[1], :] = np.array([0, 0, 0])

    for region in regions:

        if draw_bounding_boxes:

            if region.get_num_points() > 20:
                bb = region.get_bounding_box()

                cv2.rectangle(res, (bb[1], bb[0]), (bb[3], bb[2]), (randint(50,255), randint(50,255), randint(50,255)), 1)

    return res

def region_growing(image, th, dist_from_orig = False):

    try:
        import advanced_region_growing
        return region_growing_fast(image, th, dist_from_orig)
    except ImportError, e:
        return region_growing_slow(image, th, dist_from_orig)


