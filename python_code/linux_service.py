#! /usr/bin/python
### BEGIN INIT INFO
# Provides:          smartail
# Required-Start:    $network $remote_fs $syslog bluetooth
# Required-Stop:     $network $remote_fs $syslog bluetooth
# Default-Start:     5
# Default-Stop:      0 1 6
# Short-Description: Start daemon at boot time
# Description:       Enable service provided by daemon.
### END INIT INFO
# -*- coding: utf-8 -*-
import os
import subprocess
import time

import sys

NAME = "segnali"
USER = "pi"
PROGRAM = "/usr/bin/python"
PARAMETERS = "/home/pi/sito_segnali/python_service/service.py"
PID_FILE = os.path.join("/var", "run", NAME + ".pid")

def start():

    p = subprocess.Popen(["start-stop-daemon --start --background --pidfile", PID_FILE, "--make-pidfile --user", USER, "--chuid", USER, "--exec", PROGRAM, "--", PARAMETERS])

def stop():

    p = subprocess.Popen(["start-stop-daemon --stop --pidfile", PID_FILE, "--retry 10"])


if __name__ == "__main__":

    print sys.argv

    action = sys.argv[1]

    if action == "start":
        start()
        print "Started"
    elif action == "stop":
        stop()
        print "Stopped"
