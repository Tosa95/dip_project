from unittest import TestCase

from lib.area_descriptor import AreaDesriptor


class TestAreaDesriptor(TestCase):

    def test_base_usage(self):

        ad = AreaDesriptor(1)

        ad.add_point([1, 2], [128, 128, 128],False)
        ad.add_point([5, 2], [234, 128, 128], False)
        ad.add_point([1, 7], [128, 11, 128], True)

        ad.grow_end()

        self.assertEqual(ad.get_num_points(), 3)
        self.assertEqual(ad.get_num_points_bound(), 1)
        self.assertEqual(ad.get_average()[0], (128+234+128)/3.0)
        self.assertEqual(ad.get_average()[1], (128+128+11)/3.0)
        self.assertEqual(ad.get_average()[2], (128+128+128)/3.0)
        self.assertEqual(ad.get_centroid()[0], (1+5+1)/3.0)
        self.assertEqual(ad.get_centroid()[1], (2+2+7)/3.0)
