import random
import sys
from unittest import TestCase

from lib.sparse_data import SparseMatrix


class TestSparseData(TestCase):

    def test_base_interaction(self):

        sd = SparseMatrix()

        sd.set(2, 3, 100)
        sd.set(2, 4, 200)
        sd.set(1000, 1000, 1)

        self.assertEqual(sd.get(2, 3), 100)
        self.assertEqual(sd.get(2, 4), 200)
        self.assertEqual(sd.get(1000, 1000), 1)
        self.assertEqual(sd.get(2, 5), 0)
        self.assertEqual(sd.get(10,1000000000), 0)


        for i in xrange(30000):

            row = random.randint(0,30000)
            col = random.randint(0,30000)

            sd.set(row,col, 1)

        print sys.getsizeof(sd._data)

        self.assertLess(sys.getsizeof(sd._data), 1000000)