import advanced_region_growing
import cv2

import numpy as np

from lib import hierarchic_regions_c, shape_matching
from lib.area_descriptor import AreaDesriptor
from lib.img_utils import resize

import matplotlib as mpl

from lib.shape_matching import get_shape_distance, compute_contour_plot, get_correlation

mpl.use('Agg')
import matplotlib.pyplot as plt

if __name__=="__main__":

            image = cv2.imread("../test_images/stop_sign.jpg", cv2.IMREAD_COLOR).astype(np.double)

            small, wr, hr = resize(image, 0.05)

            h, w, _ = small.shape

            blurred = cv2.GaussianBlur(small, (5, 5), 0).astype(np.double)

            THRESHOLD = 70

            regions, map, _ = hierarchic_regions_c.hierarchic_region_growing(blurred.astype(np.uint8), THRESHOLD)


            #print reg

            #ordered = BFS(regions)

            # print "here2"

            #subregions(regions, ordered)

            # print "here3"
            print "Regions: %d" % len(regions)


            lbl = map.get_label(int(h/2), int(w/2))
            # collapsed_orig = collapse_inner_regions(map.get_label(int(h/2), int(w/2)), regions, map)
            # collapsed = AreaDesriptor.get_from_c_region_growing(advanced_region_growing.test_region_collapse(blurred.astype(np.uint8), 200, lbl))
            correlation = advanced_region_growing.test_correlation(blurred.astype(np.uint8), THRESHOLD, 41, 13)
            distance = advanced_region_growing.test_shape_distance(blurred.astype(np.uint8), THRESHOLD, 41, 13)

            collapsed1 = AreaDesriptor.get_from_c_region_growing(advanced_region_growing.test_region_collapse(blurred.astype(np.uint8), THRESHOLD, 41))
            collapsed2 = AreaDesriptor.get_from_c_region_growing(advanced_region_growing.test_region_collapse(blurred.astype(np.uint8), THRESHOLD, 13))

            contour1 = advanced_region_growing.compute_contour_plot(collapsed1.get_bound_points(), list(collapsed1.get_centroid()), 128)
            contour2 = advanced_region_growing.compute_contour_plot(collapsed2.get_bound_points(), list(collapsed2.get_centroid()), 128)

            contour1_orig = compute_contour_plot(collapsed1, 128)
            contour2_orig = compute_contour_plot(collapsed2, 128)

            dist_orig = get_shape_distance(contour1_orig, contour2_orig)

            correlation_orig = get_correlation(contour1, contour2)

            print "Old ff: " + str(np.fft.fft(contour1))
            print max(correlation)
            print correlation
            print collapsed1
            print collapsed2
            print "DISTANCE: %f ORIGINAL: %f" % (distance, dist_orig)

            # pp = pprint.PrettyPrinter(indent=4)
            # pp.pprint(regions)

            displacement = np.argmax(correlation)

            # print collapsed
            #
            # if collapsed.get_num_points_bound() == 0:
            #     collapse_inner_regions(map.get_label(int(h / 2 ), int(w / 2)), regions, map)

            # contour_points = collapsed.get_bound_points()
            #
            # print "points: " + str(len(contour_points))
            #
            # begin = datetime.now()
            contour_plot_orig = shape_matching.compute_contour_plot(collapsed2, 128)
            # print datetime.now() - begin
            #
            # begin = datetime.now()
            # contour_plot = advanced_region_growing.compute_contour_plot(contour_points, list(collapsed.get_centroid()), 128)
            # contour_plot_orig = advanced_region_growing.compute_contour_plot(contour_points, list(collapsed_orig.get_centroid()),
            #                                                             128)
            # print datetime.now() - begin
            #
            # print "Mean of diff " + str(np.mean(np.abs(np.array(contour_plot) - np.array(contour_plot_orig))))
            #
            graph_num = str(999)

            print np.argmax(correlation)
            print np.argmax(correlation_orig)

            plt.figure(1)
            plt.clf()
            plt.subplot(2,1,1)
            plt.title("Correlation")
            plt.plot(range(128),correlation)
            plt.plot(range(128), correlation_orig)
            plt.subplot(2, 1, 2)
            plt.title("Contours")
            plt.plot(range(128), contour1)
            plt.plot(range(128), np.roll(contour2, np.argmax(correlation_orig)))
            plt.draw()
            plt.savefig("../graphs/test" + graph_num + ".png", dpi=60)

            # cv2.imshow('original', image.astype(np.uint8))
            # cv2.imshow('blurred', blurred.astype(np.uint8))
            # cv2.waitKey(0)
            # cv2.destroyAllWindows()

            # print get_shape_distance(np.flip(np.array(contour_plot),0), contour_plot_orig)