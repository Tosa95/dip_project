from unittest import TestCase

from lib.sparse_data import SparseArray


class TestSparseArray(TestCase):

    def test_basic_usage(self):

        sa = SparseArray()

        sa.set(12, 34)
        sa.set(1000000000, 83)

        self.assertEqual(sa.get(12), 34)
        self.assertEqual(sa.get(1000000000), 83)
        self.assertEqual(sa.get(2345), 0)






