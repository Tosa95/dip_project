from unittest import TestCase

from region_growing import RegionMap


class TestRegionMap(TestCase):

    def test_base_usage(self):

        rm = RegionMap(12, 12)

        rm.set_label(11, 11, 2)
        rm.set_label(0, 0, 3)
        rm.set_label(7, 9, 5)
        rm.set_label(11, 11, 8)
        rm.set_label(2, 1, 167253753)

        self.assertEqual(rm.get_label(11, 11), 8)
        self.assertEqual(rm.get_label(0, 0), 3)
        self.assertEqual(rm.get_label(7, 9), 5)
        self.assertEqual(rm.get_label(2, 1), 167253753)
        self.assertEqual(rm.get_label(7, 7), -1)
        self.assertEqual(rm.get_label(-1, 0), 0)
        self.assertEqual(rm.get_label(-1, -1), 0)
        self.assertEqual(rm.get_label(12, 12), 0)
        self.assertEqual(rm.get_label(11, 12), 0)




