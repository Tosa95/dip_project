import cv2
import datetime

import numpy as np

from lib.recognition_utils import LastSeen, elaborate_single_frame


ls = LastSeen()

cam = cv2.VideoCapture(0)
# cam.set(cv2.cv.CV_CAP_PROP_BRIGHTNESS, 0.40)
# cam.set(cv2.cv.CV_CAP_PROP_EXPOSURE, 0.0)
# cam.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, 640)
# cam.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, 480)

while True:
    begin = datetime.datetime.now()
    ret, frame = cam.read()
    end = datetime.datetime.now()
    print end-begin
    elaborate_single_frame(frame, ls, end-begin)
    cv2.imshow('result', frame.astype(np.uint8))
    cv2.imshow('last_seen', ls.get_grid().astype(np.uint8))
    if cv2.waitKey(1) == 27:
        break

cam.release()
cv2.destroyAllWindows()
