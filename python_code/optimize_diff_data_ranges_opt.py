import random
from Queue import Queue
from datetime import datetime
from threading import Thread

import cv2
import os
import pickle

from lib.recognition_utils import preprocess_image, recognize_single_frame_shapes_2, get_full_res_bb
from parameters_extimation.genetic_programming import GeneticAlgorithm, get_mutate_random_gene_fn, get_mutate_with_p_fn, \
    get_eval_pop_fn, get_standard_cross_over_fn, get_standard_next_gen_fn, get_no_improve_terminate_fn

DATASETS_FOLDER = "/home/davide/datasets"
DATASET = os.path.join(DATASETS_FOLDER, "images2018-11-19 13:00:40.341455.pickle")

IMAGES_KEPT = 990

N_THREADS = 8
ACCEPTED_ERROR = 0.05

N_PARAMS = 3*6+4*3
POP_SIZE = 100

MAX_GEN_NO_IMPROVE = 1000

GENFILE = "last_gen_ranges_2"
BESTFILE = "best_ranges_2"

in_q = Queue()
out_q = Queue()

def eval_part():

    data = in_q.get()

    while data[0] is not None:

        chunk, diff_data = data

        tot_rec = 0
        tot_time = 0

        for data in chunk:
            img = data["img"]
            square_data = data["square_data"]

            img_sz, _, _ = img.shape

            preprocessed_frame, wr, hr = preprocess_image(img, megapixels=0.03)

            before = datetime.now()

            recognition_result = recognize_single_frame_shapes_2(preprocessed_frame,
                                                                 diff_data=diff_data,
                                                                 min_shape_contour_points=20)

            # print recognition_result, square_data

            time = (datetime.now() - before).total_seconds()

            tot_time += time
            tot_rec += 1 if shape_found(recognition_result, square_data, img_sz, wr, hr) else 0

            # print "TIME: ", time

        out_q.put((tot_rec, tot_time))

        data = in_q.get()

threads = [Thread(target=eval_part) for i in xrange(N_THREADS)]

def start_eval_threads():
    for t in threads:
        t.start()

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

with open(DATASET, "rb") as input:
    dataset = pickle.load(input)
    random.shuffle(dataset)


chunked_dataset = list(chunks(dataset[:IMAGES_KEPT], int(round(float(len(dataset))/N_THREADS))))

def shuffle_dataset():
    global chunked_dataset
    random.shuffle(dataset)
    chunked_dataset = list(chunks(dataset[:IMAGES_KEPT], int(round(float(len(dataset))/N_THREADS))))

print len(dataset)

def shape_found(recognition_result, square_data, img_sz, wr, hr):

    for descriptor in recognition_result:
        given_bb = descriptor["bounding_box"]

        bb = get_full_res_bb(given_bb, wr, hr)

        x1r = bb[1]
        y1r = bb[3]
        x2r = bb[0]
        y2r = bb[2]

        x1i = square_data[0]
        y1i = square_data[1]
        x2i = square_data[2]
        y2i = square_data[3]

        diffs = [abs(x1r-x1i), abs(y1r-y1i), abs(x2r-x2i), abs(y2r-y2i)]

        err = False

        for d in diffs:

            if float(d)/img_sz > ACCEPTED_ERROR:
                err = True
                break

        if not err:
            return True

    return False



def eval(diff_data):

    for c in chunked_dataset:
        in_q.put((c, diff_data))

    tot_time = 0
    tot_rec = 0

    begin = datetime.now()

    for i in xrange(len(chunked_dataset)):
        rec, tm = out_q.get()

        tot_time += tm
        tot_rec += rec

    print "Eval time: ", datetime.now() - begin, " Tot time: ", tot_time

    return 10 - 0.1*tot_time + tot_rec


def on_new_best_found(data):
    print data.best.fit_eval, sum(data.pop.eval_list)/len(data.pop.eval_list), data.best.genes
    with open(BESTFILE, "wb") as output:
        pickle.dump(data.best, output)

def signal_new_gen(data):
    # shuffle_dataset()
    print data.gen, sum(data.pop.eval_list)/len(data.pop.eval_list)
    with open(GENFILE, "wb") as output:
        pickle.dump(data.pop, output)
    pass

def rand_params():

    res = []

    min_range_size = 20

    for i in xrange(9):
        rangel = random.uniform(0, 256 - min_range_size)
        rangeh = random.uniform(rangel, 256)
        res.append(rangel)
        res.append(rangeh)

    return res

def mutate(f):
    indexf = (random.randint(0, int(len(f)/2)-1))*2
    indexs = indexf + 1
    first = f[indexf]
    second = f[indexs]

    avg = (first + second)/2
    span = abs(second - first)

    shrink = random.uniform(-2, 2)
    move = random.uniform(-100, 100)

    new_avg = avg + move
    new_span = span * (1+shrink)

    new_first = new_avg-new_span/2
    new_second = new_avg+new_span/2

    if new_first < 0:
        new_first = 0

    if new_second > 256:
        new_second = 256

    f[indexf] = new_first
    f[indexs] = new_second

    return f

def print_perc(p):
    print p

mutate_rnd = get_mutate_random_gene_fn([-1, 1, 10, -10, 100, -100])

mutate_with_p = get_mutate_with_p_fn(0.5, mutate)

eval_pop = eval

cross_over = get_standard_cross_over_fn()

next_gen = get_standard_next_gen_fn()

terminate = get_no_improve_terminate_fn(MAX_GEN_NO_IMPROVE)

GA = GeneticAlgorithm(
    mutate_with_p,
    eval_pop,
    cross_over,
    next_gen,
    terminate,
    on_best_changed=on_new_best_found,
    on_new_gen=signal_new_gen,
    on_next_gen_perc_change=print_perc
)

try:
    with open(GENFILE, "rb") as input:
        first_gen = pickle.load(input)
        print "First gen loaded from file"
        print first_gen
except Exception as ex:
    first_gen = [rand_params() for _ in xrange(POP_SIZE)]

start_eval_threads()
result = GA.fit(first_gen)

for _ in xrange(20):
    in_q.put((None, None))

