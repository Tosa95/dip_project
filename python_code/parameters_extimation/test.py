import pickle
import random
from math import sin

from parameters_extimation.genetic_programming import get_mutate_random_gene_fn, get_mutate_with_p_fn, \
    get_standard_cross_over_fn, get_iteration_num_terminate_fn, GeneticAlgorithm, get_standard_next_gen_fn, \
    get_eval_pop_fn, get_no_improve_terminate_fn
from parameters_extimation.probability_extract import random_extract_with_evaluation

# extr = []
# for i in xrange(100):
#     extr.append(random_extract_with_evaluation([1,6,7,89], [5,25,5,5]))
#
# print extr

mutate_rnd = get_mutate_random_gene_fn([-0.1, 0.1, -1, 1, 10, -10, 0.05, -0.05, 0.001, -0.001, 0.0001, -0.0001,
                                        0.000001, -0.000001,  0.00000001, -0.00000001, 100, -100, 1000, -1000])

mutate_with_p = get_mutate_with_p_fn(0.5, mutate_rnd)


extr = []
for i in xrange(100):
    f = [2.0, 2.0, 2.0, 2.0]
    extr.append(mutate_with_p(f))

print extr


cross_over = get_standard_cross_over_fn()

g1 = [1, 1, 1, 1, 1, 1, 1]
g2 = [2, 2, 2, 2, 2, 2, 2]

def term (pop, evaluation, best, best_eval, i):
    return best_eval>0.9

# terminate = term
terminate = get_no_improve_terminate_fn(100)

EXP_RES = 50000

def val(f):
    return sum([i * pow(v, i) + pow(v, 2) + pow(i,2)*pow(v,3) for i, v in enumerate(f)])
    # return sum([pow(v, 2) + pow(i,2)*pow(v,3) for i, v in enumerate(f)])
    # return sum([i * pow(v, i) + sin(1/(abs(v)+0.001)) for i, v in enumerate(f)])
    # return sum([pow(sin(v),i) for i, v in enumerate(f)])
    # return f[0]*f[0]

def eval(f):
    return 1/(abs(EXP_RES - val(f)) + 1) + 0.0000000001 #+ 100000 if f[0]>0 else 0


eval_pop = eval

next_gen = get_standard_next_gen_fn()

def print_state(data):
    print val(data.best.genes), data.best.fit_eval, sum(data.pop.eval_list)/len(data.pop.eval_list), data.best.genes

def signal_new_gen(data):
    print data.gen, sum(data.pop.eval_list)/len(data.pop.eval_list)
    with open("last_gen_t", "wb") as output:
        pickle.dump(data.pop, output)
    pass

GA = GeneticAlgorithm(mutate_with_p, eval_pop, cross_over, next_gen, terminate, on_best_changed=print_state,
                      on_new_gen=signal_new_gen)

def rand_children():
    return [random.uniform(-1, 1) for _ in xrange(5)]

result = GA.fit([rand_children() for _ in xrange(100)])

best, last_gen = result.best, result.pop

print val(best.genes), eval(best.genes), best.genes

# for f in last_gen:
#     print sum(f), eval(f), f
