import random
from heapq import heappush, heappushpop


def true_with_prob(p):

    return random.uniform(0.0, 1.0) < p

def random_square(img_w, img_h, min_e=70, max_e=200):

    e = random.randint(min_e, max_e)

    x = random.randint(0, img_w-e)
    y = random.randint(0, img_h-e)

    return x, x+e, y, y+e

def random_modify_range(r, f=0.2):
    a = r[0]
    b = r[1]

    mul = random.uniform(1-f, 1+f)

    a = int(a*mul)
    b = int(b*mul)

    return (a,b)

def random_modify_range_stretch(r, f=0.2, size_around=2):
    a = r[0]
    b = r[1]

    mul = random.uniform(1 - f, 1 + f)

    avg = float(a+b)/2*mul

    a = int(avg-size_around)
    b = int(avg+size_around)

    return a, b

def random_extract_with_evaluation(pop, eval, min_value=0.0001):

    min_eval = min(eval) - min_value

    eval = [e - min_eval for e in eval]

    max_eval = max(eval)

    eval = [e/max_eval for e in eval]

    tot = 0
    for ev in eval:
        tot += ev

    extracted = random.uniform(0, tot)

    prev = 0

    for i, val in enumerate(eval):

        curr = val + prev

        if prev <= extracted <= curr:
            return pop[i]

        prev = curr

    return pop[0] # TODO: sistema, non dovrebbe mai arrivare qui

def get_random_min_max(min, max, span_min, span_max=1000):

    min_s = random.randint(min, max - span_min)
    max_s = random.randint(min_s + span_min, max)

    while max_s-min_s>span_max:
        min_s = random.randint(min, max - span_min)
        max_s = random.randint(min_s + span_min, max)

    return min_s, max_s

def extract_color(h_range=(0,180), s_range=(0,255), v_range=(0,255)):

    h = random.randint(h_range[0], h_range[1])
    s = random.randint(s_range[0], s_range[1])
    v = random.randint(v_range[0], v_range[1])

    return h,s,v

def get_random_point_satisfying_predicate(min_x, min_y, max_x, max_y, predicate):

    pt = extract_point_bb(min_x, min_y, max_x, max_y)

    while not predicate(pt):
        pt = extract_point_bb(min_x, min_y, max_x, max_y)

    return pt

def extract_point_bb(min_x, min_y, max_x, max_y):

    x = random.randint(min_x, max_x)
    y = random.randint(min_y, max_y)

    return (x, y)

def log_mutate_actions(min_exp, max_exp, step=1):

    exp_10 = min_exp
    res = []

    while exp_10 <= max_exp:
        res.append(pow(10,exp_10))
        exp_10 += step

    res.extend([-r for r in res])

    return res

class KeepKBest:

    def __init__(self, k, key=lambda v: v):
        self.heap = []
        self.k = k
        self.key = key

    def add_value(self, v):
        if len(self.heap) < self.k:
            heappush(self.heap, (self.key(v), v))
        else:
            heappushpop(self.heap, (self.key(v), v))

    def get_items(self):

        if len(self.heap) == 0:
            return self.heap

        return list(zip(*self.heap)[1])

    def get_items_sorted(self):

        if len(self.heap) == 0:
            return self.heap

        return list(sorted(self.get_items(), reverse=True, key=self.key))

    def get_random_item(self):
        return random.choice(self.get_items())