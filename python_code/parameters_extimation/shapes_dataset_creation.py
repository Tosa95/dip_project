import datetime
import os
import pickle
from genericpath import isfile
from os.path import join

import cv2
import numpy as np
import fast_shape_matching
from lib.recognition_utils import preprocess_image
from parameters_extimation.image_generation import enlarge_image, get_randomized_shape
from parameters_extimation.perspective import perspectivize_image, get_camera_matrix
from parameters_extimation.probability_extract import true_with_prob

DATA_FOLDER = "/home/davide/datasets"

DESC_FILE = os.path.join(DATA_FOLDER, "descriptors_decimated_11")

def save(dataset):
    with open(DESC_FILE, "wb") as output:
        pickle.dump(dataset, output)

def add_relevant_shapes(descriptors, imgs, randomize=False):

    r_step_xy = 20
    min_deg_xy = -50
    max_deg_xy = 50

    r_step_z = 20
    min_deg_z = -70
    max_deg_z = 70

    shape_perf = 0.17

    for name,img in imgs:

        stop = False

        if randomize:
            randomized_img = get_randomized_shape(img, shape_perfection=shape_perf, h_range=(60,60), s_range=(255,255), v_range=(255,255))
        else:
            randomized_img = img

        for dx in np.arange(min_deg_xy, max_deg_xy, r_step_xy):
            for dy in np.arange(min_deg_xy, max_deg_xy, r_step_xy):
                for dz in np.arange(min_deg_z, max_deg_z, r_step_z):

                    if randomize and true_with_prob(0.001):
                        randomized_img = get_randomized_shape(img, shape_perfection=shape_perf, h_range=(60, 60), s_range=(255, 255), v_range=(255, 255))

                    perspective_img = perspectivize_image(randomized_img, dx, dy, dz, dist_from_camera=1200)

                    cv2.imshow("Prova", perspective_img)

                    desc = [d for d in fast_shape_matching.regions_descriptors(perspective_img.astype(np.uint8), 10.0, 128, 100) if d["mean_color"][1] > 10][0]

                    desc["dx"] = dx
                    desc["dy"] = dy
                    desc["dz"] = dz

                    descriptors[name].append(desc)

                    print name
                    print desc

                    if cv2.waitKey(3) == 27:
                        stop = True
                        break

                    # time.sleep(0.03)

                if stop:
                    break

            if stop:
                break

            # save(descriptors)

    return descriptors

def add_irrelevant_shapes(descriptors, imgs):

    for img in imgs:

        before = datetime.datetime.now()
        descs = fast_shape_matching.regions_descriptors(preprocess_image(img, megapixels=0.15,
                                                                         to_hsv=False)[0].astype(np.uint8)
                                                        , 30.0, 128, 10)

        print datetime.datetime.now() - before

        descriptors["__other__"].extend(descs)

descriptors = {"square": [],
               "octagon": [],
               "circle": [],
               "triangle": [],
               "__other__": []}

ENLARGEMENT = 30

imgs = [
        ("square", enlarge_image(cv2.imread("sample_images/square.png"), ENLARGEMENT)),
        ("octagon",enlarge_image(cv2.imread("sample_images/octagon.png"), ENLARGEMENT)),
        ("triangle",enlarge_image(cv2.imread("sample_images/triangle.png"), ENLARGEMENT)),
        ("circle", enlarge_image(cv2.imread("sample_images/circle.png"), ENLARGEMENT))
    ]

NOT_RELEVANT_IMAGES_FOLDER = "sample_images/not_relevant_shapes_images"
irr_imgs = [cv2.imread(join(NOT_RELEVANT_IMAGES_FOLDER, f)) for f in os.listdir(NOT_RELEVANT_IMAGES_FOLDER)
            if isfile(join(NOT_RELEVANT_IMAGES_FOLDER, f))]

print len(irr_imgs)

add_relevant_shapes(descriptors, imgs, randomize=True)
add_irrelevant_shapes(descriptors, irr_imgs)

save(descriptors)