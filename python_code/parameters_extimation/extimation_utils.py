from math import ceil

from lib.recognition_utils import get_full_res_bb

ACCEPTED_ERROR = 0.1

def shape_found(recognition_result, square_data, img_sz, wr, hr):

    for descriptor in recognition_result:
        given_bb = descriptor["bounding_box"]

        bb = get_full_res_bb(given_bb, wr, hr)

        x1r = bb[1]
        y1r = bb[3]
        x2r = bb[0]
        y2r = bb[2]

        x1i = square_data[0]
        y1i = square_data[1]
        x2i = square_data[2]
        y2i = square_data[3]

        diffs = [abs(x1r-x1i), abs(y1r-y1i), abs(x2r-x2i), abs(y2r-y2i)]

        err = False

        for d in diffs:

            if float(d)/img_sz > ACCEPTED_ERROR:
                err = True
                break

        if not err:
            return True

    return False

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def chunks_div(l, n_parts):
    n = int(ceil(float(len(l))/n_parts))
    return chunks(l, n)