from datetime import datetime
import random
import numpy as np
from Queue import Queue, Empty
from threading import Thread

from lib.recognition_utils import preprocess_image, recognize_single_frame_shapes_2
from parameters_extimation.extimation_utils import chunks, chunks_div, shape_found
from parameters_extimation.genetic_programming import Individual, FitmentData, GeneticAlgorithm

BLOCK_TIMEOUT = 1
MEGAPIXELS = 0.01
MIN_CONTOUR_POINTS = 10
TIME_WEIGHT = 0.02


class RecognitionEvaluator:

    def __init__(self, dataset, keep_n=None, shuffle=True,
                 n_threads=8, chunks_threads_factor=2,
                 megapixels = MEGAPIXELS, min_contour_points=MIN_CONTOUR_POINTS,
                 time_weight=TIME_WEIGHT, extimate_threshold=True, diff_data_size=7,
                 penalize_high_genes_factor = 1):

        self.megapixels = megapixels
        self.min_contour_points = min_contour_points
        self.time_weight = time_weight
        self.extimate_threshold = extimate_threshold
        self.diff_data_size = diff_data_size
        self.penalize_high_genes_factor = penalize_high_genes_factor


        if shuffle:
            random.shuffle(dataset)

        self.dataset = dataset

        if keep_n is not None:
            self.used_dataset = dataset[:keep_n]
        else:
            self.used_dataset = dataset

        self.chunked_dataset = list(chunks_div(list(enumerate(self.used_dataset)), n_threads*chunks_threads_factor))

        self.in_queue = Queue()
        self.out_queue = Queue()

        self.threads = [Thread(target=self.thread_body) for _ in xrange(n_threads)]

        self._stop = False

        for t in self.threads:
            t.start()

    def evaluate_chunk(self, chunk, n_recs, genes):

        chunk_time = 0

        for img_index, data in chunk:
            img = data["img"]
            square_data = data["square_data"]

            img_sz, _, _ = img.shape

            preprocessed_frame, wr, hr = preprocess_image(img, megapixels=MEGAPIXELS)

            before = datetime.now()

            diff_data = genes[:self.diff_data_size]

            if self.extimate_threshold:
                ext_data = genes[self.diff_data_size+1:]

                avg = np.mean(img)
                std = np.std(img)

                th_ext = avg*ext_data[0] + std*ext_data[1] + avg*std*ext_data[2]

                diff_data.append(th_ext)



            recognition_result = recognize_single_frame_shapes_2(preprocessed_frame,
                                                                 diff_data=diff_data,
                                                                 min_shape_contour_points=self.min_contour_points)

            # print recognition_result, square_data

            time = (datetime.now() - before).total_seconds()

            chunk_time += time

            if shape_found(recognition_result, square_data, img_sz, wr, hr):
                n_recs[img_index] += 1

        return chunk_time

    def thread_body(self):

        while not self._stop:

            try:
                data = self.in_queue.get(block=False, timeout=BLOCK_TIMEOUT)
                self.out_queue.put(self.evaluate_chunk(**data))

            except Empty:
                pass

    def eval_base(self, child, weights, n_recs):

        child_n_recs = [0 for _ in n_recs]

        for c in self.chunked_dataset:
            self.in_queue.put({
                "chunk": c,
                "n_recs": child_n_recs,
                "genes": child.genes
            })

        tot_time = 0

        # Attendo il termine dei lavori
        for _ in xrange(len(self.chunked_dataset)):
            chunk_time = self.out_queue.get()
            tot_time += chunk_time

        tot_recs = sum(child_n_recs)
        for i, rec in enumerate(child_n_recs):
            n_recs[i] += rec

        time_per_frame = tot_time/len(n_recs)

        perc_rec = (float(tot_recs) / len(n_recs)) * 100

        fit_value = float(sum([rec * weights[i] for (i, rec) in enumerate(child_n_recs)]))/ len(n_recs)*100 \
                    - self.time_weight * time_per_frame \
                    - self.penalize_high_genes_factor*sum([abs(g) for g in child.genes]) # Penality for high genes

        child.fit_eval = fit_value
        child.additional_data = {
            "child_n_recs": child_n_recs,
            "tot_recs": tot_recs,
            "perc_rec": perc_rec,
            "tot_time": tot_time,
            "time_per_frame": time_per_frame,
            "fps_single_core": 1.0/time_per_frame
        }

    def eval(self, child, data, ga):
        # type: (RecognitionEvaluator, Individual, FitmentData, GeneticAlgorithm) -> None

        weights = data.additional_data["weights"]
        n_recs = data.additional_data["n_recs"]

        self.eval_base(child, weights, n_recs)


    def eval_simple(self, child):

        weights = [1.0 for _ in xrange(len(self.used_dataset))]
        n_recs = [0 for _ in xrange(len(self.used_dataset))]

        self.eval_base(child, weights, n_recs)

    def stop(self):
        self._stop = True
        for t in self.threads:
            t.join()

    def __del__(self):
        self.stop()