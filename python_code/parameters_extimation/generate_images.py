import os
import pickle
import random
from datetime import datetime

import cv2
import numpy as np

from parameters_extimation.image_generation import randomize_image
from parameters_extimation.probability_extract import get_random_min_max, random_square, random_modify_range, \
    random_modify_range_stretch

DATASET_FOLDER = r"/home/davide/datasets"
IMG_SIZE = 223

def get_in_square_predicate(x1, x2, y1, y2):

    def in_square(pt):
        x = pt[0]
        y = pt[1]

        return x1 <= x <= x2 and y1 <= y <= y2

    return in_square


res = []


date = datetime.now()

for i in xrange(1000):

    print i

    img = np.zeros((IMG_SIZE, IMG_SIZE, 3), np.uint8)

    w, h, _ = img.shape

    v_range = get_random_min_max(30, 230, 5, 50)
    s_range = get_random_min_max(30, 150, 5, 50)

    p_range = (0.22,0.22+0.2)

    randomize_image(img, lambda pt:True, h_range=get_random_min_max(0,179,50),
                    v_range=v_range, s_range=s_range, p_range=p_range)

    square_data = random_square(w,h)

    h_range_shape = get_random_min_max(0,179,0,7)
    # s_range_shape = random_modify_range_stretch(s_range)
    s_range_shape = s_range
    # v_range_shape = random_modify_range_stretch(v_range)
    v_range_shape = v_range


    randomize_image(img, get_in_square_predicate(*square_data), p_range=p_range, shape_perfection=0.7, h_range=h_range_shape, v_range=v_range_shape, s_range=s_range_shape)

    img = cv2.cvtColor(img, cv2.COLOR_HSV2BGR)

    res.append({"square_data": square_data, "img": img})

    if i%10 == 0:
        with open(os.path.join(DATASET_FOLDER, "images%s.pickle"%str(date)), "wb") as output:
            pickle.dump(res, output)

    cv2.imshow("prova", img)

    if cv2.waitKey(1) == 27:
        break

