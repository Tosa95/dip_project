import Queue
import random
import numpy as np
import cv2

from parameters_extimation.probability_extract import extract_point_bb, get_random_point_satisfying_predicate, \
    extract_color, true_with_prob


def in_image(pt, h, w):
    x = pt[0]
    y = pt[1]
    return 0 <= x < w and 0 <= y < h

def random_region_growing(img, seed, selection_predicate, color, p, colored, min_size, shape_perfection):
    h, w, _ = img.shape

    q = Queue.Queue()

    q.put(seed)

    visited = set()

    while not q.empty():

        pt = q.get()

        if pt not in colored:

            visited.add(pt)
            colored.add(pt)

            x = pt[0]
            y = pt[1]

            img[y,x] = color

            for sx in [-1, 0, 1]:
                for sy in [-1, 0, 1]:

                    if abs(sx) + abs(sy) < 2:

                        nx = x + sx
                        ny = y + sy
                        npt = (nx, ny)

                        size = len(visited) + q.qsize()

                        select = in_image(npt, h, w) and (selection_predicate(npt) or true_with_prob(1-shape_perfection))

                        if npt not in visited and in_image(npt, h, w) and \
                                select and \
                                (true_with_prob(p) or size < min_size):
                            q.put(npt)

def get_img_shape_selection_predicate(img):

    def pred(pt):
        x = pt[0]
        y = pt[1]

        return sum(img[y, x, :])>0

    return pred

def enlarge_image(img, n_pix):

    h,w,c = img.shape

    img_big = np.zeros((h+n_pix*2, w+n_pix*2, c))

    img_big[n_pix:n_pix+h, n_pix:n_pix+w, :] = img

    return img_big

def randomize_image(img, selection_predicate, p_range=(0.12, 0.28), shape_perfection=1.0, h_range=(0,180), s_range=(0,255), v_range=(0,255), min_size=20):

    h, w, _ = img.shape

    colored = set()

    for y in xrange(h):
        for x in xrange(w):
            pt = (x,y)
            if pt not in colored and selection_predicate(pt):
                p = random.uniform(p_range[0], p_range[1])
                color = extract_color(h_range, s_range, v_range)
                random_region_growing(img, pt, selection_predicate, color, p, colored, min_size, shape_perfection)

def get_randomized_shape(shape, p_range=(0.3, 0.55), shape_perfection=0.3, h_range=(0,180), s_range=(0,255), v_range=(0,255), min_size=20, iterations=100000):
    randomized_img = np.zeros(shape.shape, np.uint8)
    randomize_image(randomized_img, get_img_shape_selection_predicate(shape), p_range=p_range,
                    shape_perfection=shape_perfection, h_range=h_range, v_range=v_range, s_range=s_range)
    randomized_img = cv2.cvtColor(randomized_img, cv2.COLOR_HSV2BGR)

    return randomized_img
