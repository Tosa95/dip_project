import random

from sympy import sin

from parameters_extimation.genetic_programming import FitmentData, get_repeated_mutate_with_p_fn, \
    get_mutate_random_gene_fn
from parameters_extimation.genetic_programming import GeneticAlgorithm
from parameters_extimation.genetic_programming import Individual
from parameters_extimation.probability_extract import log_mutate_actions
from parameters_extimation.simulated_annealing import SimulatedAnnealing, get_mutate_to_random_move_fn, \
    get_linear_schedule_fn, AnnealingFitmentData, get_intelligent_random_move_fn

RES = 0
N_GENES = 50
POP_SIZE = 100
MAX_GENS_NO_IMPROVEMENT = 1000

def eval(child, data, sa):
    # type: (Individual, FitmentData, GeneticAlgorithm) -> None

    genes = child.genes

    result = sum([(i+1)*pow(g, 2) + (i+1)*pow(g, 1) + sin(i*g + pow(i,3)) for (i,g) in enumerate(genes)])
    # result = sum([i*pow(g, 2) + i*pow(g, 1) for (i,g) in enumerate(genes)])

    child.additional_data["sum"] = result
    child.fit_eval = 1/(abs(result-RES) + 1)
    # child.fit_eval = -abs(result - RES)

def on_best_changed(data, sa):
    # type: (AnnealingFitmentData, SimulatedAnnealing) -> None

    print data.best.additional_data["sum"]

def generate_genes():
    return [random.uniform(-1, 1) for _ in xrange(N_GENES)]


SA = SimulatedAnnealing(

    random_move= get_intelligent_random_move_fn(generate_genes(), log_mutate_actions(-5, 3, 0.2)),
    # random_move= get_mutate_to_random_move_fn(get_repeated_mutate_with_p_fn(0.2, 3, get_mutate_random_gene_fn(log_mutate_actions(-5, 1, 0.2)))),
    evaluate=eval,
    schedule=get_linear_schedule_fn(0.000001, 1000),
    on_best_changed=on_best_changed

)

# SA2 = SimulatedAnnealing(
#
#     random_move= get_mutate_to_random_move_fn(get_repeated_mutate_with_p_fn(0.2, 10, get_mutate_random_gene_fn(log_mutate_actions(-5, 1, 0.2)))),
#     evaluate=eval,
#     schedule=get_linear_schedule_fn(0.000001, 1000),
#     on_best_changed=on_best_changed
#
# )

ind = generate_genes()

print "SA1--------------------------------"
data = SA.fit(ind)
# print "SA2--------------------------------"
# data2 = SA2.fit(ind)



print data.best