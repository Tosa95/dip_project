import os
import pickle

import time
from numpy import random

import cv2

from general_utils.data_analisys.dataset_utils import convert_img_color
import numpy as np

from parameters_extimation.full_frames_analisys.datasets import FULL_FRAMES__DATASET_FOLDER, DATASETS_FOLDER, \
    FULL_FRAMES__FRAMES_FOLDER

FRAMES_DATA_FILE = os.path.join(FULL_FRAMES__DATASET_FOLDER, "rec_dataset_new_evalued_reduced_with_regr_params.pickle")

IMG_SIZE_X = 20
IMG_SIZE_Y = 20

with open(FRAMES_DATA_FILE, "rb") as input:
    print "Loading frames data"
    data = pickle.load(input)

dataset = []

for i, frame_data in enumerate(data):

    print i

    if "regr_y" in frame_data:
        frame_path = os.path.join(FULL_FRAMES__FRAMES_FOLDER, frame_data["name"])
        img = cv2.imread(frame_path)
        dataset.append((convert_img_color(img, IMG_SIZE_X, IMG_SIZE_Y), frame_data["regr_y"] / 255.0))
        print "Data added"

random.shuffle(dataset)

X = []
y = []

for img, lbl in dataset:
    print lbl

    X.append(img)
    y.append(lbl)


X = np.array(X).reshape(-1, IMG_SIZE_X, IMG_SIZE_Y, 3)

result = {"X": X, "y": y}

with open(os.path.join(DATASETS_FOLDER, "dataset_th_extimation.pickle"), "wb") as output:

    pickle.dump(result, output)
    print "SAVED"

# while cv2.waitKey(27) != 27:
#     pass


