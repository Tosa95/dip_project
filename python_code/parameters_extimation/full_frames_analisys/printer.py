import os
import pickle
from pprint import pprint

from parameters_extimation.full_frames_analisys.datasets import FULL_FRAMES__DATASET_FOLDER

INPUT_FILE = os.path.join(FULL_FRAMES__DATASET_FOLDER, "rec_dataset_new_evalued_reduced_with_regr_params.pickle")

with open(INPUT_FILE, "rb") as input:
    print "Loading partial result"
    data = pickle.load(input)
    print len(data)

pprint(data)