import os
import pickle
import random
import time

import cv2

from lib.recognition_utils import print_relevant_shapes_bb_only
from parameters_extimation.full_frames_analisys.datasets import FULL_FRAMES__DATASET_FOLDER, FULL_FRAMES__FRAMES_FOLDER

RES_FILE = os.path.join(FULL_FRAMES__DATASET_FOLDER, "rec_dataset_new.pickle")

with open(RES_FILE, "rb") as input:
    print "Loading partial result"
    data = pickle.load(input)
    random.shuffle(data)
    print len(data)

for frame in data:

    fr = cv2.imread(os.path.join(FULL_FRAMES__FRAMES_FOLDER, frame["name"]))

    for s in frame["shapes"]:
        avg_th = float(sum(s["thresholds"])) / len(s["thresholds"])
        s["label"] = "%.1f %d" % (avg_th, len(s["thresholds"]))



    print_relevant_shapes_bb_only(fr, frame["shapes"])

    cv2.imshow("aaa", fr)

    if cv2.waitKey(1) == 27:
        break

    time.sleep(1)