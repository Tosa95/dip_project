import os
import pickle
import random
import time
from numpy.ma import argmax

import cv2

from config import REGION_GROWING_THRESHOLD_EXTIMATION_BB_TOP_LEFT, REGION_GROWING_THRESHOLD_EXTIMATION_BB
from general_utils.misc.arrays import get_longer_sequence_mean_index
from lib.recognition_utils import print_relevant_shapes_bb_only, crop, to_int_bb
from parameters_extimation.full_frames_analisys.datasets import FULL_FRAMES__DATASET_FOLDER, FULL_FRAMES__FRAMES_FOLDER
import numpy as np

INPUT_FILE = os.path.join(FULL_FRAMES__DATASET_FOLDER, "rec_dataset_new_evalued_reduced.pickle")
OUTPUT_FILE = os.path.join(FULL_FRAMES__DATASET_FOLDER, "rec_dataset_new_evalued_reduced_with_regr_params.pickle")
OUTPUT_FILE_FOR_REGR = os.path.join(FULL_FRAMES__DATASET_FOLDER, "th_ext_regr_data.pickle")


RELEVANT_TH = 0.8

def get_img_params(img, grid_cells_x = 3, grid_cells_y = 3):
    height, width, _ = img.shape

    span_y = float(height) / grid_cells_y
    span_x = float(width) / grid_cells_x

    data = []

    for row in xrange(grid_cells_y):
        for col in xrange(grid_cells_x):
            y1 = int(row*span_y)
            x1 = int(col*span_x)
            y2 = int((row+1) * span_y) - 1
            x2 = int((col+1) * span_x) - 1

            cropped = crop(img, [y1, x1, y2, x2])

            mn = np.mean(cropped)
            st = np.std(cropped)

            data.append(mn)
            data.append(mn**2)
            data.append(mn**3)
            data.append(mn**4)
            data.append(st)
            data.append(st**2)
            data.append(st**3)
            data.append(st**4)
            data.append(mn*st)
            data.append(mn**2*st)
            data.append(mn*st**2)
            data.append(mn**2*st**2)

    return data


th_recognized = [0 for _ in xrange(255)]
total_relevant_shapes = 0
better_single_th_rec = 0

X = []
Y = []

with open(INPUT_FILE, "rb") as input:
    print "Loading partial result"
    data = pickle.load(input)
    print len(data)

for i, frame in enumerate(data):

    print i

    frame_path = os.path.join(FULL_FRAMES__FRAMES_FOLDER, frame["name"])

    img = cv2.imread(frame_path)

    top_left = crop(img, to_int_bb(img, REGION_GROWING_THRESHOLD_EXTIMATION_BB_TOP_LEFT))
    bottom_right = crop(img, to_int_bb(img, REGION_GROWING_THRESHOLD_EXTIMATION_BB))

    mean_bright, std_bright = np.mean(top_left), np.std(top_left)
    mean_dim, std_dim = np.mean(bottom_right), np.std(bottom_right)



    # x = [mean_bright, mean_bright ** 2, std_bright, std_bright ** 2, mean_dim, mean_dim ** 2, std_dim, std_dim ** 2,
    #      mean_bright * mean_dim, std_bright * std_dim, mean_bright * mean_dim * std_bright * std_dim]

    x = get_img_params(img)

    th_recognized_single_frame = [0 for _ in xrange(255)]

    for s in frame["shapes"]:

        if "eval" in s and s["eval"] > RELEVANT_TH:

            for th in s["thresholds"]:
                th_recognized_single_frame[th] += 1

            total_relevant_shapes += 1

    highest_recognitions = max(th_recognized_single_frame)

    if highest_recognitions > 0:

        y = get_longer_sequence_mean_index(th_recognized_single_frame, highest_recognitions)

        print y

        X.append(x)
        Y.append(y)

        frame["regr_x"] = x
        frame["regr_y"] = y

print len(Y)

with open(OUTPUT_FILE, "wb") as output:
    pickle.dump(data, output)
    print "SAVED"

with open(OUTPUT_FILE_FOR_REGR, "wb") as output:
    pickle.dump({"X": X, "Y": Y}, output)
    print "REGR DATA SAVED"
