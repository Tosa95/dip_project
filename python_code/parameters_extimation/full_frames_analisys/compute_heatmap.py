import os
import pickle
import random
import time

import cv2
import numpy as np
from lib.recognition_utils import print_relevant_shapes_bb_only
from parameters_extimation.full_frames_analisys.datasets import FULL_FRAMES__DATASET_FOLDER, FULL_FRAMES__FRAMES_FOLDER

RES_FILE = os.path.join(FULL_FRAMES__DATASET_FOLDER, "rec_dataset_new_evalued_reduced.pickle")

with open(RES_FILE, "rb") as input:
    print "Loading partial result"
    data = pickle.load(input)
    random.shuffle(data)
    print len(data)

res = None
nshapes = 0

for frame in data:

    if res is None:
        fr = cv2.imread(os.path.join(FULL_FRAMES__FRAMES_FOLDER, frame["name"]))
        res = np.zeros(fr.shape[:2], dtype=np.float)

    #fr = cv2.imread(os.path.join(FULL_FRAMES__FRAMES_FOLDER, frame["name"]))

    selected_shapes = []

    for s in frame["shapes"]:

        y1, x1, y2, x2 = s["bounding_box"]
        print x1, y1, x2, y2

        res[y1:y2,x1:x2] += 1
        print res[120,120]
        nshapes += 1

res = res/np.max(res)
res[res<0.4] = 0

cv2.imshow("aaa", (res*255).astype(np.uint8))
cv2.waitKey(10000)