import time

import cv2
import os
import pickle

import keras
from keras import Sequential
from keras.layers import Conv2D, Activation, MaxPooling2D, Flatten, Dense

from datasets import DATASETS_FOLDER

from keras.callbacks import History

DATASET = os.path.join(DATASETS_FOLDER, "dataset_th_extimation.pickle")

CONV_FILTERS_NUM = 16
DENSE_SIZE = 32
EPOCHS = 80

NUM_CONV = 1
NUM_DENSE = 2

def train():

    history = History()

    with open(DATASET, "rb") as input:
        dataset = pickle.load(input)
        X = dataset["X"]
        y = dataset["y"]

    model = Sequential()

    model.add(Conv2D(CONV_FILTERS_NUM, (3, 3), input_shape=X.shape[1:]))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    for i in xrange(NUM_CONV - 1):
        model.add(Conv2D(CONV_FILTERS_NUM, (3, 3)))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors


    for i in xrange(NUM_DENSE):
        model.add(Dense(DENSE_SIZE))
        model.add(Activation('relu'))

    model.add(Dense(1))
    model.add(Activation('sigmoid'))

    model.compile(loss='mean_squared_error', optimizer='adam')

    model.fit(X, y, batch_size=32, epochs=EPOCHS, validation_split=0.2, callbacks=[history])

    val_loss = history.history['val_loss'][-1]

    print(history.history['val_loss'])

    model.save("th_ext_fn%d_ds%d_ep%d_nc%d_nd%d_val_loss%.4f.model" %
                                                                                (CONV_FILTERS_NUM,
                                                                                 DENSE_SIZE,
                                                                                 EPOCHS,
                                                                                 NUM_CONV,
                                                                                 NUM_DENSE,
                                                                                 val_loss))

# def use():
#
#     scorer = ImageScorer()
#
#     for i in xrange(1000):
#
#         id = get_random_image_id()
#         # img = get_img_by_id(id)
#
#         prediction, score = scorer.get_score(id)
#
#         if score > 0.5:
#             print (prediction, score), id
#
#         # cv2.imshow("img", img)
#         # cv2.waitKey(1)
#
#         #time.sleep(2)

# use()

train()

# img = cv2.imread("test.jpg", cv2.IMREAD_COLOR)
# scorer = ImageScorer()
# print scorer.get_score_from_img(img)