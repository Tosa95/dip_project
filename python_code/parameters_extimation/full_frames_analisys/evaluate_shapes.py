import pickle

import cv2
import os

from general_utils.files_and_folders.working_directory import py_charm_set_working_directory_project_root
from general_utils.parallel.parallel_executor import ParallelExecutor
from general_utils.parallel.utils import chunks
from nn.image_score import ImageScorer

from parameters_extimation.full_frames_analisys.datasets import FULL_FRAMES__FRAMES_FOLDER, FULL_FRAMES__DATASET_FOLDER
from lib.recognition_utils import preprocess_image, recognize_single_frame_shapes, get_full_res_bb, \
    enlarge_bounding_box, crop

py_charm_set_working_directory_project_root()

INPUT_FILE = os.path.join(FULL_FRAMES__DATASET_FOLDER, "rec_dataset_new.pickle")
OUTPUT_FILE = os.path.join(FULL_FRAMES__DATASET_FOLDER, "rec_dataset_new_evalued.pickle")

SAVE_EVERY_CHUNKS = 30
CHUNK_SIZE = 300

already_done = set()

with open(INPUT_FILE, "rb") as input:
    print "Loading partial result"
    data = pickle.load(input)

pexec = ParallelExecutor(8)

chunked_data = list(chunks(list(enumerate(data)), CHUNK_SIZE))

scorer = ImageScorer()

def elaborate_chunk(chunk):

    for i, frame_data in chunk:

        print "%.4f %d" % (float(i)/len(data), i)

        filename = frame_data["name"]

        frame_path = os.path.join(FULL_FRAMES__FRAMES_FOLDER, filename)

        frame = cv2.imread(frame_path)

        shapes = frame_data["shapes"]

        for shape in shapes:

            if "eval" not in shape:

                bb = shape["bounding_box"]

                bbe = enlarge_bounding_box(bb, 0.20)

                frame_cropped = crop(frame, bbe)

                prediction, _ = scorer.get_score_from_img(frame_cropped)

                shape["eval"] = prediction

                if prediction > 0.5:
                    print "Interesting shape found in frame %s." % filename



for i,fc in enumerate(chunked_data):

    pexec.elaborate_all(fc, elaborate_chunk, lambda c: True)

    if i % SAVE_EVERY_CHUNKS == 0:
        with open(OUTPUT_FILE, "wb") as output:
            pickle.dump(data, output)
            print "SAVED"

    print "CHUNK_DONE"

with open(OUTPUT_FILE, "wb") as output:
    pickle.dump(data, output)
    print "SAVED"

pexec.stop()
