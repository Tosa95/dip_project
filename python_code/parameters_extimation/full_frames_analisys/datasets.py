import os

DATASETS_FOLDER = "/root/full_frame_analysis"

FULL_FRAMES__DATASET_FOLDER = os.path.join(DATASETS_FOLDER, "full_frames")
FULL_FRAMES__FRAMES_FOLDER = os.path.join(FULL_FRAMES__DATASET_FOLDER, "frames")
