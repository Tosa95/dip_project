import pickle

import os

from general_utils.files_and_folders.working_directory import py_charm_set_working_directory_project_root
from parameters_extimation.full_frames_analisys.datasets import FULL_FRAMES__FRAMES_FOLDER, FULL_FRAMES__DATASET_FOLDER


py_charm_set_working_directory_project_root()

INPUT_FILE = os.path.join(FULL_FRAMES__DATASET_FOLDER, "rec_dataset_new_evalued.pickle")
OUTPUT_FILE = os.path.join(FULL_FRAMES__DATASET_FOLDER, "rec_dataset_new_evalued_reduced.pickle")

RELEVANT_THRESHOLD = 0.93

with open(INPUT_FILE, "rb") as input:
    print "Loading partial result"
    data = pickle.load(input)

to_remove = []

for i,frame_data in enumerate(data):

    print i

    selected_shapes = []

    shapes = frame_data["shapes"]

    for s in shapes:
        if s["eval"] >= RELEVANT_THRESHOLD:
            selected_shapes.append(s)

    frame_data["shapes"] = selected_shapes

    if len(selected_shapes) == 0:
        to_remove.append(i)

for index in sorted(to_remove, reverse=True):
    print index
    del data[index]

with open(OUTPUT_FILE, "wb") as output:
    print "SAVING"
    pickle.dump(data, output)
    print "SAVED"
