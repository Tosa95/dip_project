import os
import pickle
from math import sqrt
from pprint import pprint
import numpy as np

from general_utils.data_analisys.regression import multi_variable_regression, compute_regression_value
from parameters_extimation.full_frames_analisys.datasets import FULL_FRAMES__DATASET_FOLDER

INPUT_FILE = os.path.join(FULL_FRAMES__DATASET_FOLDER, "th_ext_regr_data.pickle")
OUTPUT_FILE = "regression_data.pickle"


with open(INPUT_FILE, "rb") as input:
    print "Loading data"
    data = pickle.load(input)

X = data["X"]
Y = data["Y"]

pprint(X)

print len(X)
print len(Y)

lst_square_params = multi_variable_regression(X, Y)

predicted = compute_regression_value(X, lst_square_params)

Y = np.array(Y)



print max(predicted)
# print np.sqrt(np.sum(np.power(Y-predicted, 2)))/len(Y)
# print sum(np.power((Y-predicted),2))
print sqrt(((predicted - Y)**2).mean())
print zip(Y.tolist(), predicted.tolist())

with open(OUTPUT_FILE, "wb") as output:
    pickle.dump(lst_square_params, output)
    print "SAVED"
