import os
import pickle
import random
import time

import keras
from numpy.ma import argmax

import cv2

from general_utils.data_analisys.dataset_utils import reshape_image_for_cnn, convert_img_color
from general_utils.data_analisys.regression import compute_regression_value
from lib.recognition_utils import print_relevant_shapes_bb_only
from parameters_extimation.full_frames_analisys.datasets import FULL_FRAMES__DATASET_FOLDER, FULL_FRAMES__FRAMES_FOLDER

RES_FILE = os.path.join(FULL_FRAMES__DATASET_FOLDER, "rec_dataset_new_evalued_reduced_with_regr_params.pickle")
REGRESSION_FILE = "regression_data.pickle"
NN_MODEL = keras.models.load_model("th_ext_fn16_ds32_ep80_nc1_nd2_val_loss0.0027.model")

RELEVANT_TH = 0.8

th_recognized = [0 for _ in xrange(255)]
total_relevant_shapes = 0
better_single_th_rec = 0
regression_rec = 0
nn_rec = 0

with open(RES_FILE, "rb") as input:
    print "Loading partial result"
    data = pickle.load(input)
    random.shuffle(data)
    print len(data)

with open(REGRESSION_FILE, "rb") as input:
    print "Loading regression data"
    regression_data = pickle.load(input)

for i, frame in enumerate(data):

    print i

    selected_shapes = []
    th_recognized_single_frame = [0 for _ in xrange(255)]

    for s in frame["shapes"]:

        if "eval" in s and s["eval"] > RELEVANT_TH:

            for th in s["thresholds"]:
                th_recognized[th] += 1
                th_recognized_single_frame[th] += 1

            total_relevant_shapes += 1

    if "regr_x" in frame:
        regressed_th = compute_regression_value([frame["regr_x"]], regression_data)[0]
        regression_rec += th_recognized_single_frame[int(round(regressed_th))]

    frame_path = os.path.join(FULL_FRAMES__FRAMES_FOLDER, frame["name"])
    img = cv2.imread(frame_path)
    img_prepared = reshape_image_for_cnn(convert_img_color(img, 20, 20), 20, 20)

    nn_th = NN_MODEL.predict([img_prepared])[0][0] * 255.0

    nn_rec += th_recognized_single_frame[int(nn_th)]

    better_single_th_rec += max(th_recognized_single_frame)


perc_recogn = [float(r)/total_relevant_shapes for r in th_recognized]

print list(enumerate(th_recognized))
print list(enumerate(perc_recogn))
print argmax(perc_recogn), "%.2f" % (perc_recogn[argmax(perc_recogn)]*100)
print total_relevant_shapes
print better_single_th_rec, "%.2f" % (float(better_single_th_rec)/total_relevant_shapes*100)
print regression_rec, "%.2f" % (float(regression_rec)/total_relevant_shapes*100)
print nn_rec, "%.2f" % (float(nn_rec)/total_relevant_shapes*100)
