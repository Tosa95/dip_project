import pickle

import cv2
import os

from general_utils.files_and_folders.working_directory import py_charm_set_working_directory_project_root
from general_utils.parallel.parallel_executor import ParallelExecutor
from general_utils.parallel.utils import chunks

py_charm_set_working_directory_project_root()

from parameters_extimation.full_frames_analisys.datasets import FULL_FRAMES__FRAMES_FOLDER, FULL_FRAMES__DATASET_FOLDER
from lib.recognition_utils import preprocess_image, recognize_single_frame_shapes, get_full_res_bb

RES_FILE = os.path.join(FULL_FRAMES__DATASET_FOLDER, "rec_dataset_new.pickle")

SAVE_EVERY_CHUNKS = 10

files = list(os.listdir(FULL_FRAMES__FRAMES_FOLDER))

res = []
already_done = set()

try:
    with open(RES_FILE, "rb") as input:
        print "Loading partial result"
        res = pickle.load(input)
        for f in res:
            already_done.add(f["name"])
        print "Partial result loaded from file"
except Exception as ex:
    pass

pexec = ParallelExecutor(8)


chunked_files = list(chunks(list(enumerate(files)), 300))

def in_range(x, y, tolerance):
    return x-tolerance <= y <= x+tolerance

def same_bb(bb1, bb2, tolerance=5):

    for n in xrange(len(bb1)):
        if not in_range(bb1[n], bb2[n], tolerance):
            return False

    return True

def add_shape(shapes, recognized, th, wr, hr):

    to_add_bb = get_full_res_bb(recognized["bounding_box"], wr, hr)

    for shape in shapes:
        selected_bb = shape["bounding_box"]

        if same_bb(selected_bb, to_add_bb):
            shape["thresholds"].append(th)
            return

    shapes.append({
        "bounding_box": to_add_bb,
        "thresholds": [th]
    })

def elaborate_chunk(chunk):

    for i, f in chunk:

        if f not in already_done:

            print "%.4f" % (float(i)/len(files))

            frame_path = os.path.join(FULL_FRAMES__FRAMES_FOLDER, f)

            frame = cv2.imread(frame_path)

            preprocessed_frame, wr, hr = preprocess_image(frame, to_hsv=False, megapixels=0.05)

            frame_data = {
                "name": f,
                "shapes": []
            }

            for th in xrange(1, 100):

                recognition_result = recognize_single_frame_shapes(preprocessed_frame,
                                                                   region_growing_threshold=th,
                                                                   min_shape_contour_points=10)

                for s in recognition_result:
                    add_shape(frame_data["shapes"], s, th, wr, hr)

                # print i, th, len(recognition_result)

                # if len(recognition_result) > 2:
                #     print i, th, f, len(recognition_result)

            print frame_data
            res.append(frame_data)



        else:

            print "Frame %s already done" % f

for i,fc in enumerate(chunked_files):

    elaborate = False

    for i, fn in fc:
        if fn not in already_done:
            elaborate = True
            break

    if elaborate:
        pexec.elaborate_all(fc, elaborate_chunk, lambda c: True)

        if i % SAVE_EVERY_CHUNKS == 0:
            with open(RES_FILE, "wb") as output:
                pickle.dump(res, output)
                print "SAVED"

        print "CHUNK_DONE"

with open(RES_FILE, "wb") as output:
    pickle.dump(res, output)
    print "SAVED"

pexec.stop()
