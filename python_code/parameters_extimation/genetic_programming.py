# coding=utf-8
import random

from parameters_extimation.probability_extract import random_extract_with_evaluation, true_with_prob

def get_mutate_random_gene_fn(steps):

    def mutate(f):
        index = random.randint(0, len(f)-1)
        index_step = random.randint(0, len(steps)-1)

        f[index] = f[index] + steps[index_step]

        return f

    return mutate


def get_mutate_with_p_fn(p, inner_mutation):

    def mutate(f):
        if true_with_prob(p):
            return inner_mutation(f)
        else:
            return f

    return mutate

def get_repeated_mutate_with_p_fn(p, n_rep, inner_mutation):

    def mutate(f):

        for _ in xrange(n_rep):
            if true_with_prob(p):
                # print "MUTATE"
                f = inner_mutation(f)

        return f

    return mutate


def extract_copule(pop, evaluation):
    g1 = random_extract_with_evaluation(pop, evaluation)
    g2 = random_extract_with_evaluation(pop, evaluation)
    while g2 == g1:
        g2 = random_extract_with_evaluation(pop, evaluation)
    # OSS: stranamente permettere anche di avere due genitori uguali porta ad un miglioramento delle performances

    return g1,g2

def get_standard_next_gen_fn():

    def next_gen(data, ga):

        pop = data.pop

        next_ind_list = []

        for i in xrange(len(pop)):

            genes_list = pop.get_genes_list()
            eval_list = pop.get_evaluations_list()

            g1, g2 = extract_copule(genes_list, eval_list)

            f = ga.cross_over(g1, g2) #TODO: meglio copiare i figli
            ga.mutate(f)

            child = Individual(f)
            ga.evaluate(child, data, ga)

            next_ind_list.append(child)

            perc = float(i)/len(pop)
            ga.on_next_gen_perc_change(perc)

        return Population(next_ind_list)

    return next_gen

def get_standard_cross_over_fn():

    def cross_over(g1,g2):

        if len(g1) > 1: # così lo posso usare anche nel caso in cui voglio ottimizzare un solo parametro

            index = random.randint(1, len(g1) - 1)

            f = []

            f.extend(g1[0:index])
            f.extend(g2[index:len(g2)])

            return f

        else:

            return [(g1[0]+g2[0])/2] # cross over tra due figli con un solo gene è la media (a caso hahaah)

    return cross_over

def get_iteration_num_terminate_fn(iter):

    def terminate(data):
        return data.gen > iter

    return terminate

def get_no_improve_terminate_fn(max_gen_no_improv):

    def terminate(data):
        return data.gens_with_no_improv > max_gen_no_improv

    return terminate

def genes_list_to_individual_list(g_list, eval_func, on_perc_change, fitment_data, ga):

    res = []

    for i,genes in enumerate(g_list):
        child = Individual(genes)
        eval_func(child, fitment_data, ga)
        res.append(child)
        on_perc_change(float(i)/len(g_list))

    return res

class Individual(object):
    def __init__(self, genes, fit_eval=None, additional_data=None):
        self.genes = genes
        self.fit_eval = fit_eval
        self.additional_data = additional_data if additional_data is not None else {}

    def __str__(self):
        additional_data=""
        for k,v in self.additional_data.iteritems():
            additional_data += k + ": " + str(v) + "\n"
        return "Fitness: %f\nGenes: %s\n%s" % (self.fit_eval, self.genes, additional_data)

class Population(object):
    def __init__(self, individuals_list):
        self.individals_list = individuals_list
        self.genes_list = [ind.genes for ind in self.individals_list]
        self.eval_list = [ind.fit_eval for ind in self.individals_list]

    def get_genes_list(self):
        return self.genes_list

    def get_evaluations_list(self):
        return self.eval_list

    def __getitem__(self, item):
        return self.individals_list[item]

    def __len__(self):
        return len(self.individals_list)

class FitmentData(object):

    def __init__(self, pop, additional_data = None):
        self.best = None
        self.gens_with_no_improv = 0
        self.gen = 0
        self.pop = pop
        self.additional_data = {} if additional_data is None else additional_data


class GeneticAlgorithm:

    def __init__(self, mutate, evaluate, cross_over, next_gen, terminate,
                 on_best_changed=lambda data: False,
                 on_new_gen=lambda data: False,
                 on_next_gen_perc_change=lambda perc: False):
        self.mutate = mutate
        self.evaluate = evaluate
        self.next_gen = next_gen
        self.cross_over = cross_over
        self.terminate = terminate
        self.on_best_changed = on_best_changed
        self.on_new_gen = on_new_gen
        self.on_next_gen_perc_change = on_next_gen_perc_change

    def fit(self, initial_data, initial_additional_data=None):

        if type(initial_data) is not Population:
            data = FitmentData(
                None,
                initial_additional_data)
            pop = Population(genes_list_to_individual_list(initial_data, self.evaluate, self.on_next_gen_perc_change,
                                                           data, self))
            data.pop = pop
            data.best = pop[0]
            self.on_new_gen(data)
        else:
            data = FitmentData(initial_data, initial_additional_data)

        for f in data.pop:
            if data.best is None or f.fit_eval > data.best.fit_eval:
                data.best = f
                self.on_best_changed(data)

        while not self.terminate(data):
            improvement = False
            data.pop = self.next_gen(data, self)

            for f in data.pop:
                if data.best is None or f.fit_eval > data.best.fit_eval:
                    data.best = f
                    self.on_best_changed(data)
                    # print best_eval, sum(evaluation)/len(evaluation), generations_with_no_improvements
                    data.gens_with_no_improv = 0
                    improvement = True

            if not improvement:
                data.gens_with_no_improv += 1

            self.on_new_gen(data)

            data.gen += 1

        return data

# TODO:

# culling con sostituzione dei figli più scarsi con nuovi figli random. Inserisco più figli random più è il numero di
# gen che il best non viene modificato. Magari posso estrarre a random con probabilità dettata dall'inverso dell eval
# function e sostituirne k di quelli. Bisogna poi vedere come gestire il fatto che poi per un po' comunque non si cambierà
# più il best, in quanto ho buttato dentro roba a random

# elaborazione dei figli: prima di valutrare i figli viene ricavato in qualche modo un dato per ogni figlio, che vuiene
# passato sia ad evaluate che salvato insieme al best (utile quando si tratta di addestrare reti neurali)
