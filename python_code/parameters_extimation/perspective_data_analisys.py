import os
import pickle
from pprint import pprint

import numpy as np

DATA_FOLDER = "/home/davide/datasets"

DESC_FILE = os.path.join(DATA_FOLDER, "descriptors_decimated_11")

def get_mean_std(descs, param):
    values = np.array([d[param] for d in descs])

    return {"mean": np.mean(values), "std": np.std(values)}

def get_descs_stats(descs, params):

    res = {}

    for p in params:
        res[p] = get_mean_std(descs, p)

    return res


with open(DESC_FILE, "rb") as input:
    data = pickle.load(input)

print len(data["square"])*4, len(data["__other__"])

shapes = ["square", "triangle", "circle", "octagon"]
# params = ["mean", "perimeter_ratio", "std", "d1_mean", "d1_std",
#           "area_perimeter_ratio", "area_ratio"]

params = ["perimeter_ratio", "area_perimeter_ratio", "area_ratio"]


n_pars = len(params)
genes_per_shape = n_pars*2

for n, descs in data.iteritems():
    print n
    pprint(get_descs_stats(descs, params))

genes = []

for j, shape in enumerate(shapes):

    stats = get_descs_stats(data[shape], params)

    for i, param in enumerate(params):
        genes.append(stats[param]["mean"] - stats[param]["std"]*3.5)
        genes.append(stats[param]["mean"] + stats[param]["std"] * 3.5)

print genes
print len(genes)

