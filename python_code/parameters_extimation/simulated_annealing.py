import random
from copy import deepcopy, copy
from math import exp

from parameters_extimation.genetic_programming import Individual
from parameters_extimation.probability_extract import true_with_prob, KeepKBest, log_mutate_actions


def get_avoid_useless_work_schedule_fn(low_t, it_no_improv_th, inner_schedule):

    def schedule(data, sa):
        # type: (AnnealingFitmentData, SimulatedAnnealing) -> float

        if data.iterations_with_no_improv < it_no_improv_th:
            T = low_t
        else:
            T = inner_schedule(data, sa)

        return T

    return schedule

def get_linear_schedule_fn(initial_T, num_it):

    def schedule(data, sa):

        T = -(float(initial_T)/num_it)*data.iteration + initial_T

        return T

    return schedule

def get_mutate_to_random_move_fn(mutate_fn):

    def random_move(data, sa):
        next_ind_genes = deepcopy(data.current_individual.genes)
        mutate_fn(next_ind_genes)
        return Individual(next_ind_genes)

    return random_move

def get_intelligent_random_move_fn(initial_direction, possible_deltas, initial_momentum=0.1, momentum_factor=0.01,
                                   max_var_group_size = 5, max_it_no_dir_change = 10, keep_var_group_for = 10):

    def select_var_group(n_vars, max_group_size):
        vars_to_select = random.randint(1, max_group_size)

        vars_index = [i for i in xrange(n_vars)]
        random.shuffle(vars_index)

        res = []

        for i in xrange(vars_to_select):
            res.append(vars_index[0])
            del vars_index[0]

        return res

    def slight_dir_variation(dir):

        for i in xrange(len(dir)):

            factor = 1 + random.uniform(-0.9, 0.9)
            dir[i] *= factor

    def inc_dir(dir):

        # print dir

        factor = 1 + random.uniform(0.01, 1)

        for i in xrange(len(dir)):
            dir[i] *= factor

        # print dir

    def random_move(data, sa):
        # type: (AnnealingFitmentData, SimulatedAnnealing) -> Individual

        if "direction" not in data.additional_data:
            data.additional_data["direction"] = copy(initial_direction)
            data.additional_data["momentum"] = initial_momentum
            data.additional_data["var_group"] = select_var_group(len(initial_direction), max_var_group_size)
            data.additional_data["it_no_dir_change"] = 0
            data.additional_data["keep_group_for"] = 0
            data.additional_data["last_best_dir"] = data.additional_data["direction"]

        next_ind_genes = copy(data.current_individual.genes)

        for i in data.additional_data["var_group"]:
            next_ind_genes[i] += data.additional_data["direction"][i]

        next_ind = Individual(next_ind_genes)

        sa.evaluate(next_ind, data, sa)

        DE = next_ind.fit_eval - data.current_individual.fit_eval

        if DE > 0:
            print "KEEP!"
            data.additional_data["it_no_dir_change"] += 1
            data.additional_data["keep_group_for"] = keep_var_group_for
            data.additional_data["last_best_dir"] = data.additional_data["direction"]
            inc_dir(data.additional_data["direction"])

        if DE <= 0 or data.additional_data["it_no_dir_change"] > max_it_no_dir_change:

            if data.additional_data["keep_group_for"] > 0:
                data.additional_data["keep_group_for"] -= 1

                to_modify = copy(data.additional_data["last_best_dir"])
                slight_dir_variation(to_modify)
                data.additional_data["direction"] = to_modify

                print "KEEPING VAR GROUP ", len(data.additional_data["var_group"]), data.additional_data["keep_group_for"]
            else:
                next_direction = [random.choice(possible_deltas) for _ in initial_direction]
                data.additional_data["direction"] = next_direction
                data.additional_data["var_group"] = select_var_group(len(initial_direction), max_var_group_size)

            data.additional_data["it_no_dir_change"] = 0



        return next_ind





    return random_move

class AnnealingFitmentData(object):

    def __init__(self, best_k = 2, current_individual=None, additional_data=None):
        self.best = None
        self.iterations_with_no_improv = 0
        self.iteration = 0
        self.additional_data = {} if additional_data is None else additional_data
        self.current_individual = current_individual
        self.last_trial = None
        self.keep_bests = KeepKBest(best_k, key=lambda c: c.fit_eval)
        self.T = 0

class SimulatedAnnealing(object):

    def __init__(self, random_move, evaluate, schedule,
                 on_new_individual=lambda data, sa: None,
                 on_new_iteration=lambda data, sa: None,
                 on_best_changed=lambda data, sa: None,
                 only_best_changed_is_an_improvement = True
                 ):
        self.random_move = random_move
        self.evaluate = evaluate
        self.schedule = schedule
        self.on_new_individual = on_new_individual
        self.on_new_iteration = on_new_iteration
        self.on_best_changed = on_best_changed
        self.only_best_changed_is_an_improvement = only_best_changed_is_an_improvement


    def fit(self, first_individual, initial_additional_data=None):

        data = AnnealingFitmentData(additional_data=initial_additional_data)

        if type(first_individual) is Individual:
            current_ind = first_individual
        else:
            current_ind = Individual(first_individual)
            self.evaluate(current_ind, data, self)

        data.current_individual = current_ind

        data.best = current_ind
        data.keep_bests.add_value(current_ind)

        data.iteration = 0

        while True:

            data.T = self.schedule(data, self)

            if data.T <= 0:
                break

            next_ind = self.random_move(data, self)

            if next_ind.fit_eval is None:
                self.evaluate(next_ind, data, self)

            data.last_trial = next_ind

            DE = next_ind.fit_eval - data.current_individual.fit_eval

            if DE >= 0:

                data.current_individual = next_ind
                self.on_new_individual(data, self)

                if not self.only_best_changed_is_an_improvement:
                    data.iterations_with_no_improv = 0

            elif true_with_prob(exp(float(DE)/data.T)):

                data.current_individual = next_ind
                self.on_new_individual(data, self)
                # print "WRONG MOVE ACCEPTED ", exp(float(DE)/T), DE, T

            self.on_new_iteration(data, self)

            if data.current_individual.fit_eval > data.best.fit_eval:
                data.best = data.current_individual
                data.keep_bests.add_value(data.best)
                data.iterations_with_no_improv = 0
                self.on_best_changed(data, self)
            else:
                data.iterations_with_no_improv += 1

            data.iteration += 1

        return data