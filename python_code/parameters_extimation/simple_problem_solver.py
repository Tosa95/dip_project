import random

from sympy import sin

from parameters_extimation.genetic_programming import FitmentData, get_repeated_mutate_with_p_fn, \
    get_mutate_random_gene_fn, get_standard_cross_over_fn, get_standard_next_gen_fn, get_no_improve_terminate_fn
from parameters_extimation.genetic_programming import GeneticAlgorithm
from parameters_extimation.genetic_programming import Individual
from parameters_extimation.probability_extract import log_mutate_actions

RES = 0
N_GENES = 50
POP_SIZE = 100
MAX_GENS_NO_IMPROVEMENT = 1000

def eval(child, data, ga):
    # type: (Individual, FitmentData, GeneticAlgorithm) -> None

    genes = child.genes

    result = sum([i*pow(g, 2) + i*pow(g, 1) + sin(i*g + pow(i,3)) for (i,g) in enumerate(genes)])

    child.additional_data["sum"] = result
    child.fit_eval = 1/(abs(result-RES) + 1)
    # child.fit_eval = -abs(result - RES)

def on_best_changed(data):
    # type: (FitmentData) -> None

    print data.best.fit_eval, data.best.additional_data["sum"], sum(data.pop.eval_list)/len(data.pop.eval_list), data.best.genes

def generate_genes():
    return [random.uniform(-1, 1) for _ in xrange(N_GENES)]

def generate_pop():
    return [generate_genes() for _ in xrange(POP_SIZE)]

GA = GeneticAlgorithm(

    mutate=get_repeated_mutate_with_p_fn(0.06, 10, get_mutate_random_gene_fn(log_mutate_actions(-5, 3, 0.2))),
    evaluate=eval,
    cross_over=get_standard_cross_over_fn(),
    next_gen=get_standard_next_gen_fn(),
    terminate=get_no_improve_terminate_fn(MAX_GENS_NO_IMPROVEMENT),
    on_best_changed=on_best_changed

)

print log_mutate_actions(-5, 3, 0.2)

GA.fit(generate_pop())