from unittest import TestCase

from parameters_extimation.probability_extract import KeepKBest


class TestKeepKBest(TestCase):

    def test_works(self):

        kb = KeepKBest(k=10, key=lambda v: v[0])

        lst = [(x, "w") for x in xrange(30)]

        for x in lst:
            kb.add_value(x)

        self.assertEqual(kb.get_items_sorted(), ((29, 'w'), (28, 'w'), (27, 'w'), (26, 'w'), (25, 'w'), (24, 'w'),
                                                 (23, 'w'), (22, 'w'), (21, 'w'), (20, 'w')))

        print kb.get_random_item()
