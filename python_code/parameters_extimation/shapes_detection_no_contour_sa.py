import os
import pickle
import random

from sympy import sin

from parameters_extimation.genetic_programming import FitmentData, get_repeated_mutate_with_p_fn, \
    get_mutate_random_gene_fn
from parameters_extimation.genetic_programming import GeneticAlgorithm
from parameters_extimation.genetic_programming import Individual
from parameters_extimation.probability_extract import log_mutate_actions
from parameters_extimation.simulated_annealing import SimulatedAnnealing, get_mutate_to_random_move_fn, \
    get_linear_schedule_fn, AnnealingFitmentData, get_intelligent_random_move_fn

RES = 0
N_GENES = 56
POP_SIZE = 100
MAX_GENS_NO_IMPROVEMENT = 1000

DATASET_FOLDER = "/home/davide/datasets"
DESC_FILE = os.path.join(DATASET_FOLDER, "descriptors_decimated_11")

DATA_FOLDER = "data"
BESTFILE = os.path.join(DATA_FOLDER, "best_shape_rec_no_cont_6")

with open(DESC_FILE, "rb") as input:
    dataset = pickle.load(input)

def mutate(f):
    indexf = (random.randint(0, int(len(f)/2)-1))*2
    indexs = indexf + 1
    first = f[indexf]
    second = f[indexs]

    avg = (first + second)/2
    span = abs(second - first)

    shrink = random.uniform(-0.1, 0.1)
    move = random.uniform(-0.01, 0.01)

    new_avg = avg + move
    new_span = span * (1+shrink)

    new_first = new_avg-new_span/2
    new_second = new_avg+new_span/2

    if new_first < 0:
        new_first = 0

    if new_second > 256:
        new_second = 256

    f[indexf] = new_first
    f[indexs] = new_second

    return f

def classify(genes, desc):

    params = ["perimeter_ratio", "area_perimeter_ratio", "area_ratio"]
    n_pars = len(params)
    genes_per_shape = n_pars*2

    # print 4*genes_per_shape

    shapes = ["square", "triangle", "circle", "octagon"]

    for j, shape in enumerate(shapes):

        is_this = True

        for i, param in enumerate(params):
            min = genes[j*genes_per_shape + 2*i]
            max = genes[j*genes_per_shape + 2*i + 1]

            # print j*genes_per_shape + 2*i, j*genes_per_shape + 2*i + 1, j, i

            if not (min <= desc[param] <= max):
                is_this = False
                break

        if is_this:
            return shape

    return "__other__"


def eval(child, data, sa):
    # type: (Individual, FitmentData, GeneticAlgorithm) -> None

    genes = child.genes

    correct_shape = 0
    correct_interesting_or_not = 0
    tot = 0
    for name,descs in dataset.iteritems():
        for desc in descs:
            tot += 1
            classif = classify(genes, desc)
            if name == "__other__" and classif == "__other__":
                # print classif, name
                correct_interesting_or_not += 1
            elif name != "__other__" and classif != "__other__":
                # print classif, name
                correct_interesting_or_not += 1

            if classif == name:
                correct_shape += 1

    child.additional_data["correct_interesting_or_not_perc"] = float(correct_interesting_or_not)/tot
    child.additional_data["correct_shape_perc"] = float(correct_shape)/tot
    child.fit_eval = 0*child.additional_data["correct_shape_perc"] + 1*child.additional_data["correct_interesting_or_not_perc"]
    # child.fit_eval = -abs(result - RES)

def on_best_changed(data, sa):
    # type: (AnnealingFitmentData, SimulatedAnnealing) -> None

    print data.best.additional_data["correct_interesting_or_not_perc"], \
          data.best.additional_data["correct_shape_perc"],\
          data.best.fit_eval,\
          data.iteration

    with open(BESTFILE, "wb") as output:
        pickle.dump(data.best, output)

def on_new_iter(data, sa):
    # type: (AnnealingFitmentData, SimulatedAnnealing) -> None

    print data.current_individual.additional_data["correct_interesting_or_not_perc"]

def generate_genes():
    return [0.6120734561249618, 2.436058594114802, 19.746865649357787, 148.66980101730888,
            0.13282967709756266, 1.5025848155226362, 1.103214240596622, 1.749216563370418,
            24.889918144368103, 88.6725818556319, 0.4746858989934335, 1.5982128147412467,
            1.3647390147377194, 1.4492569305546723, 64.68498704950849, 112.04417961715816,
            0.5720297442740886, 0.7126825846430317, 1.3405966035214283, 1.4794965343382878,
            60.31473681280744, 107.85192985385922, 0.5437998399444822, 0.7971932720878535]


# SA = SimulatedAnnealing(
#
#     random_move= get_intelligent_random_move_fn(generate_genes(), log_mutate_actions(-5, 3, 0.2)),
#     # random_move= get_mutate_to_random_move_fn(get_repeated_mutate_with_p_fn(0.2, 3, get_mutate_random_gene_fn(log_mutate_actions(-5, 1, 0.2)))),
#     evaluate=eval,
#     schedule=get_linear_schedule_fn(0.000001, 1000),
#     on_best_changed=on_best_changed
#
# )

SA2 = SimulatedAnnealing(

    random_move= get_mutate_to_random_move_fn(get_repeated_mutate_with_p_fn(0.05, 10, mutate)),
    evaluate=eval,
    schedule=get_linear_schedule_fn(0.001, 1000000),
    on_best_changed=on_best_changed

)

try:
    with open(BESTFILE, "rb") as input:
        ind = pickle.load(input)
        print "First ind loaded from file"
        print ind
except Exception as ex:
    ind = generate_genes()

print "SA1--------------------------------"
data = SA2.fit(ind)
# print "SA2--------------------------------"
# data2 = SA2.fit(ind)



print data.best