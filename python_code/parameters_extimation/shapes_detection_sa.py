import os
import pickle
import random

from sympy import sin

from parameters_extimation.genetic_programming import FitmentData, get_repeated_mutate_with_p_fn, \
    get_mutate_random_gene_fn
from parameters_extimation.genetic_programming import GeneticAlgorithm
from parameters_extimation.genetic_programming import Individual
from parameters_extimation.probability_extract import log_mutate_actions
from parameters_extimation.simulated_annealing import SimulatedAnnealing, get_mutate_to_random_move_fn, \
    get_linear_schedule_fn, AnnealingFitmentData, get_intelligent_random_move_fn

RES = 0
N_GENES = 56
POP_SIZE = 100
MAX_GENS_NO_IMPROVEMENT = 1000

DATASET_FOLDER = "/home/davide/datasets"
DESC_FILE = os.path.join(DATASET_FOLDER, "descriptors_decimation_1")

DATA_FOLDER = "data"
BESTFILE = os.path.join(DATA_FOLDER, "best_shape_rec_2")

with open(DESC_FILE, "rb") as input:
    dataset = pickle.load(input)

def mutate(f):
    indexf = (random.randint(0, int(len(f)/2)-1))*2
    indexs = indexf + 1
    first = f[indexf]
    second = f[indexs]

    avg = (first + second)/2
    span = abs(second - first)

    shrink = random.uniform(-0.1, 0.1)
    move = random.uniform(-0.01, 0.01)

    new_avg = avg + move
    new_span = span * (1+shrink)

    new_first = new_avg-new_span/2
    new_second = new_avg+new_span/2

    if new_first < 0:
        new_first = 0

    if new_second > 256:
        new_second = 256

    f[indexf] = new_first
    f[indexs] = new_second

    return f

def classify(genes, desc):

    params = ["mean", "perimeter_ratio", "std", "d1_mean", "d1_std",
                                   "area_perimeter_ratio", "area_ratio"]
    n_pars = len(params)
    genes_per_shape = n_pars*2

    # print 4*genes_per_shape

    shapes = ["square", "triangle", "circle", "octagon"]

    for j, shape in enumerate(shapes):

        is_this = True

        for i, param in enumerate(params):
            min = genes[j*genes_per_shape + 2*i]
            max = genes[j*genes_per_shape + 2*i + 1]

            # print j*genes_per_shape + 2*i, j*genes_per_shape + 2*i + 1, j, i

            if not (min <= desc[param] <= max):
                is_this = False
                break

        if is_this:
            return shape

    return "__other__"


def eval(child, data, sa):
    # type: (Individual, FitmentData, GeneticAlgorithm) -> None

    genes = child.genes

    correct_shape = 0
    correct_interesting_or_not = 0
    tot = 0
    for name,descs in dataset.iteritems():
        for desc in descs:
            tot += 1
            if name == "__other__" and classify(genes, desc) == name:
                correct_interesting_or_not += 1
            elif name != "__other__" and classify(genes, desc) != "__other__":
                correct_interesting_or_not += 1

            if classify(genes, desc) == name:
                correct_shape += 1

    child.additional_data["correct_interesting_or_not_perc"] = float(correct_interesting_or_not)/tot
    child.additional_data["correct_shape_perc"] = float(correct_shape)/tot
    child.fit_eval = 0.7*child.additional_data["correct_shape_perc"] + 0.3*child.additional_data["correct_interesting_or_not_perc"]
    # child.fit_eval = -abs(result - RES)

def on_best_changed(data, sa):
    # type: (AnnealingFitmentData, SimulatedAnnealing) -> None

    print data.best.additional_data["correct_interesting_or_not_perc"], \
          data.best.additional_data["correct_shape_perc"],\
          data.best.fit_eval,\
          data.iteration

    with open(BESTFILE, "wb") as output:
        pickle.dump(data.best, output)

def on_new_iter(data, sa):
    # type: (AnnealingFitmentData, SimulatedAnnealing) -> None

    print data.current_individual.additional_data["correct_interesting_or_not_perc"]

def generate_genes():
    return [0.35075388370920085, 0.9842330135775483, 0.7425487329381255, 2.2499208215783133, 0.0031721215914113088,
            0.2482570924126042, 0.011117949414520632, 0.024532919254811093, -0.0033910421491859695,
            0.039630104098601585, 17.18023022124698, 130.10492602875303, -6.339923197524893, 8.306645135821725,
            0.29842555788630704, 0.7646245779527681, 1.174607728300848, 1.659138625487158, 0.10600310232470833,
            0.18207772825086216, 0.018179167465427012, 0.02482126393863935, 0.013331020175167425, 0.035513766303173794,
            21.27306988771572, 79.36364886228428, 0.5895342197353326, 1.6483228557774336, 0.4071193571577391,
            1.190316787041693, 1.2936977114101396, 1.5342539666281256, -0.07347350924370782, 0.2936044956938095,
            -0.005182312850617855, 0.027065987754367346, -0.005649260541119914, 0.021698512007678562,
            33.35011045166436, 120.57176454833564, 0.399354151105501, 0.9751270365356717, 0.4020828494000596,
            1.1391189664943364, 1.2722621947298876, 1.5575320420533691, -0.06014724970000801, 0.2795635433376287,
            0.0015974133854429855, 0.024480126479907383, -0.001069025382949857, 0.022983363056810473,
            31.035366679082486, 115.22244582091751, 0.4024771564899594, 1.03238124549617]


# SA = SimulatedAnnealing(
#
#     random_move= get_intelligent_random_move_fn(generate_genes(), log_mutate_actions(-5, 3, 0.2)),
#     # random_move= get_mutate_to_random_move_fn(get_repeated_mutate_with_p_fn(0.2, 3, get_mutate_random_gene_fn(log_mutate_actions(-5, 1, 0.2)))),
#     evaluate=eval,
#     schedule=get_linear_schedule_fn(0.000001, 1000),
#     on_best_changed=on_best_changed
#
# )

SA2 = SimulatedAnnealing(

    random_move= get_mutate_to_random_move_fn(get_repeated_mutate_with_p_fn(0.05, 10, mutate)),
    evaluate=eval,
    schedule=get_linear_schedule_fn(0.001, 1000000),
    on_best_changed=on_best_changed

)

try:
    with open(BESTFILE, "rb") as input:
        ind = pickle.load(input)
        print "First ind loaded from file"
        print ind
except Exception as ex:
    ind = generate_genes()

print "SA1--------------------------------"
data = SA2.fit(ind)
# print "SA2--------------------------------"
# data2 = SA2.fit(ind)



print data.best