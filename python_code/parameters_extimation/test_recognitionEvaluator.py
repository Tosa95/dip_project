import os
import pickle
import random

from parameters_extimation.genetic_programming import Individual
from parameters_extimation.recognition_evaluation import RecognitionEvaluator

print os.path.split(os.getcwd())[0]

os.chdir(os.path.split(os.getcwd())[0])

DATASETS_FOLDER = "/home/davide/datasets"
DATASET = os.path.join(DATASETS_FOLDER, "images2018-11-19 13:00:40.341455.pickle")

with open(DATASET, "rb") as input:
    dataset = pickle.load(input)

r = RecognitionEvaluator(dataset)

child = Individual([random.uniform(-0.1, 0.1) for _ in xrange(7)])
r.eval_simple(child)

print child

r.stop()