import os
import pickle
import random
from math import exp

from parameters_extimation import recognition_evaluation
from parameters_extimation.genetic_programming import FitmentData, get_repeated_mutate_with_p_fn, \
    get_mutate_random_gene_fn, get_standard_cross_over_fn, get_standard_next_gen_fn, get_no_improve_terminate_fn
from parameters_extimation.genetic_programming import GeneticAlgorithm
from parameters_extimation.genetic_programming import Individual
from parameters_extimation.probability_extract import log_mutate_actions
from parameters_extimation.recognition_evaluation import RecognitionEvaluator
from parameters_extimation.simulated_annealing import AnnealingFitmentData, get_avoid_useless_work_schedule_fn, \
    get_intelligent_random_move_fn
from parameters_extimation.simulated_annealing import get_mutate_to_random_move_fn, get_linear_schedule_fn, \
    SimulatedAnnealing

DATASETS_FOLDER = "/home/davide/datasets"
DATASET = os.path.join(DATASETS_FOLDER, "images2018-11-19 13:00:40.341455.pickle")

SA_ITERATIONS = 1000

IMAGES_KEPT = 990

N_THREADS = 8
ACCEPTED_ERROR = 0.1
MEGAPIXELS = 0.01
MIN_CONTOUR = 10
WEIGHT_UPDATE_FACTOR = 0
RESET_BEST_AFTER_NO_IMPROV_FOR = 300

N_PARAMS = 11

DATA_FOLDER = "parameters_extimation/data"

BESTFILE = os.path.join(DATA_FOLDER, "best_comb_10")
BESTFILE_GENETIC = os.path.join(DATA_FOLDER, "best_comb_10")

INIT_T = 0.02
ITERATIONS = 5000

with open(DATASET, "rb") as input:
    dataset = pickle.load(input)

rec_eval = RecognitionEvaluator(
    dataset=dataset,
    keep_n=IMAGES_KEPT,
    shuffle=True,
    n_threads=8,
    chunks_threads_factor=2,
    megapixels=0.01,
    min_contour_points=10,
    time_weight=0.0,
    penalize_high_genes_factor=0.0
)

def on_best_changed(data, sa):
    # type: (AnnealingFitmentData, SimulatedAnnealing) -> None
    print data.best
    with open(BESTFILE, "wb") as output:
        pickle.dump(data.best, output)

def generate_genes():
    return [random.uniform(-0.1, 0.1) for _ in xrange(N_PARAMS)]

def print_state(data, sa):
    # type: (AnnealingFitmentData, SimulatedAnnealing) -> None
    # print " "
    # print sum(data.pop.eval_list)/len(data.pop.eval_list)
    print data.iteration, data.current_individual.additional_data["perc_rec"], \
        data.last_trial.additional_data["perc_rec"], data.iterations_with_no_improv, data.T

    if data.iterations_with_no_improv > RESET_BEST_AFTER_NO_IMPROV_FOR:
        data.current_individual = data.keep_bests.get_random_item()
        data.iterations_with_no_improv = 0

def eval_adapter(child, data, sa):
    return rec_eval.eval_simple(child)

SA = SimulatedAnnealing(

    random_move= get_intelligent_random_move_fn(generate_genes(), log_mutate_actions(-5, 0.5, 0.2)),
    # random_move=get_mutate_to_random_move_fn(get_repeated_mutate_with_p_fn(0.2, 10, get_mutate_random_gene_fn(log_mutate_actions(-5, 1, 0.2)))),
    evaluate=eval_adapter,
    schedule=get_avoid_useless_work_schedule_fn(0.00001, 30, get_linear_schedule_fn(INIT_T, ITERATIONS)),
    on_best_changed=on_best_changed,
    on_new_iteration=print_state

)

try:

    try:
        with open(BESTFILE_GENETIC, "rb") as input:
            first_ind = pickle.load(input)
            print "First ind loaded from genetic file"
            print first_ind
    except Exception as ex:
        first_ind = Individual(generate_genes())

    SA.fit(first_ind.genes)

finally:
    rec_eval.stop()