import cv2

from lib.recognition_utils import histogram_equalize

cam = cv2.VideoCapture(0)


ret, w_frame = cam.read()


cam.release()

cv2.imshow('result', w_frame)
cv2.imshow('eq', histogram_equalize(w_frame))
while cv2.waitKey(1) != 27:
    pass