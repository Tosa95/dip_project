import datetime
from math import floor, ceil
from random import randint

import cv2

import numpy as np
from mpmath.functions.functions import im

import fast_shape_matching
from config import CONTOUR_PLOT_POINTS, SHAPE_MATCH_THRESHOLD, MIN_SHAPE_CONTOUR_POINTS, MAX_DIFF_THRESHOLD, \
    MAX_DIFF_ON_MEAN
from lib.img_utils import resize
from lib.recognition_utils import LastSeen
from load_shapes import load_shapes_plots

def print_fps(image, last_timing):
    h,w,_ = image.shape
    font = cv2.FONT_HERSHEY_COMPLEX_SMALL
    fps = 1/last_timing.total_seconds()
    cv2.putText(image, "FPS: %d" % int(fps), (0, h-1), font, 2, (0,255,0), 4)


def print_relevant_shapes(image, relevant, wr, hr):

    for descriptor in relevant:

        bb = descriptor["bounding_box"]
        label = descriptor["name"]

        bb = [int(floor(bb[0]/hr)), int(floor(bb[1]/wr)), int(ceil(bb[2]/hr)), int(ceil(bb[3]/wr))]

        color = (randint(50, 255), randint(50, 255), randint(50, 255))

        cv2.rectangle(image, (bb[1], bb[0]), (bb[3], bb[2]), color, 1)

        font = cv2.FONT_HERSHEY_COMPLEX_SMALL

        cv2.putText(image, label, (bb[1], bb[2]), font, 0.7, color, 1)


if __name__ == "__main__":

    test = False

    test_img = cv2.imread("test_images/test_image_3_areas.png").astype(np.double)

    # IMPORTANTISSIMO il cv2.IMREAD_COLOR!!! altrimenti la parte in C++ sclera forte!!!!!!!!!
    image = cv2.imread("test_images/pericolo1.jpg", cv2.IMREAD_COLOR).astype(np.double)

    small, wr, hr = resize(image, 0.05)

    h, w, _ = small.shape

    blurred = cv2.GaussianBlur(small, (5, 5), 0).astype(np.uint8)

    plots = load_shapes_plots()

    begin = datetime.datetime.now()
    recognition_results = fast_shape_matching.shape_matching(blurred, 70, plots, SHAPE_MATCH_THRESHOLD, MAX_DIFF_THRESHOLD, MAX_DIFF_ON_MEAN, CONTOUR_PLOT_POINTS,
                                        MIN_SHAPE_CONTOUR_POINTS)
    end = datetime.datetime.now()
    timing = end - begin
    print_relevant_shapes(image, recognition_results, wr, hr)
    print_fps(image, timing)

    # print recognition_results
    ls = LastSeen()

    for i in xrange(7):
        ls.add_img(blurred)


    cv2.imshow('result', image.astype(np.uint8))
    cv2.imshow('last_seen', ls.get_grid().astype(np.uint8))
    cv2.imwrite("img_for_exam/bounding_boxes.png", image.astype(np.uint8))
    cv2.waitKey(0)
    cv2.destroyAllWindows()