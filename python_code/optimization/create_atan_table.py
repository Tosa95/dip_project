import numpy as np
from math import atan, pi

ASINTOTIC_MAX_ERROR = 0.001
ERROR_COMPUTATION_STEP = 0.001
SAMPLES = 50000
NUMBERS_PER_LINE = 7

def compute_k(asintotic_max_error, step):

    a = 1

    while (pi/2 - atan(a)) > asintotic_max_error:
        a = a + step

    return a


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def get_C_table(table, k, step, num_per_line):

    k_declr = "const double K = %f;\n" % k
    step_declr = "const double STEP = %f;\n" % step
    length_declr = "const int LENGTH = %d;\n" % len(table)

    lineized_array = chunks(table, num_per_line)

    printable_values = []

    for line in lineized_array:
        printable_values.append(",".join([str(x) for x in line]) + ",")

    last = printable_values[len(printable_values)-1]
    last = last[0:len(last)-1]
    printable_values[len(printable_values) - 1] = last

    table_declr = "const double TABLE[] = {\n%s\n};" % "\n".join(printable_values)

    return k_declr + step_declr + length_declr + table_declr

if __name__ == "__main__":

    k = compute_k(ASINTOTIC_MAX_ERROR, ERROR_COMPUTATION_STEP)

    print atan(k), pi/2

    step = 2*k / SAMPLES
    i = -k

    table = []

    while i <= k:
        table.append(atan(i))
        i += step

    C_table = get_C_table(table, k, step, NUMBERS_PER_LINE)

    with open("table.txt", "w") as f:
        f.write(C_table)


