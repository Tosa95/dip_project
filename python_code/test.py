import datetime
import pprint

import cv2
import matplotlib as mpl
import numpy as np

import fast_shape_matching
from config import CONTOUR_PLOT_POINTS, SHAPE_MATCH_THRESHOLD, MIN_SHAPE_CONTOUR_POINTS
from lib.area_descriptor import get_region_image, collapse_inner_regions
from lib.hierarchic_regions_c import hierarchic_region_growing
from lib.img_utils import resize
from lib.shape_matching import compute_contour_plot, get_best_matching_shape_2

from region_growing import draw_regions
from load_shapes import load_shapes_plots

mpl.use('Agg')
import matplotlib.pyplot as plt

test = False

test_img = cv2.imread("test_images/test_image_3_areas.png").astype(np.double)

# IMPORTANTISSIMO il cv2.IMREAD_COLOR!!! altrimenti la parte in C++ sclera forte!!!!!!!!!
image = cv2.imread("test_images/pericolo1.jpg", cv2.IMREAD_COLOR).astype(np.double)

cv2.imwrite("img_for_exam/full.png", image)

small, wr, hr = resize(image, 0.05)

cv2.imwrite("img_for_exam/small.png", small)

h, w, _ = small.shape

blurred = cv2.GaussianBlur(small, (5, 5), 0).astype(np.uint8)

cv2.imwrite("img_for_exam/blurred.png", blurred)

plots = load_shapes_plots()

begin = datetime.datetime.now()

regions, map, ordered = hierarchic_region_growing(test_img if test else blurred, 70)

time_rg = datetime.datetime.now()

print "bound first region: %d" % len(regions[1].get_bound_points())

#print color_distance(np.array([0, 0, 0] np.array([255, 255, 255])))

#ordered = BFS(regions)

time_bfs = datetime.datetime.now()

#subregions(regions, ordered)

time_subregions = datetime.datetime.now()

# relevant = classify_relevant_shapes(regions, ordered, map, plots, SHAPE_MATCH_THRESHOLD, CONTOUR_PLOT_POINTS, MIN_SHAPE_CONTOUR_POINTS)

time_classify = datetime.datetime.now()

# pp = pprint.PrettyPrinter(indent=4)
# pp.pprint(plots)

print "AAAAAAAAAAAAAAAAAAAA"
print fast_shape_matching.shape_matching(blurred, 70, plots, SHAPE_MATCH_THRESHOLD, CONTOUR_PLOT_POINTS, MIN_SHAPE_CONTOUR_POINTS)
# print [r[0].get_bounding_box() for r in relevant]
print "time: " + str(datetime.datetime.now() - begin) + " time_rg: " + str(time_rg - begin) + \
      " time_bfs:" + str(time_bfs - time_rg) + " time_subregions:" + str(time_subregions - time_bfs) + \
      " time_classify:" + str(time_classify - time_subregions)
#print relevant

regionized = draw_regions(map, regions, draw_bounds=False, draw_bounding_boxes=False, draw_distance=False)

cv2.imwrite("img_for_exam/regionized.png", regionized)

# print_relevant_shapes(regionized, relevant)

# collapsed = collapse_inner_regions(2247, regions, map, True)
# cv2.imshow('selected_region',
#            cv2.resize(get_region_image(h, w, collapsed),
#                       None, fx=1/wr, fy=1/hr))
# contour_plot = compute_contour_plot(collapsed, CONTOUR_PLOT_POINTS)
#
# print get_best_matching_shape(contour_plot, plots, SHAPE_MATCH_THRESHOLD)

def clbk(event,x,y,flags,param):
    if event == cv2.EVENT_LBUTTONDOWN:
        print "LABELLL: %d\n" % map.get_label(int(y*hr),int(x*wr))
        collapsed = collapse_inner_regions(map.get_label(int(y*hr),int(x*wr)), regions, map, True)
        selected_shape_img = cv2.resize(get_region_image(h, w, collapsed), None, fx=1/wr, fy=1/hr)

        cv2.imshow('selected_region', selected_shape_img)
        cv2.imwrite("img_for_exam/selected.png", get_region_image(h, w, collapsed))
        contour_plot = compute_contour_plot(collapsed, CONTOUR_PLOT_POINTS)
        plt.figure(1)
        plt.clf()
        plt.title("Distance from centroid")
        plt.plot(range(128),contour_plot)
        plt.draw()
        plt.savefig("img_for_exam/selected_graph.png", dpi=120)

        match_data = get_best_matching_shape_2(contour_plot, plots, SHAPE_MATCH_THRESHOLD)

        plt.figure(1)
        plt.clf()
        plt.title("Distance from centroid - Best Match at distance: %f" % match_data["best_match_distance"] )
        plt.plot(range(128), match_data["best_match_contour"])
        plt.draw()
        plt.savefig("img_for_exam/selected_best_match_graph.png", dpi=120)

        print match_data["best_match_label"]

cv2.imshow('original', image.astype(np.uint8))
cv2.imshow('blurred', blurred.astype(np.uint8))
cv2.imshow('regionized', cv2.resize(regionized, None, fx=1/wr, fy=1/hr))
cv2.setMouseCallback('regionized', clbk)
cv2.waitKey(0)
cv2.destroyAllWindows()

print len(regions)

pp = pprint.PrettyPrinter(indent=4)
pp.pprint(regions)
pp.pprint(collapse_inner_regions(2,regions,map,False))