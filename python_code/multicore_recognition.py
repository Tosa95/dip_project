import collections
import ctypes
from Queue import Empty
from time import sleep

import datetime
import numpy as np

import cv2
import numpy as np

from config import REGION_GROWING_THRESHOLD
from lib.recognition_utils import LastSeen, elaborate_single_frame, preprocess_image, recognize_single_frame_shapes, \
    print_relevant_shapes, save_relevant_shapes, print_fps, extimate_region_growing_threshold, histogram_equalize, \
    crop_image, recognize_single_frame_shapes_fast
from threading import Thread
from Queue import Queue

MAX_ENQUEUED_FRAMES = 2
REQUIRED_FPS = 30
RECOGNITION_PROCESSES = 4
WIDTH = 640
HEIGHT = 480

class MultithreadRecognizer:

    def __init__(self, max_enqueued_frames = MAX_ENQUEUED_FRAMES, required_fps = REQUIRED_FPS, recognition_processes_number = RECOGNITION_PROCESSES):
        self._to_process_queue = Queue(max_enqueued_frames)
        self._recognized_queue = Queue(max_enqueued_frames)
        self._stop_capture_queue = Queue()
        self._required_fps = required_fps
        self._recognition_processes_number = recognition_processes_number
        self._capture_process = Thread(target=self.capture_process_body)
        self._recognition_processes = []

        for i in xrange(self._recognition_processes_number):
            self._recognition_processes.append(Thread(target=self.recognition_process_body, args=[i]))

    def get_recognized_queue(self):
        return self._recognized_queue

    def recognition_process_body(self, id):

        stop = False

        while not stop:

            frame = self._to_process_queue.get()

            if frame is not None:

                # print "Recognition %d" % id
                # print "Before processing %d" % id
                preprocessed_frame, wr, hr = preprocess_image(frame, to_hsv=False)
                # print "After preprocessing %d" % id

                # extimated_rg_th = extimate_region_growing_threshold(preprocessed_frame)

                extimated_rg_th = REGION_GROWING_THRESHOLD

                # print "Extimated rg th: ", extimated_rg_th

                recognition_result = recognize_single_frame_shapes(preprocessed_frame,
                                                                   region_growing_threshold=extimated_rg_th)

                # recognition_result = recognize_single_frame_shapes_fast(preprocessed_frame)
                # print "After processing %d" % id
                self._recognized_queue.put({"frame": frame, "recognition_result": recognition_result,
                                            "wr": wr, "hr": hr})
                # print self._recognized_queue.qsize()

            else:
                stop = True
                print "STOPPING"

        print "STOPPED"



    def capture_process_body(self):
        stop = False
        cam = cv2.VideoCapture(0)

        i = 0

        last_enqueued = datetime.datetime.now()

        while not stop:
            begin = datetime.datetime.now()
            ret, w_frame = cam.read()
            w_frame = crop_image(w_frame)
            after_cam = datetime.datetime.now()
            # print "QSIZE: %d" % self._to_process_queue.qsize()
            # print "RECQSIZE: %d" % self._recognized_queue.qsize()

            # w_frame = histogram_equalize(w_frame)

            # print "Capture %d" % i
            spent = datetime.datetime.now() - last_enqueued
            # print spent, after_cam - begin
            to_sleep = 1.0 / self._required_fps - spent.total_seconds()

            self._to_process_queue.put(w_frame)
            last_enqueued = datetime.datetime.now()

            if to_sleep > 0:
                # print "sleeping %f seconds" % to_sleep
                sleep(to_sleep)
            try:
                stop = self._stop_capture_queue.get(block=False)
            except Empty:
                pass
            i += 1

        cam.release()
        # print "CAPTURED: %d" % i

    def start(self):

        for i in xrange(self._recognition_processes_number):
            self._recognition_processes[i].start()

        self._capture_process.start()

    def stop(self):

        self._stop_capture_queue.put(True)
        self._capture_process.join()
        print "Capture stopped"

        for i in xrange(self._recognition_processes_number):
            self._to_process_queue.put(None)
            # print "Put stop token number %d" % i
            # print self._to_process_queue.qsize()

        for i in xrange(self._recognition_processes_number):
            self._recognition_processes[i].join()
            print "Recognition process %d has been stopped" % i

if __name__ == "__main__":

    ls = LastSeen()


    # cam.set(cv2.cv.CV_CAP_PROP_BRIGHTNESS, 0.40)
    # cam.set(cv2.cv.CV_CAP_PROP_EXPOSURE, 0.0)
    # cam.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, 640)
    # cam.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, 480)

    mr = MultithreadRecognizer()

    mr.start()

    stop = False

    last = datetime.datetime.now()

    while not stop:
        recognized = mr.get_recognized_queue().get()
        current_time = datetime.datetime.now()

        frame = recognized["frame"]
        wr = recognized["wr"]
        hr = recognized["hr"]
        recognition_results = recognized["recognition_result"]

        timing = current_time - last
        last = current_time

        save_relevant_shapes(frame, recognition_results, wr, hr, ls)
        print_relevant_shapes(frame, recognition_results, wr, hr)
        print_fps(frame, timing)

        cv2.imshow('result', frame.astype(np.uint8))
        cv2.imshow('last_seen', ls.get_grid().astype(np.uint8))
        if cv2.waitKey(1) == 27:
            stop = True

    mr.stop()


    # while True:
    #     cv2.imshow('result', frame.astype(np.uint8))
    #     cv2.imshow('last_seen', ls.get_grid().astype(np.uint8))
    #     if cv2.waitKey(1) == 27:
    #         break
    #
    #
    # cv2.destroyAllWindows()
