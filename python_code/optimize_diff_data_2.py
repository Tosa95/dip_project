import os
import pickle
import random
from math import exp

from parameters_extimation import recognition_evaluation
from parameters_extimation.genetic_programming import FitmentData, get_repeated_mutate_with_p_fn, \
    get_mutate_random_gene_fn, get_standard_cross_over_fn, get_standard_next_gen_fn, get_no_improve_terminate_fn
from parameters_extimation.genetic_programming import GeneticAlgorithm
from parameters_extimation.genetic_programming import Individual
from parameters_extimation.probability_extract import log_mutate_actions
from parameters_extimation.recognition_evaluation import RecognitionEvaluator

DATASETS_FOLDER = "/home/davide/datasets"
DATASET = os.path.join(DATASETS_FOLDER, "images2018-11-19 13:00:40.341455.pickle")

POP_SIZE = 100
MAX_GENS_NO_IMPROVEMENT = 1000

IMAGES_KEPT = 300

N_THREADS = 8
ACCEPTED_ERROR = 0.1
MEGAPIXELS = 0.01
MIN_CONTOUR = 10
WEIGHT_UPDATE_FACTOR = 0

N_PARAMS = 11

DATA_FOLDER = "parameters_extimation/data"

GENFILE = os.path.join(DATA_FOLDER, "last_gen_comb_7")
BESTFILE = os.path.join(DATA_FOLDER, "best_comb_7")

with open(DATASET, "rb") as input:
    dataset = pickle.load(input)

rec_eval = RecognitionEvaluator(
    dataset=dataset,
    keep_n=IMAGES_KEPT,
    shuffle=True,
    n_threads=8,
    chunks_threads_factor=2,
    megapixels=0.01,
    min_contour_points=10,
    time_weight=0.5
)

def on_best_changed(data):
    # type: (FitmentData) -> None

    print data.best.fit_eval, data.best.additional_data["sum"], sum(data.pop.eval_list)/len(data.pop.eval_list), data.best.genes

def generate_genes():
    return [random.uniform(-0.1, 0.1) for _ in xrange(N_PARAMS)]

def generate_pop():
    return [generate_genes() for _ in xrange(POP_SIZE)]

def print_state(data):
    # print " "
    # print sum(data.pop.eval_list)/len(data.pop.eval_list)
    # print data.best
    with open(BESTFILE, "wb") as output:
        pickle.dump(data.best, output)

def update_weigths(data):
    weights = data.additional_data["weights"]
    n_recs = data.additional_data["n_recs"]

    recognized_using_all_children = sum([ (1 if r>0 else 0) for r in n_recs])
    print "Rec using all childrens: ", recognized_using_all_children

    avg_recs = float(sum(n_recs))/len(n_recs)

    avg_recs_perc = (avg_recs/len(data.pop))*100

    for i in xrange(len(weights)):
        rec_perc = (float(n_recs[i])/len(data.pop))*100
        weights[i] = exp((avg_recs_perc - rec_perc)*WEIGHT_UPDATE_FACTOR)
        n_recs[i] = 0

def on_new_gen(data):

    print " "

    print data.gen, sum(data.pop.eval_list)/len(data.pop.eval_list)

    print "\nBEST OVERALL:"
    print data.best

    best_c = None

    for c in data.pop:
        if best_c is None or c.fit_eval > best_c.fit_eval:
            best_c = c

    print "\nBEST OF POP:"
    print best_c

    weights = data.additional_data["weights"]
    n_recs = data.additional_data["n_recs"]
    print [("%3d"%n_rec, "%.2f"%w) for (n_rec, w) in zip(n_recs, weights)]

    update_weigths(data)

    with open(GENFILE, "wb") as output:
        pickle.dump(data.pop, output)

def print_perc(p):
    # sys.stdout.write("%.2f " % p)
    pickle.sys.stdout.write(".")
    pickle.sys.stdout.flush()

GA = GeneticAlgorithm(

    mutate=get_repeated_mutate_with_p_fn(0.02, 10, get_mutate_random_gene_fn(log_mutate_actions(-5, 1, 0.2))),
    evaluate=rec_eval.eval,
    cross_over=get_standard_cross_over_fn(),
    next_gen=get_standard_next_gen_fn(),
    terminate=get_no_improve_terminate_fn(MAX_GENS_NO_IMPROVEMENT),
    on_best_changed=print_state,
    on_new_gen=on_new_gen,
    on_next_gen_perc_change=print_perc


)

try:

    try:
        with open(GENFILE, "rb") as input:
            first_gen = pickle.load(input)
            print "First gen loaded from file"
            print first_gen
    except Exception as ex:
        first_gen = generate_pop()

    GA.fit(
        first_gen,
        initial_additional_data={"n_recs": [0 for _ in rec_eval.used_dataset],
                                 "weights": [1.0 for _ in rec_eval.used_dataset]}
    )

finally:
    rec_eval.stop()